

class Org {

  constructor(org_id, name,type) {
    this.org_id = org_id;
    this.name = name;
    this.type = type;
  }

  toStorage() {
    return {org_id : this.org_id, name: this.name, type: this.type};
  }
  
  static fromStorage(o) {
    return new Org(o.org_id, o.name,o.type);
  }


  getId() {
    return this.org_id;
  }

  getName() {
    return this.name;
  }

  getType() {
    return this.type;
  }
}

Org.Type = {
  SUPPLY_CHAIN: 'supply-chain',
  PROVIDER: 'provider',
}


module.exports = Org;

