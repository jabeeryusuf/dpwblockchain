'use strict'

const schemas = {

  products: {
    schema: {
      sku:                    String,
      name:                   String,
      apn:                    String,
      department:             String,
      image:                  String,
      outer_carton_height:    Number,
      outer_carton_width:     Number,
      outer_carton_length:    Number,
      pack_size:              Number,
      carton_cbm:             Number,
    },
    key:  ['sku']
  },

  containers: {
    schema: {
      container_id:           String,
      type:                   String,
      ref:                    String,
      parent_container_id:    String,
      create_time:            Date,
      update_time:            Date
    },
    key: ['container_id']
  },

  buildings: {
    schema: {
      container_id:       String,
      address:            String,
      name:               String,
      lat:                Number,
      lng:                Number,
      create_time:        Date,
      update_time:        Date
    },
    key: ['container_id']
  },

  purchase_orders: {
    schema: {
      purchase_order_id:     String,
      create_date:           Date,
      create_time:           Date,
      update_time:           Date
    },
    key: ['purchase_order_id']
  },

  purchase_order_items: {
    schema: {
      purchase_order_item_id:    String,
      purchase_order_id:         String,
      sku:                       String,
      quantity:                  Number,
      recv_quantity:             Number,
      approx_recv_date:          Date,
      origin_container:          String,
      dst_container:             String,
      port_of_loading:           String,
      mcc_warehouse:             String,
      org_country:               String,
      create_time:               Date,
      update_time:               Date
    },
    key: ['purchase_order_item_id', 'purchase_order_id']
  },

  inventory_items: {
    schema: {
      container_id:               String,
      sku:                        String,
      purchase_order_id:          String,
      purchase_order_item_id:     String,
      shipment_id:                String,
      qty_per_carton:             Number,
      quantity:                   Number,
      receive_date:               Date,
      create_time:                Date,
      update_time:                Date
    },
    key: ['container_id','sku','purchase_order_id','purchase_order_item_id']
  },

  shipments: {
    schema: {
      shipment_id:                  String,
      source_container_id:          String,
      destination_container_id:     String,
      vessel_container_id:          String,
      eta:                          Date,
      etd:                          Date,
      create_time:                  Date,
      update_time:                  Date
    },
    key: ['shipment_id']
  },

  shipment_items: {
    schema: {
      shipment_id:                String,
      purchase_order_id:          String,
      purchase_order_item_id:     String,
      sku:                        String,
      delivery_quantity:          Number,
      delivery_time:              Date,
      create_time:                Date,
      update_time:                Date
    },
    key: ['shipment_id', 'purchase_order_id', 'purchase_order_item_id']
  },

  vehicle_locations: {
    schema: {
      container_id:       String,
      last_port:          String,
      next_port:          String,
      lat:                Number,
      lng:                Number,
      signal_age:         String,
      atd:                String,
      eta:                String,
      speed:              String,
      timestamp:          Date,
      vessel_name:        String,
      course:             String,
      create_time:        Date,
      update_time:        Date
    },
    key: ['container_id', 'signal_age']
  },

  users: {
    schema: {
      user_id:            String,
      organization_id:    String,
      username:           String,
      name:               String,
      email:              String,
      password:           String,
    },
    key: ['user_id']
  },

  organizations: {
    schema: {
      organization_id:    String,
      name:               String,
      domain:             String,
    },
    key: ['organization_id']
  },

  actions: {
    schema: {
      action_id:          String,
      container_id:       String,
      type:               String,
      action_status:      String,
      request_date:       Date,
      approval_date:      Date,
      org:                String,
      create_time:        Date,
      update_time:        Date
    },
    key: ['action_id']
  },

}




class FcSchema {

  getPostgresType(proto) {
    let t = new proto();

    let type = 'text';
    if (t instanceof Date) {
      type  = 'timestamptz';
    }
    else if (t instanceof Number) {
      type = 'numeric';
    }

    return type;
  }

  generatePostgresTable(table) {
    let out = console.log;
    let schema = schemas[table]['schema'];
    let keys = schemas[table]['key'];

    out('--TABLE: %s',table);
    out('--------------------------------------')
    out('DROP TABLE IF EXISTS %s;',table);
    out('\n');
    out('CREATE TABLE %s (',table);

    let cols = Object.keys(schema);

    for (let i in cols) {
      let col = cols[i];
      let type = this.getPostgresType(schema[col]);

      out('  %s %s,',col,type);
    }

    let index_commas = '';
    let index_name = table.slice();
    for (let i in keys) {
      let comma = (i == keys.length - 1) ? '' : ',';
      let key = keys[i];

      index_commas += key + comma;
      index_name += '_' + key[0];
    }

    out('  CONSTRAINT %s PRIMARY KEY (%s)',index_name, index_commas);

    out(")");



    out('\n\n');
  }


  generatePostgresView(table,couchTable) {
    let out = console.log;
    let schema = schemas[table]['schema'];
    let keys = schemas[table]['key'];


    out('--TABLE: %s',table);
    out('--------------------------------------')
    out('DROP MATERIALIZED VIEW IF EXISTS %s;',table);
    out('\n');
    out('CREATE MATERIALIZED VIEW %s',table);
    out('AS SELECT');

    let cols = Object.keys(schema);

    for (let i in cols) {
      let comma = (i == cols.length -1) ? '' : ',';
      let col = cols[i];
      let type = this.getPostgresType(schema[col]);

      out('  (doc ->> \'%s\')::%s AS %s%s',col,type,col,comma);
    }
    out('FROM %s',couchTable);
    out('WHERE (doc ->> \'docType\')::text = \'%s\';',table);
    out('\n');

    let index_commas = '';
    let index_name = table.slice();
    for (let i in keys) {
      let comma = (i == keys.length - 1) ? '' : ',';
      let key = keys[i];

      index_commas += key + comma;
      index_name += '_' + key;
    }

    out('CREATE UNIQUE INDEX IF NOT EXISTS %s ON %s (%s);',index_name,table, index_commas);
    out('\n\n');
  }


  getStorageKey(table, row) {
    let pks = schemas[table]['key'];

    let sk = table.slice();

    for (let i in pks) {
      sk += (i == 0 ? ':' : '&');
      let val = row[pks[i]];
      sk += val ? val : 'null';
    }

    return sk;
  }

  typeifyRow(table,row) {
    // TODO:
  }


  generatePostgresTableSchema() {
    let out = console.log;
    for(let table of Object.keys(schemas)) {
      this.generatePostgresTable(table);
    }

  }


  generatePostgresViewSchema() {
    let out = console.log;

    for(let table of Object.keys(schemas)) {
      this.generatePostgresView(table,'couch_mychannel_freightcc');
    }

    out(
`
CREATE OR REPLACE FUNCTION update_materialized_views()
RETURNS trigger AS
$BODY$
  BEGIN
`
);

    for (let table of Object.keys(schemas)) {
      out('  REFRESH MATERIALIZED VIEW CONCURRENTLY %s;',table);
    }
    out(
`
  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER since_checkpoint_changes AFTER UPDATE
ON since_checkpoints FOR EACH ROW
EXECUTE PROCEDURE update_materialized_views();
`
    );
  }
}


//let fcs = new FcSchema;
//fcs.generatePostgresViewSchema();
//fcs.generatePostgresTableSchema();

module.exports = FcSchema;
