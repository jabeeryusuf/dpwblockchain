
const Role = require('./Role');
const Org = require('./Org');

let i = 0;

class User {
  
  constructor(username, email,name,org_id,role ) {
    this.email = email;
    this.name = name;
    this.org_id = org_id;
    this.role = role;
  }

  getId() {
    return this.user_id;
  }


  setContext(client) {
    this.client = client;
  }

  getName() {
    return this.name;
  }

  getOrgId() {
    return this.blockchain_id;
  }

  getOrg() {
    //fix
    return new Org(this.org_id,this.org_id,Org.Type.SUPPLY_CHAIN)
  }

  getRole() {
    return this.role;
  }
  
  getEmail() {
    return this.email;
  }


}

module.exports = User;