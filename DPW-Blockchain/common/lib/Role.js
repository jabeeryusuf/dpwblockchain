



class Role {
    
  constructor(name) {
    this.privs = {};
    this.name = name;
  }

  setPrivilege(p,access) {
    return this.privs[p] = access;
  }

  getPrivilege(p) {
    return this.privs[p] ? this.privs[p] : Role.Access.NONE;
  }

  getAllPrivileges() {
    let all = {};
    let keys = Object.keys(Role.Privilege);
    
    for (let k of keys) {
      let priv = Role.Privilege[k];
      all[priv] = this.getPrivilege(priv)
    }

    return all;
  }

  getName() {
    return this.name;
  }

}


Role.Privilege = {
  PURCHASE_ORDERS: 'purchase-orders',
  PURCHASE_ORDER_ITEMS: 'purchase-order-items',
  PRODUCTS: 'products',
};

Role.Access = {
  NONE: 'none',
  READ: 'read',
  WRITE: 'write',
};



module.exports = Role;