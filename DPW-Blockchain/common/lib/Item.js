'use strict';

const util = require('util');

class Item {

  constructor(containerId, purchaseOrderId, shipmentId, sku, qty_per_carton, quantity,dst_container,volume) {
    
    this.container_id = containerId;
    this.purchaseOrderId = purchaseOrderId;
    this.shipmentId = shipmentId;
    this.dst_container= dst_container;
    this.volume = volume;

    if (qty_per_carton) {
      this.qty_per_carton = qty_per_carton;
    }
    else {
      this.qty_per_carton = 1;
    }

    this.quantity = quantity;
    this.getDestinationContainer = this.getDestinationContainer.bind(this);
  }

  setContext(client,cache) {
    this.client = client; 
    this.cache = cache;
  }

  async getDestinationContainer(){
    return await this.client.getContainer(this.dst_container); 

  }

  async getContainer() {
    //console.log(util.format('cache = %j',this.cache));
    
    if (this.cache && this.cache[this.containerId]) {
      return this.cache[this.containerId];
    }
    else if (this.client) {
      return await this.client.getContainer(this.containerId);
    }
    else {
      throw new Error('Item does not have a cached object or available context');
    }
  }


  getId() {
    return "sku" & this.sku + "&" + this.carton;
  }

}


module.exports = Item;