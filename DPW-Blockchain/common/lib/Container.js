'use strict';



class Container {

  // Constructor and context are managed by the specific fc client.
  // TODO: Combine into the constructor?
  constructor(container_id, type, ref, parent_container_id) {
    this.container_id = container_id
    this.type = type;
    this.ref = ref;
    this.parent_container_id = parent_container_id ? parent_container_id : null;
  }

  setContext(client, cache) {
    this.client = client;
    this.cache = cache;
  }


  toStorage() {
    let storage = { type: this.type, ref: this.ref };
    console.log('storage = %j', storage)

    if (this.parentId) {
      storage['parent_id'] = this.parentId;
    }

    console.log('storage = %j', storage)
    return storage;
  }

  static fromStorage(o) {
    console.log('Container.fromStorage: %j', o);
    console.log(o.type, o.ref, o.partent_id);
    let c = new Container(o.type, o.ref, o.parent_id);
    return c;
  }

  async getId() {
    let id = "";

    if (this.type === 'area') {
      let parent = await this.getParent();

      // determine the key
      id = await parent.getId() + ':';
    }

    return id + this.type + '&' + this.ref;
  }

  getRef() {
    return this.ref;
  }

  getType() {
    return this.type;
  }

  getParentId() {
    return this.parentId;
  }


  async getParent() {
    //console.log(util.format('cache = %j',this.cache));
    if (this.cache && this.cache[this.parent_container_id]) {
      return this.cache[this.parent_container_id];
    }
    /* else if (this.client) {
      return await this.client.getContainer(this.parent_container_id);
    }
    else {
      throw new Error('Item does not have a cached object or available context');
    }*/
    else{
      return false
    }
  }

  async getBuilding() {
    return await this.client.getBuilding(this.container_id);
  }


}

Container.Type = {
  WAREHOUSE: 'warehouse',
  PORT: 'port',
  AREA: 'area',
  TRANSIT: 'transit',
  TRUCK: 'truck',
  SHIP: 'ship',
  TRAIN: 'train',
  SHIPPING_CONTAINER: 'shipping-container'
}

module.exports = Container;