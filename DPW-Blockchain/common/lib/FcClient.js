
'use strict';

class FcClient {

  static getVersion() {
    return 'v0.1';
  }


  //
  // Basic Setup
  //

  async addContainer(timestamp, type, ref, parent) {
    throw new Error("addContainer not implemented");
  }


  async updateContainerParent(timestamp, container, parent) {
    throw new Error("updateContainer not implemented");
  }
  
  async getContainer(containerId) { 
    throw new Error("getContainer not implemented");
  }
  
  async getContainerByRef(type,ref) {
    throw new Error("getContainerByRef not implemented");
  }

  //
  // Inventory and container transactions
  //

  async updateContainerInventory(timestamp,container,inventory) {
    throw new Error("updateContainerInventory not implemented")
  }



  // update shipment in container?
  // update po in container?


  async getProduct(sku) {
    throw new Error("getProduct not implemented");
  }





}

module.exports = FcClient;