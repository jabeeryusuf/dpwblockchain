'use result';

const Item = require("./Item");

class Inventory {

  // Constructors
  constructor(items) {
    this.items = [];
    
    if (items) {
      
      for(let i in items) {
        let item = new Item(items[i].sku,items[i].carton,items[i].quantity);
        this.items.push(item);
      }
    }
  }

    
  static empty() { return new Inventory(); }

  static fromString(s) {
    let o = JSON.parse(s);
    return this.fromObject(o);
  }

  static fromObject(o) {
    console.log('Inventory.fromObject: %j',o);
    
    let inv = new Inventory();
    for(let i in o) {
      let item = new InventoryItem(o[i].sku,o[i].carton,o[i].quantity);
      inv.items.push(item);
    }

    return inv;
  }


  // methods
  toString() {
    return JSON.stringify(this.items);
  }


}

