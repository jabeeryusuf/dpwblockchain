class Group {

  constructor(org_id, group_id, name, type) {
    this.org_id = org_id;
    this.group_id = group_id;
    this.name = name;
    this.type = type;
  }

  toStorage() {
    return {org_id : this.org_id, group_id : this.group_id, name: this.name, type: this.type};
  }
  
  static fromStorage(o) {
    return new Group(o.org_id,o.group_id, o.name,o.type);
  }

  getId() {
    return this.group_id;
  }

  getOrgId() {
    return this.org_id;
  }

  getName() {
    return this.name;
  }

  getType() {
    return this.type;
  }
}

Group.Type = {
  USER_DEFINED: 'user-defined',
  ISSUE: 'issue',
}


module.exports = Group;
