
# install the chaincode
peer chaincode install -l node -p ../chaincode -n freightcc -v v0

# instaniate the chaincode
peer chaincode instantiate -n freightcc -C mychannel -v v0 -c '{"Args":["from","dev"]}'