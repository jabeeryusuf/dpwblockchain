# test invoke
peer chaincode invoke -n freightcc -C mychannel -c '{"Args":["testInvoke","a","10","20"]}'

# perform a query
peer chaincode query -n freightcc -C mychannel -c '{"Args":["testQuery","what is the meaning of life?"]}'
