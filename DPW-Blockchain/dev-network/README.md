# Using dev mode

Normally chaincodes are started and maintained by peer. However in “dev
mode", chaincode is built and started by the user. This mode is useful
during chaincode development phase for rapid code/build/run/debug cycle
turnaround.

We start "dev mode" by leveraging pre-generated orderer and channel artifacts for
a sample dev network.  As such, the user can immediately jump into the process
of compiling chaincode and driving calls.

## Download docker images

We use the ``fcf-setup tool`` located in the bin directory. 

```
> fcf-setup kill
> fcf-setup nuke
> fcf-setup pull

```

Issue a ``docker images`` command to reveal your local Docker Registry.  You
should see something similar to following:

```
  > docker images
  REPOSITORY                     TAG                                  IMAGE ID            CREATED             SIZE
  hyperledger/fabric-tools       latest                c584c20ac82b        9 days ago         1.42 GB
  hyperledger/fabric-tools       x86_64-1.1.0-preview  c584c20ac82b        9 days ago         1.42 GB
  hyperledger/fabric-orderer     latest                2fccc91736df        9 days ago         159 MB
  hyperledger/fabric-orderer     x86_64-1.1.0-preview  2fccc91736df        9 dyas ago         159 MB
  hyperledger/fabric-peer        latest                337f3d90b452        9 days ago         165 MB
  hyperledger/fabric-peer        x86_64-1.1.0-preview  337f3d90b452        9 days ago         165 MB
  hyperledger/fabric-ccenv       latest                82489d1c11e8        9 days ago         1.35 GB
  hyperledger/fabric-ccenv       x86_64-1.1.0-preview  82489d1c11e8        9 days ago         1.35 GB

```


## Start the Dev environment for the first time

Open three terminals and navigate to your ``dev-network`` directory in each.

### Terminal 1 - Start the network
First we start the network with an orderer, certificate authority and 
a peer in "dev mode".  It launches two additional containers -
one for the chaincode environment and a CLI to interact with the chaincode.  

```
docker-compose up -d
docker-compose logs -f
```

### Terminal 2 - Create the Channel

We have to create a channel.  We'll use the CLI container to drive these calls.

```
docker exec -it cli bash
```

You should see something like the following:

```
root@d2629980e76b:/opt/dev/bin#
```

Now initialize the channel

```
./init-channel.sh
```



### Terminal 3 - Build & start the chaincode

```
docker exec -it chaincode bash
```

Now run the chaincode:

```
npm run dev
```

The chaincode is started with peer and chaincode logs indicating successful registration with the peer.  Note that at this stage the chaincode is not associated with any channel. This is done in subsequent steps  using the ``instantiate`` command.

### Terminal 2 (Again) - Use the chaincode

Even though you are in ``--peer-chaincodedev`` mode, you still have to install the
chaincode so the life-cycle system chaincode can go through its checks normally.
This requirement may be removed in future when in ``--peer-chaincodedev`` mode.

We'll use the CLI container to drive these calls.  Now in the container install the chaincode and instanatiate it:

```
./init-chaincode.sh
```

Now do a simple invoke & query

```
./test-chaincode
```


## Starting the dev environment after a reboot

If you reboot your computer the docker containers will be stopped.  To start development
create two terminals:

### Terminal 1 - Restart docker

```
docker-compose start
docker-compose logs -f
```

### Terminal 2 - Restart the chaincode

The commands are exactly the same as the startup above except that everything has already been registered.

```
docker exec -it chaincode bash
```

In the docker container

```
npm run dev
```


## network reset

To completely reset your network you can run:

```
./reset-network.sh
```

This will kill all containers and delete your genesis block.



## Testing Chaincode with Client

TBD





## Tips







#### goinside script
There is an issue with bash resizing and docker. A simple fix is to add this to your profile
put in ```~/.bashrc```:

```
goinside(){
    docker exec -it $1 bash -c "stty cols $COLUMNS rows $LINES && bash";
}
export -f goinside
```

now you are able to get inside a docker container without terminal size issues with:

```
$ goinside containername
```

remember to ``source ~/.profile`` before using the goinside function.



