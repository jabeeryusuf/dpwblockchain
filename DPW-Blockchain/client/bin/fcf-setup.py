#!/usr/bin/env python3

import argparse
import os

#version of images
docker_images = [
    ['hyperledger/fabric-ca','1.2.0'],

    ['hyperledger/fabric-peer','1.2.0'],
    ['hyperledger/fabric-orderer','1.2.0'],
    ['hyperledger/fabric-ccenv','1.2.0'],
    ['hyperledger/fabric-tools','1.2.0'],

    ['hyperledger/fabric-couchdb','0.4.10'],
    #['hyperledger/fabric-kafka','x86_64-0.4.7'],
    #['hyperledger/fabric-zookeeper','x86_64-0.4.7'],
]      



# create the top-level parser
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()


###################################
# COMMAND: nuke
###################################

def nuke(args):
    os.system('docker kill `docker ps -q`');
    os.system('docker rmi -f `docker images -q`');

# create the parser for the "foo" command
parser_nuke = subparsers.add_parser('nuke')
#parser_nuke.add_argument('-x', type=int, default=1)
#parser_nuke.add_argument('y', type=string)
parser_nuke.set_defaults(func=nuke)


#####################################
# COMMAND: kill
####################################

def kill(args):
    os.system('docker kill `docker ps -q`');
    os.system('docker rm `docker ps -a -q`');

# parser for the "pull" command
parser_kill = subparsers.add_parser('kill')
parser_kill.set_defaults(func=kill)
    
###################################
# COMMAND: pull
###################################

def pull_specific(image, version):
    image_version = '{}:{}'.format(image,version)
    print("\n\nPULLING: {}".format(image_version));
    os.system('docker pull {}'.format(image_version))
    print("TAGGING: {}".format(image_version)); 
    os.system('docker tag {} {}'.format(image_version, image))
    
def pull(args):
    for i in docker_images:
        pull_specific(i[0],i[1])


# parser for the "pull" command
parser_pull = subparsers.add_parser('pull')
parser_pull.set_defaults(func=pull)


# invoke sub command
args = parser.parse_args()
args.func(args)


