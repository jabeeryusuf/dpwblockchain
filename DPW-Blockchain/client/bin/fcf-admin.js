'use strict'

const cli = require('commander');

// TODO: fix
const logger = require('../../utils/logger');


var helper = require('../lib/helper.js');
var cchannel = require('../lib/create-channel')
var join = require('../libjoin-channel')
var install = require('../libinstall-chaincode')
var instantiate = require('../libinstantiate-chaincode')

logger.level = 'debug';


//require('../../config');

// TODO: Config stuff is hacky
const configFile = 'network-config.yaml';
const configAdmins = [{username:"admin", secret:"adminpw"}];

// indicate to the application where the setup file is located so it able
// to have the hfc load it to initalize the fabric client instance
hfc.setConfigSetting('network-connection-profile-path',path.join(__dirname, '../dev-network' ,configFile));
hfc.setConfigSetting('Org1-connection-profile-path',path.join(__dirname, '../dev-network', 'org1.yaml'));
hfc.setConfigSetting('admins', configAdmins);
// end hacky




// helper function

function execCommand(cmd, func) {

  (async () => {
    try {
      // display executing command
      logger.info("COMMAND: " + cmd._name);


      // Extract username and pass
      let user = cmd.parent.user;
      let org = cmd.parent.org;

      if (!user) {
        res.json(getErrorMessage('\'user\''));
        return;
      }
      if (!org) {
        res.json(getErrorMessage('\'org\''));
        return;
      }

      logger.debug('User: ' + user);
      logger.debug('Org: ' + org);

      var text = await func(cmd, user, org);
      logger.debug('returned from func exec')
      logger.debug(text);
    } catch (e) {
      logger.info('error');

      logger.error(e);

      // Deal with the fact the chain failed
    }

    logger.info("out of try block");
    return;
  })();
}




cli
.version('0.0.0')
.option('-n, --netowrk [dir]', 'directory of network details')
.option('-u, --user <use>]')
.option('-o, --org <org>');



//
// COMMAND: register-user
//

cli
.command('register-user')
.description('register [user] with the CA against the [org]')
.option('-a --admin [pass]', 'admin login and password to register user')
.action( (cmd) => { execCommand(cmd, registerUser) } );

async function registerUser(cmd, user, org) {
  let response = await helper.getRegisteredUser(user, org, true);
  return response;
}


//
// COMMAND: create-channel
//

cli
.command('create-channel')
.option('-c, --channel <channel>', "channel to create")
.action(  (cmd) => { execCommand(cmd, createChannel) } );

async function createChannel(cmd,user,org) {

  //TODO: HACK
  var channelName = 'mychannel';
  var channelConfigPath = "../../artifacts/channel/mychannel.tx"

  logger.debug('Channel name : ' + channelName);
  logger.debug('channelConfigPath : ' + channelConfigPath); //../artifacts/channel/mychannel.tx

  if (!channelName) {
    res.json(getErrorMessage('\'channelName\''));
    return;
  }
  if (!channelConfigPath) {
    res.json(getErrorMessage('\'channelConfigPath\''));
    return;
  }

  let response = await cchannel.createChannel(channelName, channelConfigPath, user, org);
  return response;
}



//
// COMMAND: join-channel
//

cli
.command('join-channel')
.option('-c, --channel <channel>', "channel to create")
.action(  (cmd) => { execCommand(cmd, joinChannel) } );

async function joinChannel(cmd,user,org) {
  //TOOD: HACK
  var channelName = 'mychannel';
  var peers = ["peer0.org1.example.com","peer1.org1.example.com"];

  logger.debug('channelName : ' + channelName);
  logger.debug('peers : ' + peers);

  if (!channelName) {
    res.json(getErrorMessage('\'channelName\''));
    return;
  }
  if (!peers || peers.length == 0) {
    res.json(getErrorMessage('\'peers\''));
    return;
  }

  let message =  await join.joinChannel(channelName, peers, user, org);
  logger.debug('returned from joinChannel');
  return message;
}


//
// COMMAND: install-chaincode
//

cli
.command('install-chaincode')
.action(  (cmd) => { execCommand(cmd, installChaincode) } );

async function installChaincode(cmd,user,org) {

  logger.debug('==================== INSTALL CHAINCODE ==================');
  var peers = ["peer0.org1.example.com","peer1.org1.example.com"];
  var chaincodeName = "productcc";
//  var chaincodePath = "artifacts/src/github.com/example_cc/node";
  var chaincodePath = "fc-fabric/lib/chaincode";
  var chaincodeVersion = "v0";
  var chaincodeType = "node";

  logger.debug('peers : ' + peers); // target peers list
  logger.debug('chaincodeName : ' + chaincodeName);
  logger.debug('chaincodePath  : ' + chaincodePath);
  logger.debug('chaincodeVersion  : ' + chaincodeVersion);
  logger.debug('chaincodeType  : ' + chaincodeType);
  if (!peers || peers.length == 0) {
    res.json(getErrorMessage('\'peers\''));
    return;
  }
  if (!chaincodeName) {
    res.json(getErrorMessage('\'chaincodeName\''));
    return;
  }
  if (!chaincodePath) {
    res.json(getErrorMessage('\'chaincodePath\''));
    return;
  }
  if (!chaincodeVersion) {
    res.json(getErrorMessage('\'chaincodeVersion\''));
    return;
  }
  if (!chaincodeType) {
    res.json(getErrorMessage('\'chaincodeType\''));
    return;
  }
  let message = await install.installChaincode(peers, chaincodeName, chaincodePath,
                                               chaincodeVersion, chaincodeType, user, org)
  return message;

}

//
// COMMAND: instantiate-chaincode
//

cli
.command('instantiate-chaincode')
.action(  (cmd) => { execCommand(cmd, instantiateChaincode) } );

async function instantiateChaincode(cmd,user,org) {
  var peers = ["peer0.org1.example.com","peer1.org1.example.com"];
  var chaincodeName = "productcc";
  var chaincodeVersion = "v0";
  var chaincodeType = "node";
  var channelName = 'mychannel';
  var fcn;
  var args = ["a","100","b","200"];


  logger.debug('peers  : ' + peers);
  logger.debug('channelName  : ' + channelName);
  logger.debug('chaincodeName : ' + chaincodeName);
  logger.debug('chaincodeVersion  : ' + chaincodeVersion);
  logger.debug('chaincodeType  : ' + chaincodeType);
  logger.debug('fcn  : ' + fcn);
  logger.debug('args  : ' + args);
  if (!chaincodeName) {
    res.json(getErrorMessage('\'chaincodeName\''));
    return;
  }
  if (!chaincodeVersion) {
    res.json(getErrorMessage('\'chaincodeVersion\''));
    return;
  }
  if (!channelName) {
    res.json(getErrorMessage('\'channelName\''));
    return;
  }
  if (!chaincodeType) {
    res.json(getErrorMessage('\'chaincodeType\''));
    return;
  }
  if (!args) {
    res.json(getErrorMessage('\'args\''));
    return;
  }



  let message = await instantiate.instantiateChaincode(peers, channelName, chaincodeName, chaincodeVersion,
    chaincodeType, fcn, args,user,org);

  return message;
}




//
// Main arg processor
//

console.log(process.argv);
cli.parse(process.argv);
