DROP TABLE IF EXISTS products;
CREATE TABLE products (
  sku                   VARCHAR(255) NOT NULL,
  apn                   VARCHAR(255) NOT NULL,
  name                  TEXT,
  department            TEXT,
  image                 TEXT,
  outer_carton_height   DECIMAL,
  outer_carton_width    DECIMAL,
  outer_carton_length   DECIMAL,
  pack_size             INT,
  carton_cbm            DECIMAL,

  CONSTRAINT pk_example PRIMARY KEY (sku)
);


DROP TABLE IF EXISTS containers;
CREATE TABLE containers (
  container_id           VARCHAR(255) NOT NULL, -- defined as: type&ref
  type                   ENUM (
                                'warehouse',
                                'factory',
                                'port',
                                'area',
                                'transit',
                                'truck',
                                'ship',
                                'train',
                                'shipping-container'),
  ref                     TEXT NOT NULL,
  parent_container_id     TEXT NULL,

  CONSTRAINT    pk_example PRIMARY KEY (container_id)
);

DROP TABLE IF EXISTS buildings;
CREATE TABLE buildings (
  container_id    VARCHAR(255) NOT NULL,
  address         VARCHAR(255) NOT NULL,
  name            VARCHAR(255),
  lat             DOUBLE,
  lng             DOUBLE,

  CONSTRAINT pk_example PRIMARY KEY (container_id)
);


DROP TABLE IF EXISTS purchase_orders;
CREATE TABLE purchase_orders (
  purchase_order_id       VARCHAR(255) NOT NULL
);


DROP TABLE IF EXISTS purchase_order_items;
CREATE TABLE purchase_order_items (
  purchase_order_item_id  VARCHAR(255) NOT NULL,
  purchase_order_id       VARCHAR(255) NOT NULL,
  sku                     VARCHAR(255) NOT NULL,
  quantity                INT NOT NULL,
  recv_quantity           INT NOT NULL, 
  approx_recv_date        TIMESTAMP,    
  origin_container        VARCHAR(255) NOT NULL,
  dst_container           VARCHAR(255) NOT NULL,
  port_of_loading         VARCHAR(255),
  mcc_warehouse           VARCHAR(255), 
  org_country             VARCHAR(255),
  update_time             TIMESTAMP DEFAULT NOW(),
  create_time             TIMESTAMP DEFAULT NOW(),
  
  CONSTRAINT pk_example  PRIMARY KEY (purchase_order_item_id,purchase_order_id,sku)
);


DROP TABLE IF EXISTS inventory_items;
CREATE TABLE inventory_items (
  container_id              VARCHAR(255) NOT NULL,
  sku                       VARCHAR(255) NOT NULL,
  purchase_order_id         VARCHAR(255) NOT NULL,
  purchase_order_items_id   VARCHAR(255) NOT NULL,
  shipment_id               VARCHAR(255) NOT NULL,
  qty_per_carton            INT,
  quantity                  INT,

  CONSTRAINT pk PRIMARY KEY (container_id,sku,purchase_order_id,purchase_order_items_id)
);



DROP TABLE IF EXISTS shipments;
CREATE TABLE shipments (
  shipment_id               VARCHAR(255) NOT NULL,
  source_container_id       VARCHAR(255) NOT NULL,
  destination_container_id  VARCHAR(255) NOT NULL,
  vessel_container_id       VARCHAR(255) NOT NULL,
  eta                       DATETIME,
  etd                       DATETIME,

  CONSTRAINT pk  PRIMARY KEY (shipment_id)
);


DROP TABLE IF EXISTS shipment_items;
CREATE TABLE shipment_items (
  shipment_id               VARCHAR(255) NOT NULL,  -- to #
  purchase_order_id         VARCHAR(255) NOT NULL,  -- PO #
  purchase_order_item_id    VARCHAR(255) NOT NULL,  -- PO Line
  delivery_quantity          INT NOT NULL,           -- ao_qty Num goes here
  delivery_time              DATETIME NOT NULL,      -- ATA on Sinatrons, and Timestamp of arriving on POD from API

  CONSTRAINT pk PRIMARY KEY (shipment_id, purchase_order_id, purchase_order_item_id)
);


DROP TABLE IF EXISTS vehicle_locations;
CREATE TABLE vehicle_locations (
  container_id      VARCHAR(255) NOT NULL, --ship&mmsi#
  last_port         VARCHAR(255) NULL,
  next_port         VARCHAR(255) NULL,
  lat               DOUBLE NOT NULL,
  lng               DOUBLE NOT NULL,
  signal_age        VARCHAR(255) NOT NULL,
  atd               VARCHAR(255) NULL, -- YESY
  eta               VARCHAR(255) NULL,
  speed             VARCHAR(255) NULL,
  course            VARCHAR(255) NULL,
  timestamp         DATETIME NOT NULL,
  CONSTRAINT pk PRIMARY KEY (container_id, timestamp)
);

DROP TABLE IF EXISTS actions; 
CREATE TABLE actions (
  action_id         SERIAL PRIMARY KEY,
  container_id      VARCHAR(255) NOT NULL,
  type              VARCHAR(255) NOT NULL,
  action_status     VARCHAR(255) DEFAULT 'pending',
  request_date      TIMESTAMP NOT NULL  DEFAULT now(),
  approval_date     TIMESTAMP,
  created_at        TIMESTAMP NOT NULL DEFAULT now(),
  unique (container_id, type)
);


CREATE TABLE vehicle_destinations (
  vessel_container_id       VARCHAR(255) NOT NULL,
  vessel_name               VARCHAR(255),
  pol                       VARCHAR(255),
  pod                       VARCHAR(255),
  eta                       timestamp(6),
  update_time               timestamp(6) without time zone default now() not NULL,
  create_time               timestamp(6) without time zone default now() not NULL,
  blockchain_id             VARCHAR(255)
);

vessel_container_id | character varying(255)         |           | not null | 
 vessel_name         | character varying(255)         |           |          | 
 pol                 | character varying(255)         |           |          | 
 pod                 | character varying(255)         |           |          | 
 eta                 | date                           |           | not null | 
 update_time         | timestamp(6) without time zone |           | not null | 
 create_time         | timestamp(6) without time zone |           | not null | 
 blockchain_id       | character varying(255)         |           | 