select     distinct id, type, ref, parent_id
from       (
            select      c6.parent_id as id6,
                        c5.parent_id as id5,
                        c4.parent_id as id4,
                        c3.parent_id as id3,
                        c2.parent_id as id2,
                        c1.parent_id as id1,
                        c1.id        as id0
            from        inventory_items ii
            inner join  containers c1 on c1.id = ii.container_id
            left join   containers c2 on c2.id = c1.parent_id 
            left join   containers c3 on c3.id = c2.parent_id
            left join   containers c4 on c4.id = c3.parent_id
            left join   containers c5 on c5.id = c4.parent_id
            left join   containers c6 on c6.id = c5.parent_id
            where       purchase_order_id = 'KG-01477'    -- Where clause for inventory items
           ) as h
inner join containers c on c.id in (id0, id1, id2, id3, id4, id5, id6)
order by   ref;
