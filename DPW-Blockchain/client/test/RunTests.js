'use strict';

const logger = require('debug-logger')('fc-fabric:RunTest');
const FCFabricClient = require('../lib/NodeTransactionClient');
const {Container} = require('fc-blockchain-common');
const query = require('../lib/query');
const helper = require('../lib/helper');


function runTests() {

  (async () => {
    await simple();
  })();
}



async function simple() {

  let user = 'tester';
  let org = 'Org1';
  
  
  try {
  
    // addTwoLocations and move container between them.
    let fcf = new FCFabricClient(user,org);
    await fcf.connect();
    let ts = 0;

    // add port with an area and a bin
    let shanghai = await fcf.addContainer(new Date(ts++),Container.Type.PORT,'SHA1');
    logger.debug('create port: %j',shanghai);

    // create storage area in shanghai
    let storage = await fcf.addContainer(new Date(ts++),Container.Type.AREA,'STORAGE',shanghai);
    logger.debug('create storage: %j',storage);

    let bin = await fcf.addContainer(new Date(ts++),Container.Type.AREA,'1-2-3',storage);
    logger.debug('create bin: %j',bin);


    /*
    // Put inventory in the bin
    let i = [
      {sku: 'abcd',quantity : 5},
      {sku: 'abce',quantity : 10, carton : 10},
      {sku: 'abcf',quantity : 5, carton : 20 }
    ];

    let inventory = new Inventory(i);
    await fcf.updateContainerInventory(ts++,bin,inventory)

    

    // Add a shipping container to the area
    let teu1 = Container.shippingContainer("SC123");
    await fcf.updateContainerHolder(ts++,teu1,storage)

    // Move the inventoroy into the shipping container
    await fcf.updateContainerInventory(ts++,teu1,inventory);
 */    




  } catch(e) {
    logger.error('TEST FAILED: ' + e.toString());
  }


}



async function test1() {
  let user = 'tester2';
  let org = 'Org1';

  // register the user
  let response = await helper.getRegisteredUser(user, org, true);
  logger.debug("registered user: " + response);

  let channelName = 'mychannel';
  let chaincodeName = 'freightcc';
  let fcn = 'testQuery';
  let args = ['what is your favorite nubmer?'];
  let peer = 'peer0.org1.example.com';

  logger.debug('channelName : ' + channelName);
  logger.debug('chaincodeName : ' + chaincodeName);
  logger.debug('fcn : ' + fcn);
  logger.debug('args : ' + args);

  if (!chaincodeName) {
    res.json(getErrorMessage('\'chaincodeName\''));
    return;
  }
  if (!channelName) {
    res.json(getErrorMessage('\'channelName\''));
    return;
  }
  if (!fcn) {
    res.json(getErrorMessage('\'fcn\''));
    return;
  }
  if (!args) {
    res.json(getErrorMessage('\'args\''));
    return;
  }

  //args = JSON.parse(args);
  logger.debug(args);




  let message = await query.queryChaincode(peer, channelName, chaincodeName, args, fcn, user, org);
  //res.send(message);
}










runTests();