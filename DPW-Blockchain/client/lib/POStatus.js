const db = require('./ConnectionPool');
const mysql = require('mysql');

function generateRow(purchase_order_id,quantity,container_id,ref,lat,lng){
    return {purchase_order_id,quantity,container_id,ref,lat,lng};
}
(async () => {
    let locations = [];
    let locationsOfInventory = [];
    let inventory = await db.query('SELECT sum(inventory_items.quantity),inventory_items.*, containers.* FROM inventory_items LEFT JOIN containers on inventory_items.container_id= containers.id WHERE inventory_items.purchase_order_id = "KG-00693" group by containers.id;')
    let purchaseOrderItems = await db.query("SELECT *,sum(quantity) FROM purchase_order_items where purchase_order_id = 'KG-00693';");
    let shipmentItems  = await db.query("SELECT sum(shipments_items.delv_quantity),shipments_items.*,shipments.* FROM shipments_items JOIN shipments on shipments_items.shipments_id = shipments.id JOIN containers on shipments.destination_container_id = containers.id where shipments_items.delv_time is not null and shipments_items.purchase_order_id = 'KG-00693' group by containers.id;")

    let factoryQuantity = parseInt(purchaseOrderItems[0]['sum(quantity)']);
    let arrivedQty = 0;
    let inventoryQty = 0;

    for (let i = 0; i < shipmentItems.length; i++) {
        locations.push(generateRow('KG-00693',shipmentItems[i]['sum(inventory_items.quantity)'],shipmentItems[i].container_id,shipmentItems[i].ref,shipmentItems[i].lat,shipmentItems[i].lng));
        arrivedQty += parseInt(shipmentItems[i]['sum(shipments_items.delv_quantity)']);
    }
    for (let i = 0; i < inventory.length; i++) {
        locations.push(generateRow('KG-00693',inventory[i]['sum(inventory_items.quantity)'],inventory[i].container_id,inventory[i].ref,inventory[i].lat,inventory[i].lng));
        inventoryQty += parseInt(inventory[i]['sum(inventory_items.quantity)']);
    }
    factoryQuantity = factoryQuantity - arrivedQty - inventoryQty;
    if(factoryQuantity>0){
        //temporary factory adder
        generateRow('KG-00693',factoryQuantity,'123','factory','30.1','122.17');
    }
    console.log(locations);

})();