'use strict';
var logger = require('debug-logger')('fc-fabric:helper');
var fabricLogger = require('debug-logger')('fabric-logger');

var path = require('path');
var util = require('util');
//var copService = require('fabric-ca-client');

var hfc = require('fabric-client');
hfc.setLogger(fabricLogger);
//var ORGS = hfc.getConfigSetting('network-config');

//var clients = {};
//var channels = {};
//var caClients = {};

//var sleep = async function (sleep_time_ms) {
//	return new Promise(resolve => setTimeout(resolve, sleep_time_ms));
//}

async function getClientForOrg (userorg, username) {
  logger.debug('getClientForOrg(org=%s,user=%s)', userorg, username)
  
  // get a fabric client loaded with a connection profile for this org
  let profileDir = hfc.getConfigSetting('profile-config-path');
  logger.debug('HL profile config directory = %s',profileDir);
  

  // build a client context and load it with a connection profile
  // lets only load the network settings and save the client for later
  let networkConfigPath = path.join(profileDir,'network-config.yaml');  
  logger.debug('HL network config path = %s',networkConfigPath);
  let client = await hfc.loadFromConfig(networkConfigPath);

  // This will load a connection profile over the top of the current one one
  // since the first one did not have a client section and the following one does
  // nothing will actually be replaced.
  // This will also set an admin identity because the organization defined in the
  // client section has one defined
  let orgConfigPath = path.join(profileDir,userorg + '.yaml');
  logger.debug('HL org config path = %s',orgConfigPath);
  await client.loadFromConfig(orgConfigPath);


  // this will create both the state store and the crypto store based
  // on the settings in the client section of the connection profile
  await client.initCredentialStores();

  // The getUserContext call tries to get the user from persistence.
  // If the user has been saved to persistence then that means the user has
  // been registered and enrolled. If the user is found in persistence
  // the call will then assign the user to the client object.
  if(username) {
    let user = await client.getUserContext(username, true);
    if(!user) {
      throw new Error(util.format('User was not found :', username));
    } else {
      logger.debug('User %s was found to be registered and enrolled', username);
    }
  }
  logger.debug('getClientForOrg - ****** END %s %s \n\n', userorg, username)

  return client;
}

var getRegisteredUser = async function(username, userOrg, isJson) {
  try {

    var client = await getClientForOrg(userOrg);
    logger.debug('Successfully initialized the credential stores');
    // client can now act as an agent for organization Org1
    // first check to see if the user is already enrolled

    logger.debug('getting user context');
    var user = await client.getUserContext(username, true);
    if (user && user.isEnrolled()) {
      logger.info('Successfully loaded member from persistence: user.isEnrolled()=%s',user.isEnrolled() );
    } else {
      // user was not enrolled, so we will need an admin user object to register
      logger.info('User %s was not enrolled, so we will need an admin user object to register',username);
      var admins = hfc.getConfigSetting('admins');
      let adminUserObj = await client.setUserContext({username: admins[0].username, password: admins[0].secret});
      let caClient = client.getCertificateAuthority();
      let secret = await caClient.register({
        enrollmentID: username,
        affiliation: userOrg.toLowerCase() + '.department1'
      }, adminUserObj);
      logger.debug('Successfully got the secret for user %s',username);
      user = await client.setUserContext({username:username, password:secret});
      logger.debug('Successfully enrolled username %s  and setUserContext on the client object', username);

    }
    if(user && user.isEnrolled()) {
      if (isJson && isJson === true) {
        var response = {
          success: true,
          secret: user._enrollmentSecret,
          message: username + ' enrolled Successfully',
        };
        return response;
      }
    } else {
      throw new Error('User was not enrolled ');
    }
  } catch(error) {
    logger.error('Failed to get registered user: %s with error: %s', username, error.toString());
    return 'failed '+error.toString();
  }

};


var setupChaincodeDeploy = function() {
  process.env.GOPATH = path.join(__dirname, hfc.getConfigSetting('CC_SRC_PATH'));
};

var getLogger = function(moduleName) {
  return logger;
};

exports.getClientForOrg = getClientForOrg;
exports.getLogger = getLogger;
exports.setupChaincodeDeploy = setupChaincodeDeploy;
exports.getRegisteredUser = getRegisteredUser;
