module.exports.processExcel = async function (client, xlData, type) {

  let containerInfo = {};
  for (let i = 0; i < xlData.length; i++) {
    containerInfo.container_id = 'shipping-container&' + xlData[i]['CONTAINER NUMBER'];
    let vessel_name = xlData[i]["VESSEL NAME"]
    let vehicles = await client.queryVehicleMap(null, null, null, ["levenshtein(LOWER(name), LOWER('" + vessel_name + "')) limit 1"]);
    //need to switch to imo but the data doesn't currently reflect that
    containerInfo.parent_container_id = 'ship&' + vehicles.items[0].mmsi;

    let container = await client.getContainer('ship&' + vehicles.items[0].mmsi);
    if (!container || !container.container_id) {
      await client.addContainer({ container_id: 'ship&' + vehicles.items[0].mmsi, type: 'ship', ref: vehicles.items[0].mmsi });
    }
    containerInfo.ref = xlData[i]['CONTAINER NUMBER'];
    containerInfo.parent_container_id = 'ship&' + vehicles.items[0].mmsi;
    //create container if container does not exsist 
    let shippingcontainer = await client.getContainer('shipping-container&' + xlData[i]['CONTAINER NUMBER']);
    if (!shippingcontainer || !shippingcontainer.container_id) {
      await client.db.query('INSERT INTO containers (container_id, parent_container_id,type,ref) VALUES ($1,$2,$3,$4)', [containerInfo.container_id, containerInfo.parent_container_id, 'shipping-container', containerInfo.ref])
    }
    let jsDate = xlData[i]['EXPECTED TO ARRIVE DATE'].split('-');
    jsDate = new Date(parseInt(jsDate[2]), parseInt(jsDate[0]) - 1, parseInt(jsDate[1]));

    //create a new vehicle destination if vehicle desintaion does not exsist
    let exsistingDestination = await client.db.query('SELECT * FROM vehicle_destinations where vessel_container_id = $1', [containerInfo.parent_container_id]);
    if (!exsistingDestination.rows.length) {
      await client.db.query(
        `INSERT INTO vehicle_destinations(vessel_container_id, vessel_name,pol,pod,eta) 
      VALUES ($1,$2,$3,$4,$5)`,
        [
          containerInfo.parent_container_id,
          vehicles.items[0].name,
          xlData[i]['PORT CODE OF LOADING'],
          'port&' + xlData[i]['PORT CODE OF DESTINATION'],
          jsDate
        ]
      )
    }
    //get the orgnaization based on the consignee name 
    let org_id = await client.db.query('SELECT org_id  FROM consignee_map where consignee_name = $1', [xlData[i]['CONSIGNEE NAME']])
    org_id = org_id.rows[0].org_id
    //create inventory_item if one does not exsist 
    let inventory = await client.queryInventory('org_id  = $1 and container_id = $2', [org_id, containerInfo.container_id]);


    if (!inventory.items.length) {
      let inventoryItemInfo = {
        org_id: 'demo',
        container_id: containerInfo.container_id,
        sku: '-',
        purchase_order_item_id: '-',
        purchase_order_id: '-'
      }
      await client.addInventoryItem(inventoryItemInfo);
    }
  }
  return true

}