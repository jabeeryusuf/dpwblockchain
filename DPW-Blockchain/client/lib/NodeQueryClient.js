
'use strict';

var debug = require('debug')('blockchain:QueryClient');

var moment = require('moment-timezone');

const { Item, Container, Inventory, FcClient } = require("../../common")
const { processExcel } = require('./ProcessExcel.js');
//const sql = require('mysql');
//const db = require('./ConnectionPool');
const logger = require('debug-logger')('fc-sql:FcSqlClient');
const postgres = require('pg');
const calculateVolume = require('./calculateVolume.js');
const util = require('util');
const XLSX = require('xlsx');
const fs = require('fs');
const MessageDb = require('./MessageDb');
const uuidv4 = require('uuid/v4');



class FcSqlClient extends FcClient {

  constructor() {
    super();
  }
  async connect(test) {
    let connectionStr;

    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === 'development') {
      //connectionStr = "postgres://abdulrahman:abdulrahman_dev@dev-db.c4o01umehjnu.us-east-2.rds.amazonaws.com:5432/rashed";
	  
	  connectionStr = "postgres://abdulrahman:abdulrahman_dev@newdb.cfmnkf5hdw75.eu-west-1.rds.amazonaws.com:5432/rashed";
    } else {
      connectionStr = "postgres://abdulrahman:abdulrahman123@abdulrahman.ce5c63ntc6xz.us-east-2.rds.amazonaws.com:5432/rashed?ssl=true";
    }
    if (test) {
      connectionStr = "postgres://rashed:password@localhost:5432/test_feeds"
    }

    console.log('connecting to ' + process.env.NODE_ENV + ' database: %s', connectionStr);
    //this.db = new postgres.Client(connectionStr);
    this.db = new postgres.Client(
      {
        connectionString: connectionStr,
        ssl: true
      });


    try {
      await this.db.connect();

      this.db.on('error', (err) => {
        console.error('something bad has happened!', err.stack)
      })

      console.log("CONNECTED");

    } catch (err) {
      console.log(err);
    }


  }

  disconnect() {
    this.db.end();
  }

  getMessageDb() {
    if (!this.messageDb) {
      this.messageDb = new MessageDb(this.db);
    }

    return this.messageDb;
  }




  /*async addContainer(timestamp, container, holder) {
    debug("addContainer: %j, %j", container, holder);

    let entry = Object.assign({ parent_id: holder.getId() }, container);
    let res = await this.db.query('INSERT INTO containers SET $1', entry);
    debug('res = %j', res);
  }*/

  async updateContainerHolder(timestamp, container, holder) {
    debug("updateContainerHolder: %j, %j", container, holder);
    //let entry = Object.assign({parent_id:holder.getId()},container);
    let res = await this.db.query('UPDATE containers SET parent_id = ? WHERE id = ?', [holder.getId(), container.getId()]);
    debug('res = %j', res);
  }

  async addContainer(containerInfo) {
    let { container_id, type, ref } = containerInfo;

    await this.db.query('INSERT INTO containers (container_id,type,ref) VALUES ($1,$2,$3)', [container_id, type, ref]);
  }
  async updateContainer(updateObj, whereClause, whereParams) {
    let iiSql = "UPDATE containers SET ";
    let sqlEnd = "";
    let first = true;
    for (let key in updateObj) {
      if (first === false) {
        iiSql += ',';
      }
      else {
        first = true;
      }
      iiSql += " " + String(key) + " = '" + String(updateObj[key]) + "' ";
    }
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    iiSql += sqlEnd;
    try {
      this.db.query(iiSql, whereParams);
    }
    catch (err) {
      console.log('update', err)
    }
  }

  async queryContainer(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = '';
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }

    let iiSql = 'SELECT * from containers' + sqlEnd;

    let iiRes = await this.db.query(iiSql, whereParams);
    let rows = iiRes.rows
    let res = { items: rows };
    return res;
  }
  async updateContainerInventory(timestamp, container, inventory) {
    debug("updateContainerInventory: %j, %j", container, inventory);
    let items = inventory.items;
    let cid = container.getId();

    for (let i in items) {
      debug("item[%i]: %j", i, items[i])
      // todo, can do in parallel
      let id = cid + ':inv&' + items[i].getId();
      let entry = Object.assign({ container_id: cid }, items[i]);
      entry.container_id = cid;
      let res = await this.db.query('INSERT INTO inventory SET ?', entry)
    }

  }

  async addProduct(product) {
    let res = await this.db.query('INSERT INTO products SET ?', product)
  }

  // Adding Product Catalog
  async addProductCatalog(e, org){
    const sku = e["payload"]["sku"];
    const apn = e["payload"]["apn"];
    const name = e["payload"]["name"];
    const department = e["payload"]["department"];
    const image = e["payload"]["image"]
    const outer_carton_height = e["payload"]["outer_carton_height"]; 
    const outer_carton_width = e["payload"]["outer_carton_width"]; 
    const outer_carton_length = e["payload"]["outer_carton_length"];
    const pack_size = e["payload"]["pack_size"];
    const carton_cbm =  e["payload"]["carton_cbm"];
    console.log(">>>>", sku,apn,name,department,image, outer_carton_height,outer_carton_width,outer_carton_length,pack_size,carton_cbm,org)
    try{
        let productsql = `INSERT into products(sku, apn, name, department, image, outer_carton_height, outer_carton_width,outer_carton_length,pack_size,carton_cbm, org_id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`
        let res = await this.db.query(productsql, [sku,apn,name,department,image,outer_carton_height,outer_carton_width,outer_carton_length,pack_size,carton_cbm,org]);
        return res;
    }catch(err){
        console.log("errorrrrr>>>>>", err)
        throw " sku already exist ";
    } 
  } 

  async getAllProducts() {
    let res = await this.db.query('SELECT * FROM products');
    return res;
  }

  async addPurchaseOrder(purchase_order_id, create_date) {
    let newPO = await this.db.query("INSERT INTO purchase_orders (purchase_order_id,create_date) VALUES ($1,$2)", [purchase_order_id, create_date]);
    return newPO;
  }
  async addInventoryItem(inventoryItem) {
    let { org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity, receive_date } = inventoryItem;
    let insertArr = [org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity, receive_date]
    try {
      let newItem = await this.db.query('INSERT INTO inventory_items (org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity,receive_date) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)', insertArr);
      return newItem;
    }
    catch (err) {
      console.log("me", err)
    }

  }
  async addPurchaseOrderItem(poItem) {
    let { purchase_order_item_id, purchase_order_id, sku, quantity, origin_container, dst_container, port_of_loading, org_country } = poItem;
    let insertArr = [purchase_order_item_id, purchase_order_id, sku, quantity, origin_container, dst_container, port_of_loading, org_country]
    let newItem = await this.db.query('INSERT INTO purchase_order_items (purchase_order_item_id, purchase_order_id,sku, quantity,origin_container,dst_container,port_of_loading,org_country) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)', insertArr);
    return newItem;
  }
  async updateInventoryItem(updateObj, whereClause, whereParams) {
    let iiSql = "UPDATE inventory_items SET ";
    let sqlEnd = "";
    let first = true;
    for (let key in updateObj) {
      if (first === false) {
        iiSql += ',';
      }
      else {
        first = false;
      }
      iiSql += " " + String(key) + " = '" + String(updateObj[key]) + "' ";
    }
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    iiSql += sqlEnd;
    try {
      this.db.query(iiSql, whereParams);
    }
    catch (err) {
      console.log('update', err)
    }
  }
  async createInventoryItem(inventoryItem) {
    return await this.db.query('INSERT INTO inventory_items  SET ?', inventoryItem);

  }

  async updatePurchaseOrderItem(updateObj, whereClause, whereParams) {
    let iiSql = "UPDATE purchase_order_items SET ";
    let sqlEnd = "";
    let first = true;
    for (let key in updateObj) {
      if (first === false) {
        iiSql += ',';
      }
      else {
        first = false;
      }
      iiSql += String(key) + " = '" + String(updateObj[key]) + "'";
    }
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    iiSql += sqlEnd;
    this.db.query(iiSql, whereParams);

  }

  async updatePurchaseOrder(updateObj, whereClause, whereParams) {
    let iiSql = "UPDATE purchase_orders SET ";
    let sqlEnd = "";
    let first = true;
    for (let key in updateObj) {
      if (first === false) {
        iiSql += ',';
      }
      else {
        first = true;
      }
      iiSql += String(key) + " = '" + String(updateObj[key]) + "'";
    }
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    iiSql += sqlEnd;
    return await this.db.query(iiSql, whereParams);

  }

  /// need to improve naming
  async queryPurchaseOrderItem(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = '';
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }

    let iiSql = 'SELECT * from purchase_order_items' + sqlEnd;


    let iiRes = await this.db.query(iiSql, whereParams);
    let rows = iiRes.rows
    let res = { items: rows };
    return res;

  }
  async getContainer(id) {
    let result = await this.db.query('SELECT * FROM containers WHERE container_id = $1', [id]);
    let container = Object.create(Container.prototype);
    let resContainer = Object.assign(container, result.rows[0]);
    return resContainer;
  }

  //there is no building class right now so I'm leaving this here 
  async getBuilding(id) {
    let result = await this.db.query('SELECT * FROM buildings WHERE container_id = $1', [id]);
    return result.rows;
  }
  async queryActions(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = '';
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }

    //select actions.*, containers.parent_container_id, shipments.destination_container_id, buildings.name from actions JOIN containers on actions.container_id=containers.container_id JOIN shipments on containers.parent_container_id=shipments.vessel_container_id JOIN buildings on shipments.destination_container_id=buildings.container_id WHERE shipments.destination_container_id='port&JKT';
    //let iiSql = 'SELECT * from actions' + sqlEnd;
    let iiSql = `SELECT actions.*, containers.parent_container_id, vehicle_destinations.pod, buildings.name, orgs.name as requested_by
                 FROM actions 
                 LEFT JOIN containers on actions.container_id=containers.container_id 
                 LEFT JOIN orgs on orgs.org_id = actions.requestor_id 
                 LEFT JOIN vehicle_destinations on containers.parent_container_id = vehicle_destinations.vessel_container_id
                 LEFT JOIN buildings on vehicle_destinations.pod = buildings.container_id` + sqlEnd;

    let iiRes = await this.db.query(iiSql, whereParams);
    let rows = iiRes.rows
    let containerIDs = [];
    let records = []

    for (let obj of rows) {
      if (containerIDs.indexOf(obj.container_id) < 0) {
        containerIDs.push(obj.container_id);
        records.push(obj);
      }
    }

    let res = { items: records };
    return res;

  }

  async createAction(container_id, type, requestor_id) {
    let actionCheck = await this.db.query(`SELECT * FROM actions WHERE container_id=$1 AND approval_date IS NULL`, [container_id]);
    if (actionCheck.rows.length === 0) {
      let actionId = String(container_id.slice(19)) + '&';
      try {
        let newAction = await this.db.query(`INSERT into actions (action_id, container_id, type, requestor_id, requestee_id) VALUES (CONCAT(($1::VARCHAR), now()), $2, $3, $4, $5)`, [actionId, container_id, type, requestor_id, 'dpworld.ca']);
        return newAction;
      }
      catch (err) {
        console.log(err)
      }

    } else {
      return false;
    }

  }

  async queryPendingActions(orgID) {
    let query = await this.db.query(`SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name 
                                     FROM actions a 
                                     LEFT JOIN containers c ON a.container_id = c.container_id 
                                     LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
                                     WHERE a.action_status = 'pending' AND requestor_id=$1`, [orgID]);
    return query.rows.length > 0 ? query.rows : []; 
  }

  async queryResolvedActions(orgID) {
    let query = await this.db.query(`SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name 
                                     FROM actions a 
                                     LEFT JOIN containers c ON a.container_id = c.container_id 
                                     LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
                                     WHERE a.action_status != 'pending' AND requestor_id=$1`, [orgID]);
    return query.rows.length > 0 ? query.rows : [];
  }

  // Case created >>>>>
  async createCase(dataObject) {
    console.log("inside NODE>>>>>>>>>> ", dataObject);
    let obj = JSON.parse(dataObject);
    let { case_id, subject, organization_id, userID, purchase_order_id, sku, case_priority, message, default_group_id, newemailAddress, case_type, case_subType, defaultEmailAddressChange, defaultEmailAddressChangeFlag, loggedInUserEmail, case_assign } = obj;
    let casesql = `insert into cases (case_id, subject, org_id, purchase_order_id, case_status, group_id, case_type, case_description, case_priority, case_subType, owner, case_assign_to) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)`;
    return this.db.query(casesql, [case_id, subject, organization_id, purchase_order_id, 'New', case_id, case_type, message, case_priority, case_subType, loggedInUserEmail, case_assign])
      .catch(e => {
        throw new Error("Case already exist on this Purchase Order")
      })
      .then(async succ => {
        if (defaultEmailAddressChangeFlag === false && default_group_id !== undefined) {
          for (let i = 0; i < default_group_id.length; i++) {
            let groupmembersql = `insert into group_group_members(group_id,child_group_id, child_type) values ($1, $2, $3)`;
            await this.db.query(groupmembersql, [case_id, default_group_id[i], 'group'])
              .catch(er => {
                throw new Error("Case's Group Group members already exist")
              })
          }
        }
      })
      .then(succ2 => {
        let creategroupsql = `insert into groups (org_id, group_id, group_name, type) values ($1, $2, $3, $4)`;
        return this.db.query(creategroupsql, [organization_id, case_id, subject, 'case'])
          .catch(e => {
            throw new Error("Case's Group already exist")
          });
      })
      .then(async succ4 => {
        if (defaultEmailAddressChange !== undefined) {
          if (newemailAddress !== undefined) {
            newemailAddress = newemailAddress.concat(defaultEmailAddressChange);
          } else {
            newemailAddress = defaultEmailAddressChange;
          }
        }
        if (newemailAddress !== undefined) {
          for (let i = 0; i < newemailAddress.length; i++) {
            let userUUID = uuidv4();
            let e = newemailAddress[i];
            await this.db.query(`select * from users where email = $1`, [e])
              .then(async userExist => {
                if (userExist.rows.length === 0) {
                  let usersql = `insert into users ( user_id, org_id, email, name, password) values ($1, $2, $3, $4, $5)`;
                  return this.db.query(usersql, [userUUID, organization_id, e, 'unknown', 'unknown'])
                    .then(succ5 => {
                      let groupmembersql = `insert into group_group_members(group_id,child_group_id,child_type) values ($1, $2, $3)`;
                      return this.db.query(groupmembersql, [case_id, userUUID, 'user'])
                    })
                } else {
                  for (let row = 0; row < userExist.rows.length; row++) {
                    let existUserUser_id = userExist.rows[row]["user_id"]
                    let groupmembersql = `insert into group_group_members(group_id,child_group_id, child_type) values ($1, $2, $3)`;
                    return this.db.query(groupmembersql, [case_id, existUserUser_id, 'user'])
                  }
                }
              });
          }
          return "Successful"
        } else {
          return "";
        }
      })

      .then(async succ6 => {
        let groupmembersql = `insert into group_group_members(group_id,child_group_id, child_type) values ($1, $2, $3)`;
        return this.db.query(groupmembersql, [case_id, userID, 'user'])
      })
      .then(async succ7 => {
        if (sku !== undefined) {
          let caselabelsql = `insert into case_labels(case_id, label_key, label_value) values ($1, $2, unnest($3 :: text[]))`;
          await this.db.query(caselabelsql, [case_id, 'sku', sku])
            .catch(err => {
              throw new Error("Case label already exist")
            })
        }
        return "Successful"
      })
      .then(async succ8 => {
        if (purchase_order_id.length > 0) {
          let po = purchase_order_id.split(",")
          if (po !== undefined) {
            let caselabelsql = `insert into case_labels(case_id, label_key, label_value) values ($1, $2, unnest($3 :: text[]))`;
            await this.db.query(caselabelsql, [case_id, 'purchase_order_id', po])
              .catch(err => {
                throw new Error("Case label already exist")
              })
          }
        }
        return "Successful"
      })
      .catch(err => {
        return {
          name: "error",
          message: err.message
        };
      });
  }

  async defaultGroupEmail(org_id, orgNames, factory_code) {
    try {
      console.log("Inside getting default Group Email ", org_id, orgNames, factory_code)
      let newOrgNames;
      let flag = false;
      let newAction1;
      if (orgNames.includes('factory')) {
        newOrgNames = orgNames.filter(e => e !== 'factory');
        flag = true
      } else {
        newOrgNames = orgNames;
      }
      if (flag) {
        let sql = `WITH RECURSIVE userdetails(default_contact_group, child_type) AS ( 
          select 
          default_contact_group, ref
          from 
          providers
          where org_id = $1 and ref = ANY ($2) and provider_org_id = $3

          UNION 

          select 
          g.child_group_id, g.child_type
          from  
          group_group_members g

          JOIN userdetails s on g.group_id = s.default_contact_group 

      ) select  s1.*, u.email, u.name from userdetails s1 left join users u on s1.default_contact_group = u.user_id ; `;
        newAction1 = await this.db.query(sql, [org_id, ['factory'], factory_code]);
      }

      let sql = `WITH RECURSIVE userdetails(default_contact_group, child_type) AS ( 
        select 
        default_contact_group, ref
        from 
        providers
        where org_id = $1 and ref = ANY ($2)

        UNION 

        select 
        g.child_group_id, g.child_type
        from 
        group_group_members g

        JOIN userdetails s on g.group_id = s.default_contact_group

    ) select  s1.*, u.email, u.name from userdetails s1 left join users u on s1.default_contact_group = u.user_id ;`;
      let newAction2 = await this.db.query(sql, [org_id, newOrgNames]);
      let newAction
      if (flag) {
        newAction = newAction1.rows.concat(newAction2.rows);
      } else {
        newAction = newAction2.rows;
      }
      return newAction;
    }
    catch (err) {
      console.log(err)
    }
  }

  async UpdateCaseStatus(case_priority, case_id, case_status, owner, case_assign_to) {
    try {
      let sql = `UPDATE cases SET case_priority = $1, case_status = $3, owner = $4, case_assign_to = $5 where case_id = $2`;
      let newAction = await this.db.query(sql, [case_priority, case_id, case_status, owner, case_assign_to]);
      console.log("newactionss", newAction)
      return newAction;
    }
    catch (err) {
      console.log(err)
    }
  }

  //>>>>>>>>.finsih
  /**
   * Updates a current request in then DB.
   * Changes the current action type to the one the user has selected
   * @param {*} id 
   * @param {*} actionType 
   */
  async updateAction(id, actionType) {
    try {
      let newUpdate = await this.db.query('UPDATE actions SET type=$2, request_date=now() WHERE action_id=$1', [id, actionType]);
      return newUpdate;
    } catch (err) {
      console.log(err);
    }
  }

  async resolveRequest(id, resolve) {
    try {
      let resolveRequest = await this.db.query('UPDATE actions SET action_status=$2, approval_date=now() WHERE action_id=$1', [id, resolve]);
      return resolveRequest;
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * Deletes a current request in the DB.
   * @param {*} id 
   */
  async deleteAction(id) {
    try {
      let deleteAction = await this.db.query('DELETE FROM actions WHERE action_id=$1', [id]);
      return deleteAction;
    } catch (err) {
      console.log(err);
    }
  }


  async cacheContainersForItems(whereClause, whereParams) {
    let cache = {};
    // MYSQL does not support recursive queries (eg CTE),  this will handle six levels of hierarchy which 
    // should be fine for now
    let cSql =
      `
    SELECT     distinct id, type, ref, lat, lng, parent_id parentId
    FROM       (
                SELECT      c6.parent_id AS id6,
                            c5.parent_id AS id5,
                            c4.parent_id AS id4,
                            c3.parent_id AS id3,
                            c2.parent_id AS id2,
                            c1.parent_id AS id1,
                            c1.id        AS id0
                FROM        inventory_items ii
                INNER JOIN  containers c1 ON c1.id = ii.container_id
                LEFT JOIN   containers c2 ON c2.id = c1.parent_id 
                LEFT JOIN   containers c3 ON c3.id = c2.parent_id
                LEFT JOIN   containers c4 ON c4.id = c3.parent_id
                LEFT JOIN   containers c5 ON c5.id = c4.parent_id
                LEFT JOIN   containers c6 ON c6.id = c5.parent_id
                WHERE 
    ` +
      whereClause
      + `
               ) AS h
    INNER JOIN containers c ON c.id in (id0, id1, id2, id3, id4, id5, id6)
    ORDER BY   ref
    `

    //debug('cSql = %s',cSql);
    let cRes = await this.db.query(cSql, whereParams);
    debug('cRes = %j', cRes);

    for (let i in cRes) {
      let c = Container.fromObject(cRes[i]);
      cache[c.getId()] = c;
    }

    return cache;
  }

  /// I don't know if this needs to be its own call
  async queryManufacturers(whereClause, whereParams, groupBy, orderBy) {
    let sqlResponse = await this.db.query('SELECT buildings.lat, buildings.lng, sum(purchase_order_items.quantity), purchase_order_items.origin_container from purchase_order_items JOIN buildings on purchase_order_items.origin_container = buildings.container_id group by purchase_order_items.origin_container, buildings.lat, buildings.lng');

    return { containers: sqlResponse.rows, cache: {} };
  }
  //Need to make this more general

  async queryPurchaseOrders(whereClause, whereParams, groupBy, orderBy) {

    let sqlEnd = ""
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }

    let iiSql = 'SELECT * FROM purchase_orders' + sqlEnd;
    let iiRes = await this.db.query(iiSql, whereParams);
    let cache;
    debug('iiRes = %j', iiRes);
    //let cache = await this.cacheContainersForItems(whereClause, whereParams);

    // debug('cache = %j', cache);
    let res = { items: iiRes.rows, cache: cache };
    return res;
  }



  async queryPurchaseOrderItems(whereClause, whereParams) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    //Select all PO Items for a group of POs
    let isql = `SELECT purchase_orders.*,purchase_order_items.*,
            containers.ref, 
            buildings.*,
            products.name, 
            products.image, 
            products.department, 
            products.image,
            products.outer_carton_height,products.outer_carton_width,
            products.outer_carton_length,products.pack_size FROM purchase_orders`
    isql += ` LEFT JOIN purchase_order_items on  
    purchase_order_items.purchase_order_id = purchase_orders.purchase_order_id 
    LEFT JOIN containers on purchase_order_items.origin_container = containers.container_id 
    LEFT JOIN buildings ON buildings.container_id = containers.container_id 
    LEFT JOIN products on purchase_order_items.sku = products.sku`;
    isql += sqlEnd;

    let purchaseOrders = await this.db.query(isql, whereParams);
    //SELECT all inventory items for a group of POs
    let isql2 = 'SELECT  inventory_items.*,containers.*,shipments.*, v1.lat as vessellat, v1.lng as vessellng, v1.vessel_name as vessel, buildings.lat, buildings.lng, buildings.name FROM inventory_items LEFT JOIN containers on inventory_items.container_id= containers.container_id LEFT JOIN buildings on buildings.container_id = inventory_items.container_id LEFT JOIN shipments on inventory_items.shipment_id = shipments.shipment_id LEFT JOIN vehicle_locations v1 on shipments.vessel_container_id = v1.vessel_container_id LEFT OUTER JOIN vehicle_locations v2 ON (shipments.vessel_container_id = v2.vessel_container_id AND (v1.timestamp < v2.timestamp)) WHERE v2.vessel_container_id IS NULL and inventory_items.quantity > 0;';
    if (whereClause) {
    }
    //isql2 += " group by inventory_items.sku,inventory_items.purchase_order_id;";
    isql2 += " ;";
    let inventoryItems = await this.db.query(isql2, whereParams);
    //Select all arrived shipments for a group of POs
    //We only want arrived shipments because all others
    //should be accounted for in inventory 
    let isql3 = "SELECT shipment_items.*,shipments.*, containers.*,buildings.* FROM shipment_items JOIN shipments on shipment_items.shipment_id = shipments.shipment_id JOIN buildings on shipments.destination_container_id = buildings.container_id JOIN containers on shipments.destination_container_id = containers.container_id ";
    isql3 += " WHERE shipment_items.delivery_time is not null and shipment_items.delivery_quantity >0"


    //isql3 += " group by shipments_items.purchase_order_items_id,shipments_items.purchase_order_id;";
    let shipmentItems = await this.db.query(isql3, whereParams);
    //doing this to avoid multiple relatively complicated joins to product table
    //this allows us to map shipments and inventory items to get volumes to product to get volumes
    //in our current data model purchase order items map to inventory by purchase_order_id and po_line(id)
    //purchase order items map to shipments via purchase order id and sku 
    let productCache = {};
    for (let i in purchaseOrders.rows) {
      let hash = String(purchaseOrders.rows[i].purchase_order_id) + "_" + String(purchaseOrders.rows[i].purchase_order_item_id + "_" + String(purchaseOrders.rows[i].sku));
      productCache[hash] = purchaseOrders.rows[i];
    }

    //The following code checks each PO to determine 
    //if any inventory is still at the manufacturer 
    // Any POs that have qty not in inventory or arrived are 
    // assumed to be at factory
    let POItemDict = {};
    let POitemlist = [];
    for (let i in inventoryItems.rows) {
      let item = inventoryItems.rows[i];
      let hash = String(item.purchase_order_id) + "_" + String(item.purchase_order_item_id) + "_" + String(item.sku);
      let wasShipped = true;
      if (item.shipment_id) {
        if (moment(item.etd).add(2, 'days').isAfter(moment(new Date))) {
          wasShipped = false
        }
      }
      else {
        wasShipped = false;
      }
      if (!item.parent_container_id || item.parent_container_id.slice(0, 4) !== 'ship') {
        wasShipped = false
      }
      wasShipped ? item.status = 'Shipped' : item.status = 'Inventory';
      item.quantity = parseInt(item['quantity']);
      if (item.status == 'Shipped') {
        item.lat = item.vessellat,
          item.lng = item.vessellng,
          item.container_id = item.vessel
      }

      if (productCache[hash]) {

        item.dst_container = productCache[hash].dst_container;
        item.productName = productCache[hash]['name'];
        item.department = productCache[hash]['department'];
        item.image = productCache[hash]['image'];
        item.volume = calculateVolume(item.quantity, productCache[hash]);
      }
      else {
        item.volume = 0;
        item.dst_container = 'Unknown';
      }
      POitemlist.push(item);
      if (POItemDict[hash]) {
        POItemDict[hash] += parseInt(item.quantity);
      }
      else {
        POItemDict[hash] = parseInt(item.quantity);
      }
    }
    for (let i in shipmentItems.rows) {
      let item = shipmentItems.rows[i];
      let hash = String(item.purchase_order_id) + "_" + String(item.purchase_order_item_id) + "_" + String(item.sku);
      item.quantity = parseInt(item['delivery_quantity']);

      item.status = 'Arrived';
      if (productCache[hash]) {
        item.productName = productCache[hash]['name'];
        item.department = productCache[hash]['department'];
        item.image = productCache[hash]['image'];
        item.sku = productCache[hash].sku;
        item.dst_container = productCache[hash].dst_container;
        item.volume = calculateVolume(item.quantity, productCache[hash]);
      }
      else {
        item.volume = 0;
        item.dst_container = 'Unknown';
      }
      POitemlist.push(item);
      if (POItemDict[hash]) {
        POItemDict[hash] += parseInt(item.quantity);
      }
      else {

        POItemDict[hash] = parseInt(item.quantity);
      }
    }

    for (let i in purchaseOrders.rows) {
      let remaining = purchaseOrders.rows[i].quantity;
      let hash = String(purchaseOrders.rows[i].purchase_order_id) + "_" + String(purchaseOrders.rows[i].purchase_order_item_id) + "_" + String(purchaseOrders.rows[i].sku);
      if (POItemDict[hash]) {
        remaining -= POItemDict[hash];
      }
      if (remaining > 0) {
        let newPOObj = { ...purchaseOrders.rows[i], productName: purchaseOrders.rows[i]['name'], department: purchaseOrders.rows[i]['department'], image: purchaseOrders.rows[i]['image'], quantity: remaining, container_id: purchaseOrders.rows[i].origin_container, volume: calculateVolume(remaining, purchaseOrders.rows[i]), type: purchaseOrders.rows[i].type, name: purchaseOrders.rows[i].name, status: 'Manufacturer', ref: purchaseOrders.rows[i].ref, lat: purchaseOrders.rows[i].lat, lng: purchaseOrders.rows[i].lng };
        POitemlist.push(newPOObj);
      }
    }
    //groups the po items by PO
    let items = [];
    for (let i in POitemlist) {
      let item = Object.create(Item.prototype);
      let resItem = Object.assign(item, POitemlist[i]);
      items.push(resItem);
    }
    return { items: items, cache: null };
  }
  async addShipmentItem(shipItemOBJ) {
    let { shipment_id, purchase_order_id, purchase_order_item_id, sku, delivery_quantity, org_id } = shipItemOBJ;
    let newShipmentItem = await this.db.query("INSERT INTO shipment_items (shipment_id,purchase_order_id, purchase_order_item_id, sku,delivery_quantity, org_id) VALUES ($1,$2,$3,$4,$5,$6)", [shipment_id, purchase_order_id, purchase_order_item_id, sku, delivery_quantity, org_id])
    return newShipmentItem;
  }
  async addShipment(shipOBJ) {
    /// etd missing from event
    let { shipment_id, eta, destination_container_id, vessel_container_id, source_container_id, org_id, vessel_name,etd, type, carrier } = shipOBJ;
    carrier = carrier === null ? "unknown" : carrier;
    let uuid = uuidv4();
    try {
      let newShipment = await this.db.query(`INSERT INTO shipments(shipment_id, etd, eta, destination_container_id, vessel_name, vessel_container_id, source_container_id, org_id, original_eta, tracking_id) 
                                            VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$3,$9)`, 
                                            [shipment_id,etd, eta, destination_container_id, vessel_name, vessel_container_id, source_container_id, org_id, uuid]);
      
      await this.db.query(`INSERT INTO shipment_trackings (tracking_id, carrier, type, tracking_ref) VALUES ($1, $2, $3, $4)`, [uuid, carrier, type, shipment_id]);
      return newShipment
   
    } catch (err) {
      console.log(err);
    }
    
  }

  async updateShipmentTracking(obj){
    let { reference, type, carrier, tracking_id, events }  = obj
    try{
      if(type === "container"){  
        if(tracking_id === undefined){
          throw new Error("for container type tracking id is required")
        }
      }else {
        let response = await this.db.query(`SELECT tracking_id FROM shipment_trackings Where tracking_ref = '${reference}'`)
        if(response.rows.length > 0){
          for(let i in response.rows){
            tracking_id = response.rows[i]["tracking_id"]
          }
        }else{
          throw new Error("No tracking_id exist in shipment_trackings")
        }
      }
      await this.db.query(`DELETE from shipment_tracking_events where tracking_id = '${tracking_id}'`);
      for(let i = 0; i < events.length; i++){
        await this.db.query(`insert into shipment_tracking_events(tracking_id, date, status, location, vessel, voyage) 
                            values('${tracking_id}','${events[i]["date"]}', '${events[i]["status"]}', 'port&${events[i]["location"]}',
                            '${events[i]["vessel"]}', '${events[i]["voyage"]}' )`);
        
        if(carrier === "cma" && events[i]["status"] === "Container to consignee"){
          await this.db.query(`UPDATE shipments SET ata = '${events[i]["date"]}' where tracking_id = '${tracking_id}'`)
        }else if(carrier === "cma" && events[i]["status"] === "Arrival final port of discharge"){
          await this.db.query(`UPDATE shipments SET eta = '${events[i]["date"]}' where tracking_id = '${tracking_id}'`)
        }
      } 
      console.log("done!!!");
      
    }catch(err){
      console.log(err);
      return err;
    }
  }
  async queryVehicleMap(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = '';
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }

    let iiSql = 'SELECT *  from vessels' + sqlEnd;
    let iiRes = await this.db.query(iiSql, whereParams);
    let rows = iiRes.rows
    let res = { items: rows };
    return res;
  }

  async queryInventory(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = ""
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }
    let iiSql
    if (!groupBy || !groupBy.length) {
      iiSql = 'SELECT *  FROM inventory_items' + sqlEnd;
    }
    else {
      iiSql = 'SELECT '
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          iiSql += ' ,'
        }
        iiSql += groupBy[i];
      }
      iiSql += ' FROM inventory_items' + sqlEnd
    }
    let iiRes = await this.db.query(iiSql, whereParams);
    let cache;
    debug('iiRes = %j', iiRes);
    //let cache = await this.cacheContainersForItems(whereClause, whereParams);

    // debug('cache = %j', cache);
    let res = { items: iiRes.rows, cache: cache };
    return res;
  }

  async getPurchaseOrderIDs(whereClause) {
    let sqlStart = "SELECT DISTINCT purchase_order_id FROM purchase_order_items";
    let sqlEnd = "";

    if (whereClause) {
      sqlEnd += " WHERE " + whereClause + "ORDER BY purchase_order_id asc";
    }
    let sql = sqlStart + sqlEnd;
    let res = this.db.query(sql);
    return res;

  }
  async queryMilestones(org_id) {
    let sql = `SELECT k.*, l.name as pol_name 
               FROM(SELECT distinct i.*, j.name 
                    FROM (SELECT g.*, h.department, h.image, h.name AS prodName 
                          FROM (SELECT e.*, f.atd, f.ata 
                                FROM (SELECT c.*, d.shipment_id, d.shipment_allocation_date 
                                      FROM (SELECT a.*, b.factory_delivery_date, b.receive_date 
                                            FROM (SELECT purchase_orders.purchase_order_id, purchase_order_items.purchase_order_item_id, purchase_orders.create_date, 
                                                  purchase_order_items.factory_booking_date,purchase_order_items.approx_recv_date, purchase_order_items.check_release_date, purchase_order_items.check_release_status,purchase_order_items.status, 
                                                  purchase_order_items.sku, purchase_order_items.quantity, purchase_order_items.dst_container, purchase_order_items.origin_container, purchase_order_items.port_of_loading
                                                  FROM purchase_orders 
                                                  JOIN purchase_order_items ON purchase_orders.purchase_order_id=purchase_order_items.purchase_order_id WHERE purchase_order_items.org_id = $1 and purchase_orders.org_id = $1) a 
                                            LEFT JOIN inventory_items b ON a.purchase_order_item_id=b.purchase_order_item_id AND a.purchase_order_id=b.purchase_order_id) c 
                                      LEFT JOIN shipment_items d ON c.purchase_order_id=d.purchase_order_id AND c.purchase_order_item_id=d.purchase_order_item_id) e 
                                LEFT JOIN shipments f ON e.shipment_id=f.shipment_id) g
                          LEFT JOIN products h on g.sku = h.sku) i
                    LEFT JOIN buildings j on i.origin_container = j.container_id) k
               LEFT JOIN buildings l on k.port_of_loading = l.container_id ORDER BY purchase_order_item_id ASC`;
    let res = await this.db.query(sql, [org_id]);
    for (let i = 0; i < res.rows.length; i++) {
      let vendorArr = res.rows[i].origin_container.split('&'); //this is to isolate the vendor code from 'factory&...'
      res.rows[i].vendor_code = vendorArr[1];

      if (res.rows[i].create_date) {
        res.rows[i].create_date = moment(res.rows[i].create_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].factory_booking_date) {
        res.rows[i].factory_booking_date = moment(res.rows[i].factory_booking_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].check_release_date) {
        res.rows[i].check_release_date = moment(res.rows[i].check_release_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].factory_delivery_date) {
        res.rows[i].factory_delivery_date = moment(res.rows[i].factory_delivery_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].approx_recv_date) {
        res.rows[i].approx_recv_date = moment(res.rows[i].approx_recv_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].receive_date) {
        res.rows[i].receive_date = moment(res.rows[i].receive_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].shipment_allocation_date) {
        res.rows[i].shipment_allocation_date = moment(res.rows[i].shipment_allocation_date).format('MM/DD/YYYY');
      }
      if (res.rows[i].ata) {
        res.rows[i].ata = moment(res.rows[i].ata).format('MM/DD/YYYY');
      }
      if (res.rows[i].atd) {
        res.rows[i].atd = moment(res.rows[i].atd).format('MM/DD/YYYY');
      }
    }
    return res;
  }

  async queryMilestoneExceptions(org_id) {

    moment.tz.setDefault("Europe/London");

    let sql = `SELECT distinct m.*, n.name as pol_name 
               FROM(SELECT k.*, l.name as dest_name
                    FROM (SELECT i.*, j.name as origin_name 
                          FROM (SELECT g.*, h.department, h.image, h.name AS prod_name
                                FROM (SELECT e.*, f.atd, f.etd
                                      FROM (SELECT c.*, d.shipment_id, d.shipment_allocation_date     
                                            FROM (SELECT a.*, b.factory_delivery_date, b.container_id as location, b.receive_date, b.blocked_quantity
                                                  FROM (SELECT purchase_orders.purchase_order_id, purchase_order_items.purchase_order_item_id, purchase_order_items.quantity, purchase_order_items.origin_container, 
                                                               purchase_order_items.dst_container, purchase_orders.create_date, 
                                                               purchase_order_items.factory_booking_status, purchase_order_items.factory_booking_date, purchase_order_items.check_release_date, purchase_order_items.sku,                                              
                                                               purchase_order_items.status, purchase_order_items.check_release_status, purchase_order_items.approx_recv_date, purchase_order_items.factory_appointment_date,
                                                               purchase_order_items.port_of_loading 
                                                        FROM purchase_orders 
                                                        JOIN purchase_order_items ON purchase_orders.purchase_order_id=purchase_order_items.purchase_order_id  and purchase_order_items.org_id = $1 where purchase_orders.org_id =$1) a 
                                                  LEFT JOIN inventory_items b ON a.purchase_order_item_id=b.purchase_order_item_id AND a.purchase_order_id=b.purchase_order_id) c 
                                            LEFT JOIN shipment_items d ON c.purchase_order_id=d.purchase_order_id AND c.purchase_order_item_id=d.purchase_order_item_id) e 
                                      LEFT JOIN shipments f ON e.shipment_id=f.shipment_id WHERE f.ata IS NULL) g 
                                LEFT JOIN products h ON g.sku = h.sku) i 
                          LEFT JOIN buildings j ON i.origin_container = j.container_id) k 
                    LEFT JOIN buildings l ON k.dst_container = l.container_id) m
              LEFT JOIN buildings n ON m.port_of_loading = n.container_id ORDER BY purchase_order_item_id ASC`;

    //let res = await this.db.query(`SELECT * FROM milestone_exceptions`);
    let res = await this.db.query(sql, [org_id]);
    let list = res.rows;
    let res2 = await this.db.query(`SELECT * FROM config_settings WHERE purpose='milestones'`);
    let intervalList = res2.rows;
    let settings = {};

    let seconds = {
      days: 86400,
      weeks: 604800,
      months: 2592000
    };

    for (let row of intervalList) {
      settings[row.setting] = row;
    }

    let columns = ['create_date', 'factory_booking_date', 'factory_appointment_date', 'factory_delivery_date', 'receive_date', 'shipment_allocation_date', 'atd'];
    let exceptions = [];

    for (let i = 0; i < list.length; i++) {
      let j = columns.length - 1;
      while (list[i][columns[j]] === null) {
        j--;
      }
      if (columns[j] !== 'atd') {
        let populatedColumn = columns[j];
        let aheadColumn = columns[j + 1];
        let aheadStr = aheadColumn.replace(new RegExp('_', 'g'), ' ');
        if (aheadStr === 'atd') {
          aheadStr = "time of departure";
        }
        let popStr = populatedColumn.replace(new RegExp('_', 'g'), ' ');

        let existingDate = moment(list[i][columns[j]]).utcOffset("+0000");
        let today = moment().utcOffset("+0000");
        let add = false;
        let msg = "";
        let status;
        let timestamp;

        let quantity = list[i].quantity;

        switch (populatedColumn) {
          case "create_date":
            let weeksUntilPOCons = moment(list[i].approx_recv_date).diff(today, 'seconds', true);
            if (weeksUntilPOCons < (settings['create_date'].number * seconds[settings['create_date'].interval])) { //1 week
              add = true;
              if (moment(list[i].approx_recv_date) <= today) {
                msg = "PO consolidation date passed with no booking date";
                status = "High";
              } else {
                msg = "PO consolidation date approaching with no booking date"
                status = weeksUntilPOCons >= ((settings['create_date'].number - 1) * seconds[settings['create_date'].interval]) ? "Low" : "Medium";
              }
            }
            break;
          case "factory_booking_date":
            if (today.diff(existingDate, 'seconds', true) > settings['factory_booking_date'].number * seconds[settings['factory_booking_date'].interval]) {
              if (list[i].factory_booking_status !== "APPROVED") {
                add = true;
                msg = "factory booking not approved after " + settings['factory_booking_date'].number + " " + settings['factory_booking_date'].interval;
                status = "Medium";
              }
            }
            break;
          case "factory_appointment_date":
            timestamp = existingDate.diff(today, 'seconds', true);
            if (timestamp < (settings['factory_appointment_date'].number * seconds[settings['factory_appointment_date'].interval])) { //3 weeks
              if (list[i].check_release_date === null) {
                add = true;
                if (existingDate <= today) {
                  msg = "factory appointment date passed with no check release date";
                  status = "High";
                } else {
                  msg = "factory appointment date within " + settings['factory_appointment_date'].number + " " + settings['factory_appointment_date'].interval + " with no check release date";
                  if (timestamp >= ((settings['factory_appointment_date'].number - 1) * seconds[settings['factory_appointment_date'].interval])) {
                    status = "Low";
                  } else {
                    status = "Medium"
                  }
                }
              } else if (list[i].check_release_date && list[i].check_release_status !== "PASS") {
                add = true;
                msg = "factory appointment date within " + settings['factory_appointment_date'].number + " " + settings['factory_appointment_date'].interval + ", check release is: " + list[i].check_release_status;
                if (timestamp >= ((settings['factory_appointment_date'].number - 1) * seconds[settings['factory_appointment_date'].interval])) {
                  status = "Low";
                } else {
                  status = "Medium"
                }
              }
            }
            break;
          case "factory_delivery_date":
            timestamp = today.diff(existingDate, 'seconds', true);
            if (timestamp > settings['factory_delivery_date'].number * seconds[settings['factory_delivery_date'].interval]) { //3 weeks
              add = true;
              msg = "receive date recording exceeds time limit of " + settings['factory_delivery_date'].number + " " + settings['factory_delivery_date'].interval;
              if (timestamp >= (settings['factory_delivery_date'].number - 1) * seconds[settings['factory_delivery_date'].interval]) {
                status = "Low";
              } else {
                status = "Medium";
              }
            }
            break;
          case "receive_date":
            timestamp = today.diff(existingDate, 'seconds', true);
            if (timestamp > settings['receive_date'].number * seconds[settings['receive_date'].interval]) {
              quantity = list[i].quantity - list[i].blocked_quantity;
              add = quantity > 0 ? true : false;
              msg = "shipment allocation recording exceeds time limit of " + settings['receive_date'].number + " " + settings['receive_date'].interval;
              if (timestamp <= (settings['receive_date'].number - 1) * seconds[settings['receive_date'].interval]) {
                status = "Low";
              } else {
                status = "Medium";
              }
            }
            break;
          case "shipment_allocation_date":
            if (today.diff(existingDate, 'seconds', true) > settings['shipment_allocation_date'].number * seconds[settings['shipment_allocation_date'].interval]) {
              add = true;
              msg = "time of departure recording exceeds time limit of " + settings['shipment_allocation_date'].number + " " + settings['shipment_allocation_date'].interval;
              status = "Low";
            }
            break;
          default:
            if (today.diff(existingDate, 'weeks', true) > 3) {
              add = true;
              msg = aheadStr + " recording exceeds time limit of 3 weeks since last timestamp";
              status = "Low";
            }
        }

        if (add) {
          let vendorArr = list[i].origin_container.split('&'); //this is to isolate the vendor code from 'factory&...'

          let obj = {
            purchase_order_id: list[i].purchase_order_id,
            purchase_order_item_id: list[i].purchase_order_item_id,
            populatedColumn: populatedColumn,
            unpopulatedColumn: aheadColumn,
            sku: list[i].sku,
            department: list[i].department ? list[i].department : "-",
            prodName: list[i].prod_name ? list[i].prod_name : "-",
            image: list[i].image,
            quantity: quantity,
            location: list[i].location,
            destination_container_id: list[i].destination_container_id,
            dest_name: list[i].dest_name ? list[i].dest_name.substring(8, list[i].dest_name.length) : "-",
            origin_container: list[i].origin_container,
            origin_name: list[i].origin_name,
            vendor_code: vendorArr[1],
            pol_name: list[i].pol_name,
            factory_booking_status: list[i].factory_booking_status ? list[i].factory_booking_status : "NULL",
            check_release_status: list[i].check_release_status ? list[i].check_release_status : "NULL",
            check_release_date: list[i].check_release_date ? moment(list[i].check_release_date).format('MM/DD/YYYY') : "NULL",
            approx_recv_date: list[i].approx_recv_date ? moment(list[i].approx_recv_date).format('MM/DD/YYYY') : null,
            factory_appointment_date: list[i].factory_appointment_date ? moment(list[i].factory_appointment_date).format('MM/DD/YYYY') : null,
            factory_booking_date: list[i].factory_booking_date ? moment(list[i].factory_booking_date).format('MM/DD/YYYY') : null,
            timestamp: popStr + ": " + moment(existingDate).format('MM/DD/YYYY'),
            msg: msg,
            status: status
          }
          exceptions.push(obj);
        }
      }
    }
    moment.tz.setDefault();
    console.log('sup',exceptions)
    return exceptions;
  }

  async queryShipment(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = ""
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }
    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }
    let iiSql = 'SELECT shipment_id, source_container_id, destination_container_id,  vessel_container_id ,eta,etd  FROM shipments' + sqlEnd;
    let iiRes = await this.db.query(iiSql, whereParams);
    let cache;
    debug('iiRes = %j', iiRes);
    //let cache = await this.cacheContainersForItems(whereClause, whereParams);

    // debug('cache = %j', cache);
    let res = { items: iiRes.rows, cache: cache };
    return res;
  }

  //for the detail location page 
  async queryShipmentID(whereClause, whereParams, type) {
    let sqlEnd = "";
    let iiSql, iiRes;
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    if (type === "port") {
      iiSql = `select *, (i.delivery_quantity/p.pack_size) as total_carton  from shipment_items as i ,(select * from shipments) as c ,(select * from products )as p   ${sqlEnd}`;
      iiRes = await this.db.query(iiSql, whereParams);
    } else if (type === "warehouse") {
      iiSql = `select *,(i.quantity / i.qty_per_carton) as total_carton from inventory_items as i, (select * from products )as p ${sqlEnd}`;
      iiRes = await this.db.query(iiSql, whereParams);
    } else if (type === "factory") {
      iiSql = `SELECT p.*, (p.quantity - p.recv_quantity) as pending_quantity,pro.* from purchase_order_items as p,(select * from products )as pro ${sqlEnd}`;
      iiRes = await this.db.query(iiSql, whereParams);
    } else {
      throw new Error("undefined data from sql query");
    }
    let cache;
    let res = { items: iiRes.rows, cache: cache };
    return res;
  }

  async getDestinationContainers(org_id) {
    let sql = 'SELECT DISTINCT destination_container_id, buildings.name FROM shipments JOIN buildings on destination_container_id=container_id where shipments.org_id = $1';
    return await this.db.query(sql, [org_id]);
  }

  async queryReport(dest) {
    let list = [];
    let sqlOne = `select a.org_country, a.port_of_loading, sum(a.quantity) 
                  from (select d.org_country, d.port_of_loading, d.quantity 
                        from (select org_country, port_of_loading, mcc_warehouse, quantity 
                              from purchase_order_items 
                              where dst_container = '` + dest + `' group by org_country, port_of_loading, mcc_warehouse, quantity) 
                        d order by org_country) a group by a.org_country, a.port_of_loading;`

    let resOne = await this.db.query(sqlOne);
    for (let obj of resOne.rows) {
      if (obj.port_of_loading === "port&YTN") {
        obj.mcc = "Shenzen";
      } else {
        obj.mcc = "Shanghai";
      }
    }
    list.push(resOne);

    let sqlTwo = `select e.etd, e.eta, f.department, f.sum 
                  from shipments e right join 
                  (select c.shipment_id, d.department, sum(d.quantity) as sum 
                  from shipment_items c left join 
                  (select a.department, quantity, b.purchase_order_item_id, b.purchase_order_id 
                  from products a right join purchase_order_items b on a.sku = b.sku) 
                  d on c.purchase_order_item_id = d.purchase_order_item_id and c.purchase_order_id = d.purchase_order_id group by shipment_id, department) 
                  f on e.shipment_id = f.shipment_id where e.destination_container_id ='` + dest + `' group by etd, eta, department, sum;`

    let resTwo = await this.db.query(sqlTwo);
    list.push(resTwo);

    let sqlThree = `select department, count(b.sku), b.approx_recv_date, sum(quantity) as sum 
                    from products a left join purchase_order_items b on a.sku = b.sku 
                    where b.dst_container='` + dest + `' group by approx_recv_date, department;`

    let resThree = await this.db.query(sqlThree);
    list.push(resThree);


    // let sqlEnd = select + "\'" + dest + "\' " + group;
    // let iiRes = await this.db.query(sqlEnd);
    return list;
  }

  async queryBuildings(whereClause, whereParams, groupBy, orderBy) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }
    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }
    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }
    //let sql = `SELECT DISTINCT ON(name) container_id, name, address, lat, lng from buildings` + sqlEnd;
    let sql = `select * from buildings ${sqlEnd}`;
    return await this.db.query(sql, whereParams);
  }

  async queryShipLocation(whereClause, whereParams, groupBy, orderBy) {
    let iiSql;
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += ' WHERE ' + whereClause;
    }

    if (groupBy) {
      sqlEnd += ' GROUP BY ';
      for (let i in groupBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += ' ORDER BY ';
      for (let i in orderBy) {
        if (i > 0) {
          sqlEnd += ','
        }
        sqlEnd += orderBy[i];
      }
    }
    if (!groupBy) {
      iiSql = `SELECT * FROM vehicle_locations` + sqlEnd;
    }
    let iiRes = await this.db.query(iiSql, whereParams);
    let cache;
    let res = { items: iiRes.rows, cache: cache };
    return res;
  }
  async queryShipHistory(shipmentId) {
    let sql = `SELECT i.*
               FROM ( SELECT g.*, h.lat AS dest_lat, h.lng AS dest_lng, h.name AS dest_name 
                      FROM ( SELECT e.*, f.lat AS next_lat, f.lng AS next_lng, f.name AS next_name, f.container_id as next_container 
                             FROM ( SELECT c.*, d.lat AS last_lat, d.lng AS last_lng, d.name AS last_name, d.container_id AS last_container 
                                    FROM ( SELECT a.*, a.destination_container_id AS dest_container, b.speed, b.last_port, b.next_port, b.lat, b.lng, b.timestamp, b.vessel_name 
                                           FROM shipments a RIGHT JOIN vehicle_locations b ON a.vessel_container_id = b.vessel_container_id 
                                           WHERE a.shipment_id = '` + shipmentId + `') c 
                                    LEFT JOIN buildings d ON c.last_port = d.address) e 
                             LEFT JOIN buildings f ON e.next_port = f.address) g 
                      LEFT JOIN buildings h ON g.dest_container = h.container_id ) i
                      where (i.ata is null or i.timestamp <= i.ata) AND (i.timestamp >= i.atd)
               ORDER BY i.ata, i.timestamp desc`

    return await this.db.query(sql);
  }

  async queryCurrentShipments(orgID) {
    let query = await this.db.query(`SELECT s.vessel_name, s.destination_container_id, s.shipment_id, s.source_container_id, s.eta, s.entry_filed, s.entry_released, s.do_issued, 
                                     b.name AS dest_name, b2.name AS source_name
                                     FROM shipments s
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                     LEFT JOIN buildings b2 ON s.source_container_id = b2.container_id
                                     WHERE ata IS NULL AND s.org_id=$1
                                     GROUP BY s.vessel_name, s.destination_container_id, s.shipment_id, s.source_container_id, s.eta, s.entry_filed, s.entry_released, s.do_issued, b.name, b2.name
                                     ORDER BY s.destination_container_id;`, [orgID]);
    let current = query.rows;
    let currentShips = [];

    //aggregation of current shipments by their destination
    if (current.length > 0) {
      let currentDest = current[0].dest_name;
      let shipments = [];
      for (let row of current) {
        // row.shipmentLink = <Button size="sm" variant="link" onClick={() => this.switchComponent("detail", row.shipment_id)}>{row.shipment_id}</Button>;
        row.eta = moment(row.eta).format('MMMM Do YYYY');
        if (row.dest_name === "Port of Seattle") {
          row.entry_filed = row.entry_filed ? moment(row.entry_filed).format('MMMM Do YYYY') : "Not Recorded";
          row.entry_released = row.entry_released ? moment(row.entry_released).format('MMMM Do YYYY') : "Not Recorded";
          row.do_issued = row.do_issued ? moment(row.do_issued).format('MMMM Do YYYY') : "Not Recorded";
        }

        if (row.dest_name !== currentDest) {
          currentShips.push({
            dest: currentDest,
            shipments: shipments
          });
          currentDest = row.dest_name;
          shipments = []
        }
        shipments.push(row);
      }
      currentShips.push({
        dest: currentDest,
        shipments: shipments
      });
    }

    return currentShips;

  }

  async queryPastShipments(orgID) {
    let query = await this.db.query(`select s.vessel_name, s.destination_container_id, s.shipment_id, s.source_container_id, s.eta,s.ata,s.atd, s.entry_filed, s.entry_released, s.do_issued, 
                                     b.name as dest_name, b2.name as source_name
                                     FROM shipments s
                                     left join buildings b on s.destination_container_id = b.container_id
                                     left join buildings b2 on s.source_container_id = b2.container_id
                                     where ata IS NOT NULL AND s.org_id = $1
                                     GROUP BY s.vessel_name,s.destination_container_id, s.shipment_id,s.source_container_id,s.eta,s.ata,s.atd, b.name, b2.name,s.entry_filed, s.entry_released, s.do_issued
                                     ORDER BY s.destination_container_id`, [orgID]);
    let past = query.rows;
    let pastShips = [];

    //aggregation of current shipments by their destination
    if (past.length > 0) {
      let currentDest = past[0].dest_name;
      let shipments = [];

      for (let row of past) {
        // row.shipmentLink = <Button size="sm" variant="link" onClick={() => this.switchComponent("detail", row.shipment_id)}>{row.shipment_id}</Button>
        row.ata = row.ata === null ? "Not Recorded" : moment(row.ata).format('MMMM Do YYYY');
        row.atd = row.atd === null ? "Not Recorded" : moment(row.atd).format('MMMM Do YYYY');
        row.entry_filed = row.entry_filed ? moment(row.entry_filed).format('MMMM Do YYYY') : "Not Recorded";
        row.entry_released = row.entry_released ? moment(row.entry_released).format('MMMM Do YYYY') : "Not Recorded";
        row.do_issued = row.do_issued ? moment(row.do_issued).format('MMMM Do YYYY') : "Not Recorded";
        if (row.dest_name !== currentDest) {
          pastShips.push({
            dest: currentDest,
            shipments: shipments
          });
          currentDest = row.dest_name;
          shipments = []
        }
        shipments.push(row);
      }
      pastShips.push({
        dest: currentDest,
        shipments: shipments
      });
    }

    return pastShips;
  }

  async queryPOsOnShipment(orgID, shipmentID) {
    let query = await this.db.query(`SELECT e.*, p.name AS prod_name 
                  FROM (SELECT d.*, bui2.name AS dest_name, bui2.lat AS dest_lat, bui2.lng AS dest_lng 
                        FROM (SELECT c.*, bui.name AS source_name 
                              FROM (SELECT a.*, si.purchase_order_id, si.purchase_order_item_id, si.sku, si.delivery_quantity as quantity
                                    FROM (SELECT s.shipment_id, s.eta, s.original_eta, s.etd, s.vessel_container_id, s.ata, s.remark, s.vessel_name, s.source_container_id, s.destination_container_id, s.tracking_id, 
                                          v.last_port, v.next_port, v.lat, v.lng, v.timestamp 
                                          FROM shipments s 
                                          LEFT JOIN vehicle_locations v ON s.vessel_container_id = v.vessel_container_id
                                          WHERE s.org_id = $1) a 
                                    LEFT JOIN shipment_items si ON a.shipment_id = si.shipment_id) c
                              LEFT JOIN buildings bui ON c.source_container_id = bui.container_id) d
                        LEFT JOIN buildings bui2 ON d.destination_container_id = bui2.container_id) e
                  LEFT JOIN products p ON e.sku = p.sku
                  LEFT OUTER JOIN vehicle_locations v2 ON (e.vessel_container_id = v2.vessel_container_id AND (e.timestamp < v2.timestamp)) 
                  WHERE e.shipment_id = $2 AND v2.vessel_container_id IS NULL;`, [orgID, shipmentID]);
    
    let list = query.rows;
    let oneShipment = [];

    if (list.length > 0) {
      for (let row of list) {
        row.etaFormat = moment(row.eta).format("MMMM Do YYYY HH:mm")
        if (row.shipment_id === shipmentID) {
          oneShipment.push(row);
        }
      }

      oneShipment.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));
    }

    return oneShipment;
  }

  async getShipmentTrackingDetails(trackingID) {
    let query = await this.db.query(`SELECT ste.*, b.name, v.name as vessel_name 
                                     FROM shipment_tracking_events ste 
                                     LEFT JOIN buildings b ON ste.location = b.container_id
                                     LEFT JOIN vessels v ON ste.vessel = v.mmsi 
                                     WHERE tracking_id=$1 
                                     ORDER BY date ASC;`, [trackingID]);
    
    //this code block exists because there are duplicate mmsi entries in the vessels table
    //once those are deleted/fixed this won't need to be here, thank god
    if (query.rows.length > 0) {
      let distinctRows = [];
      let distinctDates = {};
      for (let row of query.rows) {
        if (!distinctDates[row.date]) {
          distinctDates[row.date] = row.date;
          distinctRows.push(row);
        }
      }

      return distinctRows;
    } else {
      return []
    }
  }

  //
  // Replication APIs
  //

  async readSyncData(table, startDate, endDate) {
    logger.debug('startDate: ' + startDate)

    let sql;

    sql = 'SELECT * FROM ' + table;

    logger.debug('sql = %s', sql);
    let rows = await this.db.query(sql);
    logger.debug('rows = %j', rows);
    return rows.rows;
  }

  async writeSyncData(table, rows) {

  }

  //gets the alternate names for containers(vessel_name/buildings.name)
  async getContainerNames() {
    let query = `SELECT buildings.name, vehicle_locations.vessel_name, containers.container_id
                  FROM containers left join buildings on buildings.container_id = containers.container_id  
                  left join vehicle_locations on vehicle_locations.vessel_container_id = containers.container_id
                  group by buildings.name, vehicle_locations.vessel_name, containers.container_id`

    let containerNames = await this.db.query(query);
    return containerNames.rows;
  }
  async getSkusForContainer(containerId) {
    console.log("woo", containerId)
    ///parsing the containerId allows us to avoid making multiple queries this is relient on container id syntax staying the same 
    let containerType = ''
    let skusForContainer;
    for (let i in containerId) {
      if (containerId[i] == '&') {
        break
      }
      else {
        containerType += containerId[i];
      }
    }
    if (containerType == 'ship') {
      //This is a temporary query that will not work for nested containers
      //need to update on next demo iteration
      let query1 = `SELECT v1.vessel_name as vessel_name,
                  v1.lat as lat, 
                  v1.lng as lng, 
                  products.name as productname, 
                  products.sku, 
                  products.department,
                  products.image,
                  inventory_items.purchase_order_id,
                  inventory_items.purchase_order_item_id,
                  inventory_items.quantity as containerquantity, 
                  inventory_items.container_id,
                  inventory_items.shipment_id,
                  containers.type
                  FROM inventory_items 
                  LEFT JOIN products on inventory_items.sku = products.sku 
                  LEFT JOIN containers on inventory_items.container_id = containers.container_id 
                  LEFT JOIN vehicle_locations v1 on containers.parent_container_id = v1.vessel_container_id LEFT OUTER JOIN vehicle_locations v2 ON (containers.parent_container_id = v2.vessel_container_id AND (v1.timestamp < v2.timestamp)) WHERE v2.vessel_container_id IS NULL
                  and containers.parent_container_id = $1 
                  group by inventory_items.container_id, inventory_items.shipment_id, containers.type, containers.parent_container_id, productname, products.sku,products.image,products.department,  inventory_items.purchase_order_id, inventory_items.purchase_order_item_id,inventory_items.quantity, inventory_items.sku, v1.vessel_name,v1.lat,v1.lng;`

      let inventoryItems = await this.db.query(query1, [containerId])
      console.log("INV ITEMS", inventoryItems.rows);
      skusForContainer = inventoryItems

    }
    if (containerType == 'port') {
      let query2 = `SELECT products.sku,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(shipment_items.delivery_quantity) as containerQuantity
                    FROM shipments 
                    LEFT JOIN shipment_items on shipment_items.shipment_id=shipments.shipment_id 
                    LEFT JOIN purchase_order_items on shipment_items.purchase_order_id = purchase_order_items.purchase_order_id 
                    AND shipment_items.purchase_order_item_id = purchase_order_items.purchase_order_item_id 
                    LEFT JOIN buildings on buildings.container_id = purchase_order_items.dst_container 
                    LEFT JOIN products on products.sku = purchase_order_items.sku 
                    WHERE destination_container_id = $1 
                    AND shipment_items.delivery_quantity >0
                    GROUP BY products.sku,products.department, products.image, buildingname, productname,lat, lng`;

      let destinationItems = await this.db.query(query2, [containerId]);
      skusForContainer = destinationItems;
    }
    if (containerType == 'shipping-container') {
      let query5 = `SELECT containers.type, containers.container_id,products.sku,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(inventory_items.quantity) as containerquantity  
          FROM inventory_items 
          JOIN products on products.sku = inventory_items.sku 
          LEFT JOIN containers on inventory_items.container_id= containers.container_id  
          LEFT JOIN buildings on buildings.container_id = containers.parent_container_id
          WHERE inventory_items.container_id= $1 
          GROUP BY products.sku,products.department, containers.type, containers.container_id,products.image, buildingname, productname,lat, lng;`
      skusForContainer = await this.db.query(query5, [containerId]);
    }
    if (containerType == 'factory') {
      let query3 = `SELECT products.sku,containers.type, containers.container_id,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(purchase_order_items.quantity -purchase_order_items.recv_quantity) as containerquantity  
                      FROM purchase_order_items 
                      JOIN products on products.sku = purchase_order_items.sku 
                      LEFT JOIN containers on purchase_order_items.origin_container= containers.container_id  
                      LEFT JOIN buildings on buildings.container_id = purchase_order_items.origin_container 
                      WHERE purchase_order_items.origin_container = $1 
                      AND (purchase_order_items.quantity -purchase_order_items.recv_quantity)>0 
                      GROUP BY products.sku,containers.type, containers.container_id,products.department, products.image, buildingname, productname,lat, lng`;

      skusForContainer = await this.db.query(query3, [containerId]);
    }

    if (containerType == 'warehouse') {
      let query4 = `SELECT products.sku, containers.type, containers.container_id, products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(inventory_items.quantity) as containerquantity  
      FROM inventory_items 
      JOIN products on products.sku = inventory_items.sku 
      LEFT JOIN containers on inventory_items.container_id= containers.container_id  
      LEFT JOIN buildings on buildings.container_id = containers.container_id
      WHERE inventory_items.container_id= $1 
      GROUP BY products.sku,products.department, products.image, buildingname,containers.type, containers.container_id, productname,lat, lng`;
      skusForContainer = await this.db.query(query4, [containerId]);
    }
    let items = [];
    for (let i in skusForContainer.rows) {
      let item = Object.create(Item.prototype);

      let resItem = Object.assign(item, skusForContainer.rows[i]);
      items.push(resItem);
    }

    return { items: items, cache: null };
  }

  async addOrgToContainers(orgId, containerList) {
    let containers = []
    for (let i = 0; i < containerList.length; i++) {
      containers.push(this.queryContainer('container_id = $1', ['shipping-container&' + containerList[i]]));
    }
    let containerObjects = await Promise.all(containers);
    let newInventoryItems = [];
    for (let i = 0; i < containerObjects.length; i++) {
      if (containerObjects[i].items.length) {
        newInventoryItems.push(this.addInventoryItem({ org_id: orgId, container_id: containerObjects[i].items[0].container_id, sku: orgId + "sku", purchase_order_id: 1, purchase_order_item_id: 1 }));
      }
    }
    await Promise.all(newInventoryItems);
    return containerObjects;
  }
  async getInventoryLocations(organization_id) {
    try {
      let newQuery =
        `WITH RECURSIVE parents AS (
      SELECT
      container_id,
      parent_container_id,
      type,
      ref
      FROM
      containers 
      WHERE
      container_id in (SELECT container_id from inventory_items where org_id = $1)
      UNION
      SELECT
      c.container_id,
      c.parent_container_id,
      c.type,
      c.ref
      FROM
     containers c
      INNER JOIN parents p ON c.container_id = p.parent_container_id
     ) SELECT
      vehicle_destinations.eta, vehicle_destinations.pol, vehicle_destinations.pod, v1.vessel_name,v1.lat as vessel_lat,v1.lng as vessel_lng,buildings.*,parents.container_id, parents.type, parents.ref,parents.parent_container_id
     FROM parents 
      left join buildings on parents.container_id = buildings.container_id 
      left JOIN vehicle_locations v1 
      on parents.container_id = v1.vessel_container_id 
      LEFT JOIN vehicle_destinations on parents.container_id = vehicle_destinations.vessel_container_id 
      LEFT OUTER JOIN vehicle_locations v2 
      ON (parents.container_id = v2.vessel_container_id 
      AND (v1.timestamp < v2.timestamp)) 
      WHERE v2.vessel_container_id IS NULL;
   `
      let locations = await this.db.query(newQuery, [organization_id])
      let cache = {};
      for (let i = 0; i < locations.rows.length; i++) {
        cache[locations.rows[i].container_id] = locations.rows[i]
      }
      return { items: locations.rows, cache: cache };
    }
    catch (err) {
      console.log(err)
      return err
    }
  }

  /* async uploadExcel(file, type) {
     let workbook = await XLSX.readFile(file.path);
     let sheet_name_list = workbook.SheetNames;
     let xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]); 
     let upload = await processExcel(this, xlData, type);
     return //upload;

  } */

  async submitPull(org, service, username, password, pullInterval) {
    let sql = `INSERT INTO external_accounts (org, external_username, external_password, service, interval) VALUES ($1, $2, $3, $4, $5)`;
    try {
      let newSubmit = await this.db.query(sql, [org, username, password, service, pullInterval]);
      return newSubmit;
    } catch (err) {
      return err;
    }
  }

  async queryExistingServices(whereClause) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    let sql = "SELECT external_username, service FROM external_accounts" + sqlEnd;
    let result = await this.db.query(sql);
    return result;
  }

  async changeConfigSettings(intervals) {
    let sqlStart = `UPDATE config_settings AS cs SET number = i.number, interval = i.interval FROM( VALUES `;
    let sqlMid = ""
    let sqlEnd = ") AS i (setting, number, interval) WHERE cs.setting = i.setting";
    let keyList = [];

    for (let key in intervals) {
      keyList.push(key);
    }

    for (let i = 0; i < keyList.length; i++) {
      if (keyList[i].includes("Int")) {
        sqlMid += (`'` + intervals[keyList[i]] + `')`);
        if (i < keyList.length - 1) {
          sqlMid += `, `;
        }
      } else {
        sqlMid += (`('` + keyList[i] + `', ` + intervals[keyList[i]] + `, `);
      }
    }

    let sql = sqlStart + sqlMid + sqlEnd;
    try {
      let res = await this.db.query(sql);
      return res;
    } catch (err) {
      return { errorMessage: err.toString() };
    }
  }

}

module.exports = FcSqlClient;