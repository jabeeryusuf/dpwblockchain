

class MessageDb {

  constructor(db) {
    this.db = db;
  }

  async addGroupMessage(group_id, user_id, text) {
    try {
      return await this.db.query(`INSERT INTO messages (group_id, user_id, body) VALUES ($1, $2, $3)`, [group_id, user_id, text]);
    } catch (err) {
      return { errorMessage: err }
    }
  }

  async getMessages(groupId) {
    let messages = await this.db.query(`SELECT * FROM messages WHERE group_id = $1 ORDER BY received_time ASC`, [groupId]);
    //console.log(messages)
    return messages;
  }
  
  async getConversationsForGroups(groupIds) {

    let result = await this.db.query('SELECT DISTINCT m.group_id FROM messages m WHERE group_id = ANY ($1)', [groupIds]);

    // convert from [{group_id: foo},{group_id:bar}] to [foo,bar]
    let convs = result.rows.map((o) => o.group_id);
    return convs ? convs : [];
  }

  async getConversationReadCounts(userId, groupIds) {
    let messageRes = await this.db.query(`SELECT group_id, count FROM conversations_view WHERE group_id = ANY ($1)`, [groupIds]);
    let readCountRes = await this.db.query(`SELECT conversation_id, read_count FROM conversation_user_preferences WHERE user_id = $1 AND conversation_id = ANY ($2)`, [userId, groupIds]);
    
    let readCounts = {};
    let convDetails = {};

    for (let obj of readCountRes.rows) {
      readCounts[obj.conversation_id] = obj.read_count;
    }

    for (let obj of messageRes.rows) {
      convDetails[obj.group_id] = {
        read_count: readCounts[obj.group_id] ? readCounts[obj.group_id] : 0,
        msg_count: parseInt(obj.count)
      }
    }

    return convDetails;
  }

  async updateConversationPreferences(convId, userId, pref) {
    //getting the key in pref
    let key = Object.keys(pref)[0];
    //getting the value of key in pref;
    let value = pref[key];

    //lazy insert to table if not exists already
    let sql = `INSERT INTO conversation_user_preferences
                  (conversation_id, user_id)
               SELECT $1, $2
               WHERE
                  NOT EXISTS (
                      SELECT conversation_id, user_id FROM conversation_user_preferences WHERE conversation_id=$3 AND user_id=$4
                  )`;
    
    //I did this because for some reason it gets mad if I don't
    await this.db.query(sql, [convId, userId, convId, userId]);

    sql = `UPDATE conversation_user_preferences SET ` + key + ` = $3 WHERE conversation_id=$1 AND user_id=$2`;

    let update = await this.db.query(sql, [convId, userId, value]);
    return update;
  }

}

module.exports = MessageDb;