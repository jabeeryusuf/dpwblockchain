'use strict';

// Basic container Structure:
// container_id - system id for the contanier
// parent_id - parent container or 0 for top level locations
// type - container type
// ref - reference string externally defining the container.  Type + parent must be unique within the parent

const invoke = require('./invoke-transaction.js');
const queryChaincode = require('./query.js');
const Params = require('./Params');
const logger = require('debug-logger')('fc-fabric:FcFabricClient');
const {Container} = require('fc-blockchain-common');
const path = require('path');
const hfc = require('fabric-client');
const helper = require('./helper');

class NodeTransactionClient {

  constructor(user,org) {
    
    // This still sucks but getting better.   Specify the profile through an environment var. 
    // If it is not specified assume dev and go to the 
    // go to the dev network in thh repository.  This should probably move into common
    let configDir = process.env.FC_PROFILE_DIR;
    if (!configDir) {
      configDir = path.join(__dirname, '../../dev-network');
    }
    
    hfc.setConfigSetting('profile-config-path',configDir);

    // We set the key value store for crypto to couch db.  This is referencing a specific
    // package in the node module.
    hfc.setConfigSetting('key-value-store', 'fabric-client/lib/impl/CouchDBKeyValueStore.js');

    // HACK:  Still not sure where to store this
    const configAdmins = [{username:'admin', secret:'adminpw'}];
    // ENDHACK
    
    hfc.setConfigSetting('admins', configAdmins);

    // For now we're going to keep it to one channel and chaincode name
    this.channel = 'mychannel';
    this.chaincode = 'freightcc';
    
    // User and org is supplied ?  Is this right
    this.user = user;
    this.org = org;
 
 
  }

  // 
  // Specific hyperledger commands..  probably should have an admin class
  //

  async connect() {

    // register the user
    
    logger.info('admin = %j',hfc.getConfigSetting('admins'));
    let response = await helper.getRegisteredUser(this.user, this.org,true);
    logger.info('registered user: %j', response);
    
  }
  
  async disconnect() {
    //    hfc.disconnect();
  }



  async invoke(func, margs) {
    return await invoke.invokeChaincode(this.peer,this.channel,this.chaincode, func, margs, this.user, this.org);
  }

  async query(func, args) {
    return await queryChaincode.queryChaincode(this.peer, this.channel, this.chaincode, func, args, this.user, this.org);
  }

  //
  // Containers for the fc client
  //

  async addContainer(timestamp, type, ref, parent) {
    logger.info('in addContainer');

    let p = Params.start().date('timestamp').string('type').string('ref').optional().id('parentId');


    // parent is optional if this is a root container
    let umArgs = Array.prototype.slice.call(arguments,0,3);
    let cache = {};
    let pid;
    if (parent) {
      pid = await parent.getId();
      cache[pid] = parent;
      umArgs.push(pid);
    }
    
    logger.debug("params = %j",umArgs);
    let margs = p.marshal(umArgs);
    
    logger.info('margs = %j',margs);
    
    await this.invoke('addContainer',margs);
  
    // Create the local container with this context
    let c = new Container(type,ref,pid);
    c.setContext(this,cache);

    return c;
  }
  

  async updateContainerParent(timestamp, holder, location) {
    let margs = Params.start().date('ts').container('c').container('h').marshal(arguments);
    
    await this.invoke('updateContainerHolder',margs);
  }



  //
  // Inventory and container transactions
  //

  async updateContainerInventory(timestamp,container,inventory) {
    let p = Params.start().timestamp('ts').container('c').inventory('i').marshal(arguments);
    
    // Needs to remove existing inventory?
    // Currently focused on packing of a teu or a bin.  
    await this.invoke('updateContainerInventory',margs);
  }

  // update shipment in container?
  // update po in container?

  async updateVehicleLocation(timestamp,newLocation, vehicle) {
    

  }

 
  //
  // Synchronization API
  //

  async writeSyncData(table, rows) {
    let p = Params.start().string('table').json('rows');
    let margs = p.marshal(arguments);

    console.log(margs[1]);
    await this.invoke('writeSyncData', margs);
  }

  readSyncData(table,startDate,endDate) {
    let p = Params.start().string('table').date('startTime').date('endTime');

    


  }

  initPOItems(doc) {
    let item = {
      "purchase_order_id": doc.Record.purchase_order_id,
      "purchase_order_line": doc.Record.purchase_order_item_id,
      "origin_container": doc.Record.origin_container,
      "sku": doc.Record.sku,
      "quantity": doc.Record.quantity,
      "dst_container": doc.Record.dst_container
    }

    return item;
  }

  getProductInfo(doc, items) {
    for (let i in items) {
      if (doc.Record.docType === "products") {
        if (items[i].sku === doc.Record.sku) {
          items[i].name = doc.Record.name;
          items[i].department = doc.Record.department;
          items[i].image = doc.Record.image;
        }
      } else if (doc.Record.docType === "inventory_items") {
        let po_num = doc.Record.purchase_order_id;
        let pol = doc.Record.purchase_order_item_id;
        if (items[i].purchase_order_id === po_num && items[i].purchase_order_line === pol) {
          items[i].container_id = doc.Record.container_id;
          if (items[i].shipment_id !== "") {
            items[i].shipment_id = doc.Record.shipment_id;
          }
        }
      }
    }

    for (let i in items) {
      if (!items[i].container_id) {
        items[i].container_id = items[i].origin_container;
      }
    }
  }

  async getLocationInfo(items, containers, flag) {
    let sql = "SELECT container_id, lat, lng FROM blank WHERE container_id = ";
    let str = " OR containter_id = ";
    for (let i in containers) {
      if (i == 0) {
        sql += ("'" + containers[i] + "'");
      }
      if (i > 0) {
        sql += (str + "'" + containers[i] + "'");
      }
    }

    if (flag === "ship") {
      sql += " AND docType = 'vehicle_locations'";
    } else {
      sql += " AND docType = 'buildings'";
    }

    let result = await this.query('query', sql);
    result = JSON.parse(result);

    for (let i in result) {
      let resContainer =  result[i].Record.container_id;
      for (let j in items) {
        if (resContainer === items[j].container_id) {
          items[j].lat = result[i].Record.lat;
          items[j].lng = result[i].Record.lng;
        }
      }
    }
  }



}

module.exports = NodeTransactionClient;