
'use strict';

const {Container, Inventory} = require('fc-blockchain-common');
const util = require('util');

class Params {

  constructor() {
    this.params = [];
  }
 
  static start() {
    return new Params();
  }

  optional() {
    this.optional = this.params.length;
    return this;
  }

  //
  // descriptor
  //

  string(name) {
    this.params.push({name: name, type: 'string'});
    return this;
  }

  json(name) {
    this.params.push({name: name, type: 'json'});
    return this;
  }

  id(name) {
    this.params.push({name: name, type: 'id'});
    return this;
  }

  date(name) {
    this.params.push({name: name, type: 'date'});
    return this;
  }


  // Will remove!!!!
  container(name) {
    this.params.push({name: name, type: 'container'});
    return this;
  }
  inventory(name) {
    this.params.push({name: name, type: 'inventory'});
    return this;
  }
  product(name) {
    this.params.push({name: name, type: 'product'});
    return this;
  }

  //
  // unmarshal the params
  //

  marshal(umParams) {
    let params = this.params;
    let optional = this.optional ? this.optional : params.length;
    let args = [];

    if (umParams.length < optional) {
      throw new Error('required paramater count is ' + optional + ' and got ' + umParams.length);
    }

    for (let i = 0; i < Math.min(params.length,umParams.length); i++) {
      let type = params[i].type;
      let ump = umParams[i];

      switch(type) {
      case 'string':
      case 'id': 
        args.push(ump);
        break;
      case 'date':
        args.push(ump.toUTCString());
        break;
      case 'json':
        args.push(JSON.stringify(ump));
        break;
      default:
        throw new Error('internal error, unknown type');
      }
    }

    return args;
  }


}

module.exports = Params;