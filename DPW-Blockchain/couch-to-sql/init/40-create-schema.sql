--TABLE: products
--------------------------------------
DROP TABLE IF EXISTS products;


CREATE TABLE products
AS SELECT
  (doc ->> 'sku')::text AS sku,
  (doc ->> 'name')::text AS name,
  (doc ->> 'apn')::text AS apn,
  (doc ->> 'department')::text AS department,
  (doc ->> 'image')::text AS image,
  (doc ->> 'outer_carton_height')::numeric AS outer_carton_height,
  (doc ->> 'outer_carton_width')::numeric AS outer_carton_width,
  (doc ->> 'outer_carton_length')::numeric AS outer_carton_length,
  (doc ->> 'pack_size')::numeric AS pack_size,
  (doc ->> 'carton_cbm')::numeric AS carton_cbm
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'products';


CREATE UNIQUE INDEX IF NOT EXISTS products_sku ON products (sku);



--TABLE: containers
--------------------------------------
DROP TABLE IF EXISTS containers;


CREATE TABLE containers
AS SELECT
  (doc ->> 'container_id')::text AS container_id,
  (doc ->> 'type')::text AS type,
  (doc ->> 'ref')::text AS ref,
  (doc ->> 'parent_container_id')::text AS parent_container_id,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'containers';


CREATE UNIQUE INDEX IF NOT EXISTS containers_container_id ON containers (container_id);



--TABLE: buildings
--------------------------------------
DROP TABLE IF EXISTS buildings;


CREATE TABLE buildings
AS SELECT
  (doc ->> 'container_id')::text AS container_id,
  (doc ->> 'address')::text AS address,
  (doc ->> 'name')::text AS name,
  (doc ->> 'lat')::numeric AS lat,
  (doc ->> 'lng')::numeric AS lng,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'buildings';


CREATE UNIQUE INDEX IF NOT EXISTS buildings_container_id ON buildings (container_id);



--TABLE: purchase_orders
--------------------------------------
DROP TABLE IF EXISTS purchase_orders;


CREATE TABLE purchase_orders
AS SELECT
  (doc ->> 'purchase_order_id')::text AS purchase_order_id,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'purchase_orders';


CREATE UNIQUE INDEX IF NOT EXISTS purchase_orders_purchase_order_id ON purchase_orders (purchase_order_id);



--TABLE: purchase_order_items
--------------------------------------
DROP TABLE IF EXISTS purchase_order_items;


CREATE TABLE purchase_order_items
AS SELECT
  (doc ->> 'purchase_order_item_id')::text AS purchase_order_item_id,
  (doc ->> 'purchase_order_id')::text AS purchase_order_id,
  (doc ->> 'sku')::text AS sku,
  (doc ->> 'quantity')::numeric AS quantity,
  (doc ->> 'recv_quantity')::numeric AS recv_quantity,
  (doc ->> 'approx_recv_date')::timestamptz AS approx_recv_date,
  (doc ->> 'origin_container')::text AS origin_container,
  (doc ->> 'dst_container')::text AS dst_container,
  (doc ->> 'port_of_loading')::text AS port_of_loading,
  (doc ->> 'received_container')::text AS received_container,
  (doc ->> 'org_country')::text AS org_country,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'purchase_order_items';


CREATE UNIQUE INDEX IF NOT EXISTS purchase_order_items_purchase_order_item_id_purchase_order_id ON purchase_order_items (purchase_order_item_id,purchase_order_id);



--TABLE: inventory_items
--------------------------------------
DROP TABLE IF EXISTS inventory_items;


CREATE TABLE inventory_items
AS SELECT
  (doc ->> 'container_id')::text AS container_id,
  (doc ->> 'sku')::text AS sku,
  (doc ->> 'purchase_order_id')::text AS purchase_order_id,
  (doc ->> 'purchase_order_item_id')::text AS purchase_order_item_id,
  (doc ->> 'shipment_id')::text AS shipment_id,
  (doc ->> 'qty_per_carton')::numeric AS qty_per_carton,
  (doc ->> 'quantity')::numeric AS quantity,
  (doc ->> 'receive_date')::timestamptz AS receive_date,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'inventory_items';


CREATE UNIQUE INDEX IF NOT EXISTS inventory_items_container_id_sku_purchase_order_id_purchase_order_item_id ON inventory_items (container_id,sku,purchase_order_id,purchase_order_item_id);



--TABLE: shipments
--------------------------------------
DROP TABLE IF EXISTS shipments;


CREATE TABLE shipments
AS SELECT
  (doc ->> 'shipment_id')::text AS shipment_id,
  (doc ->> 'source_container_id')::text AS source_container_id,
  (doc ->> 'destination_container_id')::text AS destination_container_id,
  (doc ->> 'vessel_container_id')::text AS vessel_container_id,
  (doc ->> 'eta')::timestamptz AS eta,
  (doc ->> 'etd')::timestamptz AS etd,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'shipments';


CREATE UNIQUE INDEX IF NOT EXISTS shipments_shipment_id ON shipments (shipment_id);



--TABLE: shipment_items
--------------------------------------
DROP TABLE IF EXISTS shipment_items;


CREATE TABLE shipment_items
AS SELECT
  (doc ->> 'shipment_id')::text AS shipment_id,
  (doc ->> 'purchase_order_id')::text AS purchase_order_id,
  (doc ->> 'purchase_order_item_id')::text AS purchase_order_item_id,
  (doc ->> 'delivery_quantity')::numeric AS delivery_quantity,
  (doc ->> 'delivery_time')::timestamptz AS delivery_time,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'shipment_items';


CREATE UNIQUE INDEX IF NOT EXISTS shipment_items_shipment_id_purchase_order_id_purchase_order_item_id ON shipment_items (shipment_id,purchase_order_id,purchase_order_item_id);



--TABLE: vehicle_locations
--------------------------------------
DROP TABLE IF EXISTS vehicle_locations;


CREATE TABLE vehicle_locations
AS SELECT
  (doc ->> 'container_id')::text AS container_id,
  (doc ->> 'last_port')::text AS last_port,
  (doc ->> 'next_port')::text AS next_port,
  (doc ->> 'lat')::numeric AS lat,
  (doc ->> 'lng')::numeric AS lng,
  (doc ->> 'signal_age')::text AS signal_age,
  (doc ->> 'atd')::text AS atd,
  (doc ->> 'eta')::text AS eta,
  (doc ->> 'speed')::text AS speed,
  (doc ->> 'timestamp')::timestamptz AS timestamp,
  (doc ->> 'vessel_name')::text AS vessel_name,
  (doc ->> 'course')::text AS course,
  (doc ->> 'create_time')::timestamptz AS create_time,
  (doc ->> 'update_time')::timestamptz AS update_time
FROM couch_mychannel_freightcc
WHERE (doc ->> 'docType')::text = 'vehicle_locations';


CREATE UNIQUE INDEX IF NOT EXISTS vehicle_locations_container_id_signal_age ON vehicle_locations (container_id,signal_age);




-- CREATE OR REPLACE FUNCTION update_materialized_views()
-- RETURNS trigger AS
-- $BODY$
--   BEGIN

--   REFRESH MATERIALIZED VIEW CONCURRENTLY products;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY containers;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY buildings;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY purchase_orders;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY purchase_order_items;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY inventory_items;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY shipments;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY shipment_items;
--   REFRESH MATERIALIZED VIEW CONCURRENTLY vehicle_locations;

--   RETURN NULL;
-- END;
-- $BODY$ LANGUAGE plpgsql;
  
-- CREATE TRIGGER since_checkpoint_changes AFTER UPDATE
-- ON since_checkpoints FOR EACH ROW
-- EXECUTE PROCEDURE update_materialized_views();

