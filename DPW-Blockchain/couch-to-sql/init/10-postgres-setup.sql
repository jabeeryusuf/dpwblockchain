
-- Direct Copy of couchdb
CREATE TABLE couch_mychannel_freightcc
(
  id text NOT NULL,
  doc jsonb,
  CONSTRAINT example_pkey PRIMARY KEY (id)
);

CREATE TABLE since_checkpoints
(
  pgtable text NOT NULL,
  couchdb text NOT NULL,
  since text DEFAULT '0',
  enabled boolean DEFAULT false,
  CONSTRAINT since_checkpoint_pkey PRIMARY KEY (pgtable)
);

INSERT INTO since_checkpoints (pgtable, couchdb, enabled ) VALUES ('couch_mychannel_freightcc', 'mychannel_freightcc',true);