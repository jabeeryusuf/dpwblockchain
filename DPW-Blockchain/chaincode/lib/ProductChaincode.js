'use strict';
const shim = require('fabric-shim');
const util = require('util');



class ProductChaincode {


  async addProduct(stub, args) {
    console.info('============= START : Create Product ===========');
    if (args.length != 3) {
      throw new Error('Incorrect number of arguments. Expecting 5');
    }

    var product = {
      docType: 'product',
      merchantId: args[0],
      merchantSku: args[1],
      description: args[2]
    };

    await stub.putState(product.merchantId + ':' + product.merchantSku,
			Buffer.from(JSON.stringify(product)));
    console.info('============= END : Create Product ===========');
  }
  
  async getProduct(stub, args) {
    if (args.length != 2) {
      throw new Error('Incorrect number of arguments. Expecting CarNumber ex: CAR01');
    }
    let merchantId = args[0];
    let merchantSku = args[1];
    
    let productAsBytes = await stub.getState(merchantId + ':' + merchantSku);
    
    if (!productAsBytes || productAsBytes.toString().length <= 0) {
      throw new Error(merchantSku + ' does not exist: ');
    }
    console.log(productAsBytes.toString());
    return productAsBytes;
  }


  // ===== Example: Ad hoc rich query ========================================================
  // queryMarbles uses a query string to perform a query for marbles.
  // Query string matching state database syntax is passed in and executed as is.
  // Supports ad hoc queries that can be defined at runtime by the client.
  // If this is not desired, follow the queryMarblesForOwner example for parameterized queries.
  // Only available on state databases that support rich query (e.g. CouchDB)
  // =========================================================================================
  async queryProduct(stub, args, thisClass) {
    //   0
    // 'queryString'
    if (args.length < 1) {
      throw new Error('Incorrect number of arguments. Expecting queryString');
    }
    let queryString = args[0];
    if (!queryString) {
      throw new Error('queryString must not be empty');
    }
    let method = thisClass['getQueryResultForQueryString'];
    let queryResults = await method(stub, queryString, thisClass);
    return queryResults;
  }

  async getAllResults(iterator, isHistory) {
    let allResults = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        console.log(res.value.value.toString('utf8'));

        if (isHistory && isHistory === true) {
          jsonRes.TxId = res.value.tx_id;
          jsonRes.Timestamp = res.value.timestamp;
          jsonRes.IsDelete = res.value.is_delete.toString();
          try {
            jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
          } catch (err) {
            console.log(err);
            jsonRes.Value = res.value.value.toString('utf8');
          }
        } else {
          jsonRes.Key = res.value.key;
          try {
            jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
          } catch (err) {
            console.log(err);
            jsonRes.Record = res.value.value.toString('utf8');
          }
        }
        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return allResults;
      }
    }
  }

  // =========================================================================================
  // getQueryResultForQueryString executes the passed in query string.
  // Result set is built and returned as a byte array containing the JSON results.
  // =========================================================================================
  async getQueryResultForQueryString(stub, queryString, thisClass) {

    console.info('- getQueryResultForQueryString queryString:\n' + queryString)
    let resultsIterator = await stub.getQueryResult(queryString);
    let method = thisClass['getAllResults'];

    let results = await method(resultsIterator, false);

    return Buffer.from(JSON.stringify(results));
  }


}

module.exports = ProductChainCode;
