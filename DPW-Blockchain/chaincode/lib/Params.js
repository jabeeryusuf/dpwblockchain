
'use strict';

const {Container, Inventory} = require('fc-blockchain-common');
const util = require('util');

class Params {

  constructor() {
    this.params = [];
  }
 
  static start() {
    return new Params();
  }

  optional() {
    this.optional = this.params.length;
    return this;
  }

  //
  // descriptor
  //

  string(name) {
    this.params.push({name: name, type: 'string'});
    return this;
  }

  json(name) {
    this.params.push({name: name, type: 'json'});
    return this;
  }

  id(name) {
    this.params.push({name: name, type: 'id'});
    return this;
  }

  date(name) {
    this.params.push({name: name, type: 'date'});
    return this;
  }


  // Will remove!!!!
  container(name) {
    this.params.push({name: name, type: 'container'});
    return this;
  }
  inventory(name) {
    this.params.push({name: name, type: 'inventory'});
    return this;
  }
  product(name) {
    this.params.push({name: name, type: 'product'});
    return this;
  }

  //
  // unmarshal the params
  //

  unmarshal(args) {

    let params = this.params;
    let optional = this.optional ? this.optional : params.length;
    let umParams = [];

    for (let i = 0; i < params.length; i++) {
      let arg = args[i];
      
      if (!arg) {
        if (i >= optional) {
          break;
        }
        else {
          throw new Error(util.format('Missing %s parameter %s (param[%d])',params[i].type, params[i].name,i));
        }
      }

      let ump;

      switch(params[i].type) {
      case 'string':
      case 'id':
        ump = args[i];
        break;
      case 'json':
        ump = JSON.parse(args[i]);
        break;
      case 'date':
        ump = new Date(args[i]);
        break;
      default:
        throw new Error('internal error, unknown type: ' + params[i].type );
      }

      umParams.push(ump);
    }

    return umParams;
  }

}

module.exports = Params;