'use strict';


const shim = require('fabric-shim');
const util = require('util');
const Params = require('./Params'); 
const logger = require('debug-logger')('CC:CcTransactionClient');
const {Container,Schema} = require('fc-blockchain-common');
const _ = require('lodash');

class CcTransactionClient {

  constructor(stub) {
    this.stub = stub;

    this.fcSchema = new Schema();
  }

  // Containers
  async addContainer(timestamp, type, ref, parent) {
    let stub = this.stub;
    
    // Load the parentId...
    let parentId;
    if (parent) {
      parentId = await parent.getId();     
    }
    logger.debug('parentId = %s',parentId);

    let container = new Container(type,ref,parentId);
    container.setContext(this);
    
    let id = await container.getId();


    let entry = container.toStorage();
    entry['docType'] = 'containers';

    let b = Buffer.from(JSON.stringify(entry));
    //let id = await container.getId();
    await stub.putState(id, b);
    
    return container;
  }

  async getContainer(id) {
    logger.debug('getContainer: %s',id);
    let entry = await this.stub.getState(id);
    
    if (!entry || entry.toString().length <= 0) {
      throw new Error(id + ' does not exist: ');

    }
    
    let storage = JSON.parse(entry.toString());
    
    logger.debug("getEntry: %j",storage);
    let container = Container.fromStorage(storage);
    container.setContext(this);
    logger.debug(container);

    return container;
  }

  async query(args) {
    let queryObj = sqltomango.parse(args);
    let queryString = JSON.stringify(queryObj);
    console.log(queryString);
    let isHistory = false;   
    let iterator = await this.stub.getQueryResult(queryString);
    
    let results = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        
        if (isHistory && isHistory === true) {
          jsonRes.TxId = res.value.tx_id;
          jsonRes.Timestamp = res.value.timestamp;
          jsonRes.IsDelete = res.value.is_delete.toString();
          try {
            jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
          } catch (err) {
            console.log(err);
            jsonRes.Value = res.value.value.toString('utf8');
          }
        } else {
          jsonRes.Key = res.value.key;
          try {
            jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
          } catch (err) {
            console.log(err);
            jsonRes.Record = res.value.value.toString('utf8');
          }
        }
        results.push(jsonRes);
      }

      if (res.done) {
        console.log('end of data');
        await iterator.close();
        //console.log(results);
        console.log(queryString);
        return results;
      }
    }
  }




  // 
  // Sync API
  //
  

  async writeSyncData(table, rows) {
    
    logger.info('writeSyncData: writing %i rows to %s', rows.length, table);

    for (let i in rows) {

      let row = rows[i];
      //console.log(row);
      // Couchdb uses this tag
      row['docType'] = table;

      // create the couchdb storage id based on the schema
      let key = this.fcSchema.getStorageKey(table,row);

      //console.log('key = ' + key);
      //console.log('row %i:',i);
      //logger.debug('+putState');
      let entry = Buffer.from(JSON.stringify(row));
      await this.stub.putState(key, entry);
      //logger.debug('-putState');

    }
  }




/*   
  async _getContainerHolder(container) {
    let entry = _getEntryByKey(container.getKey());
    let entry = _getEntryByKey(entry.holder);
    
    delete entry.holder;
    delete entry.doctType;

    return Container.fromObject(entry);
  }





  // TODO: Add checking to make sure container is present?????
  async updateContainerHolder(stub,args) {
    const [timestamp, container, holder] = Params.start().timestamp('ts').container('c').container('h').unmarshal(args);
    logger.debug('updateContainerHolder: %j, %j',container, holder);

    // Create the entry 
    let entry = Object.assign({docType:'container'},container);
    entry.holder = holder.getId();
    logger.debug('writing entry: %j',entry);
    let b = Buffer.from(JSON.stringify(entry));

    await stub.putState(container.getKey(), b);
  }


  // only valid for area or shipping container
  async updateContainerInventory(stub, args) {
    const [timestamp, container, inventory] = Params.start().timestamp('ts').container('c').inventory('i').unmarshal(args);

    let items = inventory.items;
    let ckey = container.getKey();
    
    for (let i in items) {
      logger.debug("item[%i]: %j",i,items[i])
      // todo, can do in parallel
      let key = ckey + ':inv&' + items[i].getKey();
      let entry = items[i].toStorageRow();
      entry.container = ckey;

      let b = Buffer.from(JSON.stringify(entry));
      
      await stub.putState(key,b);
    }

    debug("updateContainerInventory: %j, %j",container,inventory);
  }

 */





};

module.exports = CcTransactionClient;
