'use strict';


const shim = require('fabric-shim');
const util = require('util');
const FcChaincodeClient = require('./lib/CcTransactionClient.js');
const Params = require('./lib/Params')
const logger = require('debug-logger')('CC:index');

class MainChaincode {

  constructor() {

  }

  // The Init method is called when the Smart Contract 'fabcar' is instantiated by the blockchain network
  // Best practice is to have any Ledger initialization in separate function -- see initLedger()
  async Init(stub) {
//    this.container = new ContainerChaincode();

    console.info('=========== Instantiated the freight-chain chaincode ===========');
    return shim.success();
  }

  // The Invoke method is called as a result of an application request to run the Smart Contract
  // 'fabcar'. The calling application program has also specified the particular smart contract
  // function to be called, with arguments

  async Invoke(stub) {

    try {
      console.log('Transaction ID: ' + stub.getTxID());
      //console.info('Args: %j', stub.getArgs()));
      //console.log(stub);

      let ret = stub.getFunctionAndParameters();
      console.info(ret);
   
      let client = new FcChaincodeClient(stub);
      let method = this[ret.fcn];
      if (!method) {
        console.log('no function of name:' + ret.fcn + ' found');
        throw new Error('Received unknown function ' + ret.fcn + ' invocation');
      }

      let jsonPayload = await method(client, ret.params, this);
      let payload;
      if (jsonPayload) {
        logger.debug('json return = %j', jsonPayload);
        let strPayload = JSON.stringify(jsonPayload);
        payload = Buffer.from(strPayload);
        
        //console.log('payload = %s',payload.toString());
      }
      //console.log("PAYLOAD: %j", payload);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }


  //
  // Test Functions: for setup help
  //

  async testInvoke(client, args) {
    let [foo,bar,car] = Params.start().string("foo").id("bar").id("car").unmarshal(args);
    console.log("TEST INVOKE CALLED: %s, %i, %i",foo,bar,car);
  }

  async testQuery(client,args) {
    console.log('args = %j',args);
    let [foo] = Params.start().string("foo").unmarshal(args);

    console.log("TEST QUERY: %s",foo);
    return {"meaning":"42"};
  }

  async query(client, args) {
    let sql = "";
    for (let i in args) {
      sql += args[i];
    }
    let str = await client.query(sql);
    return str;
  }

  //
  // Container Functions
  //
  
  async addContainer(client,args) { 
    const [timestamp,type,ref,parentId] = Params.start().date('timestamp').string('type').string('ref').optional().id('parentId').unmarshal(args);
    
    let parent; 
    if (parentId) {
      logger.debug('parent-id = %s',parentId );
      parent = await client.getContainer(parentId);
      logger.debug(parent);
      parent.setContext(client);
    }
    
    let container = await client.addContainer(timestamp, type, ref, parent); 
    return container.toStorage();
  }





  //async updateContainerHolder(stub,args) { await container.updateContainerHolder(stub,args); }
  //async updateContainerInventory(stub,args) { await container.updateContainerInventory(stub,args); }

  //
  // Sync data
  //
  
  async writeSyncData(client, args) {
    const [table, rows] = Params.start().string('table').json('rows').unmarshal(args);
    await client.writeSyncData(table,rows);
  }


}

shim.start(new MainChaincode());
