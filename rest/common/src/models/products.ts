export class Product {
  orgId: string;
  productId: string;

  title: string;
  description: string;
  type: string;           // department, clasification,etc, defined by org

  gtin: string;

  imageLink: string;

}

export class ProductsResponse {

  product?: Product;
  products?: Product[];

  // make generic?
  metadata?: {
    nextCursor?: string;
  }

  constructor(status: string, products: Product[], metaData: string) {
    this.products = products;
  }
}

export interface IProductsService {

  search(query: string, type?: string, cursor?: string, limit?: number): Promise<ProductsResponse>;
}
