
export enum AssetType {
  GROUP = "group",
  VENDOR = "vendor",
  USER = "user",
  SKU = "sku",
  PARTNER = "partner",
  PURCHASE_ORDER = "purchase-order",
}




export class Asset {

  public type: AssetType;
  public id: string;

  constructor(type: AssetType, id: string) {
    this.type = type;
    this.id = id;
  }

}
