import { Asset } from "./Asset";

export enum CasePriority {
  HIGH = "High",
  MEDIUM = "Medium",
  LOW = "Low",
}

export enum CaseStatus {
  NEW = "new",
  PENDING = "pending",
  IN_PROGRESS = "in-progress",
  CLOSED = "closed",
}

export class CaseType {
  public caseTypeId: string;
  public orgId: string;
  public name: string;
  public assetType: string;
  public assetId: string;
  public messageTemplateId: string;
}

export class Case {

  public caseId: string;           // null on creation
  public caseTypeId: string;       // type_id

  public subject: string;          // subject of the case
  public description: string;
  public orgId: string;            // org that owns the case
  public priority: CasePriority;
  public status: CaseStatus;

  public ownerUserId: string;      // Who owns the case
  public assignedEntityId: string;  // Group or user that the case is assigned to
  public ccGroupId: string[];      // User or group to be cced

  public assets: Asset[];          // asssets associated with the case

  public createTime: Date;
}

export interface ICaseTypesService {
  list(orgId: string): Promise<CaseType[]>;
  create(type: CaseType): any;
  update(type: CaseType): any;
}

export interface ICasesService {
  create(c: Case): Promise<Case>;
  update(c: Case): Promise<Case>;
  members(caseId: string): Promise<any>;
  list(orgId: string): Promise<Case[]>;
}
