
export enum MethodOfTransport {
  AIR = "air",
  SEA = "sea",
  RAIL = "rail",
}

export class Carrier {
  public carrierId: string;
  public name: string;
}

export interface ICarriersService {
  info(carrierId: string): Promise<Carrier>;
  find(alias: string): Promise<string>;
}
