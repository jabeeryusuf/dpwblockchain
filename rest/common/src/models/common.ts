import { JsonConverter, JsonCustomConvert, JsonObject, JsonProperty, Any } from "json2typescript";

@JsonConverter
export class DateConverter implements JsonCustomConvert<Date> {
  public serialize(date: Date): any {
    return date === null ? null : date.toISOString();
  }

  public deserialize(date: any): Date {
    return date === null ? null : new Date(date);
  }
}

export enum ResponseStatus {
  OK = "ok",
  NO_RESULTS = "no-results",
  RESULTS_TOO_LARGE = "results-too-large",
  INTERNAL_ERROR = "internal-error",
  INVALID_INPUT = "invalid-input",
  BAD_REQUEST = "bad-request",
}

@JsonObject
export class Response {

  @JsonProperty("status", String)
  public status: ResponseStatus = ResponseStatus.OK;

  @JsonProperty("metadata", Any, true)
  public metadata: any = undefined;
}