import { JsonProperty, JsonObject } from "json2typescript";
import { Response } from "./common";

export enum ContainerType {
  WAREHOUSE = "warehouse",
  PORT = "port",
  AREA = "area",
  TRANSIT = "transit",
  TRUCK = "truck",
  SHIP = "ship",
  TRAIN = "train",
  SHIPPING_CONTAINER = "shipping-container",
}

export enum VesselType {
  CONTAINER_SHIP = "CONTAINER SHIP",

}

@JsonObject("Building")
export class Building {
  
  @JsonProperty("containerId", String)
  public containerId: string = null;

  @JsonProperty("name", String)
  public name: string = null;

  @JsonProperty("lat", Number)
  public lat: number = 0;
  
  @JsonProperty("lng", Number)
  public lng: number = 0;

  public constructor(init?: Partial<Building>) {
    Object.assign(this, init);
  }
}

@JsonObject("Vessel")
export class Vessel {

  @JsonProperty("vesselId",String)
  public vesselId: string = null; // Alias for MMSI
  
  @JsonProperty("name",String)
  public name: string = null;

  @JsonProperty("mmsi", String)
  public mmsi: string = null;
  
  @JsonProperty("imo", String)
  public imo: string = null;

  @JsonProperty("flag", String)
  public flag: string = null;

  @JsonProperty("type", String)
  public type: string = null;

  public constructor(init?: Partial<Vessel>) {
    Object.assign(this, init);
  }
}

@JsonObject("Container")
export class Container {

  @JsonProperty("containerId", String)
  public containerId: string;
  
  @JsonProperty("type", String)
  public type: ContainerType;

  @JsonProperty("ref", String)
  public ref: string;

  @JsonProperty("parentContainerId", String, true)
  public parentContainerId: string;

  public constructor(init?: Partial<Container>) {
    Object.assign(this, init);
  }
}

@JsonObject("ContainersResponse")
export class ContainersResponse extends Response {

  @JsonProperty("vessel", Vessel, true)
  vessel?: Vessel = undefined;
  
  @JsonProperty("building", Building, true)
  building?: Building = undefined;

  public constructor(init?: Partial<ContainersResponse>) {
    super()
    Object.assign(this, init);
  }
}



export interface IContainersService {
  getVesselTrackings(): Promise<any>;
  updateVehicleLocation(doc: any): Promise<any>;
  getInventoryLocations(organizationId: string): Promise<any>;
  getProductLocations(productId: string, user: any): Promise<any>;
  queryVehicleLocations(whereClause: any, whereParams: any, groupBy: any, orderBy: any): Promise<any>;
  getNames(): Promise<any>;
  info(containerId: any): Promise<Container>;
  queryInventory(whereClause: string, whereParams: any, groupBy: string[], vorderBy: any): Promise<any>;
}
