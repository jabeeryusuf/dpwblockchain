export class MessageTemplate {
  public messageTemplateId: string;
  public orgId: string;
  public name: string;
  public messageTemplate: string;
  public subjectTemplate: string;
}

export interface ITemplatesService {
  list(orgId: string): Promise<MessageTemplate[]>;
  update(template: MessageTemplate): Promise<any>;
}
