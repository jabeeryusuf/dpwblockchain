

export enum EntityType {
  USER = "user",
  GROUP = "group",
  VENDOR = "vendor",
};

export class Entity {
  public type: EntityType;    // type of entity
  public entityId: string;   // uuid
}