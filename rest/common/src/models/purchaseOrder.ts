import { JsonObject, JsonProperty  } from "json2typescript";
import { DateConverter, Response } from "./common";

@JsonObject("purhcaseOrderItem")
export class PurchaseOrderItem {

  @JsonProperty("purchaseOrderItemRef",String, true)
  public purchaseOrderItemRef: string = null;

  // Info
  @JsonProperty("countryOfOrigin", String, true)
  public countryOfOrigin: string = null;

  @JsonProperty("productId", String, true)
  public productId: string = null;

  @JsonProperty("status", String, true)
  public status: string = null;



  // Quantities
  @JsonProperty("quantity", Number)
  public quantity: number = null;

  @JsonProperty("lot", Number)
    public lot: number = null;

 @JsonProperty("departmentid", Number)
 public departmentid: number = null;


 @JsonProperty("eta", DateConverter, true)
 public eta: Date = null;

  @JsonProperty("receivedQuantity", Number, true)
  public receivedQuantity: number = null;



  // Dates from ANKO, need to evaluate
  @JsonProperty("checkReleaseDate", DateConverter, true)
  public checkReleaseDate: Date = null;

  @JsonProperty("checkReleaseStatus", String, true)
  public checkReleaseStatus: string = null;

  @JsonProperty("factoryBookingDate", DateConverter, true)
  public factoryBookingDate: Date = null;

  @JsonProperty("approxRecvDate", DateConverter, true)
  public approxRecvDate: Date = null;

}

@JsonObject("PurchaseOrder")
export class PurchaseOrder {

  @JsonProperty("purchaseOrderId", String, true)
  public purchaseOrderId: string = null;

  @JsonProperty("createDate", DateConverter, true)
  public createDate: string = null;

  @JsonProperty("items", [PurchaseOrderItem])
  public items: PurchaseOrderItem[] = null;
}

@JsonObject("PurchaseOrdersResponse")
export class PurchaseOrdersResponse extends Response {

  @JsonProperty("purchaseOrder", PurchaseOrder)
  public purchaseOrder: PurchaseOrder = null;

  @JsonProperty("purchaseOrders", [PurchaseOrder])
  public purchaseOrders: PurchaseOrder[] = null;

  @JsonProperty("metadata", {})
  public metadata: object = {};

  public constructor(init?: Partial<PurchaseOrdersResponse>) {
    super();
    Object.assign(this, init);
  }

}

export interface IPurchaseOrdersService {

  info(purchaseOrderId: string): Promise<PurchaseOrdersResponse>;

  query(whereClause: string, whereParams: string[], groupBy: string[], orderBy: string[]): Promise<any>;
  queryItems(whereClause: string, whereParams: string[], groupBy: string[],orderBy: string[]): Promise<any>;

  queryMilestoneExceptions(orgId: string): Promise<any>;
  queryMilestones(orgId: string): Promise<any>;

  getPurchaseOrderIDs(whereClause: string): Promise<any>;
  queryManufacturers(): Promise<any>;
}

export interface IPurchaseOrderItemsService {
  create(Obj: any): Promise<any>;
}
