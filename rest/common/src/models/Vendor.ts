export class Vendor {
  public vendorId: string;
  public orgId: string;
  public name: string;
  public contactGroupId: string;
}

export interface IVendorsService {
  list(): Promise<Vendor[]>;
  info(vendorId: string): Promise<Vendor>;
}