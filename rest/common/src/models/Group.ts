
export enum GroupType {
  ORG = "org",
  CASE = "case",
  USER_CREATED = "user_created",
}

export class Group {
  public groupId: string;
  public orgId: string;
  public name: string;
  public type: string;
}
