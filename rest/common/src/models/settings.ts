export class Setting {
  public orgId: string;
  public key: string;
  public value: string;
}