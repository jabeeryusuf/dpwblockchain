import {Setting} from "./settings";

export class Org {
  public orgId: string;
  public name: string;
  public type: string;

  // temp: use the db name for backwards compat
  // tslint:disable-next-line: variable-name
  public org_id: string;

  constructor(orgId: string, name: string, type: string) {
    this.orgId = orgId;
    this.org_id = orgId;
    this.name = name;
    this.type = type;
  }
}

export interface IOrgsService {
  info(orgId: string): Promise<Org>;
  create(org: Org): Promise<Org>;
  getSetting(orgId: string, key: string): Promise<Setting>;
  updateSetting(orgSetting: Setting): Promise<Setting>;
}
