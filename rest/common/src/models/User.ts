
export class User {
  public userId: string;
  public orgId: string;
  public email: string;
  public name: string;

  constructor(userId : string, orgId: string, email:string,name:string ) {
    this.userId = userId;
    this.orgId = orgId;
    this.email = email;
    this.name = name;
  }
}
