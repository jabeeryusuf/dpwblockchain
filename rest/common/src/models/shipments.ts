import { JsonObject, JsonProperty } from "json2typescript";
import { Tracking } from "./trackings";
import { DateConverter, Response } from "./common";

export enum ShipmentStatus {
  CREATED = "created",
  IN_TRANSIT = "in-transit",
  CLOSED = "closed",
  COMPLETE = "complete",
}

@JsonObject("ShipmentItem")
export class ShipmentItem {

  @JsonProperty("orgId", String)
  public orgId: string = null;

  @JsonProperty("shipmentId", String, true)
  public shipmentId?: string = null;

  @JsonProperty("productId", String)
  public productId: string = null;

  @JsonProperty("productType", String, true)
  public productType: string = null;

  @JsonProperty("shippingContainerId", String, true)
  public shippingContainerId: string = null;

  @JsonProperty("quantity", Number, true)
  public quantity: number = null;

  // OPtional associations
  @JsonProperty("purchaseOrderId", String, true)
  public purchaseOrderId: string = null;

  @JsonProperty("purchaseOrderItemId", String, true)
  public purchaseOrderItemId: string = null;

  @JsonProperty("hbl", String, true)
  public hbl: string = null;

  @JsonProperty("lot", String, true)
  public lot: string = null;

  // Need to reconcile from ANKO!!!!!
  @JsonProperty("deliveryDate", DateConverter, true)
  public deliveryDate: Date = null;

  @JsonProperty("allocationDate", DateConverter, true)
  public allocationDate: Date = null;
}

@JsonObject("ShipmentSearchMin")
export class ShipmentSearchMin {

  @JsonProperty("id", String, true)
  public id: string = null;
  
  @JsonProperty("name", String)
  public name: string = null;

  @JsonProperty("searchtype", String)
  public searchtype: string = null;
}

@JsonObject("Shipment")
export class Shipment {

  @JsonProperty("orgId", String)
  public orgId: string = null;

  // Not passed in for create
  @JsonProperty("shipmentId", String, true)
  public shipmentId: string = null;

  @JsonProperty("shipmentRef", String)
  public shipmentRef: string = null;

  // Status
  @JsonProperty("status", String)
  public status: ShipmentStatus = null;

  // Source and Destination
  @JsonProperty("sourceContainerId", String)
  public sourceContainerId: string = null;

  @JsonProperty("destinationContainerId", String)
  public destinationContainerId: string = null;

  // Items
  @JsonProperty("items", [ShipmentItem], true)
  public items: ShipmentItem[] = null;


  // Tracking details
  @JsonProperty("trackings", [Tracking])
  public trackings: Tracking[] = null;

  @JsonProperty("transportContainerId", String, true)
  public transportContainerId: string = null;

  @JsonProperty("etd", DateConverter, true)
  public etd: Date = null;

  @JsonProperty("atd", DateConverter, true)
  public atd: Date = null;

  @JsonProperty("eta", DateConverter, true)
  public eta: Date = null;

  @JsonProperty("ata", DateConverter, true)
  public ata: Date = null;

  @JsonProperty("originalEta", DateConverter, true)
  public originalEta: Date = null;

  // OPTIONAL SHIPMENT INFO
  @JsonProperty("bol", String, true)
  public bol: string = null;

  @JsonProperty("gateout", DateConverter, true)
  public gateout: Date = null;

  @JsonProperty("expected_eta", DateConverter, true)
  public expected_eta: Date = null;

  @JsonProperty("carrier", String, true)
  public carrier: string = null;

  @JsonProperty("vesselName", String, true)
  public vesselName: string = null;

  @JsonProperty("shipType", String, true)
  public shipType: string = null;

  @JsonProperty("methodOfTransport", String, true)
  public methodOfTransport: string = null;

  @JsonProperty("updateTime", DateConverter)
  public updateTime: Date = null;

  @JsonProperty("voyage", String, true)
  public voyage: string = null;

  // for now.
}

@JsonObject("ShipmentsResponse")
export class ShipmentsResponse extends Response {

  @JsonProperty("shipments", [Shipment],true)
  public shipments?: Shipment[] = null;

  @JsonProperty("shipment", Shipment, true)
  public shipment?: Shipment = null;

  public constructor(init?: Partial<ShipmentsResponse>) {
    super();
    Object.assign(this, init);
  }
}

@JsonObject("ShipmentFilterParams")
export class ShipmentFilterParams {

  @JsonProperty("shipmentStatus", String, true)
  public shipmentStatus: string = null;

  @JsonProperty("productType", String, true)
  public productType: string = null;

  @JsonProperty("productId", String, true)
  public productId: string = null;

  @JsonProperty("destinationContainerId", String, true)
  public destinationContainerId: string = null;

  @JsonProperty("shippingContainerId", String, true)
  public shippingContainerId: string = null;
}

export class ShipmentException {

  public shipment: Shipment;
  public status: string;
  public expectedTime: Date;
  public actualTime: Date;
  public lastEvent: object;
}

export interface IShipmentsService {
  create(shipment: Shipment, fileId?: string): Promise<any>;
  update(shipment: Shipment, fileId?: string): Promise<any>;

  info(shipmentId: string): Promise<ShipmentsResponse>;

  queryShipments(whereClause: string, whereParams: string[], groupBy: string[], orderBy: string[]): Promise<any>;
  queryShipmentID(whereClause: string, whereParams: string[], type: string): Promise<any>;
  queryShipHistory(shipmentId: string): Promise<any>;
  queryCurrentShipments(orgID: string): Promise<any>;
  queryPastShipments(orgID: string): Promise<any>;
  queryLateUploadShipments(orgID: string): Promise<any>;
  queryPOsOnShipment(orgID: string, shipmentID: string): Promise<any>;
  queryShipmentTrackingExist(trackingId: any): Promise<any>;
  getDestinationContainers(orgId: any): Promise<any>;
  queryShipmentItems(whereClause: string, whereParams: string[], groupBy: string[], orderBy: string[]): Promise<any>;
}
