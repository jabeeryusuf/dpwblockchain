import { JsonObject, JsonProperty } from "json2typescript";
import { DateConverter, Response } from "./common";

export enum TrackingType {
  BOL = "bol",
  BOOKING = "booking",
  CONTAINER = "container",
  BOL_CONTAINER = "bol-container",
}

export enum TrackingStatus {

  // Actively tracking and looking for data
  TRACKING = "tracking",

  // Awaiting input from the user
  PAUSED = "paused",

  // Closed without success
  CLOSED = "closed",

  // Tracking is complete with success
  COMPLETE = "complete",
}


@JsonObject("TrackingEvent")
export class TrackingEvent {

  @JsonProperty("date", DateConverter)
  public date: Date = null;

  @JsonProperty("status", String, true)
  public status: string = null;

  @JsonProperty("notes", String, true)
  public notes: string = null;

  @JsonProperty("locationContainerId", String, true)
  public locationContainerId: string = null;

  @JsonProperty("transportContainerId", String, true)
  public transportContainerId: string = null;

  @JsonProperty("voyageId", String, true)
  public voyageId: string = null;

  public constructor(init?: Partial<TrackingEvent>) {
    Object.assign(this, init);
  }
}

@JsonObject("Tracking")
export class Tracking {

  // Ids
  @JsonProperty("trackingId", String, true)
  public trackingId: string = null;

  @JsonProperty("shipmentId", String, true)
  public shipmentId: string = null;

  // State of the tracking 
  @JsonProperty("status", String, true)
  public status: TrackingStatus = null;

  // External information to connect with the 3rd party provider
  @JsonProperty("carrierId", String)
  public carrierId: string = null;

  @JsonProperty("type", String)
  public type: TrackingType = null;

  @JsonProperty("ref", String)
  public ref: string = null;


  // Detailed tracking events
  @JsonProperty("events", [TrackingEvent], true)
  public events: TrackingEvent[] = null;

  public constructor(init?: Partial<Tracking>) {
    Object.assign(this, init);
  }
}


@JsonObject("TrackingsResponse")
export class TrackingsResponse extends Response {

  @JsonProperty("trackings", [Tracking],true)
  public trackings?: Tracking[] = null;

  @JsonProperty("tracking", Tracking,true)
  public tracking?: Tracking = null;

  public constructor(init?: Partial<TrackingsResponse>) {
    super();
    Object.assign(this, init);
  }
}

export interface ITrackingsListOptions {
  shipmentId?: string,
  shipmentRef?: string,
  orgId?: string,
  carrierId?: string,
  status?: TrackingStatus,
  trackingStatus? : TrackingStatus,
}

export interface ITrackingsService {

  info(trackingId: string): Promise<TrackingsResponse>;
  list(options: ITrackingsListOptions): Promise<TrackingsResponse>;

  create(tracking: Tracking): Promise<TrackingsResponse>;
  update(tracking: Tracking): Promise<TrackingsResponse>;
  delete(trackingId: string): void;
}
