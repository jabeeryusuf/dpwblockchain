import { JsonObject, JsonProperty } from "json2typescript";
import { Response } from "./common";

export class BuildingFeed {
  public name: string;
  public address: string;
  public lat: number;
  public lng: number;
  public type: string;
  public code: any;
  public timeZoneId: string;
  public containerId: any;
  public ref: any;
  public error: string;
}

export class ProductFeed {
  public name: string;
  public sku: string;
  public apn: string;
  public department: string;
  public error: string;
}

export class ShipmentFeed {
  public shipmentRef: string;
  public carrier: number;
  public error: string;
}

@JsonObject("FeedsResponse")
export class FeedsResponse extends Response {

  @JsonProperty("buildingFeeds", [BuildingFeed])
  public buildingFeeds: BuildingFeed[] = null;

  @JsonProperty("shipmentFeeds", [ShipmentFeed])
  public shipmentFeeds: ShipmentFeed[] = null;

  @JsonProperty("productFeeds", [ProductFeed])
  public productFeeds: ProductFeed[] = null;

  @JsonProperty("buildingFeed", BuildingFeed)
  public buildingFeed: BuildingFeed = null;

  @JsonProperty("shipmentFeed", ShipmentFeed)
  public shipmentFeed: ShipmentFeed[] = null;

  @JsonProperty("productFeed", ProductFeed)
  public productFeed: ProductFeed[] = null;

  @JsonProperty("errorMessage", String)
  public errorMessage: string = null;

  @JsonProperty("metadata", {})
  public metadata: object = {};

  public constructor(init?: Partial<FeedsResponse>) {
    super();
    Object.assign(this, init);
  }
}

export interface IFeedsService {

  validateFeedsFile(params: any): Promise<FeedsResponse>;
  addFeeds(params: any): Promise<FeedsResponse>;
}
