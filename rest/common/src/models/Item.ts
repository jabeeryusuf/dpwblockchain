export class Item {
  public containerId: string;
  public purchaseOrderId: string;
  public shipmentId: string;
  public sku: string;

  constructor(containerId:string, purchaseOrderId:string, shipmentId:string, sku:string) {
    this.containerId = containerId;
    this.purchaseOrderId = purchaseOrderId;
    this.shipmentId = shipmentId;
    this.sku = sku;
  }

}
