"use strict";

import ClientOAuth2 = require("client-oauth2");
// import ClientOAuth2 from "client-oauth2";
import moment = require("moment");
import { User } from "tw-api-common";
import { createApi  } from "./_GeneratedOAS3.js";

type Oas = ReturnType<typeof createApi>;

// Apis
import { CarriersClient } from "./CarriersClient";
import { CasesClient } from "./CasesClient";
import { CaseTypesClient } from "./CaseTypesClient";
import { ContainersClient } from "./ContainersClient";
import { ConversationsClient } from "./ConversationsClient";
import { GroupsClient } from "./GroupsClient";
import { OAuthHandler } from "./OAuthHandler";
import { OrgsClient } from "./OrgsClient";
import { ProductsClient } from "./ProductsClient";
import { PurchaseOrdersClient} from "./PurchaseOrdersClient";
import { ShipmentsClient } from "./ShipmentsClient";
import { TemplatesClient } from "./TemplatesClient";
import { TrackingsClient } from "./TrackingsClient";
import { UsersClient } from "./UsersClient";
import { VendorsClient } from "./VendorsClient";
import { VesselsClient } from "./VesselsClient";

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;

function convertDate(key: string, value: string) {

  if (typeof (value) === "string" && dateFormat.test(value)) {
    return new Date(value);
  }
  return value;
}

// var storageToken = null;
export class FcFetchClient  {

  public apiUrl: string;
  public apiEndpoint: string;
  public api3: Oas;
  public oauth: ClientOAuth2;
  public token: any;

  public carriersClient: CarriersClient;
  public casesClient: CasesClient;
  public caseTypesClient: CaseTypesClient;
  public conversationsClient: ConversationsClient;
  public orgsClient: OrgsClient;
  public usersClient: UsersClient;
  public groupsClient: GroupsClient;
  public vendorsClient: VendorsClient;
  public templatesClient: TemplatesClient;
  public trackingsClient: TrackingsClient;
  public shipmentsClient: ShipmentsClient;
  public containersClient: ContainersClient;
  public purchaseOrdersClient: PurchaseOrdersClient;
  public productsClient: ProductsClient;
  public vesselsClient: VesselsClient;

  constructor(env: string) {

    let apiUrl;
    // KEEP IN SYNC WITH NODE ENVIRONMENT
    switch (env) {
    case "production":
      apiUrl = "https://api.teuchain.com";
      break;
    case "staging":
      apiUrl = "http://54.154.192.16:5000";
      break;
    case "development":
      apiUrl = "http://localhost:5000";
      break;
    default:
      throw new Error("Must specify a client environment, got " + env);
    }

    this.apiUrl = apiUrl;
    this.apiEndpoint = apiUrl + "/" + "v0.1";
    console.log('using "' + env + '" API endpoint: ' + this.apiEndpoint);

    // raw fetch apis
    this.api3 = createApi({
      cors: true,
      endpoint: this.apiEndpoint,
      securityHandlers: { basicAuth: (h: any) => this.basicAuth(h),
        oAuth: (h: any) => this.basicAuth(h),
      },
    });

    // oauth client
    this.oauth = new ClientOAuth2({
      accessTokenUri: this.apiEndpoint + "/auth/login",
      clientId: "application",
      clientSecret: "notSecret",
      scopes: ["read write admin"],
    });

    // Setup OAUTH
    this.token = null;
    const boundBasicAuth = this.basicAuth.bind(this);
    const oauthHandler = new OAuthHandler(boundBasicAuth);

    this.carriersClient = new CarriersClient(this.apiEndpoint, oauthHandler);
    this.casesClient = new CasesClient(this.apiEndpoint, oauthHandler);
    this.caseTypesClient = new CaseTypesClient(this.apiEndpoint, oauthHandler);
    this.containersClient = new ContainersClient(this.apiEndpoint, oauthHandler);
    this.conversationsClient = new ConversationsClient(this.apiEndpoint, oauthHandler);
    this.groupsClient = new GroupsClient(this.apiEndpoint, oauthHandler);
    this.orgsClient = new OrgsClient(this.apiEndpoint, oauthHandler);
    this.productsClient = new ProductsClient(this.apiEndpoint, oauthHandler);
    this.purchaseOrdersClient = new PurchaseOrdersClient(this.apiEndpoint, oauthHandler);
    this.shipmentsClient = new ShipmentsClient(this.apiEndpoint, oauthHandler);
    this.templatesClient = new TemplatesClient(this.apiEndpoint, oauthHandler);
    this.usersClient = new UsersClient(this.apiEndpoint, oauthHandler);
    this.vendorsClient = new VendorsClient(this.apiEndpoint, oauthHandler);
    this.vesselsClient = new VesselsClient(this.apiEndpoint, oauthHandler);
    this.trackingsClient = new TrackingsClient(this.apiEndpoint, oauthHandler);
  }

  public getCasesApi() {
    return this.casesClient;
  }

  public getConversationApi() {
    return this.conversationsClient;
  }

  public getOrgsApi() {
    return this.orgsClient;
  }

  public getUsersApi() {
    return this.usersClient;
  }

  public getGroupsApi() {
    return this.groupsClient;
  }

  public getApiUrl() {
    return this.apiUrl;
  }

  public getCaseTypesClient() {
    return this.caseTypesClient;
  }

  public getTemplatesClient() {
    return this.templatesClient;
  }

  public getVendorsClient() {
    return this.vendorsClient;
  }

  public getShipmentsClient() {
    console.log("in getShipmentsClient");
    return this.shipmentsClient;
  }

  public getContainersClient() {
    return this.containersClient;
  }
  public getPurchaseOrdersClient() {
    return this.purchaseOrdersClient;
  }

  public getProductsClient() {
    return this.productsClient;
  }
  // Basic auth callback.  Token needs to be set by client.
  public basicAuth = (headers: any) => {
    if (!this.token) {
      return false;
    }

    const basic: any = {body: {}};
    const req = this.token.sign(basic);
    Object.assign(headers, req.headers);
    return true;
  }

  public setAuthToken(t: any) {
    console.log("api:setAuthToken");
    const extra = { user: t.user };
    this.token = this.oauth.createToken(t.accessToken, t.refreshToken, t.tokenType, extra);
    const expires = new Date(t.expires);
    this.token.expiresIn(expires);
  }

  public async refreshToken() {
    console.log("api:refresh");

    this.token = await this.token.refresh() as any;

    // login successful if there's a token in the response.   Convert to our internal form
    const store = {
      accessToken: this.token.accessToken,
      expires: this.token.expires,
      org: this.token.data.org,
      refreshToken: this.token.refreshToken,
      tokenType: this.token.tokenType,
      user: this.token.data.userObject,
    };

    // store user details and jwt token in local storage to keep user logged in between page refreshes
    // let user = {username:username};
    return store;
  }

  public async login(orgId: string, email: string, password: string) {
    const userId = orgId + "&" + email;
    // Throws
    this.token = await this.oauth.owner.getToken(userId, password);

    console.log("token:");
    console.log(this.token);

    // login successful if there's a token in the response.   Convert to our internal form
    const store = {
      accessToken: this.token.accessToken,
      expires: this.token.expiresIn,
      org: this.token.data.org,
      refreshToken: this.token.refreshToken,
      tokenType: this.token.tokenType,
      user: this.token.data.userObject,
    };

    console.log("FCCLient expires");
    console.log(store.expires);

    // store user details and jwt token in local storage to keep user logged in between page refreshes
    return store;
  }

  public getUserFromAuthToken(t: any) {
    const user = Object.create(User.prototype);
    Object.assign(user, { client: this }, t.user);

    return user;
  }

  public async logout() {
    // remove user from local storage to log user out
    this.token = null;
  }

  public async isLoggedIn() {
    return (this.token === null) ? false : true;
  }

//
//
////////// Everything BELOW should be moved to sub modules
////////////////////////////////////////////////////////////////////

  // for the detail location page
  // IMplemented by ShipmentClient
  public async queryShipHistory(shipmentId: string) {
    return await this.shipmentsClient.queryShipHistory(shipmentId);
  }
  public async queryShipmentID(whereClause: string, whereParams: any, type: any) {
    return await this.shipmentsClient.queryShipmentID( whereClause, whereParams, type);
  }
  public async queryCurrentShipments(orgId: any) {
    return await this.shipmentsClient.queryCurrentShipments(orgId);
  }
  public async queryPastShipments(orgId: string) {
    return await this.shipmentsClient.queryPastShipments(orgId);
  }
  public async queryShipment(whereClause: any, whereParams: any, groupBy: any, orderBy: any) {
    return await this.shipmentsClient.queryShipments(whereClause, whereParams, groupBy, orderBy);
  }
  public async queryLateUploadShipments(orgId: string) {
    return await this.shipmentsClient.queryLateUploadShipments(orgId);
  }
  public async queryPOsOnShipment(orgID: string, shipmentID: string) {
    return await this.shipmentsClient.queryPOsOnShipment(orgID, shipmentID);
  }
  public async queryShipmentTrackingExist(trackingId: any) {
    return await this.shipmentsClient.queryShipmentTrackingExist( trackingId );
  }
  public async getDestinationContainers(orgId: string) {
    return await this.shipmentsClient.getDestinationContainers(orgId);
  }
  public async addFeedsActivity(status: string, fileId: string, orgId: string) {
    return await this.shipmentsClient.addFeedsActivity(status, fileId, orgId);
  }

  // Implemented in PurchaseOrdersClient
  public async queryPurchaseOrderItems(whereClause: string, whereParams: any) { return await this.purchaseOrdersClient.queryItems(whereClause, whereParams, [], []); }
  public async queryMilestones(orgId: string) { return await this.purchaseOrdersClient.queryMilestones(orgId); }
  public async queryMilestoneExceptions(orgId: string) { return await this.purchaseOrdersClient.queryMilestoneExceptions(orgId); }
  public async getPurchaseOrderIDs(whereClause: string) { return await this.purchaseOrdersClient.getPurchaseOrderIDs(whereClause); }
  public async queryManufacturers() { return this.purchaseOrdersClient.queryManufacturers(); }

  // Implemented in ContainersClient
  public async queryBuildings(whereClause: string, whereParams: string[], groupBy: string[], orderBy: string[]) { return await this.containersClient.queryBuildings(whereClause, whereParams, groupBy, orderBy); }
  public async getVesselLocations(orgid: string) { return await this.containersClient.getVesselLocations(orgid); }
  public async getSkusForContainer(containerId: string) { return await this.containersClient.getProductsForContainer(containerId); }
  public async getBuilding(containerId: string) { return await this.containersClient.getBuilding(containerId); }

  // Implemented in CasesClient
  public async caseFiles(caseId: string) { return await this.casesClient.files(caseId); }

  public async queryReport(dest: any) {
    const response = await this.api3.queryReport({ dest });
    const jres = await response.json();

    const newData = [];
    for (const obj of jres) {
      const currentObjs = [];
      for (const row of obj.rows) {
        const objStr = JSON.stringify(row);
        const convert = JSON.parse(objStr, convertDate);
        currentObjs.push(convert);
      }
      newData.push(currentObjs);
    }

    return newData;
  }

  public async queryActions(whereClause: string, whereParams: any, groupBy: any, orderBy: any) {
    const response = await this.api3.queryActions({ whereClause, whereParams, groupBy, orderBy });
    const res = await response.json();
    const items = res.items;
    const actions = [];
    for (let i = 0; i < items.length; i++) {
      const A = {};
      if (items[i].request_date) {
        items[i].request_date = moment(items[i].request_date).format("MMMM Do YYYY, h:mm:ss a");
      }
      if (items[i].approval_date) {
        items[i].approval_date = moment(items[i].approval_date).format("MMMM Do YYYY, h:mm:ss a");
      }
      const actionObj = Object.assign(A, { client: this }, items[i]);
      actions.push(actionObj);
    }
    return actions;
  }

  public async createAction(containerId: string, actionType: string, org: string) {
    try {
      const newAction = await this.api3.createAction({ container_id: containerId, actionType, org });
      const res = await newAction.json();
      // res will be false if there is an already open request for this containerID
      if (res === false) {
        throw new Error();
      } else {
        return res;
      }
    } catch (err) {
      return { errorMessage: "Open request already exists on this container" };
    }
  }

  public async updateAction(id: string, actionType: string) {
    try {
      const newUpdate = await this.api3.updateAction({ id, actionType });
      const res = await newUpdate.json();
      return res;
    } catch (err) {
      return { errorMessage: "Changes to action cannot be completed" };
    }
  }

  public async resolveRequest(id: string, resolve: string) {
    try {
      const resolveRequest = await this.api3.resolveRequest({ id, resolve });
      const res = await resolveRequest.json();
      return res;
    } catch (err) {
      return { errorMessage: err };
    }
  }

  public async deleteAction(id: string) {
    try {
      const deleteAction = await this.api3.deleteAction({ id });
      const res = await deleteAction.json();
      return res;
    } catch (err) {
      return { errorMessage: "Changes cannot be completed. Due to: " + err };
    }
  }

  public async submitPull(org: string, service: string, username: string, password: string, pullInterval: any) {
    const res = await this.api3.submitPull({ org, service, username, password, pullInterval });
    return res;
  }

  public async AddFeeds(uploadExcelObj: any) {
    const res = await this.api3.AddFeeds({ uploadExcelObj });
    const response = await res.json();
    if (response.status === "ok") {
      return { successMessage: "Excel Insertion succeeded!", data: response };
    } else {
      return { errorMessage: (response.feeds) ? response.feeds :
                                      "Excel upload failed. Is the sheet formatted correctly" };
    }
  }

  public async uploadFileToS3(file: any) {
    console.log(file);
    const res = await this.api3.uploadFileToS3({ file });
    const response = await res.json();
    if (res.status === 200) {
      return { successMessage: "Excel upload succeeded!" };
    } else {
      return { errorMessage: (response) ? response : "Excel upload failed. Is the sheet formatted correctly" };
    }
  }

  public async ValidateFeedsFile(uploadExcelObj: any) {
    const res = await this.api3.ValidateFeedsFile({ uploadExcelObj });
    const response =  await res.json();
    if (response.status === "ok") {
      return { successMessage: "Excel succesfully validated!", data: response };
    } else {
      return { errorMessage: (response.errorMessage) ? response.errorMessage :
                                        "Excel upload failed. Is the sheet formatted correctly" };
    }
  }

  public async queryExistingServices(whereClause: string) {
    const query = await this.api3.queryExistingServices({ whereClause });
    const res = await query.json();
    return res.rows;
  }

  public async getStuff(sql: string) {
    const query = await this.api3.getStuff({sql});
    const res = await query.json();
    return res;
  }

  public async changeConfigSettings(intervals: any) {
    const query = await this.api3.changeConfigSettings({ intervals });
    const res = await query.json();
    return res;
  }

  public async getUsersInGroups(type: string, org: string, single: any) {
    return await this.groupsClient.getUsersInGroups(type, org, single);
  }

  public async queryFiles(whereClause: string, whereParams: any, orderBy: any, groupBy: any) {
    const files = await this.api3.queryFiles({whereClause, whereParams, groupBy, orderBy});
    const res  = await files.text();
    const fileLink = this.apiUrl + "/v0.1/file/downloadFile/" + res;
    console.log(fileLink);
    return fileLink;
  }

  public async uploadFile(file: any, user_id: string, permission: any) {
    console.log({file, user_id, permission});
    const res = await this.api3.uploadFile({file, user_id, permission});
    return await res.json();
  }
  public async uploadEmailLogo(file: any, org_id: string, key: string) {
    console.log({ file, org_id, key });
    const res = await this.api3.uploadEmailLogo({ file, org_id, key });
    return await res.json();
  }
}
