import * as qs from "qs";

import { Group } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class GroupsClient  {
  private rest: RestClient;
  private baseUrl: string;

  // private oauthHandler: OAuthHandler;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("groups-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/groups";
  }

  public async create(group: Group, emails?: string[], groups?: string[]): Promise<Group> {
    const res: IRestResponse<Group> =
      await this.rest.create<Group>(this.baseUrl + "/create" + "?" + qs.stringify({emails, groups}), group);
    return res.result;
  }

  public async list() {
    const res: IRestResponse<Group []> = await this.rest.get<any>(this.baseUrl + "/list");
    return res.result;
  }

  /**
   * Api call will query all groups not related to cases, a single group, or all vendor groups depending on params
   * @param {string} org
   * @param {string} single optional - uuid of single group if querying for one group
   * @param {string} vendors optional - (true/false) if querying for vendor groups
   */
  public async getUsersInGroups(org: string, single: string, vendors: boolean) {
    const res: IRestResponse<any> =
      await this.rest.get<any>(this.baseUrl + "/getUsersInGroups" + "?" + qs.stringify({ org, single, vendors }));
    return res.result;
  }

  public async update(payload: any) {
    const res: IRestResponse<any> =
      await this.rest.update<any>(this.baseUrl + "/update", payload);
    return res.result;
  }
}
