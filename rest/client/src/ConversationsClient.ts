import * as qs from "qs";

import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class ConversationsClient {
  private rest: RestClient;
  private baseUrl: string;
  private msgUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("conversations-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/conversations";
    this.msgUrl = baseUrl + "/message";
  }

  public async list(userId: string) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/list" + "?" + qs.stringify({userId}) );

    return res.result;
  }

  public async history(conversationId: string) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/history" + "?" + qs.stringify({conversationId}) );

    return res.result;
  }

  public async members(conversationId: string ) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/members" + "?" + qs.stringify({conversationId}) );

    return res.result;
  }

  public async updateConversationPreferences(conversationId: string, userId: string, pref: any) {
    const res: IRestResponse<any> = await this.rest.create<any>(
      this.baseUrl + "/updatePreferences", {conversationId, userId, pref} );

    return res.result;
  }

  // temporary
  public async updateList() {
    const res: IRestResponse<any> = await this.rest.create<any>(this.baseUrl + "/updateList", {});
    return res.result;
  }

  // messages!
  public async saveTemplate(templateObj: any) {
    const res: IRestResponse<any> = await this.rest.create<any>(
      this.msgUrl + "/updateTemplate", templateObj );

    return res.result;
  }

  public async templates(orgId: string) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.msgUrl + "/templates" + "?" + qs.stringify({orgId}) );

    return res.result;
  }

}
