"use strict";
const qs = require("qs");
export function createApi(options: any) {
  const basePath = "";
  const endpoint = options.endpoint || "";
  const cors = !!options.cors;
  const mode = cors ? "cors" : "no-cors";
  const securityHandlers = options.securityHandlers || {};
  const handleSecurity = (security: any, headers: any, params: any, operationId: any) => {
    for (let i = 0, ilen = security.length; i < ilen; i++) {
      const scheme = security[i];
      const schemeParts = Object.keys(scheme);
      for (let j = 0, jlen = schemeParts.length; j < jlen; j++) {
        const schemePart = schemeParts[j];
        const fulfilsSecurityRequirements = securityHandlers[schemePart](
            headers, params, schemePart);
        if (fulfilsSecurityRequirements) {
          return;
        }

      }
    }
    throw new Error("No security scheme was fulfilled by the provided securityHandlers for operation " + operationId);
  };
  const ensureRequiredSecurityHandlersExist = () => {
    const requiredSecurityHandlers = ["basicAuth", "oAuth"];
    for (let i = 0, ilen = requiredSecurityHandlers.length; i < ilen; i++) {
      const requiredSecurityHandler = requiredSecurityHandlers[i];
      if (typeof securityHandlers[requiredSecurityHandler] !== "function") {
        throw new Error('Expected to see a security handler for scheme "' +
            requiredSecurityHandler + '" in options.securityHandlers');
      }
    }
  };
  ensureRequiredSecurityHandlersExist();
  return {
    queryReport(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {

      };
      handleSecurity([{basicAuth: []}]
          , headers, params, "queryReport");
      return fetch(endpoint + basePath + "/report/query" + "?" + qs.stringify(params)

        , {
          method: "GET",
          headers,
          mode,
        });
    },
    getStuff(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {

      };
      handleSecurity([{basicAuth: []}]
          , headers, params, "queryShipLocation");
      return fetch(endpoint + basePath + "/general" + "?" + qs.stringify(params)

        , {
          method: "GET",
          headers,
          mode,
        });
    },
    ValidateFeedsFile(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {
        "content-type": "application/json",

      };
      return fetch(endpoint + basePath + "/feeds/validateFeedsFile"
        , {
          method: "POST",
          headers,
          mode,
          body: JSON.stringify(params),

        });
    },
    queryFiles(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {

      };
      handleSecurity([{basicAuth: []}]
          , headers, params, "queryFiles");
      return fetch(endpoint + basePath + "/feeds/queryFiles" + "?" + qs.stringify(params)

        , {
          method: "GET",
          headers,
          mode,
        });
    },
    AddFeeds(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {
        "content-type": "application/json",

      };
      return fetch(endpoint + basePath + "/feeds/addFeeds"
        , {
          method: "POST",
          headers,
          mode,
          body: JSON.stringify(params),

        });
    },
    queryExistingServices(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {

      };
      handleSecurity([{basicAuth: []}]
          , headers, params, "queryExistingServices");
      return fetch(endpoint + basePath + "/dataPull/existingServices" + "?" + qs.stringify(params)

        , {
          method: "GET",
          headers,
          mode,
        });
    },
    changeConfigSettings(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {
        "content-type": "application/json",

      };
      return fetch(endpoint + basePath + "/config/settings"
        , {
          method: "POST",
          headers,
          mode,
          body: JSON.stringify(params),

        });
    },
    updateAction(parameters:any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      let headers = {

      };
      handleSecurity([{"basicAuth":[]}]
          , headers, params, "updateAction");
      return fetch(endpoint + basePath + "/actions/update" + "?" + qs.stringify(params)

        , {
          method: "POST",
          headers,
          mode,
        });
    },
    queryActions(parameters:any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      let headers = {

      };
      handleSecurity([{"basicAuth":[]}]
          , headers, params, "queryActions");
      return fetch(endpoint + basePath + "/actions/query" + "?" + qs.stringify(params)

        , {
          method: "GET",
          headers,
          mode,
        });
    },
    deleteAction(parameters:any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      let headers = {

      };
      handleSecurity([{"basicAuth":[]}]
          , headers, params, "deleteAction");
      return fetch(endpoint + basePath + "/actions/delete" + "?" + qs.stringify(params)

        , {
          method: "POST",
          headers,
          mode,
        });
    },
    createAction(parameters:any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      let headers = {

      };
      handleSecurity([{"basicAuth":[]}]
          , headers, params, "createAction");
      return fetch(endpoint + basePath + "/actions/create" + "?" + qs.stringify(params)
        , {
          method: "POST",
          headers,
          mode,
        });
    },
    resolveRequest(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {

      };
      handleSecurity([{ basicAuth: [] }]
        , headers, params, "resolveRequest");
      return fetch(endpoint + basePath + "/actions/resolveRequest" + "?" + qs.stringify(params)
        , {
          method: "POST",
          headers,
          mode,
        });
    },
    submitPull(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;
      const headers = {
        "content-type": "application/json",

      };
      return fetch(endpoint + basePath + "/dataPull/submitPull"
        , {
          method: "POST",
          headers,
          mode,
          body: JSON.stringify(params),
        });
    },
    uploadFileToS3(parameters: any) {
      const headers = {
      };
      return fetch(endpoint + basePath + "/container/uploadFileToS3"
        , {
          method: "POST",
          body: parameters.file,
          headers,
          mode,
        });
    },
    uploadFile(parameters: any) {
      // const params =  parameters;
      const params = typeof parameters === "undefined" ? {} : parameters;
	  console.log("before upload");
      console.log(parameters);
      const headers = {

      };
      return fetch(endpoint + basePath + "/file/uploadFile" + "?" + qs.stringify(params)
         , {
           method: "POST",
           body: parameters.file,
           headers,
           mode,
         });
    },
    uploadEmailLogo(parameters: any) {
      const params = typeof parameters === "undefined" ? {} : parameters;

      console.log(parameters);
      const headers = {

      };
      return fetch(endpoint + basePath + "/file/uploadEmailLogo" + "?" + qs.stringify(params)
        , {
          method: "POST",
          body: parameters.file,
          headers,
          mode,
        });
    },
  };
}
