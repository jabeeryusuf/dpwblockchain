import { JsonConvert } from "json2typescript";
import * as qs from "qs";
import { ITrackingsService, Tracking, TrackingsResponse, ITrackingsListOptions } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class TrackingsClient implements ITrackingsService  {
  private rest: RestClient;
  private baseUrl: string;
  private jsonConvert: JsonConvert;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("trackings-client");
    this.baseUrl = baseUrl + "/shipments/trackings";
    this.jsonConvert = new JsonConvert();
  }

  public async info(trackingId: string): Promise<TrackingsResponse> {
    const res: IRestResponse<TrackingsResponse> = await this.rest.get<any>(
      this.baseUrl + "/info" + "?" + qs.stringify({ trackingId }));

    return this.jsonConvert.deserializeObject(res.result, TrackingsResponse);
  }

  public async list(options: ITrackingsListOptions): Promise<TrackingsResponse> {
    const res: IRestResponse<TrackingsResponse> = await this.rest.get<TrackingsResponse>(
      this.baseUrl + "/list" + "?" + qs.stringify({options}));

    return this.jsonConvert.deserializeObject(res.result, TrackingsResponse);
  }

  public async create(st: Tracking): Promise<TrackingsResponse> {
    const res: IRestResponse<TrackingsResponse> = await this.rest.create<any>(
      this.baseUrl + "/create", st);
    return this.jsonConvert.deserializeObject(res.result, TrackingsResponse);
  }

  public async update(st: Tracking): Promise<TrackingsResponse> {
    const res: IRestResponse<TrackingsResponse> = await this.rest.replace<any>(
      this.baseUrl + "/update", st);
    return this.jsonConvert.deserializeObject(res.result, TrackingsResponse);
  }

  public async delete(trackingId: string): Promise<TrackingsResponse> {
    const res: IRestResponse<TrackingsResponse> = await this.rest.del<any>(
      this.baseUrl + "/delete" + "?" + qs.stringify({ trackingId }));
    return this.jsonConvert.deserializeObject(res.result, TrackingsResponse);
  }

}
