import * as qs from "qs";
import { IVendorsService, Vendor } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class VendorsClient implements IVendorsService {

  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("vendors-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/vendors";
  }

  public async list(): Promise<Vendor[]> {
    const res: IRestResponse<any> =
      await this.rest.get<Vendor[]>(this.baseUrl + "/list" );
    return res.result;
  }

  public async info(vendorId: string): Promise<Vendor> {
    const res: IRestResponse<any> =
      await this.rest.get<any>(this.baseUrl + "/info" + "?" + qs.stringify({vendorId}));
    return res.result;
  }

}
