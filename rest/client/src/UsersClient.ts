import { User } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";
import * as qs from 'qs';

export class UsersClient  {
  private rest: RestClient;
  private baseUrl: string;

  // private oauthHandler: OAuthHandler;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("users-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/users";
  }

  public async create(orgId: string, email: string, name: string, password: string, code: string) {
    const res: IRestResponse<User> =
      await this.rest.create<User>(this.baseUrl + "/create", {orgId, email, name, password});
    return res.result;
  }
  public async update(user: User ){
    const res: IRestResponse<User> = await this.rest.update<User>(this.baseUrl + "/update", user);
    return res.result;
  }
  public async list(body:any) {
    const res: IRestResponse<User []> = await this.rest.get<any>(this.baseUrl + "/list" + "?" + qs.stringify(body));
    return res.result;
  }

  public async resetPassword(email: string, orgId: string){
    const res: IRestResponse<any> = await this.rest.update<any>(this.baseUrl + "/resetPassword", {email});
    return res.result;
  }

  public async changePassword(email: string, password: string, code: string) {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/changePassword", {email,password, code});
    return res.result;
  }

  public async addName(emailsAndNames: any) {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/addNamesToUser", {emailsAndNames});
    return res.result;
  }

  public async sendVerificationCode(body: {email: string} ) {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/sendVerificationCode", body);
    return res.result;
  }

  public async verifyEmail(email: string, code: string) {
    const res: IRestResponse<any> =
      await this.rest.update<any>(this.baseUrl + "/verifyEmail", {email,code});
    return res.result;
  }

  public async createInvitations(emails: string[], message: string, senderName: string, senderEmail: string) {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/createInvitations", {emails,message,senderName, senderEmail});
    return res.result;
  }

  public async deleteInvitation(verifyCode: string){
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/deleteInvitation", {verifyCode});
    return res.result;
  }

}
