import { Carrier } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class CarriersClient  {

  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("carriers-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/carriers";
  }

  public async list(): Promise<Carrier[]> {
    const res: IRestResponse<any> =
      await this.rest.get<Carrier[]>(this.baseUrl + "/list" );
    return res.result;
  }
}
