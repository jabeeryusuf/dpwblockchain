import { ITemplatesService, MessageTemplate } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class TemplatesClient implements ITemplatesService {

  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("templates-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/messages/templates";
  }

  public async list(): Promise<MessageTemplate[]> {
    const res: IRestResponse<any> =
      await this.rest.get<MessageTemplate[]>(this.baseUrl + "/list");
    return res.result;
  }

  public async update(template: MessageTemplate): Promise<any> {
    const res: IRestResponse<any> =
      await this.rest.update<any>(this.baseUrl + "/update", template);
    return res.result;
  }

}
