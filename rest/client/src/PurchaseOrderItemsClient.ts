import { IPurchaseOrderItemsService } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class PurchaseOrderItemsClient implements IPurchaseOrderItemsService {
  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("products-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/purchaseOrders/Items";
  }

  public async create(Obj: any): Promise<any>  {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/create", Obj);
    return res.result;
  }
}
