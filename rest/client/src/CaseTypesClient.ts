import { CaseType, ICaseTypesService } from "tw-api-common";
import { IRestResponse, RestClient  } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class CaseTypesClient implements ICaseTypesService {

  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("case-types-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/cases/types";
  }

  public async list(): Promise<CaseType[]> {

    const res: IRestResponse<CaseType[]> = await this.rest.get<CaseType[]>(
      this.baseUrl + "/list");
    return res.result;
  }

  public async create(type: CaseType)  {
    const res: IRestResponse<CaseType> =
      await this.rest.create<CaseType>(this.baseUrl + "/create", type);
    return res;
  }

  public async update(type: CaseType)  {
    const res: IRestResponse<CaseType> =
      await this.rest.update<CaseType>(this.baseUrl + "/update", type);
    return res;
  }

  public async deleteCaseType(caseTypeId: string) {
    const res: IRestResponse<any> =
      await this.rest.update<any>(this.baseUrl + "/delete-case", { caseTypeId });
    return res;
  }

}
