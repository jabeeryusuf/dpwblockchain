import * as qs from "qs";

import { JsonConvert } from "json2typescript";
import { Vessel, ContainersResponse } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class VesselsClient  {
  private rest: RestClient;
  private baseUrl: string;
  private jsonConvert: JsonConvert;

  // private oauthHandler: OAuthHandler;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("vessels-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/containers/vessels";
    this.jsonConvert = new JsonConvert();
  }

  public async info(vesselId: string): Promise<ContainersResponse> {
    const res: IRestResponse<Vessel> =
      await this.rest.get<Vessel>(this.baseUrl + "/info" + "?" + qs.stringify({vesselId}));

    console.log(res.result)
    const cr = this.jsonConvert.deserializeObject(res.result, ContainersResponse);
    return cr;
  }

  public async update(payload: any) {
    const res: IRestResponse<any> =
      await this.rest.update<any>(this.baseUrl + "/update", payload);
    return res.result;
  }
}
