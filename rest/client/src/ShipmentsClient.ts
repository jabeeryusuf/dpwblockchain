import { JsonConvert } from "json2typescript";
import * as qs from "qs";
import { IShipmentsService, Shipment, ShipmentsResponse } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class ShipmentsClient implements IShipmentsService {
    private rest: RestClient;
    private baseUrl: string;
    private jsonConvert: JsonConvert;

    constructor(baseUrl: string, oauthHandler: OAuthHandler) {
        this.rest = new RestClient("shipments-client");
        this.baseUrl = baseUrl + "/shipment";
        this.jsonConvert = new JsonConvert();
    }

    public async create(shipment: Shipment, fileId?: string): Promise<any> {
        const res: IRestResponse<Shipment> = await this.rest.create<Shipment>(this.baseUrl + "/create", shipment);
        return res.result;
    }

    public async newshipment(shipment: Shipment): Promise<any> {
        const res: IRestResponse<Shipment> = await this.rest.create<Shipment>(this.baseUrl + "/newshipment", shipment);
        return res.result;
    }


    public async update(shipment: Shipment): Promise<any> {
        const res: IRestResponse<Shipment> = await this.rest.replace<Shipment>(this.baseUrl + "/update", shipment);
        return res.result;
    }

    public async info(shipmentId: string): Promise<ShipmentsResponse> {
        const res: IRestResponse<Shipment> =
            await this.rest.get<any>(this.baseUrl + "/info" + "?" + qs.stringify({ shipmentId }));

        return this.jsonConvert.deserializeObject(res.result, ShipmentsResponse);
    }

    public async searchShipments(
        orgId: string,
        groupBy: string,
        orderBy: string,
        filterParams?: object,
        searchString?: object,
        limit?: number,
        cursor?: string,
        desc?: boolean):
        Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(

            this.baseUrl + "/search" + "?" + qs.stringify({
                orgId,
                filterParams,
                searchString,
                orderBy,
                groupBy,
                cursor,
                limit,
                desc,
            }));
        return res.result;
    }



    public async queryShipments(
        whereClause?: string, whereParams?: string[], orderBy?: string[], groupBy?: string[]): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/query" + "?" + qs.stringify({ whereClause, whereParams, orderBy, groupBy }));
        return res.result;
    }

    public async queryShipmentID(whereClause: string, whereParams: string[], type: string) {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/shipmentIdQuery" + "?" + qs.stringify({ whereClause, whereParams, type }));
        return res.result;
    }

    public async queryShipHistory(shipmentId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryShipHistory" + "?" + qs.stringify({ shipmentId }));

        const jres = res.result;

        const data = [];
        for (const obj of jres.rows) {
            obj.speed = parseFloat(obj.speed.substring(0, obj.speed.length - 3));
            obj.lat = parseFloat(obj.lat);
            obj.lng = parseFloat(obj.lng);
            obj.last_lat = parseFloat(obj.last_lat);
            obj.last_lng = parseFloat(obj.last_lng);
            obj.next_lat = parseFloat(obj.next_lat);
            obj.next_lng = parseFloat(obj.next_lng);
            data.push(obj);
        }

        return data;
    }

    public async queryCurrentShipments(orgId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryCurrentShipments" + "?" + qs.stringify({ orgId }));
        return res.result;
    }

    public async updatecarrier(shipmentId: string,carrier:string): Promise<any> {
        console.log("updating carrier for shipment!!!!!",shipmentId);
       var obj = {shipmentId:shipmentId,carrier:carrier};
        const res: IRestResponse<any> = await this.rest.create<any>(
            this.baseUrl + "/updatecarrier",obj);
        return res.result;
    }


    public async queryPastShipments(orgId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryPastShipments" + "?" + qs.stringify({ orgId }));
        return res.result;
    }

    public async searchSuggest(orgId: string, searchval: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/searchSuggest" + "?" + qs.stringify({ orgId, searchval }));
        return res.result;
    }

    public async queryLateUploadShipments(orgId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryLateUploadShipments" + "?" + qs.stringify({ orgId }));
        return res.result;
    }

    public async queryPOsOnShipment(orgId: string, shipmentId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryPOsOnShipment" + "?" + qs.stringify({ orgId, shipmentId }));
        return res.result;
    }

    public async queryShipmentTrackingExist(trackingId: any): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryShipmentTrackingExist" + "?" + qs.stringify({ trackingId }));
        return res.result;
    }


    public async getContainersForShipment(shipmentId: string): Promise<any> {
        console.log("this is me", shipmentId);
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/getContainersForShipment" + "?" + qs.stringify({ shipmentId }));
        return res.result;
    }

    public async getDestinationContainers(orgId: any): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/getDestinationContainers" + "?" + qs.stringify({ orgId }));
        return res.result;
    }

    public async getDestinations(orgId?: any, shipmentStatus?: any): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/getDestinations" + "?" + qs.stringify({ orgId, shipmentStatus }));
        return res.result;
    }

    public async getActiveShipmentsTrackings(options?: any): Promise<any> {
        const params = (options) ? "?" + qs.stringify({ options }) : "";
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/getActiveShipmentTrackings" + params);
        return res.result;
    }

    public async queryShipmentItems(
        whereClause: string, whereParams: string[], orderBy: string[], groupBy: string[]): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/queryItems" + "?" + qs.stringify({ whereClause, whereParams, orderBy, groupBy }));
        return res.result;
    }

    public async getExceptions(orgId: any): Promise<any> {
        const params = qs.stringify({ orgId });
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/getExceptions" + "?" + params);
        return res;
    }

    public async addFeedsActivity(status: string, fileId: string, orgId: string): Promise<any> {
        const res: IRestResponse<any> = await this.rest.get<any>(
            this.baseUrl + "/addFeedsActivity" + "?" + qs.stringify({ status, fileId, orgId }));
        return res.result;
    }
}
