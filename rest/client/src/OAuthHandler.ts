import {IHttpClient, IHttpClientResponse, IRequestHandler, IRequestInfo} from "typed-rest-client/Interfaces";

export class OAuthHandler implements IRequestHandler {
  public authFunc: any;

  // TODO: Move auth construction into here!!!
  constructor(authFunc: any) {
    this.authFunc = authFunc;
  }

  // currently implements pre-authorization
  // TODO: support preAuth = false where it hooks on 401
  public prepareRequest(options: any): void {
    this.authFunc(options.headers);
  }

  // This handler cannot handle 401
  public canHandleAuthentication(response: IHttpClientResponse): boolean {
    return false;
  }

  public handleAuthentication(httpClient: IHttpClient, requestInfo: IRequestInfo, objs: any)
  : Promise<IHttpClientResponse> {

    return null;
  }
}
