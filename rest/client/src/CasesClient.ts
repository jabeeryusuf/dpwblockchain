import * as qs from "qs";

import { Case, ICasesService } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class CasesClient implements ICasesService {

  private rest: RestClient;
  private baseUrl: string;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("cases-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/cases";
  }

  public async list(): Promise<Case[]> {
    const res: IRestResponse<any> =
      await this.rest.get<Case[]>(this.baseUrl + "/list" );
    return res.result;
  }

  public async create(c: Case): Promise<Case>  {
    const res: IRestResponse<Case> =
      await this.rest.create<Case>(this.baseUrl + "/create", c);
    return res.result;
  }

  public async update(c: Case): Promise<Case> {
    const res: IRestResponse<Case> =
      await this.rest.update<Case>(this.baseUrl + "/update", c);
    return res.result;
  }

  public async members(caseId: string): Promise<any> {
    const res: IRestResponse<any> =
      await this.rest.get<any>(this.baseUrl + "/members" + "?" + qs.stringify({caseId}));
    return res.result;
  }

  public async info(caseId: string): Promise<Case> {
    const res: IRestResponse<any> =
      await this.rest.get<any>(this.baseUrl + "/info" + "?" + qs.stringify({caseId}));
    return res.result;
  }

  public async addFile(caseId: string, fileId: string) {
    const res: IRestResponse<any> =
      await this.rest.create<any>(this.baseUrl + "/addFile", {caseId, fileId});
    return res.result;
  }


  public async files(caseId: string): Promise<any> {
    const res: IRestResponse<any> =
      await this.rest.get<any>(this.baseUrl + "/files" + "?" + qs.stringify({caseId}));
    return res.result;
  }


}
