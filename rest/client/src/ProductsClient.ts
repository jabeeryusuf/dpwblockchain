import * as qs from "qs";

import { IProductsService, ProductsResponse } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class ProductsClient implements IProductsService {
  private rest: RestClient;
  private baseUrl: string;

  // private oauthHandler: OAuthHandler;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("products-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/products";
  }

  public async info(productId: string): Promise<ProductsResponse> {
    const res: IRestResponse<ProductsResponse> = await this.rest.get<ProductsResponse>(
      this.baseUrl + "/info" + "?" + qs.stringify({productId}));

    return res.result;
  }

  public async list(productIds: string[]): Promise<ProductsResponse> {

    // using post because of large number of params
    const res: IRestResponse<ProductsResponse> = await this.rest.create<ProductsResponse>(
      this.baseUrl + "/list", productIds);

    return res.result;
  }

  public async search(query: string, type?: string, cursor?: string, limit?: number)
  : Promise<ProductsResponse> {

    const res: IRestResponse<ProductsResponse> = await this.rest.get<ProductsResponse>(
      this.baseUrl + "/search" + "?" + qs.stringify({query, type, cursor, limit}));

    return res.result;
  }

  public async listTypes()
  : Promise<string []> {
    const res: IRestResponse<string []> = await this.rest.get<string[]>(
      this.baseUrl + "/listTypes");

    return res.result;
  }

  public async listMissing(orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/listMissing" + "?" + qs.stringify({orgId}));
    return res.result;
  }

  public async addProduct(Obj: any, orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.create<any>(
      this.baseUrl + "/addProduct" + "?" , {Obj, orgId});
    return res.result;
  }

}
