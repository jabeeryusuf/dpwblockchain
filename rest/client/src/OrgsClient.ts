import * as qs from "qs";

import { IOrgsService, Org, Setting } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class OrgsClient implements IOrgsService {
  private rest: RestClient;
  private baseUrl: string;
  // private oauthHandler: OAuthHandler;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("orgs-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/orgs";
    // this.oauthHandler = oauthHandler;
  }

  public async list(): Promise<Org[]> {
    const res: IRestResponse<Org[]> = 
      await this.rest.get<Org[]>(this.baseUrl + "/list")
    return res.result;
  }

  public async info(orgId: string): Promise<Org> {
    const res: IRestResponse<Org> =
      await this.rest.get<Org>(this.baseUrl + "/info" + "?" + qs.stringify({orgId}));
    return res.result;
  }

  public async create(org: Org): Promise<Org> {
    const res: IRestResponse<Org> =
      await this.rest.create<Org>(this.baseUrl + "/create", org);
    return res.result;
  }

  public async getSetting(orgId: string, key: string): Promise<Setting> {
    const res: IRestResponse<Setting> =
      await this.rest.get<Setting>(this.baseUrl + "/getSetting" + "?" + qs.stringify({orgId,key}));

    return res.result;
  }

  public async updateSetting(orgSettingObj: Setting): Promise<Setting> {
    const res: IRestResponse<Setting> =
      await this.rest.create<Setting>(this.baseUrl + "/updateSetting", orgSettingObj);

    return res.result;
  }
}
