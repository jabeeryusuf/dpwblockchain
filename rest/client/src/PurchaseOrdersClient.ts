//import { JsonConvert } from "json2typescript";
import * as qs from "qs";
import { IPurchaseOrdersService, PurchaseOrdersResponse } from "tw-api-common";
import { IRestResponse, RestClient } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class PurchaseOrdersClient implements IPurchaseOrdersService {
  private rest: RestClient;
  private baseUrl: string;
  //private jsonConvert: JsonConvert;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("purchase-order-items-types-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/purchaseOrders";
    //this.jsonConvert = new JsonConvert();
  }

  public async info(purchaseOrderId: string): Promise<PurchaseOrdersResponse> {
    const res: IRestResponse<PurchaseOrdersResponse> = await this.rest.get<PurchaseOrdersResponse>(
      this.baseUrl + "/info" + "?" + qs.stringify({ purchaseOrderId }));

    return res.result;
  }

  public async query(whereClause: string, whereParams: string[], groupBy: string[], orderBy: string[]): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/query" + "?" + qs.stringify({ whereClause, whereParams, orderBy, groupBy }));

    return res.result;
  }

  public async searchPO(
    orgId: string,
    groupBy: string,
    orderBy: string,
    aggregate: string,
    filterParams?: object,
    searchString?: object,
    cursor?: string,
    limit?: number,
    desc?: boolean ): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/search" + "?" + qs.stringify({
        cursor,
        desc,
        filterParams,
        groupBy,
        limit,
        orderBy,
        orgId,
        searchString,
        aggregate,
      }));
    return res.result;
  }

  public async queryItems(whereClause: string, whereParams: string[], orderBy: string[], groupBy: string[])
    : Promise<any> {

    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryItems" + "?" + qs.stringify({ whereClause, whereParams, orderBy, groupBy }));

    return res.result;
  }

  public async queryMilestoneExceptions(orgId: string): Promise<any> {

    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryMilestoneExceptions" + "?" + qs.stringify({ orgId }));

    return res.result;
  }

  public async queryMilestones(orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryMilestones" + "?" + qs.stringify({ orgId }));

    return res.result;
  }

  public async getPurchaseOrderIDs(whereClause: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getPurchaseOrderIDs" + "?" + qs.stringify({ whereClause }));

    return res.result;
  }

  public async queryManufacturers(): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryManufacturers");

    return res.result;
  }

}
