import * as qs from "qs";

import { JsonConvert } from "json2typescript";
import { Container, IContainersService, ContainersResponse } from "tw-api-common";
import { IRestResponse, RestClient  } from "typed-rest-client";
import { OAuthHandler } from "./OAuthHandler";

export class ContainersClient implements IContainersService {

  private rest: RestClient;
  private baseUrl: string;
  private jsonConvert: JsonConvert;

  constructor(baseUrl: string, oauthHandler: OAuthHandler) {
    this.rest = new RestClient("containers-client", "", [oauthHandler]);
    this.baseUrl = baseUrl + "/containers";
    this.jsonConvert = new JsonConvert();
  }

  public async getVesselTrackings(): Promise<any> {
    console.log("get vessel trackings");

    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getVesselTrackings");

    console.log(res);

    return res.result;
  }

  public async getProductLocations(productId: string): Promise<any> {
    console.log("get product locations");

    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getProductLocations" + "?" + qs.stringify({productId}));

    console.log("got product locations");
    return res.result;
  }

  public async updateVehicleLocation(doc: any): Promise<any> {
    // being hit through python scraper currently
    throw new Error("Method not implemented.");
  }

  public async getInventoryLocations(orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getInventoryLocations" + "?" + qs.stringify({orgId}) );

    return res.result;
  }

  public async queryVehicleLocations(whereClause: any, whereParams: any, groupBy: any, orderBy: any): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryVehicleLocations" + "?" + qs.stringify({whereClause, whereParams, groupBy, orderBy}) );

    return res.result;
  }

  public async getVesselLocations(orgid: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/GetVesselLocations" + "?" + qs.stringify({orgid}) );
    return res.result;
  }

  public async getNames(): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getNames");

    return res.result;
  }

  public async info(containerId: any): Promise<Container> {
    const res: IRestResponse<Container> = await this.rest.get<any>(
      this.baseUrl + "/info" + "?" + qs.stringify({containerId}) );

    return res.result;
  }

  public async queryInventory(whereClause: string, whereParams: any, groupBy: string[], orderBy: any): Promise<any> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryInventory" + "?" + qs.stringify({whereClause, whereParams, groupBy, orderBy}) );

    return res.result;
  }

  public async getBuilding(containerId: string): Promise<ContainersResponse> {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getBuilding" + "?" + qs.stringify({containerId}) );

    return this.jsonConvert.deserializeObject(res.result, ContainersResponse);
  }

  public async queryBuildings(whereClause: string, whereParams: string[], groupBy: string[],orderBy: string[]) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/queryBuildings" + "?" + qs.stringify({whereClause, whereParams, groupBy, orderBy}) );
    return res.result;
  }

  public async getProductsForContainer(containerId: string) {
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getProductsForContainer" + "?" + qs.stringify({containerId}) );

    return res.result;
  }

  public async addBuilding(Obj: any, orgId: string) {
    const res: IRestResponse<any> = await this.rest.create<any>(
      this.baseUrl + "/addBuilding" , {Obj, orgId});
    return res.result;
  }

  public async deleteBuilding(containerId: any, name: string, orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.del<any>(
        this.baseUrl + "/deleteBuilding" + "?" + qs.stringify({containerId, name, orgId}));
    return res.result;
  }

  public async editBuilding(Obj: any, previousContainerId: any, orgId: string): Promise<any> {
    const res: IRestResponse<any> = await this.rest.update<any>(
      this.baseUrl + "/editBuilding", {Obj, previousContainerId, orgId});
    return res.result;
  }

  public async getShippingContainers(orgId: any, active?: boolean, containerId?: string): Promise<any>{
    console.log(qs.stringify({orgId, active, containerId}))
    const res: IRestResponse<any> = await this.rest.get<any>(
      this.baseUrl + "/getShippingContainers" + "?" + qs.stringify({orgId, active, containerId}));
    return res.result;
  }

}
