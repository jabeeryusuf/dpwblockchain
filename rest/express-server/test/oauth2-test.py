#!/usr/bin/env python3

import requests
import json
import base64

base_url = 'http://localhost:5000'

#
# oauth server verification password based
#
 
client_id = 'application'
client_secret = 'notSecret'
username = 'demo&demo@teuchain.com'
password = 'demo'

# encode 'client_id:client_secret' to base 64
basic_auth = base64.b64encode(bytes(client_id + ':' + client_secret,'utf-8')).decode('ascii')

r = requests.post(base_url +  '/user/login',
                  data = {'grant_type' : 'password',
                          'username' : username,
                          'password' : password},
                  headers = {"Authorization" : "Basic " + basic_auth})

print("PASSWORD GRANT returns: {}".format(r.text))

pw_token = r.json()['access_token']

 
#
# Using client id based authentication
#
""" print("CLIENT GRANT");
client_id = 'confidentialApp'
client_secret = 'topSecret'
basic_auth = base64.b64encode(bytes(client_id + ':' + client_secret,'utf-8')).decode('ascii')

r = requests.post(base_url +  '/user/login',
                  data = {'grant_type' : 'client_credentials'},
                  headers = {"Authorization" : "Basic " + basic_auth});
print("CLIENT GRANT returns: {}".format(r.text))
cc_token = r.json()['access_token']
 """                             

#
# Use the token to call the protected area
#
 
print("PROTECTED")

r = requests.get(base_url + '/product/test',
                 headers={'Authorization' : 'Bearer {}'.format(pw_token)},
                 )

print("PROTECTED returns: {}".format(r.text))



