let nodemailer = require('nodemailer');

/**
Install mailin:
$ sudo npm i --global mailin

run when testing:
$ mailin -p 5025 --webhook http://localhost:5000/v0.1/message/receiveEmail

Setup opensmtpd and replace smtpd.conf with the below comments:

sudo apt install dovecot-imapd opensmtpd

#/etc/smtpd.conf
listen on localhost
accept for domain message-dev.localhost relay via localhost:5025
accept for any deliver to mbox

*/


let mailConfig = {
        host: 'localhost',
        port: 5025,

        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        }
    };

let transporter = nodemailer.createTransport(mailConfig);

// add attachments
let attachments = [
{   // file on disk as an attachment
    filename: 'text3.txt',
    path: '/home/jason/Downloads/gift.svg' // stream this file
},
]

let message = {
    from: 'lil.fella@teuchain.com',
    to: 'group-1234@message.teuchain.com, group-3456@message.teuchain.com, not-message@test.com',
    subject: 'hi there',
    text: 'I like bananas how about you? ',
    attachments

}


transporter.sendMail(message).then(info=>{
    console.log('info:');
    console.log(info);
});