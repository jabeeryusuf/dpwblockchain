"use strict";

import {readFileSync} from "fs";
import { X12Parser, X12Segment } from "../index";

describe("X12Parser", () => {

  it("should parse a valid X12 document without throwing an error", () => {
    const edi = readFileSync("tests/test-data/850.edi", "utf8");
    const parser = new X12Parser(true);
    parser.parseX12(edi);
  });

  it("should produce accurate line numbers for files with line breaks", () => {
    const edi = readFileSync("tests/test-data/850_3.edi", "utf8");
    const parser = new X12Parser(true);
    const interchange = parser.parseX12(edi);

    const segments = [].concat(
      [interchange.header, interchange.functionalGroups[0].header,
        interchange.functionalGroups[0].transactions[0].header],
      interchange.functionalGroups[0].transactions[0].segments,
      [interchange.functionalGroups[0].transactions[0].trailer,
        interchange.functionalGroups[0].trailer, interchange.trailer],
    );

    for (let i = 0; i < segments.length; i++) {
      const segment: X12Segment = segments[i];

      if (i !== segment.range.start.line) {
        throw new Error(`Segment line number incorrect. Expected ${i}, found ${segment.range.start.line}.`);
      }
    }
  });

});
