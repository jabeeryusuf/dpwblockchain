"use strict";

import * as fs from "fs";
import { IX12SerializationOptions, X12Parser } from "../index";

describe("X12Formatting", () => {

  it("should replicate the source data unless changes are made", () => {
    const edi = fs.readFileSync("tests/test-data/850.edi", "utf8");
    const parser = new X12Parser(true);
    const interchange = parser.parseX12(edi);

    const options: IX12SerializationOptions = {
      endOfLine: "\n",
      format: true,
    };

    const edi2 = interchange.toString(options);

    if (edi !== edi2) {
      throw new Error(`Formatted EDI does not match source. Found ${edi2}, expected ${edi}.`);
    }
  });

});
