import * as AWS from "aws-sdk";
import * as crypto from "crypto";
import * as fs from "fs";
import * as moment from "moment";
import * as qs from "qs";
import * as util from "util";
import * as uuid from "uuid";

import { Request } from "express";
import { Client, QueryResult } from "pg";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, PATCH, Path, POST, Security } from "typescript-rest";
import { EmailsService } from "../emails/EmailsService";
import { GroupsService } from "../groups/GroupsService";
import { DbClientSingleton } from "../utils";
import { IDatabase } from "pg-promise";

const generateListWhereObj = (orgId: any, role: any, disabled: any) => {
  let whereClause = " where org_id =$1 ";
  const whereParams = [orgId];
  let idx = 2;
  const listOfOptions = [role, disabled];
  const listOfKeys = ["role", "disabled"];
  for (let i = 0; i < listOfOptions.length; i++) {
    if (listOfOptions[i]) {
      whereClause += " and " + listOfKeys[i] + " = $" + idx;
      idx += 1;
      whereParams.push(listOfOptions[i]);
    }
  }
  return {clause: whereClause, params: whereParams};
};
@Path("/v0.1/users")
export class UserService {
  @ContextRequest private request: Request;

  @Inject private group: GroupsService;
  @Inject private emailsService: EmailsService;
  @Inject private dbClient: DbClientSingleton;

  private db: Client;
  private dbp: IDatabase<any>;

  constructor() {
    this.db = this.dbClient.db;
    this.dbp = this.dbClient.dbp;
  }

  @GET
  @Security("USER")
  @Path("/list")
  public async list(@Param("role") role: string, @Param("disabled") disabled: boolean) {
    const orgId = this.request.user.orgId;
    console.log("list for org %s", orgId);
    const obj = generateListWhereObj(orgId, role, disabled);
    const query = "SELECT * FROM users " + obj.clause;
    console.log(query);
    const result = await this.db.query( query, obj.params);
    const rows = result.rows;
    // console.dir(rows);
    return rows;
  }

  public async createDailySummary(user: any) {
    const assignedCases = await this.db.query("SELECT * FROM cases where case_assign_to = $1", [user.user_id]);
    const memberCases = await this.group.getAncestorGroupsForUser(user.user_id);
    const params = {user, memberCases: memberCases.length, assignedCases: assignedCases.rows.length};
    return await this.emailsService.createEmailFromTemplate([user.email], "summary", "Your Daily Summary", params);

  }
  @PATCH
  @Security("USER")
  @Path("/update")
  public async update(body: any) {
    /*
    I am querying the current user to compare the submitted password to hashed password
    If they match I know not to hash the password (because it is already hashed) otherwise
    I hash it prior to update

    I think the correct way to handle this would be sending up the users unhashed pw (This would also allow us to show current pass as ***** in ui) and
    then hashing no matter what. This should be sufficient for the current moment though.
    */

    const currentUser = await this.db.query("SELECT * FROM users where user_id = $1 and password=$2", [body.user_id, body.password]);
    if (currentUser.rows.length > 0) {
      return await this.db.query("UPDATE users set name = $1, disabled=$2 where user_id = $3", [body.name, body.disabled, body.user_id]);
    }
    return await this.db.query("UPDATE users set name = $1, password = crypt($2, gen_salt('bf', 8)) where user_id =$3", [body.name, body.password, body.user_id]);
  }

  @POST
  @Path("/create")
  public async create(body: {orgId: string, email: string, name: string, password: string, code: string}) {
    const { orgId, email, name, password, code } = body;
    const errorList: string[] = [];
    try {
      if (code) {
        const pendingInvitation = await this.db.query("SELECT * from invitations where code = $1 AND email = $2 AND org_id =$3 AND invitation_status ='pending'", [code, email, orgId]);
        console.log(pendingInvitation.rows);
        if (pendingInvitation.rows.length < 1) {
          errorList.push("Invitation for this email could not be verified");
          throw new Error("Invitation for this email could not be verified");
        }
        if (pendingInvitation.rows[0].invitation_status === "accepted") {
          errorList.push("This invitation has already been accepted. Please login to continue");
          throw new Error("This invitation has already been accepted. Please login to continue");
        }
        const expiredDate = pendingInvitation.rows[0].expiration_date;
        const isExpired = moment(new Date()).isAfter(expiredDate);
        if (isExpired) {
          errorList.push("This invitation has expired");
          throw new Error("This invitation has expired");
        }
      }
      await this.db.query("INSERT INTO users (user_id,org_id,email,name,password) VALUES($1,$2,$3,$4,crypt($5, gen_salt('bf', 8)))", [uuid.v4(), orgId, email, name, password]);
      if (code) {
        await this.db.query("UPDATE invitations set invitation_status = 'accepted' where email = $1 and org_id =$2", [email, orgId]);
      }
      return errorList;
    } catch (err) {
      console.log(err);
      return errorList;
    }

  }

  @POST
  @Path("/addNamesToUser")
  @Security("USER")
  public async addNameToUser(body: {emailsAndNames: any}): Promise<QueryResult> {
    const {emailsAndNames} = body;
    const sqlStart = `UPDATE users AS u SET name=n.name FROM ( VALUES`;
    let sqlMid = "";
    const sqlEnd = `) AS n (email, name) WHERE u.email=n.email`;

    let i = 0;
    for (const obj of emailsAndNames) {
      if (i > 0) {
        sqlMid += ", ";
      }
      sqlMid += "('" + obj.email + "', '" + obj.name + "')";
      i++;
    }

    const sql = sqlStart + sqlMid + sqlEnd;
    return await this.db.query(sql);
  }

  @POST
  @Security("USER")
  @Path("createInvitations")
  public async createInvitations(body: {emails: any[], message: string, senderName: string, senderEmail: string}) {
    const {emails, message, senderName, senderEmail } = body;

    const orgId = this.request.user.orgId;

    console.dir(body);
    const errorList = [];
    for (const emailObj of emails) {

      if (emailObj.email.length) {
        // console.log("me to ");
        // Delete any invitations that already exist for this user
        const existingUser = await this.db.query("SELECT * FROM users where org_id = $1 and email = $2", [orgId, emailObj.email]);
        if (existingUser.rows.length === 0) {
          await this.db.query("UPDATE invitations set invitation_status = 'inactive' where org_id = $1 and email = $2", [orgId, emailObj.email]);
          // create a random code that will be attached to link in email for verification
          const code = crypto.randomBytes(20).toString("hex");
          const params = qs.stringify({email: emailObj.email, code});
          // create an expiration date for the invite
          const today = new Date();
          const todayMoment = moment(today);
          const expire = todayMoment.add(1, "week");
          const expirationDate = expire.toDate();
          const newInvitation = await this.db.query("INSERT INTO invitations (email,org_id,code,expiration_date,sender_name,sender_email,invitation_status) VALUES ($1,$2,$3,$4,$5, $6, 'pending')",
            [emailObj.email, orgId, code, expirationDate, senderName, senderEmail]);
          // console.log(newInvitation);
          const emailInfo = {
            from: senderEmail,
            to: emailObj.email,
            subject: "Join " + orgId + " on TEUchain",
            text: senderName + ` has invited you to join ` + orgId + ` on teuchain!`,
          };
          // using my email just to get basic flow
          if (newInvitation) {
            // generate a link to the page based on env
            let emailLink;
            let cancelLink;
            if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
              emailLink = "http://localhost:3000/" + orgId + "/invite-signup?" + params;
              cancelLink = "http://localhost:3000/" + orgId + "/invite-error?" + params;
            }
            if (process.env.NODE_ENV === "staging") {

              emailLink = "https://www-staging.teuchain.com/" + orgId + "/invite-signup?" + params;
              cancelLink = "https://www-staging.teuchain.com/" + orgId + "/invite-error?" + params;
            }
            if (process.env.NODE_ENV === "production") {
              emailLink = "https://app.teuchain.com/" + orgId + "/invite-signup?" + params;
              cancelLink = "https://app.teuchain.com/" + orgId + "/invite-error?" + params;
            }

            console.log("EMAIL LINK:");
            console.log(emailLink);

            if (message.length > 2) {
              // this function accounts for line breaks in the original stringified message
              const multlinemessage = this.emailsService.generateMultilineHtmlMessage(message, "font-size: 14px");
              const htmlBody =
                multlinemessage + `<p> Click <a href = "` + emailLink + `" > here </a> to accept </p>
              <img style="height: 30px; width:100px" src = "http://teuchain.com/images/teuchain_logo.png"/>
              <p style="font-size: 10px; font-style: italic"> If you believe that you are receiving this message in error, please click <a href = "` + cancelLink + `" > here </a></p>
              `;
              await this.emailsService.sendEmail([emailObj.email], emailInfo.from, emailInfo.subject, emailInfo.text, htmlBody, undefined);
            } else {
              const htmlBody = `<p style="font-size: 14px"> Hello ` + emailObj.email + `, </p> <p style="font-size: 14px;">` + senderName + ` has invited you to join ` + orgId + ` on TEUchain! Click <a href = "` + emailLink + `" > here </a> to accept </p>
              <img style="height: 30px; width:100px" src = "http://teuchain.com/images/teuchain_logo.png"/>
              <p style="font-size: 10px; font-style: italic"> If you believe that you are receiving this message in error, please click <a href = "` + cancelLink + `" > here </a></p>
              `;
              await this.emailsService.sendEmail([emailObj.email], emailInfo.from, emailInfo.subject, emailInfo.text, htmlBody, undefined);
            }
          }
        } else {
          errorList.push(emailObj.email + " is already registered");
        }
      }
    }
    return errorList;
  }

  @POST
  @Security("USER")
  @Path("/deleteInvitations")
  public async deleteInvitation(body: {code: string}) {

    const inactive = await this.db.query("UPDATE invitations set invitation_status = $1 where code = $2", ["inactive", body.code]);
    return inactive;
  }

  @POST
  @Path("/resetPassword")
  public async resetPassword(body: {email: string, org_id: string}) {
    const {email, org_id} = body;
    const users = await this.db.query("SELECT * FROM users where email = $1", [email]);
    if (users.rows.length < 1) {
      return { error: "User is not currently registered" };
    }
    const username = users.rows[0].name;
    const resetCode = uuid.v4();
    const currentDate = new Date();
    const expirationDate = currentDate.setDate(currentDate.getDate() + 1);
    let emailLink;
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
      emailLink = "http://localhost:3000/" + org_id + "/change-password/" + resetCode;
    }
    if (process.env.NODE_ENV === "staging") {
      emailLink = "https://www-staging.teuchain.com/" + org_id + "/change-password/" + resetCode;
    }
    if (process.env.NODE_ENV === "production") {
      emailLink = "https://app.teuchain.com/" + org_id + "/change-password/" + resetCode;
    }
    await this.db.query("Update users set reset_code = $1, reset_code_expiration = $2 where email = $3", [resetCode, new Date(expirationDate), email]);
    const htmlBody = `<p style="font-size: 14px"> Hello ` + username + `, </p> <p style="font-size: 14px;"> Please click  <a href = "` + emailLink + `" > here </a> to reset your password </p>
                <img style="height: 30px; width:100px" src = "http://teuchain.com/images/teuchain_logo.png"/>
                `;
    this.emailsService.sendEmail([email], "password-reset@teuchain.com", "Reset Your Password", "Reset Your Password", htmlBody, undefined);
    return {};
  }

  @POST
  @Path("/changePassword")
  public async changePassword(body: {email: string, password: string, code: any} ) {
    const {email, password, code} = body;

    console.log("change password email", email);
    const user = await this.db.query("SELECT * FROM users where email = $1 and  reset_code = $2", [email, code]);
    if (!user.rows.length) {
      return {error: "Password cannot be reset"};
    } else {
      if (new Date() > new Date(user.rows[0].reset_code_expiration)) {
        return {error: "Password reset code has expired"};
      } else {
        try {
          return await this.db.query("Update users set reset_code=$1, reset_code_expiration=$2, password=  crypt($3, gen_salt('bf', 8)) where email=$4", [null, null, password, email]);
        } catch (err) {
          console.log(err);
        }
      }
    }
  }

  @POST
  @Path("/sendVerificationCode")
  public async sendVerificationCode(body: {email: string}) {
    const verificationCode = Math.floor((Math.random() * 999999));
    console.log(`SENDING VERIFICATION CODE: ${verificationCode}`);

    const emailRow = await this.dbp.oneOrNone(`
      SELECT * FROM emails where email =$1`, 
      [body.email]);

    if (emailRow) {
      await this.dbp.none("UPDATE emails SET email=$1, code=$2, verified=0 WHERE email=$1", [body.email, verificationCode]);
    } else {
      await this.dbp.none("INSERT INTO emails (email, code) VALUES ($1,$2)", [body.email, verificationCode]);
    }

    const msgString = "Your org creation verification code is " + verificationCode;
    this.emailsService.createEmailFromTemplate([body.email], "verification", "Verify you email on DPWChain", { user: {email : "dpwchain@outlook.com"}, message : msgString});
  }

  @PATCH
  @Path("/verifyEmail")
  public async verifyEmail(body: {email: string, code: any}) {
    console.log(`VERIFY EMAIL ${body.email} WITH ${body.code}`);
    const { email, code} = body;
    const emails = await this.dbp.query(`
      SELECT * FROM emails where email =$1 and code =$2 and verified= 0`,
      [email, code]);

    if (emails.length > 0 ) {
      await this.db.query("UPDATE emails set verified = $1", [1]);
      console.log("VERIFIED");
      return true;
    }
    return false;
  }

  public async getUsers(usersIds: string[]) {

    let rows = [];
    try {
      const result = await this.db.query("SELECT * FROM users WHERE user_id = ANY ($1)", [usersIds]);
      rows = result.rows;
    } catch (err) {
      console.log(err);
    }

    return rows;
  }
  public async getUser(userId: string) {

    let rows = [];
    try {
      const result = await this.db.query("SELECT * FROM users WHERE user_id = $1", [userId]);
      rows = result.rows;
    } catch (err) {
      console.log(err);
    }

    return rows.length > 0 ? rows[0] : null;
  }

  // TEMPORARY

  public async retrieveFilesFroms3(itemId: string) {
    AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "ZKu2SnEZlBj35Dyp+VFfROlnu2YLB5LYHGSmcaoa" });
    const S3 = new AWS.S3({ params: { Bucket: "teuchain" } });
    const s3Retreive = await util.promisify(S3.getObject.bind(S3));
    const retreivedObj = await s3Retreive({ Bucket: "teuchain", Key: "user-uploads/" + itemId });
    return retreivedObj;
  }

  public async queryFiles(whereClause: string, whereParams: any[], groupBy: string, orderBy: string) {
    let sql = "SELECT * from files";
    if (whereClause) {
      sql += " WHERE ";
      sql += whereClause;
    }
    if (groupBy) {
      sql += " GROUP BY ";
      sql += groupBy;
    }
    if (orderBy) {
      sql += " ORDER BY ";
      sql += orderBy;
    }
    const files = await this.db.query(sql, whereParams);
    const s3Obj = [];
    for (let i = 0; i < files.rows.length; i++) {
      s3Obj.push(this.retrieveFilesFroms3(files.rows[i].s3_bucket));
    }
    const objs = await Promise.all(s3Obj);
    const writeFileFunc = util.promisify(fs.writeFile);
    for (let i = 0; i < files.rows.length; i++) {
      await writeFileFunc("../Downloads/" + files.rows[i].s3_bucket, objs[i].Body);
    }
    return files.rows;
  }

}
