import { Request } from "express";
import { IVendorsService, Vendor } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, Path, Security } from "typescript-rest";
import { DbClientSingleton } from "../utils";

@Path("/v0.1/vendors")
@Security("USER")
export class VendorsService implements IVendorsService {

  @ContextRequest private request: Request;

  @Inject private dbClient: DbClientSingleton;

  @GET
  @Path("/list")
  public async list(): Promise<Vendor []> {
    const orgId = this.request.user.orgId;
    const result: any = await this.dbClient.db.query(`SELECT * FROM vendors WHERE org_id = $1`, [orgId]);
    const vendors = result.rows ? result.rows.map(this.dbRowToCase) : [];
    console.log("returning " + vendors.length);
    return vendors;
  }

  @GET
  @Path("/info")
  public async info(@Param("vendorId") vendorId: string): Promise<Vendor> {
    const row: any = await this.dbClient.db.query(`SELECT * FROM vendors WHERE vendor_code = $1`, [vendorId]);
    return this.dbRowToCase(row);
  }

  private dbRowToCase = (row: any) => {
    const v: Vendor = new Vendor();
    v.vendorId = row.vendor_code;
    v.name = row.vendor_name;
    v.orgId = row.org_id;
    v.contactGroupId = row.group_id;

    return v;
  }

}
