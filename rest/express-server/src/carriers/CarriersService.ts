import { Carrier } from "tw-api-common";
import { Path, GET } from "typescript-rest";

const carriers: Carrier[] = [

  { carrierId: "APLU", name: "APL"},
  { carrierId: "CMDU", name: "CMA CGM"},
  { carrierId: "COSU", name: "COSCO"},
  { carrierId: "HDMU", name: "Hyundai"},
  { carrierId: "HLCU", name: "Hapag Lloyd"},
  { carrierId: "KLGN", name: "Kerry"},
  { carrierId: "MAEU", name: "Maersk"},
  { carrierId: "ONEY", name: "Ones"},
  { carrierId: "OOLU", name: "OOCL"},
  { carrierId: "SHPT", name: "Shipco"},
  { carrierId: "SITC", name: "SITC"},
  { carrierId: "SIKU", name: "Samudera"},
  { carrierId: "TMGB", name: "Team Global"},

  { carrierId: "HU",   name: "Hainan" },

]

// No security
@Path("/v0.1/carriers")
export class CarriersService {

  /**
   * List all available carriers
   */

  @GET
  @Path("/list")
  public async list(): Promise<Carrier[]> {
    return carriers;
  }






}