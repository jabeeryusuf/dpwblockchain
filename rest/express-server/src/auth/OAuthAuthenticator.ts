import * as express from "express";
import * as _ from "lodash";
import * as OAuth2Server from "oauth2-server";
import { ServiceAuthenticator } from "typescript-rest";
import { Inject } from "typescript-ioc";
import { OAuth2Protocol } from "./OAuth2Protocol";

export class OAuthAuthenticator implements ServiceAuthenticator {
  @Inject private oauth: OAuth2Protocol;

  constructor() {
    console.log("OAUTH AUTH CONSTRUctoR");
  }

  public initialize(router: express.Router): void {
    console.log("OAUTH: INIT AUTHENTICATOR");
  }

  public getMiddleware(): express.RequestHandler {
    // console.log("OAUTH: GET MIDDLEWARE");
    return this.authMiddleware;
  }

  public getRoles(req: express.Request): string[] {
    // console.log(req.user);
    return [ "USER" ];
  }

  // private
  private authMiddleware = async (req: express.Request, res: express.Response,next: express.NextFunction) => {
    //console.log("auth middleware,");
    //console.log(req);

    const request = new OAuth2Server.Request(req);
    const response = new OAuth2Server.Response(res);

    const options = {
      addAcceptedScopesHeader: true,
      addAuthorizedScopesHeader: true,
    };

    const token = await this.oauth.oauthServer.authenticate(request, response, options);

    // make the user available
    req.user = token.user;

    next();
  }

}
