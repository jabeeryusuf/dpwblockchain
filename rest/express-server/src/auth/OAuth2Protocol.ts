import * as OAuthServer from "oauth2-server";
import { OAuth2SqlModel } from "./OAuth2SqlModel";
import { Singleton } from "typescript-ioc";

@Singleton
export class OAuth2Protocol {
  private oauthModel: OAuth2SqlModel;
  public oauthServer: OAuthServer

  constructor() {
    this.oauthModel = new OAuth2SqlModel();
    this.oauthServer = new OAuthServer({
      allowExtendedTokenAttributes: true,
      model: this.oauthModel,
      // accessTokenLifetime: 30,  // for test
    });
  }



}