
import { Request, Response } from "express";
import * as OAuthServer from "oauth2-server";
import { Inject } from "typescript-ioc";
import { Context, Path, POST, ServiceContext } from "typescript-rest";
import { OAuth2Protocol } from "./OAuth2Protocol";

@Path("/v0.1/auth")
export class AuthService {

  @Context public context: ServiceContext;
  @Inject public oauth: OAuth2Protocol;

  @POST
  @Path("/login")
  public async login(/*body*/) {
    const request = new OAuthServer.Request(this.context.request);
    const response = new OAuthServer.Response(this.context.response);

    await this.oauth.oauthServer.token(request, response);

    this.handleResponse(this.context.request, this.context.response, response);
    console.log("new loginuser end");
  }

  // from express-oauth-server.  should move all of this into a shared
  private handleResponse = (req: Request, res: Response, response: OAuthServer.Response) => {

    if (response.status === 302) {
      const location = response.headers.location;
      delete response.headers.location;
      res.set(response.headers);
      res.redirect(location);
    } else {
      res.set(response.headers);
      res.status(response.status).send(response.body);
    }
  }

}
