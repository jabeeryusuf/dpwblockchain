
CREATE TABLE oauth_tokens (
    access_token             text NOT NULL,
    access_token_expires_at  timestamptz NOT NULL,
    refresh_token            text NOT NULL,
    refresh_token_expires_at timestamptz NOT NULL,
    client_id                text NOT NULL,
    user_id                  text NOT NULL

);

CREATE TABLE oauth_clients (
  client_id     text NOT NULL,
  client_secret text NOT NULL,
  client_grant  text NOT NULL,
    
  redirect_uri  text,     -- Only relevant for authorization grant and can be multiple


  CONSTRAINT oauth_clients_pk PRIMARY KEY (client_id, client_secret)
);


-- blockchain org, so user per org 
DROP TABLE users

CREATE TABLE users (
  user_id         text,     -- supply_chain_id&email
  org_id          text,
  email           text,
  name            text,
  password        text,

  CONSTRAINT users_pk PRIMARY KEY (user_id)
);

DROP TABLE orgs;
CREATE TABLE orgs (
  org_id    text,
  name      text,
  type      text,

  CONSTRAINT org_pk PRIMARY KEY (org_id)
);



INSERT INTO users
  (user_id, username, organization_id,password,name) 
VALUES
  ('demo-guy','demo','demo','demo','demo'); 

INSERT INTO users 
  (user_id,supply_chain_id,email,name,password) 
VALUES 
  ('lidl&lidl@teuchain.com','lidl','lidl@teuchain.com','Lidl Fella','lidl'),
  ('nike&nike@teuchain.com','nike','nike@teuchain.com','Lebron James','nike'),
  ('demo&demo@teuchain.com','demo','demo@teuchain.com','Marshal Mathers','demo'),
  ('test1&test@teuchain.com','test1','test@teuchain.com','Test Guy','test'),
  ('test2&test@teuchain.com','test2','test@teuchain.com','Test She','test'),
  ('test3&test@teuchain.com','test3','test@teuchain.com','Test Car','test'),
;

INSERT INTO orgs
  (org_id, name,type) 
VALUES
  ('lidl', 'Lidl Inc','supply-chain'),
  ('nike', 'Nike Inc','supply-chain'),
  ('demo', 'Demo Guys','supply-chain'),
  ('test1', 'Test1 Inc','supply-chain'),
  ('test2', 'Test2 Inc','supply-chain'),
  ('test3', 'Test3 Inc','supply-chain'),
  ('dpworld.ca', 'DP World Canada','provider'),
  ('sinotrans', 'Sinotrans Monster','provider')
;


