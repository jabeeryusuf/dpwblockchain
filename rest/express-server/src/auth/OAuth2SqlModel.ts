import { BaseModel, PasswordModel, RefreshTokenModel, RequestAuthenticationModel, Token} from "oauth2-server";
import { Client } from "pg";
import { Inject, Singleton } from "typescript-ioc";
import * as util from "util";
import { DbClientSingleton } from "../utils";

@Singleton
export class OAuth2SqlModel implements BaseModel, PasswordModel, RefreshTokenModel, RequestAuthenticationModel {

  @Inject
  private dbClient: DbClientSingleton;

  private clients: any;
  private db: Client;

  constructor() {
    this.clients = [
      {id: "application",  secret: "notSecret", grants: ["password", "refresh_token"] },
      {id: "confidentialApp", secret: "topSecret", grants: ["client_credentials"] },
    ];

    this.db = this.dbClient.db;
  }

  public async getClient(clientId: string, clientSecret: string) {
    console.log("Oauth: getClient(clientId=%s, clientSecret=%s)", clientId, clientSecret);

    // move to db eventually
    // results = await this.db.query('SELECT * FROM oauth_clients WHERE client_id = $1 AND client_id = $2', [clientId,clientSecret])

    const clients = this.clients.filter(function(client: any) {
      return client.id === clientId && client.secret === clientSecret;
    });

    return clients.length ? clients[0] : false;
  }

  public async verifyScope(token: Token, scope: string[]): Promise<boolean> {
    // console.log("scope = %s",scope);
    return true;
  }

  /*
  * Get access or refresh token.  Switched based on tt
  */

  public async getToken(tt: string, bearerToken: string): Promise<any> {
    // console.log("Oauth: getToken(%s,%s)",tt,bearerToken);

    // Get TOKEN
    const query = util.format(`SELECT %s_token as token, %s_token_expires_at AS token_expires_at, client_id,user_id
                             FROM oauth_tokens
                             WHERE %s_token = $1`
                             , tt , tt, tt);

    let results = await this.db.query(query, [bearerToken]);

    if (results.rowCount === 0) {
      return null;
    }

    const token = results.rows[0];

    // Get user object
    results = await this.db.query("SELECT * FROM users WHERE user_id = $1",
                                  [token.user_id]);

    if (results.rowCount === 0) {
      return null;
    }

    const user = results.rows[0];

    // Get the org
    const org = await this.getOrg(user.org_id);

    // Map to oauth model
    user.id = user.user_id;

    // fix up
    user.orgId = user.org_id;
    user.userId = user.user_id;

    const retToken: any = {
      client: {id: token.client_id},
      org,
      user,
      userObject: user,
    };

    // console.log('retToken: ')
    // console.dir(retToken);

    retToken[tt + "Token"] = token.token;
    retToken[tt + "TokenExpiresAt"] =  token.token_expires_at;

    return retToken;
  }

  public async getAccessToken(bearerToken: string) {
    // console.log("OAuth: getAccessToken:");
    return await this.getToken("access", bearerToken);
  }

  public async getRefreshToken(bearerToken: string) {
    // console.log("OAuth: getRefreshToken:");
    return await this.getToken("refresh", bearerToken);
  }

  public async revokeToken(token: Token): Promise<boolean> {
    // console.log("OAuth: revokeToken:");
    return true;
  }

  public async saveToken(token: any, client: any, user: any) {
    console.log("Oauth: saveToken: client=%s, user=%s)", client.id, user.user_id);

    const result = await this.db.query("INSERT INTO oauth_tokens (access_token, access_token_expires_at, refresh_token, refresh_token_expires_at,client_id, user_id) VALUES ($1, $2, $3, $4, $5, $6)",
      [token.accessToken, token.accessTokenExpiresAt, token.refreshToken, token.refreshTokenExpiresAt, client.id, user.user_id],
    );

    // Get the org
    const org = await this.getOrg(user.org_id);

    let ret = null;
    if (result.rowCount) {
      ret = {
        accessToken: token.accessToken,
        accessTokenExpiresAt: token.accessTokenExpiresAt,
        refreshToken: token.refreshToken,
        refreshTokenExpiresAt: token.refreshTokenExpiresAt,

        client,
        org,
        user,
        userObject: user,
      };
    }

    return ret;
  }

  /*
  * Get user: called for password grants
  */

  public async getUser(username: string, password: string) {
    console.log("Oauth: getUser(username = %s, password = %s)", username, password);

    const split = username.indexOf("&");
    if (split === -1) {
      return null;
    }

    let orgId = username.slice(0, split);
    const email = username.slice(split + 1);

    console.log("org_id = %s, email = %s", orgId, email);

    const results = await this.db.query(`
      SELECT * FROM users WHERE org_id = $1
      AND LOWER(email) = LOWER($2)
      AND password = crypt($3, password)`,
      [orgId, email, password]);

    if (results.rowCount === 0) {
      return null;
    }

    const user = results.rows[0];

    // map to oauth
    user.id = user.user_id;

    return user;
  }

  /*
   *  GetUserFromClient: only called for client_credentials
   */

  public getUserFromClient(client: any) {
    console.log("Oauth: getUserFromClient(client=%s)", client.clientId);
    const clients = this.clients.filter(function(c: any) {
      return client.clientId === c.clientId && client.clientSecret === c.clientSecret;
    });

    const user = clients.length > 0 ? {id: clients[0].clientId} : false;
    console.log("Oauth: getUserFromClient returns: %j", user);
    return user;
  }

  private async getOrg(orgId: string) {
    // Get the org
    const results = await this.db.query("SELECT * FROM orgs WHERE org_id = $1",
      [orgId]);

    if (results.rowCount === 0) {
      return null;
    }

    const org = results.rows[0];

    // Fixup: DB uses org_id
    org.orgId = orgId;

    return org;
  }

}

