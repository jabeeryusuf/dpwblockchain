import { UserService } from "./users/UsersService";
import { MessageService } from "./messages/MessagesService";

export class NotificationService {

  private socket: any;
  //private user: UserService;
  //private message: MessageService

  onEnableNotifications(socket: any, user: UserService, message: MessageService) {
    // socket.emit('notify',{channel: "poopChat", id: 'unread', value: 13 });
    // socket.emit('notify', { channel: 'cases', id: 'cases', value: 14});
     
    this.socket = socket;
  }

  notify(channel:any, id:any, value:any) {
    this.socket.emit('notify',{channel,id,value})
  }

}
