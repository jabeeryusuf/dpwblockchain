import { IDatabase } from "pg-promise";
import { Vessel, ContainersResponse } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { GET, Param, PATCH, Path } from "typescript-rest";
import { DbClientSingleton } from "../../utils";
import { ResponseStatus } from "tw-api-common/dist/models/common";

@Path("/v0.1/containers/vessels")
export class VesselsService {

  @Inject private dbClient: DbClientSingleton;

  private db: IDatabase<any>;

  constructor() {
    this.db = this.dbClient.dbp;
  }

  @GET
  @Path("info")
  public async info(@Param("vesselId") vesselId: string): Promise<ContainersResponse> {
    const res = await this.db.any(`
      select imo,mmsi,vessel_name as name, flag,type from vessel_map  WHERE mmsi = $1`,
      [vesselId]);

    // const res = await this.db.any(`
    // SELECT * FROM vessels WHERE mmsi = $1`,
    // [vesselId]);

    return new ContainersResponse(res.length
      ? { status: ResponseStatus.OK, vessel: this.dbToVessel(res[0]) }
      : { status: ResponseStatus.NO_RESULTS });

  }

  @PATCH
  @Path("update")
  public async update(vessel: Vessel): Promise<Vessel> {

    const storedVessel = await this.info(vessel.vesselId);
    if (!storedVessel) {
      throw new Error("Record doesn't exist for MMSI " + vessel.vesselId);
    }

    // console.log("updating " + vessel.mmsi)
    await this.db.none(`
      UPDATE vessels SET name = $1, imo = $2, type = $3 WHERE mmsi = $4`,
      [vessel.name, vessel.imo, vessel.type, vessel.mmsi]);

    return vessel;
  }

  private dbToVessel(row: any): Vessel {
    return {
      flag:row.flag!=null?row.flag:"",
      imo: row.imo!=null?row.imo:"",
      mmsi: row.mmsi!=null?row.mmsi:"",
      name: row.name!=null?row.name:"",
      type: row.type!=null?row.type:"",
      vesselId: row.mmsi!=null?row.mmsi:"",   // Row is the vessel ID
    };
  }
}
