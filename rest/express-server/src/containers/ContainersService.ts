import { Request } from "express";
import fetch from "node-fetch";
import { Client } from "pg";
import { Building, Container, IContainersService, ContainersResponse, ResponseStatus } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, DELETE, GET, Param, PATCH, Path, POST, Security } from "typescript-rest";
import { DbClientSingleton } from "../utils";
import { ContainersDb } from "./ContainersDb";
import { IDatabase } from "pg-promise";

@Path("v0.1/containers")
export class ContainersService implements IContainersService {
  @Inject private dbClient: DbClientSingleton;
  @Inject private containersDb: ContainersDb;
  @ContextRequest private request: Request;

  private db: Client;
  private dbp: IDatabase<any>;

  constructor() {
    this.db = this.dbClient.db;
    this.dbp = this.dbClient.dbp;
  }

  @Path("/getVesselTrackings")
  @GET
  public async getVesselTrackings() {
    const activeShips = await this.dbClient.db.query(`
      SELECT distinct vessel_container_id
      FROM shipments
      WHERE ata is null and vessel_container_id is not null`);

    const mmsi = activeShips.rows.map((val: any) => {
      return val.vessel_container_id.slice(val.vessel_container_id.indexOf("&") + 1);
    });
    return mmsi;
  }

  @Path("/updateVehicleLocation")
  @POST
  public async updateVehicleLocation(doc: any) {
    const queryName = await this.db.query(`SELECT name FROM vessels WHERE mmsi = $1 limit 1`, [doc.reference]);
    const vesselName = queryName.rows.length > 0 ? queryName.rows[0].name : null;
    const vesselContainerId = "ship&" + doc.reference;

    await this.db.query(`INSERT INTO vehicle_locations (vessel_container_id, vessel_name, lat, lng, signal_age, update_time, create_time, status, speed, course)
                           VALUES ($1, $2, $3, $4, $5, now(), now(), $6, $7, $8)`, [vesselContainerId, vesselName, doc.lat, doc.lng, doc.signal_time, doc.underway, doc.speed, doc.course]);
  }

  @Path("/getInventoryLocations")
  @GET
  public async getInventoryLocations(@Param("orgId") orgId: string) {
    const newQuery = `
      WITH RECURSIVE parents AS (
        SELECT container_id, parent_container_id, type,ref
        FROM containers
        WHERE container_id in (SELECT container_id from inventory_items where org_id = $1)
        UNION
        SELECT c.container_id, c.parent_container_id, c.type, c.ref
        FROM containers c
        INNER JOIN parents p ON c.container_id = p.parent_container_id
      )
      SELECT vehicle_destinations.eta, vehicle_destinations.pol, vehicle_destinations.pod, v1.vessel_name, v1.lat as vessel_lat,
             v1.lng as vessel_lng, buildings.*,parents.container_id, parents.type, parents.ref,parents.parent_container_id
      FROM parents
      LEFT JOIN buildings on parents.container_id = buildings.container_id
      LEFT JOIN vehicle_locations v1
        ON parents.container_id = v1.vessel_container_id
      LEFT JOIN vehicle_destinations on parents.container_id = vehicle_destinations.vessel_container_id
      LEFT OUTER JOIN vehicle_locations v2
        ON (parents.container_id = v2.vessel_container_id
        AND (v1.timestamp < v2.timestamp))
      WHERE v2.vessel_container_id IS NULL`;
    const locations = await this.db.query(newQuery, [orgId]);
    const cache: any = {};
    return { items: locations.rows, cache };
  }

  @Security("USER")
  @Path("getProductLocations")
  @GET
  public async getProductLocations(@Param("productId") productId: string) {
    const orgId = this.request.user.orgId;
    console.log("getProductLocations for pid and org: " + productId + "," + orgId);
    // this.request
    const locationInfo = await this.db.query(`
      SELECT containers.container_id as cd,vehicle_locations.vessel_name, vehicle_locations.vessel_container_id, buildings.container_id, buildings.name, inventory_items.quantity,purchase_order_items.quantity, shipment_items.delivery_quantity
      FROM purchase_order_items
      LEFT join inventory_items on
        purchase_order_items.purchase_order_item_id = inventory_items.purchase_order_item_id
        and purchase_order_items.purchase_order_id = inventory_items.purchase_order_id
        and purchase_order_items.sku = inventory_items.sku
        and inventory_items.quantity > 0
      LEFT JOIN shipment_items on purchase_order_items.purchase_order_id = shipment_items.purchase_order_id
        and purchase_order_items.purchase_order_item_id = shipment_items.purchase_order_item_id
        and purchase_order_items.sku = shipment_items.sku
      LEFT JOIN containers on inventory_items.container_id = containers.container_id
      LEFT JOIN vehicle_locations on vehicle_locations.vessel_container_id =
        (select distinct vehicle_locations.vessel_container_id from vehicle_locations
        where vehicle_locations.vessel_container_id = containers.parent_container_id)
      LEFT JOIN buildings on
        (purchase_order_items.origin_container = buildings.container_id and
        inventory_items.quantity is null and shipment_items.delivery_time is null)
        or (containers.container_id = buildings.container_id)
        or  (purchase_order_items.dst_container = buildings.container_id and shipment_items.delivery_time is not null)
      WHERE purchase_order_items.org_id = $1 and purchase_order_items.sku = $2 GROUP BY cd, vehicle_locations.vessel_name, vehicle_locations.vessel_container_id, buildings.container_id, buildings.name, inventory_items.quantity,purchase_order_items.quantity, shipment_items.delivery_quantity`,
      [orgId, productId]);

    const locations: any = [];
    const sumDict: any = {};

    // console.dir(locationInfo.rows);
    for (const li of locationInfo.rows) {

      if (!li.name && !li.container_id) {
        li.name = li.cd;
        li.container_id = li.cd;
      }

      if (li.vessel_name || li.vessel_container_id) {
        li.name = li.vessel_name;
        li.container_id = li.vessel_container_id;
      }

      if (li.delivery_quantity && li.delivery_quantity > 0) {
        li.quantity = li.delivery_quantity;
      }

      if (sumDict[li.name]) {
        sumDict[li.name].quantity += li.quantity;
      } else {
        sumDict[li.name] = { quantity: li.quantity, containerId: li.container_id };
      }
    }
    for (const name of Object.keys(sumDict)) {
      const newObj: any = {};
      newObj.name = name;
      newObj.quantity = sumDict[name].quantity;
      newObj.containerId = sumDict[name].containerId;
      locations.push(newObj);
    }
    console.dir(locations);

    return locations;
  }

  @Path("/queryVehicleLocations")
  @GET
  public async queryVehicleLocations(
    @Param("whereClause") whereClause: any, @Param("whereParams") whereParams: any,
    @Param("groupBy") groupBy: any, @Param("orderBy") orderBy: any) {

    let iiSql;
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }
    if (!groupBy) {
      iiSql = `SELECT * FROM vehicle_locations` + sqlEnd;
    }
    const iiRes = await this.db.query(iiSql, whereParams);
    const res = { items: iiRes.rows };
    return res;
  }

  // gets the alternate names for containers(vessel_name/buildings.name)
  @Path("/getNames")
  @GET
  public async getNames() {
    const query = `SELECT buildings.name, vehicle_locations.vessel_name, containers.container_id
                FROM containers left join buildings on buildings.container_id = containers.container_id
                left join vehicle_locations on vehicle_locations.vessel_container_id = containers.container_id
                group by buildings.name, vehicle_locations.vessel_name, containers.container_id`;

    const containerNames = await this.db.query(query);
    return containerNames.rows;
  }

  // fix... moving away from these ids
  @Path("/info")
  @GET
  public async info(@Param("containerId") containerId: any): Promise<Container> {
    const result = await this.db.query("SELECT * FROM containers WHERE container_id = $1", [containerId]);
    return result.rowCount > 0 ? this.dbToContainer(result.rows[0]) : null;
  }

  @Path("/queryInventory")
  @GET
  public async queryInventory(@Param("whereClause") whereClause: string,
                              @Param("whereParams") whereParams: any,
                              @Param("groupBy") groupBy: string[],
                              @Param("orderBy") orderBy: any) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }
    let iiSql;
    if (!groupBy || !groupBy.length) {
      iiSql = "SELECT *  FROM inventory_items" + sqlEnd;
    } else {
      iiSql = "SELECT ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          iiSql += " ,";
        }
        iiSql += groupBy[i];
      }
      iiSql += " FROM inventory_items" + sqlEnd;
    }
    const iiRes = await this.db.query(iiSql, whereParams);
    let cache;
    console.log("iiRes = %j", iiRes);
    // let cache = await this.cacheContainersForItems(whereClause, whereParams);

    // debug('cache = %j', cache);
    const res = { items: iiRes.rows, cache };
    return res;
  }

  @GET
  @Path("/getProductsForContainer")
  public async getProductsForContainer(@Param("containerId") containerId: any) {
    /// parsing the containerId allows us to avoid making multiple queries this is relient on container id syntax staying the same
    let containerType = "";
    let skusForContainer;
    for (const i in containerId) {
      if (containerId[i] === "&") {
        break;
      } else {
        containerType += containerId[i];
      }
    }
    console.log("container type", containerType);
    if (containerType === "ship") {
      // This is a temporary query that will not work for nested containers
      // need to update on next demo iteration
      const query1 = `SELECT v1.vessel_name as vessel_name,
                  v1.lat as lat,
                  v1.lng as lng,
                  products.name as productname,
                  products.sku,
                  products.department,
                  products.image,
                  inventory_items.purchase_order_id,
                  inventory_items.purchase_order_item_id,
                  inventory_items.quantity as containerquantity,
                  inventory_items.container_id,
                  inventory_items.shipment_id,
                  containers.type
                  FROM inventory_items
                  LEFT JOIN products on inventory_items.sku = products.sku
                  LEFT JOIN containers on inventory_items.container_id = containers.container_id
                  LEFT JOIN vehicle_locations v1 on containers.parent_container_id = v1.vessel_container_id LEFT OUTER JOIN vehicle_locations v2 ON
                  (containers.parent_container_id = v2.vessel_container_id AND (v1.timestamp < v2.timestamp)) WHERE v2.vessel_container_id IS NULL
                  and containers.parent_container_id = $1
                  group by inventory_items.container_id, inventory_items.shipment_id, containers.type, containers.parent_container_id, productname, products.sku,products.image,products.department,  inventory_items.purchase_order_id, inventory_items.purchase_order_item_id,inventory_items.quantity, inventory_items.sku, v1.vessel_name,v1.lat,v1.lng;`;

      const inventoryItems = await this.db.query(query1, [containerId]);
      skusForContainer = inventoryItems;

    }
    if (containerType === "port") {
      const query2 = `SELECT products.sku,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(shipment_items.delivery_quantity) as containerQuantity
                    FROM shipments
                    LEFT JOIN shipment_items on shipment_items.shipment_id=shipments.shipment_id
                    LEFT JOIN purchase_order_items on shipment_items.purchase_order_id = purchase_order_items.purchase_order_id
                    AND shipment_items.purchase_order_item_id = purchase_order_items.purchase_order_item_id
                    LEFT JOIN buildings on buildings.container_id = purchase_order_items.dst_container
                    LEFT JOIN products on products.sku = purchase_order_items.sku
                    WHERE destination_container_id = $1
                    AND shipment_items.delivery_quantity >0
                    GROUP BY products.sku,products.department, products.image, buildingname, productname,lat, lng`;

      const destinationItems = await this.db.query(query2, [containerId]);
      skusForContainer = destinationItems;
    }
    if (containerType === "shipping-container") {
      const query5 = `SELECT containers.type, containers.container_id,products.sku,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(inventory_items.quantity) as containerquantity
          FROM inventory_items
          JOIN products on products.sku = inventory_items.sku
          LEFT JOIN containers on inventory_items.container_id= containers.container_id
          LEFT JOIN buildings on buildings.container_id = containers.parent_container_id
          WHERE inventory_items.container_id= $1
          GROUP BY products.sku,products.department, containers.type, containers.container_id,products.image, buildingname, productname,lat, lng;`;
      skusForContainer = await this.db.query(query5, [containerId]);
    }
    if (containerType === "factory") {
      const query3 = `SELECT products.sku,containers.type, containers.container_id,products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(purchase_order_items.quantity -purchase_order_items.recv_quantity) as containerquantity
                      FROM purchase_order_items
                      JOIN products on products.sku = purchase_order_items.sku
                      LEFT JOIN containers on purchase_order_items.origin_container= containers.container_id
                      LEFT JOIN buildings on buildings.container_id = purchase_order_items.origin_container
                      WHERE purchase_order_items.origin_container = $1
                      AND (purchase_order_items.quantity -purchase_order_items.recv_quantity)>0
                      GROUP BY products.sku,containers.type, containers.container_id,products.department, products.image, buildingname, productname,lat, lng`;

      skusForContainer = await this.db.query(query3, [containerId]);
    }

    if (containerType === "warehouse") {
      const query4 = `SELECT products.sku, containers.type, containers.container_id, products.department, products.image, products.name as productname, buildings.name as buildingname,buildings.lat as lat, buildings.lng as lng, sum(inventory_items.quantity) as containerquantity
      FROM inventory_items
      JOIN products on products.sku = inventory_items.sku
      LEFT JOIN containers on inventory_items.container_id= containers.container_id
      LEFT JOIN buildings on buildings.container_id = containers.container_id
      WHERE inventory_items.container_id= $1
      GROUP BY products.sku,products.department, products.image, buildingname,containers.type, containers.container_id, productname,lat, lng`;
      skusForContainer = await this.db.query(query4, [containerId]);
    }
    return { items: skusForContainer.rows } as any;
  }

  @GET
  @Path("/getBuilding")
  public async getBuilding(@Param("containerId") containerId: string)
  : Promise<ContainersResponse> {
    console.log("get building")

    const result = await this.dbp.oneOrNone("SELECT * FROM buildings WHERE container_id = $1", [containerId]);

    const cResp = new ContainersResponse(result
      ? {status: ResponseStatus.OK, building: this.dbToBuilding(result)}
      : {status: ResponseStatus.NO_RESULTS});

    console.dir(cResp);
    return cResp;
  }

  @GET
  @Path("/GetVesselLocations")
  public async GetVesselLocations(@Param("orgid") orgid: string)
   {
    const sql = `select vessel_container_id,vessel_name,lat,lng,signal_age,speed,last_port from vehicle_locations where  vessel_container_id in (select vessel_container_id from  shipments where  org_id = '${orgid}');`;
    return await this.db.query(sql);
  }



  @GET
  @Path("/queryBuildings")
  public async queryBuildings(@Param("whereClause") whereClause: string,
                              @Param("whereParams") whereParams: string[],
                              @Param("groupBy") groupBy: string[],
                              @Param("orderBy") orderBy: string[]) {

                                
    let sqlEnd = "";
    if (whereClause) {
      console.log("Qury Buildings where Exists"+whereClause);
      sqlEnd += " WHERE " + whereClause;
    }
    else
    {
      console.log("Qury Buildings where clause doesnt exist");
    }
    if (groupBy) {
      sqlEnd += " GROUP BY ";

      for (let i = 0; i < groupBy.length; i++) {

        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }
    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }
    // let sql = `SELECT DISTINCT ON(name) container_id, name, address, lat, lng from buildings` + sqlEnd;
    const sql = `select * from buildings ${sqlEnd}`;
    return await this.db.query(sql, whereParams);
  }

  @POST
  @Path("/addBuilding")
  public async addBuilding(@Param("Obj") Obj: any, @Param("orgId") orgId: string): Promise<any> {
    const ref = (Obj.container_id.slice(Obj.container_id.indexOf("&") + 1));
    const timeZoneId = await this.getTimezone(Obj.lat, Obj.lng);
    const productExist = await this.db.query(` Select * from buildings where container_id = $1 and org_id = $2 `, [Obj.container_id, orgId]);
    if (productExist.rows.length < 1) {
      await this.db.query(`INSERT INTO buildings(container_id, address, name, lat, lng, type, ref, time_zone_id, org_id)
                                  VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)`, [Obj.container_id, Obj.address, Obj.name, Obj.lat, Obj.lng, Obj.type, ref, timeZoneId, orgId]);
      return;
    } else {
      return;
    }
  }

  @DELETE
  @Path("/deleteBuilding")
  public async deleteBuilding(@Param("containerId") containerId: any, @Param("name") name: string, @Param("orgId") orgId: any) {
    await this.db.query(` DELETE from buildings where (container_id = $1 and org_id = $2) and name = $3`, [containerId, orgId, name]);
    return;
  }

  @PATCH
  @Path("/editBuilding")
  public async editBuilding(@Param("Obj") Obj: any, @Param("previousContainerId") previousContainerId: any, @Param("orgId") orgId: string) {
    const timeZoneId = await this.getTimezone(Obj.lat, Obj.lng);
    await this.db.query(` UPDATE buildings set container_id = $1, address = $2, name = $3, lat = $4, lng = $5, type = $6, ref = $7, time_zone_id=$8 where org_id = $9 and container_id = $10`,
      [Obj.container_id, Obj.address, Obj.name, Obj.lat, Obj.lng, Obj.type, Obj.ref, timeZoneId, orgId, previousContainerId]);
    return;
  }

  @POST
  @Path("/addFeedsBuilding")
  public async addFeedsBuilding(@Param("Obj") Obj: any, @Param("orgId") orgId: string): Promise<any> {
    let value: any = "";
    for (const [index, i] of Obj.entries()) {
      const productExist = await this.db.query(` Select * from buildings where container_id = $1 and org_id = $2 `, [i.container_id, orgId]);

      if (productExist.rows.length < 1) {
        value += "('" + i.container_id + "','" + i.address + "','" + i.name + "','" + i.lat + "','" + i.lng + "','" + i.type + "','" + i.ref + "','" + i.timeZoneId + "','" + orgId + "')";
        if ( index < Obj.length - 1 ) {
          value += ",";
        } else {
          value += ";";
        }
      }
    }
    if (value.length > 0) {
      await this.db.query(`INSERT INTO buildings(container_id, address, name, lat, lng, type, ref, time_zone_id, org_id)
                                VALUES ${value}`);
    }
    return;
  }

  public async updateContainerHolder(timestamp: any, container: any, holder: any) {
    console.log("updateContainerHolder: %j, %j", container, holder);
    // let entry = Object.assign({parent_id:holder.getId()},container);
    const res = await this.db.query("UPDATE containers SET parent_id = ? WHERE id = ?",
      [holder.getId(), container.getId()]);
    console.log("res = %j", res);
  }

  public async addContainer(containerInfo: any) {
    return await this.containersDb.addContainer(containerInfo);
  }

  public async queryContainer(whereClause: string, whereParams: string[], groupBy?: any, orderBy?: any) {
    return await this.containersDb.queryContainer(whereClause, whereParams, groupBy, orderBy);
  }

  public async updateContainer(updateObj: any, whereClause: any, whereParams: any) {
    return await this.containersDb.updateContainer(updateObj, whereClause, whereParams);
  }

  public async updateContainerInventory(timestamp: any, container: any, inventory: any) {
    console.log("updateContainerInventory: %j, %j", container, inventory);
    const items = inventory.items;
    const cid = container.getId();

    // tslint:disable-next-line: forin
    for (const i in items) {
      console.log("item[%i]: %j", i, items[i]);
      // todo, can do in parallel
      // const id = cid + ":inv&" + items[i].getId();
      const entry = Object.assign({ container_id: cid }, items[i]);
      entry.container_id = cid;
      await this.db.query("INSERT INTO inventory SET ?", entry);
    }

  }

  public async addInventoryItem(inventoryItem: any) {
    // tslint:disable-next-line: max-line-length
    const { org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity, receive_date } = inventoryItem;
    // tslint:disable-next-line: max-line-length
    const insertArr = [org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity, receive_date];
    const newItem = await this.db.query(
      "INSERT INTO inventory_items (org_id, container_id, sku, purchase_order_id, purchase_order_item_id, qty_per_carton, quantity,receive_date) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)", insertArr);
    return newItem;
  }

  private dbToContainer(row: any): Container {
    return new Container({containerId: row.container_id, type: row.type, ref: row.ref, parentContainerId: row.parent_container_id});
  }

  private dbToBuilding(row: any): Building {
    return new Building({containerId: row.container_id, name: row.name, lat: row.lat, lng: row.lng });
  }

  private async getTimezone(lat: any, long: any) {
    const apiKey = "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk";
    const url = "https://maps.googleapis.com/maps/api/timezone/json?location=" + lat + "," + long + "&timestamp=1331161200&key=" + apiKey;

    const response = await fetch(url);

    const data = await response.json();

    return data.timeZoneId;
  }

}
