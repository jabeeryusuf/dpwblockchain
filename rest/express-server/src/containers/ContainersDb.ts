import { Client } from "pg";
import { Inject } from "typescript-ioc";
import { DbClientSingleton } from "../utils/DbClientSingleton";
// import { v4 as uuid } from "uuid";

export class ContainersDb {

  private db: Client;
  @Inject private dbClient: DbClientSingleton;

  constructor() {
    this.db = this.dbClient.db;
  }

  public async addContainer(containerInfo: any ) {
    const { container_id, type, ref } = containerInfo;

    await this.db.query("INSERT INTO containers (container_id,type,ref) VALUES ($1,$2,$3)", [container_id, type, ref]);
  }




  public async queryContainer(whereClause: string, whereParams?: string[],
                              groupBy?: string[], orderBy?: string[]) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i++; i < groupBy.length) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i++; i < orderBy.length) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }

    const iiSql = "SELECT * from containers" + sqlEnd;

    const iiRes = await this.db.query(iiSql, whereParams);
    const rows = iiRes.rows;
    const res = { items: rows };
    return res;
  }

  public async updateContainer(updateObj: any, whereClause: string, whereParams: string[]) {
    let iiSql = "UPDATE containers SET ";
    let sqlEnd = "";
    let first: boolean = true;

    for (const key of Object.keys(updateObj)) {
      if (first === false) {
        iiSql += ",";
      }

      first = false;
      iiSql += " " + String(key) + " = '" + String(updateObj[key]) + "' ";
    }

    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    iiSql += sqlEnd;

    console.log(iiSql);

    try {
      this.db.query(iiSql, whereParams);
    } catch (err) {
      console.log("update", err);
    }
  }

}
