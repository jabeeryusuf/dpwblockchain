import { Request } from "express";
import { ITemplatesService, MessageTemplate } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, PATCH, Path, Security } from "typescript-rest";
import { v4 as uuid } from "uuid";
import { TemplatesDb } from "./TemplatesDb";

@Path("/v0.1/messages/templates")
@Security("USER")
export class TemplatesService implements ITemplatesService {

  @ContextRequest private request: Request;
  @Inject private templatesDb: TemplatesDb;

  @GET
  @Path("/list")
  public async list(): Promise<MessageTemplate[]> {
    const orgId = this.request.user.orgId;
    const rows = await this.templatesDb.getMessageTemplates(orgId);
    return rows.map( (t: any) => ({
      messageTemplate: t.message_template,
      messageTemplateId: t.message_template_id,
      name: t.name,
      orgId: t.org_id,
      subjectTemplate: t.subject_template,
    }));
  }

  @PATCH
  @Path("/update")
  public async update(template: MessageTemplate) {
    await this.templatesDb.saveTemplates({
      message_template: template.messageTemplate,
      message_template_id: uuid(),
      name: template.name,
      org_id: template.orgId,
      subject_template: template.subjectTemplate,
    });
  }

}
