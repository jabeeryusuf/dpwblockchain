CREATE TABLE message_templates (
   message_template_id  UUID  NOT NULL,
   org_id               TEXT  NOT NULL,
   name                 TEXT,
   subject_template     TEXT,
   message_template     TEXT,

   CONSTRAINT message_template_const_org_name UNIQUE(org_id,name)
);