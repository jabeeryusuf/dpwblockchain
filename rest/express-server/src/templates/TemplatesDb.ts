import { Client } from "pg";

import { Inject, Singleton } from "typescript-ioc";
import { DbClientSingleton } from "../utils";

// tslint:disable-next-line: interface-name class-name

@Singleton
export class TemplatesDb {

  public db: Client;

  @Inject
  private dbClient: DbClientSingleton;

  constructor() {
    this.db = this.dbClient.db;
  }

  public async getMessageTemplates(orgId: string) {
    console.log("MESSAGE DB GET TEMPLATES");
    const query = await this.db.query(
      `SELECT * FROM message_templates WHERE org_id=$1`, [orgId]);
    return query.rows;
  }

  public async saveTemplates(row: any) {
    const upsert = await this.db.query(`
      INSERT INTO message_templates
        (message_template_id, org_id, name, subject_template, message_template)
      VALUES ($1, $2, $3, $4, $5)
      ON CONFLICT ON CONSTRAINT message_template_const_org_name DO UPDATE
      SET message_template = EXCLUDED.message_template`,
      [row.message_template_id, row.org_id, row.name, row.subject_template, row.message_template] );

    return upsert;
  }

}
