"use-strict";

import * as AWS from "aws-sdk";
import * as fs from "fs";
import * as path from "path";
import { Client } from "pg";
import { Inject } from "typescript-ioc";
import * as util from "util";
import { v4 as uuid } from "uuid";
import { DbClientSingleton } from "../utils";

export class FilesService {

  @Inject private dbClient: DbClientSingleton;
  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  public async uploadFile(fileName: string, filePath: string, userId: string) {
    console.log("UPLOAD FILE");
    AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "" });
    const S3 = new AWS.S3({ params: { Bucket: "teuchain" } });
    const readFileFunc = await util.promisify(fs.readFile);
    const s3Upload = await util.promisify(S3.putObject.bind(S3));
    const uploadFile = await readFileFunc(filePath);
    const fileID = uuid();
    await s3Upload({ Bucket: "teuchain", Key: "user-uploads/" + fileID, Body: uploadFile });
    // add a reference to the s3 bucket into the database
    await this.db.query("INSERT INTO files (file_id,uploader_id,s3_bucket,original_name) values ($1,$2,$3,$4)", [fileID, userId, fileID, fileName]);
    // associate the file to a case in the db
    await this.deleteFileFromServer(filePath);
    return fileID;
  }
  public async deleteFileFromServer(filePath: string) {
    const unlinkAsync = await util.promisify(fs.unlink);
    return await unlinkAsync(filePath);
  }
  public async addFileToCase(fileId: string, caseId: string) {
    console.log("add file to case");
    const newCaseFile = await this.db.query("INSERT INTO case_files (case_id,file_id) VALUES ($1,$2)", [caseId, fileId]);
    return newCaseFile;
  }

  /**
   * Uploads an image file into S3 as a public read and inserts that image file into org_settings table with key "logo"
   * @param {string} fileName
   * @param {string} filePath
   * @param {string} org_Id
   * @param {string} key
   */
  public async uploadPublicFile(fileName: string, filePath: string, userId: string) {
    AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "ZKu2SnEZlBj35Dyp+VFfROlnu2YLB5LYHGSmcaoa" });
    const S3 = new AWS.S3({ params: { Bucket: "teuchain-public" } });
    const readFileFunc = await util.promisify(fs.readFile);
    const s3Upload = await util.promisify(S3.putObject.bind(S3));
    const uploadFile = await readFileFunc(filePath);
    const fileID = uuid();
    await s3Upload({ Bucket: "teuchain-public", Key: "email-logos/" + fileID, Body: uploadFile, ACL: "public-read" });
    await this.db.query("INSERT INTO files (file_id,uploader_id,s3_bucket,original_name) values ($1,$2,$3,$4)", [fileID, userId, fileID, fileName]);
    return fileID;
  }

  public async retrieveFilesFroms3(itemId: any) {
    console.log("RETREIVE FILE:", itemId);
    AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "ZKu2SnEZlBj35Dyp+VFfROlnu2YLB5LYHGSmcaoa" });
    const S3 = new AWS.S3({ params: { Bucket: "teuchain" } });
    const s3Retreive = await util.promisify(S3.getObject.bind(S3));
    const retreivedObj = await s3Retreive({ Bucket: "teuchain", Key: "user-uploads/" + itemId });
    console.log(retreivedObj)
    return retreivedObj;
  }

  public async queryFiles(whereClause: string, whereParams: any, groupBy: string, orderBy: string) {
    let sql = "SELECT * from files";
    if (whereClause) {
      sql += " WHERE ";
      sql += whereClause;
    }
    if (groupBy) {
      sql += " GROUP BY ";
      sql += groupBy;
    }
    if (orderBy) {
      sql += " ORDER BY ";
      sql += orderBy;
    }
    const files = await this.db.query(sql, whereParams);
    const s3Obj = [];

    for (const row of files.rows) {
      s3Obj.push(this.retrieveFilesFroms3(row.s3_bucket));
    }
    const objs = await Promise.all(s3Obj);
    const writeFileFunc = util.promisify(fs.writeFile);
    for (let i = 0; i < files.rows.length; i++) {
      const folderPath = path.join(__dirname, "..", "..", "..", "Downloads/", files.rows[i].file_id);

      if (!fs.existsSync(folderPath)) {
        if (!fs.existsSync(path.join(__dirname, "..", "..", "..", "Downloads/"))){
          fs.mkdirSync(path.join(__dirname, "..", "..", "..", "Downloads/"));
        }
        fs.mkdirSync(path.join(folderPath));
      }
      await writeFileFunc(path.join(folderPath, files.rows[i].original_name), objs[i].Body);
    }
    console.log(path.join(path.join(__dirname, "..", "..", "..", "Downloads/", files.rows[0].file_id), files.rows[0].original_name));
    return path.join(path.join(files.rows[0].file_id), files.rows[0].original_name);
  }

}
