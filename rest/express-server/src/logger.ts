// const winston = require('winston');
import { createLogger, format, transports } from "winston";
// const { combine, timestamp, label, printf } = format;

/* const consoleFormat = format.printf((info: any) => {
  //  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  return `${info.level}: ${info.message}`;
});
 */
export const logger = createLogger({
  format: format.combine(
    // format.label('simple'),
    // format.timestamp(),
    format.colorize(),
    format.splat(),
    //    consoleFormat
    format.simple(),
  ),
  transports: [new transports.Console()],
});
