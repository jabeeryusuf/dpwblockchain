import * as convict from "convict";

// Define a schema
export const config = convict({
  env: {
    default: "development",
    doc: "The application environment.",
    env: "NODE_ENV",
    format: ["production", "staging", "development"],
  },
  ip: {
    default: "127.0.0.1",
    doc: "The IP address to bind.",
    env: "IP_ADDRESS",
    format: "ipaddress",
  },
  port: {
    arg: "port",
    default: 5000,
    doc: "The port to bind.",
    env: "PORT",
    format: "port",
  },
  dbUri: {
    arg: "db-uri",
    default: "postgres://abdulrahman:abdulrahman_dev@newdb.cfmnkf5hdw75.eu-west-1.rds.amazonaws.com:5432/rashed",
    doc: "The db addr.",
    env: "DB_URI",
    format: "*",
  },
});

// Load environment dependent configuration
const env = config.get("env");

config.loadFile("./config/" + env + ".json");

// Perform validation

console.log("before validatee");
console.log("Connecting pqr");
config.validate({allowed: "strict"});
