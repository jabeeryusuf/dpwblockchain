#!/usr/bin/env node

import { Server } from "../Server";
import { config } from "./config";

/**
 * Read in config
 */

const port = config.get("port");

 /**
  * Create HTTP server.
  */

const server = new Server(port);

/**
 * Start the server
 */

(async () => {
  await server.startServer();
  console.log("Server Started");

})();
