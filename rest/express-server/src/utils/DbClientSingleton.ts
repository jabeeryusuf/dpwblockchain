import { Client } from "pg";
import { IMain, IDatabase } from "pg-promise";
import * as pgPromise from "pg-promise";
import { Singleton } from "typescript-ioc";
import { config } from "../bin/config";

@Singleton
export class DbClientSingleton {

  public db: Client;
  public dbp: IDatabase<any>;
  public pgp: IMain;

  constructor() {
    const dbUri = config.get("dbUri");

    console.log(`Connecting to environment: ${config.get("env")}`)

    this.db = new Client({
      connectionString: dbUri,
      ssl: true,
    });

    this.pgp = pgPromise({capSQL: true});

    this.dbp = this.pgp(dbUri);
  }

  public async connect() {

    await this.db.connect();
    await this.dbp.connect();

    this.db.on("error", (err) => {
      throw new Error("something bad has happened!");
    });

    console.log("CONNECTED");
  }
}
