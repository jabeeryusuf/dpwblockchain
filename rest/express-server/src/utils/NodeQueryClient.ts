// import moment from "moment-timezone";
import * as AWS from "aws-sdk";
import * as fs from "fs";
import * as path from "path";
import { Client } from "pg";
import { Inject } from "typescript-ioc";
import { GET, Param, Path, POST } from "typescript-rest";
import * as util from "util";

import { DbClientSingleton } from "./DbClientSingleton";

@Path("/v0.1")
export class NodeQueryClient {

  @Inject public dbClient: DbClientSingleton;
  public db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  @GET
  @Path("/general")
  public async getStuff(@Param("sql") sql: string) {
    let r = await this.db.query(sql);
    return r.rows;
  }

  @POST
  @Path("/config/settings")
  public async changeConfigSettings(body: {intervals: any}) {
    const {intervals} = body;
    const sqlStart = `UPDATE config_settings AS cs SET number = i.number, interval = i.interval FROM( VALUES `;
    let sqlMid = "";
    const sqlEnd = ") AS i (setting, number, interval) WHERE cs.setting = i.setting";
    const keyList = [];

    for (const key in intervals) {
      if (intervals.hasOwnProperty(key)) {
        keyList.push(key);
      }
    }

    for (let i = 0; i < keyList.length; i++) {
      if (keyList[i].includes("Int")) {
        sqlMid += (`'` + intervals[keyList[i]] + `')`);
        if (i < keyList.length - 1) {
          sqlMid += `, `;
        }
      } else {
        sqlMid += (`('` + keyList[i] + `', ` + intervals[keyList[i]] + `, `);
      }
    }

    const sql = sqlStart + sqlMid + sqlEnd;
    try {
      const res = await this.db.query(sql);
      return res;
    } catch (err) {
      return { errorMessage: err.toString() };
    }
  }

  @POST
  @Path("/dataPull/submitPull")
  public async submitPull(body: {org: any, service: any, username: any, password: any, pullInterval: any}) {
    const {org, username, password, service, pullInterval} = body;

    const newSubmit = await this.db.query(`
      INSERT INTO external_accounts (org, external_username, external_password, service, interval) 
      VALUES ($1, $2, $3, $4, $5)`,
      [org, username, password, service, pullInterval]);
    return newSubmit;
  }

  @GET
  @Path("/dataPull/existingServices")
  public async queryExistingServices(@Param("whereClause") whereClause: any) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    const sql = "SELECT external_username, service FROM external_accounts" + sqlEnd;
    const result = await this.db.query(sql);
    return result;
  }

  @GET
  @Path("/report/query")
  public async queryReport(@Param("dest") dest: any) {
    const list = [];
    const sqlOne = `select a.org_country, a.port_of_loading, sum(a.quantity)
                  from (select d.org_country, d.port_of_loading, d.quantity
                        from (select org_country, port_of_loading, mcc_warehouse, quantity
                              from purchase_order_items
                              where dst_container = '` + dest + `' group by org_country, port_of_loading, mcc_warehouse, quantity)
                        d order by org_country) a group by a.org_country, a.port_of_loading;`;

    const resOne = await this.db.query(sqlOne);
    for (const obj of resOne.rows) {
      if (obj.port_of_loading === "port&YTN") {
        obj.mcc = "Shenzen";
      } else {
        obj.mcc = "Shanghai";
      }
    }
    list.push(resOne);

    const sqlTwo = `select e.etd, e.eta, f.department, f.sum
                  from shipments e right join
                  (select c.shipment_id, d.department, sum(d.quantity) as sum
                  from shipment_items c left join
                  (select a.department, quantity, b.purchase_order_item_id, b.purchase_order_id
                  from products a right join purchase_order_items b on a.sku = b.sku)
                  d on c.purchase_order_item_id = d.purchase_order_item_id and c.purchase_order_id = d.purchase_order_id group by shipment_id, department)
                  f on e.shipment_id = f.shipment_id where e.destination_container_id ='` + dest + `' group by etd, eta, department, sum;`;

    const resTwo = await this.db.query(sqlTwo);
    list.push(resTwo);

    const sqlThree = `select department, count(b.sku), b.approx_recv_date, sum(quantity) as sum
                    from products a left join purchase_order_items b on a.sku = b.sku
                    where b.dst_container='` + dest + `' group by approx_recv_date, department;`;

    const resThree = await this.db.query(sqlThree);
    list.push(resThree);

    // let sqlEnd = select + "\'" + dest + "\' " + group;
    // let iiRes = await this.db.query(sqlEnd);
    return list;
  }

  // BELOW HERE SHOULD BE REFACTORED OR DELETED

  // Adding Product Catalog
  public async addProductCatalog(e: any, org: any) {
    const sku = e.payload.sku;
    const apn = e.payload.apn;
    const name = e.payload.name;
    const department = e.payload.department;
    const image = e.payload.image;
    const outer_carton_height = e.payload.outer_carton_height;
    const outer_carton_width = e.payload.outer_carton_width;
    const outer_carton_length = e.payload.outer_carton_length;
    const pack_size = e.payload.pack_size;
    const carton_cbm = e.payload.carton_cbm;
    try {
      const skuExist = await this.db.query(`SELECT * from products where sku = '${sku}' and org_id = '${org}'`);
      if (skuExist.rows.length < 1) {
        const productsql = `INSERT into products(sku, apn, name, department, image, outer_carton_height,
                                                    outer_carton_width,outer_carton_length,pack_size,carton_cbm, org_id)
                            VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`;
        const res = await this.db.query(productsql, [sku, apn, name, department, image, outer_carton_height, outer_carton_width, outer_carton_length, pack_size, carton_cbm, org]);
        return res;
      } else {
        const productsql = `UPDATE products set apn=$1, name=$2, department=$3,
                                image=$4, outer_carton_height=$5, outer_carton_width=$6, outer_carton_length=$7, pack_size=$8, carton_cbm=$9 where org_id=$10 and sku=$11`;
        const res = await this.db.query(productsql, [apn, name, department, image, outer_carton_height, outer_carton_width, outer_carton_length, pack_size, carton_cbm, org, sku]);
        return res;
      }
    } catch (err) {
      throw new Error(" There is a problem with data ");
    }
  }

  public async uploadFileToS3(filedata: any, fileName: string, orgId: string, fileId: number) {
    try {
      console.log(filedata);
      console.log(fileName);
      console.log(orgId);
      console.log(fileId);
      // making a temp directory for files
      const dir = path.join(__dirname, "../", "s3UploadDirectory");
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      const writeFileFunc = util.promisify(fs.writeFile);
      await writeFileFunc(dir + "/" + fileId, filedata);

      AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "ZKu2SnEZlBj35Dyp+VFfROlnu2YLB5LYHGSmcaoa" });
      const S3 = new AWS.S3({ params: { Bucket: "teuchain" } });

      // const readFileFunc = await util.promisify(fs.readFile);
      const s3Upload = await util.promisify(S3.putObject.bind(S3));
      await s3Upload({ Bucket: "teuchain", Key: "user-uploads/" + orgId + "/" + fileId, Body: filedata });

      // add a reference to the s3 bucket into the database
      const newFile = await this.db.query("INSERT INTO feeds_activity (feeds_activity_id, file_name, org_id, feeds_status) values ($1,$2,$3,$4)", [fileId, fileName, orgId, "uploaded"]);
      return newFile;
    } catch (err) {
      throw new Error("Not able to upload file");
    }
  }

  public async retrieveFilesFroms3(fileId: number, orgId: string) {
    AWS.config.update({ region: "us-west-2", accessKeyId: "", secretAccessKey: "ZKu2SnEZlBj35Dyp+VFfROlnu2YLB5LYHGSmcaoa" });
    const S3 = new AWS.S3({ params: { Bucket: "teuchain" } });
    const s3Retreive = await util.promisify(S3.getObject.bind(S3));
    const retreivedObj = await s3Retreive({ Bucket: "teuchain", Key: "user-uploads/" + orgId + "/" + fileId });
    return retreivedObj.Body;
  }

  public async updateFeedActivity(status: string, feedsActivityId: string) {
    try {
      const updateFeedsActivityStatus = await this.db.query(`UPDATE feeds_activity set feeds_status='${status}' where feeds_activity_id = '${feedsActivityId}'`);
      return updateFeedsActivityStatus.rows;
    } catch (err) {
      return err;
    }
  }

  public async getAllProducts() {
    const res = await this.db.query("SELECT * FROM products");
    return res;
  }

  public async updateInventoryItem(updateObj: any, whereClause: any, whereParams: any) {
    let iiSql = "UPDATE inventory_items SET ";
    let sqlEnd = "";
    let first = true;
    // tslint:disable-next-line: forin
    for (const key in updateObj) {
      if (first === false) {
        iiSql += ",";
      } else {
        first = false;
      }
      iiSql += " " + String(key) + " = '" + String(updateObj[key]) + "' ";
    }
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }
    iiSql += sqlEnd;
    try {
      this.db.query(iiSql, whereParams);
    } catch (err) {
      console.log("update", err);
    }
  }
  public async createInventoryItem(inventoryItem: any) {
    return await this.db.query("INSERT INTO inventory_items  SET ?", inventoryItem);

  }


  public async queryPendingActions(orgID: any) {
    const query = await this.db.query(`SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name
                                     FROM actions a
                                     LEFT JOIN containers c ON a.container_id = c.container_id
                                     LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                     WHERE a.action_status = 'pending' AND requestor_id=$1`, [orgID]);
    return query.rows.length > 0 ? query.rows : [];
  }

  public async queryResolvedActions(orgID: any) {
    const query = await this.db.query(`SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name
                                     FROM actions a
                                     LEFT JOIN containers c ON a.container_id = c.container_id
                                     LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                     WHERE a.action_status != 'pending' AND requestor_id=$1`, [orgID]);
    return query.rows.length > 0 ? query.rows : [];
  }

  public async queryVehicleMap(whereClause: string, whereParams: string[],
    groupBy?: string[], orderBy?: string[]) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }

    const iiSql = "SELECT *  from vessels" + sqlEnd;
    const iiRes = await this.db.query(iiSql, whereParams);
    const rows = iiRes.rows;
    const res = { items: rows };
    return res;
  }

  public async queryScrapeMaps(whereClause: string, whereParams: string[], groupBy?: string[], orderBy?: string[]) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }

    const iiSql = "SELECT * from scrape_maps" + sqlEnd;
    const iiRes = await this.db.query(iiSql, whereParams);
    const rows = iiRes.rows;
    const res = { items: rows };
    return res;
  }

  public async insertIntoScrapeMaps(scope: string, input: string, output: any) {
    const query = await this.db.query(`INSERT INTO scrape_maps (scope, input, output) VALUES ($1, $2, $3)`, [scope, input, output]);
    return query;
  }


}
