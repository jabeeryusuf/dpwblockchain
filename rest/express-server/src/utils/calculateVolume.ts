export const calculateVolume = (qty: number, product: any) => {
  if (product.pack_size) {
    const cbm = (product.outer_carton_height / 100) *
        (product.outer_carton_width / 100) * (product.outer_carton_length / 100);
    const cartons = Math.ceil(qty / product.pack_size);
    return cbm * cartons;
  }
  return 0;
};
module.exports = calculateVolume;
