
export const etaMap = {
  "CMA": "Arrival final port of discharge",
  "HAPAG LLOYD": "Vessel arrival",
  "MAERSK": "Estimated Discharge",
};

export const ataMap = {
  "CMA": "Container to consignee",
  "HAPAG LLOYD": "Vessel arrived",
  "MAERSK": "Discharge",
};
