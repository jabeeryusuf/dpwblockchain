import { Request } from "express";
import { IDatabase } from "pg-promise";
import { IProductsService, Product, ProductsResponse } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, Path, POST, Security} from "typescript-rest";
import { DbClientSingleton } from "../utils";

@Path("/v0.1/products")
export class ProductsService implements IProductsService {

  @ContextRequest private request: Request;
  @Inject private dbClient: DbClientSingleton;
  private db: IDatabase<any>;

  constructor() {
    this.db  = this.dbClient.dbp;
  }

  @GET
  @Path("/info")
  public async info(@Param("productId") productId: string)
  : Promise<ProductsResponse> {

    const row = await this.db.oneOrNone(`
      SELECT * FROM products WHERE sku = $1`,productId);

    return row ? { product: this.dbRowToProduct(row)} : {};
  }

  @POST
  @Path("/list")
  public async list(productIds: string[])
  : Promise<ProductsResponse> {

    if (!productIds) {
      return {products: []};
    }
    console.log(productIds);

    const rows = await this.db.any(`
      SELECT * FROM products WHERE sku IN ($1:csv)`, [productIds]);

    return {
      products: rows.map(this.dbRowToProduct),
    };
  }



  @GET
  @Security("USER")
  @Path("/search")
  public async search(@Param("query") query: string,
                      @Param("type") type?: string,
                      @Param("cursor") cursor: string = "",
                      @Param("searchParams")
                      @Param("limit") limit: number = 20)
  : Promise<ProductsResponse> {

    const orgId = this.request.user.orgId;

    console.log([query, cursor, limit]);

    const cursorNum = Number(cursor);
    const typeQuery = type ? "AND department = $<type>" : "";
    console.log("vals", cursorNum,limit)
    // Query
    const results = await this.db.any(`
      SELECT *
      FROM products
      WHERE org_id = $<orgId> AND
            (apn LIKE $<query> OR sku LIKE $<query> OR name LIKE $<query>)
            ${typeQuery}
      ORDER BY sku LIMIT $<limit> OFFSET $<offset>`,
      {orgId, query: "%" + query + "%", limit, offset: +cursor, type});

    const nextCursor = results.length === limit ? String(cursorNum + limit) : "";
    // console.log("nextCursor = " +  nextCursor)

    return {
      products: results.map(this.dbRowToProduct),
      metadata: { nextCursor },
    };
  }

  @GET
  @Security("USER")
  @Path("/listTypes")
  public async listTypes(): Promise<string[]> {
    const orgId = this.request.user.orgId;

    const results = await this.db.query(`
      SELECT DISTINCT department FROM products WHERE org_id = $1`,
      [orgId]);

    // console.log(results)

    return results.map((d: any) => (d.department));
  }

  @GET
  @Path("/listMissing")
  public async listMissing(@Param("orgId") orgId: string): Promise<any> {
    const results = await this.db.query(`select p1.sku FROM purchase_order_items p1 where not exists
                                              (select * from products p2 where p1.sku = p2.sku) and p1.org_id = $1 and p1.sku NOT LIKE 'MIX%' and p1.sku NOT LIKE 'ITEM%';`, [orgId]);
    return results;
  }

  @POST
  @Path("/addProduct")
  public async addProduct(@Param("Obj") Obj: any, @Param("orgId") orgId: string): Promise<any> {
    const Object = JSON.parse(Obj);
    const productExist = await this.db.query(` Select * from products where sku = $1 and org_id = $2 `, [Object["salsify:id"], orgId]);
    if (productExist.length < 1) {
      await this.db.query(` INSERT INTO products(sku, apn, name, department, image, outer_carton_height, outer_carton_width, outer_carton_length, pack_size, carton_cbm, org_id)
                                          VALUES ( $1, $2, $3, $4, $5, $6, $7 , $8, $9, $10, $11 )`, [ Object["salsify:id"], Object.APN, Object.Product_Name, Object.Department_Name,
                                            Object["salsify:digital_assets"][0]["salsify:url"], Object["Outer_Carton_Height_(cms)"], Object["Outer_Carton_Width_(cms)"],
                                            Object["Outer_Carton_Depth_(cms)"], Object.Pack_Size, Object.Carton_CBM, orgId ]);
      return;
    } else {
      return;
    }
  }

  @POST
  @Path("/create")
  public async create(@Param("Obj") Obj: any) {
    const { sku, name, department, org_id } = Obj;
    const productExist = await this.db.query(` Select * from products where sku = $1 and org_id = $2 `, [sku, org_id]);
    if (productExist.length < 1) {
      await this.db.query(` INSERT into products(sku, apn, name, department, org_id)
                                    values($1, $2, $3, $4, $5)`, [sku, sku, name, department, org_id]);
      return;
    } else {
      return;
    }
  }
  private dbRowToProduct(row: any): Product {
    return {
      orgId: row.org_id,
      productId: row.sku,
      type: row.department,
      imageLink: row.image,

      title: row.name,
      description: "N/A",

      gtin: row.apn,
    };
  }
}
