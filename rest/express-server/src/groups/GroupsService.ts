"use-strict";

import { Request } from "express";
import { Client, QueryResult } from "pg";
import { Group } from "tw-api-common";
import { Inject, Singleton } from "typescript-ioc";
import { ContextRequest, GET, Param, Path, POST, Security, PATCH } from "typescript-rest";
import { v4 as uuid } from "uuid";
import { DbClientSingleton } from "../utils";

@Singleton
@Path("/v0.1/groups")
@Security("USER")
export class GroupsService {

  @Inject private dbClient: DbClientSingleton;
  @ContextRequest private request: Request;
  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  @Path("/list")
  @GET
  public async list() {
    const orgId = this.request.user.orgId;

    const result: QueryResult = await this.db.query(`
      SELECT * FROM groups WHERE org_id = $1 AND type != 'case' AND type != 'factory'`,
      [orgId]);
    // console.log(result);
    return result.rows;
  }

  /**
   * Get every user, child group and their
   * users for a given type of group or for a single group
   * @param {string} org Org of who is logged in
   * @param {string} single If you are looking for all users in a single group, this is the group id of that group. Otherwise, null
   */
  @Path("/getUsersInGroups")
  @GET
  public async getUsersInGroups(@Param("org") org: string, @Param("single") single?: string, @Param("vendors") vendors?: string) {
    console.log("getUsersInGroups");

    let sql;
    if (single) {
      sql = `'${single}'`;
    } else {
      if (vendors === "true") {
        sql = `SELECT group_id FROM vendors WHERE org_id='${org}'`;
      } else {
        sql = `SELECT group_id from groups WHERE type != 'case' AND type != 'factory' AND org_id='${org}'`;
      }
    }

    const query = await this.db.query(`WITH RECURSIVE userdetails AS (SELECT group_id, child_group_id
                                                                    FROM group_group_members
                                                                    WHERE group_id IN (${sql})
                                                                    UNION
                                                                    SELECT g.group_id, g.child_group_id
                                                                    FROM group_group_members g
                                                                    JOIN userdetails s ON g.group_id = s.child_group_id )
                                                                    SELECT e.*, f.vendor_code
                                                                    FROM (SELECT c.*, d.type, d.group_name, d.org_id
                                                                          FROM (SELECT a.group_id, a.child_group_id, b.user_id, b.email, b.name
                                                                                FROM userdetails a
                                                                                LEFT JOIN users b ON a.child_group_id = b.user_id) c
                                                                          LEFT JOIN groups d ON c.group_id = d.group_id) e
                                                                    LEFT JOIN vendors f ON e.group_id = f.group_id AND e.org_id = f.org_id;`);

    const returnGroups = [];
    const allGroupDict: any = {};
    const groupsWithChildrenGroups = [];
    const rows = query.rows;

    if (rows.length) {
      let currentGroup = rows[0].group_id;
      allGroupDict[currentGroup] = {
        childGroups: [],
        groupID: currentGroup,
        groupName: rows[0].group_name,
        users: [],
        vendorCode: rows[0].vendor_code, // null if not vendor group
      };
      for (const row of rows) {
        if (row.group_id !== currentGroup) {
          currentGroup = row.group_id;
          if (!allGroupDict[currentGroup]) {
            allGroupDict[currentGroup] = {
              childGroups: [],
              groupID: currentGroup,
              groupName: row.group_name,
              groupOrg: row.org_id,
              users: [],
              vendorCode: row.vendor_code, // null if not vendor group
            };
          }
        }
        if (!row.user_id) { // push the child group ID into the current group's child group array
          const childGroup = row.child_group_id;
          allGroupDict[currentGroup].childGroups.push(childGroup);
          if (groupsWithChildrenGroups.indexOf(currentGroup) < 0) {
            groupsWithChildrenGroups.push(currentGroup);
          }
        } else {
          allGroupDict[currentGroup].users.push({
            email: row.email,
            id: row.user_id,
            name: row.name,
          });
        }
      }

      // this makes me sad
      for (const group of groupsWithChildrenGroups) {
        const groupID = group;
        for (let j = 0; j < allGroupDict[groupID].childGroups.length; j++) {
          const childGroup = allGroupDict[groupID].childGroups[j];
          allGroupDict[groupID].childGroups[j] = allGroupDict[childGroup];
        }
      }

      for (const group in allGroupDict) {
        if (allGroupDict.hasOwnProperty(group)) {
          if (single && (group === single)) { // if only querying for a single group then only return that one group
            returnGroups.push(allGroupDict[group]);
          } else if (vendors === "true") { // if querying for vendor groups, org doesn't matter just add them
            returnGroups.push(allGroupDict[group]);
          } else if (allGroupDict[group].groupOrg === org) { // must be querying for org/user created groups so only return org relevant groups
            returnGroups.push(allGroupDict[group]);
          }
        }
      }

    }

    return returnGroups;
  }


  /**
   * Creates a new group
   * @param {object} payload an object that contains info about the group to be created
   */
  @Path("/create")
  @POST
  public async create(payload: Group, @Param("emails") emails?: string[], @Param("groups") groups?: string[] , @Param("users") addUsers?: string[]) {

    const groupID = uuid();
    payload.groupId = groupID;
    //payload.type = "user_created";
    payload.orgId = this.request.user.orgId;

    console.log("CREATING NEW GROUP");
    console.dir(payload);
    console.dir(emails);
    console.dir(groups);
    console.log("START");

    const found = await this.db.query(`SELECT * FROM groups WHERE group_name=$1 AND org_id=$2`,
     [payload.name, payload.orgId]);
    if (found.rows.length > 0 && payload.type === "user_created") {
      throw new Error("Group with that name already exists");
    }

    // Try to create the new group. If it already exists return error

    await this.db.query(`
      INSERT INTO groups (org_id, group_id, group_name, type) VALUES ($1, $2, $3, $4)`,
      [payload.orgId, groupID, payload.name, payload.type]);

    const newUsers: any = {};
    let existingUsers: any = {}; // dictionary to hold emails and userId's
    for (const email of emails) {
      newUsers[email] = email;
    }

    // Look to see if any of the users don't exist yet.
    // If they do, put them in existingUser dictionary with userID
    const users = await this.db.query(`SELECT email, user_id from users`);
    for (const user of users.rows) {
      if (newUsers[user.email]) {
        existingUsers[user.email] = user.user_id;
        delete newUsers[user.email];
      }
    }

    // If newUsers dict is not empty then put the ones who don't exist into the users table
    if (!isEmpty(newUsers)) {
      const addedUsers: any = await this.insertNewUsers(payload.orgId, newUsers);

      if (addedUsers.errorMessage) {
        throw new Error("addedUsers.errorMessage");
      }
      existingUsers = Object.assign({}, existingUsers, addedUsers);
    }

    // take childGroup array and reduce to map and give to insertNewChildGroups()
    if (groups && groups.length > 0) {
      const childGroupMap = groups.reduce((map: any, obj: any) => {
        map[obj] = obj;
        return map;
      }, {});

      console.log("CHILD GROUP MAP")
      console.dir(childGroupMap);
      await this.insertNewChildGroups(groupID, childGroupMap);
    }

    if (Object.keys(existingUsers).length > 0) {
      console.log("inserting keys");
      const insert: any = await this.insertNewGroupMembers(groupID, existingUsers);
      if (insert.errorMessage) {
        throw new Error("insert.errorMessage");
      }
    }

    if (addUsers) {
      const addUsersDict:any = addUsers.reduce((dict:any, a:string) => {dict[a] = a; return dict},{});
      await this.insertNewGroupMembers(groupID, addUsersDict);
    }

    const ret = {
      group: payload,
      newUsers,
    };

    // console.log("createGroup return: ");
    // console.dir(ret);
    return ret;
  }

  /**
   * Edits an existing group. Responsible for adding and removing new users and child groups to
   * an already existing group
   * This thing is slightly less garbage than it once was but nonetheless needs to be refactored
   * @param {object} payload object that contains info about the group to be edited
   */

  @Path("/update")
  @PATCH
  public async update(payload: any) {

    if (payload.groupName !== payload.prevGroupName) {
      await this.db.query(`UPDATE groups SET group_name=$1 WHERE group_id=$2`, [payload.groupName, payload.groupID]);
    }

    const currentDBUsersRes: any = await this.db.query(`SELECT email, user_id from users`);
    const currentDBUsers: any = {};
    for (const obj of currentDBUsersRes.rows) {
      currentDBUsers[obj.email] = obj.user_id;
    }

    // look up if anyone has been removed from group
    // while simultaneously building a previous emails dictionary for look up
    const removedUsers: any = {};
    const prevEmailsDict: any = {};
    for (const user of payload.prevEmails) {
      prevEmailsDict[user.email] = user.email;
      if (!payload.emails[user.email]) {
        removedUsers[user.email] = currentDBUsers[user.email];
      }
    }

    // if anyone has been added to the group
    const addedUsers: any = {};
    for (const email in payload.emails) {
      if (!prevEmailsDict[email]) {
        addedUsers[email] = email;
      }
    }

    // check to see if any groups have been added or removed
    const removedGroups: any = {};
    const prevGroupsDict: any = {};
    for (const group of payload.prevGroups) {
      prevGroupsDict[group.groupID] = group.groupID;
      if (!payload.groups[group.groupID]) {
        removedGroups[group.groupID] = group.groupID;
      }
    }

    const addedGroups: any = {};
    for (const group in payload.groups) {
      if (!prevGroupsDict[group]) {
        addedGroups[group] = group;
      }
    }

    // are any of those added exist in the DB yet
    const newUserDict: any = {};
    let existingUsersDict: any = {};
    for (const user in addedUsers) {
      if (!currentDBUsers[user]) {
        newUserDict[user] = user;
      } else {
        existingUsersDict[user] = currentDBUsers[user]; // that existing user's user_id
      }
    }

    // add new users into users table then combine them with the newly added users to the group
    if (!isEmpty(newUserDict)) {
      const newlyAddedUsers = await this.insertNewUsers(payload.org, newUserDict);
      existingUsersDict = Object.assign({}, existingUsersDict, newlyAddedUsers);
    }

    if (!isEmpty(addedUsers)) {
      await this.insertNewGroupMembers(payload.groupID, existingUsersDict);
    }

    if (!isEmpty(addedGroups)) {
      // check to see if addedGroup contains parent group
      const circle = await this.checkForCircularStructure(payload.groupID, addedGroups);
      if (circle) {
        return { errorMessage: "Cannot add a child group that contains this group" };
      }
      await this.insertNewChildGroups(payload.groupID, addedGroups);
    }

    if (!isEmpty(removedGroups)) {
      try {
        const sqlStart = `DELETE FROM group_group_members WHERE child_group_id IN (`;
        const sqlEnd = `) AND group_id='` + payload.groupID + `'`;
        let sqlMid = "";
        let i = 0;
        for (const group in removedGroups) {
          if (removedGroups.hasOwnProperty(group)) {
            if (i > 0) {
              sqlMid += ", ";
            }
            sqlMid += `'` + removedGroups[group] + `'`;
            i++;
          }
        }
        const sql = sqlStart + sqlMid + sqlEnd;
        await this.db.query(sql);
      } catch (err) {
        return { errorMessage: err.toString() };
      }
    }

    // remove users from group if any existreturn
    if (!isEmpty(removedUsers)) {
      try {
        const sqlStart = `DELETE FROM group_group_members WHERE child_group_id IN (`;
        const sqlEnd = `) AND group_id='` + payload.groupID + `'`;
        let sqlMid = "";
        let i = 0;
        for (const user in removedUsers) {
          if (removedUsers.hasOwnProperty(user)) {
            if (i > 0) {
              sqlMid += ", ";
            }
            sqlMid += `'` + removedUsers[user] + `'`;
            i++;
          }
        }
        const sql = sqlStart + sqlMid + sqlEnd;
        await this.db.query(sql);
      } catch (err) {
        return { errorMessage: err.toString() };
      }
    }

    return isEmpty(newUserDict) ? "Good job" : newUserDict;
  }


  // get group for direct messaging bewteen two users.
  public async getDirectMessageGroup(orgId: string, uid1: string, uid2: string) {
    console.log("getDirectMessageGroup");

    const result: any = await this.db.query(`
      SELECT group_id FROM groups g WHERE type = 'direct_messages' AND org_id = $1
      INNER JOIN group_group_members ggm1 ON g.group_id = ggm1.group_id AND ggm1.child_id = $2
      INNER JOIN group_group_members ggm2 ON g.group_id = ggm2.group_id AND ggm2.child_id = $3`,
      [orgId, uid1, uid2]);

    if (result.rows.length) {
      return result.rows[0];
    }

    console.log("no record found");

    const groupId = uuid();

    await this.db.query(
      `INSERT INTO group_group_members (group_id, child_group_id) VALUES ($1,$2)`
      , [groupId, uid1]);

    await this.db.query(
      `INSERT INTO group_group_members (group_id, child_group_id) VALUES ($1,$2)`
      , [groupId, uid2]);

    await this.db.query(
      `INSERT INTO groups (org_id, group_id, group_name, type) VALUES ($1, $2, $3, $4)`
      , [orgId, groupId, "DM", "direct_messages"]);

    console.log("created new group %s", groupId);
  }

  public async getGroups(groupIds: any) {

    const result = await this.db.query("SELECT * FROM groups WHERE group_id = ANY ($1)", [groupIds]);
    return result.rows;
  }
  public async getGroup(groupId: any) {

    const result = await this.db.query("SELECT * FROM groups WHERE group_id = $1", [groupId]);
    const rows = result.rows;

    return rows.length > 0 ? rows[0] : null;
  }

  public async getAncestorGroupsForUser(userId: any) {

    // temporary
    // console.log('getting groups for %s',user_id);

    const results = await this.db.query(`
      WITH RECURSIVE user_groups AS (
        SELECT group_id, child_group_id
        FROM group_group_members
        WHERE child_group_id = $1
        UNION
        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.child_group_id = ug.group_id
      )
      SELECT distinct ug.group_id
      FROM user_groups ug
      INNER JOIN messages m ON m.group_id = ug.group_id`
      , [userId]);

    // console.dir(results.rows);

    // convert from [{group_id: foo},group_id:bar] to [foo,bar]
    const groupIds = results.rows.map((o) => o.group_id);
    return groupIds ? groupIds : [];
  }

  /**
   * Gets the groupIds that are associated with cases only and return them as a map { caseID : caseID }
   * @param {string} userId
   */
  public async getAncestorCaseGroupsForUser(userId: string) {

    const results = await this.db.query(`
      WITH RECURSIVE user_groups AS (
        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN cases c on g.group_id = c.case_id
        WHERE child_group_id = $1

        UNION

        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.child_group_id = ug.group_id
      )
      SELECT distinct ug.group_id
      FROM user_groups ug
      INNER JOIN messages m ON m.group_id = ug.group_id;`, [userId] );

    // convert from [{group_id: foo}, {group_id:bar}] to [foo,bar]
    const groupIds = results.rows.map((o) => o.group_id);
    const caseGroupMap = groupIds.reduce((group, val) => { group[val] = val; return group; }, {});

    return caseGroupMap ? caseGroupMap : {};
  }

  public async getUsers(groupId: string) {

    const results = await this.db.query(`
      WITH RECURSIVE user_groups AS (
        SELECT group_id, child_type, child_group_id
        FROM group_group_members
        WHERE group_id = $1
        UNION
        SELECT g.group_id, g.child_type, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.group_id = ug.child_group_id
      )
      SELECT distinct ug.child_group_id user_id
      FROM user_groups ug
      WHERE child_type = 'user';`,
      [groupId]);

    const userIds = results.rows.map((o: any) => o.user_id);
    return userIds;

  }



  /*--------------------------------------------------------------------------------HELPER FUNCTIONS---------------------------------------------------------------------------*/

  /**
   * Inserts new users in to the DB
   * @param {string} org
   * @param {object} userDict object containing users to add
   */
  public async insertNewUsers(org: any, userDict: any) {
    try {
      const addedUsers: any = {};

      const sqlStart = `INSERT INTO users (user_id, org_id, email) VALUES `;
      let sqlEnd = "";
      let i = 0;

      for (const user in userDict) {
        if (userDict.hasOwnProperty(user)) {
          const userID = uuid();
          if (i > 0) {
            sqlEnd += ", ";
          }
          sqlEnd += "('" + userID + "', '" + org + "', '" + user + "')";
          addedUsers[user] = userID;
          i++;
        }
      }

      const sql = sqlStart + sqlEnd;
      await this.db.query(sql);
      return addedUsers;
    } catch (err) {
      return { errorMessage: err.toString() };
    }
  }

  /**
   * Inserts users into a group in the group_group_members table
   * @param {string} groupID
   * @param {object} userDict object containing users to add
   */
  public async insertNewGroupMembers(groupID: any, userDict: any): Promise<QueryResult> {
    console.log("insert new group members");
    console.log([groupID, userDict]);
    const sqlStart = `INSERT INTO group_group_members (group_id, child_group_id, child_type) VALUES `;

    let sqlEnd = "";
    let i = 0;
    for (const user in userDict) {
      if (userDict.hasOwnProperty(user)) {
        if (i > 0) {
          sqlEnd += ", ";
        }
        sqlEnd += "('" + groupID + "', '" + userDict[user] + "', 'user')";
        i++;
      }
    }
    const sql = sqlStart + sqlEnd;
    console.log(sql);
    const query = await this.db.query(sql);
    return query;
  }

  /**
   * Inserts new child groups into a group in the group_group_members table
   * @param {string} groupID
   * @param {object} childGroupDict object containing groups to add
   */
  public async insertNewChildGroups(groupID: string, childGroupDict: any) {
    console.log("child group insert");
    try {
      const sqlStart = `INSERT INTO group_group_members (group_id, child_group_id, child_type) VALUES `;
      let sqlEnd = "";
      let i = 0;
      for (const group in childGroupDict) {
        if (childGroupDict.hasOwnProperty(group)) {
          if (i > 0) {
            sqlEnd += ", ";
          }
          sqlEnd += "('" + groupID + "', '" + childGroupDict[group] + "', 'group')";
          i++;
        }
      }

      const sql = sqlStart + sqlEnd;
      await this.db.query(sql);
    } catch (err) {
      return { errorMessage: err.toString() };
    }
  }

  /**
   * Checks to see if the child groups you want to add don't contain the parent group
   * @param {string} parentGroupID
   * @param {object} groupDict
   */
  public async checkForCircularStructure(parentGroupID: string, groupDict: any) {
    const sqlStart = "SELECT * FROM group_group_members WHERE group_id IN (";
    let sqlEnd = "";
    const i = 0;

    for (const group in groupDict) {
      if (groupDict.hasOwnProperty(group)) {
        if (i > 0) {
          sqlEnd += ", ";
        }
        sqlEnd += "'" + group + "'";
      }
    }
    sqlEnd += ")";

    const sql = sqlStart + sqlEnd;
    const query = await this.db.query(sql);

    if (query.rows.length > 0) {
      for (const row of query.rows) {
        if (row.child_group_id === parentGroupID) {
          return true;
        }
      }
    }

    return false;
  }
}

/**
 * checks to see if an object is empty
 * @param {object} obj
 */
function isEmpty(obj: any) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}
