import { integer } from "aws-sdk/clients/lightsail";
import * as _ from "lodash";
import * as moment from "moment";
import * as path from "path";
import { BuildingFeed, FeedsResponse, IFeedsService, ProductFeed, ResponseStatus, ShipmentFeed } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { GET, Param, Path, POST } from "typescript-rest";
import * as XLSX from "xlsx";
import { ContainersService } from "../containers";
import { FilesService } from "../files";
import { PurchaseOrderItemsService } from "../purchaseOrders/Items/PurchaseOrderItemsService";
import { PurchaseOrdersService } from "../purchaseOrders/PurchaseOrdersService";
import { ShipmentsService } from "../shipments/ShipmentsService";
import { NodeQueryClient } from "../utils";
import { FeedProcessor } from "./FeedProcessor";
import { FeedValidator } from "./FeedValidator";
import { processMessage } from "./processSinotrans";
// let Excel = require("exceljs");

@Path("/v0.1/feeds")
export class FeedsService implements IFeedsService {
  @Inject private purchaseOrdersService: PurchaseOrdersService;
  @Inject private containersService: ContainersService;
  @Inject private fileService: FilesService;
  @Inject private client: NodeQueryClient;
  @Inject private shipment: ShipmentsService;
  @Inject private purchaseOrderItem: PurchaseOrderItemsService;

  private isValid: boolean;
  private validatedData: any[];

  constructor() {
    this.isValid = false;
    this.validatedData = [];
    this.client = new NodeQueryClient();
  }

  @Path("/validateFeedsFile")
  @POST
  public async validateFeedsFile(params: any): Promise<FeedsResponse> {
    const uploadExcelObj = params.uploadExcelObj as any;

    console.log("VALIDATE FEEDS");
    console.dir(uploadExcelObj);
    try {
      const fileData = await this.fileService.retrieveFilesFroms3(uploadExcelObj.fileId);
      const r = await this.uploadExcelValidator(fileData.Body, uploadExcelObj.type, uploadExcelObj.fileId);
      this.validatedData = r;
     // console.log("rrr>>>>>>>>", r);
      const errorLog = await this.shipment.selectErrorLog(uploadExcelObj.fileId);
      console.log("errorLog", errorLog);
      if (uploadExcelObj.type === "building") {
        if (errorLog.length > 0 ) {
          const originalColumns = r.map(( result: any ) => _.omit(result, ["error", "timeZoneId"]));
          await this.generateExcelWithComments(r, uploadExcelObj, originalColumns);
          throw new Error(` File is not validated, see logs for more datils`);
        } else {
          await this.shipment.updateFeedActivity("Validated", uploadExcelObj.fileId);
          const buildingFeeds = r.map(this.dbRowToBuilding);
          return new FeedsResponse({ status: ResponseStatus.OK, buildingFeeds});
        }
      }

      if (uploadExcelObj.type === "product") {
        if (errorLog.length > 0 ) {
          const originalColumns = r.map(( result: any ) => _.omit(result, ["error"]));
          await this.generateExcelWithComments(r, uploadExcelObj, originalColumns);
          throw new Error(` File is not validated, see logs for more datils`);
        } else {
          await this.shipment.updateFeedActivity("Validated", uploadExcelObj.fileId);
          const productFeeds = r.map(this.dbRowToProduct);
          return new FeedsResponse({ status: ResponseStatus.OK, productFeeds});
        }
      }

      if (uploadExcelObj.type === "shipment") {
        if (errorLog.length > 0 ) {
          const originalColumns = r.map(( result: any ) => _.omit(result, ["error", "shipment_ref", "type", "carriertrackingref"]));
          await this.generateExcelWithComments(r, uploadExcelObj, originalColumns);
          throw new Error(` File is not validated, see logs for more datils`);
        } else {
          await this.shipment.updateFeedActivity("Validated", uploadExcelObj.fileId);
          const shipmentFeeds = r.map(this.dbRowToShipment);
          return new FeedsResponse({ status: ResponseStatus.OK, shipmentFeeds});
        }
      }
    } catch (err) {
      const errorMessage = err.message;
      return new FeedsResponse({ status: ResponseStatus.BAD_REQUEST, errorMessage});
    }
  }

  // MOVE TO FILE SERVICE
  @Path("/queryFiles")
  @GET
  public async queryFiles(@Param("whereClause") whereClause: string, @Param("whereParams") whereParams: any,
                          @Param("groupBy") groupBy: string, @Param("orderBy") orderBy: string) {
    return await this.fileService.queryFiles(whereClause, whereParams, groupBy, orderBy);
  }

  /**
   *
   * @param fileData - xlsx file
   * @param type - type of the feed like shipment, product catalog
   * @param fileId - unique id UUID associated with file name in S3, in local S3uploadDirectory, And for Feeds_activity table
   */
  public async uploadExcelValidator(fileData: any, type: string, fileId: string) {

    console.log("Feed Validator Started @@@@@@@@@@@@@@@@@@@@");

    const feedValidator = new FeedValidator();
    const validatedData = await feedValidator.upload(fileData, type, fileId);

    this.validatedData = validatedData;
    this.isValid = true;
    console.log("Feed Validator Ended @@@@@@@@@@@@@@@@@@@@");
    return validatedData;

  }

  public async formatData(fileData: any, type: any, fileId: any) {
    const feedValidator = new FeedValidator();
    const validatedData = await feedValidator.format(fileData, type, fileId);
    return validatedData;
  }

  public async generateExcelWithComments(r: any, uploadExcelObj: any, originalColumns: any) {

    const ws = XLSX.utils.json_to_sheet(originalColumns);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet 1 with Comments");

    r.forEach( (element: any, index: integer) => {
      if (element.error !== undefined) {
        const cell = `A${index + 2}`;
        if (!ws[cell].c) {
          ws[cell].c = [];
          ws[cell].c.hidden = true;
          ws[cell].c.push({a: "SheetJS", t: element.error});
        }
      }
    });
    await XLSX.writeFile(wb, path.join(__dirname, "..", "..", "..", "/Uploads", "fileWithError.xlsx"));
    console.log("file>>>>>>>>>>>>>.", path.join(__dirname, "..", "..", "..", "/Uploads", "fileWithError.xlsx"));
    const folderPath = path.join(__dirname, "..", "..", "..", "/Uploads", "fileWithError.xlsx");
    // uploading to S3
    const newFileID = await this.fileService.uploadFile("fileWithError.xlsx", folderPath, uploadExcelObj.user_id);
    await this.shipment.updateFeedActivity("NOT Valid Format", uploadExcelObj.fileId, newFileID);
  }

  /**
   *
   * @param fileId - xlsx file
   * @param type - type of the feed like shipment, product catalog
   * @param org - org
   * @param fileName - filename to store in feeds_activity table
   */
  @Path("/addFeeds")
  @POST
  public async addFeeds(params: any): Promise<FeedsResponse> {
    let validatedData;
    const uploadExcelObj = params.uploadExcelObj as { fileId: string, type: string, org_id: string, fileName: string };
    const { fileId, type, org_id } = uploadExcelObj;

    console.log("JOLLY Called!");
    const errors = [];
    if (!this.isValid) {
      const fileData = await this.fileService.retrieveFilesFroms3(fileId);
      validatedData = await this.formatData(fileData.Body, type, fileId);
    } else {
      validatedData = this.validatedData;
    }
    const feedProcessor = new FeedProcessor();
    const result = await feedProcessor.upload(validatedData, type);
    await this.shipment.updateFeedActivity("Processing", fileId);
    const shipment = new ShipmentsService();

    if (type === "product") {
      for (const i of result.product) {
        const res = await processMessage(this.client, this.containersService, this.purchaseOrdersService, i, org_id, fileId);
        if (!res) {
          errors.push(`Error at Line ${i}`);
        }
      }
    } else if (type === "shipment") {
        // go through all the shipments and update and try to find the mmsi code
        // as we go through we update the vessel cache to avoid repeating unecessary querys
      const vesselNameCache: any = {};
      for (const i of result.shipments) {
        i.transportContainerId = "ship&" + i.vesselName;
        if (i.vesselName !== "unknown-vessel") {
          if (vesselNameCache[i.vesselName.toLowerCase()]) {
            i.transportContainerId = vesselNameCache[i.vesselName.toLowerCase()];
          } else {
            const vesselMMSI = await this.client.queryScrapeMaps("input like $1", ["%" + i.vesselName.toLowerCase() + "%"]);
            if (vesselMMSI.items.length < 1) {
              i.transportContainerId = "ship&" + vesselMMSI.items[0].output;
              vesselNameCache[i.vesselName.toLowerCase()] = i.transportContainerId;
            } else {
              vesselNameCache[i.vesselName.toLowerCase()] = false;
            }
          }
        }
        i.orgId = org_id;
        console.log("i>>>>>>>>>from feed Service : ", i);
        const shipmentExist = await this.shipment.queryShipmentRefExist(i.shipmentRef, org_id);
        if (shipmentExist.length > 0) {
          for (const s of shipmentExist) {
            i.shipmentId = s.shipment_id;
            if (i.trackings[0].type === "container") {
              const oldDate = moment(s.etd);
              const newDate = moment(i.etd);
              const diff = oldDate.diff(newDate, "days");
              console.log("dayss diffff", diff);
              if ( (diff > -16 && diff < 16) && (s.source_container_id === i.sourceContainerId) ) {
                await shipment.update(i, fileId);
              } else {
                await this.purchaseOrderItem.create(i);
                await shipment.create(i, fileId);
              }
            } else {
              await shipment.update(i, fileId);
            }
          }
        } else {
          await this.purchaseOrderItem.create(i);
          await shipment.create(i, fileId);
        }
      }
    } else if ( type === "building") {
      console.log("feeds servicesss", result);
      await this.containersService.addFeedsBuilding(result, org_id);
    }
    await this.shipment.updateFeedActivity("Done", fileId);
      // return errors;
    return new FeedsResponse({status: ResponseStatus.OK});
  }

  private dbRowToBuilding(row: any): BuildingFeed {
    return {
      address: row.address,
      code: row.code,
      containerId: row.container_id,
      error: row.error,
      lat: row.lat,
      lng: row.lng,
      name: row.name,
      ref: row.ref,
      timeZoneId: row.timeZoneId,
      type: row.type,
    };
  }

  private dbRowToProduct(row: any): ProductFeed {
    return {
      apn: row.apn,
      department: row.department,
      error: row.error,
      name: row.name,
      sku: row.sku,
    };
  }

  private dbRowToShipment(row: any): ShipmentFeed {
    return {
      carrier: row.carrier,
      error: row.error,
      shipmentRef: row.shipment_ref,
    };
  }
}
