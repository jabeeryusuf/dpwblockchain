import { Inject } from "typescript-ioc";
import { ShipmentsService } from "../shipments/ShipmentsService";

export class Shipment {
  public shipment: any;
  @Inject private shipmentsService: ShipmentsService;

  constructor(shipment: any, index: number, fileId: string) {
    let errorMessage: string;
    this.shipment = shipment;

    this.shipment.type = "booking";

    if (this.shipment.carrier === null) {
      errorMessage = `carrier should be valid at row ${index}`;
      this.addErrorLog(fileId, errorMessage);
    }

    if (!this.shipment.shipment_ref) {
      if (this.shipment.booking) {
        console.log("BOOKING REF>>>>>>>>>>>>>>>>>>>>>>>>>>>>..", this.shipment.booking);
        this.shipment.type = "booking";
        this.shipment.shipment_ref = this.shipment.booking;
        this.shipment.carriertrackingref = this.shipment.booking_ref;
      } else if (this.shipment.mbl) {
        this.shipment.type = "bol";
        this.shipment.shipment_ref = this.shipment.mbl;
        this.shipment.carriertrackingref = this.shipment.mbl;
      } else if (this.shipment.container) {
        console.log("IM IN HERE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..");
        this.shipment.type = "container";
        this.shipment.shipment_ref = this.shipment.container; // + "&" + etd;
        this.shipment.carriertrackingref = this.shipment.container;
      } else {
        errorMessage = `Container Number, Mbl OR Booking ref, at least one should be valid at row ${index}`;
        this.addErrorLog(fileId, errorMessage);
      }
    }

    if (this.shipment.type === "container") {
      const containerId = this.shipment.carriertrackingref;
      const totalNumber = containerId.replace(/[^0-9]/g, "").length;
      const totalCharacter = containerId.replace(/[^a-zA-Z]/g, "").length;

      if (totalNumber !== 7 ) {
        errorMessage = `Container should have only 7 number at row ${index}`;
        this.addErrorLog(fileId, errorMessage);
      }
      if (totalCharacter !== 4 ) {
        errorMessage = `Container should have only 4 letter at row ${index}`;
        this.addErrorLog(fileId, errorMessage);
      }

    }

    let orderNumber = this.shipment.purchase_order;
    if (this.shipment.purchase_order === null || this.shipment.purchase_order === undefined) {
      orderNumber = "unknownpo";
    }

    this.shipment.purchase_order = orderNumber;

    this.shipment.error = errorMessage;
  }

  public toSimpleObj() {
    return this.shipment;
  }

  public async addErrorLog(fileId: string, errorMessage: string) {
    await this.shipmentsService.addFeedsErrorLog(fileId, errorMessage);
  }
}
