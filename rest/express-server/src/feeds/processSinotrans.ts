import { NodeQueryClient } from "../utils";
import { ContainersService } from "containers";
import { PurchaseOrdersService } from "purchaseOrders/PurchaseOrdersService";

export async function processPOEvent(e: any, client: PurchaseOrdersService) {
  try {
        //an object that fits the format that our api will be expecting
    let currentPO = await client.query("purchase_order_id =$1", [e.payload.poNumber],undefined,undefined);
    if (!currentPO.items.length) {
      await client.addPurchaseOrder(e.payload.poNumber, e.payload.poDate);
    } else {
      await client.updatePurchaseOrder({ "create_date": e.payload.poDate }, "purchase_order_id = $1 ", [e.payload.poNumber]);
    }
    const poItems = [];
    for (let i = 0; i < e.payload.lineItems.length; i++) {
      const poOBJ : any = {};
      poOBJ.purchase_order_item_id = e.payload.lineItems[i].poLine;
      poOBJ.purchase_order_id = e.payload.poNumber;
      poOBJ.sku = e.payload.lineItems[i].keycode;
      poOBJ.quantity = e.payload.lineItems[i].OrderQTY;
      poOBJ.origin_container = "factory&" + e.payload.lineItems[i].vendorId;
      poOBJ.dst_container = "port&" + e.payload.portOfDestination;
      poOBJ.port_of_loading = e.payload.portOfLoading;
      poOBJ.org_country = e.payload.lineItems[i].countryOfOrigin;
      let foundPOItem = await client.queryItems("purchase_order_id =$1 and purchase_order_item_id=$2 and sku=$3", [e.payload.poNumber, e.payload.lineItems[i].poLine, e.payload.lineItems[i].keycode],undefined,undefined);
      if (foundPOItem.items.length) {
        poItems.push(await client.updatePurchaseOrderItem(poOBJ, "sku = $1 and purchase_order_id = $2 and purchase_order_item_id =$3", [poOBJ.sku, poOBJ.purchase_order_id, poOBJ.purchase_order_item_id]));
      }
      else {
        poItems.push(await client.addPurchaseOrderItem(poOBJ));
      }
    }
    return poItems;
  }
  catch (err) {
    console.log("po error", err);
    return;
  }
}

export async function processGoodsReceivedEvent(e:any, client: NodeQueryClient,
  containersService: ContainersService, purchaseOrdersService: PurchaseOrdersService) {

  try {
    let purchase_orders = e.payload.purchaseOrders;
    let warehouse_received = e.payload.cfsWarehouseCode;
    let receive_date = e.payload.actualArrivalDate || null;
    let existingContainer = await containersService.queryContainer("container_id =$1", ["warehouse&" + e.payload.cfsWarehouseCode]);
    if (!existingContainer.items.length) {
      await containersService.addContainer({ "container_id": "warehouse&" + warehouse_received, "type": "warehouse", "ref": warehouse_received });
    }
    for (let i = 0; i < purchase_orders.length; i++) {
      let po = purchase_orders[i].lineItems;

      for (let j = 0; j < po.length; j++) {
        let item_obj : any= {};
        item_obj.container_id = "warehouse&" + warehouse_received;
        item_obj.receive_date = receive_date;
        item_obj.quantity = parseInt(po[j].goodsReceiptQty);
        item_obj.sku = po[j].keycode;
        item_obj.purchase_order_id = purchase_orders[i].poNumber;
        if (!po.poLine) {
          let poItem = await client.db.query("SELECT purchase_order_item_id from purchase_order_items where purchase_order_id =$1 and sku =$2", [item_obj.purchase_order_id, item_obj.sku]);
          purchase_orders[i].poLine = poItem.rows[0].purchase_order_item_id;
        }
        item_obj.purchase_order_item_id = purchase_orders[i].poLine;
        item_obj.qty_per_carton = parseInt(po[j].goodsReceiptCartonPackSize);
                //using this conditional to deal with null ata which causes error.  This field theoretically shouldn't ever be null but it is in our data
        if (receive_date) {
          await purchaseOrdersService.updatePurchaseOrderItem({ "recv_quantity": po[j].goodsReceiptQty, "mcc_warehouse": e.payload.cfsWarehouseCode, "approx_recv_date": receive_date }, "sku =$1 and purchase_order_id=$2", [item_obj.sku, purchase_orders[i].poNumber]);
        }
        else {
          await purchaseOrdersService.updatePurchaseOrderItem({ "recv_quantity": po[j].goodsReceiptQty }, "sku =$1 and purchase_order_id=$2", [item_obj.sku, purchase_orders[i].poNumber]);
        }
        let existingInv = await client.db.query("SELECT * FROM inventory_items where purchase_order_id = $1 and sku =$2 and container_id =$3 ", [item_obj.purchase_order_id, item_obj.sku, item_obj.container_id]);
        
        if (existingInv.rows.length > 1) {
          await client.db.query("UPDATE inventory_items set quantity = $1 where purchase_order_id =$2 and sku =$3 and container_id = $4", [existingInv.rows[0].quantity + item_obj.quantity, item_obj.purchase_order_id, item_obj.sku, item_obj.container_id]);
        }
        else {
          await containersService.addInventoryItem(item_obj);
        }
      }
    }
    return
  }
  catch (err) {
    console.log("gr error", err);
    return;
  }
}


export async function processOutboundEvent(e:any, client: NodeQueryClient, contService: ContainersService) {
  try {
    let pos = e.payload.purchaseOrders;
    let existingContainer = await contService.info("shipping-container&" + e.payload.containerNumber);
    if (!existingContainer || !existingContainer.containerId) {
      await contService.addContainer({ "container_id": "shipping-container&" + e.payload.containerNumber, "type": "shipping-container", "ref": e.payload.containerNumber });
      await contService.updateContainer({ "parent_container_id": "warehouse&" + e.payload.cfsWarehouseCode }, "container_id=$1", ["shipping-container&" + e.payload.containerNumber]);
    }
    for (let i = 0; i < pos.length; i++) {
      let lineItems = pos[i].lineItems;
      for (let j = 0; j < lineItems.length; j++) {
        let currentInventoryItem = await contService.queryInventory("container_id = $1 and sku=$2 and  purchase_order_id=$3", ["warehouse&" + e.payload.cfsWarehouseCode, lineItems[j].keycode, pos[i].poNumber],undefined,undefined);
        if (currentInventoryItem.items.length) {
          let inventoryItem = currentInventoryItem.items[0];
          let remaining = inventoryItem.quantity - lineItems[j].allocationOrderQty;
          if (remaining > 0) {
            await client.updateInventoryItem({ "quantity": remaining }, "sku = $1 and container_id =$2 and purchase_order_id = $3 ", [lineItems[j].keycode, "warehouse&" + e.payload.cfsWarehouseCode, pos[i].poNumber]);
            inventoryItem.container_id = "shipping-container&" + e.payload.containerNumber;
            inventoryItem.quantity = lineItems[j].allocationOrderQty;
            await contService.addInventoryItem(inventoryItem);
          }
          else {
            await client.updateInventoryItem({ "container_id": "shipping-container&" + e.payload.containerNumber }, "sku = $1 and container_id =$2 and purchase_order_id = $3 ", [lineItems[j].keycode, "warehouse&" + e.payload.cfsWarehouseCode, pos[i].poNumber]);
          }
        }
      }
    }
    return true;
  }
  catch (err) {
    console.log("outbound error", err);
    return false;
  }

}

export async function processPCEvent(e:any, client:NodeQueryClient, org:any) {
  let product = await client.addProductCatalog(e, org);
  return product;

}

export async function processMessage(client: NodeQueryClient, containersService: ContainersService, purchaseOrdersService: PurchaseOrdersService, message: any, org: string, feedActivityId: string) {

  if (message.header.eventType === "PURCHASE_ORDER") {
    const pos = await processPOEvent(message, purchaseOrdersService);
    return pos;
  }
  //adding this because of missing header need to delete
  if (!message.header.eventType || message.header.eventType === "GOODS_RECEIPT") {
    let inventory = await processGoodsReceivedEvent(message, client,containersService,purchaseOrdersService);
    return inventory;
  }

  if (message.header.eventType === "OUTBOUND") {
    let inventory = await processOutboundEvent(message, client,containersService);
    return inventory;
  }

  if (message.header.eventType === "PRODUCT_CATALOG") {
    let checkOrgExist = await client.db.query(`SELECT * from orgs where org_id = '${org}'`);
    if (checkOrgExist.rows.length > 0) {
      let products = await processPCEvent(message, client, org);
      return products;
    } else {
      throw "Org does not Exist";
    }
  }
}
