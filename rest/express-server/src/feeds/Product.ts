/**
 *  check all the required fields are there and also set the default for some of them
 */
import { Inject } from "typescript-ioc";
import { ShipmentsService } from "../shipments/ShipmentsService";

export  class Product {
  public product: any;
  @Inject private shipment: ShipmentsService;

  constructor(product: any, index: number, fileId: string) {
    this.product = product;

    let errorMessage: string;

    if (this.product.sku === null) {
      errorMessage = `sku(ItemCode) should be valid at row ${index}`;
      // throw new Error(`sku(ItemCode) should be valid at row ${index}`);
      this.addErrorLog(fileId, errorMessage);
    }

    if (this.product.name === null) {
      errorMessage = `Name should be valid at row ${index}`;
      // throw new Error(`Name should be valid at row ${index}`);
      this.addErrorLog(fileId, errorMessage);
    }

    if (this.product.apn === null || this.product.apn === undefined) {
      this.product.apn = this.product.sku;
    }

    if (this.product.image === null || this.product.image === undefined) {
      this.product.image = "http://images.salsify.com/image/upload/s--OuY4S37e--/zx4qgial697fu9hjb6ye.png";
    }

    this.product.error = errorMessage;

  }
  public toSimpleObj() {
    return this.product;
  }

  public async addErrorLog(fileId: string, errorMessage: string) {
    await this.shipment.addFeedsErrorLog(fileId, errorMessage);
  }
}
