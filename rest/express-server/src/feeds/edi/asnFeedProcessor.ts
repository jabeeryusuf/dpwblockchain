import * as moment from "moment";
import { AdvanceShipNotice , MethodOfTransport } from "./AdvanceShipNotice";

export function upload(asn: AdvanceShipNotice) {
  const result = {} as any;
  result.header = {};
  result.header.version = "1.0";
  result.header.eventType = "ADVANCE_SHIPPING_NOTICE";
  result.header.eventTimeStamp = moment();
  result.header.methodOfTransport = getMethodOfTransportation(asn.getShipments[0].getMethodOfTransport[0]);

  result.payload = {};
  result.payload.portOfLoading = asn.getHeader.getSender;
  result.payload.asnObId = asn.getBeginning.getShipmentId;
  result.payload.atd = asn.getBeginning.getEtd;
  // for temporary purpose
  result.payload.vesselName = "unkown";
  result.payload.type = "container";
  result.payload.portOfDischarge = asn.getShipments[0].getDestination[0].identification_code_shipment;
  result.payload.methodOfTransport = getMethodOfTransportation(asn.getShipments[0].getMethodOfTransport[0]);
  result.payload.carrier = getCarrier(asn.getShipments[0].getMethodOfTransport[0]);
  result.payload.containers = [];

  const containerObj = {} as any;
  containerObj.containerNumber = asn.getShipments[0].getContainerId[0].shipment_id;
  containerObj.lineItems = [];

  asn.getShipments[0].getOrders.forEach((order) => {
    const poNumber = order.getPurchaseOrderNumber;
    order.getPallets.forEach((pallet) => {
      pallet.getCartons.forEach((carton) => {
        const allocationOrderCartonQty = carton.getCartonQuantity;
        carton.getItems.forEach((item) => {
          const temp = {} as any;
          temp.keycode = item.getKeyCode;
          temp.allocationOrderQty = item.getAllocationOrderquantity;
          temp.poNumber = poNumber;
          temp.allocationOrderCartonQty = allocationOrderCartonQty;
          containerObj.lineItems.push(temp);
        });
      });
    });
  });
  result.payload.containers.push(containerObj);

  return result;
}

function getMethodOfTransportation(transport: any) {
  if (transport.transportation_type_code === MethodOfTransport.MOTOR) {
    return "Motor";
  } else if (transport.transportation_type_code === MethodOfTransport.AIR) {
    return "Air";
  } else if (transport.transportation_type_code === MethodOfTransport.CONSOLIDATION) {
    return "Consolidation";
  } else if (transport.transportation_type_code === MethodOfTransport.PRIVATEPARCELSERVICE) {
    return "PrivateParcelService";
  }
}

function getCarrier(transport: any) {
  return transport.scac;
}
