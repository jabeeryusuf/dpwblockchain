import { EDIProcessor } from "./EDIProcessor";

export enum FeedType {
  EDI,
  EDI214,
}

export class FeedsApi {

  public submit(file: string, type: FeedType, org: string) {

    switch (type) {
    case FeedType.EDI:
      const edi: EDIProcessor = new EDIProcessor();
      return edi.process(file, org);
      break;
    case FeedType.EDI214:
      const edi214: EDIProcessor = new EDIProcessor();
      return edi214.process214(file, org);
      break;
    }
  }

}
