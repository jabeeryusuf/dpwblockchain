import { AdvanceShipNotice } from "./AdvanceShipNotice";
import { CarrierShipmentStatus } from "./CarrierShipmentStatus";
import { upload } from "./asnFeedProcessor";
import { upload214 } from "./cssFeedProcessor";
import { process214Transaction } from "./cssInsertTransaction";
import { Inject } from "typescript-ioc";
import { ShipmentsDb } from "../../shipments/ShipmentsDb";


export class EDIProcessor {
  @Inject shipmentsDb: ShipmentsDb

  public async process(file: any, org: string) {
    const json = JSON.parse(file);
    const asn: AdvanceShipNotice = AdvanceShipNotice.readObject(json);
    const result = upload(asn);
    console.log(">>>>>>%j", result);
    try {
      //await process856Transaction(result, this.client, org);
      // Translate to new shipment API.  should be s

    } catch (err) {
      console.log(err);
    }
    console.log("done!!!");
  }

  public async process214(file: any, org: string) {
    const json = JSON.parse(file);
    const css: CarrierShipmentStatus = CarrierShipmentStatus.readObject(json);
    console.log("%j", css);
    const result = upload214(css, org);
    console.log(result);

    try {
      for (const transaction of result) {
        await process214Transaction(transaction, this.shipmentsDb);
      }
    } catch (err) {
      console.log(err);
    }
    console.log("done!!!");
  }
}
