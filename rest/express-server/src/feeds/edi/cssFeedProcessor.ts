import * as moment from "moment-timezone";
import {  CarrierShipmentStatus,
          CSSPartyInfo,
          PartyCode,
          ShipmentStatus,
          ShipmentStatusIndicator,
          TimeZone,
       } from "./CarrierShipmentStatus";

export function upload214(css: CarrierShipmentStatus, org: string) {
  const transactions: any = [];
  const shipmentId = css.getBeginning.getShipmentId;
  const carrier = css.getBeginning.getCarrier;

  css.getLoop.forEach((loop) => {
    let transactionStatusCode: string;
    let transactionStatusIndicator: string;
    let transactionDate: string;
    let transactionPartyCode: string;
    let transactionPartyName: string;
    let transactionPartyAddress: string;
    let purchaseOrderNumber: string;

    loop.getShipments.forEach((shipment) => {
      transactionStatusCode = getShipmentStatus(shipment.getShipmentStatus);
      transactionStatusIndicator = getShipmentStatusIndicator(shipment.getShipmentStatusIndicator);
      transactionDate = getDate(shipment.getShipmentDate, shipment.getShipmentTime, shipment.getTimeZone);
    });
    loop.getPartyIdentification.forEach((party) => {
      transactionPartyCode = getPartyCode(party.getpartyCode);
      transactionPartyName = party.getname;
      transactionPartyAddress = getPartyAddress(party);
    });
    loop.getOrderInfo.forEach((order) => {
      purchaseOrderNumber = order.getpurchaseOrderNumber;
    });

    transactions.push(
      {
        carrier,
        org_id: org,
        purchase_order_id: purchaseOrderNumber,
        shipment_id: shipmentId,
        transaction_date: transactionDate,
        transaction_party_address: transactionPartyAddress,
        transaction_party_id: transactionPartyCode,
        transaction_party_name: transactionPartyName,
        transaction_status_code : transactionStatusCode,
        transaction_status_indicator : transactionStatusIndicator,
      },
    );
  });
  return transactions;
}

function getShipmentStatus(status: ShipmentStatus) {
  if ((Object.values(ShipmentStatus)).includes(status)) {
    for (const k in ShipmentStatus) {
      if (ShipmentStatus[k] === status) {
        return k;
      }
    }
  }
}

function getShipmentStatusIndicator(indicator: ShipmentStatusIndicator) {
  if ((Object.values(ShipmentStatusIndicator)).includes(indicator)) {
    for (const k in ShipmentStatusIndicator) {
      if (ShipmentStatusIndicator[k] === indicator) {
        return k;
      }
    }
  }
}

function getDate(date: string, time: string, timeZone: TimeZone) {
  let timezone: string = "UTC";
  if ((Object.values(TimeZone)).includes(timeZone)) {
    for (const k in TimeZone) {
      if (TimeZone[k] === timeZone) {
        timezone = k;
      }
    }
  }

  const datePattern = /(\d{4})(\d{2})(\d{2})/;
  const newDate = date.replace(datePattern, "$1-$2-$3");
  const timePattern = /(\d{2})(\d{2})/;
  const newTime = time.replace(timePattern, "$1:$2");
  const finalTime = newDate.concat(" ", newTime);
  const result = moment.tz(finalTime, timezone).format();
  return result;
}

function getPartyCode(party: PartyCode) {
  if ((Object.values(PartyCode)).includes(party)) {
    for (const k in PartyCode) {
      if (PartyCode[k] === party) {
        return k;
      }
    }
  }
}

function getPartyAddress(party: CSSPartyInfo) {

  const street = party.getaddress.toString();
  const city = party.getcityName;
  const state = party.getstate;
  const postal = party.getpostalCode;

  return `${street} , ${city} , ${state}  ${postal}`;
}
