
// 856: Advance ship notice
export class AdvanceShipNotice {
  public static readObject(o: any) {
    const asn = new AdvanceShipNotice();
    asn.beginning = ASNBeginning.readObject(o);
    asn.header = ASNHeader.readObject(o);
    asn.shipments = o.advance_shipment_notice.loop_id_hls.map((e: any) => ASNShipment.readObject(e));
    return asn;
  }
  public shipments: ASNShipment[];
  public beginning: ASNBeginning;
  public header: ASNHeader;

  public get getShipments(): ASNShipment[] {
    return this.shipments;
  }

  public get getBeginning(): ASNBeginning {
    return this.beginning;
  }

  public get getHeader(): ASNHeader {
    return this.header;
  }

}

export class ASNHeader {
  public static readObject(a: any) {
    const header = new ASNHeader();
    header.sender = a.advance_shipment_notice.header.sender_id_qualifier;
    return header;
  }
  public sender: string;

  public get getSender(): string {
    return this.sender;
  }
}

export class ASNBeginning {
  public static readObject(a: any) {
    const beginning = new ASNBeginning();
    beginning.shipmentId = a.advance_shipment_notice.beginning_segment.shipment_identification;
    beginning.etd = a.advance_shipment_notice.beginning_segment.shipment_date;
    if (beginning.shipmentId === undefined) {
      throw new Error("shipment_identification is required");
    }
    return beginning;
  }
  public shipmentId: number;
  public etd: number;

  public get getShipmentId(): number {
    return this.shipmentId;
  }

  public get getEtd(): number {
    return this.etd;
  }

}

export enum MethodOfTransport {
    AIR = "A",
    CONSOLIDATION = "C",
    MOTOR = "M",
    PRIVATEPARCELSERVICE = "U",
}
export class ASNShipment {
  public static readObject(shipments: any) {
    const shipment = new ASNShipment();
    shipment.destination = shipments.ship_to_name.map((e: any) => {
      if (e.identification_code_shipment === undefined) {
        throw new Error("identification_code_shipment is required");
      } else {
        return e;
      }
    });
    shipment.orders = shipments.loop_id_hlo.map((e: any) => ASNOrder.readObject(e));
    shipment.methodOfTransport = shipments.carrier_details.map((e: any) => {
      if (!(Object.values(MethodOfTransport).includes(e.transportation_type_code))) {
        throw new Error("transportation_type_code is required");
      } else {
        return e;
      }
    });

    shipment.containerId = shipments.shipment_identification.map((e: any) => e);
    return shipment;
  }
  public destination: any;
  public orders: ASNOrder[];
  public methodOfTransport: MethodOfTransport;
  public containerId: any;

  public get getDestination(): any {
    return this.destination;
  }

  public get getOrders(): ASNOrder[] {
    return this.orders;
  }

  public get getMethodOfTransport(): MethodOfTransport {
    return this.methodOfTransport;
  }

  public get getContainerId(): any {
    return this.containerId;
  }
}

export class ASNOrder {
  public static readObject(orders: any) {
    const order = new ASNOrder();
    order.purchaseOrderNumber = orders.product_order_reference.purchase_order_number;
    order.pallets = orders.loop_id_hlt.map((e: any) => ASNPallet.readObject(e));
    if (order.purchaseOrderNumber === undefined) {
      throw new Error("Purchase order number is required");
    }
    return order;
  }
  public purchaseOrderNumber: string;
  public pallets: ASNPallet[];

  public get getPallets(): ASNPallet[] {
    return this.pallets;
  }

  public get getPurchaseOrderNumber(): string {
    return this.purchaseOrderNumber;
  }
}

export class ASNPallet {
  public static readObject(pallets: any) {
    const pallet = new ASNPallet();
    pallet.cartons = pallets.loop_id_hlp.map((e: any) => ASNCarton.readObject(e));
    return pallet;
  }
  public cartons: ASNCarton[];

  public get getCartons(): ASNCarton[] {
    return this.cartons;
  }
}

export class ASNCarton {

  public static readObject(cartons: any) {
    const carton = new ASNCarton();
    carton.items = cartons.loop_id_hli.map((e: any) => ASNItem.readObject(e));
    carton.cartonQuantity = cartons.case_pack_details.pack;
    if (carton.cartonQuantity === undefined) {
      throw new Error("Case pack is required");
    }
    return carton;
  }
  public items: ASNItem[];
  public cartonQuantity: number;

  public get getItems(): ASNItem[] {
    return this.items;
  }

  public get getCartonQuantity(): number {
    return this.cartonQuantity;
  }

}

export class ASNItem {

  public static readObject(items: any) {
    const item = new ASNItem();
    item.keyCode = items.item_identification.product_id2;
    item.allocationOrderQuantity = items.item_shipment_details.units_shipped;
    if (item.keyCode  === undefined) {
      throw new Error("product_id2 is required");
    }
    if (item.allocationOrderQuantity === undefined) {
      throw new Error("units_shipped is required");
    }
    return item;
  }
  public keyCode: number;
  public allocationOrderQuantity: number;

  public get getKeyCode(): number {
    return this.keyCode;
  }

  public get getAllocationOrderquantity(): number {
    return this.allocationOrderQuantity;
  }

}
