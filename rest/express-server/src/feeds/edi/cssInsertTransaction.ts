import fetch from "node-fetch";
import * as NodeGeocoder from "node-geocoder";
import { Options } from "node-geocoder";
import * as util from "util";
import { PartyCode, ShipmentStatusIndicator } from "./CarrierShipmentStatus";
import { ShipmentsDb } from "../../shipments/ShipmentsDb";

async function createBuildings(client: ShipmentsDb, transaction: any) {

  const address = transaction.transaction_party_address;
  const options: Options = {
    apiKey: "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk",
    formatter: null,
    httpAdapter: "https",
    provider: "google",
  };
  const geocoder = NodeGeocoder(options);
  const asyncGeocode = util.promisify(geocoder.geocode.bind(geocoder));
  const locInfo = await asyncGeocode(address);
  const formattedAddress = locInfo[0].formattedAddress;
  const existingBuilding = await client.queryBuildings(formattedAddress);
  if (existingBuilding.length === 0) {
    const lat = locInfo[0].latitude;
    const lng = locInfo[0].longitude;
    const apiKey = "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk";
    const url = `https://maps.googleapis.com/maps/api/timezone/json?location=
                ${lat},${lng}&timestamp=1331161200&key=${apiKey}`;

    const response = await fetch(url);
    const data = await response.json();
    const timeZone =  data.timeZoneId;

    const buildingsObj = {} as any;
    buildingsObj.container_id = `warehouse&${transaction.transaction_party_name.replace(/\s/g, "")}`;
    buildingsObj.address = formattedAddress;
    buildingsObj.name = transaction.transaction_party_name;
    buildingsObj.lat = lat;
    buildingsObj.lng = lng;
    buildingsObj.type = "warehouse";
    buildingsObj.ref = transaction.transaction_party_name;
    buildingsObj.time_zone_id = timeZone;
    buildingsObj.org_id = transaction.org_id;

    return await client.insertBuildings(buildingsObj);
  }
  return existingBuilding;
}

async function createShipmentTrackings(client: ShipmentsDb, transaction: any) {
  const currentTracking = await client.queryShipmentExist(transaction.org_id, transaction.shipment_id);
  if (currentTracking.length === 0) {
    throw new Error("Shipment does not exist");
  } else {
    const shipmentTrackingExist = await client.queryShipmentTrackingExist(currentTracking[0].tracking_id);
    if (shipmentTrackingExist.length === 0) {
      //return await client.addShipmentTracking(currentTracking[0].tracking_id,
      //            transaction.carrier, transaction.shipment_id);
    }
    const trackingEventObj = {} as any;
    trackingEventObj.tracking_id = currentTracking[0].tracking_id;
    trackingEventObj.date = transaction.transaction_date;
    trackingEventObj.status = transaction.transaction_status_indicator;
    trackingEventObj.location = `warehouse&${transaction.transaction_party_name.replace(/\s/g, "")}`;

    await client.addShipmentTrackingEvent(trackingEventObj);
    return ;
  }
}

/// function that will create or edit the shipment depending on the event

async function modifyOrCreateShipment(client: ShipmentsDb, transaction: any) {
  const exsistingShipment = await client.queryShipmentExist(transaction.org_id, transaction.shipment_id);
  if (exsistingShipment.length === 0) {
    throw new Error("shipment does not exist");
  }
  if (PartyCode[transaction.transaction_party_id] === "SH" || PartyCode[transaction.transaction_party_id] === "SF") {
    await client.updateShipment(transaction.shipment_id, transaction.transaction_party_name, "source");
  }

  if (PartyCode[transaction.transaction_party_id] === "BT" || PartyCode[transaction.transaction_party_id] === "CN"
      || PartyCode[transaction.transaction_party_id] === "PR" || PartyCode[transaction.transaction_party_id] === "ST") {
    await client.updateShipment(transaction.shipment_id, transaction.transaction_party_name, "destination");
  }

  if (ShipmentStatusIndicator[transaction.transaction_status_indicator] === "AF") {
    await client.updateShipment(transaction.shipment_id, transaction.transaction_date, "atd");
  } else if (ShipmentStatusIndicator[transaction.transaction_status_indicator] === "AG") {
    await client.updateShipment(transaction.shipment_id, transaction.transaction_date, "eta");
  } else if (ShipmentStatusIndicator[transaction.transaction_status_indicator] === "X1") {
    await client.updateShipment(transaction.shipment_id, transaction.transaction_date, "ata");
  }
  return;
}

export async function process214Transaction(transaction: any, shipmentsDb: ShipmentsDb) {
  try {
    await createBuildings(shipmentsDb, transaction);
    await modifyOrCreateShipment(shipmentsDb, transaction);
    await createShipmentTrackings(shipmentsDb, transaction);
    return;
  } catch (err) {
    console.log(err);
  }
}
