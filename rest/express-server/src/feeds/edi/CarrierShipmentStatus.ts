export class CarrierShipmentStatus {
  public static readObject(o: any) {
    const css = new CarrierShipmentStatus();
    css.header = CSSHeader.readObject(o);
    css.beginning = CSSBeginning.readObject(o);
    css.loop = o.carrier_shipment_status.loop_id_1000.map((e: any) => CSSLoop.readObject(e));
    return css;
  }
  public beginning: CSSBeginning;
  public header: CSSHeader;
  public loop: CSSLoop[];

  public get getBeginning(): CSSBeginning {
    return this.beginning;
  }

  public get getHeader(): CSSHeader {
    return this.header;
  }

  public get getLoop(): CSSLoop[] {
    return this.loop;
  }
}

export class CSSHeader {
  public static readObject(headers: any) {
    const header = new CSSHeader();
    header.sender = headers.carrier_shipment_status.header.sender_id;
    return header;
  }
  public sender: string;

  public get getSender(): string {
    return this.sender;
  }
}

export class CSSBeginning {
  public static readObject(beginnings: any) {
    const beginning = new CSSBeginning();
    beginning.shipmentId = beginnings.carrier_shipment_status.beginning_segment.shipment_id_number;
    beginning.carrier = beginnings.carrier_shipment_status.beginning_segment.scac;
    beginning.eventDate = beginnings.carrier_shipment_status.beginning_segment.data;
    beginning.eventTime = beginnings.carrier_shipment_status.beginning_segment.time;
    if (beginning.shipmentId === undefined) {
      throw new Error("Shipment id is required in Beginning_Segment");
    }
    if (beginning.eventTime === undefined) {
      throw new Error("time is required in beginning segment");
    }
    if (beginning.carrier === undefined) {
      throw new Error("scac is required in beginning segment");
    }
    if (beginning.eventDate === undefined) {
      throw new Error("date is required in beginning segment");
    }
    return beginning;
  }
  public shipmentId: number;
  public carrier: string;
  public eventDate: string;
  public eventTime: string;

  public get getShipmentId(): number {
    return this.shipmentId;
  }

  public get getCarrier(): string {
    return this.carrier;
  }

  public get getEventDate(): string {
    return this.eventDate;
  }

  public get getEventTime(): string {
    return this.eventTime;
  }
}

export class CSSLoop {
  public static readObject(loops: any) {
    const loop = new CSSLoop();
    loop.remark = loops.remarks[0].free_form_information;
    loop.transactionSetNumber = loops.transaction_set_number.assigned_number;
    loop.shipments = loops.loop_id_1100.map((e: any) => CSSShipment.readObject(e));
    loop.partyIdentification = loops.loop_id_1200.map((e: any) => CSSPartyInfo.readObject(e));
    loop.orderInfo = loops.loop_id_1300.map((e: any) => CSSOrderInfo.readObject(e));
    return loop;
  }
  public shipments: CSSShipment[];
  public remark: string;
  public transactionSetNumber: string;
  public partyIdentification: CSSPartyInfo[];
  public orderInfo: CSSOrderInfo[];

  public get getShipments(): CSSShipment[] {
    return this.shipments;
  }

  public get getRemark(): string {
    return this.remark;
  }

  public get getTransactionSetNumber(): string {
    return this.transactionSetNumber;
  }

  public get getPartyIdentification(): CSSPartyInfo[] {
    return this.partyIdentification;
  }

  public get getOrderInfo(): CSSOrderInfo[] {
    return this.orderInfo;
  }
}

export enum ShipmentStatus {
    "Other" = "BG",
    "Accident" = "AF",
    "Driver Related" = "AH",
    "Mechanical Breakdown" = "AI",
    "Shipper Related" = "AM",
    "Weather or Natural Disaster Related" = "AO",
    "Missed Pickup" = "AY",
    "Incorrect Address" = "A2",
    "Other Carrier Related" = "AJ",
    "Previous Stop" = "AL",
    "Exceeds Service Limitations" = "AV",
    "Insufficient Pick-up Time" = "AX",
    "Road Conditions" = "BE",
    "Carrier Keying Error" = "BF",
    "Shipment Overweight" = "BQ",
    "Railroad Failed to Meet Schedule" = "BO",
    "Insufficient Time to Complete Delivery" = "BH",
    "Missing Documents" = "BC",
    "Train Derailment" = "BR",
    "Load Canceled" = "CA",
    "Carrier Dispatch Error" = "D1",
    "Driver not Available" = "D2",
    "Tractor With Sleeper Car Not Available" = "T1",
    "Tractor, Conventional, Not Available" = "T2",
    "Trailer not Available" = "T3",
    "Insufficient Delivery Time" = "T7",
}

export enum ShipmentStatusIndicator {
    "Carrier Departed Pick up Location with Shipment" = "AF",
    "Estimated Delivery" = "AG",
    "Available for Delivery" = "AV",
    "Departed the Delivery Location" = "D1",
    "Shipment Delayed" = "SD",
    "Shipment Acknowledged" = "XB",
    "Arrived at Delivery Location" = "X1",
    "Arrived at Pickup Location" = "X3",
    "Shipment Returned to Shipper" = "A3",
    "Refused by Consignee" = "A7",
    "Shipment Conveyance Loaded to Rail Car" = "AL",
    "Delivery Not Completed" = "AP",
    "Rail Arrival at Destination Intermodal Ramp" = "AR",
    "Shipment Available for Pickup" = "AW",
    "Delivered Empty" = "C2",
    "Pickup Empty" = "C3",
    "Shipment Cancelled" = "CA",
    "Completed Loading at Pickup Location" = "CP",
    "Last Free Day without Demurrage for Shipment" = "DE",
    "In-Gate" = "I1",
    "Out-Gate" = "OA",
    "Last Free Day without Per Diem for Shipment" = "PD",
    "Carrier Arrived at Rail Ramp" = "RA",
    "Carrier Departed Rail Ramp" = "RD",
    "Rail Departure from Origin Intermodal Ramp" = "RL",
    "Arrived at Delivery Location Loading Dock" = "X5",
    "En Route to Delivery Location" = "X6",
    "Arrived at Pickup Location Loading Dock" = "X8",
}
export enum TimeZone {
    "US/Central" = "CT",
    "US/Eastern" = "ET",
    "US/Mountain" = "MT",
    "US/Pacific" = "PT",
    "UTC" = "UT",
}
export class CSSShipment {
  public static readObject(shipments: any) {
    const shipment = new CSSShipment();
    shipment.shipmentStatus = shipments.shipment_status.shipment_status;
    shipment.shipmentStatusIndicator = shipments.shipment_status.shipment_status_indicator;
    shipment.shipmentDate = shipments.shipment_status.shipment_date;
    shipment.shipmentTime = shipments.shipment_status.shipment_time;
    shipment.timeZone = shipments.shipment_status.time_code;
    if (shipment.shipmentStatus === undefined) {
      throw new Error("Shipment status is required");
    }
    if (shipment.shipmentStatusIndicator === undefined) {
      throw new Error("Shipment Status Indicator is required");
    }
    return shipment;
  }
  public shipmentStatus: ShipmentStatus;
  public shipmentStatusIndicator: ShipmentStatusIndicator;
  public shipmentDate: string;
  public shipmentTime: string;
  public timeZone: TimeZone;

  public get getShipmentStatus(): ShipmentStatus {
    return this.shipmentStatus;
  }

  public get getShipmentStatusIndicator(): ShipmentStatusIndicator {
    return this.shipmentStatusIndicator;
  }

  public get getShipmentDate(): string {
    return this.shipmentDate;
  }

  public get getShipmentTime(): string {
    return this.shipmentTime;
  }

  public get getTimeZone(): TimeZone {
    return this.timeZone;
  }

}

export enum PartyCode {
    "Bill to Party" = "BT",
    "Consignee" = "CN",
    "Payer" = "PR",
    "Ship From" = "SF",
    "Shipper" = "SH",
    "Ship To" = "ST",
}
export class CSSPartyInfo {
  public static readObject(partyInfos: any) {
    const party = new CSSPartyInfo();
    party.partyCode = partyInfos.party_identification.entity_id_code;
    party.name = partyInfos.party_identification.name;
    party.address = partyInfos.party_location.map((e: any) => e.address);
    party.cityName = partyInfos.geographic_location.city_name;
    party.state = partyInfos.geographic_location.state_or_province_code;
    party.postalCode = partyInfos.geographic_location.postal_code;
    if (party.partyCode === undefined) {
      throw new Error("entity identifier code is required");
    }
    if (party.address === undefined) {
      throw new Error("Address is required");
    }
    if (party.cityName === undefined) {
      throw new Error("City Name is required");
    }
    if (party.state === undefined) {
      throw new Error("state or province code is required");
    }
    if (party.postalCode === undefined) {
      throw new Error("Postal code is required");
    }

    return party;
  }
  private partyCode: PartyCode;
  private name: string;
  private address: string[];
  private cityName: string;
  private state: string;
  private postalCode: string;

  public get getpartyCode(): PartyCode {
    return this.partyCode;
  }
  public get getname(): string {
    return this.name;
  }
  public get getaddress(): string[] {
    return this.address;
  }
  public get getcityName(): string {
    return this.cityName;
  }
  public get getstate(): string {
    return this.state;
  }
  public get getpostalCode(): string {
    return this.postalCode;
  }
}

export enum WeightCode {
  POUND = "L",
}

export class CSSOrderInfo {
  public static readObject(orders: any) {
    const order = new CSSOrderInfo();
    order.purchaseOrderNumber = orders.order_details.purchase_order_number;
    order.cartonQuantity = orders.order_details.carton_quantity;
    order.packageCode = orders.order_details.package_code;
    order.weightCode = orders.order_details.weight_code2;
    order.weight = orders.order_details.weight2;
    if (order.purchaseOrderNumber === undefined) {
      throw new Error("Purchase order number is required");
    }
    return order;
  }

  private purchaseOrderNumber: string;
  private packageCode: string;
  private cartonQuantity: string;
  private weightCode: WeightCode;
  private weight: string;

  public get getpurchaseOrderNumber(): string {
    return this.purchaseOrderNumber;
  }
  public get getpackageCode(): string {
    return this.packageCode;
  }
  public get getcartonQuantity(): string {
    return this.cartonQuantity;
  }
  public get getweightCode(): WeightCode {
    return this.weightCode;
  }
  public get getweight(): string {
    return this.weight;
  }
}
