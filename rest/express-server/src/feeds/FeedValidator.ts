import * as _ from "lodash";
import * as md5 from "md5";
import fetch from "node-fetch";
import * as XLSX from "xlsx";
import { Building } from "./Building";
import { Product } from "./Product";
import { Shipment } from "./Shipment";
/**
 *  Upload take the input as .XLSX file and output the array of object which is needed for further processing.
 *  and discards the rest of the columns which are not needed.
 *
 *  If the file header is changed please change that header value to the neededValues (with all lowerCase)
 *  and Add column which are required to REQUIREDHEADERS(with all lowerCase)
 */
// const NEEDED_SHIP_VALUES = ["purchase_order_id", "booking_ref", "bill_of_lading", "container", "shipment_id", "product_id", "pol", "pod", "eta", "etd", "carrier", "quantity", "vessel", "voyage", "department"];
const NEEDED_SHIP_VALUES = ["purchase_order", "booking", "mbl", "container", "hbl", "product_id", "pol", "pod", "eta", "etd", "original_eta", "carrier", "quantity", "vessel", "voyage", "product_title", "product_description", "product_type", "manufacture", "lot"];
const REQUIRED_SHIP_HEADERS = ["carrier", ["container", "mbl", "booking_ref"]];
const NEEDED_PRODUCT_VALUES = ["sku", "apn", "name", "department", "image", "outer_carton_height", "outer_carton_width", "outer_carton_length", "pack_size", "carton_cbm"];
const REQUIRED_PRODUCT_HEADERS = ["sku", "name"];
const NEEDED_BUILDING_VALUES = ["name", "type", "address", "lat", "lng", "code"];
const REQUIRED_BUILDING_HEADERS = ["name", "type", ["address", "lat", "lng"]];

export class FeedValidator {
  // Function that can be called to get data in the validated format without actually validating
  // expects a file path and a feed type
  public async format(path: any, type: any, fileId: string) {
    const xlData = await this.generateXlJson(path);
    const result: any = [];
    if (type === "shipment") {
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_SHIP_VALUES, xlData);
      pluckedData.forEach((e: any, i: number) => {
        const shipment = new Shipment(e, i + 2, fileId);
        result.push(shipment.toSimpleObj());
      });
    }
    if (type === "product") {
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_SHIP_VALUES, xlData);
      pluckedData.forEach((e: any, i: number) => {
        const product = new Product(e, i + 2, fileId);
        result.push(product.toSimpleObj());
      });
    }
    if (type === "building") {
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_BUILDING_VALUES, xlData);
      pluckedData.forEach((e: any, i: number) => {
        const product = new Building(e, i + 2, fileId);
        result.push(product.toSimpleObj());
      });
    }
    return result;
  }
  // Function that when called validates that the necessary columns are included
  // Returns a list of either shipments or products depending on the feed type 
  public async upload(path: any, type: string, fileId: string) {
    const xlData = await this.generateXlJson(path);
    if (xlData.length === 0) {
      throw new Error("Excel upload failed. Is the sheet formatted correctly");
    }
    const result: any[] = [];
    if (type === "shipment") {
      this.ensureHeadersIncluded(REQUIRED_SHIP_HEADERS, xlData[0]);
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_SHIP_VALUES, xlData);
      pluckedData.forEach((e: any, i: number) => {
        const shipment = new Shipment(e, i + 2, fileId);
        result.push(shipment.toSimpleObj());
      });
    } else if (type === "product") {
      this.ensureHeadersIncluded(REQUIRED_PRODUCT_HEADERS, xlData[0]);
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_PRODUCT_VALUES, xlData);

      pluckedData.forEach((e: any, i: any) => {
        const product = new Product(e, i + 2, fileId);
        result.push(product.toSimpleObj());
      });

    } else if (type === "building") {

      this.ensureHeadersIncluded(REQUIRED_BUILDING_HEADERS, xlData[0]);
      const pluckedData = this.generateFormatedObjects(xlData[0], NEEDED_BUILDING_VALUES, xlData);

      for (let i = 0 ; i < pluckedData.length; i++) {
        const building = new Building(pluckedData[i], i + 2, fileId);
        if (building.toSimpleObj().lat === null && building.toSimpleObj().lng === null) {
          const getLatLng = await this.getLatLngFromAddress(building.toSimpleObj().address);
          building.toSimpleObj().lat = getLatLng.lat;
          building.toSimpleObj().lng = getLatLng.lng;
        }
        if (building.toSimpleObj().address === null) {
          const getAddress = await this.getAddresFromLatLng(building.toSimpleObj().lat, building.toSimpleObj().lng);
          building.toSimpleObj().address = getAddress;
        }

        const timeZoneID = await this.getTimezone(building.toSimpleObj().lat, building.toSimpleObj().lng);
        building.toSimpleObj().timeZoneId = timeZoneID;
        if (building.toSimpleObj().code === null) {
          const code = md5(building.toSimpleObj().name).slice(1, 10);
          building.toSimpleObj().code = code;
        }
        console.log("new building obejct ", building.toSimpleObj(), ">>>>>>>", i);
        result.push(building.toSimpleObj());
      }
    }
    return result;
  }
  // Takes xldata json, a list of the sheets headers and a list of the columns that we need to process the data
  public generateFormatedObjects(headers: any, neededValues: any, xlData: any) {
    const entries = Object.entries(headers);
    // Only useful Column list
    const toBePlucked: any = [];
    for (const [key, value] of entries as Array<[string, string]>) {
      if (value !== null && neededValues.includes(value.toLowerCase().trim())) {
        toBePlucked.push({ [key]: value.toLowerCase().trim() });
      }
    }
    // final Array of Object with only useful columns in it, to work with
    const pluckedData = xlData.map((d: any) => {
      const r: any = {};
      for (const i of toBePlucked) {
      // tslint:disable-next-line: forin
        for (const key in i) {
          r[i[key]] = d[key];
        }
      }
      return r;
    });
    pluckedData.shift();
    return pluckedData;
  }

  private async getLatLngFromAddress(address: string) {
    if (address === null || address === undefined) {
      return {lat: 0.0, lng: 0.0};
    } else {
      const apiKey = "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk";
      const url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" + apiKey;

      const response = await fetch(url);
      const data = await response.json();
      return data.results[0].geometry.location;
    }
  }

  private async getAddresFromLatLng(lat: number, lng: number) {
    if ( lat === 0.0 || lng === 0.0) {
      return "";
    } else {
      const apiKey = "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk";
      const url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + apiKey;

      const response = await fetch(url);
      const data = await response.json();
      return data.results[0].formatted_address;
    }
  }

  private async getTimezone(lat: any, long: any) {
    const apiKey = "AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk"
    const url = "https://maps.googleapis.com/maps/api/timezone/json?location=" + lat + "," + long + "&timestamp=1331161200&key=" + apiKey

    const response = await fetch(url);

    const data = await response.json();

    return data["timeZoneId"];
  }

  // Function to determine whether or not an excel contains the necessary headers 
  private ensureHeadersIncluded(requiredHeaders: any, headers: any) {
    const headerValues = (Object.values(headers)).map((e: string) => {
      if (e) {
        return e.toLocaleLowerCase().trim();
      }
    });
    for (const i of requiredHeaders) {
      if (!(typeof (i) === "object")) {
        if (!headerValues.includes(i)) {
          throw new Error((`Missing required Columns : ${i}`));
        }
      } else {
        const substituteColumns = _.intersection(i, headerValues);
        if (substituteColumns.length < 1) {
          throw new Error((`At least one of the following columns is required : ${i}`));
        }
      }
    }
  }
  // Takes a file path and returns a workable json object representing an excel sheet
  private async generateXlJson(path: any) {
    const workbook = await XLSX.read(path, { type: "buffer", cellDates: true });
    return await XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]], { defval: null, header: "A" });
  }
}
