import * as _ from "lodash";
import * as md5 from "md5";
import * as moment from "moment";

/**
 *  take the feedValidator output as input and gives output as new array of object in format for shipment creation API
 */
export class FeedProcessor {

  public async upload(options: any, type: string) {

    if (type === "product") {

      const result: any = {};

      const pluckedData = options;
      const resultgroupedPlucked = _.groupBy(pluckedData, "sku");

      result.product = [];
  // tslint:disable-next-line: forin
      for (const obj in resultgroupedPlucked) {
        resultgroupedPlucked[obj].forEach((e: any) => {
          const tempObj: any = {};
          tempObj.payload = e;
          tempObj.header = {};
          tempObj.header.version = "1.0";
          tempObj.header.eventType = "PRODUCT_CATALOG";
          tempObj.header.eventTimeStamp = moment();
          result.product.push(tempObj);
        });
      }
      console.log("pluckdatataaa>>>>>>", JSON.stringify(result));
      return result;

    } else if (type === "shipment") {
    // Array of Object
      const pluckedData = options;
      // console.log(pluckedData);
    // group by the shipmentID
      const groupedPlucked = _.groupBy(pluckedData, "shipment_ref");
      let tempcontainer;
      let result: any = {};
      const resultSubObject: any = {};
      resultSubObject.shipments = [];

// tslint:disable-next-line: forin
      for (const property in groupedPlucked) {
        result = {};
        // result.payload = {};
        result.shipmentRef = property;

      // Group by containerNumber
        tempcontainer = _.groupBy(groupedPlucked[property], "container");
        const conatinerUniqueKey = Object.keys(tempcontainer);
      // all the container will have same POL, ETA, POD and all
        tempcontainer[conatinerUniqueKey[0]].map((e: any) => {
          result.vesselName = e.vessel ? e.vessel : "unknown-vessel";
          result.sourceContainerId = e.pol ? "port&" + e.pol : "port&unknown-source";
          result.destinationContainerId = e.pod ? "port&" + e.pod : "port&unknown-dest";
          result.eta = e.eta ? e.eta : null;
          result.etd = e.etd ? e.etd : null;
          result.atd = e.atd ? e.atd : null;
          result.ata = e.ata ? e.ata : null;
          result.bol = e.mbl ? e.mbl : null;
          result.originalEta = e.original_eta ? e.original_eta : null;
          result.shipType = e.deliverymode !== undefined ? e.deliverymode : null;
          result.carrier = e.carrier ? e.carrier : "unknown-carrier";
          result.trackings = [];
          const tracking: any = {};
          tracking.type = e.type;
          tracking.ref = e.carriertrackingref;
          tracking.carrierId = e.carrier;
          tracking.status = "tracking";
          result.trackings.push(tracking);
        });

        const itemsArray: any = [];
// tslint:disable-next-line: forin
        for (const container in tempcontainer) {

        // group by the itemCode
          const itemCode: any = _.groupBy(tempcontainer[container], "product");
// tslint:disable-next-line: forin
          for (const item in itemCode) {
            itemCode[item].forEach((element: any) => {
              const lineItems: any = {};
              lineItems.purchaseOrderId = element.purchase_order ? element.purchase_order : null;
              lineItems.purchaseOrderItemId = element.purchase_order_line ? element.purchase_order_line : "default_line";
              lineItems.productType = element.product_type ? element.product_type : null;
              lineItems.productName = element.product_title ? element.product_title : null;
              if (element.product_id === null) {
                if (element.product_title !== null) {
                  lineItems.productId = (md5(element.product_title)).slice(0, 10);
                } else {
                  lineItems.productId = "unknown-sku";
                }
              } else {
                lineItems.productId = element.product_id;
              }
              lineItems.manufacture = element.manufacture ? element.manufacture : "none";
              lineItems.quantity = element.quantity ? element.quantity : 0;
              lineItems.shippingContainerId = "shipping-container&" + container;
              lineItems.allocationOrderCartonQty = element.shipcartons !== undefined ? element.shipcartons : null;
              lineItems.hbl = element.hbl ? element.hbl : null;
              lineItems.lot = element.lot ? element.lot : null;
              itemsArray.push(lineItems);
            });
          }
          result.items = itemsArray;
         /*  result.header = {};
          result.header.version = "1.0";
          result.header.eventType = "ADVANCE_SHIPPING_NOTICE";
          result.header.eventTimeStamp = moment(); */

        }
        resultSubObject.shipments.push(result);
      }

      console.log("resultSubObject>>>>> ", JSON.stringify(resultSubObject));

      return resultSubObject;
    } else if (type === "building") {
      const result = options.map((e: any) => {
        e.container_id = e.type + "&" + e.code;
        e.ref = e.code;
        return e;
      });
      return result;
    } else {
      return;
    }
  }
}
