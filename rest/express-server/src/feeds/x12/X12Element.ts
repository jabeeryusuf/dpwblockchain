"use strict";

import { Range } from "./Positioning";

export class X12Element {

  public range: Range;
  public value: string;

  constructor() {
    this.range = new Range();
    this.value = "";
  }
}
