"use strict";

export interface IX12SerializationOptions {
  elementDelimiter?: string;
  endOfLine?: string;
  format?: boolean;
  segmentTerminator?: string;
}

export function defaultSerializationOptions(options?: IX12SerializationOptions): IX12SerializationOptions {
  options = options || {};

  options.elementDelimiter = options.elementDelimiter || "*";
  options.endOfLine = options.endOfLine || "\n";
  options.format = options.format || false;
  options.segmentTerminator = options.segmentTerminator || "~";

  if (options.segmentTerminator === "\n") {
    options.endOfLine = "";
  }

  return options;
}
