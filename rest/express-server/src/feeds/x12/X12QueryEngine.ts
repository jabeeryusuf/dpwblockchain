"use strict";

import { QuerySyntaxError } from "./Errors";
import { X12Element } from "./X12Element";
import { X12FunctionalGroup } from "./X12FunctionalGroup";
import { X12Interchange } from "./X12Interchange";
import { X12Parser } from "./X12Parser";
import { X12Segment } from "./X12Segment";
import { X12Transaction } from "./X12Transaction";

export class X12QueryEngine {
  constructor(private parser: X12Parser) { }

  public query(rawEdi: string, reference: string): X12QueryResult[] {
    const interchange = this.parser.parseX12(rawEdi);

    const hlPathMatch = reference.match(/HL\+(\w\+?)+[\+-]/g); // ex. HL+O+P+I
    const segPathMatch = reference.match(/([A-Z0-9]{2,3}-)+/g); // ex. PO1-N9-

    // ex. REF02; need to remove trailing ":" if exists
    const elmRefMatch = reference.match(/[A-Z0-9]{2,3}[0-9]{2}[^\[]?/g);
    const qualMatch = reference.match(/:[A-Za-z]{2,3}[0-9]{2,}\[\"[^\[\]\"\"]+\"\]/g); // ex. :REF01["PO"]

    const results = new Array<X12QueryResult>();

    for (const group of interchange.functionalGroups) {

      for (const txn of group.transactions) {

        let segments = txn.segments;

        if (hlPathMatch) {
          segments = this._evaluateHLQueryPart(txn, hlPathMatch[0]);
        }

        if (segPathMatch) {
          segments = this._evaluateSegmentPathQueryPart(segments, segPathMatch[0]);
        }

        if (!elmRefMatch) {
          throw new QuerySyntaxError("Element reference queries must contain an element reference!");
        }

        const txnResults = this._evaluateElementReferenceQueryPart(interchange, group,
          txn, [].concat(segments, [interchange.header, group.header, txn.header,
            txn.trailer, group.trailer, interchange.trailer]), elmRefMatch[0], qualMatch);

        txnResults.forEach((res) => {
          results.push(res);
        });
      }
    }

    return results;
  }

  public querySingle(rawEdi: string, reference: string): X12QueryResult | null {
    const results = this.query(rawEdi, reference);
    return (results.length === 0) ? null : results[0];
  }

  private _evaluateHLQueryPart(transaction: X12Transaction, hlPath: string): X12Segment[] {
    let qualified = false;
    const pathParts = hlPath.replace("-", "").split("+").filter((value, index, array) =>
      (value !== "HL" && value !== "" && value !== null));
    const matches = new Array<X12Segment>();

    let lastParentIndex = -1;

    for (let i = 0, j = 0; i < transaction.segments.length; i++) {
      const segment = transaction.segments[i];

      if (qualified && segment.tag === "HL") {
        const parentIndex = parseInt(segment.valueOf(2, "-1") || "", 10);

        if (parentIndex !== lastParentIndex) {
          j = 0;
          qualified = false;
        }
      }

      if (!qualified && transaction.segments[i].tag === "HL" && transaction.segments[i].valueOf(3) === pathParts[j]) {
        lastParentIndex = parseInt(segment.valueOf(2, "-1") || "", 10);
        j++;

        if (j === pathParts.length) {
          qualified = true;
        }
      }

      if (qualified) {
        matches.push(transaction.segments[i]);
      }
    }

    return matches;
  }

  private _evaluateSegmentPathQueryPart(segments: X12Segment[], segmentPath: string): X12Segment[] {
    let qualified = false;
    const pathParts = segmentPath.split("-").filter((value, index, array) => !!value);
    const matches = new Array<X12Segment>();

    for (let i = 0, j = 0; i < segments.length; i++) {
      if (qualified && (segments[i].tag === "HL" || pathParts.indexOf(segments[i].tag) > -1)) {
        j = 0;
        qualified = false;
      }

      if (!qualified && segments[i].tag === pathParts[j]) {
        j++;

        if (j === pathParts.length) {
          qualified = true;
        }
      }

      if (qualified) {
        matches.push(segments[i]);
      }
    }

    return matches;
  }

  private _evaluateElementReferenceQueryPart(interchange: X12Interchange,
                                             functionalGroup: X12FunctionalGroup,
                                             transaction: X12Transaction,
                                             segments: X12Segment[],
                                             elementReference: string,
                                             qualifiers: string[] | null): X12QueryResult[] {
    const reference = elementReference.replace(":", "");
    const tag = reference.substr(0, reference.length - 2);
    const pos = reference.substr(reference.length - 2, 2);
    const posint = parseInt(pos, 10);

    const results = new Array<X12QueryResult>();

    for (const segment of segments) {

      if (!segment) {
        continue;
      }

      if (segment.tag !== tag) {
        continue;
      }

      const value = segment.valueOf(posint, null);

      if (value && this._testQualifiers(transaction, segment, qualifiers)) {
        results.push(new X12QueryResult(interchange,
          functionalGroup, transaction, segment, segment.elements[posint - 1]));
      }
    }

    return results;
  }

  private _testQualifiers(transaction: X12Transaction, segment: X12Segment, qualifiers: string[] | null): boolean {
    if (!qualifiers) {
      return true;
    }

    for (const qual of qualifiers) {
      const qualifier = qual.substr(1);
      const elementReference = qualifier.substring(0, qualifier.indexOf("["));
      const elementValue = qualifier.substring(qualifier.indexOf("[") + 2, qualifier.lastIndexOf("]") - 1);
      const tag = elementReference.substr(0, elementReference.length - 2);
      const pos = elementReference.substr(elementReference.length - 2, 2);
      const posint = parseInt(pos, 10);

      for (let j = transaction.segments.indexOf(segment); j > -1; j--) {
        const seg = transaction.segments[j];
        const value = seg.valueOf(posint);

        if (seg.tag === tag && seg.tag === segment.tag && value !== elementValue) {
          return false;
        } else if (seg.tag === tag && value === elementValue) {
          break;
        }

        if (j === 0) {
          return false;
        }
      }
    }

    return true;
  }
}

// tslint:disable-next-line: max-classes-per-file
export class X12QueryResult {

  public interchange: X12Interchange | undefined;
  public functionalGroup: X12FunctionalGroup | undefined;
  public transaction: X12Transaction | undefined;
  public segment: X12Segment | undefined;
  public element: X12Element | undefined;
  constructor(interchange?: X12Interchange,
              functionalGroup?: X12FunctionalGroup,
              transaction?: X12Transaction,
              segment?: X12Segment,
              element?: X12Element) {
    this.interchange = interchange;
    this.functionalGroup = functionalGroup;
    this.transaction = transaction;
    this.segment = segment;
    this.element = element;
  }
}
