"use strict";

import { defaultSerializationOptions, IX12SerializationOptions } from "./IX12SerializationOptions";
import { X12Segment } from "./X12Segment";
import { X12Transaction } from "./X12Transaction";

export class X12FunctionalGroup {

  public header: X12Segment | undefined;
  public trailer: X12Segment | undefined;

  public transactions: X12Transaction[];
  constructor() {
    this.transactions = new Array<X12Transaction>();
  }

  public toString(options?: IX12SerializationOptions): string {

    if (!this.header || !this.trailer) {
      throw new Error("header or trailer missing");
    }

    options = defaultSerializationOptions(options);

    let edi = this.header.toString(options);

    if (options.format) {
      edi += options.endOfLine;
    }

    for (const tran of this.transactions) {
      edi += tran.toString(options);

      if (options.format) {
        edi += options.endOfLine;
      }
    }

    edi += this.trailer.toString(options);

    return edi;
  }
}
