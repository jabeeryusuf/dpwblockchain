"use strict";

export class Position {
  public line: number | undefined;
  public character: number | undefined;

  constructor(line?: number, character?: number) {
    if (typeof line === "number" && typeof character === "number") {
      this.line = line;
      this.character = character;
    }
  }
}

// tslint:disable-next-line: max-classes-per-file
export class Range {
  public start: Position | undefined;
  public end: Position | undefined;

  constructor(startLine?: number, startChar?: number, endLine?: number, endChar?: number) {

    if (typeof startLine === "number" && typeof startChar === "number" &&
        typeof endLine === "number" && typeof endChar === "number") {
      this.start = new Position(startLine, startChar);
      this.end = new Position(endLine, endChar);
    }
  }
}
