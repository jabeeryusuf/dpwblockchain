"use strict";

import { defaultSerializationOptions, IX12SerializationOptions } from "./IX12SerializationOptions";
import { Range } from "./Positioning";
import { X12Element } from "./X12Element";

export class X12Segment {

  public tag: string;
  public elements: X12Element[];
  public range: Range;
  constructor() {
    this.tag = "";
    this.elements = new Array<X12Element>();
    this.range = new Range();
  }

  public toString(options?: IX12SerializationOptions): string {
    options = defaultSerializationOptions(options);
    let edi = this.tag;

    console.log("SEGMENT: " + this.tag);

    let i = 0;
    for (const elem of this.elements) {
      edi += options.elementDelimiter;
      edi += elem.value;
      console.log("elem[%i]: %s", i, elem.value);
      i++;
    }

    edi += options.segmentTerminator;

    return edi;
  }

  public valueOf(segmentPosition: number, defaultValue?: string | null): string | null {
    const index = segmentPosition - 1;

    if (this.elements.length <= index) {
      return defaultValue || null;
    }

    return this.elements[index].value || defaultValue || null;
  }
}
