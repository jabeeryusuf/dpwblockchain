"use strict";

import { Range } from "./Positioning";

export class X12Diagnostic {

  public level: X12Diagnostic.Level;
  public  message: string;
  public range: Range | undefined;

  constructor(level?: X12Diagnostic.Level, message?: string, range?: Range) {
    this.level = level || X12Diagnostic.Level.Error;
    this.message = message || "";
    this.range = range;
  }
}

// tslint:disable-next-line: no-namespace
export namespace X12Diagnostic {
  export enum Level {
    Info = 0,
    Warning = 1,
    Error = 2,
  }
}
