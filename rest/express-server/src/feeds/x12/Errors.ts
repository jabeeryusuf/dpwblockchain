"use strict";

export class ArgumentNullError implements Error {

  public name: string;
  public message: string;
  constructor(argumentName: string) {
    this.name = "ArgumentNullError";
    this.message = `The argument, '${argumentName}', cannot be null.`;
  }
}

// tslint:disable-next-line: max-classes-per-file
export class ParserError implements Error {

  public name: string;
  public message: string;
  constructor(message: string) {
    this.name = "ParserError";
    this.message = message;
  }
}

// tslint:disable-next-line: max-classes-per-file
export class QuerySyntaxError implements Error {

  public name: string;
  public message: string;
  constructor(message: string) {
    this.name = "QuerySyntaxError";
    this.message = message;
  }
}
