"use strict";

import { defaultSerializationOptions, IX12SerializationOptions } from "./IX12SerializationOptions";
import { X12Segment } from "./X12Segment";

export class X12Transaction {
  public header: X12Segment | undefined;
  public trailer: X12Segment | undefined;

  public segments: X12Segment[];

  constructor() {
    this.segments = new Array<X12Segment>();
  }

  public toString(options?: IX12SerializationOptions): string {
    if (!this.header || !this.trailer) {
      throw new Error("header or trailer missing");
    }

    options = defaultSerializationOptions(options);
    console.log("TRANSACTION:");
    let edi = this.header.toString(options);

    if (options.format) {
      edi += options.endOfLine;
    }

    for (const segment of this.segments) {
      edi += segment.toString(options);

      if (options.format) {
        edi += options.endOfLine;
      }
    }

    edi += this.trailer.toString(options);

    return edi;
  }
}
