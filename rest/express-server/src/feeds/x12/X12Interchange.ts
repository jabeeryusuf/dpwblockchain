import { defaultSerializationOptions, IX12SerializationOptions } from "./IX12SerializationOptions";
import { X12FunctionalGroup } from "./X12FunctionalGroup";
import { X12Segment } from "./X12Segment";

export class X12Interchange {

  public header: X12Segment | undefined;
  public trailer: X12Segment | undefined;

  public functionalGroups: X12FunctionalGroup[];

  public segmentTerminator: string;
  public elementDelimiter: string;

  constructor(segmentTerminator: string, elementDelimiter: string) {
    this.functionalGroups = new Array<X12FunctionalGroup>();
    this.segmentTerminator = segmentTerminator;
    this.elementDelimiter = elementDelimiter;
  }

  public toString(options?: IX12SerializationOptions): string {
    if (!this.header || !this.trailer) {
      throw new Error("header or trailer missing");
    }

    options = defaultSerializationOptions(options);

    let edi = this.header.toString(options);

    if (options.format) {
      edi += options.endOfLine;
    }

    for (const group of this.functionalGroups) {
      edi += group.toString(options);

      if (options.format) {
        edi += options.endOfLine;
      }
    }

    edi += this.trailer.toString(options);

    return edi;
  }

}
