import { Inject } from "typescript-ioc";
import { ShipmentsService } from "../shipments/ShipmentsService";
export class Building {
  public building: any;
  @Inject private shipment: ShipmentsService;

  constructor(building: any, index: number, fileId: string) {

    this.building = building;

    // this.building.name = building.name;
   // this.building.type = building.type;
    let errorMessage: string;
    if (this.building.name === null) {
      errorMessage = `Building Name should be valid at row ${index}.`;
       // throw new Error(` Building Name should be valid at row ${index}`);
      this.addErrorLog(fileId, errorMessage);
    }

    if (this.building.type === null) {
      errorMessage = `Building type should be valid at row ${index}.`;
     // throw new Error(`Building type should be valid at row ${index}`);
      this.addErrorLog(fileId, errorMessage);
    }

    if (this.building.address === null || this.building.address === undefined) {
      if (this.building.lat === null || this.building.lat === undefined) {
        errorMessage = `Building address OR Building lat and lng should be valid at row ${index}.`;
        // throw new Error(`Building address is not provided than Building lat and lng both should be valid at row ${index}`);
        this.addErrorLog(fileId, errorMessage);
      }
    }

    /* if ((this.building.lat === null || this.building.lat === undefined) && (this.building.lng === null || this.building.lng === undefined)) {
      if (this.building.address === null || this.building.address === undefined) {
        errorMessage = `Building address is required, if Building lat and lng both not provided at row ${index}.`;
        // throw new Error(`Building address is required, if Building lat and lng both not provided at row ${index}`);
        this.addErrorLog(fileId, errorMessage);
      }
    } */

    this.building.error = errorMessage;

  }

  public toSimpleObj() {
    return this.building;
  }

  public async addErrorLog(fileId: string, errorMessage: string) {
    await this.shipment.addFeedsErrorLog(fileId, errorMessage);
  }
}
