import { Request } from "express";
import moment = require("moment");
import { Client } from "pg";
import { IDatabase } from "pg-promise";
import { IPurchaseOrdersService, Product, PurchaseOrder, PurchaseOrdersResponse, PurchaseOrderItem } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, Path, Security } from "typescript-rest";
import { DbClientSingleton } from "../utils";
import { ResponseStatus } from "tw-api-common";
import { PurchaseOrderSearch } from "./PurchaseOrderSearch";

@Path("/v0.1/purchaseOrders")
export class PurchaseOrdersService implements IPurchaseOrdersService {

    @ContextRequest private request: Request;
    @Inject private dbClient: DbClientSingleton;

    private db: Client;
    private dbp: IDatabase<any>;

    constructor() {
        this.db = this.dbClient.db;
        this.dbp = this.dbClient.dbp;
    }

    @GET
    @Security("USER")
    @Path("/info")
    public async info(@Param("purchaseOrderId") purchaseOrderId: string)
        : Promise<PurchaseOrdersResponse> {

        const orgId = this.request.user.orgId;
        const poRow = await this.dbp.oneOrNone(
            `SELECT * FROM purchase_orders WHERE purchase_order_id = $1 and org_id = $2`,
            [purchaseOrderId, orgId]);

        const poiRows = await this.dbp.any(
            `SELECT * FROM purchase_order_items WHERE purchase_order_id = $1 and org_id = $2`,
            [purchaseOrderId, orgId]);


        const purchaseOrder = this.dbToPurchaseOrder(poRow, poiRows);
        //console.log(purchaseOrder);

        return new PurchaseOrdersResponse({ status: ResponseStatus.OK, purchaseOrder });
    }

    @GET
    @Path("/query")
    public async query(@Param("whereClause") whereClause: string, @Param("whereParams") whereParams: string[],
        @Param("groupBy") groupBy: string[], @Param("orderBy") orderBy: string[]) {

        let sqlEnd = "";
        if (whereClause) {
            sqlEnd += " WHERE " + whereClause;
        }

        if (groupBy) {
            sqlEnd += " GROUP BY ";
            for (let i = 0; i < groupBy.length; i++) {
                if (i > 0) {
                    sqlEnd += ",";
                }
                sqlEnd += groupBy[i];
            }
        }

        if (orderBy) {
            sqlEnd += " ORDER BY ";
            for (let i = 0; i < orderBy.length; i++) {
                if (i > 0) {
                    sqlEnd += ",";
                }
                sqlEnd += orderBy[i];
            }
        }

        const iiSql = "SELECT * FROM purchase_orders" + sqlEnd;
        const iiRes = await this.db.query(iiSql, whereParams);
        const cache: any = {};
        console.log("iiRes = %j", iiRes);
        // let cache = await this.cacheContainersForItems(whereClause, whereParams);

        // debug('cache = %j', cache);
        const res = { items: iiRes.rows, cache };
        return res;
    }

    @GET
    @Path("/queryItems")
    public async queryItems(@Param("whereClause") whereClause: string, @Param("whereParams") whereParams: string[],
        @Param("groupBy") groupBy: string[], @Param("orderBy") orderBy: string[]) {

        console.log("queryPurchaseOrderItems");
        console.log([whereClause, whereParams, groupBy, orderBy]);

        let sqlEnd = "";
        if (whereClause) {
            sqlEnd += " WHERE " + whereClause;
        }

        if (groupBy) {
            sqlEnd += " GROUP BY ";
            for (let i = 0; i < groupBy.length; i++) {
                if (i > 0) {
                    sqlEnd += ",";
                }
                sqlEnd += groupBy[i];
            }
        }

        if (orderBy) {
            sqlEnd += " ORDER BY ";
            for (let i = 0; i < orderBy.length; i++) {
                if (i > 0) {
                    sqlEnd += ",";
                }
                sqlEnd += orderBy[i];
            }
        }
        let iiSql;
        if (groupBy) {
            iiSql = this.generateSelectClauseFromGroupBy(groupBy) + ", sum(quantity),b.name from purchase_order_items LEFT JOIN  buildings b on b.container_id = origin_container" + sqlEnd;
        } else {
            iiSql = "SELECT * from purchase_order_items" + sqlEnd;
        }
        console.log("mi friend!", iiSql);
        const iiRes = await this.db.query(iiSql, whereParams);
        const rows = iiRes.rows;
        const res = { items: rows };
        return res;

    }

    @GET
    @Path("/search")
    public async search(
        @Param("orgId") orgId: string,
        @Param("groupBy") groupBy: string,
        @Param("orderBy") orderBy: string,
        @Param("aggregate") aggregate: string,
        @Param("filterParams") filterParams?: any,
        @Param("searchString") searchString?: string,
        @Param("limit") limit?: number,
        @Param("cursor") cursor: string = "",
        @Param("desc") desc?: boolean,
    ): Promise<any> {
        const cursorNum = Number(cursor);
        const purchaseOrderSearch = new PurchaseOrderSearch(
            this.dbp,
            orgId,
            limit,
            cursorNum,
            filterParams,
            searchString,
            orderBy,
            groupBy,
            desc,
            aggregate,
        );

        const dbRows = await purchaseOrderSearch.search();
        const pageCount = await purchaseOrderSearch.getFullCount();
        const purchaseOrderList = this.createPurchaseOrderListFromDB({ items: dbRows });
        const nextCursor = String(cursorNum + limit);
        const POResp = new PurchaseOrdersResponse({
            metadata: { nextCursor, pageCount },
            purchaseOrders: purchaseOrderList,
            status: ResponseStatus.OK,
        });
        return POResp;
    }

    public createPurchaseOrderListFromDB(poInfoDB: any) {
        const poiMap: any = {};
        for (const row of poInfoDB.items) {
            const pid = row.purchase_order_id;

            poiMap[pid] = poiMap[pid] ? [...poiMap[pid], row] : [row];
        }

        const purchaseOrders: any = [];

        for (const pid of Object.keys(poiMap)) {
            const p = this.dbToPurchaseOrder(poiMap[pid][0], poiMap[pid]);
            purchaseOrders.push(p);
        }
        return purchaseOrders;
    }

    @GET
    @Path("/queryMilestoneExceptions")
    public async queryMilestoneExceptions(@Param("orgId") orgId: string) {

        // This is for GMT timezone
        moment.tz.setDefault("Europe/London");

        const sql = `SELECT distinct m.*, n.name as pol_name
               FROM(SELECT k.*, l.name as dest_name
                    FROM (SELECT i.*, j.name as origin_name
                          FROM (SELECT g.*, h.department, h.image, h.name AS prod_name
                                FROM (SELECT e.*, f.atd, f.etd
                                      FROM (SELECT c.*, d.shipment_id, d.shipment_allocation_date
                                            FROM (SELECT a.*, b.factory_delivery_date, b.container_id as location, b.receive_date, b.blocked_quantity
                                                  FROM (SELECT purchase_orders.purchase_order_id, purchase_order_items.purchase_order_item_id, purchase_order_items.quantity, purchase_order_items.origin_container,
                                                               purchase_order_items.dst_container, purchase_orders.create_date,
                                                               purchase_order_items.factory_booking_status, purchase_order_items.factory_booking_date, purchase_order_items.check_release_date, purchase_order_items.sku,
                                                               purchase_order_items.status, purchase_order_items.check_release_status, purchase_order_items.approx_recv_date, purchase_order_items.factory_appointment_date,
                                                               purchase_order_items.port_of_loading
                                                        FROM purchase_orders
                                                        JOIN purchase_order_items ON purchase_orders.purchase_order_id=purchase_order_items.purchase_order_id  and purchase_order_items.org_id = $1 where purchase_orders.org_id =$1) a
                                                  LEFT JOIN inventory_items b ON a.purchase_order_item_id=b.purchase_order_item_id AND a.purchase_order_id=b.purchase_order_id) c
                                            LEFT JOIN shipment_items d ON c.purchase_order_id=d.purchase_order_id AND c.purchase_order_item_id=d.purchase_order_item_id) e
                                      LEFT JOIN shipments f ON e.shipment_id=f.shipment_id WHERE f.ata IS NULL) g
                                LEFT JOIN products h ON g.sku = h.sku) i
                          LEFT JOIN buildings j ON i.origin_container = j.container_id) k
                    LEFT JOIN buildings l ON k.dst_container = l.container_id) m
               LEFT JOIN buildings n ON m.port_of_loading = n.container_id ORDER BY purchase_order_item_id ASC`;

        const res = await this.db.query(sql, [orgId]);
        const list = res.rows;
        const res2 = await this.db.query(`SELECT * FROM config_settings WHERE purpose='milestones'`);
        const intervalList = res2.rows;
        const settings: any = {};

        const seconds: any = {
            days: 86400,
            months: 2592000,
            weeks: 604800,
        };

        for (const row of intervalList) {
            settings[row.setting] = row;
        }

        const columns = ["create_date", "factory_booking_date", "factory_appointment_date", "factory_delivery_date", "receive_date", "shipment_allocation_date", "atd"];
        const exceptions = [];

        for (const row of list) {

            let j = columns.length - 1;
            while (row[columns[j]] === null) {
                j--;
            }
            if (columns[j] !== "atd") {
                const populatedColumn = columns[j];
                const aheadColumn = columns[j + 1];
                let aheadStr = aheadColumn.replace(new RegExp("_", "g"), " ");
                if (aheadStr === "atd") {
                    aheadStr = "time of departure";
                }
                const popStr = populatedColumn.replace(new RegExp("_", "g"), " ");

                const existingDate = moment(row[columns[j]]).utcOffset("+0000");
                const today = moment().utcOffset("+0000");
                let add = false;
                let msg = "";
                let status;
                let timestamp;

                let quantity = row.quantity;

                switch (populatedColumn) {
                    case "create_date":
                        const weeksUntilPOCons = moment(row.approx_recv_date).diff(today, "seconds", true);

                        if (weeksUntilPOCons < (settings.create_date.number * seconds[settings.create_date.interval])) {
                            add = true;
                            if (moment(row.approx_recv_date) <= today) {
                                msg = "PO consolidation date passed with no booking date";
                                status = "High";
                            } else {
                                msg = "PO consolidation date approaching with no booking date";
                                status = weeksUntilPOCons >= ((settings.create_date.number - 1) * seconds[settings.create_date.interval]) ? "Low" : "Medium";
                            }
                        }
                        break;
                    case "factory_booking_date":
                        if (today.diff(existingDate, "seconds", true) > settings.factory_booking_date.number * seconds[settings.factory_booking_date.interval]) {
                            if (row.factory_booking_status !== "APPROVED") {
                                add = true;
                                msg = "factory booking not approved after " + settings.factory_booking_date.number + " " + settings.factory_booking_date.interval;
                                status = "Medium";
                            }
                        }
                        break;
                    case "factory_appointment_date":
                        timestamp = existingDate.diff(today, "seconds", true);
                        if (timestamp < (settings.factory_appointment_date.number * seconds[settings.factory_appointment_date.interval])) { // 3 weeks
                            if (row.check_release_date === null) {
                                add = true;
                                if (existingDate <= today) {
                                    msg = "factory appointment date passed with no check release date";
                                    status = "High";
                                } else {
                                    msg = "factory appointment date within " + settings.factory_appointment_date.number + " " + settings.factory_appointment_date.interval + " with no check release date";
                                    if (timestamp >= ((settings.factory_appointment_date.number - 1) * seconds[settings.factory_appointment_date.interval])) {
                                        status = "Low";
                                    } else {
                                        status = "Medium";
                                    }
                                }
                            } else if (row.check_release_date && row.check_release_status !== "PASS") {
                                add = true;
                                msg = "factory appointment date within " + settings.factory_appointment_date.number + " " + settings.factory_appointment_date.interval + ", check release is: " + row.check_release_status;
                                if (timestamp >= ((settings.factory_appointment_date.number - 1) * seconds[settings.factory_appointment_date.interval])) {
                                    status = "Low";
                                } else {
                                    status = "Medium";
                                }
                            }
                        }
                        break;
                    case "factory_delivery_date":
                        timestamp = today.diff(existingDate, "seconds", true);
                        if (timestamp > settings.factory_delivery_date.number * seconds[settings.factory_delivery_date.interval]) { // 3 weeks
                            add = true;
                            msg = "receive date recording exceeds time limit of " + settings.factory_delivery_date.number + " " + settings.factory_delivery_date.interval;
                            if (timestamp >= (settings.factory_delivery_date.number - 1) * seconds[settings.factory_delivery_date.interval]) {
                                status = "Low";
                            } else {
                                status = "Medium";
                            }
                        }
                        break;
                    case "receive_date":
                        timestamp = today.diff(existingDate, "seconds", true);
                        if (timestamp > settings.receive_date.number * seconds[settings.receive_date.interval]) {
                            quantity = row.quantity - row.blocked_quantity;
                            add = quantity > 0 ? true : false;
                            msg = "shipment allocation recording exceeds time limit of " + settings.receive_date.number + " " + settings.receive_date.interval;
                            if (timestamp <= (settings.receive_date.number - 1) * seconds[settings.receive_date.interval]) {
                                status = "Low";
                            } else {
                                status = "Medium";
                            }
                        }
                        break;
                    case "shipment_allocation_date":
                        if (today.diff(existingDate, "seconds", true) > settings.shipment_allocation_date.number * seconds[settings.shipment_allocation_date.interval]) {
                            add = true;
                            msg = "time of departure recording exceeds time limit of " + settings.shipment_allocation_date.number + " " + settings.shipment_allocation_date.interval;
                            status = "Low";
                        }
                        break;
                    default:
                        if (today.diff(existingDate, "weeks", true) > 3) {
                            add = true;
                            msg = aheadStr + " recording exceeds time limit of 3 weeks since last timestamp";
                            status = "Low";
                        }
                }

                if (add) {
                    const vendorArr = row.origin_container.split("&"); // this is to isolate the vendor code from 'factory&...'

                    const obj = {
                        approx_recv_date: row.approx_recv_date ? moment(row.approx_recv_date).format("MM/DD/YYYY") : null,
                        check_release_date: row.check_release_date ? moment(row.check_release_date).format("MM/DD/YYYY") : "NULL",
                        check_release_status: row.check_release_status ? row.check_release_status : "NULL",
                        department: row.department ? row.department : "-",
                        dest_name: row.dest_name ? row.dest_name.substring(8, row.dest_name.length) : "-",
                        destination_container_id: row.destination_container_id,
                        factory_appointment_date: row.factory_appointment_date ? moment(row.factory_appointment_date).format("MM/DD/YYYY") : null,
                        factory_booking_date: row.factory_booking_date ? moment(row.factory_booking_date).format("MM/DD/YYYY") : null,
                        factory_booking_status: row.factory_booking_status ? row.factory_booking_status : "NULL",
                        image: row.image,
                        location: row.location,
                        msg,
                        origin_container: row.origin_container,
                        origin_name: row.origin_name,
                        pol_name: row.pol_name,
                        populatedColumn,
                        prodName: row.prod_name ? row.prod_name : "-",
                        purchase_order_id: row.purchase_order_id,
                        purchase_order_item_id: row.purchase_order_item_id,
                        quantity,
                        sku: row.sku,
                        status,
                        timestamp: popStr + ": " + moment(existingDate).format("MM/DD/YYYY"),
                        unpopulatedColumn: aheadColumn,
                        vendor_code: vendorArr[1],
                    };
                    exceptions.push(obj);
                }
            }
        }
        moment.tz.setDefault();
        return exceptions;
    }

    @GET
    @Path("/queryMilestones")
    public async queryMilestones(@Param("orgId") orgId: string) {
        const sql = `SELECT k.*, l.name as pol_name
               FROM(SELECT distinct i.*, j.name
                    FROM (SELECT g.*, h.department, h.image, h.name AS prodName
                          FROM (SELECT e.*, f.atd, f.ata
                                FROM (SELECT c.*, d.shipment_id, d.shipment_allocation_date
                                      FROM (SELECT a.*, b.factory_delivery_date, b.receive_date
                                            FROM (SELECT purchase_orders.purchase_order_id, purchase_order_items.purchase_order_item_id, purchase_orders.create_date,
                                                  purchase_order_items.factory_booking_date,purchase_order_items.approx_recv_date, purchase_order_items.check_release_date, purchase_order_items.check_release_status,purchase_order_items.status,
                                                  purchase_order_items.sku, purchase_order_items.quantity, purchase_order_items.dst_container, purchase_order_items.origin_container, purchase_order_items.port_of_loading
                                                  FROM purchase_orders
                                                  JOIN purchase_order_items ON purchase_orders.purchase_order_id=purchase_order_items.purchase_order_id WHERE purchase_order_items.org_id = $1 and purchase_orders.org_id = $1) a
                                            LEFT JOIN inventory_items b ON a.purchase_order_item_id=b.purchase_order_item_id AND a.purchase_order_id=b.purchase_order_id) c
                                      LEFT JOIN shipment_items d ON c.purchase_order_id=d.purchase_order_id AND c.purchase_order_item_id=d.purchase_order_item_id) e
                                LEFT JOIN shipments f ON e.shipment_id=f.shipment_id) g
                          LEFT JOIN products h on g.sku = h.sku) i
                    LEFT JOIN buildings j on i.origin_container = j.container_id) k
               LEFT JOIN buildings l on k.port_of_loading = l.container_id ORDER BY purchase_order_item_id ASC`;
        const res = await this.db.query(sql, [orgId]);
        for (const row of res.rows) {
            const vendorArr = row.origin_container.split("&"); // this is to isolate the vendor code from 'factory&...'
            row.vendor_code = vendorArr[1];

            if (row.create_date) {
                row.create_date = moment(row.create_date).format("MM/DD/YYYY");
            }
            if (row.factory_booking_date) {
                row.factory_booking_date = moment(row.factory_booking_date).format("MM/DD/YYYY");
            }
            if (row.check_release_date) {
                row.check_release_date = moment(row.check_release_date).format("MM/DD/YYYY");
            }
            if (row.factory_delivery_date) {
                row.factory_delivery_date = moment(row.factory_delivery_date).format("MM/DD/YYYY");
            }
            if (row.approx_recv_date) {
                row.approx_recv_date = moment(row.approx_recv_date).format("MM/DD/YYYY");
            }
            if (row.receive_date) {
                row.receive_date = moment(row.receive_date).format("MM/DD/YYYY");
            }
            if (row.shipment_allocation_date) {
                row.shipment_allocation_date = moment(row.shipment_allocation_date).format("MM/DD/YYYY");
            }
            if (row.ata) {
                row.ata = moment(row.ata).format("MM/DD/YYYY");
            }
            if (row.atd) {
                row.atd = moment(row.atd).format("MM/DD/YYYY");
            }
        }
        return res;
    }

    @GET
    @Path("/getPurchaseOrderIDs")
    public async getPurchaseOrderIDs(@Param("whereClause") whereClause: string) {
        const sqlStart = "SELECT DISTINCT purchase_order_id FROM purchase_order_items";
        let sqlEnd = "";

        if (whereClause) {
            sqlEnd += " WHERE " + whereClause + "ORDER BY purchase_order_id asc";
        }
        const sql = sqlStart + sqlEnd;
        const res = this.db.query(sql);
        return res;

    }

    @GET
    @Path("/queryManufacturers")
    public async queryManufacturers() {
        const sqlResponse = await this.db.query("SELECT buildings.lat, buildings.lng, sum(purchase_order_items.quantity), purchase_order_items.origin_container from purchase_order_items JOIN buildings on purchase_order_items.origin_container = buildings.container_id group by purchase_order_items.origin_container, buildings.lat, buildings.lng");

        return { containers: sqlResponse.rows, cache: {} };
    }
    // Need to make this more general

    public async addPurchaseOrder(purchaseOrderId: any, createDate: any, orgId?: string) {
        const newPO = await this.db.query(
            "INSERT INTO purchase_orders (purchase_order_id,create_date, org_id) VALUES ($1,$2, $3)", [purchaseOrderId, createDate, orgId]);
        return newPO;
    }

    public async addPurchaseOrderItem(poItem: any) {
        // tslint:disable-next-line: max-line-length
        const { purchase_order_item_id, purchase_order_id, sku, quantity, origin_container, dst_container, port_of_loading, org_country } = poItem;
        const insertArr = [purchase_order_item_id, purchase_order_id, sku, quantity, origin_container, dst_container, port_of_loading, org_country];
        const newItem = await this.db.query(
            "INSERT INTO purchase_order_items (purchase_order_item_id, purchase_order_id,sku, quantity,origin_container,dst_container,port_of_loading,org_country) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)", insertArr);
        return newItem;
    }

    public async updatePurchaseOrderItem(updateObj: any, whereClause: any, whereParams: any) {
        let iiSql = "UPDATE purchase_order_items SET ";
        let sqlEnd = "";
        let first = true;
        for (const key in updateObj) {
            if (first === false) {
                iiSql += ",";
            } else {
                first = false;
            }
            iiSql += String(key) + " = '" + String(updateObj[key]) + "'";
        }
        if (whereClause) {
            sqlEnd += " WHERE " + whereClause;
        }
        iiSql += sqlEnd;
        this.db.query(iiSql, whereParams);

    }

    public async updatePurchaseOrder(updateObj: any, whereClause: any, whereParams: any) {
        let iiSql = "UPDATE purchase_orders SET ";
        let sqlEnd = "";
        let first = true;
        // tslint:disable-next-line: forin
        for (const key in updateObj) {
            if (first === false) {
                iiSql += ",";
            } else {
                first = false;
            }
            iiSql += String(key) + " = '" + String(updateObj[key]) + "'";
        }
        if (whereClause) {
            sqlEnd += " WHERE " + whereClause;
        }
        iiSql += sqlEnd;
        return await this.db.query(iiSql, whereParams);

    }

    /// need to improve naming
    private generateSelectClauseFromGroupBy(groupBy: string[]) {
        let sqlStart = "SELECT ";
        groupBy.forEach((val: any, idx: any) => {
            if (idx > 0) {
                sqlStart += ", ";
            }
            sqlStart += val;
        });
        return sqlStart;
    }

    private dbToPurchaseOrder(poRow: any, poiRows: any): PurchaseOrder {
        const po = new PurchaseOrder();

        po.purchaseOrderId = poRow.purchase_order_id;
        po.createDate = poRow.create_date;

        po.items = [];
        for (const poiRow of poiRows) {
            const poi = new PurchaseOrderItem();
            poi.purchaseOrderItemRef = poiRow.purchase_order_item_id;

            // info
            poi.status = poiRow.status;
            poi.productId = poiRow.sku;
            poi.countryOfOrigin = poiRow.org_country;

            // Quantities
            poi.quantity = poiRow.quantity;
            poi.receivedQuantity = poiRow.recv_quantity;

            // Quantities
            poi.lot = poiRow.lot;
            poi.eta = poiRow.eta;
            poi.departmentid = poiRow.department_id;

            // Anko Dates
            poi.checkReleaseDate = poiRow.check_release_date;
            poi.checkReleaseStatus = poiRow.check_release_status;
            poi.factoryBookingDate = poiRow.factory_booking_date;
            poi.approxRecvDate = poiRow.approx_recv_date;

            const p = new Product();
            p.type = poiRow.department ? poiRow.department : null;
            p.imageLink = poiRow.image ? poiRow.image : null;
            p.title = poiRow.name ? poiRow.name : null;

            const obj = Object.assign({}, poi, p);
            po.items.push(obj);
        }

        return po;
    }

}
