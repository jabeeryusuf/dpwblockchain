export class PurchaseOrderSearch {
  private db: any;
  private orgId: any;
  private limit: any;
  private cursor: any;
  private filterParams: object;
  private searchString: string;
  private orderBy: string;
  private groupBy: string;
  private desc: boolean;
  private fullCount: number;
  private aggregate: string;
  constructor(
    db: any,
    orgId: string,
    limit: number,
    cursor: number,
    filterParams: object,
    searchString: string,
    orderBy: string,
    groupBy: string,
    desc: boolean,
    aggregate: string) {
    this.db = db;
    this.desc = desc;
    this.orgId = orgId;
    this.limit = limit;
    this.cursor = cursor;
    this.filterParams = filterParams;
    this.searchString = searchString;
    this.orderBy = orderBy;
    this.groupBy = groupBy;
    this.fullCount = 0;
    this.aggregate = aggregate;
  }

  public getFullCount() {
    return this.fullCount;
  }

  public async search(): Promise<any[]> {
    console.log("hey coming>>>>", this.desc, "limit", this.limit, "aggregate", this.aggregate, "cursor", this.cursor, "filterre", this.filterParams, "searchString", this.searchString, "groupBY", this.groupBy, "orderBy", this.orderBy);
    if (this.searchString.length === 0) {
      this.searchString = this.searchString + "%";
    } else {
      this.searchString = "%" + this.searchString + "%";
    }

    let PORes ;
    if (this.aggregate === "po") {
      PORes = await this.db.any(
                                    `
                                    SELECT  ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + `, count(*) OVER() AS full_count
                                    FROM purchase_orders
                                      LEFT JOIN purchase_order_items
                                            ON (
                                                  purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
                                                )
                                      LEFT JOIN products
                                            ON  (
                                                  products.sku = purchase_order_items.sku
                                                  AND products.org_id = $<orgId>
                                                )
                                      WHERE purchase_orders.org_id = $<orgId> ` +
                                      this.generateWhereClauseShips(this.filterParams) + `
                                      AND ( purchase_orders.purchase_order_id LIKE $<searchString>
                                        OR products.department LIKE $<searchString>
                                        OR products.name LIKE $<searchString>
                                        OR purchase_order_items.sku LIKE $<searchString>
                                        OR purchase_order_items.quantity::text LIKE $<searchString>
                                        )
                                      GROUP BY ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + `
                                      ORDER BY max(` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + `)`
                                      + this.addDesc() + `
                                      offset $<cursor> limit $<limit>
                                    `, Object.assign(
                                      { cursor: +this.cursor,
                                        limit: this.limit,
                                        orgId: this.orgId,
                                        searchString: this.searchString,
                                      },
                                      this.filterParams ? this.filterParams : {}));

      if (PORes.length === 0) {
        return [];
      }

      this.fullCount = Math.ceil(PORes[0].full_count / this.limit);

      const purchaseOrderIdList = PORes.map((val: any) => val[this.getColumn(this.groupBy)]);

      const sql = `
                  SELECT products.*,
                  purchase_orders.create_date,
                  purchase_orders.purchase_order_id,
                  purchase_order_items.*
                  FROM purchase_orders
                  LEFT JOIN purchase_order_items on purchase_order_items.purchase_order_id = purchase_orders.purchase_order_id
                  LEFT JOIN products on products.sku = purchase_order_items.sku
                  where purchase_orders.org_id = $1 AND ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + ` ::text in ($2:csv)
                  ORDER BY ` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + this.addDesc()  + `
                  `;

      const queryList = await this.db.any( sql, [this.orgId, purchaseOrderIdList]);

      return queryList;
    } else {

      var sqlqr = `
      SELECT products.department, products.name,products.department_id, products.image, a.* FROM
            ( SELECT purchase_order_items.recv_quantity,
                      purchase_order_items.quantity, purchase_order_items.approx_recv_date,
                      purchase_order_items.purchase_order_item_id,
                      purchase_order_items.sku,
                          shipment_items.production_lot as lot,
                      purchase_order_items.org_id,
                      purchase_orders.purchase_order_id,
                      purchase_orders.create_date,
                      shipments.eta,
                      shipments.original_eta,
                      shipments.ata,
                      shipments.shipment_status,
                      count(*) OVER() AS full_count
              FROM purchase_orders
                LEFT JOIN purchase_order_items
                      ON  purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
                      LEFT JOIN shipment_items
                      ON  purchase_order_items.purchase_order_id = shipment_items.purchase_order_id
                      left join shipments
                      On shipment_items.shipment_id = shipments.shipment_id
                     
                      WHERE purchase_orders.org_id = '`+this.orgId+`'
                      ORDER BY ` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + ``
                          + this.addDesc() + `
                      ) a
          LEFT JOIN products
                ON  (
                      products.sku = a.sku
                      AND products.org_id = '`+this.orgId+`' OR products.org_id IS NULL
                    )
          WHERE a.org_id = '`+this.orgId+`' ` +
          this.generateWhereClauseShips(this.filterParams) + `
          AND ( a.purchase_order_id LIKE '`+this.searchString+`'
            OR products.department LIKE '`+this.searchString+`'
            OR products.name LIKE '`+this.searchString+`'
            OR a.sku LIKE '`+this.searchString+`'
            OR a.quantity::text LIKE '`+this.searchString+`'
            )
          offset `+this.cursor+` limit `+this.limit+`
        `;

      //console.log(sqlqr);
      
      console.log("BEFORE QUERY EXECUTE");
      PORes = await this.db.any(sqlqr);
     // console.log("Lenagth is");
      //console.log(PORes.length);
      //console.log("RESULTS AREy");
      //console.log(PORes);
      /*
      PORes = await this.db.any(
                                `
                                  SELECT products.department, products.name,products.department_id, products.image, a.* FROM
                                        ( SELECT purchase_order_items.recv_quantity,
                                                  purchase_order_items.quantity, purchase_order_items.approx_recv_date,
                                                  purchase_order_items.purchase_order_item_id,
                                                  purchase_order_items.sku,
                                                      shipment_items.production_lot as lot,
                                                  purchase_order_items.org_id,
                                                  purchase_orders.purchase_order_id,
                                                  purchase_orders.create_date,
                                                  shipments.eta,
                                                  shipments.original_eta,
                                                  shipments.ata,
                                                  shipments.shipment_status,
                                                  count(*) OVER() AS full_count
                                          FROM purchase_orders
                                            LEFT JOIN purchase_order_items
                                                  ON  purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
                                                  LEFT JOIN shipment_items
                                                  ON  purchase_order_items.purchase_order_id = shipment_items.purchase_order_id
                                                  left join shipments
                                                  On shipment_items.shipment_id = shipments.shipment_id
                                                 
                                                  WHERE purchase_orders.org_id = $<orgId>
                                                  ORDER BY ` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + ``
                                                      + this.addDesc() + `
                                                  ) a
                                      LEFT JOIN products
                                            ON  (
                                                  products.sku = a.sku
                                                  AND products.org_id = $<orgId> OR products.org_id IS NULL
                                                )
                                      WHERE a.org_id = $<orgId> ` +
                                      this.generateWhereClauseShips(this.filterParams) + `
                                      AND ( a.purchase_order_id LIKE $<searchString>
                                        OR products.department LIKE $<searchString>
                                        OR products.name LIKE $<searchString>
                                        OR a.sku LIKE $<searchString>
                                        OR a.quantity::text LIKE $<searchString>
                                        )
                                      offset $<cursor> limit $<limit>
                                    `
                                  , Object.assign(
                                    { cursor: +this.cursor,
                                      limit: this.limit,
                                      orgId: this.orgId,
                                      searchString: this.searchString,
                                    },
                                    this.filterParams ? this.filterParams : {}));

      */

      if (PORes.length === 0) {
        return [];
      }

      this.fullCount = Math.ceil(PORes[0].full_count / this.limit);

      return PORes;
    }
  }

  private addDesc() {
    if (this.desc) {
      return " desc ";
    }
    return " ";
  }

  private generateWhereClauseShips(filters: any) {
    let whereClause = "";
    if (filters) {
      if (filters.pos === "open") {
        whereClause = whereClause + " and  purchase_order_items.recv_quantity != purchase_order_items.quantity";        
      }
      else if (filters.pos === "in-transit")
      {
        whereClause = whereClause + "and a.eta>=localtimestamp and ((a.ata>localtimestamp) or a.ata isnull)";  
      }
      else if (filters.pos === "delayed")
      {
        whereClause = whereClause + "and DATE_PART('day', a.eta - a.original_eta)>=7 and (NOT (a.ata<localtimestamp) or a.ata isnull)";  
      }
      else if (filters.pos === "completed")
      {
        whereClause = whereClause + " and a.ata<=localtimestamp and a.shipment_status='completed'";  
      }

      return whereClause;
    } 
    else 
    {
      return whereClause;
    }

  }

  // since we are working with multiple tables
  // some of which have columns of the same name we occasionally need
  // to specify which table we are referencing
  // this function provides a mapping to tables
  // to avoid table ambiguity error
  private addTablePrefixToColumns(col: string) {
    const ProductPrefixMap: any = {
      department: "products.department",
      image: "products.image",
      sku: "purchase_order_items.sku",
    };
    const PurchaseOrderPrefixMap: any = {
      create_date: "purchase_orders.create_date",
      purchase_order_id: "purchase_orders.purchase_order_id",
    };
    if (ProductPrefixMap[col]) {
      return ProductPrefixMap[col];
    }
    if (PurchaseOrderPrefixMap[col]) {
      return PurchaseOrderPrefixMap[col];
    }
    return "purchase_order_items." + col;
  }

  // Function take camel case table columns and map it to the equivelent string corresponding to a column in our db
  private getColumn(property: string) {

    const columnMap: any = {
      approxRecvDate: "approx_recv_date",
      createDate: "create_date",
      link: "purchase_order_id",
      productId: "sku",
      productImage: "image",
      productName: "name",
      productType: "department",
      purchaseOrderId: "purchase_order_id",
      purchaseOrderItemRef: "purchase_order_item_id",
      quantity: "quantity",
      recvQuantity: "recv_quantity",
    };

    return columnMap[property];
  }

}
