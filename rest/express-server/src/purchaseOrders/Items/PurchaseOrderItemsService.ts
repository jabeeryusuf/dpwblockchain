import * as _ from "lodash";
import * as md5 from "md5";
import * as moment from "moment";
import { Client } from "pg";
import { IPurchaseOrderItemsService } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { Path, POST  } from "typescript-rest";
import { ProductsService } from "../../products/ProductsService";
import { DbClientSingleton } from "../../utils";
import { PurchaseOrdersService } from "../PurchaseOrdersService";

@Path("/v0.1/purchaseOrders")
export class PurchaseOrderItemsService implements IPurchaseOrderItemsService {
  @Inject private dbClient: DbClientSingleton;
  @Inject private productsService: ProductsService;
  @Inject private purchaseOrdersService: PurchaseOrdersService;
  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  @POST
  @Path("/create")
  public async create(Obj: any): Promise<any> {
    const { orgId, sourceContainerId, destinationContainerId } = Obj;
    const groupByPO = _.groupBy(Obj.items, "purchaseOrderId");
    const groupByPOArray =  Object.values(groupByPO);
    for (const i of groupByPOArray) {
      let quantity = 0;
      let purchaseOrderItemId: string;
      let purchaseOrderId: string;
      let productId: string;
      let manufacture: string;
      const productObject = {} as any;
      productObject.org_id = orgId;
      for (const j of i) {
        productObject.sku = j.productId;
        productObject.name = j.productName;
        productObject.department = j.productType;
        quantity = quantity + j.quantity;
        purchaseOrderItemId = j.purchaseOrderItemId;
        purchaseOrderId = j.purchaseOrderId;
        productId = j.productId;
        manufacture = "factory&" + md5(j.manufacture).slice(0, 10);
      }
      const currentPO = await this.purchaseOrdersService.query("purchase_order_id =$1 and org_id = $2", [purchaseOrderId, orgId], undefined, undefined);
      if (!currentPO.items.length) {
        await this.purchaseOrdersService.addPurchaseOrder(purchaseOrderId, moment(), orgId);
      }
      const currentPOItems = await this.purchaseOrdersService.queryItems("purchase_order_id =$1 and org_id = $2", [purchaseOrderId, orgId], undefined, undefined);
      if (!currentPOItems.items.length) {
        await this.db.query(` Insert into purchase_order_items
                                                (purchase_order_item_id, purchase_order_id, sku, quantity, origin_container, port_of_loading, dst_container, org_id)
                                            Values($1, $2, $3, $4, $5, $6, $7, $8)`,
                                            [purchaseOrderItemId, purchaseOrderId, productId, quantity, manufacture, sourceContainerId, destinationContainerId, orgId]);
      }
      const productExist = await this.db.query(` Select * from products where org_id = $1 and sku = $2`, [orgId, productId]);
      if (productExist.rows.length < 1 ) {
        await this.productsService.create(productObject);
      }
    }
    return ;
  }
}
