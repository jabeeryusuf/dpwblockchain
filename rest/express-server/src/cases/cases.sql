CREATE TABLE case_types (
  case_type_id    UUID,
  org_id          TEXT,
  name            TEXT,

  CONSTRAINT case_types_const_org_id_name UNIQUE(org_id,name)
);

CREATE TABLE case_assets (
  case_id         UUID NOT NULL,
  asset_type      TEXT NOT NULL,
  asset_id        TEXT NOT NULL,

  CONSTRAINT case_associations_unique UNIQUE(case_id, asset_type, asset_id)
);