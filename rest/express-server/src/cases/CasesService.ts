import { Request } from "express";
import { Asset, Case, Group, GroupType} from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, PATCH, Path, POST, Security } from "typescript-rest";
import { GroupsService } from "../groups";
import { DbClientSingleton } from "../utils";
import { CasesDb } from "./CasesDb";

@Path("/v0.1/cases")
@Security("USER")
export class CasesService {

  @ContextRequest private request: Request;

  @Inject private casesDb: CasesDb;
  @Inject private dbClient: DbClientSingleton;
  @Inject private groupsService: GroupsService;

  @POST
  @Path("/create")
  public async create(c: Case): Promise<Case> {
    console.dir(c);
    // c.caseId = uuid();
    // This should probably be moved to its own function
    // Querying the db to get the group_id for the vendor(s)
    const GroupIDs = await Promise.all(c.assets.filter((val2:any)=>{
      return val2.type != 'sku'
    }).map(async (val:any) => {
      if (val.type === "vendor") {
        const vgroupId = await this.dbClient.db.query(
          `
        SELECT group_id from vendors where vendor_code = $1 and org_id = $2
        `,
          [val.id, c.orgId],
        );
        return vgroupId.rows[0].group_id;
      } else if(val.type == 'group' ) {
        return val.id;
      }
    }));
    console.log(c.subject);

    const pGroup = new Group();
    pGroup.name = c.subject;
    pGroup.orgId = c.orgId;
    pGroup.type = GroupType.CASE;
    console.log([c.ownerUserId, c.assignedEntityId].concat(GroupIDs));
    const ret = await this.groupsService.create(pGroup, [], GroupIDs, [c.ownerUserId]);
    c.caseId = ret.group.groupId;

    console.log("case id = " + c.caseId);
    console.dir(c);

    // Create the case
    await this.dbClient.db.query(`
      INSERT INTO cases (case_id, subject, org_id, case_status, group_id, case_type_id,
                          case_description, case_priority, owner, case_assign_to)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
      [c.caseId, c.subject, c.orgId, "New", c.caseId, c.caseTypeId,
        c.description, c.priority, c.ownerUserId, c.assignedEntityId]);

    console.log("casesCreate: completed insert");
    await this.updateAssets(c.caseId, c.assets);

    console.log("returning");
    console.dir(c);
    return c;
  }

  @PATCH  // REST TERM FOR UPDATE!!!
  @Path("/update")
  public async update(c: Case): Promise<Case> {

    console.dir(c);

    await this.dbClient.db.query(`
      UPDATE cases
      SET case_status = $1, case_type_id = $2, case_priority = $3, case_assign_to = $4,
          owner = $5
      WHERE case_id = $6`,
      [c.status, c.caseTypeId, c.priority, c.assignedEntityId, c.ownerUserId, c.caseId]);
    return c;
  }

  @GET
  @Path("/members")
  public async members(@Param("caseId") caseId: string) {
    return await this.casesDb.getPeopleInCase(caseId);
  }

  @GET
  @Path("/list")
  public async list(): Promise<Case[]> {
    const orgId = this.request.user.orgId;
    const rows: any = await this.casesDb.queryCasesForOrg(orgId);
    const cases = rows.map(this.dbRowToCase);
    return cases;
  }

  @GET
  @Path("/info")
  public async info(@Param("caseId") caseId: string): Promise<Case> {
    const row: any = await this.casesDb.queryCaseById(caseId);
    return row ? row.map(this.dbRowToCase) :  null;
  }

  @GET
  @Path("/searchById")
  public async searchById(@Param("caseId") caseId: string): Promise<Case[]> {
    const orgId = this.request.user.orgId;
    const result =
      await this.dbClient.db.query(`SELECT * FROM cases WHERE case_id::text LIKE '%${caseId}% AND org_id = $1'`,
      [orgId]);

    return result.rowCount ? result.rows.map(this.dbRowToCase) : null;
  }

  @GET
  @Path("/files")
  public async files(@Param("caseId") caseId: string) {
    const result = await this.dbClient.db.query(`
      SELECT * FROM case_files join files on case_files.file_id = files.file_id where case_files.case_id = $1`,
      [caseId]);

    return result.rows;
  }

  @POST
  @Path("/addFile")
  public async addFile(params: any) {
    console.log("add file to case");
    const newCaseFile = await this.casesDb.db.query("INSERT INTO case_files (case_id,file_id) VALUES ($1,$2)", 
      [params.caseId, params.fileId]);
    return newCaseFile;
  }

  private dbRowToCase = (row: any) => {
    const c: Case = new Case();
    c.caseId = row.case_id;
    c.subject = row.subject;
    c.orgId = row.org_id;
    c.status = row.case_status;
    c.priority = row.case_priority;
    c.caseTypeId = row.case_type_id;
    c.description = row.case_description;
    c.assignedEntityId = row.case_assign_to;
    c.ownerUserId = row.owner;
    c.assets =  [ { id: row.asset_id , type: row.asset_type} ];

    return c;
  }




  private updateAssets = async (caseId: string, assets: Asset[]) => {
    for (const a of assets) {
      await this.dbClient.db.query(`
      INSERT INTO case_assets (case_id, asset_type, asset_id ) VALUES ($1, $2, $3)`,
        [caseId, a.type, a.id]);
    }
  }

}
