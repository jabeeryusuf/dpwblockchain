import { Client } from "pg";

import { Inject, Singleton } from "typescript-ioc";
import { DbClientSingleton } from "../utils";

// tslint:disable-next-line: interface-name class-name
interface db_case_types_row {
  asset_id: string;
  asset_type: string;
  case_type_id: string;
  org_id: string;
  name: string;
  message_template_id: string;
}

@Singleton
export class CasesDb {

  public db: Client;

  @Inject
  private dbClient: DbClientSingleton;

  constructor() {
    this.db = this.dbClient.db;
  }

  public async getPeopleInCase(groupId: any) {
    const query = await this.db.query(`WITH RECURSIVE userdetails AS (SELECT
                                                                    group_id,
                                                                    child_group_id
                                                                    FROM
                                                                    group_group_members
                                                                    WHERE group_id = $1

                                                                    UNION

                                                                    SELECT
                                                                    g.group_id,
                                                                    g.child_group_id
                                                                    FROM
                                                                    group_group_members g

                                                                    JOIN userdetails s ON g.group_id = s.child_group_id)

                                                                    SELECT u.email, u.user_id, u.name FROM userdetails s1 , users u WHERE
                                                                    s1.child_group_id = u.user_id ;`, [groupId]);
    return query.rows.length > 0 ? query.rows : [];
  }

  public async defaultGroupEmail(orgId: any, orgNames: any, factoryCode: any) {
    try {
      console.log("Inside getting default Group Email ", orgId, orgNames, factoryCode);
      let newOrgNames;
      let flag = false;
      let newAction1;
      if (orgNames.includes("factory")) {
        newOrgNames = orgNames.filter((e: any) => e !== "factory");
        flag = true;
      } else {
        newOrgNames = orgNames;
      }
      let sql = "";
      if (flag) {
        sql = `WITH RECURSIVE userdetails(default_contact_group, child_type) AS (
          select
          default_contact_group, ref
          from
          providers
          where org_id = $1 and ref = ANY ($2) and provider_org_id = $3

          UNION

          select
          g.child_group_id, g.child_type
          from
          group_group_members g

          JOIN userdetails s on g.group_id = s.default_contact_group

      ) select  s1.*, u.email, u.name from userdetails s1 left join users u on s1.default_contact_group = u.user_id ; `;
        newAction1 = await this.db.query(sql, [orgId, ["factory"], factoryCode]);
      }

      sql = `WITH RECURSIVE userdetails(default_contact_group, child_type) AS (
        select
        default_contact_group, ref
        from
        providers
        where org_id = $1 and ref = ANY ($2)

        UNION

        select
        g.child_group_id, g.child_type
        from
        group_group_members g

        JOIN userdetails s on g.group_id = s.default_contact_group

    ) select  s1.*, u.email, u.name from userdetails s1 left join users u on s1.default_contact_group = u.user_id ;`;
      const newAction2 = await this.db.query(sql, [orgId, newOrgNames]);
      let newAction;
      if (flag) {
        newAction = newAction1.rows.concat(newAction2.rows);
      } else {
        newAction = newAction2.rows;
      }
      return newAction;
    } catch (err) {
      console.log(err);
    }
  }

  public async update(caseId: any, casePriority: any) {
    const sql = `UPDATE cases SET case_priority = $1 where case_id = $2`;
    const newAction = await this.db.query(sql, [casePriority, caseId]);
    console.log("newactionss", newAction);
    return newAction;
  }

  public async getCaseTypes(orgId: string): Promise<db_case_types_row[]> {
    const query = await this.dbClient.db.query(
      `SELECT * FROM case_types WHERE org_id=$1 ORDER BY name ASC`, [orgId]);
    return query.rows;
  }

  public async saveCaseType(row: any) {

    console.log("ROW:", row);

    const upsert = await this.dbClient.db.query(`
      INSERT INTO case_types
        (case_type_id, org_id, name, asset_id, asset_type, message_template_id)
      VALUES ($1, $2, $3, $4, $5, $6)
      ON CONFLICT ON CONSTRAINT case_types_const_org_id_name DO UPDATE
      SET name = EXCLUDED.name, asset_id = EXCLUDED.asset_id, asset_type = EXCLUDED.asset_type,
      message_template_id = EXCLUDED.message_template_id`,
      [row.case_type_id, row.org_id, row.name, row.asset_id, row.asset_type, row.message_template_id]);

    return upsert;
  }

  public async deleteCaseType(row: any) {
    const deleteRow = await this.dbClient.db.query(`
      DELETE FROM case_types WHERE case_type_id = $1`,
      [row.case_type_id]);
    return deleteRow;
  }

  /**
   * Function used primarily for the "All Case Page".
   * Grabs all of the cases associated with the org as well as the cases
   * the user is a part of that are not necessarily associated with their orasg
   * @param {string} userId
   * @param {string} orgId
   */
  public async queryCasesForOrg(orgId: string) {

    const results = await this.db.query(`SELECT * FROM cases c WHERE c.org_id = $1`,
     [orgId]);

    return results.rows ? results.rows : [];

  }

  public async queryCaseById(caseId: string) {
    const results = await this.db.query(`SELECT c.*, a.asset_type, a.asset_id FROM cases c LEFT JOIN case_assets a ON c.case_id = a.case_id WHERE c.case_id = $1`,
      [caseId]);

    return results.rows ? results.rows : null;
  }

}
