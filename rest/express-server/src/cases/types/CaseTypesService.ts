// Path: /cases/types
import { Request } from "express";
import { CaseType, ICaseTypesService } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, PATCH, Path, POST, Security } from "typescript-rest";
import { v4 as uuid } from "uuid";
import { CasesDb } from "../CasesDb";

@Path("/v0.1/cases/types")
@Security("USER")
export class CaseTypesService implements ICaseTypesService {

  @ContextRequest private request: Request;
  @Inject private caseDb: CasesDb;

  @GET
  @Path("/list")
  public async list(): Promise<CaseType[]> {
    console.log("GET_CASE_TYPES");
    const orgId = this.request.user.orgId;
    const rows = await this.caseDb.getCaseTypes(orgId);
    return rows.map( (t) => ({
      assetId: t.asset_id,
      assetType: t.asset_type,
      caseTypeId: t.case_type_id,
      messageTemplateId: t.message_template_id,
      name: t.name,
      orgId: t.org_id,
    }));
  }

  @POST
  @Path("/create")
  public async create(type: CaseType) {
    await this.caseDb.saveCaseType({
      case_type_id: uuid(),
      name: type.name,
      org_id: type.orgId,
    });
  }

  @PATCH
  @Path("/update")
  public async update(type: CaseType) {
    await this.caseDb.saveCaseType({
      asset_id: type.assetId,
      asset_type: type.assetType,
      case_type_id: type.caseTypeId,
      message_template_id: type.messageTemplateId,
      name: type.name,
      org_id: type.orgId,
    });
  }

  @PATCH
  @Path("/delete-case")
  public async deleteCaseType(caseTypeId: any) {
    await this.caseDb.deleteCaseType({
      case_type_id: caseTypeId.caseTypeId,
    });
  }
}
