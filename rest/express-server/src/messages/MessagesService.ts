import * as path from "path";
import * as _ from "lodash";

import { MessageTemplate } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { v4 as uuid } from "uuid";
import { EmailsService } from "../emails/EmailsService";
import { FilesService } from "../files";
import { GroupsService } from "../groups";
import { MessageDb } from "./MessageDb";
import { Path, POST, Param, GET } from "typescript-rest";


// move to messages at some point
@Path("/v0.1/message")
export class MessageService {

  private users: any;
  private sockets: any;
  private orgSockets: any;
  // private io: any;

  @Inject private messageDb: MessageDb;
  @Inject private groupService: GroupsService;
  @Inject private fileService: FilesService;
  @Inject private emailsService: EmailsService;
  // private userService: UserService;

  constructor() {
    this.users = {};
    this.sockets = {};
    this.orgSockets = {};
  }

  public async onConnect(userId: string, orgId: string, socket: any) {
    if (!this.users[userId]) {
      this.users[userId] = [];
    }

    if (!this.orgSockets[orgId]) {
      this.orgSockets[orgId] = [];
    }

    this.users[userId].push(socket);
    this.sockets[socket.id] = userId;
    this.orgSockets[orgId].push(userId);
  }

  public async onDisconnect(socket: any) {
    const uid = this.sockets[socket.id];
    if (uid && this.users[uid]) {
      // console.log('removing ' + socket.id);
      // console.log(this.users[uid].length);
      this.users[uid].splice(this.users[uid].find((s: any) => (s.id === socket.id)), 1);
      // console.log(this.users[uid].length);
    }
  }

  public async sendMessage(message: any) {
    const groupUserIds = await this.groupService.getUsers(message.to);
    const group: any = await this.groupService.getGroup(message.to);
    const orgUserIds = this.orgSockets[group.org_id];


    const userIds = _.union(groupUserIds,orgUserIds);

    console.log("sending message to convo");
    console.dir(userIds);
    console.dir(message);

    for (const uId of userIds) {
      console.log("user = %s", uId);
      const sockets = this.users[uId];
      if (sockets) {
        for (const s of sockets) {
          console.log("sending message");
          s.emit("message", message);
        }
      }
    }
  }

  public async receiveMessage(message: any) {
    await this.messageDb.addGroupMessage(message.to, message.from, message.text);
    this.emailsService.sendEmailFromMessage(message);
    const messageObj = {
      from: message.from,
      lines: [message.text],
      preview: message.text,
      text: message.text,
      to: message.to,
    };
    console.log(messageObj);
    // socket.to(message.to).emit('message', messageObj);
    // socket.emit('message', messageObj);
    this.sendMessage(messageObj);
  }

  @POST
  @Path("/receiveEmail")
  public async receiveEmail(email: any) {
    const groupId = this.getGroupIDFromEmail(email.to[0].address);
    const userRes = await this.messageDb.db.query("SELECT name,user_id,org_id from users where email = $1", [email.from[0].address]);
    const userId = userRes.rows[0].user_id;
    // upload files to s3 and add to db
    const fileIds = await Promise.all(email.attachments.map((val: any) => {
      return this.fileService.uploadFile(val.fileName, path.join(__dirname, "..", "..", "/Uploads", val.fileName), userId);
    }));
    // associate each attachment file to proper case
    fileIds.forEach((file: any) => {
      this.fileService.addFileToCase(file,groupId);
    });
    const message = {
      files: fileIds,
      from: userId,
      text: email.text,
      to: groupId,
    };
    return this.receiveMessage(message);

  }

  public async getConversationsForGroups(groupIds: string[]) {

    const result = await this.messageDb.db.query(
      "SELECT DISTINCT m.group_id FROM messages m WHERE group_id = ANY ($1)",
      [groupIds]);

    // convert from [{group_id: foo},{group_id:bar}] to [foo,bar]
    const convs = result.rows.map((o: any) => o.group_id);
    return convs ? convs : [];
  }

  public async getConversationReadCounts(userId: string, groupIds: string[]) {
    const messageRes = await this.messageDb.db.query(
      `SELECT group_id, count FROM conversations_view WHERE group_id = ANY ($1)`, [groupIds]);
    const readCountRes = await this.messageDb.db.query(
      `SELECT conversation_id, read_count FROM conversation_user_preferences
      WHERE user_id = $1 AND conversation_id = ANY ($2)`,
      [userId, groupIds]);

    // console.log(readCountRes);

    const readCounts: any = {};
    const convDetails: any = {};

    for (const obj of readCountRes.rows) {
      readCounts[obj.conversation_id] = obj.read_count;
    }

    for (const obj of messageRes.rows) {
      convDetails[obj.group_id] = {
        msg_count: parseInt(obj.count, 10),
        read_count: readCounts[obj.group_id] ? readCounts[obj.group_id] : 0,
      };
    }

    return convDetails;
  }


  public async getConversationHistory(convId: string) {

    const res = await this.messageDb.getMessages(convId);
    const messagesRaw = res.rows;
    const messages = [];
    console.log(messagesRaw);

    for (const message of messagesRaw) {
      console.log("send message to %s: %s", convId, message.user_id);
      const patt1 = /\n/;
      const lines = message.body.split(patt1);
      const messageObj = {
        from: message.user_id,
        lines,
        preview: lines[0],
        text: message.body,
        to: message.group_id,
      };

      messages.push(messageObj);
    }

    return messages;
  }

  public async messageTemplates(orgId: string) {
    const templates = await this.messageDb.getMessageTemplates(orgId);

    const templateMap: any = {};
    for (const template of templates) {
      // this is for the default templates that still have [org] in the message
      template.message_template = template.message_template.replace("[org]", orgId);

      templateMap[template.name] = {
        message: template.message_template,
        subject: template.subject_template,
      };
    }

    return templateMap;
  }

  public getGroupIDFromEmail(email: string) {
    const domain = email.indexOf("@");
    return email.slice(0, domain);
  }

  public async getConversationMembers(convId: string) {
    const userIds = await this.groupService.getUsers(convId);
    return userIds;
  }

  public async updateConversationPreferences(convId: string, userId: string, pref: any) {
    const update = await this.messageDb.updateConversationPreferences(convId, userId, pref);
    return update;
  }

  // infastructure in place for when we want to save message templates
  @GET
  @Path("templates")
  public async templates(@Param("orgId") orgId: string): Promise<MessageTemplate[]> {
    const templates = await this.messageDb.getMessageTemplates(orgId);
    return templates.map((t: any) => ({
      messageTemplate: t.message_template,
      messageTemplateId: t.message_template_id,
      name: t.name,
      orgId: t.org_id,
      subjectTemplate: t.subject_template,
    }));
  }

  // infastructure in place for when we want to save message templates
  @POST
  @Path("saveTepmlate")
  public async saveTemplate(template: MessageTemplate) {

    const row: any = {
      message_template: template.messageTemplate,
      message_template_id: uuid(),
      name: template.name,
      org_id: template.orgId,
      subject_template: template.subjectTemplate,
    };

    await this.messageDb.saveTemplates(row);

    return template;
  }



  public async getAncestorGroupsForUser(userId: any) {

    // temporary
    // console.log('getting groups for %s',user_id);

    const results = await this.messageDb.db.query(`
      WITH RECURSIVE user_groups AS (
        SELECT group_id, child_group_id
        FROM group_group_members
        WHERE child_group_id = $1
        UNION
        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.child_group_id = ug.group_id
      )
      SELECT distinct ug.group_id
      FROM user_groups ug
      INNER JOIN messages m ON m.group_id = ug.group_id`
      , [userId]);

    // console.dir(results.rows);

    // convert from [{group_id: foo},group_id:bar] to [foo,bar]
    const groupIds = results.rows.map((o) => o.group_id);
    return groupIds ? groupIds : [];
  }

  /**
   * Gets the groupIds that are associated with cases only and return them as a map { caseID : caseID }
   * @param {string} userId
   */
  public async getAncestorCaseGroupsForUser(userId: string) {

    const results = await this.messageDb.db.query(`
      WITH RECURSIVE user_groups AS (
        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN cases c on g.group_id = c.case_id
        WHERE child_group_id = $1

        UNION

        SELECT g.group_id, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.child_group_id = ug.group_id
      )
      SELECT distinct ug.group_id
      FROM user_groups ug
      INNER JOIN messages m ON m.group_id = ug.group_id;`, [userId] );

    // convert from [{group_id: foo}, {group_id:bar}] to [foo,bar]
    const groupIds = results.rows.map((o) => o.group_id);
    const caseGroupMap = groupIds.reduce((group, val) => { group[val] = val; return group; }, {});

    return caseGroupMap ? caseGroupMap : {};
  }

  public async getGroups(groupIds: any) {

    const result = await this.messageDb.db.query("SELECT * FROM groups WHERE group_id = ANY ($1)", [groupIds]);
    return result.rows;
  }



}
