import { Client } from "pg";
import { DbClientSingleton } from "../utils/DbClientSingleton";
import { Inject } from "typescript-ioc";

export class MessageDb {

  public db: Client;
  @Inject private dbClient: DbClientSingleton;

  constructor(db: any) {
    this.db = this.dbClient.db;
  }

  public async addGroupMessage(groupId: string, userId: string, text: string) {
    console.log("adding group messages");
    console.dir([groupId, userId, text]);
    return await this.db.query(
      "INSERT INTO messages (group_id, user_id, body) VALUES ($1, $2, $3)",
      [groupId, userId, text]);
  }

  public async getMessages(groupId: string) {
    const messages = await this.db.query(
      "SELECT * FROM messages WHERE group_id = $1 ORDER BY received_time ASC",
      [groupId]);
    // console.log(messages)
    return messages;
  }


  public async updateConversationPreferences(convId: string, userId: string, pref: any) {
    // getting the key in pref
    const key: string = Object.keys(pref)[0];
    // getting the value of key in pref;
    const value = pref[key];

    // lazy insert to table if not exists already
    let sql = `INSERT INTO conversation_user_preferences
                  (conversation_id, user_id)
               SELECT $1, $2
               WHERE
                  NOT EXISTS (
                      SELECT conversation_id, user_id
                      FROM conversation_user_preferences
                      WHERE conversation_id=$3 AND user_id=$4
                  )`;

    // I did this because for some reason it gets mad if I don't
    await this.db.query(sql, [convId, userId, convId, userId]);

    sql = `UPDATE conversation_user_preferences SET ` + key + ` = $3 WHERE conversation_id=$1 AND user_id=$2`;

    const update = await this.db.query(sql, [convId, userId, value]);
    return update;
  }

  /**
   * Returns a key, value map where key is the type of case and value is the template for the email
   * corresponding to that type of case
   * @param {string} orgId
   */
  public async getMessageTemplates(orgId: string) {
    console.log("MESSAGE DB GET TEMPLATES");
    const query = await this.db.query(
      `SELECT * FROM message_templates WHERE org_id=$1`, [orgId]);
    return query.rows;
  }

  public async saveTemplates(row: any) {
    const upsert = await this.db.query(`
      INSERT INTO message_templates
        (message_template_id, org_id, name, subject_template, message_template)
      VALUES ($1, $2, $3, $4, $5)
      ON CONFLICT ON CONSTRAINT message_template_const_org_name DO UPDATE
      SET message_template = EXCLUDED.message_template`,
      [row.message_template_id, row.org_id, row.name, row.subject_template, row.message_template] );

    return upsert;
  }

}
