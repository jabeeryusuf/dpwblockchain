export const generateTemplateSql = (orgId: string) => {
  return(
    `CREATE TEMPORARY TABLE temporary_table AS SELECT * FROM inventory_items WHERE org_id ='template';

     UPDATE temporary_table SET org_id= '` + orgId + `';

    INSERT INTO inventory_items SELECT * FROM temporary_table;

    CREATE TEMPORARY TABLE temporary_table1 AS SELECT * FROM purchase_orders WHERE org_id ='template';

    UPDATE temporary_table1 SET org_id= '` + orgId + `';

    INSERT INTO purchase_orders SELECT * FROM temporary_table1;

    CREATE TEMPORARY TABLE temporary_table2 AS SELECT * FROM purchase_order_items WHERE org_id ='template';

    UPDATE temporary_table2 SET org_id= '` + orgId + `';

    INSERT INTO purchase_order_items SELECT * FROM temporary_table2;

    CREATE TEMPORARY TABLE temporary_table3 AS SELECT * FROM shipment_items WHERE org_id ='template';

     UPDATE temporary_table3 SET org_id= '` + orgId + `';

    INSERT INTO shipment_items SELECT * FROM temporary_table3;

    CREATE TEMPORARY TABLE temporary_table4 AS SELECT * FROM shipments WHERE org_id ='template';

     UPDATE temporary_table4 SET org_id= '` + orgId + `', etd = CURRENT_DATE - INTERVAL '8 day',atd = CURRENT_DATE - INTERVAL '6 day',eta = CURRENT_DATE + INTERVAL '3 day' ;

    INSERT INTO shipments SELECT * FROM temporary_table4;

    CREATE TEMPORARY TABLE temporary_table5 AS SELECT * FROM cases WHERE org_id ='template';

     UPDATE temporary_table5 SET org_id= '` + orgId + `', case_id = gen_random_uuid();

    INSERT INTO cases SELECT * FROM temporary_table5;

    CREATE TEMPORARY TABLE temporary_table6 AS SELECT * FROM groups WHERE org_id ='template';

    UPDATE temporary_table6 SET org_id= '` + orgId + `', group_id = gen_random_uuid();

    INSERT INTO groups SELECT * FROM temporary_table6;

    CREATE TEMPORARY TABLE temporary_table7 AS SELECT * FROM invitations WHERE org_id ='template';

     UPDATE temporary_table7 SET org_id= '` + orgId + `';

    INSERT INTO invitations SELECT * FROM temporary_table7;
    `
  );
};
