"use-strict";

import { Client } from "pg";
import { IOrgsService, Org, Setting } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { GET, Param, Path, POST } from "typescript-rest";
import { generateTemplateSql } from "../generateTemplateSql.js";
import { DbClientSingleton } from "../utils";

@Path("v0.1/orgs")
export class OrgsService implements IOrgsService {

  @Inject private dbClient: DbClientSingleton;
  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  public async generateTemplateOrgInfo(orgId: string) {
    const sqlStatement = generateTemplateSql(orgId);
    return await this.db.query(sqlStatement);
  }

  @Path("/list")
  @GET
  public async list(): Promise<Org[]> {
    const results = await this.db.query(`SELECT * FROM orgs`);

    return results.rowCount > 0 ? results.rows.map(this.dbRowToOrg) : null;
  }


  @Path("/info")
  @GET
  public async info(@Param("orgId") orgId: string): Promise<Org> {
    const results = await this.db.query(`SELECT * FROM orgs WHERE org_id = $1`, [orgId]);

    return results.rowCount > 0 ? this.dbRowToOrg(results.rows[0]) : null;
  }

  @Path("/create")
  @POST
  public async create(org: Org): Promise<Org> {
    try {
      const results = await this.db.query(`INSERT INTO orgs (org_id, name, type) VALUES ($1, $2, $3)`,
        [org.orgId, org.name, org.type]);
      if (results.rowCount === 0) {
        return null;
      }
      //await this.generateTemplateOrgInfo(org.orgId);
      else
      {
        await this.db.query(`INSERT INTO org_settings (org_id, key, value) VALUES ($1, $2, $3)`,
        [org.orgId, 'logo', 'dpw-logow.png']);
        return await this.info(org.orgId);
      }
      
    }

    catch (e) {
      console.log("is orgpk status");
      console.log(e.constraint);
      throw e;
      //return e;
    }

  }

  @Path("/getSetting")
  @GET
  public async getSetting(@Param("orgId") orgId: string, @Param("key") key: string): Promise<Setting> {
    const query = await this.db.query(`SELECT value FROM org_settings WHERE org_id=$1 AND key=$2`, [orgId, key]);

    const setting = new Setting();
    setting.orgId = orgId;
    setting.key = key;
    setting.value = query.rowCount > 0 ? query.rows[0].value : null;

    return setting;
  }

  @Path("/updateSetting")
  @POST
  public async updateSetting(orgSettingObj: Setting): Promise<any> {
    const { orgId, key, value } = orgSettingObj;
    const results = await this.db.query(`INSERT INTO org_settings (org_id, key, value)
    VALUES ($1, $2, $3)
    ON CONFLICT ON CONSTRAINT org_settings_pkey
    DO UPDATE
    SET value = EXCLUDED.value`, [orgId, key, value]);

    if (results.rowCount === 0) {
      return null;
    }
    return await this.getSetting(orgId, key);
  }

  private dbRowToOrg(row: any) {
    return new Org(row.org_id, row.name, row.type);
  }

}
