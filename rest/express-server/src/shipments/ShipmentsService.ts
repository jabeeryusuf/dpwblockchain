import * as moment from "moment";
import {
  Client
} from "pg";
import {
  IDatabase
} from "pg-promise";
import {
  IShipmentsService,
  ResponseStatus,
  Shipment,
  ShipmentException,
  ShipmentItem,
  ShipmentSearchMin,
  ShipmentsResponse,
  Tracking
} from "tw-api-common";
import {
  Inject
} from "typescript-ioc";
import {
  GET,
  Param,
  Path,
  POST,
  PUT
} from "typescript-rest";
import {
  v4 as uuidv4
} from "uuid";
import {
  DbClientSingleton
} from "../utils";
import {
  ShipmentsDb
} from "./ShipmentsDb";
import {
  ContainersDb
} from "../containers/ContainersDb";

import {
  ShipmentSearch
} from "./ShipmentSearch";
import {
  TrackingsService
} from "./TrackingsService";


@Path("/v0.1/shipment")
export class ShipmentsService implements IShipmentsService {

  @Inject private dbClient: DbClientSingleton;
  @Inject private shipmentsDb: ShipmentsDb;
  @Inject private containersDb: ContainersDb;

  @Inject private trackingsService: TrackingsService;

  private db: Client;
  private dbp: IDatabase<any>;

  constructor() {
    this.db = this.dbClient.db;
    this.dbp = this.dbClient.dbp;
  }

  @POST
  @Path("/create")
  public async create(shipment: Shipment, @Param("fileId") fileId?: string): Promise<ShipmentsResponse> {
    console.log("creating shipment!!!!, ", shipment.shipmentRef);

    // Create the shipment record
    shipment.shipmentId = uuidv4();
    const sRow = this.shipmentToDb(shipment);
    await this.shipmentsDb.insertShipment(sRow);

    // Add the tracking information
    await this.trackingsService.updateShipment(shipment.shipmentId, shipment.trackings);
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>done with trackings start items");
    // Add the shipment items
    await this.updateShipmentItems([], shipment.items, shipment.shipmentId, shipment.orgId);
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>done with items return");
    // Return success
    return new ShipmentsResponse({
      status: ResponseStatus.OK,
      shipment
    });
  }

  @POST
  @Path("/newshipment")
  public async newshipment(shipments: any): Promise<any> {
    console.log("creating shipment!!!!##############, ");

    console.log(shipments);
    // Create the shipment record

    var results=[];
      var i = 0;
      for (i = 0; i < shipments.length; i++) {

        var carrier_upd = shipments[i].carrier;
        if(carrier_upd=="auto" || carrier_upd=="")
        {
          //console.log("Auto setting carrier");
          carrier_upd = await this.shipmentsDb.getcarrier(shipments[i].shipmentRef);

          if(carrier_upd==null) carrier_upd="";
          console.log(carrier_upd);
        }
        var shipment = {
          shipment_id: uuidv4(), 
          org_id: shipments[i].orgId,
          shipment_ref: shipments[i].shipmentRef,
          shipment_status: shipments[i].status,
          carrier: carrier_upd,
          source_container_id:" ",
          destination_container_id:" "
        };

        try {
        await this.shipmentsDb.insertShipment(shipment);
        var containerinfo = { container_id: "shipping-container&" + shipment.shipment_ref, type: "shipping-container", ref: shipment.shipment_ref }
        await this.containersDb.addContainer(containerinfo);
        await this.shipmentsDb.addShipmentItemDefault(shipment);
        var status = {cnumber:shipment.shipment_ref,status:"SUCCESS",carrier:carrier_upd}
        results.push(status);
        }
        catch (e) {

          console.log(e);

          if(e.constraint && e.constraint=="pk_example1")
          var status = {cnumber:shipment.shipment_ref,status:"Already Exist",carrier:carrier_upd}
          else
          var status = {cnumber:shipment.shipment_ref,status:"Error",carrier:carrier_upd}
          results.push(status);
        }
      }
      // shipment.shipmentId = uuidv4();
      // const sRow = this.newminshipmentToDb(shipment);
      // await this.shipmentsDb.insertShipment(sRow);
    // Add the tracking information
    //await this.trackingsService.updateShipment(shipment.shipmentId, shipment.trackings);
    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>done with trackings start items");
    // Add the shipment items
    //await this.updateShipmentItems([], shipment.items, shipment.shipmentId, shipment.orgId);
    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>done with items return");
    // Return success
    return {
      status: ResponseStatus.OK,
      results
    };
  }


  @PUT
  @Path("/update")
  public async update(shipment: Shipment, @Param("fileId") fileId?: string): Promise<ShipmentsResponse> {

    console.log("updating shipment!!!!!", shipment.shipmentRef);
    // console.log(shipment)

    const info = await this.info(shipment.shipmentId);
    if (info.status === ResponseStatus.NO_RESULTS) {
      return info;
    }

    // Update the main shipment record
    const sRow = this.shipmentToDb(shipment);
    await this.shipmentsDb.updateShipmentExist(sRow);

    // Add the tracking information
    await this.trackingsService.updateShipment(shipment.shipmentId, shipment.trackings);

    // Add the shipment items
    await this.updateShipmentItems(info.shipment.items, shipment.items, shipment.shipmentId, shipment.orgId);

    // Return success
    return new ShipmentsResponse({
      status: ResponseStatus.OK,
      shipment
    });
  }


  @POST
  @Path("/updatecarrier")
  public async updatecarrier(obj:any): Promise<any> {

    console.log("updating carrier for shipment!!!!!", obj.shipmentId);
    // console.log(shipment)

    await this.shipmentsDb.updateShipmentCarrier(obj.carrier,obj.shipmentId);
    return ({
      status: ResponseStatus.OK,
      updated:true
    });
  }


  @GET
  @Path("/info")
  public async info(@Param("shipmentId") shipmentId: string): Promise<ShipmentsResponse> {

    console.log("in info");
    const sRow = await this.dbp.oneOrNone("SELECT * FROM shipments WHERE shipment_id = $1", shipmentId);
    if (!sRow) {
      return new ShipmentsResponse({
        status: ResponseStatus.NO_RESULTS
      });
    }

    const siRows = await this.dbp.any(`SELECT * FROM shipment_items WHERE shipment_id = $1`, shipmentId);

    // Fix tracking entry
    const shipment = this.dbToShipment(sRow, [], siRows);

    // Add tracking
    const tRes = await this.trackingsService.list({
      shipmentId
    });

    if (tRes.status === ResponseStatus.OK) {
      shipment.trackings = tRes.trackings;
    }

    console.log("Tracking satttusses in shipments is");
    console.log(shipment.trackings);

    return new ShipmentsResponse({
      status: ResponseStatus.OK,
      shipment
    });
  }

  @GET
  @Path("/search")
  public async search(@Param("cursor") cursor: string = "",
    @Param("orgId") orgId: string = "anko",
    @Param("limit") limit: number = 10,
    @Param("filterParams") filterParams: any,
    @Param("searchString") searchString: string,
    @Param("desc") desc: boolean,
    @Param("orderBy") orderBy: string,
    @Param("groupBy") groupBy: string,
  ): Promise<ShipmentsResponse> {
    const cursorNum = cursor ? Number(cursor) : 0;
    const shipmentSearch = new ShipmentSearch(
      this.dbp,
      orgId,
      limit,
      cursor,
      filterParams,
      searchString,
      orderBy,
      groupBy,
      desc);
    const dbRows = await shipmentSearch.search();
    const pageCount = await shipmentSearch.getFullCount();
    const nextCursor = String(cursorNum + limit);
    const shipmentList = this.createShipmentListFromDB({
      items: dbRows
    });
    const shipResp = new ShipmentsResponse({
      metadata: {
        nextCursor,
        pageCount
      },
      shipments: shipmentList,
      status: ResponseStatus.OK,
    });
    return shipResp;
  }





  // Working through legacy


  @GET
  @Path("/getDestinations")
  public async getDestinations(@Param("orgId") orgId?: string, @Param("shipmentStatus") shipmentStatus?: string) {
    return await this.shipmentsDb.getDestinations(orgId, shipmentStatus);
  }

  @GET
  @Path("/searchSuggest")
  public async searchSuggest(@Param("orgId") orgId?: string, @Param("searchval") searchval?: Promise<ShipmentSearchMin[]>) {

    //console.log("Search Value is");
    //console.log(searchval);
    const sql = `select shipment_id::varchar(50) as id,shipment_ref as name, 'shipments' as searchtype from shipments  where org_id = '` + orgId + `' and (vessel_name ILIKE  '%` + searchval + `%' or shipment_ref ILIKE  '%` + searchval + `%')
    UNION
    select purchase_order_id as id, purchase_order_id as name, 'purchaseorders' as searchtype from purchase_orders where org_id = '` + orgId + `' and purchase_order_id like '%` + searchval + `%'
    UNION
    select si.sku as id, p.name as name, 'items' as type from shipment_items si left join products p on p.sku =si.sku  where si.org_id = '` + orgId + `' and (si.sku like '%` + searchval + `%' or p.name like '%` + searchval + `%')
    `;
    const result = await this.db.query(sql);
    console.log("Search SUGGEST RESULT IS");
    console.log(result);
    return this.createMinShipmentListFromDB(result);
    //return result.rows;
  }

  @GET
  @Path("/query")
  public async queryShipments(@Param("whereClause") whereClause: any,
    @Param("whereParams") whereParams: any,
    @Param("groupBy") groupBy: any,
    @Param("orderBy") orderBy: any): Promise<Shipment[]> {

    const shipDBInfo = await this.shipmentsDb.queryShipment(whereClause, whereParams, groupBy, orderBy);
    return this.createShipmentListFromDB(shipDBInfo);
  }

  public createMinShipmentListFromDB(result: any) {
    const shipments: ShipmentSearchMin[] = [];
    //console.log("Inside createMinShipmentListFromDB");
    for (const row of result.rows) {
      //console.log("Inside createMinShipmentListFromDB LOOP");
      const s = this.dbToMinShipment(row);
      shipments.push(s);
    }
    return shipments;
  }

  public createShipmentListFromDB(shipDBInfo: any) {
    const siMap: any = {};
    const tMap: any = {};
    const stMap: any = {};
    for (const row of shipDBInfo.items) {
      const sid = row.shipment_id;
      // Array of items per shipment
      siMap[sid] = siMap[sid] ? [...siMap[sid], row] : [row];

      // Map of tracking id to a row
      if (row.tracking_id && !tMap[row.tracking_id]) {
        tMap[row.tracking_id] = row;
      }
    }
    // invert the tracking map
    for (const t of Object.keys(tMap)) {
      const sid = tMap[t].shipment_id;

      stMap[sid] = stMap[sid] ? [...stMap[sid], tMap[t]] : [tMap[t]];
    }
    const shipments: Shipment[] = [];

    for (const sid of Object.keys(siMap)) {
      //console.log(sid);
      const s = this.dbToShipment(siMap[sid][0], stMap[sid] ? stMap[sid] : [], siMap[sid]);
      shipments.push(s);
    }
    return shipments;
  }

  // for the detail location page
  @GET
  @Path("/shipmentIdQuery")
  public async queryShipmentID(@Param("whereClause") whereClause: any, @Param("whereParams") whereParams: any,
    @Param("type") type: string) {
    let sqlEnd = "";
    let iiSql;
    let iiRes;
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }
    if (type === "port") {
      iiSql = `select *, (i.delivery_quantity/p.pack_size) as total_carton  from shipment_items as i ,(select * from shipments) as c ,(select * from products )as p   ${sqlEnd}`;
      iiRes = await this.db.query(iiSql, whereParams);
    }
    else if (type === "ship") {
      //iiSql = `select *, (i.delivery_quantity/p.pack_size) as total_carton  from shipment_items as i ,(select * from shipments) as c ,(select * from products )as p , (select vessel_name,vessel_container_id,timestamp from vehicle_locations) as vl  ${sqlEnd}`;
      iiSql = `
      select si.*, s.*, p.*, (si.delivery_quantity/p.pack_size) as total_carton, c.type,c.ref,c.parent_container_id,c.container_id,s.shipment_id from shipment_items si
      left join shipments s on s.shipment_id = si.shipment_id
      LEFT JOIN  products p on p.sku = si.sku
      LEFT JOIN  containers c on c.ref = s.shipment_ref
      where  s.org_id = $2
      and c.parent_container_id = $1 or c.parent_container_id = vesselnamebyid($1)
      `;
      console.log("iisql for ship detail is#########");
      console.log(iiSql);
      console.log(whereParams);
      iiRes = await this.db.query(iiSql, whereParams);
    }
    else if (type === "warehouse") {
      iiSql = `select *,(i.quantity / i.qty_per_carton) as total_carton from inventory_items as i, (select * from products )as p ${sqlEnd}`;
      iiRes = await this.db.query(iiSql, whereParams);
    }
    else if (type === "factory") {
      iiSql = `SELECT p.*, (p.quantity - p.recv_quantity) as pending_quantity,pro.* from purchase_order_items as p,(select * from products )as pro ${sqlEnd}`;
      console.log("Factory Query Is######################");
      console.log(iiSql);

      console.log(whereParams);

      iiRes = await this.db.query(iiSql, whereParams);
    }
    else {
      throw new Error("undefined data from sql query");
    }
    const cache: any = {};
    const res = {
      items: iiRes.rows,
      cache
    };

    console.log("Location date isIs######################");
    console.log(res);
    return res;

  }

  @GET
  @Path("/queryShipHistory")
  public async queryShipHistory(@Param("shipmentId") shipmentId: string) {
    return await this.shipmentsDb.queryShipHistory(shipmentId);
  }

  @GET
  @Path("/queryCurrentShipments")
  public async queryCurrentShipments(@Param("orgId") orgId: string) {
    return await this.shipmentsDb.queryCurrentShipments(orgId);
  }

  @GET
  @Path("/queryPastShipments")
  public async queryPastShipments(@Param("orgId") orgId: string) {
    return await this.shipmentsDb.queryPastShipments(orgId);
  }

  @GET
  @Path("/queryLateUploadShipments")
  public async queryLateUploadShipments(@Param("orgId") orgId: string) {
    return await this.shipmentsDb.queryLateUploadShipments(orgId);
  }

  @GET
  @Path("/queryPOsOnShipment")
  public async queryPOsOnShipment(@Param("orgId") orgId: string, @Param("shipmentId") shipmentId: string) {
    const results = await this.shipmentsDb.queryPOsOnShipment(orgId, shipmentId);

    // console.log("queryPO");
    // console.log(results);
    // console.log("-queryPO");

    return results;
  }

  @GET
  @Path("/queryShipmentTrackingExist")
  public async queryShipmentTrackingExist(@Param("trackingId") trackingId: any) {
    // console.log("trackingId", trackingId);
    return await this.shipmentsDb.queryShipmentTrackingExist(trackingId);
  }
  @GET
  @Path("/getContainersForShipment")
  public async getContainersForShipment(@Param("shipmentId") shipmentId: string) {
    return await this.shipmentsDb.getContainersForShipment(shipmentId);
  }

  @GET
  @Path("/updateFeedActivity")
  public async updateFeedActivity(@Param("status") status: string, @Param("fileId") fileId: string, @Param("errorFileId") errorFileId?: string) {
    return await this.shipmentsDb.updateFeedActivity(status, fileId, errorFileId);
  }

  @GET
  @Path("/addFeedsActivity")
  public async addFeedsActivity(@Param("status") status: string, @Param("fileId") fileId: string, @Param("orgId") orgId: string) {
    return await this.shipmentsDb.addFeedsActivity(status, fileId, orgId);
  }

  @GET
  @Path("/addFeedsErrorLog")
  public async addFeedsErrorLog(@Param("fileId") fileId: string, @Param("errorMessage") errorMessage: string) {
    return await this.shipmentsDb.addFeedsErrorLog(fileId, errorMessage);
  }

  @GET
  @Path("/selectErrorLog")
  public async selectErrorLog(@Param("fileId") fileId: string) {
    return await this.shipmentsDb.selectErrorLog(fileId);
  }

  public async addShipmentItem(shipItemOBJ: any) {
    return await this.shipmentsDb.addShipmentItem(shipItemOBJ);
  }

  public async queryShipmentRefExist(shipmentRef: string, orgId: string) {
    return await this.shipmentsDb.queryShipmentRefExist(shipmentRef, orgId);
  }

  public async addShipment(shipOBJ: any) {
    // This is for GMT timezone
    moment.tz.setDefault("Europe/London");
    shipOBJ.shipment_status = Math.abs(moment().diff(moment(shipOBJ.eta), "days")) > 5 ? "Late Upload" : "Pending";
    return await this.shipmentsDb.addShipment(shipOBJ);
  }


  @GET
  @Path("/getDestinationContainers")
  public async getDestinationContainers(@Param("orgId") orgId: any) {
    const sql = "SELECT DISTINCT destination_container_id, buildings.name FROM shipments JOIN buildings on destination_container_id=container_id where shipments.org_id = $1";
    const result = await this.db.query(sql, [orgId]);
    return result.rows;
  }



  @GET
  @Path("/queryItems")
  public async queryShipmentItems(@Param("whereClause") whereClause: any, @Param("whereParams") whereParams: any,
    @Param("groupBy") groupBy: string[], @Param("orderBy") orderBy: string[]) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }

    const iiSql = "SELECT * from shipment_items" + sqlEnd;
    const iiRes = await this.db.query(iiSql, whereParams);
    const rows = iiRes.rows;
    const res = {
      items: rows
    };
    return res;

  }


  @GET
  @Path("/getExceptions")
  public async getExceptions(@Param("orgId") orgId: any) {
    const exceptionsRes = await this.shipmentsDb.getExceptions(orgId);
    const mileStoneRes = await this.shipmentsDb.getTrackingEvents(orgId, true, true);
    const modifiedExceptions = this.addMostRecentEventToExceptions(exceptionsRes, mileStoneRes);
    const exceptions = modifiedExceptions.map((row: any) => {
      const currException = new ShipmentException();
      const currShipment = new Shipment();
      currShipment.destinationContainerId = row.destination_container_id;
      currShipment.sourceContainerId = row.source_container_id;
      currShipment.shipmentId = row.shipment_id;
      currException.shipment = currShipment;
      currException.status = row.status;
      currException.expectedTime = row.expected_date;
      currException.lastEvent = row.lastEvent;
      return (
        currException
      );
    });
    return {
      exceptions
    };
  }

  public prepareValuesToMatch(object: any) {
    // tslint:disable-next-line: forin
    for (const key in object) {
      // val hack to
      if (key == "shippingContainerId") {
        const newVal = "shipping-container&";
        object[key] = newVal + object[key];
      }
      object[key] += "%";
    }
    return object;
  }

  private dbToMinShipment(row: any) {
    //console.log("Inside dbToMinShipment");
    const s: ShipmentSearchMin = new ShipmentSearchMin();
    s.id = row.id;
    s.name = row.name;
    s.searchtype = row.searchtype;
    return s;
  }

  private dbToShipment(row: any, stRows: any[], siRows: any[]) {

    const s: Shipment = new Shipment();

    s.orgId = row.org_id;
    s.shipmentId = row.shipment_id;
    s.shipmentRef = row.shipment_ref;
    s.sourceContainerId = row.source_container_id;
    s.destinationContainerId = row.destination_container_id;
    s.transportContainerId = row.vessel_container_id;
    s.items = row.items;
    s.eta = row.eta;
    s.etd = row.etd;
    s.atd = row.atd;
    s.ata = row.ata;
    s.voyage = row.voyage;
    s.vesselName = row.vessel_name;
    s.bol = row.bol;
    s.gateout = row.gateout;
    s.expected_eta = row.expected_eta
    s.carrier = row.carrier;
    s.status = row.shipment_status;
    s.vesselName = row.vessel_name;

    s.trackings = [];
    for (const str of stRows) {
      const tracking = new Tracking();
      tracking.carrierId = str.carrier;
      tracking.type = str.type;
      tracking.ref = str.tracking_ref;
      tracking.status = str.status;
      s.trackings.push(tracking);
    }

    // Probably not correct
    s.updateTime = row.update_time;

    s.items = [];
    // Add the items
    for (const sir of siRows) {

      const si = new ShipmentItem();
      si.orgId = sir.org_id;
      si.shipmentId = sir.shipment_id;
      si.purchaseOrderId = sir.purchase_order_id;
      si.purchaseOrderItemId = sir.purchase_order_item_id;
      si.productId = sir.sku;
      si.productType = sir.product_type;
      si.shippingContainerId = sir.shipping_container_id;
      si.quantity = sir.delivery_quantity;
      si.deliveryDate = sir.delivery_time;
      si.allocationDate = sir.shipment_allocation_date;
      si.hbl = sir.house_bill_of_lading;
      si.lot = sir.production_lot;
      s.items.push(si);
    }

    return s;
  }

  // private newminshipmentToDb(s: any): any {
  //   const row: any = {};

  //   row.org_id = s.orgId;
  //   row.shipment_id = s.shipmentId;
  //   row.shipment_ref = s.shipmentRef;
  //   row.shipment_status = s.status;
  //   row.carrier = s.carrier;
  //   return row;
  // }

  private shipmentToDb(s: Shipment): any {
    const row: any = {};

    row.org_id = s.orgId;
    row.shipment_id = s.shipmentId;
    row.shipment_ref = s.shipmentRef;
    row.shipment_status = s.status;
    row.source_container_id = s.sourceContainerId;
    row.destination_container_id = s.destinationContainerId;
    row.vessel_container_id = s.transportContainerId;
    row.eta = s.eta;
    row.etd = s.etd;
    row.atd = s.atd;
    row.ata = s.ata;
    row.original_eta = s.originalEta;

    row.vessel_name = s.vesselName;
    row.bol = s.bol;

    return row;
  }

  private shipmentItemToDb(si: ShipmentItem, shipmentId: string, orgId?: string): any {
    const sir: any = {};

    sir.org_id = si.orgId ? si.orgId : orgId;
    sir.shipment_id = shipmentId;
    sir.purchase_order_id = si.purchaseOrderId;
    sir.purchase_order_item_id = si.purchaseOrderItemId;
    sir.sku = si.productId;
    sir.shipping_container_id = si.shippingContainerId;
    sir.delivery_quantity = si.quantity;
    sir.house_bill_of_lading = si.hbl ? si.hbl : null;
    sir.production_lot = si.lot ? si.lot : null;
    sir.delivery_time = si.deliveryDate;
    sir.shipment_allocation_date = si.allocationDate;
    sir.product_type = si.productType ? si.productType : null;

    return sir;
  }

  private smashSi(s: ShipmentItem): string {

    const hashArray = [
      s.orgId,
      s.purchaseOrderId,
      s.purchaseOrderItemId,
      s.productId,
      s.productType,
      s.shippingContainerId,
      s.deliveryDate ? s.deliveryDate.getTime() : null,
      s.allocationDate ? s.allocationDate.getTime() : null,
      s.quantity
    ];

    return hashArray.join("|");
  }

  private compareSi = (a: ShipmentItem, b: ShipmentItem): number => {
    return this.smashSi(a).localeCompare(this.smashSi(b));
  }

  private async updateShipmentItems(os: ShipmentItem[], ns: ShipmentItem[], shipmentId: string, orgId?: string) {

    const sOld = os.sort(this.compareSi);
    const sNew = ns.sort(this.compareSi);

    const identical = (sOld.length === sNew.length) && sOld.every((e, i) => (this.compareSi(e, sNew[i]) === 0));
    console.log("IDENTICAL = " + identical);
    if (identical) {
      return;
    }

    const iBatch = sNew.map((e, i) => this.shipmentItemToDb(e, shipmentId, orgId));

    this.shipmentsDb.deleteShipmentItem({
      shipment_id: shipmentId
    });
    this.shipmentsDb.insertShipmentItems(iBatch);
  }
  // function that takes in a list of exceptions and and compeleted shipment tracking events and combines them
  private addMostRecentEventToExceptions(exceptions: any, events: any) {
    const eventsHash: any = {};
    console.log(events);
    events.forEach((val: any) => {
      const eventObj = {
        date: val.event_date,
        location: val.location,
        status: val.status
      };
      if (!eventsHash[val.tracking_id]) {
        eventsHash[val.tracking_id] = eventObj;
      }
    });
    exceptions.map((val: any) => {
      const currentEvent = eventsHash[val.tracking_id];
      return Object.assign(val, {
        lastEvent: currentEvent
      });
    });
    return exceptions;
  }

}