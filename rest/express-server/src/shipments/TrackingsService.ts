import { Client } from "pg";
import { ColumnSet, IDatabase } from "pg-promise";
import {
    ITrackingsListOptions, ResponseStatus, ShipmentsResponse, Tracking, TrackingEvent, TrackingsResponse,
    TrackingStatus
} from "tw-api-common";
import { Inject } from "typescript-ioc";
import { DELETE, GET, Param, Path, POST, PUT } from "typescript-rest";
import { v4 as uuidv4 } from "uuid";
import { DbClientSingleton } from "../utils";

@Path("/v0.1/shipments/trackings")
export class TrackingsService {

    @Inject private dbClient: DbClientSingleton;

    private db: Client;
    private dbp: IDatabase<any>;

    private tCols: ColumnSet;
    private teCols: ColumnSet;
    private joinSelect: string;

    constructor() {
        this.db = this.dbClient.db;
        this.dbp = this.dbClient.dbp;

        this.tCols = new this.dbClient.pgp.helpers.ColumnSet([
            "tracking_id", "carrier", "type", "tracking_ref", "status", "shipment_id"
        ]);

        this.teCols = new this.dbClient.pgp.helpers.ColumnSet([
            "tracking_id", "date", "status", "location", "vessel", "voyage",

            //      "type", "carrier_details", "expected_date", "event_date"
        ]);

        this.joinSelect = `
      SELECT
      ${this.tCols.columns.map((c) => (`t.${c.name} as t_${c.name}`)).join(",")},
      ${this.teCols.columns.map((c) => (`te.${c.name} as te_${c.name}`)).join(",")}
      FROM shipment_trackings t
      LEFT JOIN shipment_tracking_events te ON t.tracking_id = te.tracking_id `;

    }

    @POST
    @Path("/create")
    public async create(st: Tracking): Promise<TrackingsResponse> {
        console.log("create trackingsss", st);
        const trackingId = uuidv4();

        await this.db.query(`
      INSERT INTO shipment_trackings (tracking_id, carrier, type, tracking_ref, status, shipment_id)
        VALUES ($1,$2,$3,$4,$5,$6)`,
            [trackingId, st.carrierId, st.type, st.ref, st.status, st.shipmentId]);

        st.trackingId = trackingId;

        return new TrackingsResponse({ status: ResponseStatus.OK, tracking: st });
    }

    @PUT
    @Path("/update")
    public async update(st: Tracking): Promise<TrackingsResponse> {

        const { status, trackingId, events } = st;
        /// minor optimization to avoid updating the destination container and source container multiple times in shipments table
        let destSet = false;
        let originSet = false;

        await this.dbp.none(`
      DELETE from shipment_tracking_events where tracking_id = $1`,
            [trackingId]);

        await this.dbp.none(`
      UPDATE shipment_trackings SET status = $1 WHERE tracking_id = $2`,
            [status, trackingId]);

        if (status !== TrackingStatus.TRACKING) {
            return;
        }

        const { shipment_id: shipmentId } = await this.dbp.one(`
      SELECT shipment_id FROM shipment_trackings WHERE tracking_id = $1`,
            [trackingId]);

        for (const event of events) {
            await this.db.query(`
        INSERT INTO shipment_tracking_events (tracking_id, date, status, location, vessel, voyage)
        VALUES ($1, $2, $3, $4,$5, $6)`,
                [trackingId, event.date, event.status, event.locationContainerId, event.transportContainerId, event.voyageId]);

            const status = event.status.toUpperCase();
            // temporarily closing on ata
            // we should add seperate mappings for closing event/arrival event in scrapers
            if (status === "ARRIVED" || status === "DISCHARGED") {
                console.log("update ata");
                if (!destSet && (event.locationContainerId && event.locationContainerId !== "NULL")) {
                    destSet = await this.addDestination(event.locationContainerId, shipmentId);
                }
                await this.dbp.none(`UPDATE shipments SET ata = $1, eta = $1, shipment_status = 'Closed'  where ata is null and shipment_id = $2`,
                    [event.date, shipmentId]);
            }
            if (status === "ETA" || status === "ESTIMATED ARRIVAL" || status === "ESTIMATED GATE OUT") {
                console.log("eta update");
                if (!destSet && (event.locationContainerId && event.locationContainerId !== "NULL")) {
                    destSet = await this.addDestination(event.locationContainerId, shipmentId);
                }
                await this.dbp.none(`
          UPDATE shipments SET eta = $1 WHERE shipment_id = $2`,
                    [event.date, shipmentId]);
            }
            if (status.slice(0, 3) === "ATD" || status === "VESSEL DEPARTED" || status === "GATE OUT EMPTY" || status === "LOADED") {
                console.log("atd update");
                if (!originSet) {
                    originSet = await this.addPortOfLoading(event.locationContainerId, shipmentId);
                }
                await this.dbp.none(`
          UPDATE shipments SET atd= $1 where atd is null and shipment_id = $2`,
                    [event.date, shipmentId]);
            }
            if (status === "ESTIMATED DEPARTURE") {
                console.log("etd update");
                if (!originSet) {
                    originSet = await this.addPortOfLoading(event.locationContainerId, shipmentId);
                }
                await this.dbp.none(`
          UPDATE shipments SET etd = $1 WHERE shipment_id = $2`,
                    [event.date, shipmentId]);
            }
        }

        this.updateVessel(st, shipmentId);

        console.log(st);
        console.log("done!!!");
        return;
    }

    public async updateShipment(shipmentId: string, trackings: Tracking[]) {

        // carrierId: string, type: TrackingType, ref: string) {
        console.log("validating shipment tracking: " + shipmentId);
        const res = await this.list({ shipmentId });

        if (res.status === ResponseStatus.OK && this.trackingsEqual(res.trackings, trackings)) {
            // events are already present and matching
            console.log("trackings match: returning");
            return;
        }

        // Delete tracking IDs
        await this.nullOutShipmentTracking(shipmentId);

        // Add new trackings
        for (const t of trackings) {
            t.shipmentId = shipmentId;
            await this.create(t);
        }
        console.log("done with new shipmnet trackingss>>>>>>>>>>>>>>>>>>>>>>");
    }

    @DELETE
    @Path("/delete")
    public async delete(@Param("trackingId") trackingId: string): Promise<ShipmentsResponse> {
        await this.db.query("DELETE FROM shipment_trackings where tracking_id=$1", [trackingId]);
        return new ShipmentsResponse({ status: ResponseStatus.OK });
    }

    @GET
    @Path("/info")
    public async info(@Param("trackingId") trackingId: string): Promise<TrackingsResponse> {

        const stRows = await this.dbp.any(
            this.joinSelect + " WHERE t.tracking_id = $1",
            [trackingId]);

        if (stRows.length < 1) {
            return new TrackingsResponse({ status: ResponseStatus.NO_RESULTS });
        }

        const trackings = this.dbJoinToTrackings(stRows);

 

        return new TrackingsResponse({ status: ResponseStatus.OK, tracking: trackings[0] });
    }

    @GET
    @Path("/list")
    public async list(@Param("options") options: ITrackingsListOptions)
        : Promise<TrackingsResponse> {
        console.log("optionsss>>>>>>>>>>>>>>>>>>>>>>", options)
        const { orgId, shipmentId, shipmentRef, carrierId, status } = { ...options };

        const shipmentClauses = [
            ...orgId ? ["org_id = $<orgId>"] : [],
            ...shipmentRef ? ["shipment_ref = $<shipmentRef>"] : [],
            ...shipmentId ? ["shipment_id = $<shipmentId>"] : [],
        ];

        console.log(shipmentClauses);

        const shipmentSelect = shipmentClauses.length > 0 ? "SELECT shipment_id FROM shipments WHERE " +
            shipmentClauses.join(" AND ") : undefined;

        const trackingClauses = [
            ...carrierId ? ["t.carrier = $<carrierId>"] : [],
            ...shipmentSelect ? ["t.shipment_id IN (" + shipmentSelect + ")"] : [],
            ...status ? ["t.status = $<status>"] : [],
        ];

        if (trackingClauses.length < 1) {
            return new TrackingsResponse({ status: ResponseStatus.RESULTS_TOO_LARGE });
        }



        var sql = this.joinSelect + (trackingClauses.length > 0 ? "WHERE " + trackingClauses.join(" AND ") : "");
        sql += "order by date desc";
        console.log(sql);


        const stRows = await this.dbp.any(sql, options);

        //console.log("Trackings stRows before ");
        //console.log(stRows);

        if (stRows.length < 1) {
            return new TrackingsResponse({ status: ResponseStatus.NO_RESULTS });
        }

        console.log(stRows.length);

        const trackings = this.dbJoinToTrackings(stRows);


        console.log("Trackinga after process########## is");
        //console.log("Trackings trackings after ");
        


        return new TrackingsResponse({ status: ResponseStatus.OK, trackings });
    }



    // Private

    private trackingCompare(t1: Tracking, t2: Tracking) {
        const s1 = [t1.carrierId, t1.type, t1.ref].join("|");
        const s2 = [t2.carrierId, t2.type, t2.ref].join("|");

        console.log("trackingCompare ");
        console.log(s1);
        console.log(s2);
        return s1.localeCompare(s2);
    }

    private trackingsEqual(t1: Tracking[], t2: Tracking[]) {

        if (t1.length !== t2.length) {
            return false;
        }

        t1.sort(this.trackingCompare);
        t2.sort(this.trackingCompare);

        for (let i = 0; i < t1.length; i++) {
            if (this.trackingCompare(t1[i], t2[i]) !== 0) {
                return false;
            }
        }

        return true;
    }

    // Function assumes joined rows of shipment tracking and tracking events
    private dbJoinToTrackings(rows: any[]): Tracking[] {

        const tMap: { [id: string]: Tracking } = {};

        for (const r of rows) {

            if (!tMap[r.t_tracking_id]) {

               
                tMap[r.t_tracking_id] = new Tracking({
                    carrierId: r.t_carrier,
                    events: [],
                    ref: r.t_tracking_ref,
                    shipmentId: r.t_shipment_id,
                    status: r.t_status,
                    trackingId: r.t_tracking_id,
                    type: r.t_type,
                });
            }

            if (r.te_tracking_id) {
                console.log("Vessele info:"+r.te_vessel)
                tMap[r.te_tracking_id].events.push(new TrackingEvent({
                    date: r.te_date,
                    locationContainerId: r.te_location,
                    status: r.te_status,
                    transportContainerId: r.te_vessel,
                    voyageId: r.te_voyage
                },
                ));
            }

        }

        const trackings = Object.entries(tMap).map((t) => (t[1]));
        console.log("trackings count = " + trackings.length);

        return trackings;
    }


    private async addDestination(location: string, shipmentId: string) {
        console.log("add destination");
        await this.dbp.none(`
      UPDATE shipments SET destination_container_id = $1 WHERE shipment_id = $2`,
            [location, shipmentId]);
        console.log("add destination");
        return true;
    }

    private async addPortOfLoading(location: string, shipmentId: string) {
        await this.db.query("Update shipments set source_container_id = $1 where shipment_id = $2",
            [location, shipmentId]);
        return true;
    }

    private async updateVessel(tracking: Tracking, shipmentId: string) {
        const { events } = tracking;

        // sort events by date asc
        events.sort((a: any, b: any) => (a.date > b.date) ? 1 : (b.date > a.date) ? -1 : 0);

        let transportContainerId = null;
        let vesselName = null;

        // find the most recent instance of vessel mmsi
        for (let i = events.length - 1; i >= 0; i--) {
            if (events[i].transportContainerId !== null) {
                transportContainerId = events[i].transportContainerId;
                break;
            }
        }

        if (transportContainerId) {

            // get vessel name from scrape maps
            const nameQuery = await this.dbp.query(
                `SELECT name FROM vessels WHERE mmsi = $1 limit 1`,
                [transportContainerId]);

            if (nameQuery.length) {
                vesselName = nameQuery[0].name;
                console.log(`Updateing Vessel Name ${vesselName}`);

                await this.dbp.none(`UPDATE shipments SET vessel_container_id = $1, vessel_name = $2 WHERE shipment_id = $3`,
                    [transportContainerId, vesselName, shipmentId]);
            } else {
                await this.dbp.none(`UPDATE shipments SET vessel_container_id = $1 WHERE shipment_id = $2`,
                    [transportContainerId, shipmentId]);
            }
        }

        return;
    }

    private async nullOutShipmentTracking(shipmentId: string) {
        await this.dbp.none(`
      UPDATE shipment_trackings set shipment_id = NULL where shipment_id = $<shipmentId>`,
            { shipmentId });
    }

}
