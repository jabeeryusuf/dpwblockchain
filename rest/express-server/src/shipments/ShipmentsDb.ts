import * as _ from "lodash";
import * as moment from "moment-timezone";
import { Client } from "pg";
import { Building, Container, ContainerType } from "tw-api-common";
import { Inject } from "typescript-ioc";
import { v4 as uuid } from "uuid";
import { DbClientSingleton } from "../utils";
import { IDatabase } from "pg-promise";

export class ShipmentsDb {

  @Inject private dbClient: DbClientSingleton;
  private db: Client;
  private dbp: IDatabase<any>;

  constructor() {
    this.db = this.dbClient.db;
    this.dbp = this.dbClient.dbp;
  }

  public async addShipmentItem(shipItemOBJ: any) {
    const { shipment_id, purchase_order_id, purchase_order_item_id, sku, delivery_quantity, shipping_container_id, product_type, org_id } = shipItemOBJ;
    const shipmentItemExist = await this.db.query(`SELECT * from shipment_items
                                       where shipment_id = '${shipment_id}' and org_id = '${org_id}'
                                              and purchase_order_id = '${purchase_order_id}' and sku = '${sku}' and shipping_container_id = '${shipping_container_id}'`);
    if (shipmentItemExist.rows.length < 1) {
      console.log("shipment item does not exist");
      const newShipmentItem = await this.dbp.none(`
      INSERT INTO shipment_items
        (shipment_id,purchase_order_id, purchase_order_item_id, sku,delivery_quantity, shipping_container_id, product_type, org_id)
      VALUES ($1,$2,$3,$4,$5,$6, $7, $8)`,
        [shipment_id, purchase_order_id, purchase_order_item_id,
          sku, delivery_quantity, shipping_container_id, product_type, org_id]);
      return newShipmentItem;
    } else if (shipmentItemExist.rows[0].delivery_quantity !== delivery_quantity) {
      const updateShipmentItem = await this.dbp.none(`
        UPDATE shipment_items SET delivery_quantity=$1 WHERE shipment_id=$2 and org_id=$3 `,
        [delivery_quantity, shipment_id, org_id]);
      console.log("updated shipment items table for", shipment_id);
      return updateShipmentItem;
    }
  }

  public async addShipment(shipOBJ: any) {
    /// etd missing from event
    //console.log("hahaha");
    //console.log(shipOBJ);
    const { shipment_id, shipment_ref, eta, destination_container_id, vessel_container_id,
      source_container_id, org_id, vessel_name, atd, ata, etd, type, tracking_ref, shipment_status, flag } = shipOBJ;
    const carrier = shipOBJ.carrier === null ? "unknown" : shipOBJ.carrier;
    const id = uuid();
    try {
      const newShipment = await this.db.query(
        `INSERT INTO shipments(shipment_id, shipment_ref, etd, eta, atd, ata, destination_container_id, vessel_name, vessel_container_id,
                     source_container_id, org_id, original_eta, tracking_id, shipment_status, flag)
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11, $12, $13, $14,$15)`,
        [shipment_id, shipment_ref, etd, eta, atd, ata, destination_container_id, vessel_name, vessel_container_id,
          source_container_id, org_id, eta, id, shipment_status, flag]);

      await this.db.query(
        `INSERT INTO shipment_trackings (tracking_id, carrier, type, tracking_ref)
        VALUES ($1, $2, $3, $4)`, [id, carrier, type, tracking_ref]);
      const res = { items: newShipment.rows };
      console.log("nice")
      return res;

    } catch (err) {
      console.log("69", err);
    }
  }

  public async updateShipmentExist(shipobj: any) {
    const { shipment_id, feed_activity_id } = shipobj;

    const shipmentFields2 = ["shipment_ref", "shipment_status", "source_container_id", "destination_container_id", "vessel_container_id", "eta", "etd", "atd", "ata", "vessel_name", "bol", "org_id"];

    const shipmentClosed = await this.db.query(`SELECT * from shipments where shipment_id = '${shipment_id}' and (shipment_status <> 'Closed' or shipment_status is null );`);
    if (shipmentClosed.rows.length > 0) {

      //pgp.helpers.update(dataSingle, ['val', 'msg'], 'my-table') + ' WHERE id = ' + dataSingle.id;
      const sql = this.dbClient.pgp.helpers.update(
        shipobj, shipmentFields2, 'shipments') + " WHERE shipment_id = $1";

      await this.dbp.none(sql, shipment_id);
    } else {
      const errorValue = `Shipment is already closed, can not update ${shipment_id}`;
      const addFeedsErrorLog = await this.db.query(`Insert into feeds_error_log (feeds_activity_id, feeds_error) values ($1, $2) `, [feed_activity_id, errorValue]);
      return addFeedsErrorLog;
    }
  }


  public async updateShipmentCarrier(shipmentId: any, carrier: any) {
    await this.db.query(`update shipments set carrier =$1 where shipment_id=$2`
      , [shipmentId, carrier]);
  }


  public async deleteShipmentItem(row: any) {
    const sql = `
      DELETE FROM shipment_items 
      WHERE 
      ${Object.keys(row).map((k: string) => (k + (row[k] ? "=$<" + k + ">" : " IS NULL"))).join(" AND ")}`;

    await this.dbp.none(sql, row);
  }

  public async insertShipmentItem(row: any) {
    const sql = `
      INSERT INTO shipment_items (
      ${Object.keys(row).join(",")} )
      VALUES (${Object.keys(row).map((k: string) => ("$<" + k + ">")).join(",")})`;

    await this.dbp.none(sql, row);
  }

 

  public async insertShipmentItems(rows: any[]) {
    if (rows.length < 1) {
      return;
    }

    const sql = this.dbClient.pgp.helpers.insert(rows, Object.keys(rows[0]), "shipment_items");

    // console.log("INSERT");
    // console.log(sql);

    await this.dbp.none(sql);
  }

  public async addShipmentItemDefault(shipment: any) {

    await this.db.query(`INSERT INTO public.shipment_items 
    (shipment_id, purchase_order_id,purchase_order_item_id,sku,delivery_quantity,shipping_container_id,org_id)
    VALUES ($1,$2,$3,$4,$5,$6,$7)`
      , [shipment.shipment_id, 9738, 'default_line', 'khDLn3OE99', 0, 'shipping-container&' + shipment.shipment_ref, shipment.org_id]);
  }



  public async insertShipment(row: any) {
    const sql = `
      INSERT INTO shipments (
      ${Object.keys(row).join(",")} )
      VALUES (${Object.keys(row).map((k: string) => ("$<" + k + ">")).join(",")})`;

    await this.dbp.none(sql, row);
  }

  public async queryShipment(whereClause: string, whereParams: string[], groupBy?: string, orderBy?: string) {
    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }
    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }
    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }
    // hack to get this working due to weird api behavior on larger arrays
    // api is somehow api is converting ['a','b','c'] to {'0':'a','1':'b','2':'c'}
    if ((typeof whereParams) === "object") {
      const newWhereParams = [];
      for (const i in whereParams) {
        newWhereParams.push(whereParams[i]);
      }
      whereParams = newWhereParams;
    }

    const iiSql = `
      SELECT *, s.shipment_id FROM shipments s
      LEFT JOIN shipment_items si on s.shipment_id = si.shipment_id and s.org_id = si.org_id 
      LEFT JOIN shipment_trackings st on st.shipment_id = s.shipment_id` + sqlEnd;
    const iiRes = await this.db.query(iiSql, whereParams);
    const res = { items: iiRes.rows };
    return res;
  }


  public async getcarrier(shipref: string) {
    try {
     
      const carrier = await this.db.query(`select cm.company from container_map cm where cm."_code"=LEFT($1,4) and cm.type = 'carrier'`,[shipref]);
      if(carrier.rowCount>0)
      {
        var carriern = carrier.rows[0].company;
       if(carriern=="KAWASAKI KISEN KAISHA LTD - K LINE" || carriern=="NYK LINE" || carriern=="MITSUI O.S.K. LINES LTD") 
       return "OCEAN NETWORK EXPRESS PTE. LTD.";  
        else
        return carriern;
      }
      else return null;
    } catch (err) {
      throw err;
    }
  }



  public async queryBuildings(address: string) {
    try {
      const buildings = await this.db.query(`Select * from buildings where address=$1 `, [address]);
      return buildings.rows;
    } catch (err) {
      return err;
    }
    
  }

  //comment
  public async insertBuildings(buildingsObj: any) {
    const { container_id, address, name, lat, lng, type, ref, time_zone_id, org_id } = buildingsObj;
    try {
      const buildings = await this.db.query(`
        INSERT into buildings
          (container_id,address,name,lat,lng,type,ref,time_zone_id, org_id)
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
        [container_id, address, name, lat, lng, type, ref, time_zone_id, org_id]);

      return buildings;
    } catch (err) {
      return err;
    }
  }

  public async queryShipmentExist(orgId: string, shipmentId: number) {
    try {
      const shipmentExist = await this.db.query(`
        SELECT * FROM shipments where org_id=$1 and shipment_id=$2`,
        [orgId, shipmentId]);

      return shipmentExist.rows;
    } catch (err) {
      return err;
    }
  }

  public async updateShipment(shipmentId: number, fieldToUpdate: string, column: string) {
    try {
      if (column === "source") {
        const containerId = `warehouse&${fieldToUpdate.replace(/\s/g, "")}`;
        const updateShipment = await this.db.query(`
            UPDATE shipments set source_container_id = $1 where shipment_id=$2`,
          [containerId, shipmentId]);

        return updateShipment.rows;
      } else if (column === "destination") {
        const containerId = `warehouse&${fieldToUpdate.replace(/\s/g, "")}`;
        const updateShipment = await this.db.query(`
            UPDATE shipments set destination_container_id = $1 where shipment_id=$2`,
          [containerId, shipmentId]);

        return updateShipment.rows;
      } else if (column === "atd") {
        const updateShipment = await this.db.query(`
            UPDATE shipments set atd = $1 where shipment_id=$2`,
          [fieldToUpdate, shipmentId]);

        return updateShipment.rows;
      } else if (column === "eta") {
        const updateShipment = await this.db.query(`
            UPDATE shipments set eta = $1, original_eta = $1 where shipment_id=$2`,
          [fieldToUpdate, shipmentId]);

        return updateShipment.rows;
      } else if (column === "ata") {
        const updateShipment = await this.db.query(`
            UPDATE shipments set ata = $1 where shipment_id=$2`,
          [fieldToUpdate, shipmentId]);

        return updateShipment.rows;
      } else if (column === "tracking_id") {
        console.log("update tracking," + fieldToUpdate + shipmentId);
        await this.db.query(`
            UPDATE shipments set tracking_id = $1 where shipment_id=$2`,
          [fieldToUpdate, shipmentId]);
      }

    } catch (err) {
      console.log("update err");
      return err;
    }
  }


  public async queryShipmentTrackingExist(trackingId: any) {
    try {
      const shipmentTrackingExist = await this.db.query(`
        Select * from shipment_trackings where tracking_id =$1`, [trackingId]);

      const trackingObj = shipmentTrackingExist.rows.length > 0 ? shipmentTrackingExist.rows[0] : {};

      return trackingObj;
    } catch (err) {
      return err;
    }
  }
  public async getDestinations(orgId: string, shipmentStatus: string) {
    const whereObj: any = { org_id: orgId, shipment_status: shipmentStatus };
    let queryStart = "SELECT buildings.name, buildings.container_id, buildings.lat, buildings.lng FROM shipments LEFT JOIN buildings on buildings.container_id = shipments.destination_container_id  ";
    if (orgId || shipmentStatus) {
      queryStart += "WHERE ";
    }
    let index = 1;
    const whereParams = [];
    for (const key in whereObj) {
      if (whereObj[key]) {
        if (key === "org_id") {
          queryStart += "shipments.";
        }
        queryStart += key + "= $" + index;
        index += 1;
        whereParams.push(whereObj[key]);
      }
    }
    queryStart += "group by buildings.name, buildings.container_id, buildings.lat, buildings.lng";
    const res = await this.db.query(queryStart, whereParams);
    const destinations = res.rows.map((val: any) => {
      return new Building({ containerId: val.container_id, name: val.name, lat: val.lat, lng: val.lng });
    });
    return { destinations };
  }
  public async addShipmentTrackingEvent(trackingEventObj: any) {
    const { tracking_id, date, status, location } = trackingEventObj;
    try {
      const addTrackingEvent = await this.db.query(`
                  INSERT INTO shipment_tracking_events(tracking_id, date, status, location)
                    VALUES ($1,$2,$3,$4)`,
        [tracking_id, date, status, location]);

      return addTrackingEvent.rows;
    } catch (err) {
      return err;
    }
  }

  /**
   * Queries for shipments with an ATA
   * @param orgID string
   */
  public async queryCurrentShipments(orgID: string) {
    const query = await this.db.query(`SELECT s.vessel_name, s.destination_container_id, s.shipment_id,
                                     s.source_container_id, s.eta, s.entry_filed, s.entry_released, s.do_issued,
                                     s.bol, b.name AS dest_name, b2.name AS source_name, c2.ref, shipment_trackings.status as tracking_status
                                     FROM shipments s
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                     LEFT JOIN buildings b2 ON s.source_container_id = b2.container_id
                                     LEFT JOIN containers c2 ON c2.parent_container_id = s.vessel_container_id
                                     LEFT JOIN shipment_trackings on shipment_trackings.shipment_id = s.shipment_id
                                     WHERE (s.shipment_status IN ('created','in-transit', 'arrived') or s.shipment_status is null)
                                     AND s.org_id=$1
                                     GROUP BY s.vessel_name, s.destination_container_id, s.shipment_id,
                                     s.source_container_id, s.eta, s.entry_filed, s.bol, c2.ref,
                                     s.entry_released, s.do_issued, b.name, b2.name, tracking_status
                                     ORDER BY b.name, s.eta ASC;`, [orgID]);
    const current = query.rows;
    const currentShips = [];
    const aggregate: any = {};

    // aggregation of current shipments by their destination
    if (current.length > 0) {
      for (const row of current) {
        row.format_eta = row.eta ? moment(row.eta).format("MMMM Do YYYY") : "Unknown ETA";
        if (row.dest_name === "Port of Seattle") {
          row.entry_filed = row.entry_filed ? moment(row.entry_filed).format("MMMM Do YYYY") : "Not Recorded";
          row.entry_released = row.entry_released ? moment(row.entry_released).format("MMMM Do YYYY") : "Not Recorded";
          row.do_issued = row.do_issued ? moment(row.do_issued).format("MMMM Do YYYY") : "Not Recorded";
        }
        if (!aggregate[row.dest_name]) {
          aggregate[row.dest_name] = [row];
        } else {
          const arr = aggregate[row.dest_name];
          arr.push(row);
          aggregate[row.dest_name] = arr;
        }
      }
      // tslint:disable-next-line: forin
      for (const key in aggregate) {
        const newObj = {
          dest: key,
          shipments: aggregate[key],
        };
        currentShips.push(newObj);
      }
    }

    return currentShips;

  }

  /**
   * Queries for shipments without an ATA and shipment_status is not 'Late Upload'
   * @param orgID string
   */
  public async queryPastShipments(orgID: string) {
    const query = await this.db.query(`SELECT s.vessel_name, s.destination_container_id, s.shipment_id,
                                     s.source_container_id, s.eta, s.ata, s.atd, s.entry_filed, s.entry_released,
                                     s.do_issued, s.bol, b.name AS dest_name, b2.name AS source_name
                                     FROM shipments s
                                     LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                     LEFT JOIN buildings b2 ON s.source_container_id = b2.container_id
                                     WHERE s.org_id = $1
                                     AND s.shipment_status IN ('discharged', 'user-closed')
                                     GROUP BY s.vessel_name, s.destination_container_id, s.shipment_id,
                                     s.source_container_id, s.eta, s.ata, s.atd, b.name, b2.name,
                                     s.entry_filed, s.entry_released, s.do_issued, s.bol,
                                     ORDER BY s.destination_container_id, s.ata ASC`, [orgID]);
    const past = query.rows;
    const aggregate: any = {};
    // aggregation of current shipments by their destination
    if (past.length > 0) {
      for (const row of past) {
        row.format_ata = row.ata === null ? "Not Recorded" : moment(row.ata).format("MMMM Do YYYY");
        row.format_atd = row.atd === null ? "Not Recorded" : moment(row.atd).format("MMMM Do YYYY");
        row.entry_filed = row.entry_filed ? moment(row.entry_filed).format("MMMM Do YYYY") : "Not Recorded";
        row.entry_released = row.entry_released ? moment(row.entry_released).format("MMMM Do YYYY") : "Not Recorded";
        row.do_issued = row.do_issued ? moment(row.do_issued).format("MMMM Do YYYY") : "Not Recorded";
        if (!aggregate[row.dest_name]) {
          aggregate[row.dest_name] = [row];
        } else {
          const arr = aggregate[row.dest_name];
          arr.push(row);
          aggregate[row.dest_name] = arr;
        }
      }
    }
    const ships = [];
    // tslint:disable-next-line: forin
    for (const key in aggregate) {
      const newObj = {
        dest: key,
        shipments: aggregate[key],
      };
      ships.push(newObj);
    }
    return ships;
  }
  public async getExceptions(orgId: string) {
    /*  const res = await this.db.query(
     `
     SELECT * FROM shipment_tracking_events
     join shipments on
     shipment_tracking_events.tracking_id = shipments.tracking_id
     where expected_date < now() and event_date is null and shipments.tracking_id in
     (SELECT tracking_id from shipments where org_id =$1)`, [orgId]);
     return res.rows; */

    const res = await this.db.query(
      `
      SELECT * FROM shipment_tracking_events
      join shipment_trackings on
      shipment_tracking_events.tracking_id = shipment_trackings.tracking_id
      where expected_date < now() and event_date is null and shipment_trackings.shipment_id in
      (SELECT shipment_id from shipments where org_id =$1)`, [orgId]);

    return res.rows;

  }
  // completed param allows you to grab only events that have actually occured
  // limit most recent allows you to select only the most recent tracking
  public async getTrackingEvents(orgId: string, completed?: boolean, limitMostRecent?: boolean) {
    const params = [orgId];
    let iiSql;
    if (!limitMostRecent) {
      iiSql = "SELECT * FROM shipment_tracking_events where tracking_id in (SELECT tracking_id from shipments WHERE org_id = $1) ";
    } else {
      iiSql = "SELECT DISTINCT ON(tracking_id) * FROM shipment_tracking_events where tracking_id in (SELECT tracking_id from shipments WHERE org_id = $1) ";
    }
    if (completed) {
      iiSql += " and event_date is not null";
    }
    if (limitMostRecent) {
      iiSql += " ORDER BY tracking_id, event_date DESC ";
    }

    const res = await this.db.query(iiSql, params);
    return res.rows;
  }
  /**
   * Queries for this special case of shipment 'Late Upload'
   * @param orgID ID of org logged in
   */
  public async queryLateUploadShipments(orgID: string) {
    const query = await this.db.query(`SELECT s.vessel_name, s.destination_container_id, s.shipment_id,s.shipment_ref,
                                       s.shipment_status, s.source_container_id, s.eta, s.ata, s.atd,
                                       s.entry_filed, s.entry_released, s.do_issued,
                                       b.name AS dest_name, b2.name AS source_name
                                       FROM shipments s
                                       LEFT JOIN buildings b ON s.destination_container_id = b.container_id
                                       LEFT JOIN buildings b2 ON s.source_container_id = b2.container_id
                                       WHERE shipment_status='Late Upload' AND s.org_id = $1
                                       GROUP BY s.vessel_name, s.destination_container_id, s.shipment_id,
                                       s.source_container_id, s.eta, s.ata, s.atd, b.name, b2.name,
                                       s.entry_filed, s.entry_released, s.do_issued, s.shipment_status,
                                       ORDER BY s.eta ASC;`, [orgID]);

    const lateUploads = query.rows;
    const lateUploadShips = [];

    if (lateUploads.length > 0) {
      const shipments = [];

      for (const row of lateUploads) {
        row.format_eta = row.eta ? moment(row.eta).format("MMMM Do YYYY") : "Unknown ETA";
        shipments.push(row);
      }
      lateUploadShips.push({
        dest: "Late Uploads",
        shipments,
      });
    }

    return lateUploadShips;
  }

  public async queryPOsOnShipment(orgID: string, shipmentID: string) {
    const query = await this.db.query(
      `SELECT e.*, p.name AS prod_name
     FROM (SELECT d.*, bui2.name AS dest_name, bui2.lat AS dest_lat, bui2.lng AS dest_lng
           FROM (SELECT c.*, bui.name AS source_name
                 FROM (SELECT a.*, si.purchase_order_id, si.purchase_order_item_id, si.production_lot as lot,
                       si.sku, si.delivery_quantity as quantity
                       FROM (SELECT s.shipment_id, s.shipment_ref, s.eta, s.original_eta, s.atd, s.etd, s.vessel_container_id,
                             s.ata, s.remark, s.vessel_name, s.source_container_id, s.shipment_status,
                             s.destination_container_id, st.tracking_id,
                             v.last_port, v.next_port, v.lat, v.lng, v.timestamp
                             FROM shipments s
                             LEFT JOIN vehicle_locations v ON s.vessel_container_id = v.vessel_container_id
                             LEFT JOIN shipment_trackings st ON st.shipment_id = s.shipment_id
                             WHERE s.org_id = $1 and s.shipment_id =$2 order by v.update_time limit 1) a
                       LEFT JOIN shipment_items si ON a.shipment_id = si.shipment_id and org_id=$1) c
                 LEFT JOIN buildings bui ON c.source_container_id = bui.container_id ) d
           LEFT JOIN buildings bui2 ON d.destination_container_id = bui2.container_id) e
     LEFT JOIN products p ON e.sku = p.sku and org_id=$1 ;`, [orgID, shipmentID]);
    const carrierQuery = await this.db.query(
      `SELECT cn.name, st.carrier
       FROM shipments s
       LEFT JOIN shipment_trackings st ON s.shipment_id = st.shipment_id
       LEFT JOIN carrier_names cn ON st.carrier = cn.code
       WHERE s.shipment_id=$1 AND s.org_id=$2`, [shipmentID, orgID]);
    let carrierName: string = null;
    if (carrierQuery.rows.length > 0) {
      carrierName = carrierQuery.rows[0].name;
    }

    const list = query.rows;
    const oneShipment = [];

    if (list.length > 0) {
      for (const row of list) {
        row.carrierName = carrierName === null ? "Unknown" : carrierName;
        row.etaFormat = row.eta ? moment(row.eta).format("MMMM Do YYYY HH:mm") : "Unknown ETA";
        if (row.shipment_id === shipmentID) {
          oneShipment.push(row);
        }
      }

      oneShipment.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 :
        ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));
    }
    console.log(oneShipment);
    return oneShipment;
  }

  public async queryShipHistory(shipmentId: string) {
    const sql =
      `SELECT i.*
     FROM ( SELECT g.*, h.lat AS dest_lat, h.lng AS dest_lng, h.name AS dest_name
           FROM ( SELECT e.*, f.lat AS next_lat, f.lng AS next_lng,
                  f.name AS next_name, f.container_id as next_container
                  FROM ( SELECT c.*, d.lat AS last_lat, d.lng AS last_lng,
                         d.name AS last_name, d.container_id AS last_container
                         FROM ( SELECT a.*, a.destination_container_id AS dest_container, b.speed,
                                b.last_port, b.next_port, b.lat, b.lng, b.timestamp, b.vessel_name
                                FROM shipments a RIGHT JOIN vehicle_locations b ON
                                a.vessel_container_id = b.vessel_container_id
                                WHERE a.shipment_id = $1) c
                         LEFT JOIN buildings d ON c.last_port = d.address) e
                   LEFT JOIN buildings f ON e.next_port = f.address) g
           LEFT JOIN buildings h ON g.dest_container = h.container_id ) i
           where (i.ata is null or i.timestamp <= i.ata) AND (i.timestamp >= i.atd)
     ORDER BY i.ata, i.timestamp DESC`;

    return await this.db.query(sql, [shipmentId]);
  }




  // returns a list of containers for now
  // at some point we probably add the items to container

  public async getContainersForShipment(shipmentId: string) {
    const res = await this.db.query("SELECT distinct(shipping_container_id) FROM shipment_items where shipment_id = $1", [shipmentId]);
    const containers = res.rows.map((val: any) => {
      if (val.shipping_container_id) {
        return new Container({
          containerId: val.shipping_container_id,
          type: ContainerType.SHIPPING_CONTAINER,
          ref: val.shipping_container_id.slice(val.shipping_container_id.indexOf("&") + 1)
        })
      }
    });
    // shipping container could potentially be null so we filter out falsy values from the array
    const filterContainers = containers.filter((val) => {
      if (val) {
        return val;
      }
    });
    return { containers: filterContainers };
  }

  public async updateFeedActivity(status: string, fileId: string, errorFileId?: string) {
    if (errorFileId === undefined || errorFileId === null) {
      const query = await this.db.query(` Update feeds_activity set feeds_status = $1 where feeds_activity_id = $2`, [status, fileId]);
      return query.rows;
    } else {
      const query = await this.db.query(` Update feeds_activity set feeds_status = $1, error_file_id = $2 where feeds_activity_id = $3`, [status, errorFileId, fileId]);
      return query.rows;
    }
  }

  public async addFeedsActivity(status: string, fileId: string, orgId: string) {
    const resp = await this.db.query(`Select original_name from files where file_id = $1`, [fileId]);
    const fileName = resp.rows[0].original_name;
    const query = await this.db.query(` Insert into feeds_activity(feeds_activity_id, file_name, org_id, feeds_status) values ($1, $2, $3, $4)`, [fileId, fileName, orgId, status]);
    return query.rows;
  }

  public async addFeedsErrorLog(fileId: string, errorMessage: string) {
    const query = await this.db.query(` Insert into feeds_error_log(feeds_activity_id, feeds_error) values ($1, $2)`, [fileId, errorMessage]);
    return query.rows;
  }

  public async selectErrorLog(fileId: string) {
    const query = await this.db.query(` Select * from feeds_error_log where feeds_activity_id = $1`, [fileId]);
    return query.rows;
  }

  public async queryShipmentRefExist(shipmentRef: string, orgId: string) {
    const ShipmentExist = await this.db.query(` Select * from shipments where shipment_ref = $1 and org_id = $2`, [shipmentRef, orgId]);
    return ShipmentExist.rows;
  }

}
