export class ShipmentSearch {
    private db: any;
    private orgId: any;
    private limit: any;
    private cursor: any;
    private filterParams: object;
    private searchString: string;
    private orderBy: string;
    private groupBy: string;
    private desc: boolean;
    private fullCount: number;
    constructor(
        db: any,
        orgId: string,
        limit: number,
        cursor: string,
        filterParams: object,
        searchString: string,
        orderBy: string,
        groupBy: string,
        desc: boolean) {
        this.db = db;
        this.desc = desc;
        this.orgId = orgId;
        this.limit = limit;
        this.cursor = cursor;
        this.filterParams = filterParams;
        this.searchString = searchString;
        this.orderBy = orderBy;
        this.groupBy = groupBy;
        this.fullCount = 0;
    }

    public async search() {

        console.log("Starting Search service");
        this.searchString = this.searchString + "%";

        // var sql = `SELECT ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + `, count(*) OVER() AS full_count FROM shipment_items
        // left join shipments
        // on shipment_items.shipment_id = shipments.shipment_id
        // left join shipment_trackings on
        // shipments.shipment_id = shipment_trackings.shipment_id
        // where shipments.org_id = `+this.orgId+` ` +
        //       this.generateWhereClauseShips(this.filterParams) + `
        // and (shipment_items.sku ilike `+this.searchString+`
        // or shipment_items.product_type ilike `+this.searchString+`
        // or shipment_items.shipping_container_id
        // ilike concat('shipping-container&',`+this.searchString+`)
        // or shipments.shipment_ref ilike `+this.searchString+`
        // or shipments.destination_container_id ilike concat('port&',`+this.searchString+`)
        // )
        // group by `
        //       + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) +
        //       ` order by max(` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + `)`
        //       + this.addDesc() + `
        // offset `+this.cursor+`limit `+this.limit;
        

        //console.log("Shipment Search Query");
        //console.log(sql);
        
        const shipRes = await this.db.any(
            `
      SELECT ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + `, count(*) OVER() AS full_count FROM shipment_items
      left join shipments
      on shipment_items.shipment_id = shipments.shipment_id
      left join shipment_trackings on
      shipments.shipment_id = shipment_trackings.shipment_id
      where shipments.org_id = $<orgId> ` +
            this.generateWhereClauseShips(this.filterParams) + `
      and (shipment_items.sku ilike $<searchString>
      or shipment_items.product_type ilike $<searchString>
      or shipment_items.shipping_container_id
      ilike concat('shipping-container&',$<searchString>)
      or shipments.shipment_ref ilike $<searchString>
      or shipments.destination_container_id ilike concat('port&',$<searchString>)
      )
      group by `
            + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) +
            ` order by max(` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + `)`
            + this.addDesc() + `
      offset $<cursor> limit $<limit>
      `, Object.assign({ cursor: Number(this.cursor), limit: this.limit, orgId: this.orgId, searchString: this.searchString }, this.filterParams ? this.filterParams : {}));
        // for some of the columns we need to specify table due to
        if (shipRes.length === 0) {
            return [];
        }
        this.fullCount = Math.ceil(shipRes[0].full_count / this.limit);

        const idList = shipRes.map((val: any) => val[this.getColumn(this.groupBy)]);

        //console.log("ID List is");
        //console.log(idList);

        const queryList = await this.db.any(
            `
            SELECT shipment_items.*,
            shipments.eta,
            shipments.ata,
            shipments.etd,
            shipments.atd,
            shipments.vessel_name,
            shipments.expected_eta,
            shipments.shipment_ref,
            dgateout(shipments.shipment_ref) as gateout,
            shipments.source_container_id,
            shipments.destination_container_id,
            shipment_trackings.tracking_id,
            shipment_trackings.tracking_ref,
            shipments.carrier as carrier,
            latest_event_gateout.mn as gateoutyu,
            shipment_trackings.status,
            latest_event_voyage.voyage
            FROM shipment_items
            LEFT JOIN shipments on shipment_items.shipment_id = shipments.shipment_id
            LEFT JOIN shipment_trackings on shipment_items.shipment_id = shipment_trackings.shipment_id
            LEFT JOIN (
                        SELECT a.tracking_id as tracking_iddd,*
                        FROM (
                           SELECT tracking_id,status, max(date) AS mn
                           FROM shipment_tracking_events where (position('gate out' in lower(status) ) <>0 or position('gate-out' in lower(status) ) <>0)  and position('empty' in lower(status) ) =0
                           and date <= current_timestamp
                           GROUP BY tracking_id,status) t inner JOIN shipment_tracking_events a ON a.tracking_id = t.tracking_id and a.date = t.mn and a.status = t.status
                         ) latest_event_gateout on shipment_trackings.tracking_id = latest_event_gateout.tracking_iddd
             LEFT JOIN (
                         SELECT a.tracking_id as tracking_iddd,*
                         FROM (
                            SELECT tracking_id, max(date) AS mn
                            FROM shipment_tracking_events where voyage is not null
                            GROUP BY tracking_id) t inner JOIN shipment_tracking_events a ON a.tracking_id = t.tracking_id and a.date = t.mn
                          ) latest_event_voyage on shipment_trackings.tracking_id = latest_event_voyage.tracking_iddd
    where ` + this.addTablePrefixToColumns(this.getColumn(this.groupBy)) + ` ::text in ($1:csv)
    ORDER BY ` + this.addTablePrefixToColumns(this.getColumn(this.orderBy)) + this.addDesc(),
            [idList],

        );
        //console.log("query List after is");
        //console.log(queryList);

        //console.log("Returing from Search service");
        return queryList;        
    }

    public getFullCount() {
        return this.fullCount;
    }
    // since we are working with multiple tables
    // some of which have columns of the same name we occasionally need
    // to specify which table we are referencing
    // this function provides a mapping to tables
    // to avoid table ambiguity error
    private addTablePrefixToColumns(col: string) {
        const shipmentPrefixMap: any = {
            ata: "shipments.ata",
            atd: "shipments.atd",
            destination_container_id: "shipments.destination_container_id",
            eta: "shipments.eta",
            etd: "shipments.etd",
            shipment_ref: "shipments.shipment_ref",
            source_container_id: "shipments.source_container_id",
            bol: "bol",
            gateout: "gateout",
            carrier: "shipment_trackings.carrier",
        };
        if (shipmentPrefixMap[col]) {
            return shipmentPrefixMap[col];
        }
        return "shipment_items." + col;
    }
    private addDesc() {
        if (this.desc) {
            return " desc ";
        }
        return " ";
    }
    private generateWhereClauseShips(filters: any) {
        let whereClause = "";
        if (filters) {
            Object.keys(filters).forEach((val: any) => {
                if (filters[val] && this.getColumn(val) && filters[val] !== "all") {
                    whereClause += " and " + this.getColumn(val) + " = $<" + val + "> ";
                }
            });
        }
        return whereClause;
    }
    // function that takes in a camelcased shipment
    // or shipment item propert and returns the equivelent
    // string corresponding to a column in our db
    private getColumn(property: string) {
        const columnMap: any = {
            destinationContainerId: "destination_container_id",
            productId: "sku",
            productType: "product_type",
            shipmentId: "shipment_id",
            shipmentRef: "shipment_ref",
            shipmentStatus: "shipment_status",
            shippingContainerId: "shipping_container_id",
            sourceContainerId: "source_container_id",
            eta: "eta",
            etd: "etd",
            atd: "atd",
            ata: "ata",
            bol: "bol",           
            expected_eta: "expected_eta",
            carrier: "carrier",
            gateout:"gateout",
        };
        return columnMap[property];
    }

}
