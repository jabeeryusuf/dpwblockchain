import * as ejs from "ejs";
import * as nodemailer from "nodemailer";
import * as path from "path";
import { Client } from "pg";
import { Inject } from "typescript-ioc";
import * as util from "util";

import { FilesService } from "../files";
import { DbClientSingleton } from "../utils";

// tslint:disable-next-line: no-var-requires
const nl2br = require("nl2br");

const s3URL = "http://DPWChain-public.s3.us-east-2.amazonaws.com/email-logos/";
const templateMap: any = {
  message: "emails/pages/genericMessage.ejs",
  summary: "/emails/pages/summary.ejs",
  verification: "emails/pages/verifyCode.ejs",
};
export class EmailsService {

  @Inject private dbClient: DbClientSingleton;
  @Inject private fileService: FilesService;

  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  // Function to preserve multiple lines of message sent through the api
  // accepts a message string representing the message which needs to be modified into html
  // And a style string representing the style that should be applied to the message body

  public generateMultilineHtmlMessage(message: string, style: any) {
    // naiive approach for the purpose of styling the email. Can refactor later
    const caseInfoMap: any = {
      "Case Priority": "Case Priority",
      "Case Type": "Case Type",
      "Factory Code": "Factory Code",
      "Factory Name": "Factory Name",
      "Purchase Order ID": "Purchase Order ID",
      "SKUs": "SKUs",
    };

    // removes quotes and splits into array'
    const lines = message.slice(1, message.length - 1).split("\\n");
    let htmlString = "";
    for (const line of lines) {
      const strArr = line.split(":");
      if (caseInfoMap[strArr[0]]) {
        htmlString += `<span style = "` + style + `" />` + line + `</span><br/>`;
      } else {
        htmlString += `<p style = "` + style + `" />` + line + `</p>`;
      }
    }
    return htmlString;
  }
  // returns an array of all the emails should be contacted for a given group
  public async getEmailListForGroup(groupId: string) {
    const emailList = await this.db.query(`WITH RECURSIVE userdetails AS (
            select
              group_id,
              child_group_id
              from
              group_group_members
              where group_id = $1
              UNION
            select
              g.group_id,
              g.child_group_id
              from
              group_group_members g
              JOIN userdetails s on g.group_id = s.child_group_id
          ) select u.email from userdetails s1 , users u where
            s1.child_group_id = u.user_id;`, [groupId]);
    const filteredEmails = [];
    for (const row of emailList.rows) {
      filteredEmails.push(row.email);
    }
    return filteredEmails;
  }

  public async sendEmail(to: string[], from: string, subject: string, text: string, html: string, attachments: any) {

    const transporter = await nodemailer.createTransport({
      auth: {
        pass: "5kfGT6nwESfZ999",
        user: "dpwchain@outlook.com",
      },
      host: "smtp.office365.com",
      port: 587,
      requireTLS:true,
    });

    const mailOptions = {
      attachments,
      from,
      html,
      subject, // Subject line
      text, // plain text body
      to, // list of receivers
    };
    //if ((!process.env.NODE_ENV || process.env.NODE_ENV === "dev") && (!process.env.SEND_EMAIL || process.env.SEND_EMAIL !== "true")) {
    if (false) {
      return;
    } else {
      console.log("ACTUALLY SENDING");

      transporter.sendMail(mailOptions, async (error, info) => {
        if (error) {
          return console.log(error);
        }
        // console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

        // deletes the attachments from local folder on node server once sent
      });
    }
  }

  public async createEmailFromTemplate(recipients: any, template: any, subject: string, params: any, attachments?: any) {
    const getHtmlString = util.promisify(ejs.renderFile.bind(ejs));
    const html = await getHtmlString(path.join(__dirname, "../..", "src", templateMap[template]), params);
    this.sendEmail(recipients, params.user.email,
      subject, "Message from DPWChain", html, attachments);
  }

  public async sendEmailFromMessage(message: any) {

    const filteredEmails = await this.getEmailListForGroup(message.to);
    const formattedAddresses = this.generateLinkandFromAddress(message.to);
    const fromEmail = formattedAddresses.to;
    const DPWChainLink = formattedAddresses.link;
    const userRes = await this.db.query("SELECT name,org_id from users where user_id = $1", [message.from]);
    const userName = userRes.rows[0].name;
    const orgId = userRes.rows[0].org_id;
    const groupRes = await this.db.query("SELECT group_name, org_id from groups where group_id = $1", [message.to]);
    const groupName = groupRes.rows[0].group_name;
    const subjectLine = userName + " sent a message to " + groupName;
    const displayEmail = userName + " via DPWChain <" + fromEmail + ">";

    // gets the attached file from s3 and querys the db to get the orginal display name
    let attachmentFiles;
    if (message.files) {
      attachmentFiles = await Promise.all(message.files.map(async (val: any) => {
        const awsFile = await this.fileService.retrieveFilesFroms3(val);
        const fileInfo = await this.db.query("SELECT original_name from files where file_id =$1", [val]);
        return { filename: fileInfo.rows[0].original_name, content: awsFile.Body };
      }));
    }
    const logo = await this.getLogoForSender(orgId);
    const processedMessage = nl2br(message.text);
    this.createEmailFromTemplate(filteredEmails, "message", subjectLine, { logo: logo, message: processedMessage, user: { name: userName, email: displayEmail }, group: { name: groupName }, DPWChainLink: DPWChainLink }, attachmentFiles);
  }
  public async getLogoForSender(orgId: string) {
    const senderLogo = await this.db.query("SELECT value FROM org_settings where org_id = $1 and key = 'logo'", [orgId]);
    let logo;
    if (senderLogo.rows) {
      logo = s3URL + senderLogo.rows[0].value;
    } else {
      logo = "https://dpwchainpublic.s3-eu-west-1.amazonaws.com/logo/dpwTeuLogo.png";
    }
    return logo;
  }

  public generateLinkandFromAddress(to: any) {
    let DPWChainLink;
    let fromEmail;
    if (process.env.NODE_ENV === "staging") {
      fromEmail = to + "@message-staging.DPWChain.com";
      DPWChainLink = "https://www-staging.DPWChain.com/";
    }
    if (process.env.NODE_ENV === "production") {
      fromEmail = to + "@message.DPWChain.com";
      DPWChainLink = "https://app.DPWChain.com/";
    }
    if (process.env.NODE_ENV === "dev" || !process.env.NODE_ENV) {
      fromEmail = to + "@message.DPWChain.com";
      DPWChainLink = "http://localhost:3000";
    }

    return {link: DPWChainLink, to: fromEmail};
  }
}
