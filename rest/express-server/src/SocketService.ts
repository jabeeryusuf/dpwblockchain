import * as sockIo from "socket.io";
import * as http from "http";

import { MessageService } from "./messages/MessagesService";
import { NotificationService } from "./NotificationService";
import { Inject } from "typescript-ioc";

// keep in sync with react client
const Protocol = {
  // Core protocol
  CONNECT:              "connect",
  DISCONNECT:           "disconnect",
  CONNECT_ERR:          "connect_error",
  RECONNECT_ERR:        "reconnect_error",

  // Authentication
  AUTHENTICATE:         "authenticate",
  AUTHENTICATE_SUCCESS: "authenticate_success",
  AUTHENTICATE_FAILURE: "authtenicate_failure",

  // Messaging
  JOIN_CONVERSATION:    "join_conversation",
  LEAVE_CONVERSATION:   "leave_conversation",
  UPDATE_CLIENT:        "update_client",
  MESSAGE:              "message",

  // Notification
  ENABLE_NOTIFICATIONS:  "enable_notifications",
  DISABLE_NOTIFICATIONS: "disable_notifications",
  NOTIFY:                "notify"
}


export class SocketService {

  //private userService: UserService;
  @Inject private msgService: MessageService;
  @Inject private notificationService: NotificationService;
  public io: any;


  public async init(server: http.Server) {

    // Create a socket IO for the server and start 
    this.io = sockIo.listen(server);

    // Setup socket.io
    this.io.on("connection", (socket:any) => {
      console.log(`${socket.id} connected`);
      let user:any = null;

      socket.on(Protocol.AUTHENTICATE, async (message:any) => {

        //let user = await this.userService.getUser(message.user_id);

        console.log("Socket Authenticated: %s", socket.id);
        //console.dir(message);
        socket.emit(Protocol.AUTHENTICATE_SUCCESS,{bleh: "bleh"});

        this.msgService.onConnect(message.userId,message.orgId, socket);

      });

      socket.on(Protocol.MESSAGE, (message:any) => {
        console.log("socket fella",message)
        this.msgService.receiveMessage(message)

      });

      socket.on(Protocol.DISCONNECT, () => {
        this.msgService.onDisconnect(socket)       
        console.log(`${socket.id} disconnected`);
      });

      socket.on(Protocol.ENABLE_NOTIFICATIONS, (message:any) => {
        console.log("enabling notifications");
        this.notificationService.onEnableNotifications(socket, user, message);
      });

    });



  }

}
