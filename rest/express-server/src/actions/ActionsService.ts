import { Request } from "express";
import { Client } from "pg";
import { Inject } from "typescript-ioc";
import { ContextRequest, GET, Param, Path, POST, Security } from "typescript-rest";
import { DbClientSingleton } from "../utils";

@Path("/v0.1/actions")
@Security("USER")
export class ActionsService {
  @ContextRequest private request: Request;
  @Inject private dbClient: DbClientSingleton;
  private db: Client;

  constructor() {
    this.db = this.dbClient.db;
  }

  @POST
  @Path("/create")
  public async create(@Param("container_id") containerId: string, @Param("actionType") type: string) {
    const requestorId: any = this.request.user.orgId;
    const actionCheck = await this.db.query(`SELECT * FROM actions WHERE container_id=$1 AND approval_date IS NULL`, [containerId]);
    if (actionCheck.rows.length === 0) {
      const actionId = String(containerId.slice(19)) + "&";
      try {
        const newAction = await this.db.query(`INSERT into actions (action_id, container_id, type, requestor_id, requestee_id) VALUES (CONCAT(($1::VARCHAR), now()), $2, $3, $4, $5)`, [actionId, containerId, type, requestorId, "dpworld.ca"]);
        return newAction;
      } catch (err) {
        console.log(err);
      }

    } else {
      return false;
    }
  }

  @POST
  @Path("/delete")
  public async delete(@Param("id") id: string) {
    try {
      const deleteAction = await this.db.query("DELETE FROM actions WHERE action_id=$1", [id]);
      return deleteAction;
    } catch (err) {
      console.log(err);
    }
  }

  @GET
  @Path("/query")
  public async query(@Param("whereClause") whereClause: string, @Param("whereParams") whereParams: string[],
                     @Param("groupBy") groupBy: string[] , @Param("orderBy") orderBy: string[]) {

    let sqlEnd = "";
    if (whereClause) {
      sqlEnd += " WHERE " + whereClause;
    }

    if (groupBy) {
      sqlEnd += " GROUP BY ";
      for (let i = 0; i < groupBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += groupBy[i];
      }
    }

    if (orderBy) {
      sqlEnd += " ORDER BY ";
      for (let i = 0; i < orderBy.length; i++) {
        if (i > 0) {
          sqlEnd += ",";
        }
        sqlEnd += orderBy[i];
      }
    }

    // select actions.*, containers.parent_container_id, shipments.destination_container_id, buildings.name from actions JOIN containers on actions.container_id=containers.container_id JOIN shipments on containers.parent_container_id=shipments.vessel_container_id JOIN buildings on shipments.destination_container_id=buildings.container_id WHERE shipments.destination_container_id='port&JKT';
    // let iiSql = 'SELECT * from actions' + sqlEnd;
    const iiSql = `SELECT actions.*, containers.parent_container_id, vehicle_destinations.pod, buildings.name, orgs.name as requested_by
                 FROM actions
                 LEFT JOIN containers on actions.container_id=containers.container_id
                 LEFT JOIN orgs on orgs.org_id = actions.requestor_id
                 LEFT JOIN vehicle_destinations on containers.parent_container_id = vehicle_destinations.vessel_container_id
                 LEFT JOIN buildings on vehicle_destinations.pod = buildings.container_id` + sqlEnd;

    const iiRes = await this.db.query(iiSql, whereParams);
    const rows = iiRes.rows;
    const containerIDs = [];
    const records = [];

    for (const obj of rows) {
      if (containerIDs.indexOf(obj.container_id) < 0) {
        containerIDs.push(obj.container_id);
        records.push(obj);
      }
    }

    const res = { items: records };
    return res;
  }

  @POST
  @Path("/resolveRequest")
  public async resolve(@Param("id") id: string, @Param("resolve") resolve: string) {

    const resolveRequest = await this.db.query(`
      UPDATE actions SET action_status=$2, approval_date=now() WHERE action_id=$1`,
      [id, resolve]);
    return resolveRequest;
  }

  @POST
  @Path("/update")
  public async update(@Param("id") id: any, @Param("actionType") actionType: any) {

    const newUpdate = await this.db.query(`
      UPDATE actions SET type=$2, request_date=now() WHERE action_id=$1`,
      [id, actionType]);
    return newUpdate;
  }
}