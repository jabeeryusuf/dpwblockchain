"use strict";

import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import { Express } from "express";
import * as fs from "fs";
import * as http from "http";
import { JsonConvert, OperationMode, ValueCheckingMode } from "json2typescript";
import * as multer from "multer";
import * as path from "path";
import { Inject } from "typescript-ioc";
import { Server as TSServer } from "typescript-rest";
import * as util from "util";

// Newer style typescript-rest service
import { ActionsService } from "./actions/ActionsService";
import { OAuthAuthenticator } from "./auth";
import { AuthService } from "./auth/AuthService";
import { CarriersService } from "./carriers/CarriersService";
import { CasesService } from "./cases/CasesService";
import { CaseTypesService } from "./cases/types/CaseTypesService";
import { ContainersService } from "./containers";
import { VesselsService } from "./containers";
import { ConversationsService } from "./conversations/ConversationsService";
import { FeedsService } from "./feeds";
import { FilesService } from "./files";
import { GroupsService } from "./groups";
import { MessageService } from "./messages/MessagesService";
import { OrgsService } from "./orgs";
import { ProductsService } from "./products/ProductsService";
import { PurchaseOrderItemsService } from "./purchaseOrders/Items/PurchaseOrderItemsService";
import { PurchaseOrdersService } from "./purchaseOrders/PurchaseOrdersService";
import { ShipmentsService } from "./shipments";
import { SocketService } from "./SocketService";
import { TemplatesService } from "./templates/TemplatesService";
import { TrackingsService } from "./shipments/TrackingsService";
import { UserService } from "./users/UsersService";
import { DbClientSingleton, NodeQueryClient } from "./utils";
import { VendorsService } from "./vendors";

// Import types for translation
import { Tracking, Shipment } from "tw-api-common";

const upload = multer({ dest: "./../Uploads/" });
const previewMap: any = {
  message: "pages/genericMessage.ejs",
};

// Setup Logger

export class Server {

  private socketService: SocketService;

  private app: Express;

  private server: http.Server;
  private port: number;
  @Inject private dbClient: DbClientSingleton;

  constructor(port: number) {
    this.port = port;

    this.socketService = new SocketService();

  }

  public async startServer() {

    console.log("STARTING SERVER");
    await this.dbClient.connect();

    // Start express
    this.app = express();
    this.app.set("view engine", "ejs");
    this.app.set("views", path.join(__dirname, "..", "src", "/emails"));

    // Config
    this.app.set("port", this.port);

    // Setup parsing functionality
    this.app.use(cors());
    this.app.use(bodyParser.json({limit: "50mb"}));
    this.app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
    this.initApiHandlers();

    console.log("starting http server");
    this.server = http.createServer(this.app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    this.server.listen(this.port);
    this.server.on("error", (e) => { this.onError(e); });
    this.server.on("listening", () => { this.onListening(); });

    // User and auth DB

    // Init services now that everything is up
    await this.socketService.init(this.server);
  }

  /**
   * Main API handlers
   */

  public initApiHandlers() {
    this.app.get("/v0.1/file/downloadFile/:downloadFolder/:downloadFile", async (req: any, res: any) => {
      // const current = process.cwd();
      const folderPath = path.join(__dirname, "..", "..", "Downloads/", req.params.downloadFolder);
      res.download(path.join(folderPath, req.params.downloadFile), () => {
        fs.unlink(path.join(folderPath, req.params.downloadFile), (err) => {
          if (err) { console.log("error me", err); }
          fs.rmdir(folderPath, (errx: any) => {
            if (errx) { console.log(errx); }
            return;
          });
        });
      });
    });
    this.app.get("/v0.1/message/previewEmail/:template", (req: any, res: any) => {
      const ejsTemp = previewMap[req.params.template];
      res.render(ejsTemp, req.query);
    });

    //this.oauth = new OAuth2Protocol();

    TSServer.useIoC();
    TSServer.registerAuthenticator(new OAuthAuthenticator());
    TSServer.buildServices(
      this.app,
      ActionsService,
      AuthService,
      CarriersService,
      CasesService,
      CaseTypesService,
      ContainersService,
      ConversationsService,
      FeedsService,
      GroupsService,
      PurchaseOrderItemsService,
      ProductsService,
      MessageService,
      NodeQueryClient,
      OrgsService,
      PurchaseOrdersService,
      ShipmentsService,
      TemplatesService,
      TrackingsService,
      UserService,
      VendorsService,
      VesselsService,
    );

    const jsonConvert: JsonConvert = new JsonConvert();
    //jsonConvert.operationMode = OperationMode.LOGGING;
    jsonConvert.operationMode = OperationMode.ENABLE;
    jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;

    TSServer.addParameterConverter( (o) => (jsonConvert.deserialize(o, Tracking)), Tracking);
    //TSServer.addParameterConverter( (o) => (jsonConvert.deserialize(o, Shipment)), Shipment);
    TSServer.addParameterConverter( (o) => {

      console.log("SI CONVERSTER")
      console.log(o);
      return jsonConvert.deserialize(o, Shipment);

    },Shipment);

    // this should move into paths
    // app.use(app.oauth.authenticate());
    this.app.post("/container/uploadFileToS3", upload.single("file"), async (req, res, next) => {
      try {
        console.log(req.file);
        const client = new NodeQueryClient();
        const readFileFunc = await util.promisify(fs.readFile);
        console.log("file path,", req.file.path);
        const filePath = req.file.path;
        const readFileData = await readFileFunc(filePath);

        const unlinkAsync = await util.promisify(fs.unlink);
        await unlinkAsync(filePath);

        await client.uploadFileToS3(readFileData, req.body.fileName, req.body.org, req.body.fileId);
        res.status(200).json(true);
      } catch (err) {
        res.status(401).json(err.message);
      }
    });

    // console.log(this.app._router.stack)
    this.app.post("/v0.1/file/uploadFile", upload.any(), async (req: any, res, next) => {
      const fileService = new FilesService();
      console.log(req.files);
      try {
        let fileIDs = [];

        for (let i = 0; i < req.files.length; i++) {

          if (req.query.permission == "public") {
            fileIDs.push(await fileService.uploadPublicFile(req.files[i].originalname, req.files[i].path, req.query.user_id));
          }
          else {
            fileIDs.push(await fileService.uploadFile(req.files[i].originalname, req.files[i].path, req.query.user_id));
          }
        }
        console.log("returning",fileIDs);
        res.status(200).json({ fileIDs: fileIDs });
      } catch (err) {
        console.log(err);
        res.status(401).json(err);
      }
    });

    console.log("AFTER API");

    this.app.use((err: any, req: any, res: any, next: any) => {
      console.log(err);
      res.status(err.status).json(err);
    });
  }

  /**
   * Event listener for HTTP server "error" event.
   */
  public onError(error: any) {

    if (error.syscall !== "listen") {
      throw error;
    }

    const bind = typeof this.port === "string"
      ? "Pipe " + this.port
      : "Port " + this.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */
  public onListening() {
    const addr = this.server.address();
    const bind = typeof addr === "string"
      ? "pipe " + addr
      : "port " + addr.port;
    console.log("Listening on " + bind);
  }

}
