import { Request } from "express";
import { MessageService } from "../messages/MessagesService";
import { Inject } from "typescript-ioc";
import { ContextRequest, Param, Path, Security, GET, POST } from "typescript-rest";
import { UserService } from "../users/UsersService";

@Path("/v0.1/conversations")
@Security("USER")
export class ConversationsService {

  @ContextRequest private request: Request;

  @Inject private message: MessageService;
  @Inject private user: UserService

  @GET
  @Path("/history")
  public async history(@Param("conversationId") convId: string) {

    const messages = await this.message.getConversationHistory(convId);
    return messages;
  }

  @GET
  @Path("/list")
  public async list() {
    console.log("listing converstations");

    const userId = this.request.user.userId;

    //console.log("userId=",userId);
    // get all the groups
    const allGroups:any = await this.message.getAncestorGroupsForUser(userId);
    const caseGroups = await this.message.getAncestorCaseGroupsForUser(userId);
    //console.log("listing converstations 2");
    //console.log(allGroups['9e0114ba-9680-4321-ae80-1c7e6000c59b']);
    //console.log(caseGroups['9e0114ba-9680-4321-ae80-1c7e6000c59b']);

    // get conversation summary for all the groups the user is part of
    const convs = await this.message.getConversationsForGroups(allGroups);
    const readCounts = await this.message.getConversationReadCounts(userId, convs);
    //console.log(readCounts);

    const convGroups = await this.message.getGroups(convs);

    //console.log("listing converstations 3");

    // hack for now.  map group details to conversation
    const conversations = convGroups.map((g) => ({ 
      subject: g.group_name, 
      conversation_id: g.group_id,
      isCaseGroup: caseGroups[g.group_id] ? true : false,
      total_msg_count: readCounts[g.group_id].msg_count, 
      read_count: readCounts[g.group_id].read_count 
    }));

    //console.log('got convos for %s:',userId);
    //console.log(conversations);

    return conversations;
  }

  @GET
  @Path("/members")
  public async members(@Param("conversationId") conversationId: string) {

    const userIds = await this.message.getConversationMembers(conversationId);
    const users = await this.user.getUsers(userIds);
    return users;
  }



  @POST
  @Path("/updateList")
  public async updateConversationList() {
    const userId = this.request.user.userId;

    // get all the groups
    const allGroups = await this.message.getAncestorGroupsForUser(userId);
    const caseGroups = await this.message.getAncestorCaseGroupsForUser(userId);

    // get conversation summary for all the groups the user is part of
    const convs = await this.message.getConversationsForGroups(allGroups);
    const readCounts = await this.message.getConversationReadCounts(userId, convs);
    const convGroups = await this.message.getGroups(convs);

    const conversations = convGroups.map((g) => ({ 
      subject: g.group_name, 
      conversation_id: g.group_id,
      isCaseGroup: caseGroups[g.group_id] ? true : false,
      total_msg_count: readCounts[g.group_id].msg_count, 
      read_count: readCounts[g.group_id].read_count 
    }));

    return conversations;
  }

  @POST
  @Path("/updatePreferences")
  public async updateConversationPreferences(body: {conversationId: string, pref: any} ) {
    const {conversationId, pref} = body;
    const userId = this.request.user.userId;
    console.log("updating prefs for user")
    console.dir(body);
    const update = await this.message.updateConversationPreferences(conversationId, userId, pref);
    return update;
  }
}
