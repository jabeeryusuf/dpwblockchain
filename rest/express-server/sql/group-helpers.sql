WITH RECURSIVE user_groups AS (
        SELECT group_id, child_type, child_group_id
        FROM group_group_members
        WHERE group_id = '9e0114ba-9680-4321-ae80-1c7e6000c59b'
        UNION
        SELECT g.group_id, g.child_type, g.child_group_id
        FROM group_group_members g
        JOIN user_groups ug ON g.group_id = ug.child_group_id
      )
      SELECT distinct ug.child_group_id user_id
      FROM user_groups ug
         WHERE child_type = 'user';