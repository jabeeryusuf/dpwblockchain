ALTER TABLE shipments ADD COLUMN shipment_ref TEXT;  

UPDATE shipments SET shipment_ref = shipment_id;

UPDATE shipments SET shipment_id = uuid_generate_v4()

UPDATE shipment_items si 
SET shipment_Id = s.shipment_id 
FROM shipments s WHERE si.shipment_id = s.shipment_ref;

DELETE FROM shipment_items WHERE shipment_id NOT IN (SELECT distinct shipment_id FROM shipments);

ALTER TABLE shipments ALTER COLUMN shipment_id TYPE uuid USING shipment_id::uuid;
ALTER TABLE shipment_items ALTER COLUMN shipment_id TYPE uuid USING shipment_id::uuid;
