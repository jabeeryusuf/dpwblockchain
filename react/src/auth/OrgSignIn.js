import React from 'react';
import { Card,Form, InputGroup, Alert} from 'react-bootstrap';
import { connect } from 'react-redux';
import { orgActions } from '../redux/actions';
import { Link, Redirect } from 'react-router-dom'
import { FiArrowRight } from 'react-icons/fi'

import { ButtonLoader } from '../components'
import AlreadySignedIn from './AlreadySignedIn'
import AuthCard from './AuthCard'

class OrgSignIn extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      turl: '',
      submitted: null,
    };
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  
  handleSubmit(e) {
    let {dispatch} = this.props;
    let {turl} = this.state;
    console.log('handle submit %s',turl)

    // Prevent the standard form submit.
    e.preventDefault();
      
    this.setState({ submitted: turl });
    
    dispatch(orgActions.validate(turl))
  }


  render() {
    
    const { alert,validatingOrg,validOrgs,validationError} = this.props;
    const { turl,submitted } = this.state;


    // If the user has submitted and we have a valid org (from the reducer)
    if (submitted && validOrgs[submitted]) {
      let orgId = validOrgs[submitted].orgId;
      console.log('submitted redirect to org %s', orgId);
      return <Redirect to={"/" + orgId}/>
    }

    return (
     
      <AuthCard>

        <AuthCard.Header>Sign in to your Supply Chain</AuthCard.Header>
        <AuthCard.Subheader>Enter URL to access your for your supply chain:</AuthCard.Subheader>

        {alert.message &&
          <Alert variant={alert.type}> {alert.message}</Alert>
        }


        <Form>

          <InputGroup className='mb-3'>
            <InputGroup.Prepend>
              <InputGroup.Text>dpwchain.com/</InputGroup.Text>
            </InputGroup.Prepend>
            <Form.Control isInvalid={validationError} name="turl" placeholder="your-url" value={turl} onChange={(e) => this.handleChange(e)} />
          </InputGroup>

          <ButtonLoader loading={validatingOrg} block className="mb-4" type="submit" id="main" color="primary" onClick={(e) => this.handleSubmit(e)}>
            Continue {' '} <FiArrowRight /> {' '}
          </ButtonLoader>
        </Form>

        <Card className='mb-4 border-0'>
          <center>
            Don’t know your supply chain URL? <br />
            <Link to="/get-started">
              Find your supply chain
              </Link>
          </center>
        </Card>
        <br />
        <AlreadySignedIn />

      </AuthCard>

    )
  }

}

function mapStateToProps(state) {
  return { validatingOrg: state.orgs.validating,
           validOrgs: state.orgs.orgs,
           validationError: state.orgs.validationError,
           alert: state.alert,
           user: state.auth.user
          };
}

export default connect(mapStateToProps)(OrgSignIn);
