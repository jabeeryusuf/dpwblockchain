import React from 'react';
import { Alert, Card, Form, FormGroup, InputGroup  } from 'react-bootstrap';
import { FiArrowRight } from 'react-icons/fi'
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';

import { ButtonLoader as tsButtonLoader } from "../components";
import { orgActions } from "../redux/actions";
import AuthCard from "./AuthCard";

const ButtonLoader: any = tsButtonLoader;

class OrgCreate extends React.Component<any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      orgId: "",
    };
  }

  public handleChange(e: any) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  public handleSubmit(e: any) {
    const {dispatch} = this.props;
    const {orgId} = this.state;
    console.log("org create handle submit %s", orgId);

    // Prevent the standard form submit
    e.preventDefault();

    this.setState({ submitted: true });

    dispatch(orgActions.create(orgId, orgId, "supply-chain"));
  }

  public render() {
    const { org, verification, location} = this.props;
    const { alert, orgId, submitted } = this.state;
    console.log("org");
    console.log(org);
    if (!verification.verified) {
     
      return <Redirect to={{pathname: "/verify-email",
        state: { header: "Create a new Supply Chain", from: location }}}/>;
    }

    console.log("Org is");
    console.log(org);
    if (submitted && org.validated) {
      return <Redirect to={{pathname: "/" + orgId + "/signup",
        state: { from: location }}}/>;
    }

    console.log("props");
    console.log( this.props);

    return (

      <AuthCard>

        <AuthCard.Header>Create a new Supply Chain</AuthCard.Header>

        <AuthCard.Subheader>
          We're almost done!<br/>
          Please choose your Supply Chain's URL.
        </AuthCard.Subheader>

        {/*alert.message &&
          <Alert variant={alert.type}> {alert.message}</Alert>*/
        }

        <Form>

          <FormGroup className="mb-4">

            <Form.Label>Your DPWChain URL</Form.Label>
             <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text>dpworld.com</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control name="orgId"
                              placeholder="e.g.  Company or Team name"
                              value={orgId} onChange={(e: any) => this.handleChange(e)} />
                
               
              </InputGroup>
              {this.props.org.validationError &&
              <Form.Text className="text-danger">
               It looks like this OrgId already exist. Contact org Admin to create new user
              </Form.Text>
              }
            </FormGroup>

          <ButtonLoader loading={submitted && org.creating}
                        className="mb-5" type="submit" id="main" color="primary" block
                        onClick={(e: any) => this.handleSubmit(e)}>
            Next {" "} <FiArrowRight />
          </ButtonLoader>

        </Form>

        <Card className="mb-4 border-0">
            Already know your supply chain URL? <br />
            <Link to="/get-started">
              Find your supply chain
              </Link>
        </Card>
      </AuthCard>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    org          : state.orgs,
    verification : state.verification,
    alert        : state.alert,
  };
}

export default connect(mapStateToProps)(OrgCreate);
