import React from 'react'
import { orgActions } from '../redux/actions';
import { Redirect } from "react-router-dom";
import {connect} from 'react-redux'

class OrgValidate extends React.Component {

  componentDidMount() {
    //console.log('component did mount')
    //console.log(this.props)
    const { dispatch } = this.props;
    const { orgId } = this.props.match.params;

    console.log('validating ' + orgId)
    dispatch(orgActions.validate(orgId))
  }

  render() {
    const { validOrgs } = this.props;
    const { orgId } = this.props.match.params;
    const { from } = this.props.location.state || { from: { pathname: "/" } };

    let org = validOrgs[orgId];
    //console.log(org);

    if (org) {
      console.log('redirecting back to ' + from)
      return <Redirect to={from} />;
    }
    
    return <div>Validating Org</div>;
  }
}
function mapStateToProps(state) {
  return { validatingOrg: state.orgs.validating,
           validOrgs: state.orgs.orgs,
           validationError: state.orgs.validationError,
          };
}

export default connect(mapStateToProps)(OrgValidate);
