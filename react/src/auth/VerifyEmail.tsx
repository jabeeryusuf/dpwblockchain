import React, { Ref } from "react";
import { Alert, Col, Form, InputGroup, Row  } from "react-bootstrap";
import { FiArrowRight, FiMinus } from "react-icons/fi";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { ButtonLoader as _ButtonLoader } from "../components";
import { verificationActions } from "../redux/actions";

import AuthCard from "./AuthCard";

// typescript
const ButtonLoader: any = _ButtonLoader;

class _SendVerificationEmail extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      email: "",
      submitted: false,
    };
    console.log("submit e-mail");
  }

  public handleChange(event: any) {
    const { name, value } = event.target;
    this.setState({
      [name] : value,
    });
  }

  public handleSubmit = (e: any) => {
    e.preventDefault();
    this.setState({ submitted : true });

    const { email } = this.state;

    if (email && email.includes("@")) {
      this.props.dispatch(verificationActions.sendCode(email));
    }
  }

  public render() {
    const { email, submitted } = this.state;
    const { verification } = this.props;

    const emailInvalid = !!((submitted && !email) || (submitted && !email.includes("@")));
    const emailInvalidMsg = "Please enter a valid email address";
    console.log("emailInvalid = %s", emailInvalid);

    if (submitted && verification.sent) {
      console.log("ask for code");
    }

    return (
      <Form>

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text>@</InputGroup.Text>
          </InputGroup.Prepend>

          <Form.Control
            isInvalid={emailInvalid}
            type="email"
            value={email}
            name="email"
            id="email"
            placeholder="Enter your email address"
            onChange={(e: any) => this.handleChange(e)} />

          {emailInvalid ? <Form.Control.Feedback type="invalid">{emailInvalidMsg}</Form.Control.Feedback> : ""}

        </InputGroup>

        <ButtonLoader loading={verification.sending} className="mb-5" type="submit" id="main" 
                      color="primary" block onClick={this.handleSubmit}>
          Send {" "}
          <FiArrowRight />
        </ButtonLoader>

      </Form>
    );
  }
}

const SendVerificationEmail = connect(mapStateToProps)(_SendVerificationEmail);

const DIGITS = 6;

class VerifyEmail extends React.Component<any, any> {

  public endInput: any;
  public inputs: any;

  constructor(props: any) {
    super(props);

    // reset verification
    this.props.dispatch(verificationActions.start());

    this.endInput = React.createRef();
    const digits = Array(DIGITS).fill("");

    this.state = {
      digits,
      submitted: false,
    };

    this.inputs = [...Array(DIGITS)];
  }

  public handleDigitEntry = (e: any, i: any) =>  {
    const {digits} = this.state;

    e.preventDefault(); // Prevent form submission if button present
    const num = Number(e.key);

    if (Number.isInteger(num) && num >= 0 && num <= 9) {

      console.log("setting digit %i to %s", i, e.key);
      digits[i] = e.key;
      console.log(digits);

      this.setState({
        digits,
      });

      if (i < (DIGITS - 1)) {
        this.inputs[i + 1].focus();
      } else if (this.endInput) {
        this.endInput.current.focus();
      }

    }
  }

  public handleSubmit(e: any) {

    const {verification} = this.props;
    const {digits} = this.state;
    this.setState({ submitted : true });

    if (!digits.includes("")) {
      const code = digits.join("");
      this.props.dispatch(verificationActions.verify(verification.email, code));
    }
  }

  public renderCode() {
    const {verification} = this.props;
    const {submitted, digits} = this.state;

    console.log(this.endInput);

    return (
      <>
        <p>Enter your code:</p>
        <Row className="mb-5">
          <Col sm={{ offset: 2, span: 8 }}>
            <InputGroup >
              {
                digits.map((d: any, i: any) => (
                  <React.Fragment key={i}>
                    {i === (DIGITS / 2) && <FiMinus className="verification-digit-dash" /> }
                    <Form.Control
                      className="verification-digit" type="number" key={i}
                      isInvalid={!!(submitted && !digits[i])}
                      ref={((input: any) => {this.inputs[i] = input; }) as any }
                      value={digits[i]}
                      onChange={ () => {} /*warning removal */ } 
                      onKeyPress={(e: any) => this.handleDigitEntry(e, i)} />
                  </React.Fragment>),
                )
              }

              {submitted && digits.includes("") && 
                <Form.Control.Feedback type="invalid">Please enter all digits</Form.Control.Feedback>}

            </InputGroup>
          </Col>
        </Row>

        <ButtonLoader loading={verification.verifying}
                      ref={this.endInput}
                      className="mb-5" type="submit" id="main" color="primary" block
                      onClick={(e: any) => this.handleSubmit(e)}>
          Verify {" "}
          <FiArrowRight />
        </ButtonLoader>

      </>
    );
  }

  public render() {
    const { alert, verification } = this.props;
    let { header, from } = this.props.location.state;

    if (verification.verified) {
      from = from ? from : { pathname: "/" };

      console.log("redirecting to ", from);
      console.log(from);

      return <Redirect to={from} />;
    }

    return (

      <AuthCard>
        <h2 className="my-3">{header}</h2>
        <p className="mb-5">Lets first verify your <b>email address</b></p>

        {alert.message &&
          <Alert variant={alert.type}> {alert.message}</Alert>
        }

        {verification.sent ? this.renderCode() : <SendVerificationEmail />}
      </AuthCard>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    verification : state.verification,
    alert        : state.alert,
  };
}

export default connect(mapStateToProps)(VerifyEmail);
