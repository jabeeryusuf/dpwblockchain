import React from 'react';
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { alertActions, authActions } from '../redux/actions';
import { Row, Form, FormGroup, FormControl, Alert } from 'react-bootstrap';
import { Link } from "react-router-dom";

import { ButtonLoader } from '../components';

import AuthCard from './AuthCard'

class UserSignIn extends React.Component {

  constructor(props) {
    super(props);

    // reset login status
    this.props.dispatch(authActions.signOut());
    this.props.dispatch(alertActions.clear());

    this.state = {
      email: '',
      password: '',
      submitted: false,
    };


  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch, match, validOrgs } = this.props;

    if (email && password && validOrgs[match.params.orgId]) {
      dispatch(authActions.signIn(match.params.orgId, email, password));
    }
  }

  render() {
    const { loggingIn, user, alert, validOrgs, match } = this.props;
    const { email, password, submitted } = this.state;

    let org = validOrgs[match.params.orgId];


    //console.log(this.props);
    // Check if we have a valid org
    if (!org) {
      console.log('entered directly into signin without a valid org');
      return <Redirect to="/" />;
    }


    const { from } = this.props.location.state || { from: { pathname: '/' + org.org_id } };

    // If we have a user then we're logged in
    if (user) {
      console.log('redirecting to %j', from);
      return <Redirect to={from} />;
    }

    let emailInvalid = submitted && !email;
    let emailInvalidMsg = 'Must provide a valid e-mail'

    let passwordInvalid = submitted && !password;
    let passwordInvalidMsg = 'Must provide a valid e-mail'

    return (
      <AuthCard>
        <h2 className="my-3">Sign in to {org.org_id}</h2>

        <p className="mb-3">dpwchain.com/<b>{org.org_id}</b></p>

        {alert.message &&
          <Alert variant={alert.type}>{alert.message}</Alert>
        }

        <Form>

          <FormGroup>
            <Form.Label>E-mail</Form.Label>
            <FormControl isInvalid={emailInvalid} type="email" value={email} name="email" id="email" placeholder="E-mail" onChange={(e) => this.handleChange(e)} />
            {emailInvalid ? <Form.Control.Feedback type="invalid">{emailInvalidMsg}</Form.Control.Feedback> : ''}
          </FormGroup>

          <FormGroup>
            <Form.Label>Password</Form.Label>
            <FormControl isInvalid={passwordInvalid} type="password" value={password} name="password" id="password" placeholder="Password" onChange={(e) => this.handleChange(e)} />
            {passwordInvalid ? <Form.Control.Feedback type="invalid">{passwordInvalidMsg}</Form.Control.Feedback> : ''}
          </FormGroup>
          <br />

          <Row className='justify-content-between'>
              <Link to={'/' + org.org_id + '/signup'}>Register for an account</Link>
              <Link to={'/' + org.org_id + '/reset-password'}>Forgot your Password?</Link>
          </Row>

          <br />
          <br />


          <FormGroup>
            <ButtonLoader loading={loggingIn} type="submit" block onClick={(e) => this.handleSubmit(e)} color="primary" >
              sign in
            </ButtonLoader>
          </FormGroup>


        </Form>


      </AuthCard>
    )
  }

}


function mapStateToProps(state) {
  const { loggingIn, user } = state.auth;
  const validOrgs  = state.orgs.orgs;
  const alert = state.alert;
  return {
    validOrgs,
    loggingIn,
    user,
    alert
  };
}

export default connect(mapStateToProps)(UserSignIn);