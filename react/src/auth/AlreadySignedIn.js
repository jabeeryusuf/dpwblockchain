import React from 'react';
import { ListGroup, ListGroupItem, Row,Badge,Col,Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import { authActions } from '../redux/actions';
import { FaDatabase} from 'react-icons/fa';
import { Redirect} from 'react-router-dom'

class AsiListItem extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      submitted: false,
    };
  }

  handleSubmit(e) {
    // Prevent the standard form submit
    e.preventDefault();

    let {dispatch,user} = this.props;
   
    this.setState({ submitted: true });
    
    dispatch(authActions.change(user.user_id))
  }


  render () {
    let {user} = this.props;
    let {submitted} = this.state;

    // Move forward if If we're already logged in then we can move to the next area
    if (submitted) {
      let orgId = user.org_id;
      console.log('ASI redirect to org %s', orgId);
      return <Redirect to={"/" + orgId}/>
    }

    console.log(user);

    return (
      <ListGroupItem>
        <Row className="d-flex align-items-center h-100">
          <Col xs="2">
            <h1><Badge variant="info"><FaDatabase/></Badge></h1>
          </Col>

          <Col xs="7">
            <h5>{user.org_id}</h5>
          </Col>

          <Col xs="2">
            <div >
              <Button className="mr-3" variant="outline-primary" onClick={(e) => this.handleSubmit(e)}>Launch</Button>
            </div>
          </Col>
        </Row>
      </ListGroupItem>
    );
  }
}



class AlreadySignedIn extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      turl: '',
      submitted: false,
    };
  }

  render() {
    
    const { dispatch,allUsers } = this.props;

    if (! (allUsers && allUsers.length > 0) ) {
      return <></>;
    } 


    return (
      

      <>      
        <center><h6>You're already signed into these supply chains:</h6></center>
        <ListGroup>

          {allUsers.map((user,i) => 
            <AsiListItem key={i} user={user} dispatch={dispatch} />
          )}
        </ListGroup>
      </>
    )
  }
}


function mapStateToProps(state) {
  return {allUsers: state.auth.allUsers};
}

export default connect(mapStateToProps)(AlreadySignedIn);
