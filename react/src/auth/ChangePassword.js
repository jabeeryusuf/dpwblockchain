import React from 'react';
import { Form, FormGroup, FormControl, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import AuthCard from './AuthCard';
import { getApiClient } from '../redux/utils'

class UpdatePassword extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			confirmPassword: '',
			errorMessages: [],
			successMessage: ''
		}
	}
	async handleSubmit(e) {
		e.preventDefault();
		let errorList = [];
		if (this.state.confirmPassword === this.state.password && this.state.email.length) {
			let client = getApiClient();
			let res = await client.changePassword(this.state.email, this.state.password, this.props.match.params.code);
			if (res.error) {
				errorList.push(res.error);
			}
		}
		else {
			errorList = ['Passwords do not match'];
		}
		console.log('errors!', errorList)
		if (errorList.length > 0) {
			this.setState({
				errorMessages: errorList
			})
		}
		else {
			this.setState({
				successMessage: 'Password successfully reset! Login to continue'
			})
		}
	}
	handleChange(e) {
		console.log(this.props)
		if (e.target.id === 'email') {
			this.setState({
				email: e.target.value
			})
		}
		if (e.target.id === 'name') {
			this.setState({
				name: e.target.value
			})
		}
		if (e.target.id === 'password') {
			this.setState({
				password: e.target.value
			})
		}
		if (e.target.id === 'confirmPassword') {
			this.setState({
				confirmPassword: e.target.value
			})
		}
	}
	renderErrors() {
		if (this.state.errorMessages) {
			return (
				<ul style={{ listStyle: 'none' }}>
					{
						this.state.errorMessages.map((val) => {
							if (val === 'This invitation has already been accepted. Please login to continue') {
								return (
									<li style={{ color: 'red' }}>
										This invitation has already been accepted. Please
										<Link to='/signup'> Login </Link>
										to continue
									</li>
								)
							}
							else {
								return (
									<li style={{ color: 'red' }}>
										{val}
									</li>
								)
							}
						})
					}

				</ul>
			)
		}
		else {
			return
		}
	}
	renderButton() {
		if (this.state.successMessage.length > 0) {
			return (
				<>
					<h5 style={{ color: 'green' }}>{this.state.successMessage}</h5>
					<Link style={{ color: 'white' }} to={'/signin'}>
						<Button type="submit" block color="primary" >
							Login
						</Button>
					</Link >
				</>
			)
		}
		else {
			return (
				<Form>
					<FormGroup>
						<Form.Label>E-mail</Form.Label>
						<FormControl type="email" value={this.state.email} name="email" id="email" placeholder="E-mail" onChange={(e) => this.handleChange(e)} />
					</FormGroup>
					<FormGroup>
						<Form.Label>New Password</Form.Label>
						<FormControl type="password" value={this.state.password} name="password" id="password" placeholder="Password" onChange={(e) => this.handleChange(e)} />
					</FormGroup>
					<FormGroup>
						<Form.Label>Confirm Password</Form.Label>
						<FormControl type="password" value={this.state.confirmPassword} name="password" id="confirmPassword" placeholder="Password" onChange={(e) => this.handleChange(e)} />
					</FormGroup>
					<FormGroup>
						<Button type="submit" block onClick={(e) => this.handleSubmit(e)} color="primary" >
							Reset Password
                    </Button>
					</FormGroup>
				</Form>
			)

		}
	}
	render() {
		return (
			<AuthCard>
						<h3> Reset your password </h3>
						{this.renderErrors()}
						{this.renderButton()}
			</AuthCard>
		)
	}
}


export default UpdatePassword; 