import React from 'react';
import { Link } from 'react-router-dom'
import { ListGroup, ListGroupItem, Row,Badge,Col} from 'react-bootstrap';
import { FaPlus,FaSearch,FaArrowRight } from 'react-icons/fa';

import { connect } from 'react-redux';
import AuthCard from './AuthCard'
import AlreadySignedIn from './AlreadySignedIn';
 
function GsListItem(props) {
  return (
    <ListGroupItem action as={Link} to="/create" href='/create'>
      <br/>
      <Row className="d-flex align-items-center h-100">
        <Col xs="2">
          <h1><Badge variant={props.color}>{props.icon}</Badge></h1>
        </Col>
        <Col xs="8">
          <h5>{props.title}</h5>
          {props.desc}
        </Col>
        <Col xs="2" >
          <h3><FaArrowRight/></h3>
        </Col>
      </Row>
      <br/>

    </ListGroupItem>
  );
}

class GettingStarted extends React.Component {

  constructor(props) {
    super(props);
    
    
    
    this.state = {
      turl: '',
      submitted: false,
    };
  }


  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  
  handleSubmit(e) {

    // Prevent the standard form submit
    e.preventDefault();
      
    this.setState({ submitted: true });
  }


  render() {
    // const { alert,signingIn } = this.props;
    // const { turl } = this.state;
    
    return (
      <>
      <AuthCard>
            <h2 className="my-3">Start with a Supply Chain:</h2>

            <ListGroup>

              <GsListItem icon={<FaSearch/>} color="success"
                          title="Find your Supply Chain"
                          desc="Join or sign into an existing Supply Chain" />

              <GsListItem icon={<FaPlus/>} color="primary"
                          title="Create a new Supply Chain"
                          desc="Get your company on DPWChain" />


            </ListGroup>
            <br/>

            <AlreadySignedIn/>


      </AuthCard>

      </>

    )
  }

}


function mapStateToProps(state) {
  const alert = state.alert; 
  return {
    alert,
  };
}

export default connect(mapStateToProps)(GettingStarted);
