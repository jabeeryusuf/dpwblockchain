import React from 'react';
import { connect } from 'react-redux';
import { socketActions } from '../redux/actions';
import {Redirect} from "react-router-dom";

class ConnectSocket extends React.Component {

  componentDidMount() {
    this.props.connectSocket(this.props.user.user_id);
  }

  render() {
    const { connected,authenticated } = this.props;
    const { from } = this.props.location.state || { from: { pathname: "/" } };

    if (connected  && authenticated) {
      return <Redirect to={from} />;
    }
    
    return <div>Connecting to Socket</div>;
  }
}

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    port: state.socket.port,
    connected: state.socket.connected,
    authenticated: state.socket.authenticated,
  }
}

const mapDispatchToProps = (dispatch) => ({
  connectSocket: (user_id) => dispatch(socketActions.connectSocket(user_id)),
});

export default connect(mapStateToProps,mapDispatchToProps)(ConnectSocket);


      