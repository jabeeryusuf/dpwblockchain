import React from 'react'
import OrgSignIn from './OrgSignIn'
import UserSignIn from './UserSignIn'
import Refresh from './Refresh'
import GetStarted from './GetStarted'
import UserSignUp from './UserSignUp'
import OrgCreate from './OrgCreate';
import {connect} from 'react-redux'
import PrivateRoute from '../auth/PrivateRoute'
import App from '../app/App'
import ConnectSocket from './ConnectSocket'
import InviteSignUp from './InviteSignUp';
import InviteError from './InviteError';
import ResetPassword from './ResetPassword';
import ChangePassword from './ChangePassword';
import VerifyEmail from './VerifyEmail';
import { Redirect, Route,withRouter,Switch } from "react-router-dom";
import AuthPage from './AuthPage';
import OrgValidate from './OrgValidate'


let mapOrgToProps = state => ({orgs : state.orgs.orgs })

let OrgARoute = withRouter(connect(mapOrgToProps)( ({orgs, ...props}) => {
  return <Route path={props.path} render={(cProps) => {
    //console.log('in aroute');
    //console.log(props);
    const { orgId } = props.match.params;
    let org = orgs[orgId];
    let Component = props.component;
    //console.log(Component);

    return !org 
    ? <Redirect to={{pathname: '/validate/' + orgId,state: { from: props.location }}} />
    :  <AuthPage>
        <Component {...cProps} />  
      </AuthPage>

  }} />
}))


let ARoute = withRouter((props) => {
  return <Route path={props.path} render={(cProps) => {
    //console.log('in aroute');
    //console.log(props);

    let Component = props.component;
    //console.log(Component);

    return (
      <AuthPage>
        <Component {...cProps} />  
      </AuthPage>
    );
  }} />
})


// Main app wrapper.  Wraps everything in PrivateRoute except for the login page
class AuthApp extends React.Component {

  renderOrg(props) {
    let {match} = props;
    //console.log('AuthAppRouter');
    //console.log(props);
    
    return (
      <Switch>
        {/* user signin to a specific org */}
        <OrgARoute path={match.path + "/invite-signup"} component={InviteSignUp} />
        <OrgARoute path={match.path + "/invite-error"} component={InviteError} />
        <OrgARoute path={match.path + "/reset-password"} component={ResetPassword} />
        <OrgARoute path={match.path + "/change-password/:code"} component={ChangePassword} />
        <OrgARoute path={match.path + "/signin"} component={UserSignIn} />
        <OrgARoute path={match.path + "/refresh"} component={Refresh} />
        <OrgARoute path={match.path + "/signup"} component={UserSignUp} />
        <OrgARoute path={match.path + "/connect-socket"} component={ConnectSocket} />

        {/* private routes have a base org id */}
        <PrivateRoute path={match.path + "/"} component={App} match={props.match}/>
      </Switch>
    );
  }
  
  render() {
 
    //console.log('AuthApp')
    //console.log(this.props);

    return (
      <Switch>
        <ARoute path="/" exact={true} component={OrgSignIn}/>
        <ARoute path="/signin" component={OrgSignIn} /> 
        <ARoute path="/get-started" component={GetStarted} />
        <ARoute path="/create" component={OrgCreate} />
        <ARoute path="/verify-email" component={VerifyEmail} />
        <ARoute path="/validate/:orgId" component={OrgValidate} />
        <Route path="/:orgId" render={this.renderOrg} /> 
      </Switch>
    )
  }
}


function mapStateToProps(state) {
  return { org: state.auth.org }
}

export default withRouter(connect(mapStateToProps)(AuthApp));

