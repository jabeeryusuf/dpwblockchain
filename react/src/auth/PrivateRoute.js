
import React from 'react'
import {connect} from 'react-redux'

import {Route,Redirect} from "react-router-dom";

class PrivateRoute extends React.Component {


  render() {
    let { socketConnected,validOrgs, match, user, token, component : Component, ...rest } = this.props;
    
    let now = new Date();

    // Assume we don't need to redirect
    let redirectPath = null;

    //console.log('in private route');
    //console.log(this.props);
    //console.log(org);

    // I'm entering this path with a a org URL.  This should  never trigger
    let params = match && match.params;
    if (!(params && params.orgId)) {
      redirectPath = '/signin';
    }

    // if there's not user object we're not logged in
    else if (!user) {
      
      // If the org has been validated through the first signin step
      if  (validOrgs[params.orgId]) {
        redirectPath = '/' + params.orgId + '/signin';
      }
      // Someone types a URL that which has never been logged in.  Just revert to signin so 
      // they can validate the org
      else {
        redirectPath = '/validate/' + params.orgId;
      }
    }

    // we are logged in but the URL isn't correct then we're switching.  
    // Simply redirect to that new org and start this cycle over
    else if (user.org_id !== params.orgId) {
      redirectPath = '/' + user.org_id;
    }

    // We are logged in but the token is is expired
    else if (! (token &&  (now < token.expires)) ) {
      redirectPath = '/' + params.orgId + '/refresh'
    }

    // We are are logged in with a valid token but we do not have our socket connection
    else if (!socketConnected) {
      redirectPath = '/' + params.orgId + '/connect-socket'
    }



    //console.log('redirect = %s',redirectPath)
    //console.log(rest)

    return (
      <Route {...rest} render={
        props => redirectPath === null
          ? (<Component {...props} />) 
          : (<Redirect to={{pathname: redirectPath,state: { from: props.location }}} />) }
      /> )
  }
}

function mapStateToProps(state) {
  return { validOrgs: state.orgs.orgs,
           user: state.auth.user,
           token: state.auth.token, 
           socketConnected: state.socket.connected}
	
}

export default connect(mapStateToProps)(PrivateRoute);