import React from 'react';
import { Redirect } from "react-router-dom";
import { Alert } from 'react-bootstrap'
import { connect } from 'react-redux';
import qs from 'qs'

import { verificationActions } from '../redux/actions';

import AuthCard from './AuthCard'

class InviteSignUp extends React.Component {

	parseParams(props) {
		let search = props.location.search;

		if (!search || !search.startsWith('?')) {
			return {};
		}

		return qs.parse(search.slice(1));
	}

	constructor(props) {
		super(props);

		let params = this.parseParams(this.props);
		if (params.email && params.code) {
			props.dispatch(verificationActions.verify(params.email,params.code));
		}
	}


	renderError(message) {
		return (
			<AuthCard>
			<Alert variant="danger">
				{message}
			</Alert>
			</AuthCard>
		)
	}

	render() {

		let { verification, validOrgs,match } = this.props;
		let params = this.parseParams(this.props);
		let org = validOrgs[match.params.orgId];

		console.log(verification)
		console.log(params)
		console.log(validOrgs)
		
		// This shouldn't happen
    if (!org || !params.email || !params.code) {
			return this.renderError("Error in Link");
		}

		// When verification is complete 
    if (verification.verified) {
      let to = '/' + org.org_id + '/signup';
      return <Redirect to={to} />;
		}

		return (
			<AuthCard>
				<Alert> Verifying email ... </Alert>
			</AuthCard>
		)
	}
}

function mapStateToProps(state) {
  return {
		verification: state.verification,
		validOrgs: state.orgs.orgs
	}
}

export default connect(mapStateToProps)(InviteSignUp)