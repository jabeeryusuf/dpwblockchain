import React from 'react';
import { Container } from 'react-bootstrap';

import { Header } from '../layout';

class AuthPage extends React.Component {

  render() {
    return ( 
      <div className="auth-page">
        <Header/>
  
        <Container >
          <div className="my-5">
            {this.props.children}
          </div>
        </Container>

      </div>
    )
  }
}

export default AuthPage;
