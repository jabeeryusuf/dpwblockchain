import React from 'react';
import { connect } from 'react-redux';
import { authActions } from '../redux/actions';
import { Redirect } from "react-router-dom";

class Refresh extends React.Component {

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(authActions.refresh());
  }

  render() {
    const { user, token } = this.props;
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    const now = new Date();
    console.log("REFRESH:");
    console.log(this.props);

    if (user && (token.expires > now)) {
      return <Redirect to={from} />;
    }
    
    return <div>Logging In</div>;
  }
}

function mapStateToProps(state) {
  const { user,org, token } = state.auth;
  return {
    org,
    user,
    token
  };
}

export default connect(mapStateToProps)(Refresh);


      