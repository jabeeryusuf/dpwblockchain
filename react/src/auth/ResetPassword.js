import React from 'react';
import { Card, Form, FormGroup, Col, FormControl, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import AuthPage from './AuthPage';
import { getApiClient } from '../redux/utils'

class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      errorMessages: [],
      successMessage: ''
    }
  }
  async handleSubmit(e) {
    e.preventDefault();
    
    let client = getApiClient();
    let res = await client.resetPassword(this.state.email,this.props.match.params.orgId);
    
    if (res.error) {
      this.setState({
        errorMessages: [res.error]
      })
    }
    else {
      this.setState({
        errorMessages: [],
        successMessage: 'Email sent to ' + this.state.email
      })
    }
  }
  handleChange(e) {
    if (e.target.id === 'email') {
      this.setState({
        email: e.target.value
      })
    }
  }
  renderErrors() {
    if (this.state.errorMessages) {
      return (
        <ul style={{ listStyle: 'none' }}>
          {
            this.state.errorMessages.map((val) => {
              if (val === 'This invitation has already been accepted. Please login to continue') {
                return (
                  <li style={{ color: 'red' }}>
                    This invitation has already been accepted. Please
										<Link to='/signin'> Login </Link>
                    to continue
									</li>
                )
              }
              else {
                return (
                  <li style={{ color: 'red' }}>
                    {val}
                  </li>
                )
              }
            })
          }

        </ul>
      )
    }
    else {
      return
    }
  }
  renderButton() {
    if (this.state.successMessage.length > 0) {
      return (
        <>
          <h5 style={{ color: 'green' }}>{this.state.successMessage}</h5>
          <Link style={{ color: 'white' }} to={this.props.match.params.orgId + '/signin'}>
            <Button type="submit" block color="primary" >
              Login
						</Button>
          </Link >
        </>
      )
    }
    else {
      return (
        <Form>
          <FormGroup>
            <Form.Label>E-mail</Form.Label>
            <FormControl type="email" value={this.state.email} name="email" id="email" placeholder="E-mail" onChange={(e) => this.handleChange(e)} />
          </FormGroup>
          <FormGroup>
            <Button type="submit" block onClick={(e) => this.handleSubmit(e)} color="primary" >
              Send Email
                    </Button>
          </FormGroup>
        </Form>
      )

    }
  }
  render() {
    return (
      <AuthPage>
        <Card>
          <Col sm={{ span: 6, offset: 3 }}>
            <h3> Reset Your Password </h3>
            {this.renderErrors()}
            {this.renderButton()}
          </Col>
        </Card>
      </AuthPage >
    )
  }
}


export default ResetPassword; 