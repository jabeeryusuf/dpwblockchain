import React from 'react';
import { Card, Col} from 'react-bootstrap';
import AuthPage from './AuthPage';
import { getApiClient } from '../redux/utils'

class InviteError extends React.Component {
  componentDidMount(){
    	let client = getApiClient();
		  client.deleteInvitation(this.props.match.params.verifyCode);
  }
	render() {
		return (
			<AuthPage>
				<Card>
					<Col sm={{ span: 6, offset: 3 }}>
						<h3> Thank You! </h3>
            <p> We have deleted the invitation from our database.  And we will contact the sender</p>
					</Col>
				</Card>
			</AuthPage >
		)
	}
}


export default InviteError; 