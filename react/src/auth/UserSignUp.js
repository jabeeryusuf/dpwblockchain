import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../redux/actions';
import { Redirect } from "react-router-dom";
import { Form, FormControl, FormGroup, Alert } from 'react-bootstrap';
import { ButtonLoader } from '../components';

import AuthCard from './AuthCard';

class UserSignUp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userInfo: {
        name: '',
        email: '',
        password: '',
        confirmPass: '',
      },

      submitted: false
    };
    this.do = true;;
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { userInfo } = this.state;
    this.setState({
        userInfo: {
            ...userInfo,
            [name]: value
        }
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true});

    const { userInfo } = this.state;
    const { match, dispatch,validOrgs,verification } = this.props;

    let org = validOrgs[match.params.orgId];

    if (userInfo.name && userInfo.password && userInfo.confirmPass && (userInfo.password === userInfo.confirmPass) ) {
      console.log('SIGNING UP');
      console.log(org.org_id, verification.email,userInfo.name,userInfo.password)
      dispatch(userActions.create(org.org_id, verification.email,userInfo.name,userInfo.password));
    }
  }

  render() {
    const { validOrgs, registering, verification,user,
            loggingIn, alert, match,location} = this.props;
    const { userInfo, submitted } = this.state;

    //console.log('UserSignUp');
    //console.log(this.props);

    let nameInvalid = submitted && !userInfo.name;
    let nameInvalidMsg = 'Field cannot be empty';

    //let emailInvalid = (submitted && !userInfo.email) || (submitted && !userInfo.email.includes('@'));
    //let emailInvalidMsg = 'Must be a valid email address';

    let passwordInvalid = submitted && !userInfo.password;
    let passwordInvalidMsg = 'Field cannot be empty';

    let confirmPassInvalid = (submitted && !userInfo.confirmPass) || (submitted && !(userInfo.confirmPass === userInfo.password));
    let confirmPassInvalidMsg = 'Passwords must be matching';


    // Ensure we're in an org context.  User would have to reach this through manual entry
    let org = validOrgs[match.params.orgId];
    if (!org) {
      return <Redirect to="/"/>
    }

    if (user) {
      return <Redirect to={"/" + org.org_id}/>
    }

    // To proceed we need a validated email.  Entry through invite or org creation will set this up
    if (!verification.verified) {
      return <Redirect to={{pathname: "/verify-email", 
                       state: { header: "Create a new Supply Chain", from: location }}}/>
    }

    
    return (
      
      <AuthCard>
        <AuthCard.Header>Register for an account</AuthCard.Header>
        <AuthCard.Subheader>Register for an account on DPWChain.com/<b>{org.org_id}</b></AuthCard.Subheader>

        {alert.message &&
          <Alert dismissible variant={alert.type}>{alert.message}</Alert>
        }

        <Form>
          <FormGroup>
            <Form.Label>Verified E-Mail</Form.Label>
            <FormControl
              readOnly={true}
              type="email"
              defaultValue={verification.email}
              name="email"
              id="email"
              placeholder="Verified E-mail" />
          </FormGroup>
          <FormGroup>
            <Form.Label>Name</Form.Label>
            <FormControl isInvalid={nameInvalid}
              type="text"
              value={userInfo.name}
              name="name"
              id="name"
              placeholder="Enter your name"
              onChange={(e) => this.handleChange(e)} />
            {nameInvalid ? <Form.Control.Feedback type="invalid">{nameInvalidMsg}</Form.Control.Feedback> : ''}
          </FormGroup>
          <FormGroup>
            <Form.Label>Password</Form.Label>
            <FormControl isInvalid={passwordInvalid}
              type="password"
              value={userInfo.password}
              name="password"
              id="password"
              placeholder="Enter a password"
              onChange={(e) => this.handleChange(e)} />
            {passwordInvalid ? <Form.Control.Feedback type="invalid">{passwordInvalidMsg}</Form.Control.Feedback> : ''}
          </FormGroup>
          <FormGroup>
            <Form.Label>Confirm Password</Form.Label>
            <FormControl isInvalid={confirmPassInvalid}
              type="password"
              value={userInfo.confirmPass}
              name="confirmPass"
              id="confirmPass"
              placeholder="Confirm password"
              onChange={(e) => this.handleChange(e)} />
            {confirmPassInvalid ? <Form.Control.Feedback type="invalid">{confirmPassInvalidMsg}</Form.Control.Feedback> : ''}
          </FormGroup>

          <br />

          <FormGroup>
            <ButtonLoader block loading={registering || loggingIn} onClick={(e) => this.handleSubmit(e)} color="primary" disabled={loggingIn} >
              register
            </ButtonLoader>
          </FormGroup>
        </Form>

      </AuthCard>
    )
  }
}

function mapStateToProps(state) {
  const { registering, registered } = state.registration;
  const { loggingIn, user } = state.auth;
  const validOrgs = state.orgs.orgs;
  const alert = state.alert;
  const verification = state.verification;

  return {
    validOrgs,
    registering,
    registered,
    loggingIn,
    user,
    alert,
    verification
  };
}

export default connect(mapStateToProps)(UserSignUp);