import React from 'react';
import { Card, Row,Col } from 'react-bootstrap';

class AuthCard extends React.Component {

  static Header = (props) => <h2 className="my-3">{props.children}</h2>;
  static Subheader = (props) => <p className='mb-5'>{props.children}</p>;

  render() {
    return ( 

      <Row>
        <Col sm={{ span: 8, offset: 2 }} >
          <Card className="auth-card my-5 p-4">
            <Row>
              <Col sm={{ span: 8, offset: 2 }} >
                {this.props.children}
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }

  

}

export default AuthCard;