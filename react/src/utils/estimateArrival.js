/**
 * takes the distance from calculateDistance() and computes the estimated amount
 * of days to the next port based on average speed of ship
 */
const estimateArrival = function (speed, lat1, lng1, lat2, lng2) {
    let days;

    if (speed === 0) {
        speed = 15;
    }
    let distance = calculateDistance(lat1, lng1, lat2, lng2);
    let minutes = (60 * distance) / speed;
    days = Math.ceil(minutes / 24 / 60);
    // days = (minutes / 24 / 60)

    return days;
}



/**
 * calculates distance between two specified points in 
 * nautical miles
 * @param {*} lat1 
 * @param {*} lng1 
 * @param {*} lat2 
 * @param {*} lng2 
 */
function calculateDistance(lat1, lng1, lat2, lng2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLng = deg2rad(lng2 - lng1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLng / 2) * Math.sin(dLng / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c * 1.852; // Distance in nautical miles
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

export default estimateArrival;

