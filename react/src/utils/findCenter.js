//this is a temporary bad function to find the center point
const findCenter = (arr)=>{
    let latSum = 0;
    let lngSum = 0;
    for (let i=0; i<arr.length;i++){
        if (arr[i]) {
            lngSum +=parseInt(arr[i].lng);
            latSum += parseInt(arr[i].lat);
        }
    }
    return {lat:latSum/arr.length,lng:lngSum/arr.length}
}

export default findCenter;