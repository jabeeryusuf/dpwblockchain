export const aggregateByPO = (arr) => {
    let poDict= {};
    let flagged ={};
    for(let i in arr){
        let hash = String(arr[i].purchase_order_id) + '_' + String(arr[i].container_id);
        if(poDict[hash]){
            poDict[hash].quantity = parseInt(poDict[hash].quantity) + parseInt(arr[i].quantity);
            if(parseInt(arr[i].volume)){
                poDict[hash].volume += parseFloat(arr[i].volume);
            }
            else{
                flagged[hash] = true;
            }
        }
        else{
            if(parseFloat(arr[i].volume)){
                poDict[hash] = arr[i];
            }
            else{
                arr[i].volume =0;
                poDict[hash] = arr[i];
                flagged[hash] =true;
            }
        }
    }
    console.log(poDict)
    let poLocations = [];
    for(let key in poDict){
        if(flagged[key]){
            if(parseFloat(poDict[key].volume)){
                poDict[key].volume = parseFloat(poDict[key].volume);
                poDict[key].volume += " *";
            }
            else{
                poDict[key].volume = "Unknown Volume";
            }
        }
        else{
            poDict[key].volume = parseFloat(poDict[key].volume);
        }
        poLocations.push(poDict[key]); 
    }
    return poLocations;
}
