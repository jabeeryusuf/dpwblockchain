const { Item } = require('tw-api-common');

const aggregateByDestination = function (POLocations, holding) {
	let qtyDict = {};
	let volDict = {};
	let flagged = {};
	let locations = [];
  let holdingLocs = { 'port&SZX': true, 'port&XMN': true, 'port&NGB': true, 'port&SHA': true, 'port&TAO': true, 'port&TSN': true };
	for (let i in POLocations) {
		let container = POLocations[i].dst_container;
		if (holding === true) {
			if (POLocations[i].dst_container && holdingLocs[POLocations[i].dst_container] && POLocations[i].volume !== 'Unknown Volume') {
				if (container in qtyDict) {
					qtyDict[container] += parseInt(POLocations[i].quantity);
					if (parseInt(POLocations[i].volume)) {
						volDict[container] += parseInt(POLocations[i].volume);
					}
					else {
						flagged[container] = true
					}
				}
				else {
					qtyDict[container] = parseInt(POLocations[i].quantity);
					if (parseInt(POLocations[i].volume)) {
						volDict[container] = POLocations[i].volume;
					}
					else {
						flagged[container] = true;
					}
					let item = Object.create(Item.prototype);
					let my_itm = Object.assign(item, POLocations[i]);
					locations.push(my_itm);
				}
			}
		}
		else {
			if (POLocations[i].dst_container && !holdingLocs[POLocations[i].dst_container]) {
				if (container in qtyDict) {
					qtyDict[container] += parseInt(POLocations[i].quantity);
					if (parseFloat(POLocations[i].volume)) {
						volDict[container] += parseFloat(POLocations[i].volume);
					}
					else {
						flagged[container] = true
					}
				}
				else {
					qtyDict[container] = parseInt(POLocations[i].quantity);
					if (parseFloat(POLocations[i].volume)) {
						volDict[container] = parseFloat(POLocations[i].volume);
					}
					else {
						volDict[container] = 0;
						flagged[container] = true
					}
					let item = Object.create(Item.prototype);
					let my_itm = Object.assign(item, POLocations[i]);
					locations.push(my_itm);
				}

			}
		}
  }
	for (let i in locations) {
		let container = locations[i].dst_container;
		locations[i].quantity = qtyDict[container];
		let flag = flagged[container] ? " *" : "";
		locations[i].volume = volDict[container]+ flag;
	}
	return locations;
}
const aggregateByLocation = function (POLocations, byParams) {
	let qtyDict = {};
	let volDict = {};
	let flagged = {};
	let locations = [];
	for (let i = 0; i < POLocations.length; i++) {
		let hash;
		if (byParams === 'location') {
			hash = POLocations[i].container_id;
		}
		else {
			hash = POLocations[i].sku;
		}

		if (hash in qtyDict) {
			qtyDict[hash] += parseInt(POLocations[i].quantity) || 0;
			if (parseFloat(POLocations[i].volume)) {
				volDict[hash] += parseFloat(POLocations[i].volume);
			}
			else {
				flagged[hash] = true
			}
		}
		else {
			qtyDict[hash] = parseInt(POLocations[i].quantity);
			if (parseFloat(POLocations[i].volume)) {
				volDict[hash] = parseFloat(POLocations[i].volume);
			}
			else {
				flagged[hash] = true
			}
			locations.push({ location: POLocations[i].container_id, status: POLocations[i].status, color: POLocations[i].color, lat: POLocations[i].lat, lng: POLocations[i].lng });
		}

	}
	for (let i in locations) {
		let hash = locations[i].location;
		locations[i].quantity = qtyDict[hash];
		//temporary way handling unknown volume
		let flag = flagged[hash] ? " *" : "";
		locations[i].volume = volDict[hash]?volDict[hash] + flag:null;
  }
	return locations;
}

const roundNumber = function(n, digits) {
  var negative = false;
  if (digits === undefined) {
      digits = 0;
  }
      if( n < 0) {
      negative = true;
    n = n * -1;
  }
  var multiplicator = Math.pow(10, digits);
	n = parseFloat((n * multiplicator).toFixed(11));
	n = Math.ceil(n);
	n = (Math.round(n) / multiplicator).toFixed(2);
  if( negative ) {    
      n = (n * -1).toFixed(2);
  }
	return parseFloat(n);
}

const isEmpty = function(obj) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}
export { aggregateByLocation, aggregateByDestination, roundNumber, isEmpty };