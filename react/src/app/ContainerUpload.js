import React from 'react';
import Subheader from '../layout/Subheader'
import { connect } from 'react-redux';
import { loadingActions } from '../redux/actions';
import { Button,Card } from 'react-bootstrap'

//TODO Style the uoload button
//This is not a trivial process but this looks like trash


class ContainerUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadFile: null,
      errorMessage: '',
      successMessage: ''
    }
    this.handleSelect = this.handleSelect.bind(this);
    this.handleUpload = this.handleUpload.bind(this);
  }
  async handleUpload() {
    const { dispatch } = this.props;
    let formData = new FormData();
    formData.append('file',this.state.uploadFile);
    formData.append('type','manifest');
    dispatch(loadingActions.start('Uploading your information...'));		
    let uploadResult = await this.props.client.uploadExcel(formData);	
    console.log(uploadResult)
    if(uploadResult.successMessage){
      this.setState({
        successMessage: uploadResult.successMessage,
        errorMessage: ''
      })
    }
    if(uploadResult.errorMessage){
      this.setState({
        successMessage:'',
        errorMessage: uploadResult.errorMessage
      })
    }		
    dispatch(loadingActions.stop());

   
  }
  async handleSelect(e) {
    this.setState({
      uploadFile: e.target.files[0]
    })
  }
  render() {
    return (
      <>
        <Subheader title="Upload Manifest" />
        <Card>
            <p style = {{color:'red'}}> {this.state.errorMessage} </p>
            <p style = {{color:'green'}}> {this.state.successMessage} </p>
            <input onChange={this.handleSelect}  type="file" />
          <Button buttonText='Upload' onClick={this.handleUpload} />
          <a href='../files/exampleManifest.xlsx' download>Click to download example excel</a>
        </Card>
      </>
    )
  }
}
function mapStateToProps(state) {
	return { client: state.auth.client }
}
export default connect(mapStateToProps)(ContainerUpload);
