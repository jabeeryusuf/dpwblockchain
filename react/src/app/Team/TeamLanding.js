import React from 'react';
import { Nav } from 'react-bootstrap';
import { Subheader } from '../../layout';
import { withRouter, Link } from 'react-router-dom';
import TeamMembers from './TeamMembers';
import PendingInvitations from './PendingInvitations';
import Invite from './Invite';
import Groups from './Groups';
import { withLoader } from '../../components';
import Profile from './Profile';
import { Row, Col, Button, Dropdown, DropdownButton, ButtonToolbar, ButtonGroup } from 'react-bootstrap';

const tabMap = {
  'members': 0,
  'invitations': 1,
  'invite': 2,
  'groups': 3,
  'profile': 4
}
const titleMap = {
  'members': 'Team Members',
  'invitations': 'Pending Invitations',
  'invite': 'Invite New Members',
  'groups': 'Groups',
  'profile': 'My Profile'
}
const filterOptions = [
  { label: "Active Users", value: "active" },
  { label: "Inactive Users", value: "inactive" }
];
const getLabel = (opts, value) => {
  let l = opts.find((o) => (o.value === value));
  return l ? l.label : "";
}
const createDdi = (opts) => opts.map((o, i) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);


class TeamLanding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shown: 0,
      filter: 'active',
      title: 'Team Members'
    }
    this.viewSwitch = this.viewSwitch.bind(this);
    this.caseid = this.caseid.bind(this);
    this.switchTabs = React.createRef();
    this.onFilterChange = this.onFilterChange.bind(this)
  }
  componentDidMount() {
    if (this.props.match.params.tab && this.switchTabs.current.props != undefined) 
    {
      this.setState({
        shown: tabMap[this.props.match.params.tab],
        title: titleMap[this.props.match.params.tab]
      }, this.switchTabs.current.props.onSelect(titleMap[this.props.match.params.tab]))
    }
  } 
  onFilterChange(e){
    console.log(e)
    this.setState({
      filter: e
    })
  }
  componentDidUpdate(prevProps, prevState) {
    if (tabMap[this.props.match.params.tab] !== prevState.shown) {
      this.setState({
        shown: tabMap[this.props.match.params.tab],
        title: titleMap[this.props.match.params.tab]
      }, this.switchTabs.current.props.onSelect(titleMap[this.props.match.params.tab]))
    }
  }

  viewSwitch(tab, title) {
    this.switchTabs.current.props.onSelect(title);
    this.setState({
      shown: tabMap[tab],
      title: title
    }, this.props.history.push(this.props.basePath + '/team/' + tab))
  }

  caseid(case_id) {
    this.setState({
      case_id: case_id
    })
  }
  render() {
    return (
      <>
        <Subheader title={this.state.title}>
        </Subheader>
        <Nav variant="tabs" defaultActiveKey='Case Tracker' className="mb-3" ref={this.switchTabs} >
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('members', "Team Members")} eventKey="Team Members" > Team Members </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('invitations', "Pending Invitations")} eventKey="Pending Invitations" > Invitations </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('groups', "Groups")} eventKey="Groups" > Groups </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('profile', "My Profile")} eventKey="My Profile" > My Profile </Nav.Link>
          </Nav.Item>
        </Nav>
        <Row>
          <ButtonToolbar className="toolbar">

            <ButtonGroup className="filter" >
              {this.state.shown == 0 &&
                <DropdownButton title={getLabel(filterOptions,this.state.filter)} onSelect={this.onFilterChange}>
                  {createDdi(filterOptions)}
                </DropdownButton>
              }


              {
                (this.state.shown !== 3 && this.state.shown !== 4) &&
                <Link to={this.props.basePath + '/team/invite'}>
                  <Button style={{ marginBottom: 10 }}  >
                    Invite New Members
            </Button>
                </Link>
              }
            </ButtonGroup>
          </ButtonToolbar>
        </Row>
        <TeamMembers status = {this.state.filter} hide={this.state.shown === 0 ? false : true} {...this.props} viewSwitch={this.viewSwitch} />
        <PendingInvitations hide={this.state.shown === 1 ? false : true} {...this.props} viewSwitch={this.viewSwitch} />
        <Invite hide={this.state.shown === 2 ? false : true} {...this.props} viewSwitch={this.viewSwitch} />
        <Groups hide={this.state.shown === 3 ? false : true} {...this.props} viewSwitch={this.viewSwitch} vendors={false} />
        <Profile hide={this.state.shown === 4 ? false : true} {...this.props} viewSwitch={this.viewSwitch} />
      </>
    )
  }
}
export default withLoader(withRouter(TeamLanding));