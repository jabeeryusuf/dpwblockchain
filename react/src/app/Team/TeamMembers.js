import React from 'react';
import { connect } from 'react-redux';
import { userColumns, inactiveUserColumns } from '../columns/TeamColumns'
import { withLoader } from '../../components';
import ReactTable from 'react-table';
import { userActions } from '../../redux/actions';


const generateQuery = (org_id, status) => {
  if (status == 'active') {
    return "SELECT * FROM users where org_id = '" + org_id + "' and disabled = false and role='standard'"
  }
  return "SELECT * FROM users where org_id = '" + org_id + "' and disabled = true and role='standard'"
}
class TeamMembers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      users: []
    }
    this.deleteUser = this.deleteUser.bind(this);
    this.restoreUser = this.restoreUser.bind(this);
    this.getUsers = this.getUsers.bind(this);
  }
  async deleteUser(user) {
    this.props.startLoading('Loading your data...');

    user.disabled = true;
    await this.props.dispatch(userActions.update(user))
    this.getUsers()
  }
  async restoreUser(user) {
    this.props.startLoading('Loading your data...');

    user.disabled = false;
    await this.props.dispatch(userActions.update(user))
    this.getUsers()
  }
  async componentDidMount() {
    this.getUsers()
  }
  async getUsers() {
    const { user, startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');
    let query = generateQuery(this.props.user.org_id, this.props.status)

    let currentUsers = await this.props.client.getStuff(query);
    currentUsers.map((val) => {
      val.onClick = this.props.status == 'active' ? this.deleteUser : this.restoreUser
    })
    stopLoading();
    this.setState({
      users: currentUsers
    })
  }
  componentDidUpdate(prevProps) {
    if (this.props.status != prevProps.status) {
      this.getUsers()
    }
  }

  render() {
    return (
      <ReactTable
        data={this.state.users}
        columns={this.props.status == 'active' ? userColumns : inactiveUserColumns}
        defaultPageSize={20}
        className="-striped -highlight"
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user
  };
}

export default withLoader(connect(mapStateToProps)(TeamMembers));