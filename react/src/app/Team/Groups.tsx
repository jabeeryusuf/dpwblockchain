import { FcFetchClient } from "fc-rest-client";
import React from "react";
import { Button, Col, Form, InputGroup, Row } from "react-bootstrap";
import { FaPlus } from "react-icons/fa";
import { connect } from "react-redux";
import ReactTable from "react-table";
import { withLoader } from "../../components";
import NamesForNewUsers from "../../components/forms/NamesForNewUsers.js";
import VerticalModal from "../../components/VerticalModal.js";
import { groupsActions } from "../../redux/actions";
import { userGroupColumns, vendorGroupColumns } from "../columns/columns";
import JsCreateGroup from "../../components/forms/CreateGroup.js";
import { GroupType } from "tw-api-common";

const CreateGroup: any = JsCreateGroup;

class Groups extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      columns: userGroupColumns,
      errorMessage: "",
      filterList: [],
      groups : [],
      newEmails: [],
      showCreateGroup: false,
      showNewusers: false,
      submitting: false,
      successMessage: "",
    };

    this.fetchGroups = this.fetchGroups.bind(this);
    this.onCreateGroup = this.onCreateGroup.bind(this);
    this.onAddNames = this.onAddNames.bind(this);
  }

  public async componentDidMount() {
    const { startLoading, stopLoading } = this.props;

    startLoading();
    await this.fetchGroups();
    stopLoading();
  }

  public async fetchGroups() {
    const { org, basePath, vendors } = this.props;

    const groups = await this.props.groupsApi.getUsersInGroups(org.org_id, null, vendors);
    console.log("GROUPS: ", groups);
    for (const group of groups) {
      group.link =
        <Button size="sm" variant="link" onClick={() => this.onLinkClick(group, basePath)}>
          {group.groupName}
        </Button>;
    }

    groups.sort((a:any, b:any) => (a.groupName > b.groupName) ? 1 : ((b.groupName > a.groupName) ? -1 : 0));

    this.setState({
      columns: vendors ? vendorGroupColumns : userGroupColumns,
      groups,
      filterList: groups,
    });

    this.props.getUpdatedGroupList();

  }

  public async onLinkClick(group: any , basePath: any) {
    await sessionStorage.setItem("group", JSON.stringify(group));
    this.props.history.push({ pathname: basePath + "/team/group-detail" });
  }

  /**
   * Function gets called after creating group in pop up modal.
   * Accepts a group name, list of emails and list of child groups from modal.
   * Makes an API call to create that group
   * @param {string} groupName
   * @param {list} emails
   * @param {list} groups
   */
  public async onCreateGroup(groupName: any, emails: string[], groups: string[]) {
    const client = this.props.client as FcFetchClient;

    this.setState({
      submitting: true,
    });

    const emailList = [];

    console.log(emails);
    console.log(groups);

    const payload = {
      name: groupName.trim(),
      orgId: this.props.org.org_id,
      type: GroupType.USER_CREATED,
    };

    const createGroup = await this.props.groupsApi.create(payload, emails, groups);
    if (!createGroup.errorMessage) {
      this.setState({
        successMessage: "Group successfully created",
        errorMessage: "",
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showCreateGroup: false,
            submitting: false,
          });
          if (Object.keys(createGroup.newUsers).length > 0) {
            this.setState({
              newEmails: createGroup.newUsers,
              showNewUsers: true,
            });
          }
        }, 1500);
      });
      this.fetchGroups();
    } else {
      this.setState({
        errorMessage: createGroup.errorMessage,
        submitting: false,
      });
    }
  }

  /**
   * When a user creates or edits a new group and adds contacts that don't exist in DB
   * yet, this function is called after the user enters their new contacts' names.
   * @param {list} emailsAndNames
   */
  public async onAddNames(emailsAndNames: any) {
    this.setState({
      submitting: true,
    });
    const addedNames = await this.props.usersApi.addName(emailsAndNames);
    if (!addedNames.errorMessage) {
      this.setState({
        successMessage: "Names successfully added",
        errorMessage: "",
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showNewUsers: false,
            submitting: false,
          });
        }, 1500);
      });
    } else {
      this.setState({
        errorMessage: addedNames.errorMessage,
        submitting: false,
      });
    }
  }

  public onSearchChange = (event: any) => {
    const { value } = event.target;

    if (value === "") {
      this.setState({
        filterList: this.state.groups,
      });
    } else {
      const filterList = this.state.groups.filter((group :any) => {
        return group.groupName.toLowerCase().includes(value.toLowerCase());
      });

      this.setState({
        filterList
      });
    }
  }

  public render() {
    const { submitting, successMessage, errorMessage, newEmails, columns } = this.state;
    const modalClose = () => this.setState({ showCreateGroup: false, errorMessage: "", successMessage: "" });

    return (
      <div>
        <Row style={{ display: "flex", justifyContent: "Between",  marginBottom: 10 }}>
          <Col>
            {!this.props.vendors &&
            <Button onClick={ () => { this.setState({ showCreateGroup: true }); }}>
              Create a new group &nbsp;
              <FaPlus />
            </Button>}
          </Col>
          <Col sm={{ span: 3 }}>
            <InputGroup>
              <Form.Control
                style={{ width: "30%" }}
                type="text"
                placeholder="Search groups"
                onChange={(event: any) => this.onSearchChange(event)} />
              <InputGroup.Append>
                <Button>
                  Search
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Col>

        </Row>

        <VerticalModal title="Create a new group"
          size="md"
          show={this.state.showCreateGroup}
          onHide={modalClose}
          page={<CreateGroup onSubmit={this.onCreateGroup}
            submitting={submitting}
            successMessage={successMessage}
            errorMessage={errorMessage}
            footerButton={"Create"} />} />

        <VerticalModal title="Names for new contacts"
          size="md"
          show={this.state.showNewUsers}
          page={<NamesForNewUsers onSubmit={this.onAddNames}
            submitting={submitting}
            successMessage={successMessage}
            errorMessage={errorMessage}
            emails={newEmails} />} />

        <ReactTable   data={this.state.filterList}
                      columns={columns}
                      defaultPageSize={20}
                      className="-striped -highlight"
        />

      </div>
    );
  }
}

function mapStateToProps(state :any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    groupsApi: state.auth.groupsApi,
    usersApi: state.auth.usersApi,
  };
}

const mapDispatchToProps = (dispatch: any) => ({
  getUpdatedGroupList: () => dispatch(groupsActions.list()),
});

export default withLoader(connect(mapStateToProps, mapDispatchToProps)(Groups));
