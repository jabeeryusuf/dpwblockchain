import React from 'react';
import { connect } from 'react-redux';
import {inviteColumns} from '../columns/TeamColumns'
import { withLoader } from '../../components';
import ReactTable from 'react-table';

class PendingInvitations extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invitations :[]
    }
    this.resendInvite = this.resendInvite.bind(this);
  }

  async componentDidMount() {
    const { user } = this.props;
    console.log(user.org_id)
    let currentInvitations = await this.props.client.getStuff("SELECT * FROM invitations where org_id = '" + user.org_id + "' and invitation_status = 'pending' and email not in (SELECT distinct email from users)");
    currentInvitations.forEach(
      (val)=>{
        val.onClick = this.resendInvite;
      }
    )
    this.setState({
      invitations: currentInvitations
    })
  }
  async resendInvite(email){
    console.log(this.props.user.email)
    let fun = await this.props.client.createInvitations([{'email':email}], "",this.props.user.org_id, this.props.user.name, this.props.user.email);
    console.log(fun)
  }

  render() {
    return (
      <ReactTable
      data={this.state.invitations}
      columns={inviteColumns}
      searchable
      defaultPageSize={20}
      className="-striped -highlight"
    />
    );
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user
  };
}

export default withLoader(connect(mapStateToProps)(PendingInvitations));