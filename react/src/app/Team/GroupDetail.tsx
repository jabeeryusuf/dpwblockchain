import { FcFetchClient } from "fc-rest-client";
import React from "react";
import { Button, Card, Col, ListGroup, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { withLoader } from "../../components";
import JsCreateGroup from "../../components/forms/CreateGroup.js";
import NamesForNewUsers from "../../components/forms/NamesForNewUsers.js";
import VerticalModal from "../../components/VerticalModal.js";
import { groupsActions } from "../../redux/actions";

const CreateGroup: any = JsCreateGroup;

class GroupDetail extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      errorMessage: "",
      group: null,
      newEmails: {},
      showEditGroup: false,
      showNewUsers: false,
      submitting: false,
      successMessage: "",
      type: null,
    };

    this.onEditGroup = this.onEditGroup.bind(this);
    this.onAddNames = this.onAddNames.bind(this);
  }

  public componentDidMount() {
    const stored = sessionStorage.getItem("group")
    const something = stored ? JSON.parse(stored) : null;

    this.setState({
      group: something,
    });
  }

  public async onEditGroup(groupName: string, emails: string[], groups: string[]) {
    const client = this.props.client as FcFetchClient;

    this.setState({
      submitting: true,
    });

    const emailDict: any = emails.reduce((dict: any, e: any) => {dict[e] = e; return dict; }, {});
    const childGroupDict: any = groups.reduce((dict: any, g: any) => {dict[g] = g; return dict; }, {});



    console.log("SUBMIT EDIT");
    console.log(emails);
    console.log(emailDict);
    console.log(groups);
    console.log(childGroupDict);

    const payload: any = {
      groupID: this.state.group.groupID,
      prevGroupName : this.state.group.groupName.trim(),
      groupName: groupName.trim(),
      prevEmails: this.state.group.users,
      emails: emailDict,
      groups: childGroupDict,
      prevGroups: this.state.group.childGroups ? this.state.group.childGroups : [],
      type: this.state.type,
      org: this.props.org.org_id,
    };

    try {

      const editGroup = await client.groupsClient.update(payload);

      this.setState({
        errorMessage: "",
        successMessage: "Group successfully edited",
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showEditGroup: false,
            submitting: false,
          });
          if (isObject(editGroup)) {
            this.setState({
              newEmails: editGroup,
              showNewUsers: true,
            });
          }
        }, 1500);
        this.getUpdatedGroup();
        this.props.getUpdatedGroupList();
      });

    } catch(err) {
      this.setState({
        errorMessage: err,
        submitting: false,
      });
    }
  }

  public async onAddNames(emailsAndNames: any) {
    this.setState({
      submitting: true,
    });
    const addedNames = await this.props.client.getUsersApi().addName(emailsAndNames);
    if (!addedNames.errorMessage) {
      this.setState({
        successMessage: "Names successfully added",
        errorMessage: "",
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showNewUsers: false,
            submitting: false,
          });
        }, 1500);
      });
    } else {
      this.setState({
        errorMessage: addedNames.errorMessage,
        submitting: false,
      });
    }
  }

  public async getUpdatedGroup() {
    const { group } = this.state;
    const { startLoading, stopLoading, org } = this.props;
    const client = this.props.client as FcFetchClient;

    startLoading();

    // querying for a single group
    const updatedGroup = await client.groupsClient.getUsersInGroups(org.org_id, group.groupID, false); 
    console.log("UPDATED GROUP: ", updatedGroup);

    this.setState({
      group: updatedGroup[0],
    });

    stopLoading();
  }

  public render() {
    const { group } = this.state;

    return (
      <div>
        { group === null ?
          <Row className="justify-content-center">
            <Col sm={{ span: 4 }}>
              <Card>
                <Card.Header>
                  <Card.Title>No group selected</Card.Title>
                </Card.Header>
                <Card.Body>
                  <Card.Text>
                    To see group details and edit an existing group, select a group from the previous tabs
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
          :
          this.buildGroupDetail()
        }
      </div>
    );
  }
  public componentWillUnmount() {
    this.props.stopLoading();
  }
  public buildGroupDetail() {
    const { group, successMessage, errorMessage, submitting, type, newEmails } = this.state;
    const modalClose = () => this.setState({ showEditGroup: false, errorMessage: "", successMessage: "" });

    return (
      <>
        <VerticalModal title="Edit group"
                        size="md"
                        show={this.state.showEditGroup}
                        onHide={modalClose}
                        page={<CreateGroup onSubmit={this.onEditGroup}
                                            type={type}
                                            submitting={submitting}
                                            successMessage={successMessage}
                                            errorMessage={errorMessage}
                                            group={group}
                                            footerButton={"Edit"} />} />
        <VerticalModal title="Names for new contacts"
                        size="md"
                        show={this.state.showNewUsers}
                        page={<NamesForNewUsers onSubmit={this.onAddNames}
                                                type={type}
                                                submitting={submitting}
                                                successMessage={successMessage}
                                                errorMessage={errorMessage}
                                                emails={newEmails} />} />
        <Row className="justify-content-center">
          <Button onClick={() => this.setState({ showEditGroup: true })}>Edit this Group</Button>
        </Row>
        <br/>
        <Row>
          <Col>
            <Card>
              <Card.Header>
                <Card.Title>
                  {"Users in " + group.groupName}
                </Card.Title>
              </Card.Header>
              <ListGroup>
                {group.users && group.users.map((user: any, idx: number) => {
                  return (
                    <ListGroup.Item key={idx}>
                      {user.email}
                    </ListGroup.Item>
                  );
                })}
              </ListGroup>
            </Card>
          </Col>
          <Col>
            {group.childGroups &&
              <Card>
                <Card.Header>
                  <Card.Title>
                    {"Groups in " + group.groupName}
                  </Card.Title>
                </Card.Header>
                <ListGroup>
                  {group.childGroups.length < 1 ?
                    <ListGroup.Item>
                      <Row className="justify-content-center">
                        No Groups in {group.groupName}
                      </Row>
                    </ListGroup.Item>
                    :
                    group.childGroups.map((group: any, idx: number) => {
                      return (
                        <ListGroup.Item key={idx}>
                          {group.groupName}
                        </ListGroup.Item>
                      );
                    })}
                </ListGroup>
              </Card>
            }
          </Col>
        </Row>
        <br/>
        <Row>
        {group.childGroups && group.childGroups.length > 0 &&
          <>
            {group.childGroups.map((group: any, idx: number) => {
              return (
                <Col sm={{ span: 4 }} key={idx + "col"}>
                  <Card key={idx + "card"}>
                    <Card.Header key={idx + "head"}>
                      <Card.Title key={idx + "title"}>{"Users in " + group.groupName}</Card.Title>
                      <Card.Body key={idx + "body"}>
                        <ListGroup key={idx + "group"}>
                          {group.users.map((user: any, j: number) => {
                            return (
                              <ListGroup.Item key={j + "li"}>
                                {user.email}
                              </ListGroup.Item>
                            );
                          })}
                        </ListGroup>
                        {group.childGroups && group.childGroups.length > 0 &&
                          <>
                          <br/>
                            <div>{"Groups in " + group.groupName}</div>
                            <ListGroup>
                              {group.childGroups.map((childGroup: any, k: number) => {
                                return (
                                  <ListGroup.Item key={k + "li-2"}>
                                    {childGroup.groupName}
                                  </ListGroup.Item>
                                );
                              })}
                            </ListGroup>
                          </>
                        }
                      </Card.Body>
                    </Card.Header>
                  </Card>
              </Col>
              );
            })}
          </>
        }
        </Row>
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    client : state.auth.client,
    org : state.auth.org,
  };
}

const mapDispatchToProps = (dispatch: any) => ({
  getUpdatedGroupList: () => dispatch(groupsActions.list()),
});

function isObject(obj: any) {
  return obj === Object(obj);
}

export default withLoader(connect(mapStateToProps, mapDispatchToProps)(GroupDetail));
