import React from 'react';
import { connect } from 'react-redux';
import { userGroupColumns, vendorGroupColumns, consolePastColumns } from '../columns/columns';
import { withLoader } from '../../components';
import { groupsActions,userActions } from '../../redux/actions';
import { Button } from 'react-bootstrap';
const displayFields = ["Name", "Email"]
const inputFields = ["Name", "Password", "Confirm New Password"]

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: userGroupColumns,
      errors: [],
      filterList: [],
      formVals: {},
      newEmails: [],
      fields: displayFields,
      update: false,
      showCreateGroup: false,
      showNewusers: false,
      submitting: false,
      successMessage: "",
    }
    this.setInitialFormVals = this.setInitialFormVals.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.props != prevProps) && this.props.hide == false) {
      this.setInitialFormVals();
    }

  }
  renderErrors() {
    return (
      <ul style={{ listStyle: 'None' }}>
        {this.state.errors.map((val, idx) => {
          return (
            <li key={idx} style={{ color: "red" }}> { val } </li>
        )
      })
    }
    </ul>
    )
  }
  setInitialFormVals() {
    let { name, email } = this.props.user;
    let vals = [name, email];
    let formDict = {};
    displayFields.forEach((field, idx) => {
      formDict[field] = idx < vals.length ? vals[idx] : ""
    })

    this.setState({
      formVals: formDict
    })
  }
  onChange(e) {
    this.setState({
      formVals: {
      ...this.state.formVals,
        [e.target.name]: e.target.value
      }
    });
  }
  renderProfileInfo() {

    let { name, email } = this.props.user;
    let vals = [name, email];
    return (
      <ul style={{ listStyle: 'None' }}>
        {this.state.fields.map((val, idx) => {
          return (
            <li key={idx}> {val}: {this.state.update == true ? <input name={val} value={this.state.formVals[val]} onChange={this.onChange} /> : vals[idx]} </li>
          )
        })
        }
        <li>
          {this.state.update && <><Button onClick={() => { this.handleSubmit() }}>Save Changes</Button> <Button onClick={() => { this.setUpdating() }}> Cancel</Button></>} {!this.state.update && <Button onClick={() => { this.setUpdating() }}> Update Info </Button>}
        </li>
      </ul>
    )
  }
  async handleSubmit() {
    if(this.validateFields()){
      let user = await this.generateUpdateUser();
      this.props.dispatch(userActions.update(user))
      this.setUpdating()
  }
}
  async generateUpdateUser(){
    let newUser = await JSON.parse(JSON.stringify(this.props.user))
    for(let key in this.state.formVals){
      if(this.state.formVals[key].length>0){
        newUser[key.toLowerCase()] = this.state.formVals[key]
      }
    }
    return newUser 
  }
  validateFields() {
    let validateErrors = []
    if (this.state.formVals['Confirm New Password'] != this.state.formVals["Password"]) {
      validateErrors.push("Passwords do not match")
    }
    if (this.state.formVals['Name'].length<2) {
      validateErrors.push("Name must be at least 2 characters")
    }
    if(this.state.formVals["Password"] && this.state.formVals['Password'].length<4){
      validateErrors.push('Password must be at least 4 characters')
    }
    this.setState({
      errors: validateErrors
    })
    if(validateErrors.length == 0){
      return true 
    }
    console.log("Errors")
    return false 
  }
  setUpdating() {

    if (!this.state.update) {
      this.setState({
        update: true,
        fields: inputFields,

      })
    }
    else {
      this.setState({
        update: false,
        fields: displayFields,
        errors:[]
      })
      this.setInitialFormVals()
    }
  }
  async componentDidMount() {
    const { startLoading, stopLoading } = this.props;
  }

  render() {
    return (
      <>
        <h1> Your Profile </h1>
        {this.renderErrors()}
        {this.renderProfileInfo()}
      </>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org,
    groupsApi: state.auth.groupsApi,
    usersApi: state.auth.usersApi
  };
}

const mapDispatchToProps = (dispatch) => ({
  getUpdatedGroupList: () => dispatch(groupsActions.list())
});

export default withLoader(connect(mapStateToProps, mapDispatchToProps)(Profile));