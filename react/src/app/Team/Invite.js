import React from 'react';
import { connect } from 'react-redux';
import { Card, Container, Form, FormLabel, FormControl, FormGroup, Col, Button, Row } from 'react-bootstrap';
import { withLoader } from '../../components';

class Invite extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      emails: [{ email: '' }, { email: '' }, { email: '' }],
      submitted: false,
      errorMessages: [],
      successMessage: '',
      inviteMessage: ''
    }

    this.addClick = this.addClick.bind(this);
  }

  componentDidMount() {
    const { user } = this.props;
    console.log(user.blockchain_id);
  }

  addClick() {
    let newEmails = this.state.emails;
    newEmails.push({ email: '' });

    this.setState({
      emails: newEmails
    });
  }

  handleChange(event) {
    const { name, value } = event.target;
    let currentEmails = this.state.emails;
    currentEmails[name].email = value;
    this.setState({
      emails: currentEmails
    })
  }
  handleMessageChange(event) {
    this.setState({
      inviteMessage: event.target.value
    })
  }
  async handleSubmit() {
    let usersApi = this.props.client.getUsersApi();
    console.log("INVITE SUBMIT");
    console.log(this.state.emails);
    console.log(this.state.inviteMessage)
    let errors = await usersApi.createInvitations(this.state.emails.slice(),
                                                  this.state.inviteMessage.slice(),
                                                  this.props.user.org_id,
                                                  this.props.user.name,
                                                  this.props.user.email);
    if (errors.length) {
      this.setState({
        errorMessages: errors
      });
    }
    else {
      this.setState({
        successMessage: 'Invitations succesfully sent!'
      })
    }
  }
  renderSuccess() {
    if (this.state.successMessage.length > 0) {
      return (
        <p style={{ color: 'green' }}>{this.state.successMessage} </p>
      )
    }
  }
  renderErrors() {
    console.log(this.state.errorMessages)
    if (this.state.errorMessages.length > 0) {
      return (
        <ul style={{ listStyle: 'none' }}>
          {this.state.errorMessages.map((val) => {
            return (
              <li style={{ color: 'red' }}>
                {val}
              </li>
            )
          })
          }
        </ul>
      )
    }
  }
  render() {
    const { user } = this.props;
    return (
      <Container>
        <Card>
          <h2>Invite members to <b>{user.blockchain_id}</b>.DPWChain.com</h2>
          <br />
          <Form>
            <Col sm={{ span: 6, offset: 3 }}>
              {this.renderErrors()}
              {this.renderSuccess()}
              <FormGroup>
                <FormLabel>Email Address</FormLabel>
                {this.state.emails.map((email, index) => {
                  let margin = 10;
                  if (index === 0) {
                    margin = 0;
                  }
                  return (
                    <FormControl name={index}
                      key={index}
                      type="email"
                      value={this.state.emails[index].email}
                      placeholder="name@example.com"
                      style={{ marginTop: margin }}
                      onChange={(e) => this.handleChange(e)} />
                  );
                })}
                <Button size="sm" style={{ marginInlineStart: 84 }} variant="link" onClick={this.addClick}>Add another email</Button>
              </FormGroup>
              <br/>
              <FormGroup>
                <FormLabel>Add a custom message</FormLabel>

                      <textarea    
                      value={this.state.inviteMessage}
                      placeholder="Your message to invitees here"
                      onChange={(e) => this.handleMessageChange(e)} 
                      style={{ height: 200, width: 500, verticalAlign:'top'}} >

                        </textarea>
              </FormGroup>
            </Col>
            <Row className="justify-content-center">
              <Col sm={{ span: 6 }}>
                <Button onClick={() => { this.handleSubmit() }} block>Invite</Button>
              </Col>
            </Row>
            <br />
          </Form>
        </Card>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user
  };
}

export default withLoader(connect(mapStateToProps)(Invite));