import React from 'react'
import { Form, Button, Card, InputGroup, Col, Row, Container } from 'react-bootstrap';

const UpdateTrackingForm = (props) => {
  return(
    <>
    <Form.Label> Tracking Reference : </Form.Label> <Form.Control onChange={(e) => { props.handleChange(e) }} name='newTrackingRef' value={props.formValues['Reference']} />
    <Form.Label> Carrier : </Form.Label> <Form.Control as='select' onChange={(e) => { props.handleChange(e) }} name='newTrackingCarrier' value={props.formValues['Carrier']} >
      {
        props.carriers.map((val, idx) => {
        return (
          <option key={idx} value={val.carrierId}>{val.name}</option>
        )
      })
      }
    </Form.Control>
    <Form.Label> Type : </Form.Label> <Form.Control as='select' name="newTrackingType" value={props.formValues['Type']} onChange={(e) => { props.handleChange(e) }}>
      <option value="bol">Bol #</option>
      <option value="container">Container #</option>
      <option value="booking_ref">Booking Reference #</option>

    </Form.Control>
    <Button variant="outline-primary" onClick={props.submit}>Update </Button>
      </>
  )
}

export default UpdateTrackingForm; 