import React from 'react';
import { Subheader } from '../../layout';
import { Nav } from 'react-bootstrap';
import { connect } from 'react-redux';
import Groups from '../Team/Groups';

class VendorLanding extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    }


  }

  async componentDidMount() {

  }

  render() {
    return (
      <>
        <Subheader title="Vendors" />
        <Nav variant="tabs" className="mb-3">
          <Nav.Item>
            <Nav.Link active>Vendors Groups</Nav.Link>
          </Nav.Item>
        </Nav>
        <Groups {...this.props} vendors={true} />
      </>
    );
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client
  };
}

export default connect(mapStateToProps)(VendorLanding);