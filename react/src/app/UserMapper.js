import React from 'react'
import { View,Row,Col } from '../layout/View'
import { GMap} from '../components/GMap'
import Portlet from '../layout/Portlet'
import Table from '../components/Table'
import Subheader from '../layout/Subheader'
import findCenter from '../utils/findCenter'
import aggregateByPO from '../utils/aggregateByPO'
import { aggregateByLocation, aggregateByDestination, roundNumber } from '../utils/aggregateByLocation'
import Button from '../components/Buttons/Button.js';
import { Button as RButton } from 'react-bootstrap'
import Itemdetailview from '../components/Itemdetailview';
import { dstColumns, originColumns, columns } from './columns';
import { loadingActions } from '../redux/actions';
import { connect } from 'react-redux';
import Select from '../components/UI/Select';

class UserMapper extends React.Component {

  constructor(props) {
   super(props);

   this.state = {
      date:  new Date(),
     	searchText: '',
      searchText1: '',
      consignee: '',
      supply_chain: [],
      data: [],
      selected_supply_chain: ''
   };
   this.handleSearchChange = this.handleSearchChange.bind(this);
   this.handleSearchChange1 = this.handleSearchChange1.bind(this);
   this.handleSearchChange2 = this.handleSearchChange2.bind(this);
   this.onSelectChange = this.onSelectChange.bind(this);
   this.handleSearchClick = this.handleSearchClick.bind(this);
   this.map_user = this.map_user.bind(this);

 }
 async componentDidMount() {
   const { dispatch } = this.props;
   dispatch(loadingActions.start('Loading your page...'));

   let data = await this.props.client.getExistingUsers();
   this.setOptionData(data)
   console.log(data)
   this.timerID = setInterval(
     () => this.tick(), 1000)
   this.setState({
     data: data
   })
   console.log('111')
   console.log(this.state.data[0]['user_id'])
   dispatch(loadingActions.stop());
 }

 componentWillUnmount() {
   clearInterval(this.timerID);
 }
 handleSearchChange(event) {
   this.setState({
     searchText: event.target.value
   })
 }
 handleSearchChange1(event) {
   this.setState({
     searchText1: event.target.value
   })
 }
 handleSearchChange2(event) {
   this.setState({
     consignee: event.target.value
   })
 }
 handleSearchClick(event) {
   event.preventDefault();
   // let newData = [];
   // for (let i in this.state.storedData) {
   //   if (this.state.searchText === this.state.storedData[i].sku) {
   //     newData.push(this.state.storedData[i])
   //   }
   // }
   // let aggregated = aggregateByLocation(newData, 'location');
   // this.setMarkers(aggregated);
   //
   // this.setState({
   //   data: newData,
   //   item: newData.length ? { sku: newData[0].sku, name: newData[0].productName, department: newData[0].department, image: newData[0].image } : {}
   // })
   if (this.state.searchText ==this.state.searchText1){
     console.log('Equal')
   }else{
     console.log('Not Equal')
   }

 }

 map_user(event){
   event.preventDefault();
   console.log(this.state.consignee)
 }

 setOptionData(data){

   let temp = []
   for (let obj of data){
     let a = {
       title: obj.user_id,
       value: obj.user_id
     }
     temp.push(a)
   }
   console.log(temp)
   this.setState({
     supply_chain: temp
   })
 }

 tick() {
   this.setState({
     date: new Date()
   })
 }
 onSelectChange(value){
   this.setState({
     selected_supply_chain: value
   })
   console.log(this.state.selected_supply_chain);
 }

  render() {
    return (
      <View>
        <Row>
          <div>
            <input onChange={this.handleSearchChange} style={{ width: "30%" }} type="text" placeholder={"A"} />&nbsp;
            <input onChange={this.handleSearchChange1} style={{ width: "30%" }} type="text" placeholder={"B"} />&nbsp;
            <RButton bsSize="small" bsStyle="info" onClick={this.handleSearchClick}>Check</RButton>
          </div>
        </Row>
        <br></br>
        <Row>
          <Col>
          <input onChange={this.handleSearchChange2} style={{ width: "100%", height: "70%" }} type="text" placeholder={"Consignee"} />
          </Col>
          <Col>
          <Select options={this.state.supply_chain} onValueChange={this.onSelectChange} />
          </Col>
          <Col>
            <RButton bsSize="small" bsStyle="info" onClick={this.map_user}>confirm</RButton>
          </Col>
        </Row>
        <Row>
          <div>
            It is {this.state.date.toLocaleTimeString()}.
          </div>
        </Row>
      </View>
    )
  }

}


function mapStateToProps(state) {
	return { client: state.userAuth.client }
}
export default connect(mapStateToProps)(UserMapper);
