import React from 'react';
import { connect } from 'react-redux';
import { withLoader } from '../components';
import { Form, Button, ButtonGroup, Row, Col, Table, Alert } from 'react-bootstrap';

class MessageTemplateConfig extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      caseOptions: [],
      selectedCase: null,
      templateMap: {},
    };

    this.handleFileUpload = this.handleFileUpload.bind(this);
  }

  componentDidMount() {
    this.getCaseOptions();
  }

  async getCaseOptions() {
    const { startLoading, stopLoading } = this.props;
    startLoading();

    let caseTypes = await this.props.client.getCaseOptions();
    let caseOptions = [];
    for (let i = 0; i < caseTypes.length; i++) {
      let type = caseTypes[i];
      caseOptions.push(<option key={type} value={type}>{type}</option>)
    }

    this.setState({
      caseOptions: caseOptions,
      selectedCase: caseOptions[0].props.children, //value of the option
    }, () => this.getExistingTemplates());

    stopLoading();

  }

  async getExistingTemplates() {
    let queryTemplates = await this.props.conversationApi.templates(this.props.org.org_id);
    
    this.setState({
      templateMap: queryTemplates
    }, () => this.tryAndGetImage());
  }

  onSelectChange = (event) => {
    const { value } = event.target;

    this.setState({
      selectedCase: value  
    });
  }

  handleMessageChange = (event) => {
    const { value } = event.target;

    let tempMap = this.state.templateMap;
    tempMap[this.state.selectedCase] = value;

    this.setState({
      templateMap: tempMap
    });
  }

  async handleFileUpload(event) {
    let formData = new FormData();
    formData.append('file', event.target.files[0]);
    let uploadLogo = await this.props.client.uploadEmailLogo(formData, this.props.org.org_id, "logo");
  }

  async tryAndGetImage() {
    let queryFileID = await this.props.client.getStuff(`SELECT value FROM org_settings WHERE org_id='` + this.props.org.org_id + `' AND key='logo'`);
    if (queryFileID.length > 0) {
      let fileID = queryFileID[0].value;
      this.setState({
        imageURL : 'https://s3-eu-west-1.amazonaws.com/dpwchainpublic/logo/' + fileID
      });
    }
  }

  async saveTemplates(single) {
    const { templateMap, selectedCase } = this.state;
    const { org, conversationApi } = this.props;
    
    this.setState({
      submitting: true
    });
    
    let body = {
      templateMap: templateMap,
      orgId: org.org_id,
      selectedCase: single ? selectedCase : null
    }

    let save = await conversationApi.saveTemplate(body);
    if (!save.errorMessage) {
      this.setState({
        successMessage: single ? 'Template Successfully Saved' : 'All Templates Successfully Saved',
        errorMessage: '',
        submitting: false
      });
    } else {
      this.setState({
        successMessage: '',
        errorMessage: save.errorMessage,
        submitting: false
      });
    }
  }

  render() {
    const { caseOptions, selectedCase, templateMap, submitting, successMessage, errorMessage } = this.state;

    return (
      <div>
        <Row>
          <Col sm={10}>
            <h2>Case Creation Message Templates</h2>
          </Col>
        </Row>
        <br/>
        <Row>
          <Col sm={10}>
            {successMessage ? <Alert variant="success" dismissible>{successMessage}</Alert> : null}
            {errorMessage ? <Alert variant="danger" dismissible>{errorMessage}</Alert> : null}
          </Col>
        </Row>
        <Row>
          <div>
            {this.state.imageURL ? <img style={{height: "30px", width:"100px"}} src = {this.state.imageURL} alt="That did not work"/> : null}
          </div>
        </Row>
        <Row>
          <Col sm={10} className="tablehead">
            <Table bordered>
              <tbody>

                <tr>
                  <th>Case Type:</th>
                  <td>
                    <Form.Control style={{ width: "54%" }} 
                                  as="select"
                                  disabled={submitting}
                                  onChange={(e) => {this.onSelectChange(e)}} >
                      {caseOptions}
                    </Form.Control>
                  </td>
                </tr>

                <tr>
                  <th>Message Template:</th>
                  <td>
                    <textarea style={{ height: 400, width: 600 }} 
                              onChange={(e) => this.handleMessageChange(e)}
                              disabled={submitting}
                              placeholder="Enter a quick message to send along with each case creation" 
                              value={templateMap[selectedCase] ? templateMap[selectedCase] : ""} 
                              required >
                    </textarea>
                  </td>
                </tr>

                <tr>
                  <th>Image of Company Logo:</th>
                  <td><Form.Control type="file" accept=".jpg,.gif,.png" onChange={this.handleFileUpload} /></td>
                </tr>

              </tbody>
            </Table>
          </Col>
        </Row>
        <Form.Group as={Row}>
          <Col sm={{ span: 10 }}>
            <ButtonGroup>
              <Button onClick={() => this.saveTemplates(true)} disabled={submitting}>Save Template</Button>
              <Button className="ml-2" variant="outline-primary" disabled={submitting} onClick={() => {this.saveTemplates(false)}}>Save All Templates</Button>
            </ButtonGroup>
          </Col>
        </Form.Group>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org,
    conversationApi: state.auth.conversationApi
  }
}

export default withLoader(connect(mapStateToProps)(MessageTemplateConfig));