import React from 'react';
import ReactTable from 'react-table';
import { Card, Row, Button, Dropdown, ButtonToolbar, ButtonGroup, DropdownButton } from 'react-bootstrap';
import { connect } from 'react-redux';
import { allColumns } from '../columns/milestoneColumns';
import { Link } from "react-router-dom";
import { CSVLink } from 'react-csv';
import { withLoader } from '../../components';


const createDdi = 
  (opts) => opts.map((o,i) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

const getLabel = (opts, value) => {
  let l = opts.find((o) => (o.value === value));
  return l ? l.label : "";
}



class Milestones extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      destinationSelectFields: [],
      poSelectFields: [],
      columns: allColumns,
      tableData: [],
      destination: '',
      currentPO: "All",
      inputValue: '',
      noResults: false,
      exportData: [],
      headers: [],
      showButtons: true,
      //changing this for development purposes only 
      shown: props.shown ? props.shown : true
    };
    this._isMounted = true;
    this.setPOSelect = this.setPOSelect.bind(this);
    this.setExportData = this.setExportData.bind(this);
    this.reactTable = React.createRef();
  }

  componentDidMount() {
    if (!this.props.hide) {
      this.getLocations();
    }
  }

  /**
   * Gets list of destination containers for all PO's and creates a list of options for 
   * "Select Destination" dropdown menu.
   */
  async componentDidUpdate(prevProps){
    if(prevProps.hide !== this.props.hide && !this.state.data.length){
      this.getLocations();
    }
    if(this.props.searchText !== prevProps.searchText && this.props.searchText.length>0){
      this.searchSubmitted(this.props.searchText)
    }
    if(this.props.searchText !== prevProps.searchText && this.props.searchText.length === 0){
      this.updateInputValue({target:{value:''}})
    }
  }
  async getLocations() {
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');

    let locations = await this.props.client.getDestinationContainers(this.props.user.org_id);
    let data = await this.props.client.queryMilestones(this.props.user.org_id);
    let selectors = [];
    if (locations.length) {
      for (let location of locations) {
        selectors.push({value: location.destination_container_id, label: location.name})
      }
      selectors.push({value: "other", label: "Other"});

      let exportHeaders = [];
      for (let col of this.state.columns) {
        let obj = {
          label: col.Header,
          key: col.accessor
        };
        exportHeaders.push(obj);
      }
      if (this._isMounted) {
        this.setState({
          data: data.rows,
          destinationSelectFields: selectors,
          destination: locations[0].destination_container_id,
          headers: exportHeaders
        }, () => this.setPOSelect());
      }
    }

    stopLoading();
  }

  /**
   * Sets the "Select PO ID" doprdown menu with list of PO's with 
   * above menu's selected destination
   */
  setPOSelect() {
    const { basePath } = this.props;
    let list = this.state.data;
    let dest = this.state.destination;
    let selectFields = [];
    let distinct = [];
    for (let obj of list) {
      obj.poLink = <Link to={basePath + "/purchaseorders/detail/" + obj.purchase_order_id}>{obj.purchase_order_id}</Link>
      obj.skuLink = <Link to={basePath + "/items/detail/" + obj.sku}>{obj.sku}</Link>
      obj.pic = obj.image ? <img style={{ width: 75, height: 75 }} src={obj.image} alt={"Failed to Load"} /> : "No Image";
      if (obj.dst_container === dest) {
        if (distinct.indexOf(obj.purchase_order_id) < 0) {
          let field = {value: obj.purchase_order_id, label: obj.purchase_order_id };
          selectFields.push(field);
          distinct.push(obj.purchase_order_id);
        }
      }
    }

    selectFields.unshift({label:"All", value:"All"});

    this.setState({
      poSelectFields: selectFields,
      destination: dest
    }, () => this.onPOSelectChange(selectFields[0].value));

  }

  onDestSelectChange = (value) => {
    this.setState({
      poSelectFields: [],
      destination: value
    }, () => this.setPOSelect());
  }

  onPOSelectChange = (value) => {

    let data = this.state.data;
    let list = [];
    let field = 'purchase_order_id';
    let columns = this.state.columns;
    let copy = value;
    if (value === "All") {
      field = 'dst_container';
      value = this.state.destination;
      columns = allColumns;
    }

    for (let obj of data) {
      if (obj[field] === value) {
        list.push(obj);
      }
    }
    list.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));

    let str = value;
    switch (str) {
      case "port&BKK":
        str = "Port of Bangkok";
        break;
      case "port&JKT":
        str = "Port of Jakarta";
        break;
      case "port&SEA":
        str = "Port of Seattle";
        break;
      default:
        str = value;
    }
    this.setState({
      header: "Milestone Report for: " + str,
      tableData: list,
      columns: columns,
      currentPO: copy,
      csvData: [],
      csvLink: ''
    }, () => this.setExportData());

  }

  /**
 * triggered when input box on webapp is changed
 * @param {*} evt 
 */
  updateInputValue(evt) {
    if (evt.target.value === '') {
      let list = this.state.data;
      let table = [];
      if (this.state.currentPO === "All") {
        for (let obj of list) {
          if (obj.dst_container === this.state.destination) {
            table.push(obj);
          }
        }
      } else {
        for (let obj of list) {
          if (obj.purchase_order_id === this.state.currentPO && obj.dst_container === this.state.destination) {
            table.push(obj);
          }
        }
      }
      table.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));

      this.setState({
        inputValue: evt.target.value,
        tableData: table,
        noResults: false,
        csvData: []
      }, () => this.setExportData());

    } else {
      this.setState({
        inputValue: evt.target.value
      });
    }
  }

  searchSubmitted(sku) {
    if (sku === '') {
      return;
    }
    let list = this.state.tableData;
    let newTable = [];
    let noResults = true;
    for (let obj of list) {
      if (obj.sku === sku) {
        newTable.push(obj);
      }
    }
    newTable.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));
    if (!newTable.length) {
      newTable = list;
    } else {
      noResults = false;
    }


    this.setState({
      tableData: newTable,
      inputValue: '',
      noResults: noResults,
      csvLink: ''
    }, () => this.setExportData());

  }

  setExportData() {
    let list = this.state.columns;
    let data = this.state.tableData;
    let csvHeaders = [];
    for (let col of list) {
      let key = "";
      switch (col.Header) {
        case "PO ID":
          key = "purchase_order_id";
          break;
        case "Product SKU":
          key = "sku";
          break;
        default:
          key = col.accessor;
      }

      if (col.Header !== "Photo") {
        let obj = {
          label: col.Header,
          key: key
        };

        csvHeaders.push(obj);
      }
    }

    //Info that Emma wants in the exported data that is not in the table
    csvHeaders.push({
      label: "Vendor Code",
      key: "vendor_code"
    });

    csvHeaders.push({
      label: "Vendor Name",
      key: "name"
    });

    csvHeaders.push({
      label: "Port of Loading",
      key: "pol_name"
    });

    this.setState({
      exportData: data,
      headers: csvHeaders,
      csvLink: <CSVLink data={data} headers={csvHeaders} filename={"milestone_report.csv"}><Button variant="outline-primary">Download Table</Button></CSVLink>
    });

  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {

    const { currentPO, poSelectFields, destination, destinationSelectFields, csvLink } = this.state

    return (
      <>
        <Row>
          <ButtonToolbar className="toolbar">

            <ButtonGroup className="filter" >
              <DropdownButton title={getLabel(destinationSelectFields, destination)} onSelect={this.onDestSelectChange}>
                { createDdi(destinationSelectFields)}
              </DropdownButton>

              <DropdownButton title={getLabel(poSelectFields, currentPO)} onSelect={this.onPOSelectChange}>
                { createDdi(poSelectFields) }
              </DropdownButton>
            </ButtonGroup>

            <ButtonGroup className="action">
              {csvLink}
            </ButtonGroup>

          </ButtonToolbar>

        </Row>
        <Card style={{ marginTop: 15 }}>
          <ReactTable
            columns={[{ Header: <font size="3">{this.state.header}</font>, columns: this.state.columns }]}
            data={this.state.tableData}
            className="-striped -highlight itemstable"
            ref={this.reactTable} />
        </Card>
      </>
    )
  }
}

function mapStateToProps(state) {
  return { client: state.auth.client, user: state.auth.user }
}

export default withLoader(connect(mapStateToProps)(Milestones));