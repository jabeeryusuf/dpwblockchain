import React from 'react';
import { Nav, Col, Button, InputGroup, Form } from 'react-bootstrap';
import Milestones from './Milestones';
import MilestoneException from './MilestoneException';
import { Subheader } from '../../layout';
import { withRouter } from 'react-router-dom'
import GroupInfo from '../Cases/GroupInfo';
import MilestoneException2 from './MilestoneException2'
const tabMap = {
  'tracker': 0,
  'exceptions': 1,
  'settings':2
}
const titleMap = { 
  'tracker': 'Milestone Tracker',
  'exceptions': 'Milestone Exceptions',
  'settings': 'Settings'
}
class MilestoneLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      shown: 0,
      title: 'Milestone Tracker',
      inputText: '',
      searchText: ''
    }
    this.nav = React.createRef()
  }

  componentDidMount() {
    const tab = this.props.match.params.tab ? this.props.match.params.tab : "tracker";

    if (this.props.match.params.tab && this.nav.current.props != undefined) // props may be undefined here
    {
      this.setState({
        shown: tabMap[this.props.match.params.tab],
        title: titleMap[this.props.match.params.tab]
      }, this.nav.current.props.onSelect(this.props.match.params.tab));
    }
  }
  componentDidUpdate() {
    if (tabMap[this.props.match.params.tab] !== this.state.shown) {
      this.setState({
        shown: tabMap[this.props.match.params.tab],
        title: titleMap[this.props.match.params.tab]
      }, this.nav.current.props.onSelect(this.props.match.params.tab))
    }
  }
  viewSwitch(tab, title) {
    this.setState({
      shown: tabMap[tab],
      title: title
    }, this.props.history.push(this.props.basePath + '/milestones/' + tab))
  }
  updateInputValue(e) {
    this.setState({
      inputText: e.target.value
    })
    if (e.target.value === '') {
      this.setState({
        searchText: e.target.value
      })
    }
  }
  searchSubmitted() {
    this.setState({
      searchText: this.state.inputText
    })
  }
  render() {
    return (
      <>
        <Subheader title={this.state.title}>
          {(this.state.shown === 1 || this.state.shown === 0) &&
            <Col sm={{ span: 4, offset: 4 }}>
              <InputGroup>
                <Form.Control
                  style={{ width: "30%" }}
                  type="text"
                  placeholder="Search SKU"
                  onChange={(evt) => this.updateInputValue(evt)} />
                <InputGroup.Append>
                  <Button onClick={() => this.searchSubmitted()} >
                    Search
                  </Button>
                </InputGroup.Append>

              </InputGroup>
            </Col>
          }
        </Subheader>
        <Nav ref={this.nav} variant="tabs" defaultActiveKey='tracker' className="mb-3">
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('tracker', "Milestone Tracker")} eventKey="tracker" > Milestones </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('exceptions', 'Milestone Exceptions')} eventKey='exceptions' > Milestone Exceptions </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onSelect={() => this.viewSwitch('settings', 'Settings')} eventKey='settings' > Settings </Nav.Link>
          </Nav.Item>
        </Nav>

        <Milestones basePath={this.props.basePath} searchText={this.state.searchText} hide={this.state.shown === 0 ? false : true} />
        <MilestoneException basePath={this.props.basePath} searchText={this.state.searchText} hide={this.state.shown === 1 ? false : true} history={this.props.history} />
        <GroupInfo  hide={this.state.shown === 2 ? false : true} history={this.props.history} basePath={this.props.basePath} />
      </>
    )
  }
}
export default withRouter(MilestoneLanding);