import moment from "moment"
import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import ReactTable from "react-table";
import { withLoader } from "../../components";
const jswithLoader: any = withLoader;
const jswithRouter: any = withRouter;
const exceptionColumns2 = [
  {
    Header: "Shipment ID",
    accessor: "shipmentId",
  },
  {
    Header: "Status",
    accessor: "status",
  },
  {
    Header: "Expected Date",
    accessor: "expectedTime",
    Cell: (val: any) => {
      return displayDate(val.original.expectedTime);
    },
  },
  {
    Header: "From",
    accessor: "sourceContainerId",
    Cell : (val: any) => {
      return filterContainer(val.original.sourceContainerId);
    },
  },
  {
    Header: "To",
    accessor: "destinationContainerId",
    Cell : (val: any) => {
      return filterContainer(val.original.destinationContainerId);
    },
  },
  {
    Header: "Last Event",
    accessor: "lastStatus",
  },
  {
    Header: "Last Location",
    accessor: "lastLocation",
    Cell : (val: any) => {
      return filterContainer(val.original.lastLocation);
    },
  },
  {
    Header: "Last Date",
    accessor: "lastDate",
    Cell : (val: any) => {
      if (val.original.lastDate) {
        return displayDate(val.original.lastDate);
      }
      return null;
    },
  },
];
const displayDate = (val: any) => {
  if (val) {
    const event = new Date(val);
    return(
      event.toLocaleDateString(navigator.language)
    );
  }
  return null;
};
const filterContainer = (val: any) => {
  if (val) {
    return(
    val.slice(val.indexOf("&") + 1)
    );
  }
  return null;
};
const flattenException = (val: any) => {
  return Object.assign(val,
    { destinationContainerId: val.shipment.destinationContainerId,
      shipmentId : val.shipment.shipmentId,
      sourceContainerId: val.shipment.sourceContainerId,
      lastStatus: val.lastEvent ? val.lastEvent.status : null,
      lastLocation: val.lastEvent ? val.lastEvent.location : null,
      lastDate: val.lastEvent ? val.lastEvent.date : null,
    });
};
interface IMilestoneProps extends RouteComponentProps<IMatchParams> {
  basePath?: string;
  client?: any;
  hide?: boolean;
  org?: any;
  user?: any;
  startLoading?: any;
  stopLoading?: any;
}

interface IMatchParams {
  tab: string;
}

class MilestoneExceptions2 extends React.Component<IMilestoneProps, any>  {
  private detail: any;
  private all: any;

  constructor(props: IMilestoneProps) {
    super(props);
    this.state = {
      tableData: [],
    };
  }
  public async componentDidMount() {
    this.getExceptions();

  }
  public async getExceptions() {
    const exceptions = await this.props.client.shipmentsClient.getExceptions(this.props.org.org_id);
    console.log(exceptions);
    console.log("wud up");
    const tableData = exceptions.exceptions.map((val: any) => flattenException(val));
    console.log(tableData);
    this.setState({
      tableData,
    });
  }
  public render() {
    return (
      <>
        <h1> I am the new milestones </h1>
        <ReactTable
          data={this.state.tableData}
          columns={exceptionColumns2}
          defaultPageSize={20}
          className="-striped -highlight"
        />
      </>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    user: state.auth.user,
  };
}
export default jswithRouter(jswithLoader(connect(mapStateToProps)(MilestoneExceptions2)));
