import React from 'react';
import { Row, Button, Card,Dropdown,ButtonToolbar,ButtonGroup,DropdownButton, ToggleButton } from 'react-bootstrap';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import { CSVLink } from 'react-csv';
import { withLoader } from '../../components';
import { Link } from 'react-router-dom';
import VerticalModal from '../../components/VerticalModal';
import MilestoneIntervals from '../../components/MilestoneIntervals';
import { exceptionColumns, factory_appointment_dateColumns, factory_booking_dateColumns, factory_delivery_dateColumns, distinctSkuColumns, distinctSkuColumnsnew} from '../columns/milestoneColumns';
//import { optionCSS } from 'react-select/lib/components/Option';
import { AssetType } from "tw-api-common";

const filterOptions = [
  { label: "Purchase Order", value: "purchase order" },
  { label: "Issue", value: "msg" },
  { label: "Factory", value: "factory" }
];

const destOptions = [
  { label: "All Destinations", value: "All" },
  { label: "Bangkok", value: "Bangkok" },
  { label: "Jakarta", value: "Jakarta" },
  { label: "Seattle", value: "Seattle" },
];

const exceptionOptions = [
  { label: "No booking from factory to 3PL", value: "factory_booking_date" },
  { label: "Booking not approved by 3PL", value: "factory_appointment_date" },
  { label: "Retailer not approved product delivery to 3PL", value: "factory_delivery_date" },
  { label: "Delayed in transit between factory to 3PL", value: "receive_date" },
  { label: "Stock available in 3PL MCC with no shipment allocation", value: "shipment_allocation_date" },
  { label: "Customer allocation order placed but not shipped", value: "atd" },
];


const createDdi = 
  (opts) => opts.map((o,i) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

const getLabel = (opts, value) => {
  let l = opts.find((o) => (o.value === value));
  return l ? l.label : "";
}


//Using this dictionary to decide what the issue is based on lastPopulatedColumn when 
//creating issue and sending props to create issue page
const issueNames = {
  create_date: {
    name: "No booking from factory to 3PL",
    Low: ["factory"],
    Medium: ["factory", "global_retailer"],
    High: ["factory", "global_retailer"]
  },
  factory_booking_date: {
    name: "Booking not approved by 3PL",
    Low: ["3PL"],
    Medium: ["3PL", "global_retailer2"],
    High: ["3PL", "global_retailer2"]
  },
  factory_appointment_date: {
    name: "Retailer not approved product delivery to 3PL",
    Low: ["global_retailer3"],
    Medium: ["global_retailer3", "global_management"],
    High: ["global_retailer3", "global_management"]
  },
  factory_delivery_date: {
    name: "Delayed in transit between factory to 3PL",
    Low: ["3PL2"],
    Medium: ["3PL2", "global_retailer4"],
    High: ["3PL2", "global_retailer4"]
  },
  receive_date: {
    name: "Stock available in 3PL MCC with no shipment allocation",
    Low: ["local_retailer"],
    Medium: ["local_retailer"],
    High: ["local_retailer"]
  },
  shipment_allocation_date: {
    name: "Customer allocation order placed but not shipped",
    Low: ["3PL3"],
    Medium: ["3PL3", "global_retailer5"],
    High: ["3PL3", "global_retailer5"]
  }
};

class MilestoneExceptions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      tableData: [],
      columns: exceptionColumns,
      poList: [],
      exceptionList: [],
      selectFields: [],
      selectOptions: [],
      headers: [],
      checked: false,
      showPopUp: false,
      submitting: false,
      selectValue: "All",
      filterValue: "purchase order",
      successMessage: '',
      errorMessage: '',
      destSelected: "All",
      csvLink: '',
      page: 0,
      //changing this for development purposes only 
      shown: props.shown ? props.shown : true
    };
    this._isMounted = true;
    this.reactTable = React.createRef();

  }

  componentDidMount() {
    if (!this.props.hide) {
      this.refreshPage();
    }
  }

  componentDidUpdate(prevProps){
    if(prevProps.hide !== this.props.hide && this.state.data.length<1) {
      this.refreshPage()
    }
    if (this.props.searchText !== prevProps.searchText) {
      this.searchSubmitted(this.props.searchText)
    }
  }

  /**
   * Refreshes the page with new data.
   * Mainly used for when the user changes intervals. We would like them
   * to see the effect of their changes right away.
   */

  async refreshPage() {
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');
    let data = await this.props.client.queryMilestoneExceptions(this.props.user.org_id);
    //console.log(data)
    console.log("REFRESH");
    if (this._isMounted) {
      this.setSelectFields(data);
    }
    data.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));
    if (this._isMounted) {
      this.setState({
        data: data,
        tableData: data,
        csvData: [],
      }, () => this.setExportData());
    }

    /*if (sessionStorage.length > 0) {
      this.hydrateStateWithSessionStorage();
    }*/

    stopLoading();
  }

  hydrateStateWithSessionStorage() {
    //dest
    //checked
    //page
    //filter
    //select
    
    console.log("IN HYDRATE");
    let options; 
    let list;
    let filter = JSON.parse(sessionStorage.getItem("filter"));
    let select = JSON.parse(sessionStorage.getItem("select"));
    switch(filter) {
      case "purchase order":
        list = this.state.poList;
        options = this.state.poOptions;
        break;
      case "msg":
        list = this.state.exceptionList;
        options = this.state.exceptionOptions; 
        break;
      default:
        list = this.state.factoryList;
        options = this.state.exceptionOptions;
    }

    let index = 0;
    if (filter === "msg") {
      for(let i = 0; i < list.length; i++) {
        if (list[i].props.value === select) {
          index = i;
        }
      }
    } else {
      for(let i = 0; i < list.length; i++) {
        if (list[i].key === select) {
          index = i;
        }
      }
    }
    this.setState({
      filterValue: filter,
      selectFields: list,
      selectOptions: options,
      checked: sessionStorage["checked"] ? JSON.parse(sessionStorage["checked"]) : this.state.checked,
      destSelected: sessionStorage["dest"] ? JSON.parse(sessionStorage["dest"]) : this.state.destSelected
    }, () =>  { 
      this.onSelectChange(filter === "msg" ? list[index].props.value : list[index].key, true);
      this.setState({
        page: sessionStorage["page"] ? parseInt(sessionStorage["page"]) : this.state.page
      });
    });
  }

  /**
   * Creates the drop down lists for the bottom drop down select
   * @param {list} data 
   */
  setSelectFields(data) {
    const { basePath } = this.props;
    let distinctPOs = [];
    let distinctFactories = [];

    let poOptions = [];
    let factoryOptions = [];

    console.log("INITIAL SET SELECT");

    for (let obj of data) {
      let vendorArr = obj.origin_container.split('&'); //this is to isolate the vendor code from origin_container

      obj.pic = obj.image ? <img style={{ width: 75, height: 75 }} src={obj.image} alt={"Failed to Load"} /> : "No Image";
      obj.skuLink = <Link to={basePath + "/items/detail/" + obj.sku}>{obj.sku}</Link>;
      obj.poLink = <Link to={basePath + "/purchaseorders/detail/" + obj.purchase_order_id}>{obj.purchase_order_id}</Link>;
      obj.issueButton = <Button size="sm" variant="link" onClick={() => this.onIssueClick(obj)}>{"Create Case for PO"}</Button>;
      obj.vendor_code = vendorArr[1];

      let po = obj.purchase_order_id;
      let factory = obj.origin_container;

      if (distinctPOs.indexOf(po) < 0) {
        distinctPOs.push(po);
        poOptions.push({ value: po, label: po});
      }

      if (distinctFactories.indexOf(factory) < 0) {
        distinctFactories.push(factory);
        factoryOptions.push({value: factory, label: obj.origin_name});
      }

    }

    poOptions.unshift({value: "All", label: "All POs"})

    this.setState({
      poOptions, exceptionOptions,factoryOptions, selectOptions: poOptions,
    });

  }

  /**
   * Handles the change of the top select on the page
   */
  onFilterChange = (value) => 
  {
    console.log("switched to " + value)
    if (value === "msg") {
      console.log(this.state.exceptionList[0]);
      this.setState({
        selectOptions: this.state.exceptionOptions,
        filterValue: value
      }, () => {
        if (this.state.exceptionOptions[0] != undefined) // avoid accessing undefined element
          this.onSelectChange(this.state.exceptionOptions[0].value, false)
      });
    } else if (value === "purchase order") {
      this.setState({
        selectOptions: this.state.poOptions,
        filterValue: value,
        checked: false
      }, () => {
        if (this.state.poOptions[0] != undefined) // avoid accessing undefined element
          this.onSelectChange(this.state.poOptions[0].value, false)
      });
    } else 
    {

      this.setState({
        selectOptions: this.state.factoryOptions, 
        filterValue: value
      }, () => {
        if (this.state.factoryOptions[0] != undefined) // avoid accessing undefined element
          this.onSelectChange(this.state.factoryOptions[0].value, false)
      });
    }
    sessionStorage.setItem("filter", JSON.stringify(value));
  }

  /**
   * Handles the change of the bottom select of the page. The one that changes according to which filter was set
   * @param {object} event
   * @param {boolean} fromHydrate tells this function if it's being called from hydrateState function
   */
  onSelectChange = (value, fromHydrate) => 
  {

    const { destSelected } = this.state;

    console.log("VALUE:", value);

    let newTable = [];
    let list = this.state.data;
    let field;
    if (this.state.selectOptions === this.state.poOptions) {
      if (value === "All") {
        if (destSelected !== "All") {
          for (let obj of list) {
            if (obj['dest_name'] === destSelected) {
              newTable.push(obj);
            }
          }
        } else {
          newTable = this.state.data;
        }
      } else {
        field = "purchase_order_id";
      }
    } else if (this.state.selectOptions === this.state.exceptionOptions) {
      field = "unpopulatedColumn";
    } else {
      field = "origin_container";
    }


    //This code will not affect the newTable if value is "All"
    for (let obj of list) {
      if (destSelected !== "All") {
        if (obj[field] === value) {
          if (obj['dest_name'] === destSelected) {
            newTable.push(obj);
          }
        }
      } else {
        if (obj[field] === value) {
          newTable.push(obj);
        }
      }
    }

    let newColumns = exceptionColumns;
    if (!this.state.checked) {
      if (value === "factory_booking_date") {
        newColumns = factory_booking_dateColumns;
      } else if (value === "factory_appointment_date") {
        newColumns = factory_appointment_dateColumns;
      } else if (value === "factory_delivery_date") {
        newColumns = factory_delivery_dateColumns;
      }
    } else {
      newColumns = distinctSkuColumns;
    }

    console.log("settings state: " + value);
    this.setState({
      tableData: newTable,
      columns: newColumns,
      selectValue: value,
      page: fromHydrate ? this.state.page : 0,
      csvData: [],
      csvLink: ''
    }, () => { this.state.checked && this.state.selectFields !== this.state.poList ? this.onCheckboxClick(true, fromHydrate) : this.setExportData() });

    sessionStorage.setItem("select", JSON.stringify(value));
    if (!fromHydrate) {
      sessionStorage.setItem("page", 0);
    }

  }

  /**
   * Every time there is an update to the page, this function is called to update the data
   * to be exported such that it reflects what's on the page
   */
  setExportData() {

    let list = this.state.columns;
    let data = this.state.tableData;
    let csvHeaders = [];

    for (let col of list) {
      let key = "";
      switch (col.Header) {
        case "Purchase Order ID":
          key = "purchase_order_id";
          break;
        case "Product SKU":
          key = "sku";
          break;
        default:
          key = col.accessor;
      }

      if (col.Header !== "Photo" && col.Header !== "Create Case") {
        let obj = {
          label: col.Header,
          key: key
        }
        csvHeaders.push(obj);
      }
    }

    //Info that Emma wants in the exported data that is not in the table
    csvHeaders.push({
      label: "Vendor Code",
      key: this.state.checked ? "info.vendor_code" : "vendor_code"
    });

    csvHeaders.push({
      label: "Vendor Name",
      key: this.state.checked ? "info.factory" : "origin_name"
    });

    csvHeaders.push({
      label: "Port of Loading",
      key: this.state.checked ? "info.pol" : "pol_name"
    });

    this.setState({
      headers: csvHeaders,
      csvLink: <CSVLink data={data} headers={csvHeaders} filename={"issue_report.csv"}><Button variant="outline-primary">Download Table</Button></CSVLink>
    });

  }

  /**
   * Handles exactly what you think it does
   * Will only be called in fromHydrate call chain if the user left the page with the checkbox checked.
   * @param {boolean} check 
   * @param {boolean} fromHydrate let's me know if it's being called within fromHydrate call chain
   */
  onCheckboxClick(check, fromHydrate) {


    if (check) {

      let list = this.state.tableData;
      let distinctSku = []
      let newTable = []

      for (let row of list) {
        if (distinctSku.indexOf(row.sku) < 0) {
          distinctSku.push(row.sku);
          newTable.push({
            sku: row.sku,
            skuLink: row.skuLink,
            info: {
              prodName: row.prodName,
              factory: row.origin_name,
              vendor_code: row.vendor_code,
              quantity: row.quantity,
              department: row.department,
              dest_name: row.dest_name,
              msg: row.msg,
              timestamp: row.timestamp,
              pic: row.pic,
              image: row.image,
              issueButton: row.issueButton,
              vendorCode: row.vendor_code,
              pol: row.pol_name
            }
          });
        } else {
          for (let sku of newTable) {
            if (row.sku === sku.sku) {
              if (sku.info.factory != undefined && sku.info.factory !=null && sku.info.factory.indexOf(row.origin_name) < 0) {
                sku.info.factory.push(row.origin_name);
              }
              sku.info.quantity += row.quantity;
            }
          }
        }
      }

      this.setState({
        tableData: newTable,
        columns: distinctSkuColumnsnew,
        checked: check,
        page: fromHydrate ? this.state.page : 0,
        csvLink: ''
      }, () => {
        sessionStorage.setItem("page", this.state.page); 
        this.setExportData();
      });

    } else {
      this.setState({
        checked: check
      }, () => this.onSelectChange(this.state.selectValue, false));
    }
    sessionStorage.setItem("checked", JSON.stringify(check));
  }

  onDestChange = (value) => {
    console.log("dest = " +value)
    this.setState({
      destSelected: value
    }, () => this.onSelectChange(this.state.selectValue, false));
    sessionStorage.setItem("dest", JSON.stringify(value));
  }

  /**
   * Terrbile function that collects all of the information necessary to pass to the Create Issue page
   * @param {object} row 
   */
  async onIssueClick(row) {
    console.log(row)
    let issue = issueNames[row.populatedColumn].name;
    let orgs = issueNames[row.populatedColumn][row.status];
    let purchase_order = row.purchase_order_id;
    let vendor = row.location ? row.location : row.origin_container;
    const { basePath } = this.props;

    let propsObj = {
      status: row.status,
      issue: issue,
      purchase_order: purchase_order,
      vendor: vendor
    };

    //propsObj.rows = rows;
    //console.log(propsObj);
    this.props.history.push({
      pathname: basePath + "/cases/create",
      state: {
        casex:{'priority': propsObj.status,
        'subject': propsObj.issue,
        'subType': propsObj.issue,
        'type' : 'Milestone Exceptions'},
        purchase_order: purchase_order,
        vendor: vendor.slice(vendor.indexOf('&')+1),
        orgs: orgs,
        assets: [{ id: purchase_order, type: AssetType.PURCHASE_ORDER }, { id: vendor.slice(vendor.indexOf('&')+1), type: AssetType.VENDOR }],
        //rows: propsObj.rows
      }
    });
  }


  /**
   * Handles the submit button on the pop up window that allows the change of intervals
   * @param {object} intervals 
   */
  async handleSubmit(intervals) {
    this.setState({
      submitting: true
    });

    let req = await this.props.client.changeConfigSettings(intervals);
    if (!req.errorMessage) {
      this.setState({
        successMessage: "Changes successfully submitted",
        errorMessage: ""
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            submitting: false,
            showPopUp: false
          });
        }, 1500);
      });
      this.refreshPage();
    } else {
      this.setState({
        errorMessage: req.errorMessage,
        submitting: false
      });
    }
  }

  searchSubmitted(sku) {
    let list = this.state.data;
    let newTable = [];
    let noResults = true;
    // looping through on empty search is totally unecessary
    // will refactor later
    for (let obj of list) {
      if (obj.sku === sku || (!sku || !sku.length)) {
        newTable.push(obj);
      }
    }
    newTable.sort((a, b) => (a.purchase_order_id > b.purchase_order_id) ? 1 : ((b.purchase_order_id > a.purchase_order_id) ? -1 : 0));
    if (!newTable.length) {
      newTable = list;
    } else {
      noResults = false;
    }



    this.setState({
      tableData: newTable,
      inputValue: '',
      noResults: noResults,
      page: noResults ? this.state.page : 0,
      csvLink: '#'
    }, () => this.setExportData());

  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { csvLink, submitting, successMessage, errorMessage,selectOptions } = this.state;

    let modalClose = () => this.setState({ showPopUp: false, errorMessage: '', successMessage: '' });
    // let searchInvalidMsg = "No Results";

    if ((!this.props.shown && this.props.shown !== false) || this.props.shown === true) {
      return (
        <>
          <VerticalModal show={this.state.showPopUp}
            size={"md"}
            title={"Edit Time Intervals"}
            onHide={modalClose}
            page={<MilestoneIntervals submitting={submitting} onSubmit={this.handleSubmit} successMessage={successMessage} errorMessage={errorMessage} />}
          />
          <Row>
            <ButtonToolbar className="toolbar">

              <ButtonGroup className="filter" >
                <DropdownButton title={getLabel(filterOptions, this.state.filterValue)} onSelect={this.onFilterChange}>
                  { createDdi(filterOptions) }
                </DropdownButton>

                <DropdownButton title={getLabel(selectOptions, this.state.selectValue)} onSelect={this.onSelectChange}>
                  { createDdi(selectOptions) }
                </DropdownButton>
  
               {this.state.selectOptions !== this.state.poOptions 
                ? <ToggleButton type="checkbox" onChange={() => { this.onCheckboxClick(!this.state.checked, false) }} checked={this.state.checked} > 
                    Distinct SKUs 
                  </ToggleButton> 
                : ''}

                <DropdownButton title={getLabel(destOptions, this.state.destSelected)} onSelect={this.onDestChange}>
                  { createDdi(destOptions) }
                </DropdownButton>

              </ButtonGroup>

              <ButtonGroup className="action">
                <Button size="md" onClick={() => { this.setState({ showPopUp: true }) }}>
                  Edit Exception Intervals
                </Button>&nbsp;
                  {csvLink}
              </ButtonGroup>
            </ButtonToolbar>
          </Row>
          <Card style={{ marginTop: 15 }}>
            <ReactTable
              style={{ height: "800px" }}
              page={this.state.page}
              ref={this.reactTable}
              data={this.state.tableData}
              columns={[{ Header: <font size="3">{"Total Rows: " + this.state.tableData.length}</font>, columns: this.state.columns }]}
              onPageChange={page => { sessionStorage.setItem("page", page); this.setState({ page: page });}}
              className="-striped -highlight itemstable" />
          </Card>
        </>
      )
    } else {
      return null;
    }
  }

}
function mapStateToProps(state) {
  return { client: state.auth.client, user: state.auth.user }
}

export default withLoader(connect(mapStateToProps)(MilestoneExceptions));