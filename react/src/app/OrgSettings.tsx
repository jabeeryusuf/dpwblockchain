import React from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { withLoader } from "../components";
import { Subheader } from "../layout";
import { getEnabledCategories } from "trace_events";
import { FormGroupSelect, FormMessageFromTemplate } from "../components/forms";
import { conversationActions } from "../redux/actions";
import FilePondComponent from "./Cases/FilePondComponent";
const dblogo = require("../images/honda-logo.png");

//const s3URL = "http://teuchain-public.s3.us-east-2.amazonaws.com/email-logos/";//
const s3URL = "https://s3-eu-west-1.amazonaws.com/dpwchainpublic/logo/";

const FormCaseRow = (props: any) => (
  <Form.Group as={Row} controlId="formCaseSubject">
    <Form.Label className="col-2">
      {props.title} :
    </Form.Label>
    <Col sm="8">
      {props.children}
    </Col>
  </Form.Group>
);

class OrgSettings extends React.Component<any, any> {
  private pond: React.RefObject<any>;
  constructor(props: any) {
    super(props);
    this.pond = React.createRef();
    this.state = {
      updated: {
        logo: null
      }
    };
    this.handleFilePondUpdate = this.handleFilePondUpdate.bind(this);
  }
  public async getEmailLogo() {
    const logo = await this.props.orgApi.getSetting(this.props.org.org_id, "logo");
    console.log("logo = ", logo.value);

    this.setState({
      emailLogo: logo.value,
    });
  }
  public componentDidMount() {
    this.getEmailLogo();
  }

  public async updateOrgSettings() {

    for (let key in this.state.updated) {
      if (this.state.updated[key]) {
        const orgSettingObj = {} as any;
        orgSettingObj.orgId = this.props.org.org_id;
        orgSettingObj.key = key;
        orgSettingObj.value = this.state.updated[key];
        await this.props.orgApi.updateSetting(orgSettingObj);
      }
    }
    this.getEmailLogo();
    this.pond.current.clearFiles();
  }

  public renderPreviewMessage() {
    if (this.state.previewMessage.length > 0) {
      return (
        <p style={{ color: "green" }}> {this.state.previewMessage} </p>
      );
    }
  }

  public async handleFilePondUpdate(files: any) {
    this.setState({
      updated: Object.assign(this.state.updated,{logo:files[0]})
    });
  }

  public render() {
    console.log("ORG SETTINGS:");
    console.log(this.props);

    return (
      <>
        <Subheader title="Org Settings">

        </Subheader>
        <Row>
          <Col sm="4">
            <FormCaseRow title="Current Logo">
              <img style={{ maxHeight: "100px", maxWidth: "200px", width: "auto", height: "auto" }}
                            src={s3URL + this.state.emailLogo} alt={this.props.org.name} />
            </FormCaseRow>
          </Col>
          <Col sm="8">
            <FormCaseRow title="Upload new Logo">
              <FilePondComponent permission={"public"} apiUrl={this.props.client.apiUrl} ref={this.pond}
                handleFilePondUpdate={this.handleFilePondUpdate} user={this.props.user.user_id} maxUploads={1} />
            </FormCaseRow>
          </Col>
        </Row>
        <Button onClick = {() => {this.updateOrgSettings()}} type="submit">Save Updates</Button>

      </>
    )
  }
}

function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    orgApi: state.auth.orgApi,
    user: state.auth.user,
  };
}

const mapDispatchToProps = (dispatch: any) => ({
  sendMessage: (groupId: any, message: any, files: any) =>
    dispatch(conversationActions.sendMessage(groupId, message, files)),
});

export default withLoader(connect(mapStateToProps, mapDispatchToProps)(OrgSettings));
