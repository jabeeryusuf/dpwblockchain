import React from 'react';
import { Button,Card, Row, Col } from 'react-bootstrap';

import Subheader from '../layout/Subheader';
import { Map, TileLayer, Popup, Marker } from 'react-leaflet';
import {StormView, AffectedView} from '../components/StormView';
import GreenPortlet from '../components/GreenPortlet'
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import { withLoader } from '../components';
import { connect } from 'react-redux';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


const stormType = {
    "TD": "Tropical Depression",
    "TS": "Tropical Storm",
    "H": "Hurricane",
    "TY": "Typhoon"
}

const stormCat = {
    "TD": "Tropical Depression",
    "TS": "Tropical Storm",
    "H1": "Category 1 Hurricane",
    "H2": "Category 2 Hurricane",
    "H3": "Category 3 Hurricane",
    "H4": "Category 4 Hurricane",
    "H5": "Category 5 Hurricane",
    "TY": "Typhoon",
    "STY": "Super Typhoon"
}

const stormBasin = {
    "AL": "Atlantic",
    "EP": "Eastern Pacific",
    "CP": "Central Pacific",
    "WP": "Western Pacific",
    "IO": "Indian Ocean",
    "SH": "Southern Hemisphere"
}

let stormApi = 'https://api.aerisapi.com/tropicalcyclones?';

// const accessIdOne = 'zqXOBGWa2UzqXv96tWBvj';
// const secretKeyOne = '718VpCGkgNdX4OHTC9JIS2CHI037KXlNSMIOlHPM';


const accessIdOne = 'HQHBwhwZmogiraOnsYdDK';
const secretKeyOne = '9ELWiT4jf9ICIXN7PT6UQiyuflh7Dz6MmXQUEJaP';


//https://maps.aerisapi.com/R0YhifLIIw0fIeGqOd4JX_FJoS8nukddVP3hBUcHEOt4uv8XUN113pnIMTVshJ/stormcells,stormreports/{z}/{x}/{y}/current.png


//const accessIdOne = 'vCV7x6lobIzS2crUqknzI';
//const secretKeyOne = '9t9CazO1OYr0sXTzgp1s9N99KVgOaw45559XTZks';
//let mapstaticurl = "https://maps.aerisapi.com/R0YhifLIIw0fIeGqOd4JX_FJoS8nukddVP3hBUcHEOt4uv8XUN113pnIMTVshJ/land-flat-dk:75,water-depth,radar,admin-cities-dk,tropical-cyclones-names,tropical-cyclones/800x480/21.943,-70.3125,1/current.png";

let maprooturl = 'https://maps.aerisapi.com/'+accessIdOne+'_'+secretKeyOne+'/';
let mapUrl = maprooturl+'land-flat-dk:75,water-depth,radar,admin-cities-dk,tropical-cyclones-names,tropical-cyclones/{z}/{x}/{y}/current.png';
let accessId = accessIdOne;
let secretKey = secretKeyOne



if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    //mapUrl = 'https://maps.aerisapi.com/SGrBrYJ2LhWOSViPLsdXj_dGgdWkNt4Sc8RFWhYjP2J1FvyKvZB1tHOWO5JUB9/land-flat-dk:75,water-depth,radar,admin-cities-dk,tropical-cyclones-names,tropical-cyclones/{z}/{x}/{y}/current.png';
    //mapUrl = 'https://maps.aerisapi.com/FKrNzmqvpttAk4QSqRupe_G6qt6IzWWGZ6UeTVZ4ymn2toqWVVVghSqh8iA5Y7/flat-dk,water-depth,admin-cities-dk,tropical-cyclones,tropical-cyclones-names/{z}/{x}/{y}/current.png';
    //mapUrl = 'https://maps.aerisapi.com/SGrBrYJ2LhWOSViPLsdXj_dGgdWkNt4Sc8RFWhYjP2J1FvyKvZB1tHOWO5JUB9/land-flat-dk:75,water-depth,radar,admin-cities-dk,tropical-cyclones-names,tropical-cyclones/800x800/21.943,-70.3125,1/current.png';

    //secretKey = secretKeyOne;
}

class WeatherTracker extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            storms: [],
            buildings: [],
            vehicleLocations: [],
            markers: [],
            zoom: 3,
            lat: 43.505,
            lng: -0.09,
            popup: null,
            buildingPort: null,
            affected_buildings: null,
            affected_vehicleLocations:null
        }

        //this.onClicky = this.onClicky.bind(this);
    }

    async componentDidMount() {
        const { startLoading, stopLoading } = this.props;
        startLoading('Loading your data...');
        let fetchOptions = {
            method: "GET",
            headers: { "content-type": "application/json" }
        };

        let url = stormApi + 'client_id=' + accessId + '&client_secret=' + secretKey;
        console.log('url= %s', url);

        let resp = await fetch(url, fetchOptions);
        let json = await resp.json();

        await this.parseResponse(json);

        await this.getBuildings();

        await this.getVehicleLocations();
        //await this.getAffected();

        stopLoading();


    }

    /**
     * Parses the reponse of the db query object
     * @param {*} json 
     */
    async parseResponse(json) {
        let stormList = [];
        let key = 0;
        for (let storm of json.response) {
            let obj = {
                "key": key,
                "name": storm.position.details.stormName,
                "basin": stormBasin[storm.profile.basinCurrent],
                "stormType": stormType[storm.position.details.stormType],
                "stormCat": stormCat[storm.position.details.stormCat],
                "lat": storm.position.loc.lat,
                "lng": storm.position.loc.long,
                "movement": storm.position.details.movement.direction,
                "movementSpeedKPH": storm.position.details.movement.speedKPH,
                "movementSpeedMPH": storm.position.details.movement.speedMPH,
                "windSpeedKPH": storm.position.details.windSpeedKPH,
                "windSpeedMPH": storm.position.details.windSpeedMPH,
                "maxWindSpeedKPH": storm.profile.windSpeed.maxKPH,
                "maxWindSpeedMPH": storm.profile.windSpeed.maxMPH,
            }
            stormList.push(obj);
            key++;
        }
        console.log(stormList);

        this.setState({ storms: stormList });

    }

    /**
     * Queries db for all buildings
     */
    async getBuildings() {
      let {client} = this.props;
      //console.log("Buildings are as below before-----------");
      console.log("Org ID is");
      console.log(this.props.org.org_id);
      client.queryBuildings("org_id ='"+this.props.org.org_id+"'")
      .then(res => {
        
        //console.log("Buildings are as below");
        //console.log(res);


        this.setState({
          buildings: res.rows
        });
      }).catch(err => console.log(err));
    }


    async getVehicleLocations() {
        let {client} = this.props;
        //console.log("Vehicle locations are as below before-----------");
        client.getVesselLocations(this.props.org.org_id)
        .then(res => {
          
          //console.log("Vehicle locations are as below");
          //console.log(res);  
  
          this.setState({
            vehicleLocations: res.rows
          });
        }).catch(err => console.log(err));
      }

    
    /**
     * hanldes the button clicks on the colored portlets underneath the map
     * @param {} name 
     */
    onClicky(name) {
        //console.log("Executed before check onClicky");
        //this.setState({buildings: []});
        if (name === "Green Close") {
            this.setState({
                buildingPort: null,
                markers: []
            });
        } else {
            let storms = this.state.storms;
            for (let storm of storms) {
               
                if (storm.name === name && this.state.buildings.length>0) {
                   

                    //console.log("Executed after check onClicky");
                    let inDanger = this.findThreat(storm);
                    let building_codes = [];
                    let vessel_codes = [];
                    let codes = [];
                    let affected_buildings_storm = [];
                    let affected_vehicleLocations_storm = [];

                    for (let obj of inDanger) {
                       
                        let str="none";
                        if(obj.type=="Building"){
                            affected_buildings_storm.push(obj);
                            let container_id = obj.container_id;
                            let indexOne = container_id.indexOf('&');
                            str = " " + container_id.substring(indexOne + 1, container_id.length) + ",";
                            building_codes.push(str);
                        }
                       else if(obj.type=="Vessel") {
                            affected_vehicleLocations_storm.push(obj);
                            //let vessel_id = obj.vessel_container_id;
                            //let indexOne = vessel_id.indexOf('&');
                            //str = " " + vessel_id.substring(indexOne + 1, vessel_id.length) + ",";
                            str = obj.vessel_name;
                            vessel_codes.push(str);
                        }
                        
                        
                        codes.push(str);

                    }

                    //console.log("Affected Buildinsg are");                    
                    //console.log(affected_buildings_storm);
                    //console.log("Affected Vehicle locations are");
                    //console.log(affected_vehicleLocations_storm);

                    window.scrollTo(0, 0);
                    this.setState({
                        lat: storm.lat,
                        lng: storm.lng,
                        zoom: 6,
                        popup: {
                            key: 0,
                            position: {
                                lat: storm.lat,
                                lng: storm.lng
                            },
                            contentb: "Buildings possibly affected: " + affected_buildings_storm.length,
                            contentv: "Buildings possibly affected: " + affected_vehicleLocations_storm.length,
                            buildings: affected_buildings_storm,
                            vessels: affected_vehicleLocations_storm,
                            Codes: codes,
                            buildingcodes:building_codes,
                            vesselcodes:vessel_codes
                        },
                        buildingPort: null,
                        markers: []
                        
                    });

                    this.setState({
                        affected_buildings: affected_buildings_storm,
                        affected_vehicleLocations : affected_vehicleLocations_storm
                      });




                    console.log("Affected Buildinsg are");                    
                    console.log(this.state.affected_buildings);
                    console.log("Affected Vehicle locations are");
                    console.log(this.state.affected_vehicleLocations);

                    console.log("POpupis");                    
                    console.log(this.state.popup);
                }
            }
        }
    }
    


    /**
     * Runs through all buildings and calculates the distance between them and the storm.
     * Adds them to a list if the building is within 250 km of the storm
     * @param {*} storm 
     */
    findThreat(storm) {
        let inDanger = [];
        let buildings = this.state.buildings;
        let vesseles = this.state.vehicleLocations;
        for (let building of buildings) {
            let dist = this.calculateDistance(storm, building);
            if (dist <= 10000) {
                building.type="Building";
                inDanger.push(building);
            }
        }

        for (let vessel of vesseles) {
            let dist = this.calculateDistance(storm, vessel);
            if (dist <= 10000) {
                vessel.type="Vessel";
                inDanger.push(vessel);
            }
        }

        return inDanger;
    }


    /**
     * Distance calculation in km
     * @param {*} storm 
     * @param {*} building 
     */
    calculateDistance(storm, building) {
        let lat1 = storm.lat;
        let lat2 = building.lat;
        let lng1 = storm.lng;
        let lng2 = building.lng;
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLng = this.deg2rad(lng2 - lng1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLng / 2) * Math.sin(dLng / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }


    /**
     * Handles the clicks for the "Buildings" button in the popups on the map. 
     * Creates a list of markers to display on the map and the Building Codes Portlet above the storms
     * @param {*} buildingCodes 
     * @param {*} buildings 
     */
    handleClick(buildingCodes, buildings, type) {
        let item = {
            name: "buildingList",
            content: buildingCodes
        }

        let markerList = []
        for (let building of buildings) {
            if(type=="Building")
            {
                building.title = building.container_id;
            }
            
            else if (type=="Vessel")
            {
                building.title = building.vessel_name;
            }
            markerList.push(building);
        }

        this.setState({
            popup: null,
            buildingPort: item,
            markers: markerList
        })
    }

    

     getAffected(name) {

        console.log("Executed before check");
        let storms = this.state.storms;
        let buildings = this.state.buildings;

        let inDangerlist = [];

        for (let storm of storms) {
            if (storm.name === name) {
                console.log("Executed after check");
                inDangerlist = this.findThreat(storm);
            }
        }
        console.log("List of Storms");
        console.log(storms);
        console.log("List of Buildings");
        console.log(buildings);

        
    }



    renderMap() {
        let position = [this.state.lat, this.state.lng];
        let styles = {
            height:'800px',
            backgroundImage: `url(${mapUrl})`,
            backgroundRepeat: 'repeat-y',
            backgroundSize: 'cover'
          };


        console.log('rendering map');
        return (

            <React.Fragment>
               {/** <div style={styles}>
                    
               </div>
            
            */}

            
            <Map style={{height: '480px', backgroundRepeat: 'no-repeat'}}  zoom={this.state.zoom} popup={this.state.popup} center={position}>
                <TileLayer
                    url={mapUrl}
                />
                {this.state.markers.length > 0 && this.state.markers.map((building, idx) =>
                    <Marker key={'marker-${idx}'} position={[building.lat, building.lng]}>
                        <Popup>
                            <span>{building.title}</span>
                        </Popup>
                    </Marker>
                )}
                {this.state.popup &&
                    <Popup
                        key={this.state.popup.key}
                        position={this.state.popup.position} >
                        <div>
                            <p>{this.state.popup.contentb}</p>
                            <p>{this.state.popup.contentv}</p>
                            {this.state.popup.buildings.length > 0 &&
                            
                                <Button buttonText='View Buildings' onClick={() => this.handleClick(this.state.popup.buildingcodes, this.state.popup.buildings, "Building")}>View Buildings </Button>}
                                <Button buttonText='View Vessels' onClick={() => this.handleClick(this.state.popup.vesselcodes, this.state.popup.vessels, "Vessel")}>View Vessels</Button>}
                        </div>
                    </Popup>
                }
            </Map> 
            
                 
            </React.Fragment>

           
        )
    }

    render() {
        return (
            <>
                <Subheader title="Weather Tracker" />
                <Card>
                    {this.renderMap()}
                </Card>
                <Row className="affected_buildings_vessels">
                {this.state.buildingPort &&

                    <Row>
                    <div className="col-md-6">
                    
                    {this.state.affected_buildings &&
                    <AffectedView title="Builidings Possibly Affected" items={this.state.affected_buildings} closeButton = {() => this.onClicky("Green Close")}>

                    </AffectedView>
                    }
                    </div>

                    <div className="col-md-6">
                    {this.state.affected_vehicleLocations && 
                    <AffectedView title="Vessels Possibly Affected" items={this.state.affected_vehicleLocations}  closeButton = {() => this.onClicky("Green Close")} >
                        
                    </AffectedView>
                    }
                    </div>
                   
               </Row>
                  
                }
                </Row>
                {/*
                <Row>
                    {this.state.buildingPort &&
                        <GreenPortlet header={"Building Codes"} content={this.state.buildingPort.content} onClick={() => this.onClicky("Green Close")} buttonText={"Close"} />
                    }
                </Row>
                 */}
                <Row>
                    {this.state.storms.length > 0 && this.state.storms.map((storm,index)=> {
                        return (
                            <Col key = {index}>
                                <StormView onClick={() => this.onClicky(storm.name)} storm={storm} buildingcount = {this.state.buildings.length}  vesselcount = {this.state.vehicleLocations.length} />
                                {/*<Button onClick={this.getAffected(storm.name)}></Button>*/}
                            </Col>
                        );
                    }, this)}
                </Row>

                
            </>
        )
    }
}

function mapStateToProps(state) {
  return { client: state.auth.client,
        org: state.auth.org }
};
  
export default withLoader(connect(mapStateToProps)(WeatherTracker));
