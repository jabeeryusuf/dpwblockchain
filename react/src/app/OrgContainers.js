import React from 'react';
import Subheader from '../layout/Subheader'
import { connect } from 'react-redux';
import { GMap } from '../components/GMap'
import Table from '../components/Table'
import { expediteColumns } from './columns/columns'
import { Link } from 'react-router-dom';
import { loadingActions } from '../redux/actions';
import PopUpWindow from '../components/PopUpWindow.js'
import Moment from 'moment'
import {Card, Button} from 'react-bootstrap'

const { Container } = require("tw-api-common")

const initialState = {
  markers: [],
  tableData: [],
  containers: [],
  errorMessage: '',
  successMessage: '',
  showPopUp: false,
  zoom: null,
  //creating this field to handle confirmation 
  pendingAction: null
};

class OrgContainers extends React.Component {
  constructor(props) {
    super(props);
    this.state = localStorage.getItem("appState") ? JSON.parse(localStorage.getItem("appState")) : initialState;
  }

  async componentDidMount() {
    const { dispatch } = this.props;
    console.log(this.props)
    const contClient = this.props.client.getContainersClient();
        
    dispatch(loadingActions.start('Loading your data...'));
    let myLocations = await contClient.getInventoryLocations(this.props.org.org_id?this.props.org.org_id:'demo');
    let containerList = [];
    for (let i = 0; i < myLocations.items.length; i++) {
      let containerObj = new Container(myLocations.items[i].container_id, myLocations.items[i].type, myLocations.items[i].ref, myLocations.items[i].parent_container_id)
      await containerObj.setContext(this.props.client, myLocations.cache);
      containerList.push(containerObj);
    }
    this.setState({
      containers: containerList
    })
    this.setTableData(containerList)
    this.setMarkers(myLocations.items);

    dispatch(loadingActions.stop());
  }

  setMarkers(data) {
    let markers = [];
    for (let i = 0; i < data.length; i++) {
      if (!data[i].parent_container_id || data[i].parent_container_id === 'NULL') {
        const event = new Date(data[i].eta);
        let marker = {
          container_id: data[i].container_id,
          lat: data[i].vessel_lat ? data[i].vessel_lat : data[i].lat,
          lng: data[i].vessel_lng ? data[i].vessel_lng : data[i].lng,
          eta: event.toLocaleDateString(navigator.language),
          location: data[i].name ? data[i].name : data[i].vessel_name,
          destination: data[i].pod,
          id: i,
          button: <Button buttonText="View Containers at Location" onClick={() => { this.handleMarkerClick(i) }} />,
          onClick: ()=>{return},
          onMouseOver: (i) => {
            this.setState({
              openWindow: i
            });
          },
          onMouseOut: () => {
            this.setState({
              openWindow: null
            });
          },
          color: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
        }
        markers.push(marker);
      }
    }
    this.setState({
      markers: markers
    });
  }
  async componentDidUpdate() {
    if (!this.state.zoom) {
      this.setState({
        zoom: 3
      })
    }
  }

  async submitAction() {
    let { container_id } = this.state.pendingAction.container;
    let {action} = this.state.pendingAction;
    let newRequest = await this.props.client.createAction('shipping-container&' + container_id, action, this.props.user.blockchain_id);
    this.setState({
      showPopUp: false
    });
    if (!newRequest.errorMessage) {
      this.setState({
        successMessage: 'Request successfully submitted',
        errorMessage: ''
      });
    }
    else {
      this.setState({
        successMessage: '',
        errorMessage: 'Something went wrong.  Have you already requested to expedite this container?'
      });
    }
  }

  async setTableData(data, max_parent_id) {
    let tableData = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].type === 'shipping-container') {
        let parentLocation = data[i];
        let current;
        while (parentLocation) {
          parentLocation = await parentLocation.getParent();
          if (parentLocation) {
            current = parentLocation;
            let { container_id, ref, type, parent_container_id } = parentLocation;
            parentLocation = new Container(container_id, ref, type, parent_container_id);
            await parentLocation.setContext(this.props.client, data[i].cache);
          }
          else {
            console.log(data[i])
          }
        }
        if (!max_parent_id || max_parent_id === current.container_id) {
          let row = {
            parent_container: current.container_id,
            destination: current.pod,
            location: current.name ? current.name : current.vessel_name,
            container_id: data[i].container_id.slice(19),
            expedite: <Button style={{ backgroundColor: 'transparent', margin: 0, width: '100%' }} buttonText='Expedite this container' />
          }
          tableData.push(row);
        }
      }
    }
    this.setState({
      tableData: tableData
    });
  }

  handleMarkerClick(id) {
    this.setTableData(this.state.containers, this.state.containers[id].container_id);
    window.scrollTo(0,400)
  }
  showPopUp(action) {
    this.setState({
      showPopUp: true,
      pendingAction: action
    });
  }

  onCancel() {
    this.setState({
      showPopUp: false,
      pendingAction: null
    });
  }

  renderPopUp() {
    if (this.state.pendingAction) {
      const confirmWindow = (
        <div>
          <p> Expedite container {this.state.pendingAction.container.container_id} at port {this.state.pendingAction.container.destination}? </p>
          <Button onClick={() => { this.submitAction() }}> Expedite</Button> <Button onClick={() => { this.onCancel() }}>Cancel </Button>
        </div>
      )
      return (
        <PopUpWindow shown={this.state.showPopUp} page={confirmWindow} />
      )
    }
  }
  onRowClick = (state, rowInfo, column, instance) => {
    return {
      onClick: () => {
        if (column.Header === 'Expedite') {
          let submitInfo = rowInfo.original;
          this.showPopUp({ container: submitInfo, action: "expedite" });
        }
      }
    }

  }

  renderTable() {
    return (
      <Card>
        <Table style={{ cursor: 'pointer' }} className="-highlight"
          data={this.state.tableData}
          columns={expediteColumns}
          getTdProps={this.onRowClick}
        />
      </Card>
    )
  }

  renderMap() {
    if (this.state.markers.length) {
      return (
        <Card>
          <GMap isMarkerShown
            markers={this.state.markers}
            zoom={this.state.zoom}
            openWindow={this.state.openWindow}
            center={this.state.center}
          />
        </Card>

      )
    }
    else {
      return (
        <Card>
          <p> We currently don't have any containers for you organization. <Link to='add-containers'> Click to add your containers </Link></p>
        </Card>
      )
    }
  }

  renderErrorMessage() {
    return (
      <div>
        {this.state.errorMessage &&
          <div>
            <p style={{ display: 'inline-block', color: 'red' }}>{this.state.errorMessage}</p>
            <Link style={{ paddingLeft: 5, display: 'inline-block' }} to={'/Actions'}> View current requests</Link>
          </div>
        }
        {this.state.successMessage &&
          <div>
            <p style={{ display: 'inline-block', color: 'green' }}>{this.state.successMessage}</p>
            <Link style={{ paddingLeft: 5, display: 'inline-block' }} to={'/Actions'}> View current requests</Link>
          </div>
        }
      </div>
    )

  }

  render() {
    return (
      <>
        <Subheader title="Dashboard" />
        {this.renderMap()}
        {this.renderErrorMessage()}
        {this.renderPopUp()}
        <Button buttonText='Show All Shipping Containers' onClick={() => this.setTableData(this.state.containers)} />
        <Link to='add-containers'> <Button buttonText='Add Containers' /></Link>
        <Link to='actions'> <Button buttonText='View My Requests' /></Link>
        {this.renderTable()}
      </>
    )
  }
}

function mapStateToProps(state) {
  return { client: state.userAuth.client, user: state.userAuth.user, org:state.orgAuth.org }
}
export default connect(mapStateToProps)(OrgContainers);
