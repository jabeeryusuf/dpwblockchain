import React from 'react';
import {connect} from 'react-redux';
import Table from '../components/Table';
import { consoleCurrentColumns, consolePastColumns } from './columns/columns';
import { Subheader } from '../layout';
import PopUpWindow from '../components/PopUpWindow';
import { Card, Button, Row, Col } from 'react-bootstrap';
import { ResolveRequest } from '../components/forms';

const currentColumns = [{ Header: <font size="3">Current Requests</font>, columns: consoleCurrentColumns }];
const pastColumns = [{Header: <font size="3">Past Requests</font>, columns: consolePastColumns }];

class PortConsole extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      tableData: [],
      org: "port&SEA",
      orgName: "Port of Seattle",
      showPopUp: false,
      currentRequest: null,
      columns: currentColumns,
      successMessage: '',
      errorMessage: ''
    };

    this.onCancel = this.onCancel.bind(this);
    this.handleWindowClick = this.handleWindowClick.bind(this);
    this.handleTabClick = this.handleTabClick.bind(this);

  }

  async componentDidMount() {
    this.setCurrentTable();
  }

  async setCurrentTable() {
    let data = await this.props.client.queryActions("requestee_id='" + this.props.org + "' AND action_status='pending'");
    for (let obj of data) {
      obj.container_id = obj.container_id.slice(19);
      obj.button = <Button size="sm" variant="link" onClick={() => this.handleLinkClick(obj)}>View Request</Button>
    }

    this.setState({
      tableData: data,
      columns: currentColumns
    });
  }

  async setPastTable() {
    let data = await this.props.client.queryActions("destination_container_id='" + this.props.org + "' AND action_status!='pending'");
    for (let obj of data) {
      if (obj.action_status === "approved") {
        obj.action_status = <font className="text-success">{obj.action_status}</font>
      } else {
        obj.action_status = <font className="text-danger">{obj.action_status}</font>
      }
      obj.container_id = obj.container_id.slice(19);
    }

    this.setState({
      tableData: data,
      columns: pastColumns
    });
  }

  handleLinkClick(request) {
    console.log(request)
    this.setState({
      showPopUp: true,
      currentRequest: request
    });
  }

  onCancel() {
    this.setState({
      showPopUp: false
    });
  }

  handleTabClick(str) {
    if (str === "current") {
      this.setCurrentTable();
    } else {
      this.setPastTable();
    }
  }

  async handleWindowClick(id, str) {
    let resolveRequest = await this.props.client.resolveRequest(id, str);
    if (!resolveRequest.errorMessage) {
      this.setState({
        successMessage: 'Request Resolved',
        errorMessage: ''
      });
      setTimeout(() => {
        this.setState({
          successMessage: '',
          showPopUp: false
        });
      }, 1500);
    } else {
      console.log(resolveRequest.errorMessage);
      this.setState({
        errorMessage: resolveRequest.errorMessage
      });
    }

    this.setCurrentTable();
  }

  render() {
    return (
      <>
        <Subheader title={"Console for " + this.state.orgName}/>
        <PopUpWindow shown={this.state.showPopUp} page={<ResolveRequest req={this.state.currentRequest} 
                                                                        onClick={this.handleWindowClick} 
                                                                        onCancel={() => this.onCancel}
                                                                        successMessage={this.state.successMessage}
                                                                        errorMessage={this.state.errorMessage} />} />
          <Row>
            <Col md={9}></Col>
            <Col md={3}>
              <Button onClick={() => this.handleTabClick("current")}>Current Requests</Button>{'\t'}
              <Button onClick={() => this.handleTabClick("past")}>Past Requests</Button>
            </Col>
          </Row>
          <br></br>
          <Card>
            <Table
              data={this.state.tableData}
              columns={this.state.columns}
              defaultPageSize={10}
              className="-striped -highlight" />
          </Card>
      </>
    )
  }
}
function mapStateToProps(state) {
	return { client: state.userAuth.client, user: state.userAuth.user }
}
export default connect(mapStateToProps)(PortConsole);