import React from 'react'
import Table from '../components/Table'
import { Subheader } from '../layout'
import { Role,User } from 'tw-api-common';
import {Card} from 'react-bootstrap';


const columns = [
  {
    Header: 'username',
    id: 'username',
    accessor: (d) => d.getName()
  },
  {
    Header: 'org',
    id: 'org',
    accessor: (d) => d.getOrgId()
  },
  {
    Header: 'email',
    id: 'email',
    accessor: (d) => d.getEmail()
  },
  {
    Header: 'role',
    id: 'role',
    accessor: (d) => d.getRole().getName()
  },

]


class UsersAdmin extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      users: [],
      active: 0,
    };
  }

  // Replace with db load
  async loadUsers() {
    
    let admin = new Role('Administrator');
    let ff = new Role('Freight Forwarder');
    let analyst = new Role('Financial Audit');

    let ag = 'Anko Global';
    let af = 'Finance';
    let sino = 'Sinotrans';


    let users = [
      new User('emma','emma@anko.com','Emma Toup',ag, admin),
      new User('joel','joel@kmart.au','Joel Niran',ag, admin),
      new User('henry','henry_zou@sinotrans.co.cn','Henry Zou',sino, ff),
      new User('g','gsun@sinontrans.co.cn','Guangming Sun',sino, ff),
      new User('dg','glenndav@anko.com','Glenn Davies',af, analyst),
      new User('rd','rudyd@anko.com','Rudy Darmawan',ag, analyst),
    ];
    return users;
  }

 
  // Did mount will load the roles
  async componentDidMount(){
    let users = await this.loadUsers();

    this.setState({ 
      users,
      active: 0
    });
  }

  async handleRoleClick(active) {
    
    let roles = this.state.roles;
    let privs = await this.allPrivsToTable(roles[active])
    
    //console.log(value);
    this.setState({active: active,
                   privs: privs});
  }

  render() {
    
    return (
        <>
        <Subheader title="Manage Users"/>
        <Card>
          <Card.Header>test</Card.Header>
          <Card.Body>
            <Table data={this.state.users}
                   columns={columns}  />
          </Card.Body>
        </Card>
        </>
    )
  }
}

export default UsersAdmin;
