import GenerateReport from './GenerateReport';
import UsersAdmin from './UsersAdmin';
import WeatherTracker from './Weather';
import Actions from './Actions';
import MyBuildings from './MyBuildings';
import PortConsole from './PortConsole';
import OrgContainers from './OrgContainers';
import ContainerUpload from './ContainerUpload';
import Invite from './Team/Invite';
import ShipmentDetail from './shipments/ShipmentDetail';
import {
  FiClock,
  FiUsers,
  FiMap,
  FiTruck,
  FiUmbrella,
  FiMapPin,
  FiArchive,
  FiShoppingBag,
  FiUser,
  FiActivity,
  FiLock,
  FiLayout,
  FiClipboard,
  FiCheckSquare,
  FiCloud
} from 'react-icons/fi'
import {
  FaTicketAlt,
  FaUsers
} from 'react-icons/fa'
import DetailLocation from './DetailLocation';
import Dashboard from './Dashboard';
import Groups from './GroupManager/Groups';
import MilestoneLanding from './milestones/MilestoneLanding';
import {
  PurchaseOrdersLanding
} from './PurchaseOrders/PurchaseOrdersLanding'

import ItemDetail from './items/ItemDetail'
import {
  ItemsLanding
} from './items/ItemsLanding'
import {
  ComingSoon
} from '../components';
import ViewConversation from './conversations/ViewConversation';
import ListConversations from './conversations/ListConversations';
import ContainersList from "./shipments/Containers/ContainerList";
import ListUsers from './user/ListUsers'
import FeedsLanding from './feeds/FeedsLanding'
import TeamLanding from './Team/TeamLanding';
import ShipmentReports from './ShipmentReports'
import FeedsErrorLog from './feeds/FeedsErrorLog';
import GroupDetail from './Team/GroupDetail';
import VendorLanding from './Vendors/VendorLanding';
import OrgSettings from './OrgSettings'
import PODetailView from './PurchaseOrders/PODetailView';
import {
  ShipmentsLanding
} from './shipments/ShipmentsLanding';
import ShipmentMap from './shipments/ShipmentMap';

import UserSignUp from '../auth/UserSignUp';

let devMenu = {
  groupTitle: 'Development',
  items: [{
      title: "Inventory",
      icon: FiClipboard,
      route: "/inventory",
    },
    {
      title: "Tracker",
      route: '/weather',
      icon: FiUmbrella
    }
  ]
};

const devGroups = [{
    groupTitle: "Messages",
    groupSelectPath: "/conversation/list",
    dynamic: true,
    itemsProp: 'conversations',
    itemShow: (conv) => (conv.tags['sidebar']),
    itemTitle: (conv) => (conv.subject ? conv.subject : conv.id),
    itemPath: (conv) => '/conversation/id-' + conv.conversation_id,
  },


  {
    groupTitle: "Team",
    groupSelectPath: "/user/list",
    dynamic: true,
    itemsProp: 'users',
    itemShow: (u) => (false),
    itemTitle: (u) => (u.name ? u.name : u.user_id),
    itemPath: (u) => '/user/' + u.user_id
  },
]



let devRoutes = [{
    path: "/weather",
    component: WeatherTracker
  },
  {
    path: "/shipmentMap",
    component: ShipmentMap,
  },

]



let routes = [{
    path: "/weather",
    component: WeatherTracker
  },
  {
    path: "/org-settings",
    component: OrgSettings
  },
  {
    path: "/vendors",
    component: VendorLanding
  },
  {
    path: "/team/group-detail",
    component: GroupDetail,
    fullscreen: true
  },

  {
    path: "/org-containers",
    exact: true,
    component: OrgContainers,
  },
  {
    path: "/",
    exact: true,
    component: Dashboard,
  },

  // Feeds
  {
    path: "/feeds/error-log",
    component: FeedsErrorLog,
    fullscreen: true
  },
  {
    path: "/feeds/:tab",
    component: FeedsLanding
  },


  // Purchase Orders
  {
    path: '/purchaseorders/detail/:po',
    fullscreen: true,
    component: PODetailView,
  },
  {
    path: '/purchaseorders/:statusfilter?',
    component: PurchaseOrdersLanding
  },



  // Items
  {
    path: '/items/detail/:productId',
    component: ItemDetail,
    fullscreen: true
  },
  {
    path: '/items',
    component: ItemsLanding
  },

  // Shipments(/:pathParam)
  {
    path: '/shipments/detail/:shipmentId/:shipmentIdnew?',
    component: ShipmentDetail,
    fullscreen: true
  },
  {
    path: '/shipments/:statusfilter?',
    component: ShipmentsLanding
  },



  {
    path: "/conversation/list",
    component: ListConversations,
    fullscreen: true,
  },
  {
    path: "/conversation/id-:id",
    component: ViewConversation,
  },
  {
    path: "/partners",
    component: ComingSoon,
  },
  {
    path: '/detail-location/:containerId?',
    fullscreen: true,
    component: DetailLocation,
  },
  {
    path: "/users-admin",
    component: UsersAdmin
  },
  {
    path: "/user/list",
    component: ListUsers,
    fullscreen: true
  },
  {
    path: "/generate-report",
    component: GenerateReport
  },

  {
    path: '/team/:tab',
    component: TeamLanding
  },
  {
    path: "/team/invite",
    component: Invite,
    fullscreen: true,
  },
  {
    path: '/actions',
    component: Actions
  },
  {
    path: '/my-buildings',
    component: MyBuildings
  },
  {
    path: '/port-console',
    component: PortConsole
  },
  {
    path: '/milestones/:tab',
    component: MilestoneLanding
  },
  {
    path: '/container-upload',
    component: ContainerUpload
  },
  {
    path: "/Dashboard",
    component: Dashboard
  },
  {
    path: '/groups/:tab',
    component: Groups
  },
  {
    path: '/shipment-reports',
    component: ShipmentReports
  },
  {
    path: '/containers',
    component: ContainersList
  },
  
  {
    path: "/signup",
    exact: true,
    component: UserSignUp,
  },

];




let sideMenu = (dpworld) => ([

  dpworld ?
  ({
    groupTitle: "Actions",
    items: [{
      title: "Late Shipments",
      route: "/shipment-reports",
      icon: FiClock
    }, ]
  }) :
  ({
    groupTitle: "WORK-SPACE",
    items: [{
        title: "Dashboard",
        route: "",
        icon: FiLayout
      },

      {
        title: "Milestones",
        route: "/milestones",
        tab: "tracker",
        icon: FiCheckSquare
      },
      {
        title: "Late Shipments",
        route: "/shipment-reports",
        icon: FiClock
      },

      {
        title: "Analytics",
        route: "/generate-report",
        icon: FiArchive
      },
    ]
  }),


  {
    groupTitle: "Explorer",
    items: [{
        title: "Purchase Orders",
        route: "/purchaseorders/all",
        icon: FiMap
      },
      {
        title: "Shipments",
        route: "/shipments/all",
        icon: FiTruck
      },
      {
        title: "Network",
        route: '/my-buildings',
        icon: FiMapPin
      },
      {
        title: "Containers",
        route: '/containers',
        tab: 'all',
        icon: FiLock
      },
      {
        title: "Products",
        route: '/items',
        tab: 'explorer',
        icon: FiShoppingBag
      },
      {
        title: "Tracker",
        route: "/weather",
        icon: FiCloud
      },
      {
        title: "Vendors",
        route: "/vendors",
        icon: FaUsers
      },

    ]
  },


  {
    groupTitle: "Settings",
    expanded: false,
    items: [{
        title: "Users",
        route: "/team",
        tab: "members",
        icon: FiUser
      },
      {
        title: "Org Settings",
        route: "/org-settings",
        icon: FiLock
      },
      {
        title: "Feeds",
        icon: FiActivity,
        route: "/feeds/list",

        path: "/feeds",
        component: FeedsLanding,
      },

    ]
  },

]);


// if (process.env.NODE_ENV === 'development' ) {

//   sideMenu = [...sideMenu, ...devGroups, devMenu];
//   routes = [...routes, ...devRoutes];
// }





export {
  routes,
  sideMenu
}