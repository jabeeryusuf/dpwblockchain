// Copyright (c) 2019 Microwarehouse
import React from 'react';
import { connect } from 'react-redux';
import { withLoader } from '../../components';
import { Table } from 'react-bootstrap';
import "react-table/react-table.css";
import { Link } from 'react-router-dom';

class GroupInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: [],
      data: []
    }
    this._isMounted = true;
    this.sortData = this.sortData.bind(this);
  }

  componentDidMount() {
    if (!this.props.hide) {
      this.sortData();
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.hide !== this.props.hide && this.state.data.length < 1) {
      this.sortData();
    }
  }

  async componentWillMount() {
    this.sortData();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  async sortData() {
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');
    let case1 = [], case2 = [], case3 = [], case4 = [], case5 = [], case6 = [];
    let finalArrayOfCase = []

    let org_id = this.props.org.org_id;
    let data ="";
    // let data = await this.props.client.getStuff(`select p.*, g.* from (select * from providers where org_id = '${org_id}' and 
    //                                                   ref IN ('global_retailer', '3PL', 'global_retailer2', 'global_retailer3', 'global_management','3PL2', 'global_retailer4','local_retailer', '3PL3','global_retailer5')) p
    //                                               Left Join groups g on g.group_id = p.default_contact_group`);
    let responseObject = data;
    if (responseObject.length !== 0) {
      for (let row of responseObject) {
        if (row.ref === 'global_retailer') {
          row.caseName = 'No booking from factory to 3PL';
          row.priority = 'Medium/High';
          case1.push(row);
        }
        if (row.ref === '3PL') {
          row.caseName = 'Booking not approved by 3PL';
          row.priority = 'Low'
          case2.push(row);
        }
        if (row.ref === 'global_retailer2') {
          row.caseName = 'Booking not approved by 3PL';
          row.priority = 'Medium/High'
          case2.push(row);
        }
        if (row.ref === 'global_retailer3') {
          row.caseName = 'Retailer not approved product delivery to 3PL';
          row.priority = 'Low';
          case3.push(row);
        }
        if (row.ref === 'global_management') {
          row.caseName = 'Retailer not approved product delivery to 3PL';
          row.priority = 'Medium/High'
          case3.push(row);
        }
        if (row.ref === '3PL2') {
          row.caseName = 'Delayed in transit between factory to 3PL';
          row.priority = 'Low'
          case4.push(row);
        }
        if (row.ref === 'global_retailer4') {
          row.caseName = 'Delayed in transit between factory to 3PL';
          row.priority = 'Medium/High'
          case4.push(row);
        }
        if (row.ref === 'local_retailer') {
          row.caseName = 'Stock available in 3PL MCC with no shipment allocation';
          row.priority = 'Low'
          case5.push(row);
        }
        if (row.ref === '3PL3') {
          row.caseName = 'Customer allocation order placed but not shipped';
          row.priority = 'Low'
          case6.push(row);
        }
        if (row.ref === 'global_retailer5') {
          row.caseName = 'Customer allocation order placed but not shipped';
          row.priority = 'Medium/High'
          case6.push(row);
        }
        let response = await this.props.client.getUsersInGroups('org', 'anko', row.default_contact_group);
        row.seeDetails = <Link to={{ pathname: `${this.props.basePath}/groups/detail`, state: { response } }}  > See details </Link>
        row.default_contact_group = row.default_contact_group.slice(row.default_contact_group.lastIndexOf('&') + 1)
      }
    }
    finalArrayOfCase.push(case1)
    finalArrayOfCase.push(case2)
    finalArrayOfCase.push(case3)
    finalArrayOfCase.push(case4)
    finalArrayOfCase.push(case5)
    finalArrayOfCase.push(case6)
    if (this._isMounted) {
      this.setState({
        tableData: finalArrayOfCase,
        data
      })
    }
    stopLoading();
  }
  render() {
    let singleTable = [], finalContainerOfTable = [];
    this.state.tableData.forEach((element, index) => {
      let caseName;
      let rowset = [];
      for (let e of element) {
        caseName = e.caseName
        rowset.push(
          <tr key={e.group_name}>
            <td> {e.priority} </td>
            <td> {e.group_name} </td>
            <td> {e.seeDetails} </td>
          </tr>
        );
      }
      singleTable.push(
        <span key={index} >
          <h6> Case Subject : {caseName} </h6>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th style={{ "width": 300 }}> Case Priority</th>
                <th> Group Name </th>
                <th style={{ "width": 200 }}> More Details </th>
              </tr>
            </thead>
            <tbody>
              {rowset}
            </tbody>
          </Table>
        </span>
      )
    })
    finalContainerOfTable.push(singleTable)

    return (
      <>
        {finalContainerOfTable}
      </>
    )
  }
}



function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org
  }
}
export default withLoader(connect(mapStateToProps)(GroupInfo));




