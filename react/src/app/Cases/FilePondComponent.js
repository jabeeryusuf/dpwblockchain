// Copyright (c) 2019 Microwarehouse
import {FilePond, File, registerPlugin} from "react-filepond";

import qs from 'qs';
// Import FilePond styles
import 'filepond/dist/filepond.min.css';

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import React from 'react';
// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview, FilePondPluginFileValidateType);
// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately

// Register the plugins
// Our app
class FilePondComponent extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      // Set initial files
      files: [],
      fileIDs: []
    };
  }
  clearFiles(){
    this.setState({
      files:[],
      fileIDs:[]
    })
  }
  handleInit() {
    console.log('FilePond instance has initialised', this.pond);
  }
  setServerCode(){
    let stringifiedParams = qs.stringify({user_id:this.props.user,permission:this.props.permission?this.props.permission:'private'});
    console.log(this.props.apiUrl+ '/v0.1/file/uploadFile')
    return (this.props.apiUrl+ '/v0.1/file/uploadFile?' + stringifiedParams)

  }
  handleServerReturn(files){
    let newFileIDs = this.state.fileIDs;
    console.log(files)
    files.forEach((val)=>{
      if(!newFileIDs.includes(val)){
        newFileIDs.push(val);
      }
    });
    this.setState({
      fileIDs:newFileIDs
    },()=>{
      this.props.handleFilePondUpdate(this.state.fileIDs);
    });
  }
  removeFileFromList(fileID){
    let removalIndex = this.state.fileIDs.indexOf(fileID);
    let newFileList = this.state.fileIDs.splice(removalIndex,1)
    this.setState({
      fileIDs:newFileList
    }) 
  }
  render() {
    return (
      <div className="App" >

        {/* Pass FilePond properties as attributes */}
        <FilePond ref={ref => this.pond = ref}
          allowMultiple={true}
          acceptedFileTypes={this.props.fileTypes ? this.props.fileTypes : null}
          maxFiles={this.props.maxUploads?this.props.maxUploads:3}
          server = {this.setServerCode()}
          oninit={() => this.handleInit()}
          onprocessfile = {async(err,data)=>{this.handleServerReturn(await JSON.parse(data.serverId).fileIDs)}}
          onpreparefile = {(file,output)=>{console.log('prepare',file,output)}}
          onremovefile = {(file)=>{return(file.serverId?this.removeFileFromList(JSON.parse(file.serverId).fileIDs[0]):null)}}
          onupdatefiles={(fileItems) => {
            console.log(fileItems)
            // Set current file objects to this.state
            // let formData = new FormData();
            // formData.append('file', fileItems);
            // let file = await this.props.client.uploadFile(formData, this.props.user.user_id, 'private');
            // await this.props.client.addFileToCase(this.props.location.state.case_id, file.fileID);
            // let caseFileNames = await this.props.client.caseFiles(this.props.location.state.case_id);
            this.setState({
              files: fileItems.map(fileItem => fileItem.file)
            });
          }}>

          {/* Update current files  */}
          {this.state.files.map(file => (
            <File key={file} src={file} origin="local" />
          ))}

        </FilePond>

      </div>
    );
  }
}
export default FilePondComponent;