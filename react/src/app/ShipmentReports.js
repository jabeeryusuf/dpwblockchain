import React from 'react';
import { connect } from 'react-redux';
import { withLoader } from '../components';
import Table from '../components/Table'
import {lateShipColumns} from './columns/reportColumns.js';
import {Card} from 'react-bootstrap'

class ShipmentReports extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  async componentDidMount(){
    let lateShipments = await this.props.client.getStuff("SELECT * FROM shipments where ((date_part('day',age(eta, original_eta)) > 1 and ata is null)) and org_id ='"+ this.props.org.org_id +"'");
    lateShipments.map((val)=>{
      val.basePath = this.props.basePath
    })
    this.setState({
      data: lateShipments
    })
  }
  render() {
    return (
      <div>
        <h1>Late Shipments</h1>
        <Card>
					<Table style={{ cursor: 'pointer' }} className="-highlight"
						data={this.state.data}
						columns={lateShipColumns}
            filterable
             />
				</Card>
      </div>
    )
  }
}
function mapStateToProps(state) {
  return { client: state.auth.client, user: state.auth.user, org: state.auth.org }
}
export default withLoader(connect(mapStateToProps)(ShipmentReports));