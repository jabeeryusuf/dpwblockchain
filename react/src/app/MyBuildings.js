import React from 'react';
import { GMap } from '../components/GMap';
import { Card, Row, Col, Form, Button,InputGroup,Nav,ButtonGroup} from 'react-bootstrap';
import findCenter from '../utils/findCenter';
import PopUpWindow from '../components/PopUpWindow.js';
import {EditBuilding, DeleteBuilding} from '../components/forms';
import { buildingColumns } from './columns/columns'; 
import { withLoader } from '../components';
import { connect } from 'react-redux';
import { Subheader } from '../layout';
import { Link } from 'react-router-dom';
import ReactTable from "react-table";

const Portpin = require("../images/mapicons/portpin.png");
const Factorypin = require("../images/mapicons/factorypin.png");
const Warehousepin = require("../images/mapicons/warehousepin.png");
const Shippin = require("../images/mapicons/shippin.png");

const Porticon = require("../images/mapicons/porticon.png");
const Factoryicon = require("../images/mapicons/factoryicon.png");
const Warehouseicon = require("../images/mapicons/warehouseicon.png");
const Shipicon = require("../images/mapicons/shipicon.png");

const colorMap = {
  'Port': Portpin,
  'Factory': Factorypin,
'Warehouse': Warehousepin,
'Vessel': Shippin,
}

const createItem ={
  id: '',
  name: '',
  address: '',
  code: '',
  lat: 0,
  lng: 0,
  type: 'Factory',
};

class MyBuildings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      tableData: [],
      markers: [],
      paths: [],
      tab: "all",
      openWindow: null,
      inputValue: '',
      zoom: 3,
      showCreate: false,
      showDelete: false,
      active: 'all',
      noResults: false
    }
    this.closeWindow = this.closeWindow.bind(this);
    this.handleCreateSubmit = this.handleCreateSubmit.bind(this);
    this.handleDeleteSubmit = this.handleDeleteSubmit.bind(this);
    this.handleMarkerClick = this.handleMarkerClick.bind(this);

  }

  /**
   * When page loads, queries DB for all buildings, assigns behavior and look of the markers to
   * be displayed on map based on row information
   */
  async componentDidMount() {
    const { startLoading, stopLoading,client,basePath} = this.props;
		startLoading('Loading your data...');
    //let list = await client.queryBuildings("container_id LIKE 'factory&%' OR container_id LIKE 'warehouse&%' OR container_id IN ('port&SEA','port&JKT', 'port&BKK', 'port&SZX', 'port&SHA') order by container_id");
    /* let list = await client.getStuff(`SELECT buildings.* from 
      (SELECT origin_container, dst_container, port_of_loading, mcc_warehouse 
      FROM purchase_order_items WHERE purchase_order_items.org_id ='` +this.props.user.org_id+`') po
      join buildings on po.origin_container = buildings.container_id or
      po.dst_container = buildings.container_id or 
      po.port_of_loading = buildings.container_id or
      concat('warehouse&',po.mcc_warehouse) = buildings.container_id
      WHERE container_id LIKE 'factory&%' OR 
      container_id LIKE 'warehouse&%' OR
      container_id 
      IN ('port&SEA','port&JKT', 'port&BKK', 'port&SZX', 'port&SHA', 'port&JEA','port&PUS') 
      group by buildings.container_id
      order by container_id`) */
    // let list = await client.getStuff(`
    // SELECT buildings.* from 
    //     (SELECT origin_container, dst_container, port_of_loading, mcc_warehouse 
    //     FROM purchase_order_items WHERE purchase_order_items.org_id ='` +this.props.user.org_id+`') po

    //     join buildings on po.origin_container = buildings.container_id or
    //                       po.dst_container = buildings.container_id or 
    //                       po.port_of_loading = buildings.container_id or
    //                       concat('warehouse&',po.mcc_warehouse) = buildings.container_id
    //     WHERE ( container_id LIKE 'factory&%' OR 
    //           container_id LIKE 'warehouse&%' OR
    //           container_id LIKE 'port&%' ) AND 
    //           (org_id = '` +this.props.org.org_id+ `' OR org_id IS NULL)
    // UNION 
    // SELECT buildings.* from 
    //     (SELECT source_container_id, destination_container_id 
    //     FROM shipments WHERE shipments.org_id ='` +this.props.user.org_id+`') sh

    //     join buildings on sh.source_container_id = buildings.container_id or
    //                       sh.destination_container_id = buildings.container_id  
    //     WHERE container_id LIKE 'port&%' AND  
    //           (org_id = '` +this.props.org.org_id+ `' OR org_id IS NULL)
    //     group by buildings.container_id
    // UNION
    // SELECT buildings.* from 
    //     buildings WHERE (container_id LIKE 'factory&%' OR 
    //           container_id LIKE 'warehouse&%') AND  org_id = '` +this.props.org.org_id+ `'
    //     group by buildings.container_id`
    // )

    let list = await client.getStuff(`

    select * from (

    SELECT buildings.* from 
        (SELECT origin_container, dst_container, port_of_loading, mcc_warehouse 
        FROM purchase_order_items WHERE purchase_order_items.org_id ='` +this.props.user.org_id+`') po

        join buildings on po.origin_container = buildings.container_id or
                          po.dst_container = buildings.container_id or 
                          po.port_of_loading = buildings.container_id or
                          concat('warehouse&',po.mcc_warehouse) = buildings.container_id
        WHERE ( container_id LIKE 'factory&%' OR 
              container_id LIKE 'warehouse&%' OR
              container_id LIKE 'port&%' ) AND 
              (org_id = '` +this.props.org.org_id+ `' OR org_id IS NULL)
    UNION 
    SELECT buildings.* from 
        (SELECT source_container_id, destination_container_id 
        FROM shipments WHERE shipments.org_id ='` +this.props.user.org_id+`') sh

        join buildings on sh.source_container_id = buildings.container_id or
                          sh.destination_container_id = buildings.container_id  
        WHERE container_id LIKE 'port&%' AND  
              (org_id = '` +this.props.org.org_id+ `' OR org_id IS NULL)
        group by buildings.container_id
    UNION
    SELECT buildings.* from 
        buildings WHERE (container_id LIKE 'factory&%' OR 
              container_id LIKE 'warehouse&%') AND  org_id = '` +this.props.org.org_id+ `'
        group by buildings.container_id ) innerq

        where
        EXISTS
        (
            select c.parent_container_id
            from shipment_items si
                    left join shipments s on s.shipment_id = si.shipment_id
                    LEFT JOIN products p on p.sku = si.sku
                    LEFT JOIN containers c on c.ref = s.shipment_ref
            where s.org_id = 'hondauk'
              and parent_container_id notnull
              and parent_container_id <> ''
            and (innerq.container_id = c.parent_container_id or innerq.name = c.parent_container_id)

            group by parent_container_id
            having count(si.purchase_order_id) > 1
        )
    `)

    //list = await client.getStuff(``);
    //  select * from vehicle_locations where vessel_container_id in (select vessel_container_id from shipments where org_id='` +this.props.org.org_id+ `')`)

    // let vessel_list = await client.getStuff(
    // ` select b.* from (
    //   select vessel_name,max(timestamp) as latest
    //   from vehicle_locations
    //   group by vessel_name
    //  ) a inner join vehicle_locations b
    //  on a.vessel_name = b.vessel_name and a.latest = b.timestamp
    //  where b.ata isnull  and b.vessel_name in (select distinct vessel_name from shipments where org_id = '` +this.props.org.org_id+ `' ) order by b.vessel_name
    // `);
    
    let vessel_list = await client.getStuff(
    `select vl.vessel_name as vessel_name, vl.vessel_container_id , (select lat from vehicle_locations vllat where vllat.vessel_name = vl.vessel_name order by vllat.update_time desc limit 1) as lat, (select lng from vehicle_locations vllng where vllng.vessel_name = vl.vessel_name order by vllng.update_time desc limit 1) as lng from shipment_items si
    left join shipments s on s.shipment_id = si.shipment_id
    LEFT JOIN  containers c on c.ref = s.shipment_ref
    LEFT JOIN  vehicle_locations vl on(c.parent_container_id = vl.vessel_container_id or c.parent_container_id = vl.vessel_name)
    where  s.org_id = '` +this.props.org.org_id+ `'
    group by  vl.vessel_name,vl.vessel_container_id
    having count(s.shipment_id)>1 and vl.vessel_name  notnull;`);


    console.log("Vessel List is1---------------------");
    console.log(vessel_list);

    let markerList = [];
    for (let i = 0; i < list.length; i++) {
      let container = list[i].container_id;
      if (container.includes("factory")) {
        list[i].color = colorMap['Factory'];
      } else if (container.includes("warehouse")) {
        list[i].color = colorMap['Warehouse']
      } else {
        list[i].color = colorMap['Port'];
      }
      list[i].id = i;
      list[i].code = list[i].container_id.substring(list[i].container_id.indexOf('&') + 1, list[i].container_id.length);
      list[i].onMouseOver = () => this.handleMarkerHover(i);
      list[i].onClick = () => this.handleMarkerClick(list[i]);
      list[i].onClose = () => this.handleMarkerClose();
      list[i].moreInfo = <Link to={basePath + '/detail-location/' + container }> More Info </Link>
      markerList.push(list[i]);
    }


    for (let i = 0; i < vessel_list.length; i++) {

      let container = vessel_list[i].vessel_container_id;
      
      vessel_list[i].color = colorMap['Vessel'];
      
      vessel_list[i].id = i;
      vessel_list[i].code = vessel_list[i].vessel_name;
      vessel_list[i].name = vessel_list[i].vessel_name;
      vessel_list[i].onMouseOver = () => this.handleMarkerHover(i);
      vessel_list[i].onClick = () => this.handleMarkerClick(vessel_list[i]);
      vessel_list[i].onClose = () => this.handleMarkerClose();
      vessel_list[i].moreInfo = <Link to={basePath + '/detail-location/' + container +"?vname =testname"}> More Info </Link>
      markerList.push(vessel_list[i]);
    }

    console.log("Vessel List is1---------------------");
    console.log(vessel_list);

    this.setState({
      data: markerList,
      tableData: markerList,
      markers: markerList,
      orgId: this.props.org.org_id
    });
    
    stopLoading();

  }

  /**
   * Handles the click event of buttons on top right of page to filter types of 
   * buildings to be shown on the map
   * @param {*} buttonText 
   */
  onClicky(buttonText) {
    let list = this.state.data;
    let newMarkers = [];
    
  
    console.log("Button text is");
    console.log(buttonText);
    if (buttonText === "all") {
      this.setState({
        markers: this.state.data,
        tableData: this.state.data,
        center: findCenter(this.state.data),
        openWindow: null,
        active: "all",
        zoom: 3
      })
    }    
    
    else
    {
      if (buttonText === "Vessel")
      {
        for (let obj of list) {   
          if (obj['vessel_container_id']!=null && obj['vessel_container_id'].includes('ship')) {
            newMarkers.push(obj);
          }    
            
          
        }
      }


      else {
        for (let obj of list) {
          if (obj['container_id']!=null && obj['container_id'].includes(buttonText)) {
            newMarkers.push(obj);
          }
        }
  
      }
      
        this.setState({
          markers: newMarkers,
          tableData: newMarkers,
          center: findCenter(newMarkers),
          openWindow: null,
          active: buttonText,
          zoom: 3
        });
      


    }
  
    

    
  }

  /**
   * Updates this.state.inputValue every time the input of the search bar changes
   * @param {*} evt 
   */
  handleInputChange(evt) {
    this.setState({
      inputValue: evt.target.value
    })
  }

  /**
   * Handles the click of the "Search" button.
   * Looks through the list of rows to find a name that includes to search string.
   * Will only display matching markers on map when found. Otherwise displays error message
   * @param {*} building 
   */
  searchSubmitted(building) {
    if (building.length === 0) {
      return;
    }

    let list = this.state.data;

    let newMarkers = [];
    for(let obj of list) {
      let name = obj.name.toLowerCase();
      if (name.includes(building.toLowerCase())) {
        newMarkers.push(obj);
      }
    }

    if (newMarkers.length === 0) {
      this.setState({
        noResults: true
      });
    } else {
      this.setState({
        markers: newMarkers,
        tableData: newMarkers,
        center: findCenter(newMarkers),
        zoom: 5,
        inputValue: '',
        noResults: false
      });
    }
  }

  /**
   * Handles the clicks of the pill buttons right above the table.
   * Will display designated pop up window based on button pressed.
   * @param {*} buttonText 
   */
  handleEditClicks(buttonText) {
    switch (buttonText) {
      case "create":
        this.setState({
          showCreate: true
        });
        break;
      case "delete":
        this.setState({
          showDelete: true
        });
        break;
      default:
        throw new Error('What button was that?');
    }
    
  }

  /**
   * When the cancel button is closed on an popup window, this function
   * will set all show popup booleans to false.
   * Didn't want to try to decide which one was pressed so I just close them all since
   * only one is open at a time anyways
   */
  closeWindow() {
    this.setState({
      showCreate: false,
      showDelete: false,
    });
  }

  /**
   * When the "Submit" button is pressed in the Create Building pop up window,
   * this function is called to create a new row in the data and display the building that
   * was just created.
   * @param {} name 
   * @param {*} address 
   * @param {*} lat 
   * @param {*} lng 
   * @param {*} type 
   */
  async handleCreateSubmit(id, name, code, address, lat, lng, type) {
    const { basePath } = this.props; 
    const { orgId } = this.state;

    let list = this.state.data;
    let newMarker = {
      id: list.length + id,
      name: name,
      address: address,
      lat: lat,
      lng: lng,
      container_id: type.toLowerCase() + "&" + code,
      type: type,
      color: colorMap[type]
    }
    newMarker.onClick = () => {this.handleMarkerClick(newMarker)};
    newMarker.onMouseOver = () => {this.handleMarkerHover(newMarker.id)}
    newMarker.onClose = () => this.handleMarkerClose();
    newMarker.code = code;
    newMarker.button = <Button value="Edit" onClick={() => this.handleInfoWindowClick(newMarker.id)} />
    if (type !== "port") {
      newMarker.button = <Button onClick={() => this.handleInfoWindowClick(newMarker.id)} >Edit</Button>;
    }
    newMarker.moreInfo = <Link to={`${basePath}/detail-location/${newMarker.container_id} `}> More Info </Link>

    list.push(newMarker);

    await this.props.client.containersClient.addBuilding(newMarker, orgId);

    this.onClicky(type.toLowerCase());

    this.setState({
      data: list,
      
      markerList: list,
      tableData: list,
      center: {lat: newMarker.lat, lng: newMarker.lng},
      zoom: 5
    });

    this.handleMarkerClick(newMarker);
  }

  /**
   * When the "Submit" button is pressed in the delete popup window, this function
   * is called to search through the list and and remove the matching building
   * @param {*} name 
   * @param {*} address 
   */
  async handleDeleteSubmit(name, address, type) {
    const { orgId } = this.state;

    let list = this.state.data;
    let index = -1;
    let codeForContainerId;
    for (let i = 0; i < list.length; i++) {
      if (list[i].name === name && list[i].address === address) {
        index = i;
        codeForContainerId = list[i].code;
        break;
      }
    }

    if (index >= 0) {
      list.splice(index, 1);
    }
    const container_id = type + "&" + codeForContainerId;
    await this.props.client.containersClient.deleteBuilding(container_id, name, orgId);

    this.onClicky("all");

    this.setState({
      data: list,
      markers: list,
      tableData: list
    });

  }


  /**
   * Not sure what the mouse over function should do yet if anything at all
   * @param {*} i 
   */
  handleMarkerHover(i) {
    return i;
  }

  /**
   * Handles when user clicks the marker on the map.
   * @param {*} item 
   */
  handleMarkerClick(item) {
    this.setState({
      openWindow: item.id,
      tableData: [item],
      center: {lat: item.lat, lng: item.lng},
      zoom: 5
    });
  }

  /**
   * Handles the close button on the infowindow to reset the table data
   */
  handleMarkerClose() {
    this.setState({
      tableData: this.state.markers,
      center: findCenter(this.state.markers),
      zoom: 3
    });
  }

  renderMap() {
    let markers = this.state.markers;
    if (this.state.data.length) {
      return (
        <GMap isMarkerShown
          markers={markers}
          paths={this.state.paths}
          openWindow={this.state.openWindow}
          zoom={this.state.zoom}
          center={this.state.center} />
      )
    }
  }


  render() {
    const { basePath } = this.props;
    return (
      <>

        <Subheader title="Locations">
        <PopUpWindow shown={this.state.showCreate} page={<EditBuilding onCancel={this.closeWindow} 
                                                                       onSubmit={this.handleCreateSubmit} 
                                                                       data={this.state.data} 
                                                                       item={createItem}  
                                                                       header={"Create Location"} />} />
        <PopUpWindow shown={this.state.showDelete} page={<DeleteBuilding onCancel={this.closeWindow} onSubmit={this.handleDeleteSubmit} data={this.state.data} />} />
          <Col>
            <InputGroup>
              <Form.Control
                type="text"
                placeholder={"Search location name"}
                value={this.state.inputValue}
                onChange={evt => this.handleInputChange(evt)} />
              <InputGroup.Append>
                <Button
                  onClick={() => this.searchSubmitted(this.state.inputValue)} >
                  Search
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Col>  
        </Subheader>


        <Row  className="row-bottom-border pb-1 mb-2">
          <Col sm="6">
            <Nav variant="tabs" defaultActiveKey="all">
              <Nav.Item  >
                <Nav.Link eventKey="all" onSelect={() => this.onClicky("all")} >All</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="factory" onSelect={() => this.onClicky("factory")}> Factory </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="warehouse" onSelect={() => this.onClicky("warehouse")} > Warehouse </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="port" onSelect={() => this.onClicky("port")}> Port </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="Vessel" onSelect={() => this.onClicky("Vessel")}> Vessel </Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>

          <Col sm="6">
           <ul className="linkColour">
           <li><img src={Factoryicon}/></li>
           <li><img src={Warehouseicon}/></li>
           <li><img src={Porticon}/></li>
           <li><img src={Shipicon}/></li>
           </ul>
         </Col>

          {this.state.noResults && <font className="text-danger">No such location exists</font>}
        </Row>


        <Card>
          {this.renderMap()}
        </Card>
        <Card className="mt-3">
          <ButtonGroup >
          <Button variant="outline-secondary" onClick={() => this.handleEditClicks("create")}> Add a new location{'\t'}</Button>
          <Button variant="outline-secondary" onClick={() => this.handleEditClicks("delete")}> Delete an existing location</Button>
          </ButtonGroup>
          <ReactTable
            data={this.state.tableData}
            columns={buildingColumns(this.props.basePath)}
            defaultPageSize={20}
            className="-striped -highlight"
          />
        </Card>
      </>
    )
  }

}

function mapStateToProps(state) {
  return  { 
            client: state.auth.client, 
            user:state.auth.user,
            org: state.auth.org,
          }
}
export default withLoader(connect(mapStateToProps)(MyBuildings));
