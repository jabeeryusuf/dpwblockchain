import moment from 'moment';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import React from 'react'
const allSKUColumns = (basePath) => ([
    {
        Header: 'SKU',
        accessor: 'productId',
        Cell: (val) => {
            return <Link to={basePath + "/items/detail/" + val.original.productId}>{val.original.productId} </Link>
        }

    },
    {
        Header: "Product Name",
        accessor: 'title'
    },
    {
        Header: 'Department',
        accessor: 'type'
    },
    // {
    //   Header: 'Vendor',
    //   accessor: 'fact_name'
    // },
    // {
    //   Header: 'Vendor Code',
    //   accessor: 'vendorCode'
    // },
    {
        Header : 'Product Image ',
        accessor: 'image',
        Cell: (val) => {

            if (val.original.imageLink !== null)
                return <p style={{textAlign:"center"}}><img style={{ width: 85, height: 60, border: 8,}} src={val.original.imageLink} alt='Temporarily Unavailable' /></p>;
            else return "";
        }
    }
])

const vendorColumns = [
    {
        Header: "Vendor Name",
        accessor: 'origin_container',
        Cell: (val) => {
            if (val.original.origin_container) {
                return val.original.origin_container.slice(val.original.origin_container.indexOf('&') + 1)
            }
            else {
                return null
            }
        }
    }
    // {
    //     Header: 'Create Case',
    //     accessor: 'issueLink',
    // }
]
const productColumns = [

    {
        Header: 'SKU',
        accessor: 'sku'
    },
    {
        Header: "Product Name",
        accessor: 'name'
    },
    {
        Header: "APN",
        accessor: 'apn'
    },
    {
        Header: 'Department',
        accessor: 'department'
    },
    {
        Header: 'Carton CBM',
        accessor: 'carton_cbm'
    },
    {
        Header: 'Pack Size',
        accessor: 'pack_size'
    },
    {
        Header: 'Total Ordered',
        accessor: 'order_qty'
    }
]
const poColumns = [{
    Header: 'Purchase Order ID',
    accessor: 'purchase_order_id',
    Cell: (val) => {
        return <Button size="sm" variant="link" > {val.original.purchase_order_id} </Button>
    }
},
{
    Header: 'Quantity',
    accessor: 'quantity'
},
{
    Header: 'PO Create Date',
    accessor: 'create_date',
    Cell: (val) => {
        if (val) {
            const event = new Date(val.original.create_date);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        }
        return null
    }
},
{
    Header: 'Destination',
    accessor: 'dst_container',
    Cell: (val) => {
        if (val.original.dst_container) {
            return <p>{val.original.dst_container.slice(val.original.dst_container.indexOf('&') + 1)}</p>
        }
        return null
    }
}
]
const shipColumns = [
    {
        Header: 'Shipment ID',
        accessor: 'shipment_id',
        Cell: (val) => {
            return <Button size="sm" variant="link" > {val.original.shipment_ref} </Button>
        }
    },
    {
        Header: 'Shipped Quantity',
        accessor: 'delivery_quantity'
    },
    {
        Header: 'Port of Loading ',
        accessor: 'source_container_id',
        Cell: (val) => {
            return <p> {val.original.source_container_id.slice(val.original.source_container_id.indexOf('&') + 1)}</p>
        },

    },
    {
        Header: 'Destination ',
        accessor: 'destination_container_id',
        Cell: (val) => {
            return <p> {val.original.destination_container_id.slice(val.original.destination_container_id.indexOf('&') + 1)}</p>
        }
    },
    {
        Header: 'Vessel Name',
        accessor: 'vessel_name'
    },
    {
        Header: 'ETD',
        accessor: 'etd',
        Cell: (val) => {
            if (val) {
                const event = new Date(val.original.etd);
                return <p> {event.toLocaleDateString(navigator.language)}</p>
            }
            return null;
        }
    },
    {
        Header: 'ETA',
        accessor: 'eta',
        Cell: (val) => {
            if (val) {
                const event = new Date(val.original.eta);
                return <p> {event.toLocaleDateString(navigator.language)}</p>
            }
            return null;
        }
    },
    {
        Header: 'ATD',
        accessor: 'atd',
        Cell: (val) => {
            if (val) {
                const event = new Date(val.original.atd);
                return <p> {event.toLocaleDateString(navigator.language)}</p>
            }
            return null;
        }
    },
    {
        Header: 'ATA',
        accessor: 'ata',
        Cell: (val) => {
            if (val.original.ata) {
                const event = new Date(val.original.ata)
                return <p> {event.toLocaleDateString(navigator.language)}</p>
            }
            return null;
        }
    }
]
const locationColumns = [{
    Header: 'Location',
    accessor: 'name',
    Cell: (val) => {
        return <Link to={val.original.link}>{val.original.name} </Link>
    }
},
{
    Header: 'Quantity',
    accessor: 'quantity'
}
]

const detailCaseColumns = [
    {
        Header: "Subject",
        accessor: "subject"
    },
    {
        Header: "Case ID",
        accessor: "showCaseId"
    }
]

const updateCaseColumns = [
    {
        Header: "Case Status",
        accessor: "status"
    },
    {
        Header: "Case Priority",
        accessor: "colorCasePriority"
    }
]
export { vendorColumns, locationColumns, shipColumns, poColumns, productColumns, allSKUColumns, detailCaseColumns, updateCaseColumns }