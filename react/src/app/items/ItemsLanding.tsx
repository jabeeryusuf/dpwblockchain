import React from "react";

import { ILandingTab, Landing } from "../../layout/Landing"
import ItemsExplorer from "./ItemsExplorer";

const tabs: ILandingTab[] = [
  { label: "Product Explorer",  path: "/explorer", component: ItemsExplorer },
];

export const ItemsLanding = (props: any) => <Landing {...props} tabs={tabs} />
