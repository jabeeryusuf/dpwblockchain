import React from "react";
import { Button, ButtonGroup, ButtonToolbar, Dropdown, DropdownButton, Row } from "react-bootstrap";
import { CSVLink } from "react-csv";
import { connect } from "react-redux";
import ReactTable from "react-table";
import { withLoader } from "../../components";
import { FormSearch } from "../../components/forms";
import { allSKUColumns } from "./ItemDetailColumns";
import { Product } from "tw-api-common";

// const selectFilter = [
//   'No Filter',
//   'Item has unallocated Inventory',
//   "Item has top 100 most orders",
//   "Item has top 100 ordered quantity",
//   "Item has been ordered"
// ]

const typeOptions = [
  { label: "All", value: "all" },
  { label: "baby", value: "open" },
]

const createDdi = (opts: any) =>
  opts.map((o: any, i: any) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

const getLabel = (opts: any, value: any) => {
  const l = opts.find((o: any) => (o.value === value));
  return l ? l.label : "";
};

interface IState {
  csvLink: any;
  data: Product[];

  searchValue: string;
  typeValue: string;

  loading: boolean;
  cursorList: string[];
  page: number;
  pageSize: number;
  typeOptions: any[];
}

const newPagination = () => ({ page: 0, cursorList: [""] });


class ItemExplorer extends React.Component<any, IState> {

  private searchTimeout: any;

  constructor(props: any) {
    super(props);
    this.state = {
      csvLink: "",
      data: [],

      searchValue: "",

      pageSize: 20,
      typeValue: "all",

      typeOptions: [{ label: "All", value: "all" }],

      loading: false,

      ...newPagination(),
    };

    this.searchTimeout = null;
  }

  public setExportData() {
    const columns = allSKUColumns(this.props.basePath);
    const csvHeaders = [];

    for (const col of columns) {
      let obj;
      if (col.Header !== "Product Image") {
        if (col.Header === "SKU") {
          obj = {
            label: col.Header,
            key: "sku",
          };
        } else {
          obj = {
            label: col.Header,
            key: col.accessor,
          };
        }
        csvHeaders.push(obj);
      }
    }

    this.setState({
      csvLink: <CSVLink data={this.state.data} headers={csvHeaders}
        filename={"item_list.csv"}><Button variant="outline-primary">Download Table</Button></CSVLink>,
    });

  }

  public componentDidMount = async () => {

    const { client: { productsClient } } = this.props;

    this.setState({ loading: true })
    const types = await productsClient.listTypes();

    const typeOptions = [
      { label: "All", value: "all" },
      ...types.map((t: string) => ({ label: t, value: t }))
    ];

    console.log("CDM ItemXplorer")
    console.log(typeOptions);

    this.setState({ typeOptions })

    await this.updateDataNow();
  }

  public componentDidUpdate = async (prevProps: any, prevState: any) => {
    console.log("CDU ItemXplorer")
    console.log(this.state)
    console.log(prevState)
    if (this.state.pageSize !== prevState.pageSize ||
      this.state.page !== prevState.page ||
      this.state.typeValue !== prevState.typeValue) {
      await this.updateDataNow();
    }
    else if (this.state.searchValue !== prevState.searchValue) {
      await this.updateDataTimed();
    }

  }

  public render() {

    const { typeOptions, typeValue } = this.state;

    console.log([this.state.page, this.state.cursorList.length]);

    return (

      <>
        <Row>
          <ButtonToolbar className="toolbar">

            <ButtonGroup className="filter" >

              <DropdownButton id="type" title={getLabel(typeOptions, typeValue)} onSelect={this.handleTypeChange}>
                {createDdi(typeOptions)}
              </DropdownButton>

              <FormSearch placeholder="Search... "
                value={this.state.searchValue}
                onChange={this.handleSearchChange}
                onClick={this.clearSearchValue} />

            </ButtonGroup>

            <ButtonGroup className="action">
              {this.state.data &&
                <CSVLink data={this.state.data} filename={"products.csv"}>
                  <Button variant="outline-primary">Download Table</Button>
                </CSVLink>
              }
            </ButtonGroup>

          </ButtonToolbar>
        </Row>

        <ReactTable 
          style={{ cursor: "pointer" }} className="-highlight -striped itemstable"
          columns={allSKUColumns(this.props.basePath)}
          data={this.state.data}
          page={this.state.page}
          pages={this.state.cursorList.length}
          loading={this.state.loading}

          manual // informs React Table that you'll be handling sorting and pagination server-side

          onPageChange={this.handlePageChange}
          onPageSizeChange={this.handlePageSizeChange}

          showPageJump={false}
        />
      </>
    );
  }


  private handlePageChange = async (page: number) => {
    this.setState({ page });
  }

  private handlePageSizeChange = async (pageSize: number, page: number) => {
    this.setState({ pageSize, ...newPagination() });
  }

  private handleTypeChange = async (value: string) => {
    this.setState({ typeValue: value, ...newPagination() });
  }

  private handleSearchChange = async (e: any) => {
    this.setState({ searchValue: e.target.value, ...newPagination() });
  }

  private clearSearchValue = async (e: any) => {
    this.setState({ searchValue: "", ...newPagination() });
  }

  // Update data with the components current page and page size
  private updateDataTimed = () => {
    console.log("TIMED");
    if (this.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }

    this.searchTimeout = setTimeout(
      async () => { await this.updateDataNow() }, 300);
  }

  private updateDataNow = async () => {
    console.log("NOW");

    const { client: { productsClient } } = this.props;
    const { searchValue, cursorList, page, pageSize, typeValue } = this.state;

    this.setState({ loading: true });

    // Get the cursor
    console.log("getting cursor for " + page);
    const cursor = page < cursorList.length ? cursorList[page] : cursorList[0];

    // pull results
    const results = await productsClient.search(
      searchValue, typeValue === 'all' ? undefined : typeValue, cursor, pageSize);

    // update the next cursor
    if (page === cursorList.length - 1 && results.metadata.nextCursor) {
      cursorList.push(results.metadata.nextCursor);
    }

    console.log(results);
    this.setState({ loading: false, data: results.products }, () => {
      console.log("wassup", this.state.data.length);
    });
  }

}

function mapStateToProps(state: any) {
  return { client: state.auth.client, user: state.auth.user };
}

export default withLoader(connect(mapStateToProps)(ItemExplorer));
