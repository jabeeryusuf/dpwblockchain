import { ContainersClient, FcFetchClient, PurchaseOrdersClient } from "fc-rest-client";
import React from "react";
import { Card, Container } from "react-bootstrap";
import { connect } from "react-redux";
import { Link, withRouter, RouteComponentProps } from "react-router-dom";
import { AssetType } from "tw-api-common";
import { withLoader } from "../../components";
import ItemCaseCreate from "../../components/forms/ItemCaseCreate";
import ItemDetailCard from "../../components/ItemDetailCard";
import TableCard from "../../components/TableCard";
import VerticalModal from "../../components/VerticalModal";
import { detailCaseColumns, locationColumns, poColumns, productColumns, 
  shipColumns, updateCaseColumns, vendorColumns } from "./ItemDetailColumns";

const getPOQuantity = (array: any) => {
  let po_quantity = 0;
  array.forEach((val: any) => {
    po_quantity += parseInt(val.sum);

  });
  return po_quantity;
};
const generateProductSummary = (product: any, poItems: any) => {
  const totalOrder = { order_qty: getPOQuantity(poItems) };
  let rowObj = totalOrder;
  if (product.length) {
    rowObj = Object.assign(product[0], totalOrder);
  }
  return [rowObj];
};

interface IProps extends RouteComponentProps<IMatchParams> {
  basePath: string;
  client: any;
  org: any;
  user: any;
}

interface IMatchParams {
  productId: string;
}

class ItemDetailView extends React.Component<IProps, any> {

  constructor(props: IProps) {
    super(props);
    this.state = {
      data: [],
      // searchError: '',
      poData: [],
      shipData: [],
      searchText: "",
      showCreateCase: false,
      caseData: [],
    };
    this.onRowClick = this.onRowClick.bind(this);

  }
  public async componentDidMount() {
    let sku;
    if (this.props.match) {
      sku = this.props.match.params.productId;
    }
    console.log("SETTING SKU TO " + sku);
    if (sku) {
      this.setSKU(sku);
    }
  }

  public async setSKU(sku: string) {
    const { startLoading, stopLoading } = this.props as any;
    const client: FcFetchClient = this.props.client;
    startLoading("Loading your data...");
    console.log("-4");

    const caseInfo = await client.getStuff(`SELECT * from case_assets ca
                                                    JOIN cases c on c.case_id = ca.case_id
                                                    WHERE ca.asset_id = '${sku}' and ca.asset_type = 'sku' `);
    console.log("-3");
    const poClient = await client.getPurchaseOrdersClient() as PurchaseOrdersClient;
    const contClient = await client.getContainersClient() as ContainersClient;
    console.log("-1");
    const product = await client.getStuff("SELECT * FROM products where sku ='" + sku + "'");

    const poItems = await poClient.queryItems("purchase_order_items.org_id =$1 and sku=$2",
      [this.props.user.org_id, sku], [], ["origin_container","b.name"]);

    console.log("RETURN FROM PO ITEMS");
    console.log(poItems);
    const poInfo = await client.getStuff(
      `SELECT purchase_orders.create_date,
      purchase_orders.purchase_order_id,
      --purchase_order_item_id,
      sum(quantity) as quantity,
      dst_container
      from purchase_order_items
      join purchase_orders on
      purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
      where sku= '` + sku + `' and purchase_order_items.org_id = '` + this.props.user.org_id + `'
      and purchase_orders.org_id = '` + this.props.user.org_id + `'
      GROUP BY
      purchase_orders.create_date,
      purchase_orders.purchase_order_id,
      --purchase_order_item_id,
      --quantity,
      dst_container
      ;`,
    );

    for (let i = 0; i < poItems.items.length; i++) {
      poItems.items[i].origin_container = poItems.items[i].origin_container.slice(poItems.items[i].origin_container.indexOf("&") + 1);
      poItems.items[i].issueLink = <Link to={{
        pathname: this.props.basePath + "/cases/create",
        state: {
          assets: [{ id: poItems.items[i].origin_container, type: AssetType.VENDOR }, { id: sku, type: AssetType.SKU }],
          orgs: [this.props.org.org_id],
          vendor: poItems.items[i].origin_container,
        },
      }}> {"create issue for sku "}</Link>;
    }

    const shipInfo = await this.props.client.getStuff(
      `SELECT shipment_items.sku,
      shipment_items.delivery_quantity,
      shipments.*
      from shipment_items
      join shipments
      on shipment_items.shipment_id = shipments.shipment_id and shipments.org_id = '` + this.props.user.org_id + `'
      where shipment_items.org_id ='` + this.props.user.org_id + `' and sku= '` + sku + `';`,
    );

    const locations = await contClient.getProductLocations(sku);
    const locationData = locations.map((l: any) => ({
      link: this.props.basePath + "/detail-location/" + l.containerId,
      ...l,
    }));

    console.log(locationData);

    console.log("######Venodr Data################");
    console.log(poItems.items);

    this.setState({
      // searchError: productSummary.length ? '' : "Sorry we couldn't find anything for this SKU",
      data: generateProductSummary(product, poItems.items),
      vendorData: poItems.items,
      sku,
      poData: poInfo,
      shipData: shipInfo,
      locationData,
      caseData: caseInfo,
    });

    stopLoading();
  }

  public onRowClick = (state: any, rowInfo: any, column: any, instance: any) => {
    return {
      onClick: () => {
        if (column.Header === "Purchase Order ID") {
          this.props.history.push(this.props.basePath + "/purchaseorders/detail/" + rowInfo.original.purchase_order_id);
        }
        if (column.Header === "Shipment ID") {
          this.props.history.push(this.props.basePath + "/shipments/detail/" + rowInfo.original.shipment_id);
        }
      },
    };
  }
  public componentDidUpdate(prevProps: any) {
    if (prevProps.match.params.productId !== this.props.match.params.productId) {
      this.setSKU(this.props.match.params.productId);
    }
  }
  /// Quickly querys the db to get sku for apn allows the user to search by apn
  public async queryAPN(apn: any) {
    const sku = await this.props.client.getStuff(`SELECT sku from products where apn='${apn}' or sku='${apn}'`);
    let searchSku;
    if (sku.length >= 1) {
      searchSku = sku[0].sku;
    }
    this.setSKU(searchSku);
  }

  public render() {
    /*this.state.caseData.forEach((c) => {
      if (c.case_priority === "High") {
        c.color_case_priority = <span style={{ color: "red" }}> {c.case_priority} </span>
      }
      if (c.case_priority === "Medium") {
        c.color_case_priority = <span style={{ color: "blue" }}> {c.case_priority} </span>
      }
      if (c.case_priority === "Low") {
        c.color_case_priority = <span style={{ color: "orange" }}> {c.case_priority} </span>
      }

      let show_case_id = c.case_id.slice(0, 12);

      c.show_case_id = <Link to={{ pathname: this.props.basePath + "/cases/detail", state: { case_id: c.case_id } }} >  {show_case_id} </Link>
      c.short_case_id = show_case_id;
    })*/
    return (
      <>
        <Card style={{ padding: 20 }} >
          {/*<h5 style={{ fontSize: 12, color: 'red' }}> {this.state.searchError}</h5>*/}
          {this.state.data && this.state.data.length > 0 &&
            <ItemDetailCard page="Sku"
              heading={this.state.sku}
              image={this.state.data[0].image}
              subComponent={<TableCard tableSpecs={{ filterable: false, rowNumber: 1 }} data={this.state.data} columns={productColumns} />}
            />
          }
          {((!this.state.data || this.state.data.length === 0) && this.state.poData.length === 0) &&
            <Container style={{ textAlign: "center", verticalAlign: "middle", paddingTop: 80, minHeight: 200, margin: 10 }}>
              <h4 > View Item details here</h4>
            </Container>
          }
          {this.state.vendorData && this.state.vendorData.length > 0 &&

            <Card style={{ padding: 20 }}>
              <TableCard heading="Vendors" tableSpecs={{ filterable: true, rowNumber: 5 }} data={this.state.vendorData} columns={vendorColumns} />
            </Card>
          }
          {this.state.poData && this.state.poData.length > 0 &&
            <Card style={{ padding: 20 }}>
              <TableCard heading="Purchase Orders" tableSpecs={{ filterable: true, rowNumber: 5 }} getTdProps={this.onRowClick} data={this.state.poData} columns={poColumns} />
            </Card>
          }
          {this.state.shipData && this.state.shipData.length > 0 &&
            <Card style={{ padding: 20 }}>
              <TableCard heading="Shipments" tableSpecs={{ filterable: true, rowNumber: 5 }} getTdProps={this.onRowClick} data={this.state.shipData} columns={shipColumns} />
            </Card>
          }
          {/*I cant get this work for target so i am just removing this from the console for target*/}
          {(this.state.locationData && this.props.user.org_id !== "mytarget") && this.state.locationData.length > 0 &&
            <Card style={{ padding: 20 }}>
              <TableCard heading="Locations" tableSpecs={{ filterable: true, rowNumber: 5 }} data={this.state.locationData} columns={locationColumns} />
            </Card>
          }
          {
            this.state.caseData && this.state.caseData.length > 0 &&
            <Card style={{ padding: 20 }}>
              <TableCard heading="Open Cases On SKU" tableSpecs={{ filterable: true, rowNumber: 5 }} getTdProps={this.onRowClick} data={this.state.caseData} columns={[{ Header: "Case Details", columns: detailCaseColumns }, { Header: "Case Updates", columns: updateCaseColumns }]} />
            </Card>
          }
        </Card>
      </>
    );
  }

  public createIssue() {
    const { createData } = this.state;
    const modalClose = () => this.setState({ showCreateCase: false, errorMessage: "", successMessage: "" });
    return (
      <>
        <VerticalModal title="Select Case Type"
          size="md"
          show={this.state.showCreateCase}
          onHide={modalClose}
          page={
            <ItemCaseCreate createData={createData} history={this.props.history} basePath={this.props.basePath} />
          }
        />
      </>
    );

  }
}
function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org,
  };
}
export default withLoader(withRouter(connect(mapStateToProps)(ItemDetailView))) ;
