import React from 'react';
import moment from 'moment';
import { Button } from 'react-bootstrap';
import _ from 'lodash'
import { Link } from 'react-router-dom'

const allPOColumns = [
    {
        Header: 'Purchase Order ID',
        accessor: 'purchase_order_id',
        Cell: (val) => {
            if (val.original.link) {
                return <Link to={val.original.link}> {val.original.purchase_order_id} </Link>
            } else {
                return null;
            }
        }

    },
    {
        Header: 'Created on',
        accessor: 'create_date',
        Cell: (val) => {
            const event = new Date(val.original.create_date);
            return <p> {event.toLocaleDateString(navigator.language)} </p>
        }
    },

    {
        Header: "SKU",
        accessor: 'sku',
        Cell: (val) => {
            if (val.original.sku_link) {
                return <Link to={val.original.sku_link}>{val.original.sku}</Link>
            } else {
                return null;
            }
        }
    },

    {
        Header: "Line Product ID",
        accessor: 'purchase_order_item_id'
    },
    {
        Header: "PO Cons Date",
        accessor: 'approx_recv_date',
        Cell: (val) => {
            const event = new Date(val.original.approx_recv_date);
            return <p> {event.toLocaleDateString(navigator.language)} </p>

        }
    },
    {
        Header: 'Quantity',
        accessor: 'quantity'
    },
    {
        Header: 'Product Name',
        accessor: 'name'
    },
    {
        Header: "Image",
        accessor: 'image',
        Cell: (val) => {
            return <img style={{ width: 60, height: 60 }} src={val.original.image} alt="Temporarily unavailable" />
        }
    }

]

var allPOColumnsNew = [
    {
        Header: 'Purchase Order ID',
        accessor: 'purchaseOrderId',
        Cell: (val) => {
            if (val.original.purchaseOrderId) {
                console.log(val.original);
                return <Link to={val.original.basePath + "/purchaseorders/detail/" + val.original.purchaseOrderId}> {val.original.purchaseOrderId} </Link>
            } else {
                return null;
            }
        }

    },
    {
        Header: 'Lot Number',
        accessor: 'lot',
        onlyfor: 'peacocks'
    },

    {
        Header: 'Created on',
        accessor: 'createDate',
        Cell: (val) => {
            if (val.original.createDate !== null) {
                const event = new Date(val.original.createDate);
                return <p> {event.toLocaleDateString(navigator.language)} </p>
            }

            else
                return "-";

        }
    },

    {
        Header: "SKU",
        accessor: 'productId',
        Cell: (val) => {
            if (val.original.productId) {
                return <Link to={val.original.basePath + "/Items/detail/" + val.original.productId}>{val.original.productId}</Link>
            } else {
                return null;
            }
        }
    },

    {
        Header: "Line Product ID",
        accessor: 'purchaseOrderItemRef',
        Cell: (val) => {
            if (val.original.purchaseOrderItemRef !== "default_line") {
                return <p> {val.original.purchaseOrderItemRef} </p>;
            }
            else {
                return "-";
            }
        }

    },


    {
        Header: 'ETA',
        accessor: 'eta',
        Cell: (val) => {
            if (val.original.eta !== null) {
                const event = new Date(val.original.eta);
                return <p> {event.toLocaleDateString(navigator.language)} </p>;
            }

            else return "-";
        }

    },

    {
        Header: "PO Cons Date",
        accessor: 'approxRecvDate',
        Cell: (val) => {
            const event = new Date(val.original.approxRecvDate);
            return <p> {event.toLocaleDateString(navigator.language)} </p>

        },
        onlyfor: 'hondauk'
    },
    {
        Header: 'Quantity',
        accessor: 'quantity'
    },

    {
        Header: 'Department',
        accessor: 'departmentid',
        onlyfor: 'peacocks'
    },

    {
        Header: 'Product Name',
        accessor: 'title'
    },
    {
        Header: "Image",
        accessor: 'imageLink',
        Cell: (val) => {
            if (val.original.imageLink !== null)
                return <img style={{ width: 60, height: 60 }} src={val.original.imageLink} alt="Temporarily unavailable" />;
            else
                return "";
        }
    }

]

// >>> for new PO format
var aggregatedPOColumnsnew = [
    {
        Header: 'Purchase Order ID',
        accessor: 'purchaseOrderId',

    },
    {
        Header: 'Created on',
        accessor: 'createDate',
        aggregate: vals => new Date(vals[0]),
        Cell: (val) => {
            if (!val.subRows) {
                return <p>   </p>
            }
            else {
                const event = new Date(val.subRows[0].createDate);
                return <p> {event.toLocaleDateString(navigator.language)} </p>
            }
        },
    },

    {
        Header: "SKUs",
        accessor: 'productId',
        sortable: true,
        aggregate: vals => vals.length,
        /*Aggregated: row => {
          return (
              row.subRows.length
          );
        },*/
        Cell: (row) => {
            if (row.original) {
                return <p> {row.original.productId} </p>
            }
            else {
                return <p> {row.subRows.length}</p>
            }
        }
    },
    {
        Header: "Products",
        accessor: 'purchaseOrderItemRef',
        aggregate: vals => vals.length,
        Cell: (row) => {
            if (row.original) {
                return <p> {row.original.purchase_order_item_id} </p>
            }
            else {
                return <p> {row.subRows.length}</p>
            }
        }
    },

    {
        Header: 'Quantity',
        accessor: 'quantity',
        aggregate: vals => _.sum(vals)
    },

    {
        Header: 'View PO Details',
        accessor: 'link',
        Aggregated: (val) => {

            if (val.subRows) {

                if (val.subRows[0]._original && val.subRows[0]._original.basePath) {
                    return <Link to={val.subRows[0]._original.basePath + "/purchaseorders/detail/" + val.subRows[0].purchaseOrderId}>       View PO Details   </Link>
                }
                else {
                    return <Link to={val.subRows[0].basePath + "/purchaseorders/detail/" + val.subRows[0].purchaseOrderId}>
                        View PO Details
                </Link>
                }
            }
        },
        Cell: (val) => {
            if (val.original) {
                return <Link to={val.original.basePath + "/Items/detail/" + val.original.productId}>
                    SKU Details
          </Link>
            } else {
                return null;
            }
        }


    }
];



const aggregatedPOColumns = [
    {
        Header: 'Purchase Order ID',
        accessor: 'purchase_order_id',

    },
    {
        Header: 'Created on',
        accessor: 'create_date',
        aggregate: vals => new Date(vals[0]),
        Cell: (val) => {
            if (!val.subRows) {
                return <p>   </p>
            }
            else {
                const event = new Date(val.subRows[0].create_date);
                return <p> {event.toLocaleDateString(navigator.language)} </p>
            }
        },
    },

    {
        Header: "SKUs",
        accessor: 'sku',
        sortable: true,
        aggregate: vals => vals.length,
        /*Aggregated: row => {
          return (
              row.subRows.length
          );
        },*/
        Cell: (row) => {
            if (row.original) {
                return <p> {row.original.sku} </p>
            }
            else {
                return <p> {row.subRows.length}</p>
            }
        }
    },
    {
        Header: "Line Product",
        accessor: 'purchase_order_item_id',
        aggregate: vals => vals.length,
        Cell: (row) => {
            if (row.original) {
                return <p> {row.original.purchase_order_item_id} </p>
            }
            else {
                return <p> {row.subRows.length}</p>
            }
        }
    },

    {
        Header: 'Quantity',
        accessor: 'quantity',
        aggregate: vals => _.sum(vals)
    },
    {
        Header: 'View PO Details',
        accessor: 'link',
        Aggregated: (val) => {

            if (val.subRows) {
                return <Link to={val.subRows[0].link}> View PO Details </Link>
            }
        },
        Cell: (val) => {
            if (val.original) {
                return <Link to={val.original.sku_link}> SKU Details </Link>
            } else {
                return null;
            }
        }


    }
]
const locationColumns = [{
    Header: 'Location',
    accessor: 'location',
    Cell: (val) => {
        if (val.original) {
            return <Link to={val.original.link}> {val.original.location} </Link>
        } else {
            return null;
        }
    }
},
{
    Header: "Location Type",
    accessor: 'type'
},
{
    Header: 'Quantity',
    accessor: 'quantity'
}


]
const shipColumns = [{
    Header: 'Shipment Ref',
    accessor: 'shipment_id',
    Cell: (val) => {
        return <Button size="sm" variant="link" > {val.original.shipmentRef} </Button>
    }
},
{
    Header: "SKU",
    accessor: 'sku'
},
{
    Header: "LOT",
    accessor: 'production_lot'
},
{
    Header: "Shipped Quantity",
    accessor: 'delivery_quantity'
},
{
    Header: 'Port of Loading ',
    accessor: 'sourceContainerId',
    Cell: (val) => {
        if (val.original.sourceContainerId) {
            return <p> {val.original.sourceContainerId.slice(val.original.sourceContainerId.indexOf('&') + 1)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'Destination ',
    accessor: 'destinationContainerId',
    Cell: (val) => {
        if (val.original.destinationContainerId) {
            return <p> {val.original.destinationContainerId.slice(val.original.destinationContainerId.indexOf('&') + 1)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'ETD',
    accessor: 'etd',
    Cell: (val) => {
        if (val) {
            const event = new Date(val.original.etd);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'ETA',
    accessor: 'eta',
    Cell: (val) => {
        if (val) {
            const event = new Date(val.original.eta);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'ATD',
    accessor: 'atd',
    Cell: (val) => {
        if (val) {
            const event = new Date(val.original.atd);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'ATA',
    accessor: 'ata',
    Cell: (val) => {
        if (val.original.ata) {
            const event = new Date(val.original.ata);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        } else {
            return null;
        }
    }
}
]
const itemColumns = [{
    Header: 'SKU',
    accessor: 'productId',
    Cell: (val) => {
        return <Button size="sm" variant="link" > {val.original.productId} </Button>
    }
},
{
    Header: 'Received Quantity',
    accessor: 'receivedQuantity'
},
{
    Header: 'Quantity',
    accessor: 'quantity'
},
{
    Header: 'Status',
    accessor: 'status'
},
{
    Header: 'PO Cons Date',
    accessor: 'approx_recv_date',
    Cell: (val) => {
        const event = new Date(val.original.approx_recv_date);
        return <p> {event.toLocaleDateString(navigator.language)}</p>
    }
},
{
    Header: 'Check Release Status',
    accessor: 'checkReleaseStatus'
},
{
    Header: 'Factory Booking Date',
    accessor: 'factoryBookingDate',
    Cell: (val) => {
        if (val.original.factoryBookingDate) {
            const event = new Date(val.original.factoryBookingDate);
            return <p> {event.toLocaleDateString(navigator.language)}</p>
        } else {
            return null;
        }
    }
},
{
    Header: 'Origin Factory',
    accessor: 'origin_container',
    Cell: (val) => {
        return <p> {val.original.originContainer ? val.original.originContainer.slice(8) : "N/A"}</p>
    }
},
{
    Header: 'Origin Country',
    accessor: 'countryOfOrigin'
}
]

export { locationColumns, itemColumns, shipColumns, allPOColumns, aggregatedPOColumns, aggregatedPOColumnsnew, allPOColumnsNew }