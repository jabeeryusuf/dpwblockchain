import React from 'react';
import _ from 'lodash';
import { Button } from 'react-bootstrap';
import { FaExclamationCircle } from "react-icons/fa";
import { Link } from "react-router-dom";
import { TrackingStatus } from 'tw-api-common';

const originColumns = [{
    Header: 'Origin Location',
    accessor: 'location',
    Cell: (val) => {
        return <p>{val.original.location.slice(val.original.location.indexOf('&') + 1)}</p>
    }
},
{
    Header: 'Quantity',
    accessor: 'quantity'
}]
const dstColumns = [{
    Header: 'Location',
    accessor: 'container_id',
    Cell: (val) => {
        return <p>{val.original.container_id.slice(val.original.container_id.indexOf('&') + 1)}</p>
    }
},
{
    Header: "Volume (cbm)",
    accessor: 'volume'
},
{
    Header: 'Quantity',
    accessor: 'quantity'
}]
const columns = [
    {
        Header: 'PO Number',
        accessor: 'po_num',
        Cell: (val) => {
            return <Button size="sm" variant="link" > {val.original.po_num} </Button>
        }
    },
    {
        Header: 'Location',
        accessor: 'container_id',
        //Enables case insensitive filtering
        filterMethod: (filter, row) => {
            const id = filter.pivotId || filter.id;
            if (row[id] !== null) {
                return (
                    row[id] !== undefined ?
                        String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) || String(row[id].slice(row[id].indexOf('&') + 1).toLowerCase()).startsWith(filter.value.toLowerCase())
                        :
                        true
                );
            }
            else {
                return null
            }
        },
        Cell: (val) => {
            if (val.original.location) {
                return <Button size="sm" variant="link" >val.original.location</Button>
            }
            if (val.original.container_id) {
                return <Button size="sm" variant="link" > {val.original.container_id.slice(val.original.container_id.indexOf('&') + 1)} </Button>
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Volume (cbm)",
        accessor: 'volume'
    },
    {
        Header: 'Quantity',
        accessor: 'quantity'
    },
    {
        Header: 'Status',
        accessor: 'status',
        //Enables case insensitive filteringg
        filterMethod: (filter, row) => {
            const id = filter.pivotId || filter.id;
            if (row[id] !== null) {
                return (
                    row[id] !== undefined ?
                        String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase())
                        :
                        true
                );
            }
            else {
                return null
            }
        }
    },
    {
        Header: 'Destination',
        accessor: 'dstName'
    }

];

const actionColumns = [
    {
        Header: 'Container ID',
        accessor: 'container_id'
    },
    {
        Header: 'Request Type',
        accessor: 'type'
    },
    {
        Header: 'Status',
        accessor: 'action_status'
    },
    {
        Header: 'Location',
        accessor: 'name'
    },
    {
        Header: 'Request Date',
        accessor: 'request_date'
    },
    {
        Header: 'Edit Request',
        accessor: 'button'
    }
];

const consoleCurrentColumns = [
    {
        Header: "Container ID",
        accessor: "container_id"
    },
    {
        Header: "Request Type",
        accessor: "type"
    },
    {
        Header: "Request Date",
        accessor: "request_date"
    },

    {
        Header: "View Request",
        accessor: 'button'
    }
];
const containerColumns = [
    {
        Header: "Container Number",
        accessor: "container_id"
    },
]
const consolePastColumns = [
    {
        Header: "Container ID",
        accessor: "container_id"
    },
    {
        Header: "Request Type",
        accessor: "type"
    },
    {
        Header: "Request Date",
        accessor: "request_date"
    },
    {
        Header: "Resolve Date",
        accessor: "approval_date"
    },
    {
        Header: "Made By",
        accessor: "org"
    },
    {
        Header: "Location",
        accessor: "name"
    },
    {
        Header: "Status",
        accessor: "action_status"
    }
];

const skuColumns = [
    {
        Header: "Shipping Container",
        accessor: 'container_id',
        Cell: (val) => {
            if (val.original.type === 'shipping-container') {
                return <p> {val.original.container_id.slice(19)}</p>
            }
            else {
                return null;
            }
        }
    },
    {
        Header: 'SKU',
        accessor: 'sku'
    },
    {
        Header: 'Product Name',
        accessor: 'productname'
    },
    {
        Header: 'Quantity',
        accessor: 'containerquantity'
    },
    {
        Header: "Shipment ID",
        accessor: 'expediteLink'
    }
];

//table columns skus by location when location is not a ship
const skuColumns2 = [
    {
        Header: 'SKU',
        accessor: 'sku'
    },
    {
        Header: 'Product Name',
        accessor: 'productname'
    },
    {
        Header: 'Quantity',
        accessor: 'containerquantity'
    }
];

const etaColumns = [
    {
        Header: "Origin Container",
        accessor: "origin_container"
    },
    {
        Header: "Estimated Warehouse Arrival Date",
        accessor: "warehouse_estimate"
    },
    {
        Header: "Warehouse Arrival Date",
        accessor: "warehouse_arrival"
    },
    { Header: "Estimated Retailer Arrival", accesor: "estimate" },
    {
        Header: "Retailer Arrival",
        accessor: "arrival"
    },
    {
        Header: 'Destination Container',
        accessor: 'dst_container'
    }
];

const shipmentColumns = [
    {
        Header: "Last Port",
        accessor: "last_port"
    },
    {
        Header: "Next Port",
        accessor: "next_port"
    },
    {
        Header: "Latitude",
        accessor: "lat"
    },
    {
        Header: "Longitutde",
        accessor: "lng"
    },
    {
        Header: "Timestamp",
        accessor: "timestamp"
    }
];

const buildingColumns = (basePath) => ([
    {
        Header: "Name",
        accessor: "locationTraker",
        Cell: (val) => {
            return <Link to={basePath + "/detail-location/" + val.original.container_id}>{val.original.name} </Link>
        }
    },
    {
        Header: "Address",
        accessor: "address"
    },
    {
        Header: "Latitude",
        accessor: "lat"
    },
    {
        Header: "Longitude",
        accessor: "lng"
    },
    {
        Header: "Building Code",
        accessor: "code"
    }
]);

const vesselColumns = (basePath) => ([
    {
        Header: "Latitude",
        accessor: "lat"
    },
    {
        Header: "Longitude",
        accessor: "lng"
    },
    {
        Header: "Vessel Name",
        accessor: "code"
    }
]);

const expediteColumns = [
    {
        Header: "Location",
        accessor: "location"
    },
    {
        Header: "Container Number",
        accessor: "container_id"
    },
    {
        Header: "Expedite",
        accessor: "expedite",
        style: { margin: 0, padding: 0 }
    }
];

const serviceColumns = [
    {
        Header: "Username",
        accessor: "external_username"
    },
    {
        Header: "Service",
        accessor: "service"
    }
]

const currentShipColumns = (someFunc) => ([
    {
        Header: "",
        accessor: "trackings",
        width: 20,
        Cell: (val) => {
            // Check to see if the tracking_status is in any of the failure statuses

            //return val.original.trackings.length;

            if (val.original.trackings.length==0 || val.original.trackings[0].status == TrackingStatus.PAUSED) {
                return <FaExclamationCircle style={{ margin: 0, padding: 0 }} color={"red"} />
            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Shipment Ref",
        accessor: "shipmentRef",
        Cell: (val) => {
            if (val.original.shipmentId) {
               
                
                
                    //console.log("No Base Path");
                    return <Link to={"/"+val.original.orgId+"/shipments/detail/" + val.original.shipmentId}>
                        {val.original.shipmentRef}
                    </Link>
                
            }
            else {
                return null
            }
        }
    },

    
    {
        Header: "ATD",
        accessor: "atd",
        Cell: (val) => {
            if (val.original.atd) {
                const event = new Date(val.original.atd);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Target ETA",
        accessor: "expected_eta",
        Cell: (val) => {
            if (val.original.expected_eta) {
                const event = new Date(val.original.expected_eta);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },

    {
        Header: "ETA",
        accessor: "eta",
        Cell: (val) => {
            if (val.original.eta) {
                const event = new Date(val.original.eta);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "ATA",
        accessor: "ata",
        Cell: (val) => {
            if (val.original.ata) {
                const event = new Date(val.original.ata);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },

    {
        Header: "carrier",
        accessor: "carrier",
        Cell: (val) => {
           console.log("Carrier="+val.original.carrier)
          if (val.original.carrier) {
              const carrier = val.original.carrier;
             
              
              if(carrier==='none' || carrier===null)
              {
                  return <Link to="#">Update Carrier</Link>
                return null;  
              }
              else
              return (carrier);
          }
          else {
             var sid = val.original.shipmentId
            return <button onClick={() => someFunc(sid)} value="Update Carrier"> Update Carrier </button>
        
              
          }
      }


    },




    {
        Header: "GATEOUT",
        accessor: "gateout",
        Cell: (val) => {
            if (val.original.gateout) {
                const event = new Date(val.original.gateout);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },

    {
        Header: "Vessel Name",
        accessor: "vesselName"
       
    },

    {
        Header: "Voyage",
        accessor: "voyage"
       
    },


    {
        Header: "Port of Loading",
        accessor: "sourceContainerId",
        Cell: (val) => {
            if (val.original.sourceContainerId) {
               let portLoad = val.original.sourceContainerId.slice(val.original.sourceContainerId.indexOf('&') + 1);
                     if (portLoad === 'dummy') {
                         return (<font color="red">
                                {portLoad}
                                </font>)
                    } else { 
                    return (portLoad) 
                    }
            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Port of Destination",
        accessor: "destinationContainerId",
        Cell: (val) => {
            if (val.original.destinationContainerId) {
                let destPort = val.original.destinationContainerId.slice(val.original.destinationContainerId.indexOf('&') + 1);
                
                        if (destPort === 'dummy') {
                          return (<font color="red">
                            {destPort}
                          </font>)
                        }
                        else {
                          return (destPort)
                        }
            }
            else {
                return null;
            }
        }

    }

])

const currentShipColumnsSeattle = [
    {
        Header: "",
        accessor: "tracking",
        width: 20,
        Cell: (val) => {
            if (val.original.tracking_status == 'failure-no-results') {
                return (
                    <FaExclamationCircle style={{ margin: 0, padding: 0 }} color={"red"} />
                )
            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Shipment ID",
        accessor: "shipmentLink"
    },
    {
        Header: "Bol",
        accessor: "bol",
        Cell: (val) => {
            if (val.original.bol) {
                return (
                    val.original.bol.slice(val.original.bol.indexOf('&') + 1)
                )
            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Container Number",
        accessor: "ref"
    },
    {
        Header: "Estimated Time of Arrival",
        accessor: "eta",
        Cell: (val) => {
            if (val.original.eta) {
                const event = new Date(val.original.eta);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Port of Loading",
        accessor: "source_name"
    },
    {
        Header: "Port of Destination",
        accessor: "dest_name"
    },
    {
        Header: "Vessel Name",
        accessor: "vessel_name"
    },
    {
        Header: "Entry Filed",
        accessor: "entry_filed"
    },
    {
        Header: "Entry Released",
        accessor: "entry_released"
    },
    {
        Header: "DO Issued",
        accessor: "do_issued"
    },

]

const pastShipColumnsSeattle = [
    {
        Header: "",
        accessor: "tracking",
        width: 20,
        Cell: (val) => {
            return null;
        }
    },
    {
        Header: "Shipment ID",
        accessor: "shipmentLink"
    },
    {
        Header: "Bol",
        accessor: "bol",
        Cell: (val) => {
            if (val.original.bol) {
                return (
                    val.original.bol.slice(val.original.bol.indexOf('&') + 1)
                )
            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Time of Arrival",
        accessor: "ata",
        Cell: (val) => {
            if (val.original.ata) {
                const event = new Date(val.original.ata);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Time of Departure",
        accessor: "format_atd",
        Cell: (val) => {
            if (val.original.atd) {
                const event = new Date(val.original.atd);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Port of Loading",
        accessor: "source_name"
    },
    {
        Header: "Port of Destination",
        accessor: "dest_name"
    },
    {
        Header: "Vessel Name",
        accessor: "vessel_name"
    },
    {
        Header: "Entry Filed",
        accessor: "entry_filed"
    },
    {
        Header: "Entry Released",
        accessor: "entry_released"
    },
    {
        Header: "DO Issued",
        accessor: "do_issued"
    }
]

const pastShipColumns = [
    {
        Header: "",
        accessor: "tracking",
        width: 20,
        Cell: (val) => { return null }
    },
    {
        Header: "Shipment ID",
        accessor: "shipmentLink"
    },
    {
        Header: "Bol",
        accessor: "bol",
        Cell: (val) => {
            if (val.original.bol) {
                return val.original.bol.slice(val.original.bol.indexOf('&') + 1);

            }
            else {
                return null;
            }
        }
    },
    {
        Header: "Time of Arrival",
        accessor: "ata",
        Cell: (val) => {
            if (val.original.ata) {
                const event = new Date(val.original.ata);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Time of Departure",
        accessor: "atd",
        Cell: (val) => {
            if (val.original.atd) {
                const event = new Date(val.original.atd);
                return (
                    event.toLocaleDateString(navigator.language)
                )
            }
            else {
                return null
            }
        }
    },
    {
        Header: "Port of Loading",
        accessor: "source_name"
    },
    {
        Header: "Port of Destination",
        accessor: "dest_name"
    },
    {
        Header: "Vessel Name",
        accessor: "vessel_name"
    }
]

// for the warehouse location detail component
const warehouseLocationColumns = (basePath) => ([
    {
        Header: "Purchase Order",
        columns: [
            {
                Header: "Purchase Order ID",
                id: "purchase_order_id",
                accessor: "purchase_order_id",
                style: { textAlign: "left" }
            },
            {
                Header: "Purchase Order Product ID",
                id: "purchase_order_item_id",
                accessor: "purchase_order_item_id",
            }
        ]
    },
    {
        Header: "Info",
        columns: [
            {
                Header: "SKU",
                accessor: "sku",
                aggregate: vals => {
                    return _.countBy(vals, 'length')
                },
                Aggregated: row => {
                    return (
                        <span>
                            {row.subRows.length}
                        </span>
                    );
                }
            },
            {
                Header: "Shipment ID ",
                accessor: "shipmentTraker",
                /*aggregate: (vals) => {
                      style={{ align : "center"  }}
                  },*/
                Aggregated: (vals) => {
                    let countShipID = 0;
                    for (let i = 0; i < vals.subRows.length; i++) {
                        if (vals.subRows[i]._original.shipment_id !== '') {
                            countShipID++;
                        }
                    }
                    return countShipID > 0 ? (<span> {countShipID} Shipment ID available </span>) : (<span> No Shipment ID available </span>)
                },
                Cell: (val) => {
                    return <Link to={basePath + "/shipments/detail/" + val.original.shipment_id}>{val.original.shipment_id} </Link>
                }
            },
            {
                Header: "Total Carton",
                accessor: "total_carton",
                aggregate: vals => _.sum(vals),
                Aggregated: row => {
                    return (
                        <span>
                            {row.value}
                        </span>
                    );
                }
            },
            {
                Header: "Quantity",
                accessor: "delivery_quantity",
                aggregate: vals => _.sum(vals),
                Aggregated: row => {
                    return (
                        <span>
                            {row.value}
                        </span>
                    );
                }
            }
        ]
    }
])
const locationSubComponent = [
    {
        Header: "Product Info",
        columns: [
            {
                Header: "Product Name",
                id: "name",
                accessor: "name"
            },
            {
                Header: "Department",
                id: "department",
                accessor: "department"
            },
            {
                Header: "Product Image",
                id: "imageLink",
                accessor: "imageLink",
            }
        ]
    }
]

// for factory detail location component
const factoryLocationColumns =
    [
        {
            Header: "Purchase Order",
            columns: [
                {
                    Header: "Purchase Order ID",
                    id: "purchase_order_id",
                    accessor: "purchase_order_id",
                    style: { textAlign: "left" }
                },
                {
                    Header: "Purchase Order Product ID",
                    id: "purchase_order_item_id",
                    accessor: "purchase_order_item_id"
                }
            ]
        },
        {
            Header: "Info",
            columns: [
                {
                    Header: "SKU",
                    accessor: "sku",
                    aggregate: vals => {
                        return _.countBy(vals, 'length')
                    },
                    Aggregated: row => {
                        return (
                            <span>
                                {row.subRows.length}
                            </span>
                        );
                    }
                },
                {
                    Header: "Total Quantity",
                    accessor: "quantity",
                    aggregate: vals => _.sum(vals),
                    Aggregated: row => {
                        return (
                            <span>
                                {row.value}
                            </span>
                        );
                    }
                },
                {
                    Header: "Receive Quantity",
                    accessor: "recv_quantity",
                    aggregate: vals => _.sum(vals),
                    Aggregated: row => {
                        return (
                            <span>
                                {row.value}
                            </span>
                        );
                    }
                },
                {
                    Header: "Quantity at factory",
                    accessor: "pending_quantity",
                    aggregate: vals => _.sum(vals),
                    Aggregated: row => {
                        return (
                            <span>
                                {row.value}
                            </span>
                        );
                    }
                }
            ]
        }
    ]

const vendorGroupColumns = [
    {
        Header: "Vendor Name",
        accessor: "link"
    },
    {
        Header: "Vendor Code",
        accessor: "vendorCode",
        filterable: true
    },
    {
        Header: "Number of contacts in group",
        accessor: "users.length"
    }
]

const feedstatusColumns = [
    {
        Header: "ContainerNumber",
        accessor: "cnumber"
    },
    {
        Header: "Status",
        accessor: "status"
    },
    {
        Header: "Carrier",
        accessor: "carrier"
    }
    
]

const userGroupColumns = [
    {
        Header: "Group Name",
        accessor: "link"
    },
    {
        Header: "Number of contacts",
        accessor: "users.length"
    },
    {
        Header: "Number of groups",
        accessor: "childGroups.length"
    }
]

export {
    containerColumns,
    expediteColumns,
    columns,
    dstColumns,
    originColumns,
    etaColumns,
    skuColumns,
    skuColumns2,
    shipmentColumns,
    actionColumns,
    consoleCurrentColumns,
    consolePastColumns,
    buildingColumns,
    serviceColumns,
    currentShipColumns,
    currentShipColumnsSeattle,
    pastShipColumnsSeattle,
    pastShipColumns,
    warehouseLocationColumns,
    factoryLocationColumns,
    locationSubComponent,
    vendorGroupColumns,
    userGroupColumns,
    vesselColumns,
    feedstatusColumns
};
