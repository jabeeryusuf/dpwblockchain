
const milestoneColumns = [
  {
    Header: "PO Line",
    accessor: "purchase_order_item_id",
    filterable: true
  },
  {
    Header: "SKU",
    accessor: "sku"
  },
  {
    Header: "Create Date",
    accessor: "create_date"
  },

  {
    Header: "Factory Booking Date",
    accessor: "factory_booking_date"
  },
  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  {
    Header: "Check Release Date",
    accessor: "check_release_date"
  },
  {
    Header: "Check Release Status",
    accessor: 'check_release_status'
  },
  {
    Header: "Factory Delivery Date",
    accessor: "factory_delivery_date"
  },
  {
    Header: "Receive Date",
    accessor: "receive_date"
  },
  {
    Header: "Shipment Allocation Date",
    accessor: "shipment_allocation_date"
  },
  {
    Header: "Time of Departure",
    accessor: "atd"
  },
  {
    Header: "Time of Arrival",
    accessor: "ata"
  },
];

const allColumns = [
  {
    Header: "PO ID",
    accessor: "poLink",
  },
  {
    Header: "Item Name",
    accessor: "prodname"
  },
  {
    Header: "Product SKU",
    accessor: "skuLink",
  },
  {
    Header: 'Quantity',
    accessor: 'quantity',
    width: 200
  },
  {
    Header: "Create Date",
    accessor: "create_date"
  },

  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  
  {
    Header: "Factory Booking Date",
    accessor: "factory_booking_date"
  },
  {
    Header: "Check Release Date",
    accessor: "check_release_date"
  },
  {
    Header: "Check Release Status",
    accessor: 'check_release_status'
  },

  {
    Header: "Factory Delivery Date",
    accessor: "factory_delivery_date"
  },
  {
    Header: "Receive Date",
    accessor: "receive_date"
  },
  {
    Header: "Shipment Allocation Date",
    accessor: "shipment_allocation_date"
  },
  {
    Header: "Time of Departure",
    accessor: "atd"
  },
  {
    Header: "Time of Arrival",
    accessor: "ata"
  },
  {
    Header: "Photo",
    accessor: "pic",
    width: 100
  }
];

const exceptionColumns = [
  {
    Header: 'Purchase Order ID',
    accessor: 'poLink',
    width: 200,
  },
  {
    Header: "Item Name",
    accessor: "prodName"
  },
  {
    Header: 'Product SKU',
    accessor: 'skuLink',
    width: 100
  },
  {
    Header: "Quantity",
    accessor: "quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "department",
    width: 150
  },
  {
    Header: "Current Location",
    accessor: "location",
    Cell: (val) => {
      if (val.original.location) {
        return (
          val.original.location.slice(val.original.location.indexOf('&') + 1)
        )
      }
      return null
    }
  },
  {
    Header: "Destination",
    accessor: "dest_name",
    width: 100
  },
  {
    Header: 'Issue',
    accessor: 'msg'
  },
  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  {
    Header: "Check Release Status",
    accessor: 'check_release_status'
  },
 /* {
    Header: 'Last Recorded Timestamp',
    accessor: 'timestamp'
  },*/
  {
    Header: "Photo",
    accessor: 'pic',
    width: 100
  }
 
];

const factory_appointment_dateColumns = [
  {
    Header: 'Purchase Order ID',
    accessor: 'poLink',
    width: 200,
  },
  {
    Header: "Item Name",
    accessor: "prodName"
  },
  {
    Header: 'Product SKU',
    accessor: 'skuLink',
    width: 100
  },
  {
    Header: "Quantity",
    accessor: "quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "department",
    width: 150
  },
  {
    Header: "Current Location",
    accessor: "location"
  },
  {
    Header: "Factory",
    accessor: "origin_name"
  },
  {
    Header: "Destination",
    accessor: "dest_name",
    width: 100
  },
  {
    Header: 'Issue',
    accessor: 'msg'
  },
  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  {
    Header: "Check Release Status",
    accessor: 'check_release_status'
  },
 /* {
    Header: 'Last Recorded Timestamp',
    accessor: 'timestamp'
  },*/
  {
    Header: 'Factory Boooking Status',
    accessor: 'factory_booking_status',
    width: 100,
    filterable: true,
    filterMethod: (filter, row) => {
      return String(row[filter.id]).toLowerCase().startsWith(String(filter.value).toLowerCase());
    }
  },
  {
    Header: "Photo",
    accessor: 'pic',
    width: 100
  }
  // {
  //   Header: "Create Case",
  //   accessor: "issueButton"
  // }
];

const factory_booking_dateColumns = [
  {
    Header: 'Purchase Order ID',
    accessor: 'poLink',
    width: 200,
  },
  {
    Header: "Item Name",
    accessor: "prodName"
  },
  {
    Header: 'Product SKU',
    accessor: 'skuLink',
    width: 100
  },
  {
    Header: "Quantity",
    accessor: "quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "department",
    width: 150
  },
  {
    Header: "Destination",
    accessor: "dest_name",
    width: 100
  },
  {
    Header: 'Issue',
    accessor: 'msg'
  },
  /*{
    Header: 'Last Recorded Timestamp',
    accessor: 'timestamp'
  },*/
  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  {
    Header: 'Check Release Status',
    accessor: 'check_release_status',
    width: 150
  },
  {
    Header: "Photo",
    accessor: 'pic',
    width: 100
  }
  // {
  //   Header: "Create Case",
  //   accessor: "issueButton"
  // }
];

const factory_delivery_dateColumns = [
  {
    Header: 'Purchase Order ID',
    accessor: 'poLink',
    width: 200,
  },
  {
    Header: "Item Name",
    accessor: "prodName"
  },
  {
    Header: 'Product SKU',
    accessor: 'skuLink',
    width: 100
  },
  {
    Header: "Quantity",
    accessor: "quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "department",
    width: 150
  },
  {
    Header: "Destination",
    accessor: "dest_name",
    width: 100
  },
  {
    Header: 'Issue',
    accessor: 'msg'
  },
  {
    Header: 'PO Consolidation Date',
    accessor: 'approx_recv_date',
    width: 200
  },
  /*{
    Header: 'Last Recorded Timestamp',
    accessor: 'timestamp'
  },*/
  {
    Header: 'Check Release Date',
    accessor: 'check_release_date',
    width: 150
  },
  {
    Header: 'Check Release Status',
    accessor: 'check_release_status',
    width: 150,
    filterable: true,
    filterMethod: (filter, row) => {
      return String(row[filter.id]).toLowerCase().startsWith(String(filter.value).toLowerCase());
    }
  },
  {
    Header: "Photo",
    accessor: 'pic',
    width: 100
  },
  // {
  //   Header: "Create Case",
  //   accessor: "issueButton"
  // }
];

const distinctSkuColumnsnew = [
  {
    Header: "Product SKU",
    accessor: "skuLink",
    width: 100
  },
  {
    Header: "Item Name",
    accessor: "info.prodName"
  },
  {
    Header: "Quantity",
    accessor: "info.quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "info.department",
    width: 150
  },
  {
    Header: "Destination",
    accessor: "info.dest_name",
    width: 125
  },
  {
    Header: "Factories",
    accessor: "info.factory"
  },
  {
    Header: "Issue",
    accessor: "info.msg"
  },
  {
    Header: "Photo",
    accessor: "info.pic",
    width: 100
  }
]

const distinctSkuColumns = [
  {
    Header: "Product SKU",
    accessor: "skuLink",
    width: 100
  },
  {
    Header: "Item Name",
    accessor: "info.prodName"
  },
  {
    Header: "Quantity",
    accessor: "info.quantity",
    width: 100
  },
  {
    Header: "Department",
    accessor: "info.department",
    width: 150
  },
  {
    Header: "Factories",
    accessor: "info.factory"
  },
  {
    Header: "Destination",
    accessor: "info.dest_name",
    width: 125
  },
  {
    Header: "Issue",
    accessor: "info.msg"
  },
  /*{
    Header: "Last Recorded Timestamp",
    accessor: "info.timestamp"
  },*/
  {
    Header: "Photo",
    accessor: "info.pic",
    width: 100
  }
  // {/*
  //   Header: "Create Case",
  //   accessor: "info.issueButton"
  // */}
]


export {
  milestoneColumns,
  allColumns,
  exceptionColumns,
  factory_appointment_dateColumns,
  factory_booking_dateColumns,
  factory_delivery_dateColumns,
  distinctSkuColumns,
  distinctSkuColumnsnew
}