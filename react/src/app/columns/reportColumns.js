import Moment from 'moment';
import React from 'react';
import {Link} from 'react-router-dom'
const productionColumns = [
    {
      Header: 'Country of Origin',
      accessor: 'org_country'
    },
    {
      Header: 'Port of Loading',
      accessor: 'port_of_loading'
    },

    {
      Header: 'Production Quantity',
      accessor: 'sum'
    }
  ]
  
  const arrivalColumns = [
    {
      Header: "Vessel Sailing Date",
      accessor: "etd"
    },
    {
      Header: "Arrival Into Port",
      accessor: "eta"
    },

    {
      Header: "Product Quantity",
      accessor: "sum"
    }
  ]
  const lateShipColumns = [
    {
      Header: 'Shipment ID',
      accessor:'shipment_id',
      Cell: (val) => {
        if(val.original.shipment_id){
          return <Link to ={val.original.basePath + '/shipments/detail/'+ val.original.shipment_id}> {val.original.shipment_ref} </Link>
        
        }
      }
    },
    {
      Header: "Original ETA",
      accessor: 'original_eta',
      Cell: (val) => {
        if(val.original.original_eta){
          const event = new Date(val.original.original_eta);
          return ( 
            event.toLocaleDateString(navigator.language)
          )
        }
      }
    },
    {
      Header: "Target ETA",
      accessor: 'expected_eta',
      Cell: (val) => {
         if(val.original.expected_eta){
          const event = new Date(val.original.expected_eta);
          return (
            event.toLocaleDateString(navigator.language)
          )
        }
        else
        return "";
      }
    },

    {
      Header: "Revised ETA",
      accessor: 'eta',
      Cell: (val) => {
        if(val.original.ata){
          const event = new Date(val.original.ata);
          return (
            event.toLocaleDateString(navigator.language)
          )
        }
        else if(val.original.eta){
          const event = new Date(val.original.eta);
          return (
            event.toLocaleDateString(navigator.language)
          )
        }
      }
    },
    {
      Header: "ATA",
      accessor: 'ata',
      Cell: (val) => {
        if(val.original.ata){
          const event = new Date(val.original.ata);
          return (
            event.toLocaleDateString(navigator.language)
          )
        }
        else{
          return 'Not Arrived' 
        }
      }
    },
    {
      Header: 'Days Late',
      id:'late',
      accessor: (val) =>{
        let currentEta = val.ata?val.ata: val.eta
        let diff = Moment(currentEta).diff(Moment(val.original_eta),'days')
        return parseInt(diff)
      },
      Cell: (val)=>{
        if(val.original.original_eta && (val.original.eta || val.original.ata)){
          let currentEta = val.original.ata?val.original.ata: val.original.eta
          let diff = Moment(currentEta).diff(Moment(val.original.original_eta),'days')
          if(diff>0){
            return diff
          }
          else{
            return "Less than 1 day"
          }
        }
      }

    },
    {
      Header: "Origin",
      accessor: 'source_container_id',
      Cell: (val)=>{
        if(val.original.source_container_id){
          return val.original.source_container_id.slice(val.original.source_container_id.indexOf('&')+1)
        }
      }
    },
    {
      Header: 'Destination',
      accessor: 'destination_container_id',
      Cell: (val)=>{
        if(val.original.destination_container_id){
          return val.original.destination_container_id.slice(val.original.destination_container_id.indexOf('&')+1)
        }
      }
    },
    {
      Header: 'Vessel',
      accessor: 'vessel_name'
    }

  ]
  const shipColumns = [
    {
      Header: "Ship By Date",
      accessor: "approx_recv_date"
    },
    {
      Header: "Product Quantity",
      accessor: "sum"
    }
  ]
  
  const dst_containers = [
    {
      title: 'Seattle',
      value: 'SEA'
    },
    {
      title: 'Jakarta',
      value: 'JKT'
    },
    {
      title: 'Bangkok',
      value: 'BKK'
    },
  ]
  export {productionColumns,arrivalColumns,shipColumns,dst_containers,lateShipColumns}