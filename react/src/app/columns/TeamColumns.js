import Moment from 'moment';
import {Link} from 'react-router-dom';
import React from 'react';
const userColumns = [
  {
    Header: 'Name',
    accessor: 'name',
    width: 200,
  },
  {
    Header: "Email",
    accessor: "email"
  },
  {
    Header: 'Joined On',
    accessor: 'create_time',
    Cell: (val) => {
      const event = new Date(val.original.create_time);
      return (
        event.toLocaleDateString(navigator.language)
      )
    }
  },
  {
    Header: "Delete User",
    accessor: "delete",
    Cell: (val)=>{
      return(
        <Link to='#' onClick = {()=>{val.original.onClick(val.original)}} >
         Delete
      </Link>
      )
    }
  }

];
const inactiveUserColumns = [
  {
    Header: 'Name',
    accessor: 'name',
    width: 200,
  },
  {
    Header: "Email",
    accessor: "email"
  },
  {
    Header: 'Deleted On',
    accessor: 'update_time',
    Cell: (val) => {
      const event = new Date(val.original.update_time);
      return (
        event.toLocaleDateString(navigator.language)
      )
    }
  },
  {
    Header: "Restore User",
    accessor: "restore",
    Cell: (val)=>{
      return(
        <Link to='#' onClick = {()=>{val.original.onClick(val.original)}} >
         Restore
      </Link>
      )
    }
  }

];
const inviteColumns = [

  {
    Header: "Invite Sent To",
    accessor: "email"
  },
  {
    Header: "Invite Sent by",
    accessor: "sender_email"
  },
  {
    Header: "Sender",
    accessor:"sender_name"
  },
  {
    Header: 'Sent On',
    accessor: 'create_time',
    Cell: (val) => {
      if (val.original.create_time) {
        const event = new Date(val.original.create_time);
        return (
          event.toLocaleDateString(navigator.language)
        )
      }
      return null
    }
  },
  {
    Header: "Resend Invitation",
    accessor:"resend",
    Cell: (val) => {
        return (
          <Link to='#' onClick = {()=>{val.original.onClick(val.original.email)}} >
            Resend Invitation
          </Link>
        )
    }
  },

];
export { userColumns, inviteColumns,inactiveUserColumns };