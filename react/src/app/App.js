import React from 'react'
import { routes,sideMenu } from './Config'
import { Page, Fullscreen } from '../layout'
import { matchPath,Route, Switch, withRouter } from "react-router-dom";

//import { Modal,Card,Button } from 'react-bootstrap';


import { CSSTransition, TransitionGroup } from 'react-transition-group';

import '../styles/default.scss'
import '../styles/font-awesome.css'
import { connect } from 'react-redux';


//let dpWorldOrgs = [
//  'peacocks', 'hondauk',,
//]

class App extends React.Component {

  previousLocation = this.props.location;

  constructor(props) {
    super(props)

    console.log("APP");
    console.log(props.org);

    this.sideMenu = sideMenu(false);

    let split = routes.reduce((split,route) => {
      if (route.fullscreen) {
        split.fsRoutes.push(route);
      }
      else {
        split.pageRoutes.push(route);
      }

      return split;
    },{fsRoutes: [], pageRoutes: []});

    this.fsRoutes = split.fsRoutes;
    this.pageRoutes = split.pageRoutes;

  }
  
  componentWillUpdate(nextProps) {
    let { location } = this.props;

    // set previousLocation if props.location is not modal
    if (nextProps.history.action !== "POP" && (!location.state || !location.state.modal)) {
      this.previousLocation = this.props.location;
    }
  }

  render() {
    let { match,location } = this.props;

    // test if we have a modal route
    let modalMatch = null;
    modalMatch = this.fsRoutes.find((r) => {
      return !!matchPath(location.pathname, {path: match.url + r.path, exact: true});
    })

    // The key to trigger transitions is based on a match to a modal.  If the location 
    // is not full screen just use a standard key to keep the transition from happening. 
    let transKey = modalMatch ? location.key : "page";

    return (

      <TransitionGroup className="transition-group">

        <CSSTransition key={transKey} timeout={{ enter: 200, exit: 200 }} classNames={'fade'} >
          {(stage) => {
            //console.log("STAGE=%s,%s",stage,location.pathname);

            return (
              <section className="route-section">

                <Switch location={location}>
                  {
                    this.fsRoutes.map((route, index) => {

                      return (<Route key={index} path={match.url + route.path} exact={true}
                        render={() => this.renderFullscreen(route.component)} />)
                    })
                  }

                  <Route key={this.fsRoutes.length} path={match.url + '/'}
                    render={() => { return this.renderPage(modalMatch ? this.previousLocation : location) }} />

                </Switch>


              </section>)
          }}


        </CSSTransition>

      </TransitionGroup>


    )

  }

  renderFullscreen(Component) {
    let {match} = this.props;
    let props = this.props;

    return (
      <Fullscreen>
        <Component {...props} basePath={match.url} /> 
      </Fullscreen>
    );
  }

  renderPage(location) {
    let {match} = this.props;  

    //console.log('renderPage');
    //console.log(this.props);

    return (

      <Page sideMenu={this.sideMenu} match={match} currentPath={location.pathname}>

        <Switch location={location}>
        {/*console.log(this.props.location)*/}
        { 
          this.pageRoutes.map((route, index) => {
            let Component = route.component;

            return (
              // Render the routes for the main page
              <Route key={index} path={match.url + route.path} exact={route.exact} 
                     render={ (props) => { return <Component {...props} basePath={match.url} />} } />
            )
          })
        }
        </Switch>


      </Page>
    )
  }
}

function mapStateToProps(state) {
  return { user: state.auth.user }
}

export default withRouter(connect(mapStateToProps)(App));


