import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Card, Alert, Button } from 'react-bootstrap';
import ReactTable from "react-table";
import { withLoader } from '../components';
import { warehouseLocationColumns, factoryLocationColumns, locationSubComponent } from './columns/columns';
import { EditBuilding } from '../components/forms';
import PopUpWindow from '../components/PopUpWindow.js';

class DetailLocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      locationdata: [],
      columns: [],
      buildingData: undefined
    }
    this.type = {
      "warehouse": "warehouse",
      "factory": "factory",
      "port": "port",
      "ship":"ship"
    }
    this.createColumns = this.createColumns.bind(this);
    this.handleEditLocationClick = this.handleEditLocationClick.bind(this);
    this.closeWindow = this.closeWindow.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
  }

  async componentDidMount() {
    let responseData;
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');

    let searchShipment = this.props.match.params.containerId || "";
    let type = searchShipment.substring(0, searchShipment.indexOf("&"));

    console.log ("Type is");
    console.log(type);


  
    let containrtlist = null

    containrtlist = await this.props.client.getStuff(`select  ref from containers where parent_container_id = '${searchShipment}' or  parent_container_id = vesselnamebyid('${searchShipment}')`);

    console.log("Container List is");
    console.log(containrtlist);
    const containrtlistItems = containrtlist.map((cntr) =>
    <li>{cntr.ref}</li>
    );

    this.setState({
      containrtlistItems: containrtlistItems
    });

    let locationdata = null;
    if(type=='ship')
    locationdata = await this.props.client.getStuff(`select * from vehicle_locations where vessel_container_id = '${searchShipment}'`);
    else
    locationdata = await this.props.client.getStuff(`select * from buildings where container_id = '${searchShipment}'`);

    let buildingData;
    locationdata.forEach((e, index) => {
      e.id = index;
      e.code = e.ref;
      let add = {
        id : index,
        code : e.ref
      }
      buildingData = Object.assign({}, e, add)
    });
    
    this.setState({
      locationdata,
      buildingData,
    })
    let org_id = this.props.org.org_id;
    if (this.type[type] === "warehouse") {
      responseData = await this.props.client.queryShipmentID("p.sku = i.sku and i.container_id=$1", [searchShipment], type);
    } else if (this.type[type] === "factory") {
      responseData = await this.props.client.queryShipmentID("p.origin_container=$1 and (p.quantity - p.recv_quantity) > 0 and pro.sku = p.sku ", [searchShipment], type);
    } else if (this.type[type] === "port") {
      responseData = await this.props.client.queryShipmentID("c.destination_container_id = $1 and p.sku = i.sku and c.ata IS NOT NULL and i.shipment_id = c.shipment_id and i.org_id = $2 and c.org_id = $2", [searchShipment, org_id], type);
    }
    else if (this.type[type] === "ship") {
      responseData = await this.props.client.queryShipmentID("", [searchShipment, org_id], type);
    }

    this.createColumns(type, responseData);
    stopLoading();
  }

  createColumns(type, responseData) {
    if (this.type[type] === "port" || this.type[type] === "warehouse" || this.type[type] === "ship") {
      /* for (let row = 0; row < responseData.items.length; row++) {
        responseData.items[row].shipmentTraker = <Link to={`${this.props.basePath}/shipment-detail/${responseData.items[row].shipment_id}`}>{responseData.items[row].shipment_id}</Link>;
      } */
      this.setState({
        data: responseData.items,
        columns: warehouseLocationColumns(this.props.basePath)
      })
    }
    
    else if (this.type[type] === "factory") {
      this.setState({
        data: responseData.items,
        columns: factoryLocationColumns
      })
    }
  }

  handleEditLocationClick() {
    let editForm = <EditBuilding  item={this.state.buildingData} 
                                  onCancel={this.closeWindow} 
                                  onSubmit={this.handleEditSubmit} 
                                  data={this.state.locationdata}
                                  header={"Edit Location"} />;
    
    this.setState({
      editPage: editForm,
      showEdit: true,
    });
  }

  closeWindow() {
    this.setState({
      showEdit: false
    });
  }

  async handleEditSubmit(id, name, code, address, lat, lng, type) {
    const orgId = this.props.org.org_id;
    let list = this.state.locationdata;
    let edit;
    let previousContainerId;
    for (let obj of list) {
      if (obj.id === id) {
        previousContainerId = obj.container_id;
        edit = obj;
        obj.code = code;
        obj.ref = code;
        obj.name = name; 
        obj.address = address;
        obj.lat = lat;
        obj.lng = lng;
        obj.container_id = type.toLowerCase() + "&" + obj.code;
        obj.type = type;
        await this.props.client.containersClient.editBuilding(obj, previousContainerId, orgId);
      }
    }
    let buildingData;
    list.forEach((e, index) => {
      if(e.id === id) {
        let add = {
          id : index,
          code : e.ref
        }
        buildingData = Object.assign({}, e, add);
      }
    });

    this.setState({
      locationdata: list, 
      buildingData,
    });

  }

  render() {
    const { basePath } = this.props;
    const { data, locationdata, columns, buildingData, containrtlistItems} = this.state;
    let locationAddress, locationName, locationLat, locationLng, VesselName, nexport;
    let renderTable;

    let editLocationButton;
    if(buildingData !== undefined && buildingData.type !== "port") {
      editLocationButton  =  <Button variant="outline-secondary" onClick={ this.handleEditLocationClick }> Edit Location Details</Button>
    }

    for (let i in locationdata) {
      locationName = locationdata[i]["name"];
      locationAddress = locationdata[i]["address"];
      locationLng = locationdata[i]["lng"];
      locationLat = locationdata[i]["lat"];
      VesselName = locationdata[i]["vessel_name"];
      nexport = locationdata[i]["next_port"];
    }
    if (data.length > 0) {
      renderTable = <ReactTable data={data} columns={columns}
        style={{ textAlign: "center" }}
        pivotBy={["purchase_order_id"]}
        className="-striped -highlight"
        filterable
        defaultPageSize={10}
        SubComponent={row => {
          let newData = [{
            name: row.original.name,
            image: row.original.image,
            department: row.original.department
          }]
          for (let r = 0; r < newData.length; r++) {
            newData[r].imageLink = <Card>
              <img style={{ width: 150, height: 150 }} src={newData[r].image} alt="" />
            </Card>
          }
          return (
            <div style={{ padding: "20px" }}>
              <ReactTable
              className = "itemstable"
                data={newData}
                columns={locationSubComponent}
                defaultPageSize={1}
                showPagination={false}
              />
            </div>
          );
        }}
      />
    } else {
      renderTable =
        <Alert dismissible variant="info">
          <Alert.Heading>No Data Available </Alert.Heading>
          <p>
            There is no information about inventory items at this time. Please check back later.
          </p>
        </Alert>
    }
    return (
      <>
      <PopUpWindow shown={this.state.showEdit} page={this.state.editPage} />
  

  
        <Card>
        <div className="row">
<div className="col-lg-4 Lheader1">

          <Card.Header> Location Detail </Card.Header>
          <Card.Body>
     
            { locationName!=null &&
            <> Location Name: {locationName} <br /></>
            }

            { locationAddress!=null &&
            <> Location Address : {locationAddress} <br /></>
            }

            { VesselName!=null &&
            <>Vessel Name: {VesselName} <br /></>
            }

            { nexport!=null &&
            <>Next Port : {nexport} <br /></>
            }
           
            
            location Longitude : {locationLng} <br />
            location Latitude : {locationLat} <br />

<br/><br/>
  
            {editLocationButton}

         
          </Card.Body>

</div>
<div className="col-lg-8 Lheader2">
<Card.Header> Container Numbers </Card.Header>
<Card.Body>
     
     <ul className = "locationList">{containrtlistItems}</ul>
  
   </Card.Body>
</div>
</div>

          
 
        </Card>
     
  




        {renderTable}

    </>
    )
  }
}
function mapStateToProps(state) {
  return {
    client: state.auth.client,
    org: state.auth.org,
  }
}
export default withRouter(withLoader(connect(mapStateToProps)(DetailLocation)));
