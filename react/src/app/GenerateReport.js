//Class that acts as the Reports page of the webapp
import React from 'react'
import { Card, ButtonToolbar, Button, Row, Col, Form } from 'react-bootstrap'
import Table from '../components/Table'
import { Subheader } from '../layout'
import TwoLevelPieChart from '../components/graphs/TwoLevelPie'
import GraphOptionsBar from '../components/graphs/GraphOptionsBar';
import RadarChart from '../components/graphs/RadarChart';
import RadialBarGraph from '../components/graphs/RadialBarGraph';
import { productionColumns, arrivalColumns, shipColumns } from './columns/reportColumns.js'
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { withLoader } from '../components';

function Spinner(props) {
  return props.display ? <div className="loader" ></div> : '';
}

class GenerateReport extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      data1: [],
      data2: [],
      data3: [],
      columns: [],
      selectFields: [],
      graphType: 'pie',
      graphGroupBy: 'org_country',
      graphButtons: [{ onClick: () => { this.groupBySelect('org_country') }, text: "Group by Origin Country" }, { onClick: () => { this.groupBySelect('port_of_loading') }, text: "Group by Port of loading" }, { onClick: () => { this.groupBySelect('mcc') }, text: "Group By Destination" }],
      container: '',
      spinnerOn: false
    }
    this.onClicky = this.onClicky.bind(this);
    this.onChange = this.onChange.bind(this);
    this.groupBySelect = this.groupBySelect.bind(this);
    this.generateTables = this.generateTables.bind(this);
    this.renderGraphs = this.renderGraphs.bind(this);
  }

  async componentDidMount() {
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading your data...');
    let destContainers = await this.props.client.getDestinationContainers(this.props.user.org_id);
    console.log("REPORT");
    console.log(destContainers)

    if(destContainers.length){
      this.generateTables(destContainers[0].destination_container_id);
    }
    let selectors = [];
    for (let container of destContainers) {
      selectors.push(<option key={container.destination_container_id} value={container.destination_container_id}>{container.name}</option>)
    }

    this.setState({
      selectFields: selectors,
      container: selectors[0]
    });

    stopLoading();
  }

  generateTables(container) {

    let t1 = [];
    let t2 = [];
    let t3 = [];

    this.props.client.queryReport(container)
      .then(async res => {
        t1 = res[0];
        t2 = res[1];
        t3 = res[2];

        for (let obj of t2) {
          if (obj.department === null) {
            obj.department = "KIDS-GM";
          }
          obj.eta = obj.eta.toString().substring(0, 15);
          obj.etd = obj.etd.toString().substring(0, 15);
        }

        for (let obj of t3) {
          let date = this.getEstimatedDate(obj.approx_recv_date, 7);
          obj.approx_recv_date = date.toString().substring(0, 15);
        }
        // I am changing this to maintain the current clicked Tab on location change
        //This seems like the expected behavior
        let current;
        let currentColumns = this.state.columns;
        if (this.state.columns === productionColumns) {
          current = t1
        }
        if (this.state.columns === arrivalColumns) {
          current = t2
        }
        if (this.state.columns === shipColumns) {
          current = t3
        }
        this.setState({
          data: current ? current : t1,
          data1: t1,
          data2: t2,
          data3: t3,
          columns: currentColumns.length ? currentColumns : productionColumns,
          spinnerOn: false
        });

      }).catch(err => console.log(err));
    
  }

  getEstimatedDate(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  getdisplayTitle(value) {
    for (let i in this.state.columns) {
      if (this.state.columns[i].accessor === value) {
        return this.state.columns[i].Header
      }
    }
  }

  renderGraphs() {
    let groupBy = this.state.graphGroupBy;
    let title = this.getdisplayTitle(groupBy)
    if (this.state.data) {
      let grouped = [];
      let aggregateDict = {};
      for (let i in this.state.data) {
        if (aggregateDict[this.state.data[i][groupBy]]) {
          aggregateDict[this.state.data[i][groupBy]] += parseInt(this.state.data[i].sum)
        }
        else {
          aggregateDict[this.state.data[i][groupBy]] = parseInt(this.state.data[i].sum)
        }
      }
      for (let key in aggregateDict) {
        grouped.push({ name: key, value: aggregateDict[key] })
      }
      if (this.state.graphType === 'radialBar') {
        return (
          <div>
            <h1 style={{ paddingLeft: '10%' }}> {"Inventory by " + title}</h1>
            <RadialBarGraph data={grouped} />
          </div>
        )
      }
      if (this.state.graphType === 'pie') {
        return (
          <div>
            <h1 style={{ paddingLeft: '10%' }}> {"Inventory by " + title}</h1>
            <TwoLevelPieChart data={grouped} />
          </div>
        )
      }
      if (this.state.graphType === 'radar') {
        return (
          <div>
            <h1 style={{ paddingLeft: '10%' }}> {"Inventory by " + title}</h1>
            <RadarChart data={grouped} />
          </div>
        )
      }
    }
  }
  onClicky(button) {
    if (button === "production") {
      this.setState({
        data: this.state.data1,
        columns: productionColumns,
        graphGroupBy: 'org_country',
        graphButtons: [{ onClick: () => { this.groupBySelect('org_country') }, text: "Group by Origin Country" }, { onClick: () => { this.groupBySelect('port_of_loading') }, text: "Group by Port of loading" }, { onClick: () => { this.groupBySelect('mcc') }, text: "Group By Destination" }]
      });
    } else if (button === "arrival") {
      let graphType = this.state.graphType;
      if (this.state.graphType === 'radialBar') {
        graphType = 'radar'
      }
      this.setState({
        data: this.state.data2,
        columns: arrivalColumns,
        graphType: graphType,
        graphGroupBy: 'etd',
        graphButtons: [{ onClick: () => { this.groupBySelect('etd') }, text: "Group by Vessel Departure" }, { onClick: () => { this.groupBySelect('eta') }, text: "Group by Vessel Arrival" }, { onClick: () => { this.groupBySelect('department') }, text: "Group By Department" }]
      });
    } else {
      let graphType = this.state.graphType;
      if (this.state.graphType === 'radialBar') {
        graphType = 'radar'
      }
      this.setState({
        data: this.state.data3,
        columns: shipColumns,
        graphGroupBy: 'approx_recv_date',
        graphType: graphType,
        graphButtons: [{ onClick: () => { this.groupBySelect('approx_recv_date') }, text: "Group by Ship By Date" }, { onClick: () => { this.groupBySelect('department') }, text: "Group by Department" }]
      })
    }
  }

  groupBySelect(groupBy) {
    this.setState({
      graphGroupBy: groupBy
    })
  }

  onChange = (event) => {
    const { value } = event.target;

    this.setState({
      container: value,
      spinnerOn: true
    }, () => this.generateTables(value));

  }

  setGraphType(value) {
    this.setState({
      graphType: value
    })
  }
  render() {
    const { basePath } = this.props;
    const { data, spinnerOn } = this.state;
    let columns = this.state.columns;

    return (
      <>
        <Subheader title="Report Generator" />

        <Card>
        <Form.Label><font size="3">Port of Destination</font></Form.Label>
          <Form.Control as="select"
                                onChange={(e) => {this.onChange(e)}} >{this.state.selectFields}</Form.Control>
          <Spinner display={spinnerOn}/>
        </Card>
        <br/>
        <Card>
          <Card.Header>
          <ButtonToolbar>
            <Button onClick={() => this.onClicky("production")}>Production Report</Button>&nbsp;
            <Button onClick={() => this.onClicky("arrival")}>Arrival Prediction</Button>&nbsp;
            <Button onClick={() => this.onClicky("ship")}>Ready to Ship By Dates</Button>&nbsp;
            <Link to={basePath + "/milestones"}><Button variant="outline-primary">Milestone Reports</Button></Link>
          </ButtonToolbar>     
          </Card.Header>
              <Table
                data={data}
                columns={columns}
                filterable getTrProps={this.onRowClick}
                onFilteredChange={this.handleFilterChange}
                defaultPageSize={15}
                className="-striped -highlight" />
            </Card>
            <br/>

            <Card>
              <Card.Header>
                <ButtonToolbar>
                  <Button onClick={() => this.setGraphType("pie")} buttonText="Pie Chart">Pie Chart</Button>&nbsp;
                  <Button onClick={() => this.setGraphType("radar")} buttonText="Radar Map">Radar Map</Button>&nbsp;
                  {this.state.columns === productionColumns &&
                    <Button onClick={() => this.setGraphType("radialBar")} buttonText="Bar Graph">Bar Graph</Button>
                  }
                </ButtonToolbar>
              </Card.Header>
                {this.renderGraphs()}
              <ButtonToolbar className="mb-5">
                <GraphOptionsBar data={this.state.graphButtons} />
              </ButtonToolbar>
            </Card>
      </>
    )
  }
}

function mapStateToProps(state) {
	return { client: state.auth.client, user: state.auth.user }
}
export default withLoader(connect(mapStateToProps)(GenerateReport));
