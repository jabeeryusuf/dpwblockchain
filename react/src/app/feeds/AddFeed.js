import React from 'react';
import { connect } from 'react-redux';
import { dataPullActions, alertActions } from '../../redux/actions';
import { serviceColumns } from '../columns/columns';
import ReactTable from 'react-table';
import { Card, Container, Form, FormLabel, FormControl, FormGroup, Col, Row, Button, Alert } from 'react-bootstrap';

class AddDataPull extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      options: [<option key={"MAERSK"} value={"MAERSK"}>MAERSK</option>, <option key={"US Customs"} value={"US Customs"}>US Customs</option>],
      username: '',
      password: '',
      confirmPass: '',
      service: "MAERSK",
      submitted: false,
      hasExistingServices: false,
      servicesList: []
    };
    
  }

  async componentDidMount() {
    this.updateTable();
  }

  async updateTable() {
    this.setState({
      servicesList: []
    });

    const { user } = this.props;

    let org = user.blockchain_id;
    let whereClause = `org='` + org + `'`;
    let existingServices = await this.props.client.queryExistingServices(whereClause);
    
    if (existingServices.length > 0) {
      this.setState({
        hasExistingServices: true,
        servicesList: existingServices
      });
    }
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  handleServiceChange = (event) => {
    const { value } = event.target;
    this.setState({
      service: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });

    const { service, username, password, confirmPass} = this.state;
    const { user, dispatch } = this.props;
    dispatch(alertActions.clear());  

    if (service && username && password && confirmPass && (password === confirmPass)) {
      let pullInterval = "1 hour";
      let org = user.blockchain_id;
      dispatch(dataPullActions.submitPull(org, service, username, password, pullInterval));
      setTimeout(() => {this.updateTable()}, 500);
    }
  }

  render() {

    const { options, username, password, confirmPass, submitted, hasExistingServices, servicesList } = this.state;
    const { submittingPull, alert } = this.props;

    let usernameInvalid = submitted && !username;
    let passwordInvalid = submitted && !password;
    let confirmPassInvalid = (submitted && !confirmPass) || (submitted && (password !== confirmPass));
    let usernameInvalidMsg = "Field cannot be empty"; 
    let passwordInvalidMsg = "Field cannot be empty";
    let confirmPassInvalidMsg = "Passwords must be matching";

    return (
      <div>
        <br/>
        <Container>
          <Card>
            <Col sm={{ span: 10 , offset: 1}}>
              {hasExistingServices ? 
                <div>
                  <h2>Current Services</h2>
                  <ReactTable columns={[{ Header: "Data sources DPWChain is already pulling from", columns: serviceColumns}]}
                              data={servicesList} 
                              defaultPageSize={3}
                              className="-striped -highlight" />
                </div> : 
                <h2>No current serivces</h2>
              }
              <br/>
            </Col>
            <hr />
            <br/>
            <h2>Add a data source for DPWChain to pull from</h2>
            <br/>
            <p>Choose a service and submit your credentials to allow DPWChain to begin pulling <b>your data</b> for you</p>
            <Col sm={{ span: 6, offset: 3 }}>
              {alert.message &&
                <Alert dismissible variant={alert.type} >{alert.message}</Alert>        
              }
              <br/>
            </Col>
            <Col sm={{ span: 6, offset: 3 }}>
              <Form>
                <FormGroup>
                  <FormLabel for="service">Service</FormLabel>
                  <FormControl as="select"
                               onChange={(e) => {this.handleServiceChange(e)}} >{options}</FormControl>
                </FormGroup>
                <FormGroup>
                  <FormLabel for="username">Username</FormLabel>
                  <FormControl  isInvalid={usernameInvalid}
                                type="text"
                                name="username"
                                id="username"
                                placeholder="Enter your username"
                                value={username}  
                                onChange={(e) => this.handleChange(e)} />
                    {usernameInvalid ? <Form.Control.Feedback type="invalid">{usernameInvalidMsg}</Form.Control.Feedback> : ''}
                </FormGroup>
                <FormGroup>
                  <FormLabel for="password">Password</FormLabel>
                  <FormControl  isInvalid={passwordInvalid}
                                type="password"
                                name="password"
                                id="password"
                                placeholder="Enter your password"
                                value={password}
                                onChange={(e) => this.handleChange(e)} />
                    {passwordInvalid ? <Form.Control.Feedback type="invalid">{passwordInvalidMsg}</Form.Control.Feedback> : ''}
                </FormGroup>
                <FormGroup>
                  <FormLabel for="confirmPass">Confirm Password</FormLabel>
                  <FormControl  isInvalid={confirmPassInvalid}
                                type="password"
                                name="confirmPass"
                                id="confirmPass"
                                placeholder="Confirm your password"
                                value={confirmPass}
                                onChange={(e) => this.handleChange(e)} />
                    {confirmPassInvalid ? <Form.Control.Feedback type="invalid">{confirmPassInvalidMsg}</Form.Control.Feedback> : ''}
                </FormGroup>
                <br/>
                <Row>
                  <Col sm={{ span: 12 }}>
                    <FormGroup check>
                      <Button block color="primary" disabled={submittingPull} onClick={(e) => this.handleSubmit(e)}>
                      {submittingPull ? "Loading..." : "Submit"}</Button>
                    </FormGroup>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Card>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { submittingPull, submittedPull } = state.dataPull;
  const alert = state.alert;
  return { 
    client: state.auth.client, 
    user: state.auth.user,
    submittingPull,
    submittedPull, 
    alert
  };
}

export default connect(mapStateToProps)(AddDataPull);