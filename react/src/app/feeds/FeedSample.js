import React from "react";
import { FaFileDownload } from "react-icons/fa";
const typeToSampleDict = {
  "Shipment": "f6f64c5d-2c2f-4486-9c5c-abfbbdafcef2"
}
const FeedSample = (props) => {
  return (
    <>
    <FaFileDownload />
    <span className="linklook" onClick={() => props.onClick(typeToSampleDict[props.type])}>
      Sample Upload Sheet
    </span>
    </>
  )
};
export default FeedSample;