import React from 'react';
import { Link } from 'react-router-dom';

import { Nav } from 'react-bootstrap';
import { Subheader } from '../../layout';
import { withRouter } from 'react-router-dom'


import ListFeeds from './ListFeeds'
import FeedsActivity from './FeedsActivity'

const tabMap = {
  'list'      : { title: 'My Feeds', component: ListFeeds},
  'activity'  : { title: 'Recent Activity', component: FeedsActivity },
  'settings'  : { title: 'Settings', component: FeedsActivity }
}


class FeedsLanding extends React.Component {

  render() {
    let {match} = this.props;
    let tab = this.props.match.params.tab;

    console.log("TAB=%s",tab);
    console.log(match)

    let Component = tabMap[tab].component;
    let { basePath } = this.props
    return (
      <>
        <Subheader title={tabMap[tab].title}/>
        <Nav variant="tabs" activeKey={tab} onSelect={(k) => {}} >
          {
            Object.keys(tabMap).map((k,i) => (
              <Nav.Item key={i}>
                <Nav.Link as={Link} to={k} href={k} >{tabMap[k].title}</Nav.Link>
              </Nav.Item>
            ))
          }
        </Nav>
        { <Component basePath={basePath}/> }
      </>
    )
  }




}
export default withRouter(FeedsLanding);