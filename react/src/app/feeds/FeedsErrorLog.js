import React from 'react';
import { connect } from 'react-redux';
import { ListGroup } from 'react-bootstrap';
import { FaFileDownload } from "react-icons/fa";

class FeedsErrorLog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feedsErrorLog: []
    }
    this.downloadFile = this.downloadFile.bind(this);
  }

  componentDidMount() {
    this.getAllMyErrorLog();
  }

  async getAllMyErrorLog() {
    console.log(this.props.location.state)
    if (this.props.location.state !== undefined) {
      let response = await this.props.client.getStuff(`Select * from feeds_error_log where feeds_activity_id = '${this.props.location.state.feeds_activity_id}' `);
      let fileIdResponse = await this.props.client.getStuff(`Select error_file_id from feeds_activity where feeds_activity_id = '${this.props.location.state.feeds_activity_id}' `);
      let fileId ;
      if(fileIdResponse !== undefined || fileIdResponse !== null) {
        fileId = fileIdResponse[0].error_file_id;
      }
      this.setState({
        feedsErrorLog: response,
        fileId
      })
    }
  }

  async downloadFile(){
    console.log("PROPS");
    console.log(this.props);
    console.log(this.state.fileId)
    const fileLink = await this.props.client.queryFiles(" file_id = $1", [this.state.fileId]);
    window.open(fileLink);
  }


  render() {
    let { feedsErrorLog } = this.state;
    let logs = feedsErrorLog.map( (e, index) => {
      return (
        <ListGroup.Item action key={index} > { e.feeds_error } </ListGroup.Item>
      )
    })
    return (
      <>
        <h1> Error log </h1>
        <FaFileDownload />
        <span className="linklook" onClick={this.downloadFile}>
          Download Excel with Error showing
        </span>
        <ListGroup>
          {logs}
        </ListGroup>
      </>
    )
  }

}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org,
  }
}

export default connect(mapStateToProps)(FeedsErrorLog);