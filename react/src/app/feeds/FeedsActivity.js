import React from 'react';
import { connect } from 'react-redux';
import { Table, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class FeedsActivity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feedsActivity: [],
    }
  }

  componentDidMount() {
    this.getAllMyCases();
  }

  async getAllMyCases() {
    let org_id = this.props.org.org_id;
    let feedsActivity = await this.props.client.getStuff(` SELECT DISTINCT feeds_activity.feeds_activity_id, feeds_activity.create_time, feeds_activity.file_name, feeds_activity.feeds_status, feeds_activity.error_file_id,
                                                              (SELECT COUNT(*) as error_count FROM feeds_error_log
                                                              WHERE feeds_error_log.feeds_activity_id = feeds_activity.feeds_activity_id) AS error_count
                                                          FROM feeds_activity where org_id = '${org_id}' order by create_time DESC`);
    this.setState({
      feedsActivity: feedsActivity
    })
  }

  render() {
    let { feedsActivity } = this.state;
    let { basePath } = this.props;
    const tableRows = feedsActivity.map(e => {
      let status = e.feeds_status;
      if (e.feeds_status === 'Done' && e.error_count > 0) {
        status = " Not uploaded correctly, see more details";
      }

      return (
        <tr key={e.feeds_activity_id}>
          <td> {e.file_name} </td>
          <td> {e.create_time.substring(0, 10)} </td>
          <td> {status} </td>
          <td>
            {e.error_count > 0 &&
              <ButtonGroup>
                <Link to={{ pathname: basePath + "/feeds/error-log", state: { feeds_activity_id: e.feeds_activity_id } }}>
                  Details
              </Link>
              </ButtonGroup>
            }
          </td>
        </tr>
      )
    })
    return (
      <>
        <Table>
          <thead>
            <tr>
              <td> File Name </td>
              <td> Uploaded On </td>
              <td> Upload Status </td>
              <td> Details </td>
            </tr>
          </thead>
          <tbody>
            {tableRows}
          </tbody>
        </Table>
      </>
    )
  }

}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user,
    org: state.auth.org,
  }
}

export default connect(mapStateToProps)(FeedsActivity);