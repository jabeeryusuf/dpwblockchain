import React from 'react';
import { Row, Col, ButtonToolbar, ButtonGroup, Dropdown, DropdownButton, Fade, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withLoader } from '../../components';
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa";
import FilePondComponent from "../Cases/FilePondComponent";
import FeedSample from "./FeedSample"
import { feedstatusColumns } from "../columns/columns";
import ReactTable from "react-table";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'; 

import { Alert, Card, Form, FormGroup, InputGroup,Button  } from 'react-bootstrap';

const Greet = ({ tcount, tscount,tecount}) => <div> Values <p>Total Containers Processed {tcount} </p> <p>Total Containers Success {tscount} </p> <p>Total Containers Failure {tecount} </p></div>

const TableRow = (props) => {
  return (
    <tr>
      <td colSpan="1">{props.title}</td>
      <td colSpan="1">
        {props.condition ? <FaCheckCircle color={"green"} /> : <FaTimesCircle color={"red"} />}
      </td>
      <td colSpan="1">
        <font>{props.message}</font>
      </td>
    </tr>
  )
}

class ListFeeds extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'Shipment',
      uploadFile: null,
      success: undefined,
      error: undefined,
      errorUpload: undefined,
      selected: true,
      uploadMessage: undefined,
      uploadSuccess: false,
      validateSuccess: false,
      validatorMessage: undefined,
      processSuccess: false,
      processMessage: undefined,
      insertSuccess: false,
      insertMessage: undefined,
      loading:false,
      statusdata:[]
    };
    this.handleValidate = this.handleValidate.bind(this);
    this.downloadExample  = this.downloadExample.bind(this)
  }

  async downloadExample(fileId){
    console.log("PROPS");
    console.log(this.props);
    const fileLink = await this.props.client.queryFiles(" file_id = $1", [fileId]);
    window.open(fileLink);
  }


  async btnFeedCreate(carrier,cnumbers){
    console.log("Form Submitted");
    
    this.setState({
      loading: true
    });
    console.log(carrier);

    

    
    var cnumberslist = cnumbers.split("\n");
    var shipmentslist = [];

    var orgggid = await this.props.user.org_id;
    console.log(orgggid);
    var i=0;
    for(i=0;i<cnumberslist.length;i++)
    {
      if(cnumberslist[i]!="" && cnumberslist[i]!=" " && cnumberslist[i]!="\n" && cnumberslist[i]!="\t" && cnumberslist[i].length>5)
      {
      var newshipment = {orgId:orgggid, shipmentRef:cnumberslist[i],status:"in-transit", carrier:carrier};
      
      shipmentslist.push(newshipment);
      }
    }
    var newfeedstatus = await this.props.client.shipmentsClient.newshipment(shipmentslist);

    console.log(newfeedstatus.results);

  //   const options = {
  //     onOpen: props => console.log("Opening"),
  //     onClose: props => console.log("Closing"),
  //     autoClose: 6000,
  //     type: toast.TYPE.INFO,
  //     hideProgressBar: false
  // };

  //   toast(<div>njhfhjhf</div>,options);
    this.newfeed_cnumbers.current.value = "";
    this.setState({
      statusdata: newfeedstatus.results,
      loading:false
    });

  

  }


  onFilterChange(filter, key) {
    this.setState({
      [filter]: key.toLowerCase(),
      selected: true,
    }, () => console.log("state", this.state));
  }

  onFilePondUpdate = (files) => {
    this.setState({
      uploadFile: files,
      uploadSuccess: true,
      uploadMessage: "File upload complete"
    }, () => this.handleValidate());
  }

  async showPopUpTracking() {
    if (this.state.showPopUpTracking == false) {
      const carriers = await this.props.client.carriersClient.list();
      this.setState({
        carriers,
        newTrackingCarrier: carriers[0].carrierId,
        showPopUpTracking: true,
      });
    } else {
      this.setState({
        showPopUpTracking: false,
      });
    }
  }

  async handleValidate() {
    let uploadExcelObj = {}
    uploadExcelObj.type = this.state.type;
    uploadExcelObj.org_id = this.props.user.org_id;
    uploadExcelObj.fileId = this.state.uploadFile[0];
    uploadExcelObj.user_id = this.props.user.user_id;

    // adding status to the feeds_acivity table 
    await this.props.client.addFeedsActivity("uploaded", this.state.uploadFile[0], this.props.user.org_id);
    console.log("handle submit ", uploadExcelObj);
    
    let excelValidate = await this.props.client.ValidateFeedsFile(uploadExcelObj);
    console.log('xl validate',excelValidate)
    if (excelValidate.errorMessage === undefined) {
      this.setState({
        validatorMessage: excelValidate.successMessage,
        validateSuccess: true
      });

      let insertResult;
      console.log('insert res!')
      insertResult = await this.props.client.AddFeeds(uploadExcelObj);
      console.log("bottom",insertResult)
      if (insertResult.successMessage !== undefined) {
        this.setState({
          processSuccess: true,
          processMessage: "Successfully processed",
          insertSuccess: true,
          insertMessage: insertResult.successMessage,
        });
      } else {
        this.setState({
          insertMessage: insertResult.errorMessage
        });
      }

    } else {
      this.setState({
        validatorMessage: excelValidate.errorMessage
      });
    }
  }

  render() {
    const { type, validatorMessage, processMessage, uploadMessage, insertMessage } = this.state
    console.log(this.state)
    this.newfeed_caarrier = React.createRef();
    this.newfeed_cnumbers = React.createRef();
    return (
      <>
        <Row className="mb-2">
          <ButtonToolbar className="toolbar">
            <ButtonGroup className="filter">
              <DropdownButton title={type === 'Select' ? 'Select file type to upload' : type} onSelect={(k) => this.onFilterChange('type', k)} >
                <Dropdown.Item eventKey={"Product"} key={"Product"}>{"Product"}</Dropdown.Item>
                <Dropdown.Item eventKey={"Shipment"} key={"Shipment"}>{"Shipment"}</Dropdown.Item>
                <Dropdown.Item eventKey={"Building"} key={"Building"}>{"Building"}</Dropdown.Item>
              </DropdownButton>
            </ButtonGroup>
          </ButtonToolbar>

        </Row>
        <Fade in={this.state.selected}>
          <Row>
            <Col sm={{ span: 4 }}>
            {/* <FeedSample type="Shipment" onClick={this.downloadExample}/> */}

            <Form>
              <Form.Group controlId="carrier">
                <Form.Label>Select Carrier</Form.Label>
                <Form.Control as='select' name="ncarrier" ref={this.newfeed_caarrier} >
                <option value="">Auto Detect</option>
                <option value="MAERSK LINE A/S">MAERSK LINE A/S</option>
                <option value="OCEAN NETWORK EXPRESS PTE. LTD.">OCEAN NETWORK EXPRESS PTE. LTD.</option>
                <option value="COSCO SHIPPING DEVELOPMENT">COSCO SHIPPING DEVELOPMENT</option>
                <option value="CMA-CGM">CMA-CGM</option>
                <option value="MSC- MEDITERRANEAN SHIPPING COMPANY S.A.">MSC- MEDITERRANEAN SHIPPING COMPANY S.A.</option>
                <option value="ORIENT OVERSEAS CONTAINER LINE LTD.">ORIENT OVERSEAS CONTAINER LINE LTD.</option>
                </Form.Control>
                <Form.Text className="text-muted"> </Form.Text>
              </Form.Group>

              <Form.Group controlId="containernums">
                <Form.Label>Container Numbers</Form.Label>
                <Form.Control as="textarea" rows="11"ref={this.newfeed_cnumbers} placeholder="Enter container numbers one in a line" />
              </Form.Group>
              
              

              { !(this.state.loading) &&
              <Form.Group controlId="submitbutton">
              <Button variant="primary" onClick={()=>this.btnFeedCreate(this.newfeed_caarrier.current.value,this.newfeed_cnumbers.current.value)}>
                Submit
              </Button>
              </Form.Group>
              }
            </Form>


              {/* <FilePondComponent apiUrl={this.props.client.apiUrl} handleFilePondUpdate={this.onFilePondUpdate} maxUploads={1}
                user={this.props.user.user_id} fileTypes={["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", ".xlsx"]} /> */}
              
            </Col>
          </Row>
        </Fade>
        {
        <ReactTable
                    data={this.state.statusdata}
                    columns={feedstatusColumns}
                    defaultPageSize={1}
                    loading={this.state.loading}
                    manual
                    className="-striped -highlight"
                />
        }
        {/* <Table>
          <thead>
            <tr>
              <th>
                Protcol
              </th>
              <th>
                Status
              </th>
              <th>
                Message
              </th>
            </tr>
          </thead>
          <tbody>
            <TableRow title={"Uploaded"} condition={this.state.uploadSuccess} message={uploadMessage && uploadMessage} />
            <TableRow title={"Validated"} condition={this.state.validateSuccess} message={validatorMessage && validatorMessage} />
            <TableRow title={"Processed"} condition={this.state.processSuccess} message={processMessage && processMessage} />
            <TableRow title={"Done"} condition={this.state.insertSuccess} message={insertMessage && insertMessage} />
          </tbody>
        </Table> */}

      </>
    )
  }

}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    user: state.auth.user
  }
}

export default withLoader(connect(mapStateToProps)(ListFeeds));