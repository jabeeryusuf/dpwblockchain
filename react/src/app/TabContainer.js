import React from 'react';
import { Nav } from 'react-bootstrap';
class TabContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shown: 0
    }
  }
  viewSwitch(tab) {
    this.setState({
      shown: tab
    })
  }
  render() {
    return (
      <>
        <Subheader title="Milestone Tracker"/>
        <Nav variant="tabs" defaultActiveKey={this.props.children[0].key} className="mb-3">
          {this.props.children.map((val, key) => {
            return (
              <Nav.Item>
                <Nav.Link onSelect={() => this.viewSwitch(key)} eventKey={val.key} > {val.key} </Nav.Link>
              </Nav.Item>
            )
          })
          }
        </Nav>
        {this.props.children.map((val, key) => {
          if (key == this.state.shown) {
            return (
              React.cloneElement(val, { hide: false })
            )
          }
          else {
            return (
              React.cloneElement(val, { hide: true })
            )
          }
        })
        }
      </>
    )
  }
}
export default TabContainer