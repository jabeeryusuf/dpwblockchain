import React from 'react'
import { Form, Button, Card, InputGroup, Col, Row, Container } from 'react-bootstrap';

const UpdateCarrierForm = (props) => {
  return(
    <>
    <Form.Label> Carrier : </Form.Label> <Form.Control as='select' onChange={(e) => { props.handleChange(e) }} name='newTrackingCarrier' value={props.formValues['Carrier']} >
      
             
              
                <option value="MAERSK LINE A/S">MAERSK LINE A/S</option>
                <option value="OCEAN NETWORK EXPRESS PTE. LTD.">OCEAN NETWORK EXPRESS PTE. LTD.</option>
                <option value="COSCO SHIPPING DEVELOPMENT">COSCO SHIPPING DEVELOPMENT</option>
                <option value="CMA-CGM">CMA-CGM</option>
                <option value="MSC- MEDITERRANEAN SHIPPING COMPANY S.A.">MSC- MEDITERRANEAN SHIPPING COMPANY S.A.</option>
                <option value="ORIENT OVERSEAS CONTAINER LINE LTD.">ORIENT OVERSEAS CONTAINER LINE LTD.</option>
                <option value="HAPAG LLOYD A.G">HAPAG LLOYD A.G</option>
                
      {/* {
        props.carriers.map((val, idx) => {
        return (
          <option key={idx} value={val.carrierId}>{val.name}</option>
        )
      })
      } */}
    </Form.Control>
  
    <Button variant="outline-primary" onClick={props.submit}>Update </Button>
      </>
  )
}

export default UpdateCarrierForm; 