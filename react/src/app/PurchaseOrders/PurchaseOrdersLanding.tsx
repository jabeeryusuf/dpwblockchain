import React from "react";

import { POExplorer } from "./POExplorer";
import { POExplorerPOAgg } from "./POExplorerPOAgg"
import { Landing, ILandingTab } from "../../layout/Landing";

const tabs: ILandingTab[] = [
  { label: "PO Products",  path: "/items", component: POExplorer, props: {aggregate: "item"} },
  { label: "All POs",  path: "/all", component: POExplorerPOAgg, props: {aggregate: "po"} } ,
];

export const PurchaseOrdersLanding = (props: any) => <Landing {...props} tabs={tabs} />;
