import React from "react";
import {
    Button, ButtonGroup, ButtonToolbar, Card, Dropdown,
    DropdownButton as _DropdownButton, Row
} from "react-bootstrap";
import { connect as _connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import ReactTable from "react-table";
import { withLoader } from "../../components";
import { FormSearch } from "../../components/forms";
import { locationSubComponent } from "../columns/columns";
import { aggregatedPOColumnsnew, allPOColumnsNew } from "../columns/POColumns";
import { user } from "../../redux/reducers/userReducer";


const jswithLoader: any = withLoader;

const { CSVLink } = require("react-csv");
const DropdownButton: any = _DropdownButton;
const connect: any = _connect;

const filterOptions = [
    { label: "All POs", value: "all" },
    { label: "Open POs", value: "open" },
    { label: "In-Transit", value: "in-transit" },
    { label: "Delayed", value: "delayed" },
    { label: "Completed", value: "completed"}
]

const createDdi =
    (opts: any) => opts.map((o: any, i: any) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

const getLabel = (opts: any, value: any) => {
    const l = opts.find((o: any) => (o.value === value));
    return l ? l.label : "";
};


interface IProps extends RouteComponentProps<IMatchParams> {
    searchText: string;
    aggregate: any;
    user: any;
  }

interface IMatchParams {
    statusfilter:string;
  }

interface IPOstate {
    poData: any[];
    tableData: any[];
    desc: boolean;
    orderBy: string;
    filterOptions: any[];
    searchValue: string;
    filterValue: string;
    loading: boolean;
    cursorList: string[];
    page: number;
    pageSize: number;
    pages: number;
}

const newPagination = () => ({ page: 0, cursorList: [""] });

const csvHeaders = ((apocs: any[]) => {
    const headers = [];
    for (const ap of apocs) {
        if (ap.accessor !== "image") {
            const newObj = {
                key: ap.accessor,
                label: ap.Header,
            }
            headers.push(newObj)
        }
    }
    return headers;
})(allPOColumnsNew);

class _POExplorer extends React.Component<any, IPOstate> {

    private searchTimeout: any;

    constructor(props: any) {
        super(props);
        this.state = {
            desc: true,
            orderBy: "purchaseOrderId",

            poData: [],
            tableData: [],

            filterOptions: [{ label: "All POs", value: "all" }],
            filterValue: this.props.match.params.statusfilter ? this.props.match.params.statusfilter : "all",
            searchValue: "",

            pages: 0,
            pageSize: 20,
            loading: false,

            ...newPagination(),
        };
        this.searchTimeout = null;
    }

    public componentDidMount = async () => {

        const statusfilterval = this.props.match.params.statusfilter ? this.props.match.params.statusfilter : "";
        console.log("Purchase Order Filter  is "+statusfilterval);

        this.setState({ loading: true });
        await this.loadData();
    }

    public componentDidUpdate = async (prevProps: any, prevState: any) => {
        if (this.state.pageSize !== prevState.pageSize ||
            this.state.page !== prevState.page ||
            this.state.filterValue !== prevState.filterValue ||
            this.state.desc !== prevState.desc ||
            this.state.orderBy !== prevState.orderBy) {
            await this.loadData();
        } else if (this.state.searchValue !== prevState.searchValue) {
            await this.updateDataTimed();
        }

    }

    public render() {
        return (
            <>
                {this.renderFilterBar()}
                {this.state.tableData && this.thisRenderTable()}

            </>
        );
    }

    private renderFilterBar() {
        const { filterValue } = this.state;


        return (
            <Row>
                <ButtonToolbar className="toolbar">
                    <ButtonGroup className="filter" >

                        <DropdownButton title={getLabel(filterOptions, filterValue)} onSelect={this.handleFilterChange}>
                            {createDdi(filterOptions)}
                        </DropdownButton>

                        <FormSearch
                            placeholder="Search... "
                            value={this.state.searchValue}
                            onChange={this.handleSearchChange}
                            onClick={this.clearSearchValue} />
                    </ButtonGroup>

                    <ButtonGroup className="action">
                        {this.state.tableData &&
                            <CSVLink data={this.state.tableData} headers={csvHeaders} filename={"purchase_orders.csv"}>
                                <Button variant="outline-primary">Download Table</Button>
                            </CSVLink>
                        }
                    </ButtonGroup>

                </ButtonToolbar>
            </Row>
        );
    }

    private thisRenderTable() {
        const { aggregate } = this.props;
        const { user } = this.props;

        var orgidfltr = user.orgId;


        var allPOColumnsNewOrgfiltered = allPOColumnsNew.filter(function (item) {

            if (item.onlyfor == undefined) return true;
            else {

                if (item.onlyfor === orgidfltr) {
                    return true;
                }
                else {
                    return false;
                }
            }
            

            //onsole.log("Return");
            //return item;

            //return !(item.onlyfor==='peacoks')

        });

        if (aggregate === "po") {
            return (
                this.state.poData &&
                <ReactTable
                    data={this.state.tableData}
                    columns={aggregatedPOColumnsnew}
                    style={{ textAlign: "center", cursor: "pointer" }}
                    pivotBy={["purchaseOrderId"]}
                    className="-highlight -striped itemstable"
                    filterable
                    page={this.state.page}
                    pages={this.state.cursorList.length}
                    manual
                    onPageChange={this.handlePageChange}
                    onPageSizeChange={this.handlePageSizeChange}
                    onSortedChange={this.handlePageSort}

                    loading={this.state.loading}

                    showPageJump={false}
                    SubComponent={(row) => {
                        if (!row.original.title && !row.original.imageLink && !row.original.type) {
                            return (
                                <Card style={{ minHeight: 200, paddingTop: 20 }}>
                                    <h4> No product information currently available </h4>
                                </Card>
                            );
                        }
                        const newData: any[] = [{
                            department: row.original.type,
                            image: row.original.imageLink,
                            name: row.original.title,
                        }];
                        for (const nd of newData) {
                            nd.imageLink =
                                <Card>
                                    <img style={{ width: 150, height: 150 }} src={nd.image} alt="" />
                                </Card>
                        }
                        return (
                            <div style={{ padding: "20px" }}>
                                <ReactTable
                                    data={newData}
                                    columns={locationSubComponent}
                                    defaultPageSize={1}
                                    showPagination={false}
                                    className="-striped -highlight"
                                />
                            </div>
                        );
                    }}
                />
            );
        } else {
            return (
                <ReactTable
                    data={this.state.tableData}
                    columns={allPOColumnsNewOrgfiltered}
                    style={{ textAlign: "center", cursor: "pointer" }}
                    className="-highlight -striped itemstable"
                    filterable
                    minRows={20}
                    page={this.state.page}
                    pages={this.state.pages}
                    manual
                    onPageChange={this.handlePageChange}
                    onPageSizeChange={this.handlePageSizeChange}
                    onSortedChange={this.handlePageSort}

                    loading={this.state.loading}

                    showPageJump={false}
                />
            );
        }
    }

    private handleFilterChange = async (value: string) => {
        this.setState({ filterValue: value, ...newPagination() });
    }

    private handleSearchChange = async (e: any) => {
        this.setState({ searchValue: e.target.value, ...newPagination() });
    }

    private clearSearchValue = async (e: any) => {
        this.setState({ searchValue: "", ...newPagination() });
    }

    private handlePageChange = async (page: number) => {
        this.setState({ page });
    }

    private handlePageSizeChange = async (pageSize: number, page: number) => {
        this.setState({ pageSize, ...newPagination() });
    }

    private handlePageSort = async (orderBy: any) => {
        const { id, desc } = orderBy[0];
        this.setState({ orderBy: id, desc, ...newPagination() });
    }

    private updateDataTimed = () => {
        console.log("TIMED");
        if (this.searchTimeout) {
            clearTimeout(this.searchTimeout);
        }

        this.searchTimeout = setTimeout(
            async () => { await this.loadData(); }, 300);
    }

    private loadData = async () => {
        this.setState({ loading: true });

        const { client: { purchaseOrdersClient }, aggregate } = this.props;
        const orgId = this.props.user.org_id;
        const { searchValue, cursorList, page, pageSize, filterValue, desc } = this.state;

        //console.log("getting cursor for " + page);

        const cursor = page < cursorList.length ? cursorList[page] : cursorList[0];

        let allPO = await purchaseOrdersClient.searchPO(orgId, "purchaseOrderId", this.state.orderBy, aggregate,
            filterValue === "all" ? {} : { pos: filterValue },
            searchValue, cursor, pageSize, desc);
        if (page === cursorList.length - 1 && allPO.metadata.nextCursor) {
            cursorList.push(allPO.metadata.nextCursor);
        }
        const pages = allPO.metadata.pageCount;

        //console.log("allPOOO Explorer", allPO);
        allPO = allPO.purchaseOrders;
        const getDataInFormat = this.getDataInFormat(allPO);
        this.setState({
            loading: false,
            poData: getDataInFormat,
            pages,
            tableData: getDataInFormat,
        });
    }

    private getDataInFormat(allPO: any) {
        const { basePath } = this.props;
        const result: any = [];
        for (const po of allPO) {
            const POObject = {} as any;
            POObject.purchaseOrderId = po.purchaseOrderId;
            POObject.createDate = po.createDate;
            POObject.basePath = basePath;

            for (const item of po.items) {
                const poi = Object.assign({}, POObject, item);
                result.push(poi);
            }
        }
        return result;
    }
}

function mapStateToProps(state: any) {
    return { client: state.auth.client, user: state.auth.user }
}

export const POExplorer = withLoader(connect(mapStateToProps)(_POExplorer));
