import { FcFetchClient } from "fc-rest-client";
import moment from "moment";
import React from "react";
import { Card, Container } from "react-bootstrap";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom"
import { Link } from "react-router-dom";
import { Shipment, PurchaseOrder } from "tw-api-common";
import { withLoader } from "../../components";
import PODetailCard from "../../components/PODetailCard";
import TableCard from "../../components/TableCard";
import { itemColumns, locationColumns, shipColumns } from "../columns/POColumns";
import { detailCaseColumns, updateCaseColumns } from "../items/ItemDetailColumns";

// const legend = require('../../images/legend.png');
// converts array into ($1,$2..) format string

interface IProps extends RouteComponentProps<IMatchParams> {
  startLoading: any;
  stopLoading: any;
  client: FcFetchClient;
  user: any;
  basePath: string;
  searchText: string;
}

interface IMatchParams {
  po: string;
}

interface IState {
  po: PurchaseOrder | null;

  shipdata: any;
  searchText: string;
  markers: any[],
  openWindow: any,
  caseData: any[],
  locationdata: any,
  summary: any,
}


const createWhereInClause = (array: any, startIndex: any) => {
  let whereShip = "(";
  array.forEach((val: any, index: number) => {
    if (index > 0) {
      whereShip += ", ";
    }
    whereShip += ("$" + String(index + startIndex));
  });
  whereShip += ")";
  return whereShip;
};



class PODetailView extends React.Component<IProps, IState> {

  constructor(props: any) {
    super(props);
    this.state = {
      po: null,

      summary: null,
      locationdata: null,
      shipdata: null,
      searchText: "",
      markers: [],
      openWindow: null,
      caseData: [],
    };
    this.handleMarkerHover = this.handleMarkerHover.bind(this);
    this.renderDisplay = this.renderDisplay.bind(this);
    this.mapStatusFilter = this.mapStatusFilter.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
  }
  public async componentDidMount() {
    let po;
    if (this.props.match) {
      po = this.props.match.params.po;
    }
    if (po) {
      this.setPO(po);
    }
  }

  public async setPO(poId: any) {
    const { startLoading, stopLoading } = this.props;
    startLoading("Loading your data...");
    const poClient = await this.props.client.purchaseOrdersClient;
    const shipClient = await this.props.client.getShipmentsClient();

    const poRes = await poClient.info(poId);

    // Check if the PO is present 
    const po = poRes.purchaseOrder;
    console.log("PO DETAIL INFO");
    console.log(po);


    let shipItems = await shipClient.queryShipmentItems(`
      org_id =$1 and purchase_order_id =$2`,
      [this.props.user.org_id, poId], [] , []);

    console.log("SHIP ITEMS");
    console.log(shipItems);
      
    shipItems = shipItems.items;
    const shipmentIds = shipItems.map((val: any) => {
      return val.shipment_id;
    });
    const whereShip = createWhereInClause(shipItems, 2);
    const whereClause = "s.org_id =$1 and s.shipment_id in " + whereShip;
    let ship;
    if (shipItems.length) {
      ship = await shipClient.queryShipments(whereClause, [this.props.user.org_id].concat(shipmentIds));
    }

    // leaving this get stuff for rn
    const pos = await this.props.client.getStuff(`
      SELECT * FROM purchase_orders pos
      WHERE pos.org_id = '${this.props.user.org_id}' and purchase_order_id = '${poId}'` );

    const locationInfo = await this.props.client.getStuff(`
      SELECT containers.container_id as cd, shipment_items.delivery_time, v1.lat as vessel_lat, v1.lng as vessel_lng,
        buildings.lat, buildings.lng, v1.vessel_name, v1.vessel_container_id, buildings.container_id, buildings.name,
        purchase_order_items.sku, purchase_order_items.purchase_order_item_id, inventory_items.quantity,
        purchase_order_items.quantity,
        shipment_items.delivery_quantity
      FROM purchase_order_items
      LEFT JOIN inventory_items ON
     purchase_order_items.purchase_order_item_id = inventory_items.purchase_order_item_id
     and purchase_order_items.purchase_order_id = inventory_items.purchase_order_id
     and purchase_order_items.sku = inventory_items.sku
     and inventory_items.quantity > 0
     left join shipment_items on purchase_order_items.purchase_order_id = shipment_items.purchase_order_id
     and purchase_order_items.purchase_order_item_id = shipment_items.purchase_order_item_id
     and purchase_order_items.sku = shipment_items.sku
     left join containers on inventory_items.container_id = containers.container_id
     LEFT JOIN vehicle_locations v1 on containers.parent_container_id = v1.vessel_container_id
     LEFT OUTER JOIN vehicle_locations v2 ON (containers.parent_container_id = v2.vessel_container_id AND (v1.timestamp < v2.timestamp))
     left join buildings on (purchase_order_items.origin_container = buildings.container_id and (purchase_order_items.recv_quantity<=0 and (inventory_items.quantity is null and shipment_items.delivery_time is null))) or
     (containers.container_id = buildings.container_id ) or
     (buildings.container_id = containers.parent_container_id and shipment_items.shipment_id is null)
     or  (purchase_order_items.dst_container = buildings.container_id and shipment_items.delivery_time is not null)
     where purchase_order_items.org_id ='` + this.props.user.org_id + `' and v2.vessel_container_id IS NULL and  purchase_order_items.purchase_order_id = '` + po.purchaseOrderId + `'GROUP BY shipment_items.delivery_time, cd,vessel_lat,vessel_lng,v1.vessel_name, v1.vessel_container_id, buildings.container_id, buildings.name, purchase_order_items.purchase_order_item_id, purchase_order_items.sku, inventory_items.quantity, purchase_order_items.quantity, shipment_items.delivery_quantity;`);

    //console.log("LOCATIONSSSSSSSSSSS");
    //console.log(locationInfo);

    // use different aggregate function for more info
    const locations = [];
    const sumDict: any = {};
    for (const [i,li] of locationInfo.entries()) {
      // Reconsider this logic
      if (locationInfo[i].delivery_time) {
        locationInfo[i].status = "Delivered";
        locationInfo[i].color = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
      } else if (!locationInfo[i].cd) {
        locationInfo[i].status = "Factory";
        locationInfo[i].color = "http://maps.google.com/mapfiles/ms/icons/orange-dot.png";
      } else {
        if (locationInfo[i].container_id && 
            locationInfo[i].container_id.slice(0, locationInfo[i].container_id.indexOf("&")) === "port") {
          locationInfo[i].status = "Port of Loading";
          locationInfo[i].color = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
        } else {
          locationInfo[i].status = "Warehouse";
          locationInfo[i].color = "http://maps.google.com/mapfiles/ms/icons/blue-dot.png";
        }
      }
      if (locationInfo[i].vessel_name || locationInfo[i].vessel_container_id) {
        locationInfo[i].container_id = locationInfo[i].vessel_container_id;
        locationInfo[i].name = locationInfo[i].vessel_name;
        locationInfo[i].lat = locationInfo[i].vessel_lat;
        locationInfo[i].lng = locationInfo[i].vessel_lng;
        locationInfo[i].status = "Shipped";
        locationInfo[i].color = "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png";
      }
      if (!locationInfo[i].container_id) {
        locationInfo[i].container_id = locationInfo[i].cd;
      }
      if (sumDict[locationInfo[i].container_id]) {
        sumDict[locationInfo[i].container_id].quantity += locationInfo[i].quantity;
      } else {
        sumDict[locationInfo[i].container_id] = locationInfo[i];
      }
    }
    const count = 0;
    for (const key in sumDict) {
      const i = count;
      let loc = sumDict[key].name;
      if (sumDict[key].container_id) {
        if (!sumDict[key].name || sumDict[key].name === "NULL") {
          loc = sumDict[key].container_id.slice(sumDict[key].container_id.indexOf("&") + 1);
        }
        locations.push({
          id: i,
          onMouseOver: () => { this.handleMarkerHover(i); },
          onMouseOut: () => { this.handleMarkerLeave(); },
          onClick: () => { },
          // color: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
          lat: sumDict[key].lat,
          lng: sumDict[key].lng,
          status: sumDict[key].status,
          location: loc,
          color: sumDict[key].color,
          link: this.props.basePath + "/detail-location/" + sumDict[key].container_id,
          quantity: sumDict[key].quantity,
          type: sumDict[key].container_id.slice(0, sumDict[key].container_id.indexOf("&")),
        });
      }
    }

    this.setState({
      po,

      shipdata: this.createShipRows(ship, shipItems),
      locationdata: locations,
      markers: locations,
      summary: this.createSummaryRow(po, shipItems),
    });

    stopLoading();
  }
  public handleMarkerHover(i: any) {
    this.setState({
      openWindow: i - 1,
    });
  }
  public handleMarkerLeave() {
    this.setState({
      openWindow: null,
    });
  }
  public onRowClick = (state: any, rowInfo: any, column: any, instance: any) => {
    return {
      onClick: () => {
        if (column.Header === "SKU") {
          this.props.history.push(this.props.basePath + "/items/detail/" + rowInfo.original.productId);
        }
        if (column.Header === "Shipment Ref") {
          this.props.history.push(this.props.basePath + "/shipments/detail/" + rowInfo.original.shipment_id);
        }
      },
    };
  }
  public async componentDidUpdate(prevProps: any) {
    if (this.props.searchText !== prevProps.searchText && this.props.searchText.length > 0) {
      this.setPO(this.props.searchText);
    } else if (prevProps.match.params.po !== this.props.match.params.po && this.props.match.params.po) {

      this.setPO(this.props.match.params.po);
    }
  }

  public mapStatusFilter(status: any) {
    const newMarkers = [];
    for (let i = 0; i < this.state.locationdata.length; i++) {
      if (status === this.state.locationdata[i].status || status === "*") {
        newMarkers.push(this.state.locationdata[i]);
      }
    }
    this.setState({
      markers: newMarkers,
    });
  }
  public renderDisplay() {
    if (this.state.po) {
      const event = new Date(this.state.po.createDate);
      return (

        <PODetailCard page="Purchase Order"
          heading={this.state.summary.purchase_order_id}
          markers={this.state.markers}
          openWindow={this.state.openWindow}
          mapStatusFilter={this.mapStatusFilter}
          details={[{ label: "Purchase Order Number", value: this.state.po.purchaseOrderId },
          { label: "Created On", value: event.toLocaleDateString(navigator.language) },
          { label: "Line Items", value: this.state.summary.linenumber },
          { label: "Order Quantity", value: this.state.summary.sum },
          { label: "Delivered Quantity", value: this.state.summary.arrive_qty ? this.state.summary.arrive_qty : 0 },
          { label: "Open Quantity", value: (this.state.summary.sum - (this.state.summary.arrive_qty ? this.state.summary.arrive_qty : 0)) },
          { label: "Shipped Quantity", value: this.state.summary.ship_qty ? this.state.summary.ship_qty : 0 },
            /*{ label: "Warehouse Received Quantity", value: this.state.summary.received_qty ? this.state.summary.received_qty : 0 }*/
          ]}
        />
      );
    } else {
      return (
        <Container style={{ textAlign: "center", verticalAlign: "middle", paddingTop: 80, minHeight: 200, margin: 10 }}>
          <h4 > View purchase order details here </h4>
        </Container>
      );
    }
  }

  public render() {
    this.state.caseData.forEach((c: any) => {
      if (c.case_priority === "High") {
        c.color_case_priority = <span style={{ color: "red" }}> {c.case_priority} </span>;
      }
      if (c.case_priority === "Medium") {
        c.color_case_priority = <span style={{ color: "blue" }}> {c.case_priority} </span>;
      }
      if (c.case_priority === "Low") {
        c.color_case_priority = <span style={{ color: "orange" }}> {c.case_priority} </span>;
      }

      let show_case_id = c.case_id.slice(0, 12);

      c.show_case_id = <Link to={{ pathname: this.props.basePath + "/cases/detail", state: { case_id: c.case_id } }} >  {show_case_id} </Link>;
      c.short_case_id = show_case_id;
    });
    return (
      <>
        <Card style={{ padding: 20 }}>
          {this.renderDisplay()}
          {this.state.po &&
            <Card>
              <TableCard heading="Products" tableSpecs={{ filterable: true, rowNumber: 5 }} getTdProps={this.onRowClick}
                         data={this.state.po.items} columns={itemColumns} />
            </Card>
          }
          {this.state.shipdata && this.state.shipdata.length > 0 &&
            <Card>
              <TableCard heading="Shipments" tableSpecs={{ filterable: true, rowNumber: 5 }}
                         getTdProps={this.onRowClick} data={this.state.shipdata} columns={shipColumns} />
            </Card>
          }
          {/*I cant get this work for target so i am just removing this from the console for target*/}
          {(this.state.locationdata && this.props.user.org_id != "mytarget") && this.state.locationdata.length > 0 &&
            <Card>
              <TableCard heading="Locations" tableSpecs={{ filterable: true, rowNumber: 5 }}
                         data={this.state.locationdata} columns={locationColumns} />
            </Card>
          }
          {
            this.state.caseData && this.state.caseData.length > 0 &&
            <Card>
              <TableCard heading="Open Cases On PO" tableSpecs={{ filterable: true, rowNumber: 5 }} getTdProps={this.onRowClick} data={this.state.caseData} columns={[{ Header: "Case Details", columns: detailCaseColumns }, { Header: "Case Updates", columns: updateCaseColumns }]} />
            </Card>
          }
        </Card>
      </>
    );
  }

  private createSummaryRow = (po: PurchaseOrder, shipItems: any) => {
    const summaryRow: any =  {
      arrive_qty: this.getDeliveredQuantity(shipItems),
      ship_qty: this.getShippedQuantity(shipItems),
      sum: this.getPOQuantity(po),
      linenumber: po.items.length,
    };

    return summaryRow;
  }

  private getPOQuantity = (po: PurchaseOrder) => {
    let poQuantity = 0;
    for (const poi of po.items) {
      poQuantity += poi.quantity;
    }
    return poQuantity;
  }

  private getShippedQuantity = (array: any[]) => {
    let shippedQuantity = 0;
    array.forEach((val) => {
      shippedQuantity += val.delivery_quantity;
    });
    return shippedQuantity;
  }

  private getDeliveredQuantity = (array: any[]) => {
    let delivery_quantity = 0;
    array.forEach((val: any) => {
      if (val.delivery_time) {
        delivery_quantity += val.delivery_quantity;
      }
    });
    return delivery_quantity;
  }

  private createShipRows = (shipRows: Shipment[], shipItemRows: any) => {

    const shipMap: any = {};
    let newRows;
    if (shipRows && shipItemRows) {
      shipRows.forEach((val) => {

        shipMap[val.shipmentId] = val;
      });
      newRows = shipItemRows.map((val: any) => {
        console.log(val.sku);
        console.log(val.delivery_quantity);
        return Object.assign(val, shipMap[val.shipment_id]);
      });
    }
    console.log("new ROWS");
    console.log(newRows);
    
    return newRows;
  }
}

function mapStateToProps(state: any) {
  return { client: state.auth.client, user: state.auth.user };
}
export default withLoader(withRouter(connect(mapStateToProps)(PODetailView)));
