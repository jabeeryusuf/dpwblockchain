import React from "react";
import { Button, ButtonGroup, ButtonToolbar, Card, Col, Dropdown,
         DropdownButton as _DropdownButton, Form, Row } from "react-bootstrap";
import { connect as _connect} from "react-redux";
import { withRouter as _withRouter } from "react-router-dom"
import ReactTable from "react-table";
import { withLoader } from "../../components";
import { FormSearch } from "../../components/forms";
import Table from "../../components/Table";
import { locationSubComponent } from "../columns/columns";
import { aggregatedPOColumns, allPOColumns } from "../columns/POColumns";

const { CSVLink } = require("react-csv");
const DropdownButton: any = _DropdownButton;
const withRouter: any = _withRouter;
const connect: any = _connect;

const aggregateOptions = [
  { label: "Group by Items", value: "items" },
  { label: "Group by Purchase Order", value: "pos" },
];

const filterOptions = [
  { label: "All POs", value: "all" },
  { label: "Open POs", value: "open" },
]

const createDdi =
  (opts: any ) => opts.map((o: any, i: any) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

const getLabel = (opts: any, value: any) => {
  const l = opts.find((o: any) => (o.value === value));
  return l ? l.label : "";
};

const csvHeaders = ((apocs: any[]) => {
  const headers = [];
  for (const ap of apocs) {
    if (ap.accessor !== "image") {
      const newObj = {
        key: ap.accessor,
        label: ap.Header,
      }
      headers.push(newObj)
    }
  }
  return headers;
})(allPOColumns);

class _POExplorer extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
    this.state = {
      aggregateValue: "items",
      filterValue: "all",
      openWindow: -1,
      poData: null,
      searchValue: "",
      tableData: null,
    };
  }


  public render() {
    return (
      <>
        {this.renderFilterBar()}
        {this.state.tableData && this.thisRenderTable()}

      </>
    );
  }

  public componentDidMount = async () => {
    if (!this.props.hide) {
      this.loadData();
    }
  }

  public componentDidUpdate = async (prevProps: any) => {
    if (!this.state.poData && prevProps.hide !== this.props.hide) {
      this.loadData();
    }
  }

  private loadData = async () => {
    const { startLoading, stopLoading } = this.props;
    startLoading("Loading your data ...");
    const res = await this.props.client.getStuff(`
      SELECT purchase_order_items.*,purchase_orders.create_date, products.image, products.department, products.name
      FROM purchase_orders
      JOIN purchase_order_items ON purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
      AND purchase_order_items.org_id ='` + this.props.user.org_id + `'
      LEFT JOIN products on products.sku = purchase_order_items.sku
      WHERE purchase_orders.org_id ='` + this.props.user.org_id + `'`,
    );

    for (const r of res) {
      r.link = this.props.basePath + "/purchaseorders/detail/" + r.purchase_order_id;
      r.sku_link = this.props.basePath + "/Items/detail/" + r.sku;
    }
    this.setState({
      poData: res,
      tableData: res
    })

    stopLoading();
  }
  private handleAggregateChange = async (value: string) => {
    this.setState({
      aggregateValue: value,
    })
  }

  private handleFilterChange = async (value: string) => {
    this.setState({
      filterValue: value,
    }, this.doFilterChange);
  }

  private handleSearchChange = async (e: any) => {
    let value = e.target.value;
    this.setState({
      searchValue: value,
    }, this.doFilterChange)
  }

  private doFilterChange() {
    let { filterValue, searchValue }  = this.state;

    let newTable = [];

    for (const pod of this.state.poData) {

      if (filterValue === "open" && pod.recv_quantity === 0 ) {
        continue;
      }

      if (pod.purchase_order_id.indexOf(searchValue) === -1) {
        continue;
      }

      newTable.push(pod);
    }

    this.setState({
      tableData: newTable,
    });
  }



  private renderFilterBar() {
    const { filterValue } = this.state;


    return (
      <Row>
        <ButtonToolbar className="toolbar">
          <ButtonGroup className="filter" >

            <DropdownButton title={getLabel(filterOptions, filterValue)} onSelect={this.handleFilterChange}>
              { createDdi(filterOptions) }
            </DropdownButton>

            <FormSearch
              placeholder="Search... "
              value={this.state.searchValue}
              onChange={this.handleSearchChange}
              onClick={this.clearSearchValue} />
          </ButtonGroup>

          <ButtonGroup className="action">
              {this.state.tableData &&
                <CSVLink data={this.state.tableData} headers={csvHeaders} filename={"purchase_orders.csv"}>
                  <Button variant="outline-primary">Download Table</Button>
                </CSVLink>
              }
          </ButtonGroup>

        </ButtonToolbar>
      </Row>
    );
  }
  
  private clearSearchValue = (event: any) => {
    this.setState({
      searchValue: "",
    }, () => this.handleFilterChange("hi"));
  }

  private thisRenderTable() {
    const { aggregate } = this.props;

    if (aggregate === "po") {
      return (
        this.state.poData &&
        <ReactTable data={this.state.tableData} columns={aggregatedPOColumns}
          style={{ textAlign: "center" }}
          pivotBy={["purchase_order_id"]}
          className="-striped -highlight"
          filterable
          defaultPageSize={10}
          SubComponent={row => {
            if (!row.original.name && !row.original.image && !row.original.department) {
              return (
                <Card style={{ minHeight: 200, paddingTop: 20 }}>
                  <h4> No product information currently available </h4>
                </Card>
              )
            }
            const newData: any[] = [{
              department: row.original.department,
              image: row.original.image,
              name: row.original.name,
            }]
            for (const nd of newData) {
              nd.imageLink =
                <Card>
                  <img style={{ width: 150, height: 150 }} src={nd.image} alt="" />
                </Card>
            }
            return (
              <div style={{ padding: "20px" }}>
                <ReactTable
                  data={newData}
                  columns={locationSubComponent}
                  defaultPageSize={1}
                  showPagination={false}
                />
              </div>
            );
          }}
        />
      );
    } else {
      return (
        <Table style={{ cursor: "pointer" }} className="-highlight"
        data={this.state.tableData}
          columns={allPOColumns}
          filterable
        />
      );
    }
  }
}

function mapStateToProps(state: any) {
  return { client: state.auth.client, user: state.auth.user }
}

export const POExplorerOLD = withRouter(withLoader(connect(mapStateToProps)(_POExplorer)));