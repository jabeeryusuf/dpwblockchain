import React from 'react';
import Portlet from '../layout/Portlet';
import Select from '../components/UI/Select';
import { View } from '../layout/View';
import { Row, Col } from 'react-bootstrap';
import {connect} from 'react-redux';
import { loadingActions } from '../redux/actions';
import { Subheader } from '../layout';
import { exceptionColumns, factory_appointment_dateColumns, factory_booking_dateColumns, factory_delivery_dateColumns } from './milestoneColumns';
import ReactTable from 'react-table';
import { CSVLink } from 'react-csv';
import PillButton from '../components/Buttons/PillButton';


class BCOFileUpload extends React.Component {

  constructor(props) {
   super(props);

   this.state = {
     date: new Date(),
     imageURL: ''
   };

   this.handleUploadImage = this.handleUploadImage.bind(this);
 }
 async componentDidMount() {
   const { dispatch } = this.props;
   dispatch(loadingActions.start('Loading your page...'));
   this.timerID = setInterval(
     () => this.tick(), 1000)


   dispatch(loadingActions.stop());
 }
 componentWillUnmount() {
   clearInterval(this.timerID);
 }
 handleUploadImage(ev) {
  ev.preventDefault();

  const data = new FormData();
  data.append('file', this.uploadInput.files[0]);
  console.log(data)
  //data.append('filename', this.fileName.value);

  fetch('http://demo.bc.localhost:3000/bco-data', {
    method: 'POST',
    body: data,
  }).then((response) => {
    response.json().then((body) => {
      this.setState({ imageURL: `http://demo.bc.localhost:3000/${body.file}` });
    });
  });
}
 tick() {
   this.setState({
     date: new Date()
   })
 }
  render() {
    return (
      <View>
      <form onSubmit={this.handleUploadImage}>
        <Row><font size="5">Upload File</font></Row>
        <Row>
        <Col md={5}></Col>
        <Col>
          <Portlet>
            <Row><font size="3">Enter containers data</font></Row>
            <br></br>
            <Row>
              <input ref={(ref) => { this.uploadInput = ref; }} type="file" />
              <div>
                <button>Upload</button>
              </div>
            </Row>

          </Portlet>
        </Col><Col>
        </Col>
        </Row>
        <Row>
          <dir>
            <img src={this.state.imageURL} alt="img" />
          </dir>
        </Row>
        <Row>
          It is {this.state.date.toLocaleTimeString()}.

        </Row>
      </form>
      </View>
    )
  }

}


function mapStateToProps(state) {
	return { client: state.userAuth.client }
}
export default connect(mapStateToProps)(BCOFileUpload);
