import React from 'react';
import {Link} from 'react-router-dom';
import {withLoader} from '../../components';
import { ListGroup,Row,InputGroup,FormControl,Col} from 'react-bootstrap';
import { connect } from 'react-redux';


class AddConversation extends React.Component {
  
  render() {
    
    let {userPath,users} = this.props;

    // Fix
    userPath = 'id-'

    return (
      <>
        <Row >
          <Col sm={{ span: 8, offset: 2 }} >
            <h2 className="text-left">Find or Create Users</h2>
          </Col>
        </Row>

        <Row>
          <Col sm={{ span: 8, offset: 2 }}>
            <InputGroup size="lg">
              <FormControl aria-label="Go" aria-describedby="inputGroup-sizing-sm" />
              <InputGroup.Append>
                <InputGroup.Text id="inputGroup-sizing-lg">Go</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Col>
        </Row>

        <br />
        <br />

        <Row>
          <Col sm={{ span: 8, offset: 2 }}>
            <ListGroup>
              {Object.keys(users).map((uid, i) => (
                <ListGroup.Item key={i}>
                  <Link to={userPath + uid}>
                    {users[uid].name  ? users[uid].name : uid}
                  </Link>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Col>
        </Row>
      </>
    );
  }


}

function mapStateToProps(state) {
  return { 
    conversations: state.conversation.conversations,
    users: state.user.users,
    user: state.auth.user,
  };
}

export default withLoader(connect(mapStateToProps)(AddConversation));
