import React from 'react';
import { Link } from 'react-router-dom';
import { Card, Button, Col, Row } from 'react-bootstrap';
import { vendorGroupColumns, userGroupColumns } from '../columns/columns';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import { withLoader } from '../../components';
import VerticalModal from '../../components/VerticalModal.js';
import CreateGroup from '../../components/forms/CreateGroup.js';
import NamesForNewUsers from '../../components/forms/NamesForNewUsers.js';


class GroupTables extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      type: "",
      search: "",
      orgVariant: null,
      userGroups: [],
      orgGroups: [],
      serviceGroups: [],
      newUsers: [],
      columns: vendorGroupColumns,
      showCreateGroup: false,
      showNewusers: false,
      submitting: false,
      successMessage: "",
      errorMessage: "",
    }
    this._isMounted = true;
    this.onCreateGroup = this.onCreateGroup.bind(this);
    this.onAddNames = this.onAddNames.bind(this);
  }

  componentDidMount() {
    let type = this.props.type;
    this.setState({
      type: type,
      columns: type === "org" ? vendorGroupColumns : userGroupColumns
    }, () => this.fetchGroups());
  }

  /**
   * Depending on the props that are passed in to this component, I want to render different group
   * information. This thing is a mess, I know. 
   */
  componentDidUpdate() {
    const { orgGroups, userGroups, serviceGroups } = this.state;

    //variant applies mostly to org type groups
    //the variants are either 'service' groups or 'vendor groups'
    if ((this.props.type !== this.state.type) || (this.props.type === this.state.type && this.props.variant !== this.state.orgVariant)) {
      if (this.props.type === "org" && orgGroups.length < 1) { //if I haven't fetched org groups yet do the api call
        this.setState({
          type: this.props.type,
          orgVariant: this.props.variant,
          columns: vendorGroupColumns,
        }, () => this.fetchGroups());
      } else if (this.props.type === "org" && orgGroups.length >= 1) { //if I've made the fetch call already and have groups in state, don't do it again
        this.setState({
          type: this.props.type,
          orgVariant: this.props.variant,
          columns: vendorGroupColumns,
          filteredGroups: this.props.variant === this.props.type ? orgGroups : serviceGroups
        });
      } else if (this.props.type === "user_created" && userGroups.length < 1) { //same thing with user groups. If no groups, make fetch call 
        this.setState({
          type: this.props.type,
          orgVariant: this.props.variant,
          columns: userGroupColumns,
        }, () => this.fetchGroups());
      } else {
        this.setState({
          type: this.props.type,
          orgVariant: this.props.variant,
          columns: userGroupColumns,
          filteredGroups: userGroups
        });
      }
    }

    if (this.props.search !== this.state.search) {
      this.setState({
        search: this.props.search
      }, () => this.searchGroups(this.props.search));
    }
  }

  /**
   * Fetches the groups depending on type and org.
   * Separates the groups in to 'service' and 'vendor' groups if the type is org
   */
  async fetchGroups() {
    const { type } = this.state;
    const { startLoading, stopLoading } = this.props;

    startLoading("Loading your data...");

    let groups = await this.props.client.getUsersInGroups(type, this.props.org.org_id, null);

    //If I have already fetched these groups then I want to keep using them
    let vendorGroups = this.state.orgGroups.slice();
    let serviceGroups = this.state.serviceGroups.slice();

    for (let group of groups) {
      group.button = <Link to={this.props.basePath + '/groups/detail'} onClick={() => this.props.onClick(group, type)}>{group.groupName}</Link>
      if (type === "org" && this.state.orgGroups.length < 1) {
        if (group.venderCode && group.venderCode.startsWith('V')) {
          vendorGroups.push(group);
        } else {
          serviceGroups.push(group);
        }
      }
    }
    (vendorGroups.length > 0) && vendorGroups.sort((a, b) => (a.groupName > b.groupName) ? 1 : ((b.groupName > a.groupName) ? -1 : 0));
    (serviceGroups.length > 0) && serviceGroups.sort((a, b) => (a.groupName > b.groupName) ? 1 : ((b.groupName > a.groupName) ? -1 : 0));
    groups.sort((a, b) => (a.groupName > b.groupName) ? 1 : ((b.groupName > a.groupName) ? -1 : 0));

    //When setting state, looking to see if I already have these groups. If not, use the fetched ones
    //If so, use the ones I already have
    if (this._isMounted) {
      this.setState({
        userGroups: type !== "org" && this.state.userGroups.length < 1 ? groups : this.state.userGroups,
        orgGroups: type === "org" && this.state.orgGroups.length < 1 ? vendorGroups : this.state.orgGroups,
        serviceGroups: type === "org" && this.state.serviceGroups.length < 1 ? serviceGroups : this.state.serviceGroups,
        filteredGroups: type !== "org" ? groups : this.state.orgVariant === "service" ? serviceGroups : vendorGroups
      })
    };


    stopLoading();
  }

  /**
   * Searches the groups in current table for anything that matches the input string
   * @param {string} str 
   */
  searchGroups(str) {
    const { type, userGroups, orgGroups, serviceGroups } = this.state;
    let filter = [];
    let groups = type !== "org" ? userGroups : this.state.orgVariant === "service" ? serviceGroups : orgGroups;

    //If string is empty then reset the table
    if (str === "") {
      filter = groups;
    } else {
      for (let group of groups) {
        if (group.groupName.includes(str.trim())) {
          filter.push(group);
        }
      }
    }

    this.setState({
      filteredGroups: filter
    });

  }

  /**
   * Function gets called after creating group in pop up modal.
   * Accepts a group name, list of emails and list of child groups from modal.
   * Makes an API call to create that group
   * @param {string} groupName 
   * @param {list} emails 
   * @param {list} groups 
   */
  async onCreateGroup(groupName, emails, groups) {
    this.setState({
      submitting: true
    });

    let emailList = [];

    for (let i = 0; i < emails.length; i++) {
      emailList.push(emails[i].email)
    }

    let payload = {
      groupName: groupName.trim(),
      emails: emailList,
      groups: groups,
      type: this.state.type,
      org: this.props.org.org_id
    }

    let createGroup = await this.props.client.createGroup(payload);
    if (!createGroup.errorMessage) {
      this.setState({
        successMessage: "Group successfully created",
        errorMessage: ""
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showCreateGroup: false,
            submitting: false
          });
          if (isObject(createGroup)) {
            this.setState({
              newEmails: createGroup,
              showNewUsers: true
            });
          }
        }, 1500);
      });
      this.fetchGroups();
    } else {
      this.setState({
        errorMessage: createGroup.errorMessage,
        submitting: false
      });
    }
  }

  /**
   * When a user creates or edits a new group and adds contacts that don't exist in DB
   * yet, this function is called after the user enters their new contacts' names.
   * @param {list} emailsAndNames 
   */
  async onAddNames(emailsAndNames) {
    this.setState({
      submitting: true
    });
    let addedNames = await this.props.client.getUsersApi().addNameToUser(emailsAndNames);
    if (!addedNames.errorMessage) {
      this.setState({
        successMessage: "Names successfully added",
        errorMessage: ""
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showNewUsers: false,
            submitting: false
          });
        }, 1500);
      });
    } else {
      this.setState({
        errorMessage: addedNames.errorMessage,
        submitting: false
      });
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    const { type, filteredGroups, columns, submitting, successMessage, errorMessage, newEmails } = this.state;
    let modalClose = () => this.setState({ showCreateGroup: false, errorMessage: "", successMessage: "" });

    return (
      <div>
        <VerticalModal title="Create a new group"
          size="md"
          show={this.state.showCreateGroup}
          onHide={modalClose}
          page={<CreateGroup onSubmit={this.onCreateGroup}
            submitting={submitting}
            successMessage={successMessage}
            errorMessage={errorMessage}
            footerButton={"Create"} />} />
        <VerticalModal title="Names for new contacts"
          size="md"
          show={this.state.showNewUsers}
          page={<NamesForNewUsers onSubmit={this.onAddNames}
            type={type}
            submitting={submitting}
            successMessage={successMessage}
            errorMessage={errorMessage}
            emails={newEmails} />} />
        {type === "user_created" ?
          <Row className="justify-content-center">
            <Button onClick={() => this.setState({ showCreateGroup: true })}>Create a new group</Button>
          </Row>
          : ''
        }
        <br />
        {filteredGroups && filteredGroups.length > 0 ?
          <ReactTable data={filteredGroups}
            columns={columns}
            defaultPageSize={15} />
          :
          <Row className="justify-content-center">
            <Col sm={{ span: 4 }}>
              <Card>
                <Card.Header>
                  <Card.Title>No Groups Found</Card.Title>
                </Card.Header>
                <Card.Body>
                  <Card.Text>
                    <font size="3">No groups were found. Use the button above to create a new group</font>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    org: state.auth.org
  }
}

/**
 * Helper function to check if something is an object
 * @param {object} obj 
 */
function isObject(obj) {
  return obj === Object(obj);
}

export default withLoader(connect(mapStateToProps)(GroupTables));