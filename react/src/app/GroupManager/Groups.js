import React from 'react';
import { connect } from 'react-redux';
import Subheader from '../../layout/Subheader';
import { Nav, Container, InputGroup, Form, Button, Col } from 'react-bootstrap';
import GroupTables from './GroupTables.js';
import GroupDetail from '../Team/GroupDetail';

const tabMap = {
  "user": "user_created",
  "org": "org",
  "detail": "detail",
  "service": "service"
}
class Groups extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      groups: [],
      activeKey: "user_created",
      searchStr: "",
      searchSubmitted: "",
      selectedGroup: null,
      selectedType: null
    }

    this.onChildClick = this.onChildClick.bind(this);
    this.updateSelectedGroup = this.updateSelectedGroup.bind(this);
    this.nav = React.createRef();
  }

  componentDidMount() {

    if (this.props.location.state !== undefined && this.props.location.state.response !== undefined) {
      let selectedType = this.props.location.state.response[0].type

      this.setState({
        activeKey: "detail",
        selectedType,
        selectedGroup: this.props.location.state.response[0]
      })
    }
    else {
      this.setState({
        activeKey: tabMap[this.props.match.params.tab]
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (this.state.activeKey !== tabMap[this.props.match.params.tab]) {
      this.setState({
        activeKey: tabMap[this.props.match.params.tab]
      })
    }
  }

  renderComponent() {
    const { activeKey, searchSubmitted, selectedGroup, selectedType } = this.state;

    if (activeKey === "user_created") {
      return (
        <GroupTables basePath={this.props.basePath} type="user_created" variant={activeKey} search={searchSubmitted} onClick={this.onChildClick} />
      )
    } else if (activeKey === "org" || activeKey === "service") {
      return (
        <GroupTables basePath={this.props.basePath} type="org" variant={activeKey} search={searchSubmitted} onClick={this.onChildClick} />
      )
    } else {
      return (
        <GroupDetail group={selectedGroup} type={selectedType} onGroupUpdate={this.updateSelectedGroup}/>
      )
    }
  }

  /**
   * Updates the search bar value
   */
  updateInputValue = (event) => {
    const { value } = event.target;

    this.setState({
      searchStr: value
    }, () => {
      if (value === "") {
        this.searchSubmitted();
      }
    });
  }

  /**
   * When the search button is clicked, this function sets the state
   * with the current input so that the input gets passed to the child component
   */
  searchSubmitted() {
    this.setState({
      searchSubmitted: this.state.searchStr
    });
  }

  /**
   * When a link for a group in a child component is clicked,
   * this function gets called with a group and the type of that group.
   * When this state gets set the child component changes to the group detail view
   * @param {object} group 
   * @param {string} type 
   */
  onChildClick(group, type) {
    this.nav.current.props.onSelect(tabMap["detail"]);
    this.setState({
      activeKey: tabMap["detail"],
      selectedGroup: group,
      selectedType: type
    });

  }

  /**
   * When a change to a group is made through the group detail view,
   * this parent component sets the state to the new updated group so that
   * when you return to the group detail tab, you don't see old gropu info
   * @param {object} group 
   */
  updateSelectedGroup(group) {
    this.setState({
      selectedGroup: group
    });
  }

  viewSwitch(tab) {
    this.props.history.push(this.props.basePath + "/groups/" + tab);
    this.nav.current.props.onSelect(tabMap[tab]);
    this.setState({
      activeKey: tabMap[tab]
    })
  }

  render() {
    const { activeKey } = this.state;

    return (
      <div>
        <Subheader title="Groups">
          {activeKey !== "detail" ?
            <Col sm={{ span: 4, offset: 4 }}>
              <InputGroup>
                <Form.Control
                  style={{ width: "30%" }}
                  type="text"
                  placeholder="Search Group"
                  onChange={(evt) => this.updateInputValue(evt)} />
                <InputGroup.Append>
                  <Button onClick={() => this.searchSubmitted()} >
                    Search
                  </Button>
                </InputGroup.Append>
              </InputGroup>
            </Col>
            : ''
          }
        </Subheader>
        <Nav ref={this.nav} variant="tabs" defaultActiveKey="user_created">
          <Nav.Item>
            <Nav.Link eventKey="user_created" onSelect={() => { this.viewSwitch("user") }}>User Created Groups</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="service" onSelect={() => { this.viewSwitch("service") }}>Service Groups</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="org" onSelect={() => { this.viewSwitch("org") }}>Vendor Groups</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="detail" onSelect={() => { this.viewSwitch("detail") }}>Group Detail</Nav.Link>
          </Nav.Item>
        </Nav>
        <br />
        <Container>
          {this.renderComponent()}
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    org: state.auth.org
  }
}

export default connect(mapStateToProps)(Groups);