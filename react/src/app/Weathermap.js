import React from 'react';
import { Button,Card, Row, Col } from 'react-bootstrap';

import Subheader from '../layout/Subheader';
import { Map, TileLayer, Popup, Marker } from 'react-leaflet';
import StormView from '../components/StormView';
import GreenPortlet from '../components/GreenPortlet'
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import { withLoader } from '../components';
import { connect } from 'react-redux';

delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});


const stormType = {
    "TD": "Tropical Depression",
    "TS": "Tropical Storm",
    "H": "Hurricane",
    "TY": "Typhoon"
}

const stormCat = {
    "TD": "Tropical Depression",
    "TS": "Tropical Storm",
    "H1": "Category 1 Hurricane",
    "H2": "Category 2 Hurricane",
    "H3": "Category 3 Hurricane",
    "H4": "Category 4 Hurricane",
    "H5": "Category 5 Hurricane",
    "TY": "Typhoon",
    "STY": "Super Typhoon"
}

const stormBasin = {
    "AL": "Atlantic",
    "EP": "Eastern Pacific",
    "CP": "Central Pacific",
    "WP": "Western Pacific",
    "IO": "Indian Ocean",
    "SH": "Southern Hemisphere"
}

let stormApi = 'https://api.aerisapi.com/tropicalcyclones?';

const accessIdOne = 'SGrBrYJ2LhWOSViPLsdXj';
const secretKeyOne = 'dGgdWkNt4Sc8RFWhYjP2J1FvyKvZB1tHOWO5JUB9';

// const accessIdTwo = 'vCV7x6lobIzS2crUqknzI';
// const secretKeyTwo = '9t9CazO1OYr0sXTzgp1s9N99KVgOaw45559XTZks';

let mapUrl = 'https://maps.aerisapi.com/FKrNzmqvpttAk4QSqRupe_G6qt6IzWWGZ6UeTVZ4ymn2toqWVVVghSqh8iA5Y7/flat-dk,water-depth,admin-cities-dk,tropical-cyclones,tropical-cyclones-names/{z}/{x}/{y}/current.png';
let accessId = accessIdOne;
let secretKey = secretKeyOne



if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    //mapUrl = 'iuihttps://maps.aerisapi.com/zebYv299EcMkH1EqqWCMq_dRyPiJQnCopbb79NgdOKiTnwSmFCr8hsHe422QfN/land-flat-dk:75,water-depth,radar,admin-cities-dk,tropical-cyclones-names,tropical-cyclones/{z}/{x}/{y}/current.png';
    //mapUrl = 'https://maps.aerisapi.com/FKrNzmqvpttAk4QSqRupe_G6qt6IzWWGZ6UeTVZ4ymn2toqWVVVghSqh8iA5Y7/flat-dk,water-depth,admin-cities-dk,tropical-cyclones,tropical-cyclones-names/{z}/{x}/{y}/current.png';
    mapUrl = 'https://maps.aerisapi.com/SGrBrYJ2LhWOSViPLsdXj_dGgdWkNt4Sc8RFWhYjP2J1FvyKvZB1tHOWO5JUB9/flat,fradar,radar,wind-speeds,wind-gusts,stormreports,tropical-cyclones-names,tropical-cyclones-positions,admin,admin-dk,admin-cities/900x600/8.93,17.651,46.01,86.409/20190625114733_20190625114733.png';


    secretKey = secretKeyOne;
}

class WeatherMap extends React.Component {   

    constructor(props) {
        super(props);

        this.state = {
            storms: [],
            buildings: [],
            markers: [],
            zoom: 3,
            lat: 43.505,
            lng: -0.09,
            popup: null,
            buildingPort: null
        }

        this.onClicky = this.onClicky.bind(this);
    }

    async componentDidMount() {
        const { startLoading, stopLoading } = this.props;
        startLoading('Loading your data...');
        let fetchOptions = {
            method: "GET",
            headers: { "content-type": "application/json" }
        };

        let url = stormApi + 'client_id=' + accessId + '&client_secret=' + secretKey;
        console.log('url= %s', url);

        let resp = await fetch(url, fetchOptions);
        let json = await resp.json();

        this.parseResponse(json);

        this.getBuildings();
        stopLoading();
    }

    /**
     * Parses the reponse of the db query object
     * @param {*} json 
     */
    parseResponse(json) {
        let stormList = [];
        let key = 0;
        for (let storm of json.response) {
            let obj = {
                "key": key,
                "name": storm.position.details.stormName,
                "basin": stormBasin[storm.profile.basinCurrent],
                "stormType": stormType[storm.position.details.stormType],
                "stormCat": stormCat[storm.position.details.stormCat],
                "lat": storm.position.loc.lat,
                "lng": storm.position.loc.long,
                "movement": storm.position.details.movement.direction,
                "movementSpeedKPH": storm.position.details.movement.speedKPH,
                "movementSpeedMPH": storm.position.details.movement.speedMPH,
                "windSpeedKPH": storm.position.details.windSpeedKPH,
                "windSpeedMPH": storm.position.details.windSpeedMPH,
                "maxWindSpeedKPH": storm.profile.windSpeed.maxKPH,
                "maxWindSpeedMPH": storm.profile.windSpeed.maxMPH,
            }
            stormList.push(obj);
            key++;
        }
        console.log(stormList);

        this.setState({ storms: stormList });

    }

    /**
     * Queries db for all buildings
     */
    getBuildings() {
      let {client} = this.props;
      client.queryBuildings()
      .then(async res => {
        console.log(res);
        this.setState({
          buildings: res
        });
      }).catch(err => console.log(err));
    }


    /**
     * Runs through all buildings and calculates the distance between them and the storm.
     * Adds them to a list if the building is within 250 km of the storm
     * @param {*} storm 
     */
    findThreat(storm) {
        let inDanger = [];
        let buildings = this.state.buildings;
        for (let building of buildings) {
            let dist = this.calculateDistance(storm, building);
            if (dist <= 250) {
                inDanger.push(building);
            }
        }

        return inDanger;
    }


    /**
     * Distance calculation in km
     * @param {*} storm 
     * @param {*} building 
     */
    calculateDistance(storm, building) {
        let lat1 = storm.lat;
        let lat2 = building.lat;
        let lng1 = storm.lng;
        let lng2 = building.lng;
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLng = this.deg2rad(lng2 - lng1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLng / 2) * Math.sin(dLng / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }


    /**
     * Handles the clicks for the "Buildings" button in the popups on the map. 
     * Creates a list of markers to display on the map and the Building Codes Portlet above the storms
     * @param {*} buildingCodes 
     * @param {*} buildings 
     */
    handleClick(buildingCodes, buildings) {
        let item = {
            name: "buildingList",
            content: buildingCodes
        }

        let markerList = []
        for (let building of buildings) {
            markerList.push(building);
        }

        this.setState({
            popup: null,
            buildingPort: item,
            markers: markerList
        })
    }

    
    /**
     * hanldes the button clicks on the colored portlets underneath the map
     * @param {} name 
     */
    onClicky(name) {
        this.setState({buildings: []});
        if (name === "Green Close") {
            this.setState({
                buildingPort: null,
                markers: []
            });
        } else {
            let storms = this.state.storms;
            for (let storm of storms) {
                if (storm.name === name) {
                    let inDanger = this.findThreat(storm);
                    let codes = [];
                    for (let obj of inDanger) {
                        let container_id = obj.container_id;
                        let indexOne = container_id.indexOf('&');
                        let str = " " + container_id.substring(indexOne + 1, container_id.length) + ",";
                        codes.push(str)
                    }
                    window.scrollTo(0, 0);
                    this.setState({
                        lat: storm.lat,
                        lng: storm.lng,
                        zoom: 6,
                        popup: {
                            key: 0,
                            position: {
                                lat: storm.lat,
                                lng: storm.lng
                            },
                            content: "Buildings possibly affected: " + inDanger.length,
                            buildings: inDanger,
                            buildingCodes: codes
                        },
                        buildingPort: null,
                        markers: []
                    });
                }
            }
        }
    }

    renderMap() {
        let position = [this.state.lat, this.state.lng];
        console.log('rendering map');
        return (
            <Map style={{height: '600px' }}  zoom={this.state.zoom} popup={this.state.popup} center={position}>
                <TileLayer
                    url={mapUrl}
                />
                {this.state.markers.length > 0 && this.state.markers.map((building, idx) =>
                    <Marker key={`marker-${idx}`} position={[building.lat, building.lng]}>
                        <Popup>
                            <span>{building.container_id}</span>
                        </Popup>
                    </Marker>
                )}
                {this.state.popup &&
                    <Popup
                        key={this.state.popup.key}
                        position={this.state.popup.position} >
                        <div>
                            <p>{this.state.popup.content}</p>
                            {this.state.popup.buildings.length > 0 &&
                                <Button onClick={() => this.handleClick(this.state.popup.buildingCodes, this.state.popup.buildings)} buttonText='Buildings' />}
                        </div>
                    </Popup>
                }
            </Map>
        )
    }

    render() {
        return (
            <>
                <Subheader title="Weather Tracker" />
                <Card>
                    {this.renderMap()}
                </Card>
                <Row>
                    {this.state.buildingPort &&
                        <GreenPortlet header={"Building Codes"} content={this.state.buildingPort.content} onClick={() => this.onClicky("Green Close")} buttonText={"Close"} />
                    }
                </Row>
                <Row>
                    {this.state.storms.length > 0 && this.state.storms.map((storm,index)=> {
                        return (
                            <Col key = {index}>
                                <StormView onClick={() => this.onClicky(storm.name)} storm={storm} />
                            </Col>
                        );
                    }, this)}
                </Row>
            </>
        )
    }
}

function mapStateToProps(state) {
  return { client: state.auth.client }
};
  
export default withLoader(connect(mapStateToProps)(WeatherMap));
