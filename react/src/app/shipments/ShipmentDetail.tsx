import { FcFetchClient } from "fc-rest-client";
import moment from "moment-timezone";
import React from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { CSVLink } from "react-csv";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom"
import { Link, RouteComponentProps } from "react-router-dom";
import { ShipmentStatus, Tracking, Shipment } from "tw-api-common";
import { withLoader } from "../../components";
import VerticalModal from "../../components/VerticalModal.js";
import UpdateTrackingForm from "../UpdateTrackingForm";
import ShipmentDetailCard from "./ShipmentDetailCard";
import { ResponseStatus } from "tw-api-common/dist/models/common";
import { shipmentColumns } from "../columns/columns";
const cargoShipEast = require("../../images/cargo-ship1.png");
const cargoShipWest = require("../../images/west-ship1.png");
import { positions, Provider } from "react-alert";
import { useAlert } from "react-alert";
const openAlert = require('simple-react-alert');




const destination = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";

const optionsalert = {
  timeout: 5000,
  position: positions.BOTTOM_CENTER
};


interface IProps extends RouteComponentProps<IMatchParams> {
  basePath: string;
  client: FcFetchClient;
  org: any;
  user: any;

  startLoading: any;
  stopLoading: any;
}

interface IMatchParams {
  shipmentId: string;
  shipmentIdnew :string
}



// GMT timezone
moment.tz.setDefault("Europe/London");

class ShipmentDetail extends React.Component<IProps, any> {
  public _isMounted: boolean;

  constructor(props: any) {
    super(props);

    this.state = {
      data: [],
      oneShipment: [],
      distinctPOs: [],
      csvData: [],
      markers: [],
      carriers: [],
      containerList: [],
      trackings: [],

      buildings: {},
      vessels: {},

      percentage: 0,
      submitted: false, // used for submit button on search bar
      submitting: false, // used for the pop up window when submitting an action
      results: true,
      arrived: false,
      leftYet: true,
      searching: false,
      showPopUpTracking: false,
      showExpediteButton: false,
      noSelection: false,
      showPopUp: false,
      matchingEta: true,
      trackingInfoRetrieved: false, // used for render method to wait until tracking call is finished
      containersRetrieved: false,
      search: "",
      destContainer: "",
      destName: "",
      errorMessage: "",
      successMessage: "",
      newTrackingRef: "",
      newTrackingCarrier: "",
      newTrackingType: "bol",
    };
    this._isMounted = false;
    // this.submitSearch = this.submitSearch.bind(this);
    this.submitClick = this.submitClick.bind(this);
    this.handleActionClick = this.handleActionClick.bind(this);
    this.cancelClick = this.cancelClick.bind(this);
    this.getTrackingInformation = this.getTrackingInformation.bind(this);
    this.markShipmentClosed = this.markShipmentClosed.bind(this);
    this.handleClick_expedite = this.handleClick_expedite.bind(this);
    this.showPopUpTracking = this.showPopUpTracking.bind(this);
    this.updateTracking = this.updateTracking.bind(this);
    this.removeTracking = this.removeTracking.bind(this);
  }

  public componentDidMount() {
    const shipmentId = this.props.match.params.shipmentId ? this.props.match.params.shipmentId : "";
    const shipmentIdnew = this.props.match.params.shipmentIdnew ? this.props.match.params.shipmentIdnew : "";
    console.log("Shipment ID is"+shipmentIdnew);
    this.getOneShipment(shipmentId);
  }

  /**
   * Function goes through the rows collected from the DB and filters out
   * the rows with shipment ID's matching the argument and sorts list by
   * purchase order id
   * @param {string} shipmentId
   */
  public async getOneShipment(shipmentId: string) {
    const { startLoading, stopLoading, user } = this.props;
    startLoading("Loading your data...");
    const oneShipment = await this.props.client.queryPOsOnShipment(user.org_id, shipmentId);
    const res = await this.props.client.shipmentsClient.info(shipmentId);

    console.log("SHIPMENT")
    console.log(res)

    if (res.status === ResponseStatus.OK && res.shipment) {
      this.setState({
        destContainer: oneShipment.destination_container_id,
        destName: oneShipment[0].dest_name,
        matchingEta: oneShipment[0].original_eta === oneShipment[0].eta,
      });

      // adding a check to keep the app from erring when vessel container id is null
      if (oneShipment[0].vessel_container_id) {
        await this.getContainersForShipment(oneShipment[0].vessel_container_id);
      } else {
        this.setState({
          containersRetrieved: true,
        });
      }

      await this.getPoStuff(oneShipment);
      await this.getTrackingInformation(res.shipment);
    } else {
      this.setState({
        noSelection: true,
        results: false,
        search: shipmentId,
      });
    }
    stopLoading();
  }

  public renderCargoShip(currentLat: number, destLat: number) {
    let ship;
    destLat > currentLat ? ship = cargoShipWest : ship = cargoShipEast;
    return ship;
  }

  /**
   * Makes a call to the DB to find the shipping containers on the vessel to expedite.
   * @param {string} shipId
   */
  public async getContainersForShipment(shipId: string) {
    const ItemsOnShip =
      await this.props.client.shipmentsClient.getContainersForShipment(this.props.match.params.shipmentId);

    this.setState({
      containerList: ItemsOnShip.containers,
      showExpediteButton: ItemsOnShip.length > 0,
      containersRetrieved: true,
    });
  }

  /**
   * Function accepts an array containing rows from the DB pertaining to a single shipment.
   * Function needs create a structure containing list of disitinct POs on shipment and the items
   * those POs contain. Sometimes a shipment will have the PO info in the shipments items rows, so before
   * sorting, function makes decision whether to gather info from purchase order items or shipment items.
   * Function also organizes data to allow for CSV export
   * @param {array} oneShipment
   */
  public getPoStuff(oneShipment: any) {
    const { basePath } = this.props;
    const distinctPOs = [];
    const csvTable = [["Shipment ID","lot", "ETA", "ATA", "POL", "POD", "Purchase Order ID", "Purchase Order Line Item", "Item Name", "SKU", "Quantity"]];
    let currentPO = oneShipment[0].purchase_order_id;

    const poColumn = "purchase_order_id";
    const poIDColumn = "purchase_order_item_id";
    const skuColumn = "sku";
    const quantityColumn = "quantity";

    let polineInfo = [];
    let distinctPOLines = [];
    for (const row of oneShipment) {
      if (row[poColumn] !== currentPO) {
        distinctPOs.push({
          po: currentPO,
          items: polineInfo,
        });
        currentPO = row[poColumn];
        polineInfo = [];
        distinctPOLines = [];
      }
      if (distinctPOLines.indexOf(row[poIDColumn]) < 0) {
        distinctPOLines.push(row[poIDColumn]);
        polineInfo.push({
          poline: row[poIDColumn],
          sku: <Link to={basePath + "/items/detail/" + row[skuColumn]}>{row[skuColumn]}</Link>,
          quantity: row[quantityColumn],
          prod_name: row.prod_name,
          lot:row.lot,
        });

        csvTable.push([
          row.shipment_id,
          "8989",
          row.eta ? moment(row.eta).format("MMMM Do YYYY") : "-",
          row.ata ? moment(row.ata).format("MMMM Do YYYY") : "-",
          row.source_name,
          row.dest_name,
          row[poColumn],
          row[poIDColumn],
          row[skuColumn],
          row.prod_name,
          row[quantityColumn],
        ]);
      }
    }
    distinctPOs.push({ po: currentPO, items: polineInfo });
    this.setState({
      oneShipment,
      distinctPOs,
      csvData: csvTable,
      csvLink: <CSVLink data={csvTable} filename={"Shipment_" + oneShipment[0].shipment_id + "_data.csv"}><Button variant="outline-primary">Export Shipment Data</Button></CSVLink>,
    }, () => this.getProgress());
  }

  public async getVesselsAndBuildings(tr: Tracking) {

    const { buildings, vessels } = this.state;
    const { client } = this.props as {client: FcFetchClient};
    for (const e of tr.events) {
      console.log(e)

      if (!buildings[e.locationContainerId]) {
        const bResp = await client.containersClient.getBuilding(e.locationContainerId);
        console.log(`got building for ${e.locationContainerId}`)
        console.log(bResp);
        buildings[e.locationContainerId] = bResp.status === ResponseStatus.OK ? bResp.building : null;
      }

      if (!vessels[e.transportContainerId]) {
        const vResp = await client.vesselsClient.info(e.transportContainerId);
        console.log(`got vessel for ${e.transportContainerId}`)
        //console.log(vResp);
        var veselname_text = `{"name":"${e.transportContainerId}"}`;
        var result = JSON.parse(veselname_text);
        //console.log("Un processed vessel info is"+ju);
        vessels[e.transportContainerId] = vResp.status === ResponseStatus.OK ? vResp.vessel : result;
      } 

    }

    console.log("BUILDINGS AND VESSELS:");
    console.log(vessels);
    console.log(buildings);
  }

  public async getTrackingInformation(shipment: Shipment) {

    const trackings = shipment.trackings;

    console.log("TRACKINGS")
    console.log(shipment)

    // Only handles 1
    for (const t of trackings) {
      await this.getVesselsAndBuildings(t);
    }

    this.setState({
      trackings,
      trackingInfoRetrieved: true,
    });

  }

  public async removeTracking(e: any) {
    e.preventDefault();
    //const shipClient = await this.props.client.getShipmentsClient();
    //await shipClient.removeTracking(this.state.oneShipment[0].tracking_id);
    //this.getOneShipment(this.state.oneShipment[0].shipment_id);
  }

  /**
   * Estimates the amount of progress the shipment has made based on the time it was supposed to have left,
   * the current date and the time it is supposed to arrive.
   */
  public getProgress() {
    const today = moment();
    const total = moment(this.state.oneShipment[0].eta).diff(moment(this.state.oneShipment[0].atd), "days");
    const daysSinceAtd = today.diff(moment(this.state.oneShipment[0].atd), "days");
    const percentage = daysSinceAtd / total * 100;
    
    // console.log("########################");
    
    // console.log("ETA ##: "+this.state.oneShipment[0].eta);
    // console.log("ATD ##: "+this.state.oneShipment[0].atd);

    // console.log("Days ##: "+daysSinceAtd);
    // console.log("Total ##: "+total);

    // console.log("Percentage is"+percentage);

    this.setState({
      percentage,
      leftYet: moment(this.state.oneShipment[0].atd),
    }, () => this.getMarkers());
  }

  /**
   * Creates a list of markers for GMap based on whether the shipment has arrived or not.
   * If not arrived, create two markers, one for the vessel and one for the destination. If arrived,
   * only create one marker, the vessel at the point of destination.
   */
  public getMarkers() {
    const list = this.state.oneShipment;
    let arrived = false;

    if (list[0].ata) {
      arrived = true;
    }

    const markers = [];

    if (arrived) {
      markers.push({
        id: 0,
        name: list[0].vessel_name,
        lat: list[0].dest_lat,
        lng: list[0].dest_lng,
        color: this.renderCargoShip(0, 0),
        onMouseOver: () => { },
        onClick: () => { },
      });
    } else if ((list[0].lat === null || list[0].lng === null)) {
      markers.push({
        id: 0,
        name: list[0].vessel_name,
        "lat": list[0].dest_lat,
        lng: list[0].dest_lng,
        color: destination,
        onMouseOver: () => { },
        onClick: () => { },
      });
    } else {
      markers.push({
        id: 0,
        name: list[0].vessel_name,
        "lat": list[0].lat,
        lng: list[0].lng,
        color: this.renderCargoShip(list[0].lat, list[0].dest_lat),
        onMouseOver: () => { },
        onClick: () => { },
      },
        {
          id: 1,
          name: list[0].dest_name,
          "lat": list[0].dest_lat,
          lng: list[0].dest_lng,
          color: destination,
          onMouseOver: () => { },
          onClick: () => { },
        });
    }

    this.setState({
      arrived,
      markers,
    });
  }

  /**
   * Handle the search bar's input change.
   */
  public handleChange = (event: any) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
      results: true,
    });
  }

  /**
   * Handles the Expedite button click
   * @param {string} str
   */
  public handleActionClick(str: any) {
    if (str === "exp") {
      this.setState({
        showPopUp: true,
      });
    } else {
      this.setState({
        holdWindow: true,
      });
    }
  }

  /**
   * Handles the submission of the expedite request in the pop up window
   * @param {string} container_id
   * @param {string} action
   */
  public async submitClick(container_id: any, action: any) {
    this.setState({
      submitting: true,
    });

    const org = this.props.org.org_id;
    const newRequest = await this.props.client.createAction("shipping-container&" + container_id, action, org);
    if (!newRequest.errorMessage) {
      this.setState({
        successMessage: "Request successfully submitted",
        errorMessage: "",
        submitting: false,
      });
      setTimeout(() => {
        this.setState({
          successMessage: "",
          showPopUp: false,
          holdWindow: false,
        });
      }, 1500);
    } else {
      this.setState({
        errorMessage: "Open request already exists on this container",
        successMessage: "",
        submitting: false,
      });
    }
  }
  public async markShipmentClosed(e: any) {
    e.preventDefault();
    const shipClient = this.props.client.shipmentsClient;
    // Query this to get the pure shipm
    const res = await shipClient.info(this.state.oneShipment[0].shipment_id);

    // do update
    if (res.status === ResponseStatus.OK && res.shipment) {
      res.shipment.status = ShipmentStatus.CLOSED;
      await shipClient.update(res.shipment);
      window.location.reload();
    }
  }

  public handleClick_expedite() {
    //const alert = useAlert();

    //openAlert({ message: 'Your changes have succesfully saved', type: 'success' });
    //useAlert().show("Jahjhja");
    alert("Expedite Request Initiated");
    // this.container.success(
    //   <strong>I am a strong title</strong>,
    //   <em>I am an emphasized message</em>
    // );
  }

  /**
   * Handles the cancel button in the pop up window
   */
  public cancelClick() {
    this.setState({
      showPopUp: false,
      holdWindow: false,
      errorMessage: "",
    });
  }

  public componentWillUnmount() {
    this._isMounted = false;
  }
  public async updateTracking(e: any) {
    e.preventDefault();
    const shipClient = await this.props.client.getShipmentsClient();
    // Query this to get the pure shipm
    //const newTrackingObj = { shipment: this.state.oneShipment[0], ref: this.state.newTrackingRef, carrierId: this.state.newTrackingCarrier, type: this.state.newTrackingType, trackingId: this.state.oneShipment[0].tracking_id };
    //await shipClient.updateTracking(newTrackingObj);
    //this.showPopUpTracking();
  }
  public async showPopUpTracking() {
    if (this.state.showPopUpTracking == false) {
      const carriers = await this.props.client.carriersClient.list();
      this.setState({
        carriers,
        newTrackingCarrier: carriers[0].carrierId,
        showPopUpTracking: true,
      });
    } else {
      this.setState({
        showPopUpTracking: false,
      });
    }
  }
  public render() {
    const { oneShipment, distinctPOs, percentage, arrived, leftYet,vessels,buildings,
            csvLink, noSelection, showExpediteButton, containerList, trackings, 
            matchingEta, trackingInfoRetrieved, containersRetrieved } = this.state;
    const ata = arrived ? moment(oneShipment[0].ata).format("MMMM Do YYYY HH:mm") : null;
    const etd = leftYet ? null : oneShipment[0].etd ? moment(oneShipment[0].etd).format("MMMM Do YYYY") : "Unknown ETD";

    const modalClose = () => this.setState({ showPopUp: false });

    console.log(oneShipment);
    console.log(this.state);
    let containerr;

    return (

      <div>
        <VerticalModal show={this.state.showPopUpTracking} title={"Update Tracking Info"} onHide={this.showPopUpTracking} size={"lg"}
          page={<>
            <UpdateTrackingForm carriers={this.state.carriers} submit={this.updateTracking} handleChange={this.handleChange} formValues={{ Reference: this.state.newTrackingRef, Carrier: this.state.newTrackingCarrier, Type: this.state.newTrackingType }} />
          </>} />
          
        <Row>
          <Col sm={{ span: 4 }}>
          </Col>
          <Col>
            {(showExpediteButton && !arrived) && <Button variant="success" onClick={() => this.handleActionClick("exp")}>Expedite Container on this Shipment</Button>}
          </Col>
        </Row>

        <br />
        {(oneShipment.length > 0) && (trackingInfoRetrieved) && (this.state.markers.length > 0) && (containersRetrieved) &&
          <ShipmentDetailCard
            eta={arrived ? "Arrived" : oneShipment[0].etaFormat}
            polist={distinctPOs}
            percentage={arrived ? 100 : (percentage >= 100 && !arrived) ? 90 : percentage}
            marker={this.state.markers}
            animated={!arrived}
            ata={arrived ? ata : null}
            etd={leftYet ? null : etd}
            containers={containerList}
            displayAddTracking={this.showPopUpTracking}
            removeTracking={this.removeTracking}
            closeShip={this.markShipmentClosed}
            handleClick_expedite={this.handleClick_expedite}
            csvLink={csvLink}
            trackings={trackings}
            matchingEta={matchingEta}
            ship={oneShipment[0]}
            buildings={buildings}
            vessels={vessels} />}
        {noSelection &&
          <Container>
            <Card>
              <Card.Header>
                <div><b>Shipment Details</b></div>
              </Card.Header>
              <Card.Body>
                <Card.Text>
                  <div>
                    Search for a shipment using the search bar above or select a shipment from the "Current Shipments" tab
                    or "Past Shipments" tab to see a detailed view of your shipment.
                  </div>
                </Card.Text>
              </Card.Body>
            </Card>
          </Container>
        }
      </div>
    );
  }
}

function mapStateToProps(state: any) {
  return { client: state.auth.client, user: state.auth.user, org: state.auth.org };
}

export default withLoader(withRouter(connect(mapStateToProps)(ShipmentDetail)));
