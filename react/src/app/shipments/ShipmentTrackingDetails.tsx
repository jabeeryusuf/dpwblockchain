import moment from "moment-timezone";
import React from "react";

import { Badge, Col, ListGroup, Row } from "react-bootstrap";
import { FaShip, FaTruckLoading } from "react-icons/fa";
import ReactTable from "react-table";

import { Tracking, TrackingEvent, TrackingStatus, TrackingType } from "tw-api-common";

const trackingColumns = (buildings: any, vessels: any) => ([
    {
        Header: "Date",
        id: "date",

        accessor: (d: TrackingEvent) => d.date.toLocaleString(),
    },
    {
        Header: "Status",
        id: "status",

        accessor: (d: TrackingEvent) => d.status
    },


    {
        Header: "Location",
        id: "locationContainerId",

        accessor: (d: TrackingEvent) =>
        {

            
                var location  = d.locationContainerId && buildings[d.locationContainerId] ? buildings[d.locationContainerId].name : "N/A"
            
                if(location=="N/A" && d.locationContainerId)
                {
                    return d.locationContainerId.replace("port&", "Port of ");
                }
                else
                return location;

            

        }

           
    },
    {
        Header: "Vessel",
        id: "vesselId",

        accessor: (d: TrackingEvent) =>
            (d.transportContainerId && vessels[d.transportContainerId]
                ? vessels[d.transportContainerId].name : "N/A")
    },
    {
        Header: "Voyage",
        id: "voyageId",

        accessor: (d: TrackingEvent) => (d.voyageId ? d.voyageId : "N/A")
    },
]);

const trackingStatusToMessage: any = {
    "failure-no-scraper":
        "We are currently not integrated with this carrier and unable to display progress of this shipment.",
    "failure-no-results":
        "We are unable to track this shipment. Please ensure the information that you entered is correct.",
    "created":
        "We have tracking information for this shipment and will be updating the status shortly. Check back later for more details!",

};

const ShipmentTrackingDetails = (props: { trackingInfo: Tracking, buildings: any, vessels: any, ata: any, ship: any }) => {
    console.log(props.trackingInfo);
    return (
        <>


            <ListGroup.Item className="justify-content-between"  >
                <Row className="align-items-center">
                    {props.trackingInfo.type === TrackingType.BOL_CONTAINER ?
                        <>
                            <Col><Badge><h5><FaTruckLoading /></h5></Badge> In the Port</Col>
                            <Col><b>Port:</b> {props.trackingInfo.ref}</Col>
                        </>
                        :

                        <>
                            <Col><Badge><h5><FaShip /></h5></Badge>  {props.ata != null ? <> Arrived  </> : props.ship.shipment_status == "in-transit" ? <> In-Transit  </> : <> Not Started  </>} </Col>
                            <Col><b>Carrier:</b> {props.trackingInfo.carrierId}</Col>
                            <Col><b>Tracked By:</b> {props.trackingInfo.type}</Col>
                            <Col><b>Tracking Ref:</b> {props.trackingInfo.ref}</Col>
                        </>
                    }

                </Row>
            </ListGroup.Item>

            {console.log("Evntes are##########################")}
            {console.log(props.trackingInfo.events)}

            {props.trackingInfo.status === TrackingStatus.PAUSED
                ?
                <ListGroup.Item variant="dark">
                    {trackingStatusToMessage[props.trackingInfo.status]}
                </ListGroup.Item>
                :

                <ReactTable data={props.trackingInfo.events}
                    showPagination={false}
                    columns={trackingColumns(props.buildings, props.vessels)}
                    defaultPageSize={props.trackingInfo.events.length}
                    getTrProps={(state: any, rowInfo: any, column: any) => {
                        return {
                            style: {
                                // this logic needs to be reworked. It's too primitive and incorrect at times
                                background: rowInfo ? moment(rowInfo.row.date) < moment() ? "#d8d8d8" : null : null,
                            },
                        };
                    }}
                />
            }
        </>
    );
};

export default ShipmentTrackingDetails;
