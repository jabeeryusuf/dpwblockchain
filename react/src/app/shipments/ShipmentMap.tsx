import _ from "lodash";
import React from "react";
import { Button, ButtonGroup, Card, Col, Form, InputGroup, Nav, Row} from "react-bootstrap";
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from "react-google-maps";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { MyMapComponent } from "../../components/ReactGMap";
import findCenter from "../../utils/findCenter";

const colorMap = {
  Factory: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
  Port: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
  Warehouse: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
};

class ShipmentMap extends React.Component<any>  {
  public state = {
    center: {},
    isMarkerShown: false,
    markers: [],
    openWindow: null,
    paths : [],
  };

  public async componentDidMount() {
    let list: {[k: string]: any} = {};
    list = await await this.props.client.getStuff(`SELECT sh.*, buildings.* from
                              (SELECT * FROM shipments
                                WHERE shipments.org_id ='` + this.props.user.org_id + `'
                                           ) sh
                              join buildings on sh.source_container_id = buildings.container_id or
                                                sh.destination_container_id = buildings.container_id
                              WHERE sh.org_id = '` + this.props.user.org_id + `'
    `) ;

    const markerList = [] as any;
    const paths = [] as any;
    for (let i = 0; i < list.length; i++) {
      const container = list[i].container_id;
      if (container.includes("factory")) {
        list[i].color = colorMap.Factory;
      } else if (container.includes("warehouse")) {
        list[i].color = colorMap.Warehouse;
      } else {
        list[i].color = colorMap.Port;
      }

      // const container2 = list[i].container_id2;
      // if (container2.includes("factory")) {
      //   list[i].color2 = colorMap.Factory;
      // } else if (container2.includes("warehouse")) {
      //   list[i].color2 = colorMap.Warehouse;
      // } else {
      //   list[i].color2 = colorMap.Port;
      // }

      list[i].id = i;
      list[i].code = list[i].container_id.substring(list[i].container_id.indexOf("&") + 1, list[i].container_id.length);
      list[i].onMouseOver = () => this.handleMarkerHover(i);
      list[i].onClick = () => this.handleMarkerClick(list[i]);
      list[i].onClose = () => this.handleMarkerClose();
      //const Obj1: {[k: string]: any} = {};
      //Obj1.lat = list[i].lat;
      //Obj1.lng = list[i].lng;
      //const Obj2: {[k: string]: any} = {};
      //Obj2.lat = list[i].lat2;
      // Obj2.lng = list[i].lng2;
      //const pathArray = [] as any;
      //pathArray.push(Obj1);
      // pathArray.push(Obj2);
      //paths.push(pathArray);
      markerList.push(list[i]);
    }

    console.log("groups by", _.values(_.groupBy(list, "shipment_id")));
    const data = _.values(_.groupBy(list, "shipment_id"));

    data.forEach((element) => {
      const pathArray = [] as any;
      const holdValue: {[k: string]: any} = {};
      let flag: boolean = false;
      element.forEach((e) => {
        const Obj: {[k: string]: any} = {};
        if (e.container_id === "port&unknown-dest" || e.container_id === "port&unknown-source") {
          flag = true;
          return;
        } else if (e.container_id === e.source_container_id) {
          Obj.lat = e.lat;
          Obj.lng = e.lng;
          pathArray.push(Obj);
        } else {
          holdValue.lat = e.lat;
          holdValue.lng = e.lng;
        }
      });
      if (!flag) {
        pathArray.push(holdValue);
        paths.push(pathArray);
      }
    });

    console.log("listtttttt", list);
    console.log("pathssssss", paths);
    this.setState({
      center: findCenter(list),
      markers: list,
      paths,
    });

  }

  public  handleMarkerHover(i: any) {
    return i;
  }

  public handleMarkerClick(item: any) {
    this.setState({
      center: {lat: item.lat, lng: item.lng},
      openWindow: item.id,
      tableData: [item],
    });
  }

  public handleMarkerClose() {
    this.setState({
      center: findCenter(this.state.markers),
      tableData: this.state.markers,
    });
  }

  public render() {
    console.log("statee", this.state);
    return (
      <>
        <MyMapComponent
        isMarkerShown={this.state.isMarkerShown}
        onMarkerClick={this.handleMarkerClick}
        markers={this.state.markers}
        openWindow={this.state.openWindow}
        paths = {this.state.paths}
        center = {this.state.center}
      />
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    user: state.auth.user,
  };
}

export default connect(mapStateToProps)(ShipmentMap);
 

/* import _ from "lodash";
import React from "react";
import { connect } from "react-redux";
import { MyMapComponent } from "../../components/temp";

const colorMap = {
  Factory: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
  Port: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
  Warehouse: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
};

class ShipmentMap extends React.Component<any>  {
  public state = {
    isMarkerShown: false,
    markers: [],
    openWindow: null,
    paths : [],
  };

  public async componentDidMount() {
    let list: {[k: string]: any} = {};
    list = await await this.props.client.getStuff(`SELECT sh.*, buildings.* from
                              (SELECT * FROM shipments
                                WHERE shipments.org_id ='` + this.props.user.org_id + `'
                                           ) sh
                              join buildings on sh.source_container_id = buildings.container_id or
                                                sh.destination_container_id = buildings.container_id
                              WHERE sh.org_id = '` + this.props.user.org_id + `'
    `) ;

    const markerList = [] as any;
    const paths = [] as any;
    for (let i = 0; i < list.length; i++) {
      const container = list[i].container_id;
      if (container.includes("factory")) {
        list[i].color = colorMap.Factory;
      } else if (container.includes("warehouse")) {
        list[i].color = colorMap.Warehouse;
      } else {
        list[i].color = colorMap.Port;
      }

      // const container2 = list[i].container_id2;
      // if (container2.includes("factory")) {
      //   list[i].color2 = colorMap.Factory;
      // } else if (container2.includes("warehouse")) {
      //   list[i].color2 = colorMap.Warehouse;
      // } else {
      //   list[i].color2 = colorMap.Port;
      // }

      list[i].id = i;
      list[i].code = list[i].container_id.substring(list[i].container_id.indexOf("&") + 1, list[i].container_id.length);
      list[i].onMouseOver = () => this.handleMarkerHover(i);
      list[i].onClick = () => this.handleMarkerClick(list[i]);
      list[i].onClose = () => this.handleMarkerClose();
      //const Obj1: {[k: string]: any} = {};
      //Obj1.lat = list[i].lat;
      //Obj1.lng = list[i].lng;
      //const Obj2: {[k: string]: any} = {};
      //Obj2.lat = list[i].lat2;
      // Obj2.lng = list[i].lng2;
      //const pathArray = [] as any;
      //pathArray.push(Obj1);
      // pathArray.push(Obj2);
      //paths.push(pathArray);
      markerList.push(list[i]);
    }

    console.log("groups by", _.values(_.groupBy(list, "shipment_id")));
    const data = _.values(_.groupBy(list, "shipment_id"));

    data.forEach((element) => {
      const pathArray = [] as any;
      const holdValue: {[k: string]: any} = {};
      let flag: boolean = false;
      element.forEach((e) => {
        const Obj: {[k: string]: any} = {};
        if (e.container_id === "port&unknown-dest" || e.container_id === "port&unknown-source") {
          flag = true;
          return;
        } else if (e.container_id === e.source_container_id) {
          Obj.lat = e.lat;
          Obj.lng = e.lng;
          pathArray.push(Obj);
        } else {
          holdValue.lat = e.lat;
          holdValue.lng = e.lng;
        }
      });
      if (!flag) {
        pathArray.push(holdValue);
        paths.push(pathArray);
      }
    });

    console.log("listtttttt", list);
    console.log("pathssssss", paths);
    this.setState({
      markers: list,
      paths,
    });

  }

  public  handleMarkerHover(i: any) {
    return i;
  }

  public handleMarkerClick(item: any) {
    this.setState({
      openWindow: item.id,
      tableData: [item],
    });
  }

  public handleMarkerClose() {
    this.setState({
      tableData: this.state.markers,
    });
  }

  public render() {
    return(
      <>
        <MyMapComponent
        isMarkerShown={this.state.isMarkerShown}
        onMarkerClick={this.handleMarkerClick}
        markers={this.state.markers}
        openWindow={this.state.openWindow}
        paths = {this.state.paths}
        />
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    user: state.auth.user,
  };
}

export default connect(mapStateToProps)(ShipmentMap); */
