//
import React from "react";


import ShipmentsList from "./ShipmentsList"
import { Landing, ILandingTab } from "../../layout/Landing";

const tabs: ILandingTab[] = [
  { label: "All Shipments",  path: "/all", component: ShipmentsList },
  
];

export const ShipmentsLanding = (props: any) => <Landing {...props} tabs={tabs} />;
