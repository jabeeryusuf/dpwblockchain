import * as React from 'react'


const ShipmentDetailItem = (props) => {
    return (
        <div>
            <ul style={{ padding: 0, listStyle: 'none', fontSize: 18 }}>
                {props.current.location && 
                    <li> Location: {props.current.location}</li>
                }
                {props.current.arrivalTime &&
                    <li> Arrival Time: {props.current.arrivalTime}</li>
                }
                {props.current.eta &&
                    <li> Estimated time of arrival: {props.current.eta}</li>
                }
                {props.current.etd &&
                    <li> Estimated time of departure: {props.current.etd}</li>
                }
                {props.current.vesselDeparture &&
                    <li> Actual time of depature: {props.current.vesselDeparture}</li>
                }
                {props.current.lat && 
                    <li> Lat: {props.current.lat}</li>
                }
                {props.current.lng && 
                    <li> Lng: {props.current.lng}</li>
                }
                {props.current.speed && 
                    <li> Speed:{props.current.speed} </li>
                }
                {props.current.course &&
                    <li> Course: {props.current.course}</li>
                }
                {props.current.timestamp && 
                    <li> Last Updated: {props.current.timestamp} </li>
                }
            </ul>
        </div >
    )
}
export default ShipmentDetailItem;