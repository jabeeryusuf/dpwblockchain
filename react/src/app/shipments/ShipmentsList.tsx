import React from "react";
import ReactDOM from "react-dom";

import { Button, ButtonGroup, ButtonToolbar, Dropdown, DropdownButton, Form, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import ReactTable from "react-table";
import { withLoader } from "../../components";
import { currentShipColumns } from "../columns/columns";
import { allSKUColumns } from "../items/ItemDetailColumns";
import VerticalModal from "../../components/VerticalModal.js";
import UpdateCarrierForm from "../UpdateCarrierForm";

const jswithLoader: any = withLoader;
const jswithRouter: any = withRouter;
const { CSVLink } = require("react-csv");
const { CSVDownload } = require("react-csv");


interface IShipmentListProps extends RouteComponentProps<IMatchParams> {
    basePath?: string;
    client?: any;
    hide?: boolean;
    org?: any;
    user?: any;
    startLoading?: any;
    stopLoading?: any;
}

const csvHeaders = ((apocs: any[]) => {
    const headers = [];
    for (const ap of apocs) {
        if (ap.accessor !== "tracking") {
            const newObj = {
                key: ap.accessor,
                label: ap.Header,
            };
            headers.push(newObj);
        }
    }
    return headers;
})(currentShipColumns("jji"));

const statusOptions = [
    { label: "All Shipment Statuses", value: "all" },
    { label: "Not arrived", value: "created" },
    { label: "In-transit", value: "in-transit" },
    { label: "Complete", value: "completed" },
];

const filterStates = ["destFilter", "statusFilter"];

const DropDownBar = (fields: any, states: any, onFilterChange: any) => {
    return (
        fields.map((val: any, idx: number) => {
            return (
                <DropdownButton
                    onSelect={(e: any) => { onFilterChange(filterStates[idx], e); }}
                    id="idx"
                    key={idx}
                    title={getLabel(val, states[idx])}
                >
                    {createDdi(val)}
                </DropdownButton>
            );
        })
    );
};
const getLabel = (opts: any, value: any) => {
    const l = opts.find((o: any) => (o.value === value));
    return l ? l.label : "";
};

const createDdi =
    (opts: any) => opts.map((o: any, i: any) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

interface IMatchParams {
    tab: string;
    statusfilter:string;
}

class ShipmentList extends React.Component<IShipmentListProps, any>  {
    private searchDelay: any;
    private inputElement:any;
    private exampleRef:any;
    constructor(props: IShipmentListProps) {
        
        super(props);
        this.exampleRef = React.createRef()

        this.state = {
            destList: [],
            destFilter: "all",
            hide: props.hide,
            searchFilter: "",
            shipmentData: [],
            statusFilter: this.props.match.params.statusfilter ? this.props.match.params.statusfilter : "all",
            tableDate: [],
            pageSize: 20,
            loading: true,
            orderBy: "shipmentRef",
            trackingFilter: "all",
            cursorList: ["0"],
            desc: false,
            page: 0,
            pages: 0,
            tableData_download_all:[],
            showPopUpTracking: false,
            newTrackingRef: "",
            newTrackingCarrier: "MAERSK LINE A/S",
            newTrackingshipmentID: "",
        };
        this.onFilterChange = this.onFilterChange.bind(this);
        this.searchDelay = null;
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSizeChange = this.handlePageSizeChange.bind(this);
        this.handlePageSort = this.handlePageSort.bind(this);
        this.setShipmentData = this.setShipmentData.bind(this);
        this.showPopUpTracking = this.showPopUpTracking.bind(this);
        this.updateTracking = this.updateTracking.bind(this);
        this.someFunc = this.someFunc.bind(this);
    }

    public async componentDidMount() {
        
        const statusfilterval = this.props.match.params.statusfilter ? this.props.match.params.statusfilter : "";
        console.log("Shipment Filter  is "+statusfilterval);

        const { startLoading } = this.props;
        startLoading("Loading your data ...");
        this.LoadAll();
        let allShipments = await this.props.client.shipmentsClient.searchShipments(
            this.props.org.org_id, "shipmentRef", "shipmentRef", { shipmentStatus: this.state.statusFilter }, this.state.searchFilter, 20, "0",
            this.state.desc);
        const pages = allShipments.metadata.pageCount;
        const newCursors = this.state.cursorList.slice();
        newCursors.push(allShipments.metadata.nextCursor);
        allShipments = allShipments.shipments;

        console.log("##################ALL Shipments##############");
        //console.log(allShipments);
        console.log("##################Shipments In Format########");
        allShipments = this.getDataInFormat(allShipments);
        console.log(allShipments);

        const allDestinations = await this.props.client.shipmentsClient.getDestinations(this.props.org.org_id);
        const destLabels = allDestinations.destinations.map((val: any) => ({ label: val.name, value: val.containerId }));
        destLabels.push({ label: "All Destinations", value: "all" });

        this.setState({
            allDestinations: destLabels,
            shipmentData: allShipments,
            tableData: allShipments,
            cursorList: newCursors,
            loading: false,
            pages
        });
        this.props.stopLoading();
    }


    private getDataInFormat(allShipments: any) {
        const { basePath } = this.props;
        const result: any = [];
        for (const sp of allShipments) {
            const SObject = {} as any;            
            SObject.basePath = basePath;
            var jui = Object.assign({}, sp); 
            jui.basePath = basePath;
            result.push(jui);
        }
        return result;
    }


    public async setShipmentData(shipments: any, wait: boolean = false) {
        if (wait) {
            if (this.searchDelay) {
                clearTimeout(this.searchDelay);
            }
            this.searchDelay = setTimeout(() => {
                return;
            }, 500);
        }
        const allShipments = await this.props.client.shipmentsClient.searchShipments(
            this.props.org.org_id, "shipmentRef", this.state.orderBy, {
                destinationContainerId: this.state.destFilter,
                shipmentStatus: this.state.statusFilter,
            },
            this.state.searchFilter,
            this.state.pageSize,
            this.state.cursorList[this.state.page], this.state.desc);
        const newCursors = this.state.cursorList.slice();
        if (this.state.page === this.state.cursorList.length - 1) {
            newCursors.push(allShipments.metadata.nextCursor);
        }
        this.setState({
            cursorList: newCursors,
            tableData: allShipments.shipments,
            pages: allShipments.metadata.pageCount,
            loading: false
        });
    }
    public onFilterChange(name: any, val: any, wait: boolean = false) {
        this.setState({
            [name]: val,
            cursorList: ["0"],
            page: 0
        }, () => { this.setShipmentData(this.state.shipmentData, wait); });
    }
    public handleSearchChange(e: any) {
        this.onFilterChange(e.target.name, e.target.value);
    }

    public async TriggerOther() {
        //this.resetFilters();
        console.log("staring load all");
        await this.LoadAll();
    }

    public async showPopUpTracking() {
        if (this.state.showPopUpTracking == false) {
          //const carriers = await this.props.client.carriersClient.list();
          this.setState({
            showPopUpTracking: true,
          });
        } else {
          this.setState({
            showPopUpTracking: false,
            newTrackingCarrier: "MAERSK LINE A/S",
            newTrackingshipmentID: "",
          });
        }
      }

      public async updateTracking(e: any) {
        e.preventDefault();
        const shipClient = await this.props.client.getShipmentsClient();

        
        var shipmid = this.state.newTrackingshipmentID;
        var carrier = this.state.newTrackingCarrier;

        console.log("Updating Carrier for :"+shipmid+ "- carrier : "+carrier);
        // Query this to get the pure shipm
        //const newTrackingObj = { shipment: this.state.oneShipment[0], ref: this.state.newTrackingRef, carrierId: this.state.newTrackingCarrier, type: this.state.newTrackingType, trackingId: this.state.oneShipment[0].tracking_id };
        await shipClient.updatecarrier(shipmid,carrier);
        this.showPopUpTracking();
      }


      public async someFunc(shipmentID: any) {
        
        this.setState({
            newTrackingshipmentID: shipmentID
          });

        //console.log(stuff);
        this.showPopUpTracking();
        const shipClient = await this.props.client.getShipmentsClient();
        // Query this to get the pure shipm
        //const newTrackingObj = { shipment: this.state.oneShipment[0], ref: this.state.newTrackingRef, carrierId: this.state.newTrackingCarrier, type: this.state.newTrackingType, trackingId: this.state.oneShipment[0].tracking_id };
        //await shipClient.updateTracking(newTrackingObj);
        //this.showPopUpTracking();
      }
    
     public handleChange = (event: any) => {
       const { name, value } = event.target;
   
       console.log(event.currentTarget.value);
       this.setState({
         [name]: value,
         results: true,
         newTrackingCarrier:event.currentTarget.value,
       });
     }

    public async LoadAll() {

        let allShipments_all = await this.props.client.shipmentsClient.searchShipments(
            this.props.org.org_id, "shipmentRef", "shipmentRef", { shipmentStatus: this.state.statusFilter }, this.state.searchFilter, 5, "0",
            this.state.desc);     
        const newCursors = this.state.cursorList.slice();
        newCursors.push(allShipments_all.metadata.nextCursor);
        allShipments_all = allShipments_all.shipments;       
      
        //console.log("##################Filtered All Shipments is In Format########");
        allShipments_all = this.getDataInFormat(allShipments_all);
        //(allShipments_all);

        this.setState({           
            tableData_download_all: allShipments_all         
        }
        , () => {
            //console.log(this.state.tableData_download_all); 
           }
        );
           
        console.log("All Loading completed");

        
        //var rootdiv = ReactDOM.findDOMNode(this.inputElement);
        //var anode = ReactDOM.findDOMNode(this.exampleRef);
        //var rootdiva = ReactDOM.findDOMNode(rootdiv);
        //console.log(rootdiv);
        //this.exampleRef.handleClick();
        //console.log(anode.curreent.state.href);
        //this.inputElement.click();
        //this.exampleRef.click();
        //return true;
    }

    public resetFilters() {
        this.setState({
            destFilter: "all",
            searchFilter: "",
            statusFilter: "all",
            trackingFilter: "all",
            cursorList: ["0"],
            loading: false,
            page: 0
        }, () => { this.setShipmentData([]); });
    }

    public handlePageChange(page: any) {
        this.setState({ page, loading: true }, () => {
            this.setShipmentData([]);
        });
    }
    public handlePageSizeChange(pageSize: any) {
        this.setState({ pageSize }, () => {
            this.setShipmentData([]);
        });
    }
    public handlePageSort(orderBy: any) {
        console.log("Sort by");
        //console.log(orderBy);
        const { id, desc } = orderBy[0];
        this.setState({ loading: true, orderBy: id, desc, cursorList: ["0"], page: 0 }, () => {
            this.setShipmentData([]);
        });
    }
    public render() {
        return (
            <>
            <VerticalModal show={this.state.showPopUpTracking} title={"Update Tracking Info"} onHide={this.showPopUpTracking} size={"lg"}
          page={<>
            <UpdateCarrierForm carriers={this.state.carriers} submit={this.updateTracking} handleChange={this.handleChange} formValues={{ Reference: this.state.newTrackingRef, Carrier: this.state.newTrackingCarrier, Type: this.state.newTrackingType }} />
          </>} />

                <Row>
                    <ButtonToolbar className="toolbar">
                        <ButtonGroup className="filter" >

                            {this.state.allDestinations && DropDownBar([this.state.allDestinations,
                                statusOptions],
                                [this.state.destFilter, this.state.statusFilter, this.state.trackingFilter], this.onFilterChange)
                            }
                            <Form.Control
                                style={{ marginRight: 10, display: "inline-block" }}
                                type="text"
                                name="searchFilter"
                                value={this.state.searchFilter}
                                placeholder="Search Shipments"
                                onChange={(evt: any) => this.handleSearchChange(evt)} />
                        </ButtonGroup>
                        <ButtonGroup className="action" >
                            <Button onClick={() => { this.resetFilters(); }}> Reset Filters</Button>

                            {/* 
                            <Button onClick={() => { this.TriggerOther(); }}> Retrive All</Button>
                           
                            <input  type="button" onClick={() => {
                                    alert("new inout clciked");                                    
                                    
                                  }}></input>
                            {this.state.tableData &&
                          
                                <CSVLink ref={this.exampleRef} data={this.state.tableData} headers={csvHeaders} filename={"shipments.csv"}>
                                    <Button variant="outline-primary">Download Table</Button>
                                </CSVLink>
                            }

                            {this.state.tableData_download_all &&
                                <CSVLink data={this.state.tableData_download_all} 
                                asyncOnClick={true} 
                                onClick={(event:any, done:any) => {
                                    this.LoadAll().then(() => {
                                        console.log("Execution commpleted");
                                        done(); // REQUIRED to invoke the logic of component
                                      });
                                  }}
                                headers={csvHeaders}
                                filename={"shipments_all.csv"}>
                                <Button variant="outline-primary">Download All</Button>
                                </CSVLink>
                            }


                            {this.state.tableData_download_all &&
                            <div  ref={input => this.inputElement = input} onClick={() => {alert("new qwqwqwqw clciked");}}>
                                <CSVLink ref={this.exampleRef} className = "downloadall_link" data={this.state.tableData_download_all} 
                                headers={csvHeaders}
                                filename={"shipments_all.csv"}>
                                <Button variant="outline-primary">Download All</Button>
                                </CSVLink>
                            </div>
                            }                            
                            */}
                            


                            {this.state.tableData &&
                                
                                <CSVLink  data={this.state.tableData} 
                                headers={csvHeaders}
                                filename={"shipments_all.csv"}>
                                <Button variant="outline-primary">Download View</Button>
                                </CSVLink>
                            
                            }

                            {this.state.tableData_download_all &&
                            <div  ref={input => this.inputElement = input}>
                                <CSVLink ref={this.exampleRef} className = "downloadall_link" data={this.state.tableData_download_all} 
                                headers={csvHeaders}
                                filename={"shipments_all.csv"}>
                                <Button variant="outline-primary">Download All</Button>
                                </CSVLink>
                            </div>
                            }   

 


                        </ButtonGroup>
                    </ButtonToolbar>
                </Row>
                <ReactTable
               
                    data={this.state.tableData}
                    columns={currentShipColumns(this.someFunc)}
                    defaultPageSize={20}
                    showPageJump={false}
                    onPageChange={this.handlePageChange}
                    onPageSizeChange={this.handlePageSizeChange}
                    onSortedChange={this.handlePageSort}
                    pages={this.state.pages}
                    page={this.state.page}
                    loading={this.state.loading}
                    manual
                    className="-striped -highlight"
                />
            </>
        );
    }
}

function mapStateToProps(state: any) {
    return {
        client: state.auth.client,
        org: state.auth.org,
        user: state.auth.user,
    };
}
export default jswithRouter(jswithLoader(connect(mapStateToProps)(ShipmentList)));
