import * as React from "react";
import { Badge, Button, Card, Col, ListGroup, ProgressBar, Row } from "react-bootstrap";
import { FaCheckCircle, FaMinusCircle, FaTimesCircle } from "react-icons/fa";
import Accordion from "../../components/Accordion";
import { GMap } from "../../components/GMap";
import ShipmentTrackingDetails from "./ShipmentTrackingDetails";

const destination = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";

const columns = [
  { Header: "Purchase Order Line Item", accessor: "poline" },
  { Header: "LOT", accessor: "lot" },
  { Header: "Item Name", accessor: "prod_name" },
  { Header: "SKU", accessor: "sku" },
  { Header: "Quantity", accessor: "quantity" },
];

const trackingStatusToIcon: any = {
  "failure-no-scraper": <FaTimesCircle color={"red"} />,
  "failure-no-results": <FaTimesCircle color={"red"} />,
  "created": <FaMinusCircle color={"orange"} />,
  "success": <FaCheckCircle color={"green"} />,
};


const ShipmentDetailCard = (props: any) => {
  const { trackingInfo, ...other } = props;



  const portTrackingExample = {
    info: [
      {
        type: "port",
        tracking_ref: "GBSOU",
      },
    ],
    events: [
      {
        date: "2019-03-26 16:46",
        status: "Departed by Road",
        name: "Port of Southampton",
        vessel_name: "Truck",
        voyage: "--",

      },
    ],

  };


  return (
    <>
      <Card border="secondary">
        <Card.Header>

          <Row>
            <Col>
              <b>Shipment Reference ID: </b>{props.ship.shipment_ref}
            </Col>
            <Col>
              <b>Vessel Name: </b>{props.ship.vessel_name}
            </Col>
            <Col>
              <b>Tracking: </b>
              {props.trackings.length
                ? trackingStatusToIcon[props.trackings[0].status]
                : <FaTimesCircle color={"red"} />}

            </Col>
            <Col>
           
           
            <Button  onClick={() => props.handleClick_expedite()} variant="outline-primary">Expedite Shipment</Button>
            </Col>
            <Col>
              <Button onClick={props.closeShip} variant="outline-primary">Mark Shipment Complete</Button>
            </Col>
           
            <Col>
              {props.csvLink}
            </Col>

          </Row>

        </Card.Header>
        <Card.Body>

          <Row className="justify-content-center">
            <Col sm={{ span: 5 }}>
              {
                props.eta === "Arrived" ?
                  <div>Arrived On: <div><b>{props.ata}</b></div></div>
                  :
                  <div>Estimated Time of Arrival: <div className="text-success"><b>{props.eta}</b></div></div>
              }
            </Col>
            <Col sm={{ span: 5, offset: 1 }}>
              {props.etd &&
                <div>Estimated Time of Departure: <div className="text-success"><b>{props.etd}</b></div></div>}
            </Col>
          </Row>

          <br />

          <Row className="justify-content-center">
            <Col xs="2">
              <div>{props.ship.source_name ? props.ship.source_name : "Unknown"}</div>
            </Col>
            <Col xs="4" className="center">
              <ProgressBar animated={props.animated} striped variant={props.percentage < 100 ? "info" : "success"}
                now={props.percentage}></ProgressBar>
            </Col>
            <Col xs="2">
              <div>
                {props.ship.dest_name ? props.ship.dest_name : "Unknown"}
              </div>
              <Badge><img src={destination} alt="Finish" /></Badge>
            </Col>
          </Row>

          <br />
          <br />

        </Card.Body>
      </Card>

      <Card style={{ marginTop: 5 }} border="secondary">
        <Card.Body className="justify-content-center">
          <Card.Title>Shipment Tracking Events</Card.Title>

          {props.trackings.length > 0
            ?
            <>
              {
               props.trackings.map((val: any, i: number) => {
                 return (
                    <ShipmentTrackingDetails key={i} trackingInfo={val} {...other} />
                 );
               })
              }
            </>
            :
            <>
              <Row className="justify-content-center">
                <ListGroup.Item style={{ width: "70%" }} variant="dark">
                  We currently have no tracking information for this shipment.
                  If you would like to track the shipment, please update the tracking information.
                </ListGroup.Item>
              </Row>
              <Row className="justify-content-center">
                <Col>
                  <Button onClick={props.displayAddTracking} variant="outline-primary">
                    Update Shipment Tracking
                        </Button>
                </Col>
                <Col>
                  {(props.oneShipment && props.oneShipment[0].tracking_id) &&
                    <Button onClick={props.removeTracking} variant="outline-primary">Mark Untrackable</Button>
                  }
                </Col>
              </Row>
            </>
          }
        </Card.Body>
      </Card>
      {props.polist.length > 0 &&
        <Card style={{ marginTop: 5 }} border="secondary">
          <Card.Body style={{ maxHeight: 350, overflowY: "auto" }}>
            <Card.Title>List of Purchase Orders on {props.ship.shipment_ref}</Card.Title>
            <Accordion list={props.polist} columns={columns} />
          </Card.Body>
        </Card>
      }

      {props.containers.length > 0 &&
        <Card style={{ marginTop: 5 }} border="secondary">
          <Card.Body>
            <Card.Title>Containers on this Vessel</Card.Title>
            <ListGroup style={{ maxHeight: 350, overflowY: "auto" }}>
              {props.containers.length < 1 ? <ListGroup.Item>Information not yet available</ListGroup.Item>
                :
                props.containers.map((obj: any, idx: any) => {
                  return (
                    <ListGroup.Item key={idx}>{obj.ref}</ListGroup.Item>
                  );
                })}
            </ListGroup>
          </Card.Body>
        </Card>
      }

      <Card style={{ marginTop: 5 }} border="secondary">
        <Card.Body>
          <GMap isMarkerShown
            markers={props.marker}
          />
        </Card.Body>
      </Card>

    </>
  );
};

export default ShipmentDetailCard;
