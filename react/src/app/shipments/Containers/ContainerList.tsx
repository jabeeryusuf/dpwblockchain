import * as _ from "lodash";
import React from "react";
import { Button, ButtonGroup, ButtonToolbar, Dropdown, DropdownButton, Form } from "react-bootstrap";
import { connect } from "react-redux";
import { RouteComponentProps, withRouter } from "react-router-dom";
import ReactTable from "react-table";
import { withLoader } from "../../../components";
import { shipmentItemsColumns, subItemCols } from "./ContainerColumns";
const jswithLoader: any = withLoader;
const jswithRouter: any = withRouter;

interface IContainerListProps extends RouteComponentProps {
  basePath?: string;
  client?: any;
  hide?: boolean;
  org?: any;
  user?: any;
  startLoading?: any;
  stopLoading?: any;
}
// Currently unused but needed for future filtering
const filterStates = ["departmentFilter", "destinationFilter"];
const getLabel = (opts: any, value: any) => {
  const l = opts.find((o: any) => (o.value === value));
  return l ? l.label : "";
};
const DropDownBar = (fields: any, states: any, onFilterChange: any) => {
  return (
    fields.map((val: any, idx: number) => {
      return (
        <DropdownButton
          id="idx"
          key={idx}
          onSelect={(e: any) => { onFilterChange(filterStates[idx], e); }}
          title={getLabel(val, states[idx])}
        >
          {createDdi(val)}
        </DropdownButton>
      );
    })
  );
};
const createDdi =
  (opts: any) => opts.map((o: any, i: any) => <Dropdown.Item eventKey={o.value} key={i}>{o.label}</Dropdown.Item>);

class ContainerList extends React.Component<IContainerListProps, any>  {
  private detail: any;
  private all: any;
  private searchDelay: any;
  constructor(props: IContainerListProps) {
    super(props);
    this.state = {
      departmentFilter: "all",
      departmentList: [],
      destinationFilter: "all",
      statusFilter: "all",
      destinationList: [],
      loading: false,
      hide: props.hide,
      searchBy: "shippingContainerId",
      searchFilter: "",
      searchText: "",
      cursorList: ["0"],
      pages: 0,
      tableData: [],
      orderBy: "shippingContainerId",
      pageSize: 20,
      page: 0,
      desc: false,
    };
    this.searchDelay = null;
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handlePagesizeChange = this.handlePagesizeChange.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
    this.setTableData = this.setTableData.bind(this);
    this.handlePageSort = this.handlePageSort.bind(this);
  }
  // extracts the shipment items to get their containers ancd then groups by container
  // probably want to move this out of the component and into either the class or just an auxilary funtion file

  public flattenShipments(shipments: any) {
    const flatened = [];
    // tslint:disable-next-line: forin
    for (let i = 0; i < shipments.length; i++) {
      for (let j = 0; j < shipments[i].items.length; j++) {
        const temp = Object.assign(shipments[i].items[j], shipments[i]);
        flatened.push(shipments[i].items[j]);
      }
    }
    const containerAggregate: any = {};
    let containerAggregateList = [];
    for (let i = 0; i < flatened.length; i++) {
      if (!containerAggregate[flatened[i].shippingContainerId]) {
        let tempObj = {};
        let combinded = Object.assign(tempObj, flatened[i]);
        combinded.items = []
        combinded.items.push(flatened[i])
        containerAggregate[flatened[i].shippingContainerId] = combinded;
      }
      else {
        containerAggregate[flatened[i].shippingContainerId].items.push(flatened[i]);
      }
    }
    for (let key in containerAggregate) {
      containerAggregateList.push(containerAggregate[key]);
    }
    return containerAggregateList;
  }
  // might want to add an api for departments within an org
  // for now im just going to grab it from the shipments on the front end

  public getDepartments(containers: any) {
    const deDupeDepartment: any = {};
    const departmentList = containers.filter((val: any) => {
      if (!deDupeDepartment[val.productType]) {
        deDupeDepartment[val.productType] = true;
        return val.productType;
      }
    });
    const departments = departmentList.map((val: any) => {
      return { label: val.productType, value: val.productType };
    });
    departments.push({ label: "All Departments", value: "all" });

    return departments;
  }

  public async componentDidMount() {
    this.props.startLoading("Loading your data ...");
    this.setTableData();
    const allDestinations = await this.props.client.shipmentsClient.getDestinations(this.props.org.org_id);
    const destLabels = allDestinations.destinations.map((val: any) => ({ label: val.name, value: val.containerId }));
    destLabels.push({ label: "All Destinations", value: "all" });
    this.setState({
      destinationList: destLabels
    });
    this.props.stopLoading();
  }

  public onFilterChange(filter: any, val: any) {
    this.setState({
      [filter]: val,
      cursorList: ["0"],
      loading: true,
      page: 0
    }, () => { this.setTableData(); });
  }

  public async setTableData() {
    let itemSearch: any = {};
    if (this.searchDelay) {
      clearTimeout(this.searchDelay);
    }
    this.searchDelay = setTimeout(() => {
      return;
    }, 500);
    // the below code will not work once additional 
    // refer to shipment list for proper user
    itemSearch[this.state.searchBy] = this.state.searchText;
    const filteredData = await this.props.client.shipmentsClient.searchShipments(
      this.props.org.org_id, "shippingContainerId",this.state.orderBy, {
        destinationContainerId: this.state.destinationFilter,
        productType: this.state.departmentFilter,
      },
      this.state.searchText,
      this.state.pageSize,
      this.state.cursorList[this.state.page],
      this.state.desc
    );
    // code ensures that the list of departments is only generated on load of data
    let departmentList = this.state.departmentList.slice();
    if (!departmentList || !departmentList.length) {
      departmentList = this.getDepartments(this.flattenShipments(filteredData.shipments));
    }
    const pages = filteredData.metadata.pageCount;
    const newCursorList = this.state.cursorList.slice();
    if (this.state.page === this.state.cursorList.length - 1) {
      newCursorList.push(filteredData.metadata.nextCursor);
    }
    this.setState({
      departmentList,
      cursorList: newCursorList,
      pages,
      loading: false,
      tableData: this.flattenShipments(filteredData.shipments)
    });
  }

  public resetFilters() {
    this.setState({
      departmentFilter: "all",
      destinationFilter: "all",
      searchText: "",
      cursorList: ["0"],
      loading: true,
      page: 0
    },()=> this.setTableData());
  }

  public handlePageChange(page: any) {
    this.setState({ page, loading:true }, () => {
      this.setTableData();
    });
  }
  public handlePagesizeChange(pageSize: any) {
    this.setState({ pageSize, loading:true }, () => {
      this.setTableData();
    });
  }
  public handlePageSort(orderBy: any) {
    const { id, desc } = orderBy[0];
    this.setState({ loading:true, orderBy: id, desc, cursorList: ["0"], page: 0 }, () => {
      this.setTableData();
    });
  }

  public render() {
    return (
      <>
        <ButtonToolbar className="toolbar">
          <ButtonGroup className="filter" >
            {this.state.departmentList && DropDownBar([this.state.departmentList, this.state.destinationList],
              [this.state.departmentFilter, this.state.destinationFilter], this.onFilterChange)
            }
          </ButtonGroup>
          <Form.Control
            style={{ marginRight: 10, display: "inline-block" }}
            type="text"
            name="searchFilter"
            placeholder={`Search By Containers`}
            value={this.state.searchText}
            onChange={(evt: any) => this.onFilterChange("searchText", evt.target.value)} />
          <ButtonGroup className="action" >
            <Button onClick={() => { this.resetFilters(); }}> Reset Filters</Button>
          </ButtonGroup>
        </ButtonToolbar>
        <ReactTable
          data={this.state.tableData}
          columns={shipmentItemsColumns}
          defaultPageSize={20}
          showPageJump={false}
          onPageChange={this.handlePageChange}
          pages={this.state.pages}
          onPageSizeChange={this.handlePagesizeChange}
          onSortedChange={this.handlePageSort}
          page={this.state.page}
          loading={this.state.loading}
          manual
          className="-striped -highlight"
          SubComponent={(row: any) => {
            return (
              <div style={{ padding: "20px" }}>
                Items
              <ReactTable
                  data={row.original.items}
                  columns={subItemCols}
                  defaultPageSize={5}
                  className="-striped -highlight"
                />
              </div>
            );
          }}
        />
      </>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    user: state.auth.user,
  };
}
export default jswithRouter(jswithLoader(connect(mapStateToProps)(ContainerList)));
