import * as _ from "lodash";
import moment from "moment";
import React from "react";
import { Link } from "react-router-dom";

const shipmentItemsColumns = [
  {
    Header: "Container Number",
    accessor: "shippingContainerId",
    // tslint:disable-next-line: object-literal-sort-keys
    Cell: (val: any) => {
      if (val.original.shippingContainerId) {
        return val.original.shippingContainerId.slice(val.original.shippingContainerId.indexOf("&") + 1);
      }
      return "Unknown Container";
    },
  },
  {
    Header: "Origin",
    accessor: "sourceContainerId",
    Cell: (val: any) => {
      if (val.original.sourceContainerId) {
        return val.original.sourceContainerId.slice(val.original.sourceContainerId.indexOf("&") + 1);
      } else {
        return null;
      }
    },
  },
  {
    Header: "Destination",
    accessor: "destinationContainerId",
    Cell: (val: any) => {
      if (val.original.destinationContainerId) {
        return val.original.destinationContainerId.slice(val.original.destinationContainerId.indexOf("&") + 1);
      } else {
        return null;
      }
    },
  },
  {
    Header: "ETD",
    accessor: "etd",
    Cell: (val: any) => {
      if (val.original.etd) {
        const event = new Date(val.original.etd);
        return event.toLocaleDateString(navigator.language);
      } else  {
        return "";
        //return "No ETD available";
      }
    },
  },
  {
    Header: "ATD",
    accessor: "atd",
    Cell: (val: any) => {
      if (val.original.atd) {
        const event = new Date(val.original.atd);
        return event.toLocaleDateString(navigator.language);
      } else {
        return "";
        //return "Not yet departed";
      }
    },
  },
  {
    Header: "ETA",
    accessor: "eta",
    Cell: (val: any) => {
      if (val.original.eta) {
        const event = new Date(val.original.eta);
        return event.toLocaleDateString(navigator.language);
      } else {
        return "";
        //return "No ETA available";
      }
    },

  },
  {
    Header: "ATA",
    accessor: "ata",
    Cell: (val: any) => {
      if (val.original.ata) {
        const event = new Date(val.original.ata);
        return event.toLocaleDateString(navigator.language);
      } else {
        return "";
        //return "Not yet arrived";
      }
    },

  },

  {
    Header: "Voyage",
    accessor: "voyage",
    Cell: (val:any) => {
      if (val.original.voyage) {
          const voyage = val.original.voyage;
          if(voyage==='none')
          {
            return null;  
          }
          else
          return (voyage);
      }
      else {
          return null
      }
  }
},

{
  Header: "Vessel",
  accessor: "vesselName",
  Cell: (val:any) => {
    if (val.original.vesselName) {
        const vesselName = val.original.vesselName;
        if(vesselName==='none')
        {
          return null;  
        }
        else
        return (vesselName);
    }
    else {
        return null
    }
}
},

{
  Header: "Shipment Ref",
  accessor: "shipmentRef",
  Cell: (val:any) => {
    if (val.original.shipmentRef) {

      return <Link to={"/"+val.original.orgId+"/shipments/detail/" + val.original.shipmentId}>{val.original.shipmentRef} </Link>
        
    }
    else {
        return null
    }
}
},


];
const subItemCols = [
  {
    Header: "Product Type",
    accessor: "productType",
    Cell: (row: any) => {
      console.log("row guy", row)
      if (row.original) {
        return <p> {row.original.productType} </p>;
      } else {
        return null;
      }
    }
  },
  {
    Header: "SKU",
    accessor: "productId",
    aggregate: (vals: any) => vals.length,
    Cell: (row: any) => {
      if (row.original) {
        return <p> {row.original.productId} </p>;
      } else {
        return null;
      }
    },
  },
  {
    Header: "Quantity",
    accessor: "quantity"
  },
  {
    Header: "Shipment Ref",
    accessor: "shipmentRef",
    aggregate: (vals: any) => vals.length,
    Cell: (row: any) => {
      if (row.original) {
        return <p> {row.original.shipmentRef} </p>;
      } else {
        return null;
      }
    },
  },
];
export { shipmentItemsColumns, subItemCols};