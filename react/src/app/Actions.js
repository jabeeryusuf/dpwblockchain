import React from 'react';
import Table from '../components/Table'
import { actionColumns, consolePastColumns } from './columns/columns';
import { GenericForm, EditAction } from '../components/forms';
import { connect } from 'react-redux';
import { Button, Row, Card, Container, ButtonToolbar, ButtonGroup } from 'react-bootstrap';
import VerticalModal from '../components/VerticalModal';

const moment = require('moment');

class Actions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      org: this.props.user.blockchain_id,
      currentAction: null,
      showPopUp: false,
      columns: actionColumns,
      newAction: false,
      tableHeader: "Pending Requests"
    }
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.newActionSubmit = this.newActionSubmit.bind(this);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.renderPopUp = this.renderPopUp.bind(this);
  }

  async componentDidMount() {
    console.log(this.props)
    this.resetTable();
  }

  /**
   * Handles the click of the "Edit Request" link in the table to
   * show the popup window the contains the current row's request information 
   * @param {*} row 
   */
  handleLinkClick(row) {
    this.setState({
      newAction: false,
      showPopUp: true,
      currentRow: row
    });
  }

  /**
   * Handles the "Cancel" button on the popup window.
   */
  onCancel() {
    this.setState({
      showPopUp: false,
      errorMessage: '',
      successMessage: ''
    });
  }

  /**
   * Queries the DB for the updated table information after a change to a request
   * has been made.
   */
  async resetTable() {
    let sql = `SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name 
               FROM actions a 
               LEFT JOIN containers c ON a.container_id = c.container_id 
               LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
               LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
               WHERE a.action_status = 'pending' AND requestor_id='` + this.props.org.org_id + `';`;
    let actionData = await this.props.client.getStuff(sql);
    for (let obj of actionData) {
      obj.request_date = moment(obj.request_date).format('MMMM Do YYYY');
      obj.container_id = obj.container_id.slice(19);
      obj.name = obj.name ? obj.name.substring(7, obj.name.length) : "Unknown"; //Does not show "Port of"
      obj.button = <Button size="sm" variant="link" onClick={() => this.handleLinkClick(obj)}>Edit Request</Button>
    }

    this.setState({
      data: actionData,
      columns: actionColumns,
      tableHeader: "Pending Requests"
    });
  }

  async setResolvedTable() {
    let sql = `SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id, s.destination_container_id, b.name 
               FROM actions a 
               LEFT JOIN containers c ON a.container_id = c.container_id 
               LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
               LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
               WHERE a.action_status != 'pending' AND requestor_id='` + this.props.org.org_id + `';`;
    let data = await this.props.client.getStuff(sql);
    for (let obj of data) {
      if (obj.action_status === "approved") {
        obj.action_status = <font className="text-success">{obj.action_status}</font>;
      } else {
        obj.action_status = <font className="text-danger">{obj.action_status}</font>;
      }
      obj.container_id = obj.container_id.slice(19);
    }

    this.setState({
      data: data,
      columns: consolePastColumns,
      tableHeader: "Resolved Requests"
    });
  }

  handleTabClick(str) {
    if (str === "pending") {
      this.resetTable();
    } else {
      this.setResolvedTable();
    }
  }

  async showNewActionForm() {
    this.setState({
      showPopUp: true,
      newAction: true
    })
  }
  async newActionSubmit(fields) {
    let {container_id,action_type} = fields;
    let createRequest = await this.props.client.createAction('shipping-container&' + container_id, action_type,'demo');
    if (!createRequest.errorMessage) {
      this.setState({
        successMessage: 'Action sucessfully create!',
        errorMessage: ''
      });
      setTimeout(() => {
        this.setState({
          successMessage: '',
          showPopUp: false
        });
      }, 1500);
      this.resetTable();
    } else {
      this.setState({
        errorMessage: createRequest.errorMessage,
        successMessage: ''
      })
    }
  }

  /**
   * Handles the "Submit" button click on the popup window
   * @param {*} id 
   * @param {*} action 
   */
  async onSubmit(id, action) {
    if (action === this.state.currentRow.type) {
      this.setState({
        errorMessage: 'No changes made'
      });
    } else {
      let updateRequest = await this.props.client.updateAction(id, action);
      if (!updateRequest.errorMessage) {
        this.setState({
          successMessage: 'Change successfully made',
          errorMessage: ''
        });
        setTimeout(() => {
          this.setState({
            successMessage: '',
            showPopUp: false
          });
        }, 1500);
      }

      this.resetTable();
    }
  }

  /**
   * Occurs after the "yes" button has been clicked in the "Are You Sure" window.
   * Sends a post request to delete currently selected request
   * @param {*} id 
   */
  async onDelete(id) {
    let deleteRequest = await this.props.client.deleteAction(id);
    if (!deleteRequest.errorMessage) {
      this.setState({
        successMessage: 'Request successfully deleted',
        errorMessage: ''
      });
      setTimeout(() => {
        this.setState({
          successMessage: '',
          showPopUp: false
        });
      }, 1500);
    }

    this.resetTable();
  }
  renderPopUp() {
    let modalClose = () => this.setState({ showPopUp: false, errorMessage: '', successMessage: '' });

    if (this.state.newAction) {
      return (
        <VerticalModal show={this.state.showPopUp}
                       size={"md"}
                       title={"Create New Request"}
                       onHide={modalClose} 
                       page={ <GenericForm inputs={[{ title:'Container Number', fieldValue:'container_id', type: 'text', values: null }, { title:'Action Type', fieldValue: 'action_type', type: 'select', values: ['expedite', 'hold', 'JOIP'] }]} 
                                           buttons={[{ text: 'submit', action:this.newActionSubmit}, { text: 'cancel', action: ()=>{this.onCancel()}}]}  
                                           successMessage = {this.state.successMessage}
                                           errorMessage = {this.state.errorMessage} /> }
        />
      )
    }
    else {
      return (
        <VerticalModal show={this.state.showPopUp}
                       size={"lg"}
                       title={"Edit Request"}
                       onHide={modalClose}                       
                       page={<EditAction row={this.state.currentRow}
                                         errorMessage={this.state.errorMessage}
                                         successMessage={this.state.successMessage}
                                         onCancel={this.onCancel}
                                         onSubmit={this.onSubmit}
                                         onDelete={this.onDelete} />} />
      )
    }
  }
  onRowClick = (rowInfo) => {
    return {
      onClick: () => {
        let row = rowInfo.original;
        this.setState({
          currentAction: row
        });
      }
    }
  }
  render() {
    return (
      <Container style={{ marginTop: 15}}>
        {this.renderPopUp()}
        <Card>
          <Card.Header>
            <Row>
              <ButtonToolbar className="toolbar">
                <Button variant="outline-primary" onClick={() => this.showNewActionForm()}>Create New Requests</Button>
                <ButtonGroup className="action">
                  <Button onClick={() => this.handleTabClick("pending")}>Pending Requests</Button>
                  <Button onClick={() => this.handleTabClick("resolved")}>Resolved Requests</Button>
                </ButtonGroup>
              </ButtonToolbar>
            </Row>
          </Card.Header>
          <Card.Body>
            <Table
              filterable
              data={this.state.data}
              columns={[{ Header: <font size="3">{this.state.tableHeader}</font>, columns: this.state.columns }]}
              getTrProps={this.onRowClick}
              defaultPageSize={10}
              className="-striped -highlight" />
          </Card.Body>
        </Card>
      </Container>
    )
  }
}
function mapStateToProps(state) {
  return { client: state.auth.client, user: state.auth.user, org: state.auth.org}
}
export default connect(mapStateToProps)(Actions);
