import React from 'react';
import {Conversation} from '../../components/conversation'
import {Subheader } from '../../layout';
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import {conversationActions} from '../../redux/actions';


class ViewConversation extends React.Component {

  tagConversation() {
    let {conversation, tagConversation} = this.props;
    let {id} = this.props.match.params;

    if (!conversation || !conversation.tags['sidebar']) {
      tagConversation(id, 'sidebar');
    }

  }
  
  componentDidMount() { 
    this.tagConversation(); 
  }
  
  componentDidUpdate(prevProps, PrevState) {

    if (this.props.match.params !== prevProps.match.params) {
      this.tagConversation();
    }
  }


  renderConversation(id) {
    let {conversations} = this.props;
    let topic = conversations[id] ? conversations[id].subject : id;

    return (
      <>
        <Subheader title={topic} />
        <Conversation groupId={id}/>
      </>
    );
  }


  render() {
    let {id} = this.props.match.params;
    //console.log('nav = %s',id);

    return this.renderConversation(id);
 
  }
}

function mapStateToProps(state) {
  return { 
    conversations: state.conversation.conversations,
  };
}


const mapDispatchToProps = (dispatch) => ({
  tagConversation: (convId,tag) => dispatch(conversationActions.tag(convId,tag)),
});


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewConversation));