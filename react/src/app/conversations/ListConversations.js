import React from 'react';
import {Link} from 'react-router-dom';
import {withLoader} from '../../components';
import {ListGroup,Row,InputGroup,FormControl,Col} from 'react-bootstrap';
import { connect } from 'react-redux';


class AddConversation extends React.Component {
  
  render() {
    
    let {convPath,conversations} = this.props;

    convPath = 'id-'

    return (
      <>
        <Row >
          <Col sm={{ span: 8, offset: 2 }} >
            <h2 className="text-left">Direct Messages</h2>
          </Col>
        </Row>

        <Row>
          <Col sm={{ span: 8, offset: 2 }}>
            <InputGroup size="lg">
              <FormControl aria-label="Large" aria-describedby="inputGroup-sizing-sm" />
              <InputGroup.Append>
                <InputGroup.Text id="inputGroup-sizing-lg">Go</InputGroup.Text>
              </InputGroup.Append>
            </InputGroup>
          </Col>
        </Row>

        <br />
        <br />

        <Row>
          <Col sm={{ span: 8, offset: 2 }}>
            <ListGroup>
              {Object.keys(conversations).map((cid, i) => (
                <ListGroup.Item key={i}>
                  <Link to={convPath + cid}>
                    {conversations[cid].subject ? conversations[cid].subject : cid}
                  </Link>
                </ListGroup.Item>
              ))}
            </ListGroup>
          </Col>
        </Row>
      </>
    );
  }


}

function mapStateToProps(state) {
  return { 
    conversations: state.conversation.conversations,
    users: state.conversation.users,
    user: state.auth.user,
  };
}

export default withLoader(connect(mapStateToProps)(AddConversation));
