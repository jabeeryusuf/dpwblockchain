import React from 'react';
import { connect } from 'react-redux';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DashboardRequests from '../components/DashboardRequests.js';
import DashboardWidget from '../components/DashboardWidget.js';
import Subheader from '../layout/Subheader';
import { currentShipColumns } from './columns/columns.js';
import ReactTable from 'react-table';
import { withLoader } from "../components";
const dblogo = require("../images/honda-logo.png");

const moment = require('moment');
const orgMap = {
    'utc': 'Jebel Ali',
    'anko': 'Seattle',
    'demo': 'Busan',
    'peacocks': 'South Hampton'
}

//const s3URL = "http://teuchain-public.s3.us-east-2.amazonaws.com/email-logos/";
const s3URL = "https://s3-eu-west-1.amazonaws.com/dpwchainpublic/logo/";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            emailLogo: null,
        };

    }



    async componentDidMount() {
        const { basePath } = this.props;
        const { startLoading, stopLoading } = this.props;
        startLoading("Loading your data ...");
        let id = this.props.user.user_id;
        console.log("ID: ", id);
        await this.getEmailLogo();
        let tempPortFella;
        if (this.props.user.org_id === 'utc') {
            tempPortFella = 'port&JEA'
        }
        else if (this.props.user.org_id === 'demo') {
            tempPortFella = 'port&PUS'
        }
        else if (this.props.user.org_id === 'peacocks') {
            tempPortFella = 'port&GBSOU'
        }

        else if (this.props.user.org_id === 'hondauk') {
            tempPortFella = 'port&Felixstowe'
        }
        else {
            tempPortFella = 'port&USSEA'
        }
        let data = await this.props.client.shipmentsClient.queryShipments('s.org_id=$1 and s.destination_container_id =$2 and s.shipment_status=$3', [this.props.org.org_id, tempPortFella, "in-transit"]);
        for (let row of data) {
            // row.eta = moment(row.eta).format("MMMM Do YYYY");
            // row.shipmentLink = <Link to={basePath + "/shipments/" + row.shipment_id}>{row.shipment_id}</Link>
            row.basePath = this.props.basePath;
        }

        this.setState({
            data: data
        });
        stopLoading();
    }

    async getEmailLogo() {
        const logo = await this.props.orgApi.getSetting(this.props.org.org_id, "logo");

        console.log(this.state);
        this.setState({
            emailLogo: logo.value,
        });
    }

    render() {
        return (
            <div>
                {/*
                <Row>
                    <Col style={{ padding: "0px 0px 0px", margin: "9px 8px 7px" }}>
                        <img style={{ maxHeight: "120px", maxWidth: "120px", width: "auto", height: "auto" }}
                            src={s3URL + this.state.emailLogo} alt={this.props.org.name} />
                        
                    </Col>
                </Row>
                <Subheader title="Dashboard" />

                <Row>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title>
                                    Current Open Requests
                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <DashboardRequests />
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title>
                                    Current Shipments to {orgMap[this.props.user.org_id] ? orgMap[this.props.user.org_id] : 'Felixstowe'}
                                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                                {this.state.data.length > 0 &&
                                    <ReactTable data={this.state.data}
                                        columns={currentShipColumns}
                                        defaultPageSize={10}
                                        className="-striped -highlight" >
                                    </ReactTable>}
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                */}
                <Col className='col-md-12'>

                <DashboardWidget/>
                   
                </Col>

                <Col className='row Footer'>

               <div className="Footer_Logo"><img src={s3URL + "dpwTeuLogo.png"}/></div>
           
                </Col>

            </div>
        );
    }

}

function mapStateToProps(state) {
    return {
        client: state.auth.client,
        org: state.auth.org,
        user: state.auth.user,
        orgApi: state.auth.orgApi,
    }
}

export default withLoader(connect(mapStateToProps)(Dashboard));