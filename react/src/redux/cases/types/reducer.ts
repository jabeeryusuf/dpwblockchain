import { CaseType } from "tw-api-common";
import { ActionsUnion } from "../../utils";
import { baseActions, BaseActionType } from "./actions";

export interface ICaseTypesState {
  list: CaseType[];
  map: any;
}

const initialState: ICaseTypesState  = { list: [], map: {} };

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  let reduced;

  switch (action.type) {
  case BaseActionType.SUCCESS:
    // rest api client requires a bearer token

    const map = Object.assign({}, initialState.map);
    for (const row of action.payload.list) {
      map[row.caseTypeId] = row;
    }

    reduced = { list: action.payload.list, map };
    break;

  default:
    return state;
  }

  console.log("CASE TYPE REDUCER");
  console.log(reduced);
  return reduced;
}
