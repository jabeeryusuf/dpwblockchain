import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { CaseType } from "tw-api-common";
import { alertActions } from "../../actions";
import { createAction,  getApiClient } from "../../utils";

const client = getApiClient().getCaseTypesClient();

export enum BaseActionType {
  CREATE_REQUEST = "caseTypes/CREATE",
  DELETE_REQUEST = "caseTypes/DELETE",
  LIST_REQUEST = "caseTypes/LIST",
  UPDATE_REQUEST = "caseTypes/UPDATE",
  FAILURE = "caseTypes/FAILURE",
  SUCCESS = "caseTypes/SUCCESS",
}

export const baseActions = {
  createRequest: () => createAction(BaseActionType.CREATE_REQUEST, {}),
  deleteCaseRequest: () => createAction(BaseActionType.DELETE_REQUEST, {}),
  failure: (message: string) => createAction(BaseActionType.FAILURE, {message}),
  listRequest: () => createAction(BaseActionType.LIST_REQUEST, {}),
  success: (listAct: CaseType[]) => createAction(BaseActionType.SUCCESS, {list: listAct}),
  updateRequest: () => createAction(BaseActionType.UPDATE_REQUEST, {}),
};

// Thunks
const list = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.listRequest());
    console.log("list caseTypes");

    try {
      const types = await client.list();
      console.log("list success");
      console.dir(types);
      // Now dispatch that we are fully signed in
      dispatch(baseActions.success(types));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
    }
  };
};

const create = (caseType: CaseType): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(baseActions.createRequest());
    console.log("add case request");

    try {
      const addCase = await client.create(caseType);
      console.log("add case success");
      console.log(addCase);

      // retrieve new list of cases
      dispatch(list());
      dispatch(alertActions.success("Case Type Added"));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

const update = (type: CaseType): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(baseActions.updateRequest());
    console.log("update case type request");

    try {

      const updateCase = await client.update(type);
      console.log("update case type success");
      console.log(updateCase);

      // retrieve new list of cases
      dispatch(list());
      dispatch(alertActions.success("Case Type updated"));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

const deleteCaseType = (caseTypeId: string): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    dispatch(baseActions.deleteCaseRequest());
    console.log("delete case type request");

    try {

      console.log("CASE TYPE ID:", caseTypeId);
      await client.deleteCaseType(caseTypeId);
      console.log("delete case type success");
      // retrieve new list of cases
      dispatch(list());
    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
    }
  }
}

export const actions = {
  create,
  deleteCaseType,
  list,
  update,
};
