export const verificationConstants = {
  START          : 'verification/start',
  SEND_REQUEST   : 'verification/send-request',
  SEND_SUCCESS   : 'verification/send-success',
  SEND_FAILURE   : 'verification/send-failure',
  VERIFY_REQUEST : 'verification/verify-request',
  VERIFY_SUCCESS : 'verification/verify-success',
  VERIFY_FAILURE : 'verification/verify-failure',
};