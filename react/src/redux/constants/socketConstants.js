// Socket related actions
export const socketConstants = {
  CONNECTION_CHANGED:     'socket/connection-changed',
  AUTH_CHANGED:           'socket/authentication-changed',
  PORT_CHANGED:           'socket/port-changed',
  CONNECT_SOCKET:         'socket/connect',
  DISCONNECT_SOCKET:      'socket/disconnect',
};