

export const conversationConstants = {
  // Updates message stream or via api call
  LIST_RECEIVED:    'message/list-received',
  HISTORY_RECEIVED: 'message/history-received',
  MEMBERS_RECEIVED: 'message/members-received',

  // Message related actions
  TAG_CONVERSATION:   'message/tag-conversation',

  CHANGE_OUTGOING_MESSAGE : 'message/change-outgoing-message',
  MESSAGE_SENT : 'message/message-sent',
  SEND_MESSAGE : 'message/send-message',

  MESSAGE_RECEIVED : 'message/message-received',

  UPDATE_PREFS_SUCCESS: 'message/update-prefs-success',
  UPDATE_PREFS_ERROR: 'message/update-prefs-error',

}