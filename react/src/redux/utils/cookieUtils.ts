import { Cookies } from "react-cookie";
import { Org, User} from "tw-api-common";

const cookies = new Cookies();
const COOKIE_PREFIX = "org-";

export function getOrgIdFromHostname() {
  const path = window && window.location && window.location.pathname;
  console.log("path = %s", path);
  const pElems = path ? path.split("/") : null;
  return (pElems && (pElems.length > 0)) ? pElems[1] : null;
}

export function getUserFromCookie(t: any) {
  const user = Object.create(User.prototype);
  Object.assign(user, t.user);

  return user;
}

export function getOrgFromCookie(t: any): Org {
  const org = Object.create(Org.prototype);
  Object.assign(org,  t.org);
  return org;
}

export function getAllCookies() {
  const allCookies = cookies.getAll();
  const retCookies = [];

  for (const c of Object.keys(allCookies)) {
    if (c.startsWith(COOKIE_PREFIX)) {
      retCookies.push(allCookies[c]);
    }
  }

  return retCookies;
}

export function getCookieFromUser(userId: string) {
  const allCookies = cookies.getAll();

  for (const c of Object.keys(allCookies)) {

    if (c.startsWith(COOKIE_PREFIX)) {
      const cu = getUserFromCookie(allCookies[c]);
      console.log(cu);

      if (cu.user_id  === userId) {
        allCookies[c].expires = new Date(allCookies[c].expires);
        return allCookies[c];
      }
    }
  }
  return null;
}

export function getAllUsersFromCookies() {

  const allCookies = cookies.getAll();
  const users = [];

  for (const c of Object.keys(allCookies)) {
    if (c.startsWith(COOKIE_PREFIX)) {
      const user = getUserFromCookie(allCookies[c]);
      users.push(user);
    }
  }
  return users;
}

export function setCookie(t: any) {

  // The token has user and blockchain info
  const org = getOrgFromCookie(t);

  // We store one cookie per blockchain
  const key = "org-" + org.orgId;

  cookies.set(key, t, {path: "/"} );
}

export function getCookie() {
  const orgId = getOrgIdFromHostname();
  const key = "org-" + orgId;
  const cookie = cookies.get(key);
  if (cookie) {
    cookie.expires = new Date(cookie.expires);
  }
  return cookie;
}

export function removeCookie() {
  const bcId = getOrgIdFromHostname();

  // We store one cookie per blockchain
  const key = "org-" + bcId;

  cookies.remove(key, {path: "/"});
}
