import { FcFetchClient} from "fc-rest-client";

let client: FcFetchClient;

export function getApiClient(): FcFetchClient {

  if (client) {
    return client;
  }

  let env: "production" | "development" | "staging" | undefined;

  // create-react-app automatically differentiates between production, development and test
  if (process.env.NODE_ENV === "production") {
    const hostname = window && window.location && window.location.hostname;

    if (['18.191.0.111','54.154.192.16','3.17.143.81'].includes(hostname) || hostname.includes("staging.")) {
      env = "staging";
    } else {
      env = "production";
    }

  } else if (process.env.NODE_ENV === "development") {
    env = "development";
  } else {
    throw new Error("ENVIRONMENT UNKNOWN!!!!");
  }

  client = new FcFetchClient(env);

  return client;
}
