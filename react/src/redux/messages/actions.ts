import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { MessageTemplate } from "tw-api-common";
import { alertActions } from "../actions/alertActions";
import { createAction,  getApiClient } from "../utils";

const templatesClient = getApiClient().getTemplatesClient();

export enum BaseActionType {
  TEMPLATE_REQUEST = "messages-templates/TEMPLATE",
  SAVE_REQUEST = "messages-templates/SAVE",
  FAILURE = "messages-templates/FAILURE",
  SUCCESS = "messages-templates/SUCCESS",
}

export const baseActions = {
  failure: (message: string) => createAction(BaseActionType.FAILURE, { message }),
  success: (templates: any[]) => createAction(BaseActionType.SUCCESS, { templates }),
  templateRequest: () => createAction(BaseActionType.TEMPLATE_REQUEST, {}),
  updateTemplateRequest: () => createAction(BaseActionType.SAVE_REQUEST, {}),
};

// Thunks
const templateList = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.templateRequest());
    console.log("template request");

    try {
      const templates = await templatesClient.list();
      console.log("templates success");
      console.dir(templates);

      dispatch(baseActions.success(templates));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

// used to update AND create new templates
const updateTemplate = (templateObj: MessageTemplate): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.updateTemplateRequest());
    dispatch(alertActions.clear());
    console.log("update template request");

    try {
      await templatesClient.update(templateObj);
      console.log("saved template");

      dispatch(templateList());

      dispatch(alertActions.success("Template Saved"));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

export const actions = {
  templateList,
  updateTemplate,
};
