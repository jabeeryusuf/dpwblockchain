import { ActionsUnion } from "../utils";
import { baseActions, BaseActionType } from "./actions";

const initialState = { templates: [], templatesMap: {} };

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  let reduced;

  switch (action.type) {
  case BaseActionType.SUCCESS:

    const templatesMap: any = {};
    for (const row of action.payload.templates) {
      templatesMap[row.messageTemplateId] = row;
    }

    reduced = { templates: action.payload.templates, templatesMap };
    break;

  case BaseActionType.FAILURE:
    return {};
  default:
    return state;
  }

  console.log("MESSAGE REDUCER");
  console.log(reduced);
  return reduced;
}
