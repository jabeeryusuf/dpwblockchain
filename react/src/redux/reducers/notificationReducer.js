
// CONSTANTS
import { notificationConstants } from '../constants';

// Initial state
const INITIAL_STATE = {
  channels: {},
};

// Message Reducer
export function notification(state = INITIAL_STATE, action) {
  let reduced;
  let channels;
  switch (action.type) {

  case notificationConstants.UPDATE:

    //console.log('updating with action ',action);
    channels = state.channels;
    let id = action.id;
    let value = action.value;
    let channelObj = channels[action.channel] ? channels[action.channel] : {};
    channelObj[id] = value;

    reduced = Object.assign({},state, {
      channels: Object.assign({}, state.channels, {
        [action.channel]: channelObj,
      })
    })

    break;

  
  default:
    reduced = state;
  }

  return reduced;

 
}

