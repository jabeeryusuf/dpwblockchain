import { combineReducers } from 'redux';
import { registration } from './registrationReducer';
import { dataPull } from './dataPullReducer';
import { alert } from './alertReducer';
import { loading } from './loadingReducer';
import { user } from './userReducer';
import { verification } from './verificationReducer'
import { status } from './statusReducer'; 
import { conversation } from './conversationReducer'; 
import { socket } from './socketReducer'; 
import { notification } from './notificationReducer';
import { reducer as auth } from "../auth/reducer"
import { reducer as groups } from "../groups/reducer"
import { reducer as caseTypes } from "../cases/types"
import { reducer as messages } from "../messages/reducer";
import { reducer as vendors } from "../vendors";
import { reducer as orgs } from "../orgs";

const rootReducer = combineReducers({
  auth,
  caseTypes,
  groups,
  messages,
  registration,
  alert,
  loading,
  verification,
  dataPull,
  status,
  socket,
  conversation,
  notification,
  orgs,
  user,
  vendors,
});

export default rootReducer;