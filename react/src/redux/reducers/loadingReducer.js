import { loadingConstants } from '../constants';

export function loading(state = {}, action) {
  switch (action.type) {
    case loadingConstants.START:
      return {
        ...state,
        [action.id]: {
          isLoading: true,
          text: action.text
        }
      };

    case loadingConstants.STOP:
      return {
        ...state,
        [action.id]: {
          isLoading: false
        }
      };

    default:
      return state;
  }
}
