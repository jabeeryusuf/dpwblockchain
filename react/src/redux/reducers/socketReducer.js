// ACTIONS
import { socketConstants  } from '../constants';

// Initial state
const INITIAL_STATE = {
  connected: false,
  port: 5000
};

// Socket reducer
export function socket(state=INITIAL_STATE, action) {
  let reduced;
  switch (action.type) {
  case socketConstants.CONNECTION_CHANGED:
    reduced = Object.assign({}, state, {
      connected: action.connected,
      isError: false
    });
    break;

  case socketConstants.AUTH_CHANGED:
    //console.log('auth reducer ' + action.authenticated);

    reduced = Object.assign({}, state, {
      authenticated: action.authenticated,
      isError: false
    });
    break;

  case socketConstants.PORT_CHANGED:
    reduced = Object.assign({}, state, {port: action.port});
    break;

  default:
    reduced = state;
  }

  return reduced;
}

