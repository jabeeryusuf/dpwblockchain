import { dataPullConstants } from '../constants';

export function dataPull(state = {}, action) {
  switch (action.type) {
    case dataPullConstants.SUBMIT_PULL_REQUEST:
      return { submittingPull: true};
    case dataPullConstants.SUBMIT_PULL_SUCCESS:
      return { submittedPull: true, submittingPull: false };
    case dataPullConstants.SUBMIT_PULL_FAILURE:
      return { submittedPull: false, submittingPull: false};
    default:
      return state;
  }
}