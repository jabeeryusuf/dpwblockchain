// CONSTANTS
import { userConstants } from '../constants';

// Initial state
const INITIAL_STATE = {
  users: {},
  list: [],
};

// Message Reducer
export function user(state = INITIAL_STATE, action) {
  let reduced;
  switch (action.type) {

    case userConstants.LIST_RECEIVED:

      let users = Object.assign({}, state.users);
      let list = action.users;

      // convert cache
      users = list.reduce((_users, u) => {
        _users[u.user_id] = u;
        return _users;
      }, users);


      reduced = {
        users,
        list
      };  // switching to list name
      //console.log(reduced);
      break;
    case userConstants.UPDATE_USERS:
      users = state.users;
      users[user.user_id] = user 
      reduced = {
        ...state,
        users
      }
      break;
    default:
      reduced = state;
  }

  return reduced;
}