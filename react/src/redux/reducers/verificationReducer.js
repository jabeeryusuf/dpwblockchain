import { verificationConstants } from '../constants';

// Initial state
const INITIAL_STATE = {};

export function verification(state = INITIAL_STATE, action) {
  let reduced;

  switch (action.type) {
    case verificationConstants.START:
      reduced = {};
      break;
    case verificationConstants.SEND_REQUEST:
      reduced = { sending: true, email: action.email };
      break;
    case verificationConstants.SEND_SUCCESS:
      reduced = { sent: true, email: action.email };
      break;
    case verificationConstants.SEND_ERROR:
      reduced = { error: true, email: action.email }
      break;
    case verificationConstants.VERIFY_REQUEST:
      reduced = Object.assign({verifying: true}, state);
      break;
    case verificationConstants.VERIFY_SUCCESS:
      reduced = Object.assign({},state,{verified: true, verifying: false, email: action.email});
      break;
    case verificationConstants.VERIFY_ERROR:
      reduced = { error: true }
      break;
    default:
      reduced = state;
  }

  //console.log('VERIFICATION REDUCED:');
  //console.log(reduced);
  
  return reduced;
}