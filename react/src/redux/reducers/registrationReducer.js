import { authConstants } from '../constants';

export function registration(state = {}, action) {
  switch (action.type) {
    case authConstants.REGISTER_REQUEST:
      return { registering: true };
    case authConstants.REGISTER_SUCCESS:
      return { registered: true };
    case authConstants.REGISTER_FAILURE:
      return {};
    case authConstants.REGISTER_START:
      return { registering: false, registered: false, loggingIn: false};
    default:
      return state
  }
}