// ACTIONS
import { STATUS_CHANGED } from '../actions';


// Initial state
const INITIAL_STATE = {
    status: 'todo?',
    isError: false
};

// Status reducer
export function status(state=INITIAL_STATE, action) {
    let reduced;
    switch (action.type)
    {
        case STATUS_CHANGED:
            reduced = Object.assign({}, state, {
                status: String(action.status),
                isError: action.isError
            });
            break;

        default:
            reduced = state;
    }
    return reduced;
}

