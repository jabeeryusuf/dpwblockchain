
// CONSTANTS
import { conversationConstants } from '../constants';


// Initial state
const INITIAL_STATE = {
  conversations: {},
  summary: {}
  //users: {},
};

// Message Reducer
export function conversation(state = INITIAL_STATE, action) {
  let reduced;
  let conversation;
  switch (action.type) {

  case conversationConstants.TAG_CONVERSATION:
    
    //console.log('join');
    conversation = getConversation(action.groupId);
    conversation.tags[action.tag] = action.value;
    console.log('tagged conversation: %s=%s',action.tag, action.value);
    reduced = reduceConversation(conversation,action.groupId)
    break;

  // Messaging state is managed at the conversation level
  case conversationConstants.CHANGE_OUTGOING_MESSAGE:

    // Add the keyed message to a clone of the appropriate thread
    conversation = getConversation(action.groupId);
    conversation.outgoingMessage = action.text;
    reduced = reduceConversation(conversation,action.groupId)
    break;

  case conversationConstants.MESSAGE_RECEIVED:

    // action.message.to contains the group this message is being sent to
    const groupId = action.message.to;
    //console.log(action.message.to)
    conversation = getConversation(groupId);

    // Rendered e components need a key, we'll use array index
    const messageKey = conversation.messages ? conversation.messages.length : 0;
    const keyedMessage = Object.assign({}, action.message, {key: messageKey});

    conversation.messages = conversation.messages ? 
      conversation.messages.concat([keyedMessage]) :
      [keyedMessage];

    conversation.total_msg_count++;
    conversation.unread_count++;

    // Reduce
    reduced = reduceConversation(conversation,groupId);
    //console.dir(reduced);

    break;

  case conversationConstants.HISTORY_RECEIVED:
    //console.log("HISTORY RECEIVED");
    conversation = getConversation(action.conversationId);
    conversation.messages = action.messages.slice();
    // console.log(conversation.messages);
    reduced = reduceConversation(conversation,action.conversationId);
    // console.log("message history updated >>", reduced);
    break;

  case conversationConstants.MEMBERS_RECEIVED:
    // console.log('REDUCER MEMBERS');
    // console.log(action.members);

    reduced = state;
    break;

  case conversationConstants.LIST_RECEIVED:

    //let users = Object.assign({},state.users);
    let conversations = Object.assign({},state.conversations);
    
    //TODO: SUM UP UNREAD COUNT INTO 
    let summary = Object.assign({}, state.summary);

/*     if (action.users) {
      for (let u of action.users) {
        users[u.user_id] = users[u.user_id] ? users[u.user_id] : {};
        Object.assign(users[u.user_id],u);
      }
    }
 */ 
    let total_unread_count = 0;
    let case_unread_count = 0;
    if (action.conversations) {
      conversations = action.conversations.reduce((convs,c) => {
        let id = c.conversation_id;

        c.unread_count = c.total_msg_count - c.read_count;
        
        convs[id] = convs[id] ? convs[id] : {messages:[],tags:{}};
        Object.assign(convs[id],c);
        
        total_unread_count += c.unread_count;
        if (c.isCaseGroup) { case_unread_count += c.unread_count };

        return convs;
      },{});
    }

    summary.total_unread_count = total_unread_count;
    summary.case_unread_count = case_unread_count;

    //console.dir(users);
    // console.log("CONVERSATION REDUCED");
    // console.log(conversations)

    reduced = Object.assign({},state, {conversations}, {summary} );
    
    break;

  case conversationConstants.MESSAGE_SENT:
    // Add the keyed message to a clone of the appropriate thread
    conversation = getConversation(action.groupId);
    conversation.outgoingMessage = '';
    reduced = reduceConversation(conversation,action.groupId);
    break;
  
  case conversationConstants.UPDATE_PREFS_SUCCESS:
    conversations = Object.assign({}, state.conversations);

    let conv = conversations[action.conversationId];

    //can be either 'read_count' or 'starred' or whatever we come up with later
    let key = Object.keys(action.pref)[0];

    if (key === 'read_count') {
      conv.read_count = action.pref[key];
      conv.total_msg_count = conv.messages.length;
      conv.unread_count = conv.total_msg_count - conv.read_count //should always be 0
    }

    reduced = Object.assign({}, state, { conversations });
    
    break;

  default:
    reduced = state;
  }

  return reduced;


  // helper function
  function getConversation(groupId) {
    return (state.conversations[groupId]) 
            ? state.conversations[groupId] 
            : {messages: [], tags: {}};
  }
  
  function reduceConversation(conversation,groupId) {
    let reduced = Object.assign({},state, {
      conversations: Object.assign({}, state.conversations, {
        [groupId]: conversation,
      })
    })
  
    return reduced;
  }

}

