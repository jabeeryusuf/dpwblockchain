import { Org } from "tw-api-common";
import { ActionsUnion, getAllCookies, getOrgFromCookie } from "../utils";
import { baseActions, BaseActionType } from "./actions";

const tokens = getAllCookies();

const orgs: any = {};
for (const t of tokens) {
  const org = getOrgFromCookie(t);
  orgs[org.orgId] = org;
}

const initialState = {orgs};

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  let reduced;

  switch (action.type) {
  case BaseActionType.VALIDATE_REQUEST:
    reduced = reduceOrg({validating: true});
    break;

  case BaseActionType.CREATE_REQUEST:
    reduced = reduceOrg({creating: true});
    break;

  case BaseActionType.SUCCESS:
    reduced = reduceNewOrg({validated: true}, action.payload.org);
    break;

  case BaseActionType.FAILURE:
    reduced = reduceOrg({validationError: true});
    break;
  default:
    reduced = state;
  }

  // console.log('ORG REDUCER:');
  // console.log(reduced);
  return reduced;

  function reduceOrg(base: any) {
    return Object.assign(base, {
      orgs: Object.assign({}, state.orgs),
    });
  }

  function reduceNewOrg(base: any, org: Org) {
    return Object.assign(base, {
      orgs: Object.assign({}, state.orgs, {
        [org.orgId]: org,
      }),
    });
  }
}
