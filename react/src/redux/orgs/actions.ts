import { Org} from 'tw-api-common';
import { FcFetchClient } from "fc-rest-client";
import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { alertActions } from "../actions/alertActions";
import { createAction,  getApiClient } from "../utils";

const client: FcFetchClient = getApiClient();

export enum BaseActionType {
  VALIDATE_REQUEST = "orgs/VALIDATE_REQUEST",
  CREATE_REQUEST = "orgs/CREATE_REQUEST",
  SUCCESS = "orgs/SUCCESS",
  FAILURE = "orgs/FAILURE",
}

export const baseActions = {
  createRequest: (orgId: string) => createAction(BaseActionType.CREATE_REQUEST, {orgId}),
  failure: (message: string) => createAction(BaseActionType.FAILURE, {message}),
  success: (org: Org) => createAction(BaseActionType.SUCCESS, {org}),
  validateRequest: (orgId: string) => createAction(BaseActionType.VALIDATE_REQUEST,{orgId}),
};

// Thunks
const validate = (orgId: string):
  ThunkAction<Promise<Org|null>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<Org|null> => {

    try {
      dispatch(baseActions.validateRequest(orgId));
      const org = await client.orgsClient.info(orgId)
      dispatch(baseActions.success(org));
      return org;
    } catch(error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
      return null;
    }
  };
};

// Thunks
const create = (orgId: string, name: string, type: string):
  ThunkAction<Promise<Org|null>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<Org|null> => {

    try {
      dispatch(baseActions.createRequest(orgId));
      const org = await client.orgsClient.create(new Org(orgId,name,type));
      dispatch(baseActions.success(org));
      return org;
    } catch (error) {
      console.log("Org Create error is");
      console.log(error);
      dispatch(baseActions.failure(error));
      return null;
    }
  };
};

export const actions = {
  create,
  validate,
};



