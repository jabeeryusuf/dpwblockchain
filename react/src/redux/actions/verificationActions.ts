import { verificationConstants } from "../constants";
import { getApiClient } from "../utils";

export const verificationActions = {
  start,
  sendCode,
  verify,
};

const usersApi = getApiClient().getUsersApi();

function start() {
  return { type: verificationConstants.START };
}

function sendCode(email: string) {

  return async (dispatch: any) => {
    function sendRequest(em: string) { return {type: verificationConstants.SEND_REQUEST, email: em}; }
    function sendSuccess(em: string) { return {type: verificationConstants.SEND_SUCCESS, email: em}; }
    function sendFailure(em: string) { return {type: verificationConstants.SEND_FAILURE, email: em}; }

    try {
      dispatch(sendRequest(email));
      const sent = await usersApi.sendVerificationCode({email});
      dispatch(sendSuccess(email));
      return sent;
    } catch (error) {
      console.log("verification send failure", error);
      dispatch(sendFailure(email));
    }
  };
}

function verify(email: string, code: string) {
  return async (dispatch: any) => {
    function verifyRequest() { return { type: verificationConstants.VERIFY_REQUEST}; }
    function verifySuccess(em:string) { return { type: verificationConstants.VERIFY_SUCCESS, email: em};}
    function verifyFailure(em:string) { return { type: verificationConstants.VERIFY_FAILURE, email: em};}

    try {
      dispatch(verifyRequest());
      const verified = await usersApi.verifyEmail(email, code);
      if (verified) {
        dispatch(verifySuccess(email));
      }
      dispatch(verifyFailure(email));
      return verified;
    } catch (error) {
      dispatch(verifyFailure(email));
    }
  };
}

