
import {socketConstants} from '../constants';

export const socketActions = {
  connectionChanged,
  authenticationChanged,
  portChanged,
  connectSocket,
  disconnectSocket
};

// The socket's connection state changed
function connectionChanged(isConnected) {
  return {
    type: socketConstants.CONNECTION_CHANGED,
    connected: isConnected,
  }
}

function authenticationChanged(isAuth) {
  return {
    type: socketConstants.AUTH_CHANGED,
    authenticated: isAuth,
  }
}


// The user selected a different port for the socket
function portChanged(port) {
  return {
    type: socketConstants.PORT_CHANGED,
    port: port
  }
}

// The user clicked the connect button
function connectSocket() {
  return {
    type: socketConstants.CONNECT_SOCKET
  }
}

// The user clicked the disconnect button
function disconnectSocket() {
  return {
    type: socketConstants.DISCONNECT_SOCKET
  }
}