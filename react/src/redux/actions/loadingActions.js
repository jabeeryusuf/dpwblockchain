import { loadingConstants } from '../constants';

export const loadingActions = {
    start,
    stop
};

function start(id, text) {
    return { id, type: loadingConstants.START, text };
}

function stop(id) {
  return { id, type: loadingConstants.STOP };        
}
