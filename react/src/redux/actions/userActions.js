import { userConstants } from '../constants'
import { getApiClient } from '../utils';
import { authActions } from "../actions"
import { alertActions } from "../actions/alertActions"
import { authConstants } from "../constants/authConstants";
import {baseActions} from "../auth";
let usersApi = getApiClient().getUsersApi();

function list(role,disabled) {

  return async dispatch => {
    function listReceived(users) { return { type: userConstants.LIST_RECEIVED, users} }
    function listError() {return { type: userConstants.LIST_ERROR }}
    
    try {
      console.log('calling user api');
      const users = await usersApi.list({role,disabled})
      console.log("users in org:");
      console.dir(users);
      dispatch(listReceived(users));
      return users;
    } catch (error) {
      console.log(error);
      dispatch(listError());
    }
  }
}

const create = (orgId, email, name,password, verificationKey) => {

  return async dispatch => {

    try {
      dispatch(registerRequest(email, orgId));
      const user = await usersApi.create(orgId, email, name, password, verificationKey);
      dispatch(registerSuccess(user));
      dispatch(alertActions.success("Registration Successful"));
      dispatch(authActions.signIn(orgId, email, password));
    } catch (error) {
      dispatch(registerFailure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };

  function registerRequest(user, nOrgId) {
    return { type: authConstants.REGISTER_REQUEST, user, nOrgId };
  }
  function registerSuccess(user) { return { type: authConstants.REGISTER_SUCCESS, user }; }
  function registerFailure(error) { return { type: baseActions.REGISTER_FAILURE, error }; }
  
}
function updateUsers(user){
  return { type: userConstants.UPDATE_USERS, user }

}
function updateUser(user){
 return baseActions.UPDATE_USER(user)
} 

const update = (user) =>{
  return async dispatch =>{
    try{
      const userUpdate = await usersApi.update(user);
      dispatch(updateUsers(user))
      dispatch(updateUser(user))
      
    }
    catch(err){
      dispatch(alertActions.error(err.toString()));
    }
  }
}

const startRegistration = () =>  {
  return { type: authConstants.REGISTER_START };
}

export const userActions = {
  list,
  create,
  update
}
