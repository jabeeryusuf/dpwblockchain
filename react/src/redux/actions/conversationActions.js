import { conversationConstants } from '../constants';
import { notificationActions } from '.';

import { getApiClient } from '../utils';

export const conversationActions = {
  list,
  history,
  members,

  tag,
  untag,

  changeOutgoingMessage,
  sendMessage,
  messageSent,

  messageReceived,
  clientUpdateReceived,

  updatePreferences
}

let messageApi = getApiClient().getConversationApi();

function list(userId) {

  return async dispatch => {
    function listReceived(conversations) { return { type: conversationConstants.LIST_RECEIVED, conversations} }
    function listError() {return { type: conversationConstants.LIST_ERROR }}
    
    try {
      const conversations = await messageApi.list(userId);

      let total_unread_count = 0;
      let case_unread_count = 0;

      for (let conv of conversations) {
        conv.unread_count = conv.total_msg_count - conv.read_count;
        if (conv.isCaseGroup) { case_unread_count += conv.unread_count};
        total_unread_count += conv.unread_count;
      }

      dispatch(notificationActions.update("cases", "cases", case_unread_count));
      dispatch(notificationActions.update("messages", "unread-messages", total_unread_count));

      // console.log("CONVERSATIONS for %s: ",userId);
      // console.dir(conversations);
      dispatch(listReceived(conversations));
      return conversations;
    } catch (error) {
      dispatch(listError());
    }
  }
}

function history(convId) {

  return async dispatch => {
    function historyReceived(conversationId, messages) { return { type: conversationConstants.HISTORY_RECEIVED, conversationId, messages} }
    function historyError(conversationId) { return {type: conversationConstants.HISTORY_ERROR, conversationId }}

    try {
      const messages = await messageApi.history(convId)
      dispatch(historyReceived(convId, messages));
      return messages;
    } catch (error) {
      dispatch(historyError(convId));
    }
  }
}

function members(convId) {

  return async dispatch => {
    function membersReceived(conversationId, members) { return { type: conversationConstants.MEMBERS_RECEIVED, conversationId, members} }
    function membersError(conversationId) { return {type: conversationConstants.MEMBERS_ERROR, conversationId }}

    try {
      console.log("members action");
      const members = await messageApi.members(convId)
      dispatch(membersReceived(convId, members));
      return members;
    } catch (error) {
      dispatch(membersError(convId));
    }
  }
}

/**
 * Updates the conversation_user_preferences table with specified preference 
 * and updates redux storeupon success.
 * @param {string} convId conversation ID
 * @param {string} userId user ID
 * @param {object} pref object that describes the preference to be updated like { 'read_count' : 4 } or { 'starred' : 1 }
 */
function updatePreferences(convId, userId, pref) {

  return async dispatch => {
    function updateSuccess(conversationId, userId, pref) { return { type: conversationConstants.UPDATE_PREFS_SUCCESS, conversationId, userId, pref } }
    function updateError(conversationId) { return { type: conversationConstants.UPDATE_PREFS_ERROR, conversationId } }

    try {
      const update = await messageApi.updateConversationPreferences(convId, userId, pref);
      dispatch(updateSuccess(convId, userId, pref));

      if (pref['read_count']) {
        const conversations = await messageApi.list(userId);

        let total_unread_count = 0;
        let case_unread_count = 0;

        for (let conv of conversations) {
          conv.unread_count = conv.total_msg_count - conv.read_count;
          if (conv.isCaseGroup) { case_unread_count += conv.unread_count};
          total_unread_count += conv.unread_count;
        }

        dispatch(notificationActions.update("cases", "cases", case_unread_count));
        dispatch(notificationActions.update("messages", "unread-messages", total_unread_count));
      }

      return update;
    } catch (err) {
      dispatch(updateError(convId));
    }
  }
}


// Join or leave a conversation.
function tag(groupId,tag,value = true) {
  return {
    type: conversationConstants.TAG_CONVERSATION,
    groupId,
    tag,
    value
  }
}

function untag(groupId,tag) {
  return {
    type: conversationConstants.TAG_CONVERSATION,
    groupId,
    tag,
    value: false
  }
}


// The outgoing message has changed
function changeOutgoingMessage(groupId, text) {
  return {
    type: conversationConstants.CHANGE_OUTGOING_MESSAGE,
    groupId,
    text: text
  }
}

// Send a message
function sendMessage (groupId, message,files) {
  console.log('action', files)
  return {
    type: conversationConstants.SEND_MESSAGE,
    groupId,
    message: message,
    files: files
  }
}

// Sent an instant message
function messageSent(groupId) {
  return {
    type: conversationConstants.MESSAGE_SENT,
    groupId,
  }
}


// Handler for receiving messages, will route to appropriate group
function messageReceived (message) {
  //console.log('message received');
  return async dispatch => {
    function msgReceive(message) { return { type: conversationConstants.MESSAGE_RECEIVED, message: message }}

    //temporary work around solution for real time notifications
    try {
      let conversations = await messageApi.updateList();

      let total_unread_count = 0;
      let case_unread_count = 0;

      for (let conv of conversations) {
        conv.unread_count = conv.total_msg_count - conv.read_count;
        if (conv.isCaseGroup) { case_unread_count += conv.unread_count};
        total_unread_count += conv.unread_count;
      }

      dispatch(notificationActions.update("cases", "cases", case_unread_count));
      dispatch(notificationActions.update("messages", "unread-messages", total_unread_count));

      dispatch(msgReceive(message));
    } catch (err) {
      console.log(err);
    }
  } 
}

function clientUpdateReceived ( message ) {
  //console.log('message update');
  return {
    type: conversationConstants.CLIENT_UPDATE_RECEIVED,
    message: message
  }
}

