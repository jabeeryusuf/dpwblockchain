import { dataPullConstants } from '../constants';
import { alertActions } from '.';
import { getApiClient } from '../utils';


export const dataPullActions = {
  submitPull
};

let client = new getApiClient();

function submitPull(org, service, username, password, pullInterval) {
  return dispatch => {
    dispatch(submitPullRequest());
    client.submitPull(org, service, username, password, pullInterval)
    .then (
      res => {
        console.log(res);
        if (res.status === 400) {
          dispatch(submitPullFailure());
          dispatch(alertActions.error('Account already exists'));
        } else {
          dispatch(submitPullSuccess());
          dispatch(alertActions.success('Submisssion Successful'));
        }
      },
      error => {
        dispatch(submitPullFailure(error.toString()));
        dispatch(alertActions.error(error.toString()));
      }
    );
  };

  function submitPullRequest() { return { type: dataPullConstants.SUBMIT_PULL_REQUEST }}
  function submitPullSuccess() { return { type: dataPullConstants.SUBMIT_PULL_SUCCESS }}
  function submitPullFailure() { return { type: dataPullConstants.SUBMIT_PULL_FAILURE }}
}