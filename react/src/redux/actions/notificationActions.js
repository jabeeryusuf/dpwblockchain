import { notificationConstants } from '../constants';

export const notificationActions = {
    update,
};

function update(channel, id, value) {
  return { type: notificationConstants.UPDATE,
           channel,
           id, 
           value};
}
