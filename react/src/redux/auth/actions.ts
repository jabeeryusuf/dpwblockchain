import { FcFetchClient } from "fc-rest-client";
import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { alertActions } from "../actions/alertActions";
import { createAction,  getApiClient, removeCookie, setCookie } from "../utils";

const client: FcFetchClient = getApiClient();

export enum BaseActionType {
  SIGN_IN_REQUEST = "auth/SIGN_IN_REQUEST",
  REFRESH_REQUEST = "auth/REFRESH_REQUEST",
  SIGN_OUT_REQUEST = "auth/SIGN_OUT_REQUEST",
  CHANGE_REQUEST = "auth/CHANGE_REQUEST",
  SUCCESS = "auth/SUCCESS",
  FAILURE = "auth/FAILURE",
  UPDATE_USER = "auth/UPDATE_USER"
}

export const baseActions = {
  changeRequest: (userId: string) => createAction(BaseActionType.CHANGE_REQUEST, {userId}),
  failure: (message: string) => createAction(BaseActionType.FAILURE, {message}),
  refreshRequest: () => createAction(BaseActionType.REFRESH_REQUEST),
  signInRequest: (email: string) => createAction(BaseActionType.SIGN_IN_REQUEST, {email}),
  signOutRequest: () => createAction(BaseActionType.SIGN_OUT_REQUEST),
  success: (token: any) => createAction(BaseActionType.SUCCESS, {token}),
  UPDATE_USER: (user: any) => createAction(BaseActionType.UPDATE_USER, {user}),

};

// Thunks
const signIn = (orgId: string, email: string, password: string):
  ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.signInRequest(email));
    console.log("about to call client login");
    console.log(orgId);

    try {
      const token = await client.login(orgId, email, password);
      console.log("login success");
      // store the token which has user and org info in it.
      setCookie(token);

      // Now dispatch that we are fully signed in
      dispatch(baseActions.success(token));
    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

const refresh = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    try {
      dispatch(baseActions.refreshRequest());
      console.log("about to call refresh");

      // client.setAuthToken(token)
      const token = await client.refreshToken();

      // store the token which has user in it.
      setCookie(token);

      // Now dispatch that we are fully signed in
      dispatch(baseActions.success(token));
    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

const signOut = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {
    client.logout();
    removeCookie();
    dispatch(baseActions.signOutRequest());
  };
};

export const actions = {
  change: baseActions.changeRequest,
  refresh,
  signIn,
  signOut,
};
