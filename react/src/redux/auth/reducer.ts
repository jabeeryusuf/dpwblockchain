import { ActionsUnion, getAllUsersFromCookies, getApiClient,
         getCookie, getCookieFromUser, getOrgFromCookie,
         getUserFromCookie } from "../utils";
import { baseActions, BaseActionType } from "./actions";


const client = getApiClient();

const iToken = getCookie();
const initialState = setupUserState(iToken);

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  switch (action.type) {
  case BaseActionType.SIGN_IN_REQUEST:
  case BaseActionType.REFRESH_REQUEST:
    return {
      loggingIn: true,
    };
  
  case BaseActionType.CHANGE_REQUEST:
    const token = getCookieFromUser(action.payload.userId);
    return setupUserState(token);

  case BaseActionType.SUCCESS:
    // rest api client requires a bearer token
    return setupUserState(action.payload.token);
  case BaseActionType.FAILURE:
    return {};
  case BaseActionType.UPDATE_USER:
    let user = action.payload.user 
    return {...state,user}
  case BaseActionType.SIGN_OUT_REQUEST:
    return {};
  default:
    return state;
  }
}

function setupUserState(token: string) {
  const allUsers = getAllUsersFromCookies();

  if (token) {
    client.setAuthToken(token);
    const user = getUserFromCookie(token);
    const org = getOrgFromCookie(token);
    return {
      allUsers,
      caseTypesClient: client.getCaseTypesClient(),
      casesApi: client.getCasesApi(),
      client,
      conversationApi: client.getConversationApi(),
      groupsApi: client.getGroupsApi(),
      org,
      orgApi: client.getOrgsApi(),
      token,
      user,
      usersApi: client.getUsersApi() };
  }

  return {allUsers};
}
