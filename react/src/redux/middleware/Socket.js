import io from 'socket.io-client';
import {getApiClient} from '../utils'

// keep in sync with react client
const Protocol = {
  // Core protocol
  CONNECT:              'connect',
  RECONNECT:            'reconnect',
  DISCONNECT:           'disconnect',
  CONNECT_ERR:          'connect_error',
  RECONNECT_ERR:        'reconnect_error',

  // Authentication
  AUTHENTICATE:         'authenticate',               
  AUTHENTICATE_SUCCESS: 'authenticate_success',             
  AUTHENTICATE_FAILURE: 'authtenicate_failure',

  // Messaging
  JOIN_CONVERSATION:    'join_conversation',  
  LEAVE_CONVERSATION:   'leave_conversation', 
  UPDATE_CLIENT:        'update_client',
  MESSAGE:              'message',

  // Notification
  ENABLE_NOTIFICATIONS:  'enable_notifications',
  DISABLE_NOTIFICATIONS: 'disable_notifications',
  NOTIFY:                'notify'

}


// Socket manager
export default class Socket {

  constructor(onConnectionChange, onAuthenticationChange, onSocketError, onMessage, onUpdateClient,onNotify) {
    console.log("new socket")

    this.onConnectionChange = onConnectionChange;
    this.onAuthenticationChange = onAuthenticationChange;
    this.onSocketError = onSocketError;
    this.onMessage = onMessage;
    this.onUpdateClient = onUpdateClient;
    this.onNotify = onNotify;
    this.socket = null;
    this.user_id = null;
    this.port = null;
  }



  // User clicked connect button
  connect = (user) => {
    
    console.log("connect socket");
    // Connect

    this.user = user;

    const client = getApiClient();
    let host = client.getApiUrl();
    
    this.socket = io(host, {reconnection: true, 
                            forceNew: false,
                            transports: ['websocket']});

    // Without switching to once the reconnect logic causes double events
    this.socket.once(Protocol.CONNECT, this.onConnected);
    this.socket.on(Protocol.RECONNECT, this.onReconnected);
    this.socket.on(Protocol.DISCONNECT, this.onDisconnected);
    this.socket.on(Protocol.CONNECT_ERR, this.onError);
    this.socket.on(Protocol.RECONNECT_ERR, this.onError);
  };

  onReconnected = (a) => {
    // Weird behavior of socket.io
    console.log("onReconnect: ",a);
  }

  // Received connect event from socket
  onConnected = () => {
    console.log("onConnected: ",this.socket);
    this.socket.on(Protocol.MESSAGE, this.onMessage);
    this.socket.on(Protocol.UPDATE_CLIENT, this.onUpdateClient);
    this.socket.on(Protocol.AUTHENTICATE_SUCCESS, this.onAuthSuccess);
    this.socket.on(Protocol.NOTIFY,this.onNotify);

    this.sendAuthentication();
    this.onConnectionChange(true);
  };
  
  onAuthSuccess = () => {
    console.log('onAuthSuccess:',this.socket);
    this.enableNotifications();
    this.onAuthenticationChange(true);
  }

  // Received disconnect event from socket
  onDisconnected = () => {
    console.log('onDisconnect:', this.socket);
    this.onConnectionChange(false);
  }
  
  // Send an identification message to the server
  sendAuthentication = () => this.socket.emit(Protocol.AUTHENTICATE,{userId: this.user.userId, orgId: this.user.orgId});
  enableNotifications = () => this.socket.emit(Protocol.ENABLE_NOTIFICATIONS,{foo: "bar"});

  
  // Send a message over the socket
  sendMessage = message => this.socket.emit(Protocol.MESSAGE, message);
  
  joinConversation = group_id => this.socket.emit(Protocol.JOIN_CONVERSATION,{group_id});

  // Close the socket
  disconnect = () => this.socket.close();
  
  // Received error from socket
  onError = message  => {
    console.log('on error',this.socket);
    this.onSocketError(message);
    // this screws up the auto reconnect logic
    //this.disconnect();
  };

}
