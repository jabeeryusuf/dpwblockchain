import Socket from "./Socket";


import { statusChanged, notificationActions, messageActions, vendorsActions} from "../actions";

import { socketConstants, conversationConstants } from '../constants'
import { socketActions, caseTypesActions, conversationActions, userActions, groupsActions } from '../actions';;

const socketMiddleware = store => {

  // The socket's connection state changed
  const onConnectionChange = isConnected => {
    store.dispatch(socketActions.connectionChanged(isConnected));
    store.dispatch(statusChanged(isConnected ? 'Connected' : 'Disconnected'));
  };

  const onAuthenticationChange = isAuthorized => {
    store.dispatch(socketActions.authenticationChanged(isAuthorized));
  }

  // There has been a socket error
  const onSocketError = (status) => store.dispatch(statusChanged(status, true));

  // The client has received a message
  const onIncomingMessage = message => {console.log("in incoming");store.dispatch(conversationActions.messageReceived(message))};
  
  // The server has updated us with a list of all users currently on the system
  const onUpdateClient = message => {
    //console.log('on update client')
    //console.dir(message);
      //const messageState = store.getState().messageState;
    store.dispatch(conversationActions.clientUpdateReceived(message));
  };

  const onNotify = message => {
    console.log('on notify',message);
    console.log(message);
    store.dispatch(notificationActions.update(message.channel, message.id, message.value));
  }

  const socket = new Socket(
    onConnectionChange,
    onAuthenticationChange,
    onSocketError,
    onIncomingMessage,
    onUpdateClient,
    onNotify
  );

  

  // Return the handler that will be called for each action dispatched
  return next => action => {
    
    const state = store.getState();
    
    switch (action.type) {
    case socketConstants.CONNECT_SOCKET:
      socket.connect(state.auth.user, state.socket.port);
      break;

    case socketConstants.DISCONNECT_SOCKET:
      socket.disconnect();
      break;
    
    case socketConstants.AUTH_CHANGED:
      if (action.authenticated) {
        store.dispatch(userActions.list('standard',false));
        store.dispatch(groupsActions.list());
        store.dispatch(messageActions.templateList(state.auth.user.org_id));
        store.dispatch(conversationActions.list(state.auth.user.user_id));
        store.dispatch(caseTypesActions.list());
        store.dispatch(vendorsActions.list())
      }
      break;

    case conversationConstants.SEND_MESSAGE:
   
      socket.sendMessage({
        'from': state.auth.user.user_id,
        'to': action.groupId,
        'text': action.message,
        'files': action.files
      });
      store.dispatch(conversationActions.messageSent(action.groupId));
      break;

    default:
      break;
    }

    return next(action)
  };
};

export default socketMiddleware;