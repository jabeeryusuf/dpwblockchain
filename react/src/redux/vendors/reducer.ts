import { Vendor } from "tw-api-common";
import { ActionsUnion } from "../utils";
import { baseActions, BaseActionType } from "./actions";

const initialState = {
  list: [],
  map: {},
  selectList: [],
};

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  let reduced;

  switch (action.type) {
  case BaseActionType.SUCCESS:
    // rest api client requires a bearer token
    const vendorMap: any = {};
    for (const row of action.payload.list) {
      vendorMap[row.vendorId] = row;
    }

    const selectList: any = action.payload.list.map((v: Vendor) =>
      ({label: v.name, value: v.vendorId}));
    reduced = {
      list: action.payload.list,
      map: vendorMap,
      selectList,
    };
    break;

  case BaseActionType.FAILURE:
    return {};
  default:
    return state;
  }

  // console.log("VENDOR REDUCER");
  // console.log(reduced);
  return reduced;
}
