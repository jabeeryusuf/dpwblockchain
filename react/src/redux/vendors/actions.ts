import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { Vendor } from "tw-api-common";
import { IVendorsService } from "tw-api-common";
import { alertActions } from "../actions/alertActions";
import { createAction,  getApiClient } from "../utils";

const vendorsApi: IVendorsService = getApiClient().getVendorsClient();

export enum BaseActionType {
  LIST_REQUEST = "vendors/LIST",
  FAILURE = "vendors/FAILURE",
  SUCCESS = "vendors/SUCCESS",
}

export const baseActions = {
  failure: (message: string) => createAction(BaseActionType.FAILURE, {message}),
  listRequest: () => createAction(BaseActionType.LIST_REQUEST, {}),
  success: (ilist: Vendor[]) => createAction(BaseActionType.SUCCESS, {list: ilist}),
};

// Thunks
const list = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.listRequest());
    console.log("list groups");

    try {
      const vendors = await vendorsApi.list();
      //console.log("list success");
      //console.dir(vendors);
      // Now dispatch that we are fully signed in
      dispatch(baseActions.success(vendors));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

export const actions = {
  list,
};
