import { ActionsUnion } from "../utils";
import { baseActions, BaseActionType } from "./actions";

const initialState = { list: [], map: {} };

export function reducer(state: any = initialState,
                        action: ActionsUnion<typeof baseActions>) {

  let reduced;

  switch (action.type) {
  case BaseActionType.SUCCESS:
    // rest api client requires a bearer token
    const groupMap: any = {};
    for (const row of action.payload.groups) {
      groupMap[row.group_id] = row;
    }

    reduced = { list: action.payload.groups, map: groupMap };
    break;

  case BaseActionType.FAILURE:
    return {};
  default:
    return state;
  }

  console.log("GROUP REDUCER");
  console.log(reduced);
  return reduced;
}
