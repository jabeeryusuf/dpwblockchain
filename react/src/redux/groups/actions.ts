import { AnyAction } from "redux";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { alertActions } from "../actions/alertActions";
import { createAction,  getApiClient } from "../utils";

const groupsApi = getApiClient().getGroupsApi();

export enum BaseActionType {
  LIST_REQUEST = "groups/LIST",
  FAILURE = "groups/FAILURE",
  SUCCESS = "groups/SUCCESS",
}

export const baseActions = {
  failure: (message: string) => createAction(BaseActionType.FAILURE, {message}),
  listRequest: () => createAction(BaseActionType.LIST_REQUEST, {}),
  success: (groups: any[]) => createAction(BaseActionType.SUCCESS, {groups}),
};

// Thunks
const list = (): ThunkAction<Promise<void>, {}, {}, AnyAction> => {

  // Invoke API
  return async (dispatch: ThunkDispatch<{}, {}, AnyAction>): Promise<void> => {

    dispatch(baseActions.listRequest());
    console.log("list groups");

    try {
      const groups = await groupsApi.list();
      console.log("list success");
      console.dir(groups);
      // Now dispatch that we are fully signed in
      dispatch(baseActions.success(groups));

    } catch (error) {
      dispatch(baseActions.failure(error.toString()));
      dispatch(alertActions.error(error.toString()));
    }
  };
};

export const actions = {
  list,
};
