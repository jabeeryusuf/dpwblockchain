/*global google*/ 
import React from "react"
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Polyline } from "react-google-maps"

var symbolTwo = {
  path: 'M -1,0 A 1,1 0 0 0 -3,0 1,1 0 0 0 -1,0M 1,0 A 1,1 0 0 0 3,0 1,1 0 0 0 1,0M -3,3 Q 0,5 3,3',
  strokeColor: '#00F',
  rotation: 45
};

export const MyMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `700px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  lifecycle({
    componentWillMount() {
      const refs = {};

      this.setState({
        mapProjection: null,
        zoom: 3,
        onMapMounted: ref => {
          refs.map = ref;
        },
        projectionChanged: () => {
          this.setState({
            mapProjection: refs.map.getProjection()
          });
        },
        onZoomChanged: () => {
          this.setState({
            zoom: refs.map.getZoom()
          });
        }
      });
    },
    componentDidMount() {
      this.setState({
        zoomToMarkers: map => {
          console.log("MAP>>>>>>>", map);
          if (map) {
            const bounds = new window.google.maps.LatLngBounds();
            map.props.children.forEach((child) => {
              if (child.length >= 1) {
                child.forEach((grandchild) => {
                  if (grandchild.type === Marker) {
                    if (parseInt(grandchild.props.position.lng) && parseInt(grandchild.props.position.lat)) {
                      bounds.extend(new window.google.maps.LatLng(parseInt(grandchild.props.position.lat), parseInt(grandchild.props.position.lng)));
                    }
                  }
                });
                if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                  var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 3.00, bounds.getNorthEast().lng() + 3.00);
                  var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 3.00, bounds.getNorthEast().lng() - 3.00);
                  bounds.extend(extendPoint1);
                  bounds.extend(extendPoint2);
                }
                map.fitBounds(bounds);
              }
            });
          }
        }
      });
    }
  }),
  withScriptjs,
  withGoogleMap
)((props) => 
  <GoogleMap
    defaultZoom={4}
    defaultCenter={{ lat: 39.8283, lng: -98.5795 }}
    onProjectionChanged={props.projectionChanged}
    zoom={props.zoom}
    onZoomChanged={props.onZoomChanged}
    ref={props.zoomToMarkers} center={props.center}
  >
   {/*  {props.paths ? props.paths.map(path => */}  {/*  )) : []} */}
    {props.paths && props.paths.map((path, index) => {
      console.log("checkinggggg", path, index)
      let xyz = {
        path: 'M -1,0 A 1,1 0 0 0 -3,0 1,1 0 0 0 -1,0M 1,0 A 1,1 0 0 0 3,0 1,1 0 0 0 1,0M -3,3 Q 0,5 3,3',
        strokeColor: '#00F',
        rotation: 45
      } 
      {/* const position1 = new google.maps.LatLng(37.773972, -122.431297);
      const position2 = new google.maps.LatLng(40.7128, -74.006);

      const curvature = 0.4;
      const p1 = props.mapProjection.fromLatLngToPoint(position1);
      const p2 = props.mapProjection.fromLatLngToPoint(position2);
      console.log("type of", typeof p2.x)
  // Calculating the arc.
  const e = new google.maps.Point(p2.x - p1.x, p2.y - p1.y), // endpoint
            m = new google.maps.Point(e.x / 2, e.y / 2), // midpoint
            o = new google.maps.Point(e.y, -e.x), // orthogonal
            c = new google.maps.Point(m.x + curvature * o.x, m.y + curvature * o.y);
      
      
      const pathDef = "M 0,0 " + "q " + c.x + "," + c.y + " " + e.x + "," + e.y;

      const scale = 1 / Math.pow(2, -3);
      console.log("pathDef", pathDef)
      const symbol = {
          path: pathDef,
          scale: scale,
          strokeWeight: 2,
          fillColor: "none"
      };
      console.log("symbol", symbol); */}
      return (
      <Polyline
        path={path}
        geodesic={true}
        options={{
          strokeColor: "#ff2527",
          strokeOpacity: 0.75,
          strokeWeight: 2,
          icons: [
            {
              icon: { path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW },
              offset: "100%",
            },
            {
              icon: xyz,
              offset: '50%'
            }
          ]
        }}
        key={index}
      />
      )  
    })}

    {props.markers && props.markers.map(marker =>
      (<Marker
        key={marker.id}
        id={marker.id}
        onClick={() => { marker.onClick(marker.id) }}
        onMouseOver={() => { marker.onMouseOver(marker) }}
        location={marker.location}
        qty={marker.qty}
        ponum={marker.ponum}
        position={{ lat: parseFloat(marker.lat), lng: parseFloat(marker.lng) }}
        icon={{ url: marker.color , anchor: marker.offset ? new google.maps.Point(4, 4) : null }}>
        {props.openWindow === marker.id &&
          <InfoWindow onCloseClick={marker.onClose}>
            <div>
              {marker.name &&
                <p>{"Name: " + marker.name}</p>
              }
              {marker.address &&
                <p>{"Address: " + marker.address}</p>
              }
            </div>
          </InfoWindow>
        }

      </Marker>
      ))
    }
    {/* {props.markers && props.markers.map(marker =>
      (<Marker
        key={marker.id}
        id={marker.id}
        onClick={() => { marker.onClick(marker.id) }}
        onMouseOver={() => { marker.onMouseOver(marker) }}
        location={marker.location}
        qty={marker.qty}
        ponum={marker.ponum}
        position={{ lat: parseFloat(marker.lat2), lng: parseFloat(marker.lng2) }}
        icon={{ url: marker.color2 , anchor: marker.offset ? null : null }}>
        {props.openWindow === marker.id &&
          <InfoWindow onCloseClick={marker.onClose}>
            <div>
              {marker.name &&
                <p>{"Name: " + marker.name}</p>
              }
              {marker.address &&
                <p>{"Address: " + marker.address}</p>
              }
            </div>
          </InfoWindow>
        }

      </Marker>
      ))
    } */}
  </GoogleMap>
)





