import React from 'react'
import moment from 'moment'
class TimelineItem extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      circleColor : 'fa fa-genderless m--font-success'
    }
  }
  componentWillMount() {
    if(this.props.expected>=this.props.actual){
      this.setState({
      circleColor: 'fa fa-genderless m--font-success'
    })
    }
    else{
      this.setState({
        circleColor: 'fa fa-genderless m--font-danger'
      })
    }
  }
  render(){
    return(
      <div className="m-timeline-2__item m--margin-top-30">
        <span className="m-timeline-2__item-time">{moment.unix(this.props.actual).format('MM/DD')}</span>
        <div className="m-timeline-2__item-cricle" >
          <i className={this.state.circleColor}></i>
        </div>
        <div className="m-timeline-2__item-text  m--padding-top-5">
          {this.props.children}
        </div>
      </div>
      )
      }
    }
export default TimelineItem;
