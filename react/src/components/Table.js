import ReactTable from 'react-table'
import 'react-table/react-table.css'
import { compose, withProps } from "recompose"
import React from 'react'



const Table = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyCl4SD1zRZCaNsB3M7QcSSimMLb7ku9lsc",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
)((props) =>
  <ReactTable {...props}>
    {props.children}
  </ReactTable>
)
export default Table;
