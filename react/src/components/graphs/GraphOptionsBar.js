import React from 'react'
import {Button} from 'react-bootstrap'


const GraphOptionsBar = (props) =>{
    return(
        <div style = {{margin:'0 auto'}}>
            {props.data.map((item,index)=>{
                return(
                <div key ={index} style ={{display:'inline-block', paddingRight:10}}>
                <Button onClick={item.onClick}> {item.text}</Button>
                </div>
                )
            })}
        </div>
    )
}
export default GraphOptionsBar; 