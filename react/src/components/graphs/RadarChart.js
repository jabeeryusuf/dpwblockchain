import React from 'react';
const { RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar} = require('recharts/lib');
const RadarGraph = (props)=> {
        return (
            <RadarChart cx={300} cy={250} outerRadius={150} width={600} height={500} data={props.data}>
                <PolarGrid />
                <PolarAngleAxis dataKey="name" />
                <PolarRadiusAxis />
                <Radar name="Mike" dataKey="value"stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
            </RadarChart>
        )
}
export default RadarGraph;