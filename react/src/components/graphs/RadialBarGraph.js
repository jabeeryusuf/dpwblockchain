import React from 'react';
import colorList from './colors.json';
const { RadialBarChart, RadialBar, Legend } = require('recharts');


const style = {
  top: 0,
  left: 450,
  lineHeight: '24px'
};

export default class RadialBarGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  componentWillMount() {
    let newData = this.props.data.slice()
    for (let i in this.props.data) {
      this.props.data[i].fill = colorList[i].hex;
    }
    this.setState({
      data: newData
    })
  }
  componentWillUpdate(newProps) {
    if (this.props.data !== newProps.data) {
      for (let i in newProps.data) {
        newProps.data[i].fill = colorList[i].hex;
      }
      this.setState({
        data: newProps.data
      })
    }
  }

  render() {
    if (this.state.data.length) {
      return (
        <RadialBarChart width={600} height={400} cx={200} cy={200} innerRadius={20} outerRadius={200} barSize={40} data={this.props.data}>
          <RadialBar minAngle={15} label={{ position: 'insideStart', fill: '#666' }} background clockWise={true} dataKey='value' />
          <Legend iconSize={10} width={100} height={100} layout='vertical' verticalAlign='middle' wrapperStyle={style} />
        </RadialBarChart>
      );
    }
  }
}