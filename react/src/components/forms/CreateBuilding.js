import React from 'react';
import {Button,Form } from 'react-bootstrap';


class CreateBuilding extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: this.props.data,
      value: '',
      name: '',
      address: '',
      lat: 0,
      lng: 0,
      code: '',
      selectField: "Factory",
      emptyName: false,
      emptyAddress: false,
      errorMessage: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.validationCheck = this.validationCheck.bind(this);

  }

  /**
   * Handles the change of the drop down selector object in the form
   * @param {*} e 
   */
  handleSelectChange(e) {
    this.setState({
      selectField: e
    });
  }

   /**
   * Handles the change of the inputs in the form to set the corresponding state values
   * to those that have been changed.
   * @param {*} evt 
   * @param {*} str 
   */
  handleChange(evt, str) {
    switch (str) {
      case "name":
        this.setState({
          name: evt.target.value
        });
        break;
      case "address":
        this.setState({
          address: evt.target.value
        });
        break;
      case "lat":
        this.setState({
          lat: evt.target.value
        });
        break;
      case "lng":
        this.setState({
          lng: evt.target.value
        });
        break;
      default:
        throw new Error('Something horrbile happened');
    }
  }

  /**
   * Client side checks of the form to make sure user is not putting
   * in bad information before submission of form
   */
  validationCheck() {
    let pass = true;
    if (this.state.name.length === 0) {
      pass = false;
      this.setState({
        emptyName: true,
        errorMessage: "Whatever"
      });
    }
    
    if (this.state.address.length === 0) {
      pass = false;
      this.setState({
        emptyAddress: true,
        errorMessage: "Field cannot be empty"
      });
    }

    let list = this.state.data;
    for (let i = 0; i < list.length; i++) {
      if (list[i].name === this.state.name && list[i].address === this.state.address) {
        pass = false;
        this.setState({
          emptyName: true,
          errorMessage: "Building already exists"
        });
      }
    }

    if (pass) {
      this.props.onSubmit(this.state.name, this.state.address, this.state.lat, this.state.lng, this.state.selectField);
      this.props.onCancel();
    }
  }

  render() {
    return (
      <div style={{ width: 500}}>
      <h1>Create Location</h1>
      <Form>
        <Form.Group>
          <Form.Label>Location Type</Form.Label>
            <Form.Control as="select" onValueChange={this.handleSelectChange} >
              <option>Factory</option>
              <option>Warehouse</option>
              <option>Port</option>
            </Form.Control>
          </Form.Group>
        <Form.Group>
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            value={this.state.name}
            placeholder="Enter building name"
            onChange={evt => this.handleChange(evt, "name")}
          />
          {this.state.emptyName && <Form.Label><font className="text-danger">{this.state.errorMessage}</font></Form.Label>}
        </Form.Group>
        <Form.Group>
          <Form.Label>Address</Form.Label>
          <Form.Control
            type="text"
            value={this.state.address}
            placeholder="Enter building address"
            onChange={evt => this.handleChange(evt, "address")}
          />
          {this.state.emptyAddress && <Form.Label><font className="text-danger">{this.state.errorMessage}</font></Form.Label>}
        </Form.Group>
        <Form.Group>
          <Form.Label>Latitude</Form.Label>
          <Form.Control
            style={{width: "30%"}}
            type="decimal"
            value={this.state.lat}
            placeholder="Latitude"
            onChange={evt => this.handleChange(evt, "lat")}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Longitutde</Form.Label>
          <Form.Control
            style={{width: "30%"}}
            type="decimal"
            value={this.state.lng}
            placeholder="Longitude"
            onChange={evt => this.handleChange(evt, "lng")}
          />
        </Form.Group>
      </Form>
      <Button onClick={this.validationCheck}>Submit</Button>{'\t'}<Button text="Cancel" onClick={this.props.onCancel}>Cancel</Button>
      </div>
    )
  }
}

export default CreateBuilding;