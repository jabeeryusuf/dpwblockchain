import React from 'react';
import { Button, Form, InputGroup } from 'react-bootstrap';
import { GoX } from "react-icons/go"


class FormSearch extends React.Component {

  render() {
    let {placeholder, value, onChange, onClick} = this.props;

    return (
      <InputGroup className='form-search'>
        <Form.Control type="text"
          placeholder={placeholder}
          value={value}
          onChange={onChange} />
        <InputGroup.Append>
          <Button >
            <GoX onClick={onClick} />
          </Button>
        </InputGroup.Append>
      </InputGroup>

    )
  }
}

export default FormSearch;