import React from 'react';
import PopUpWindow from '../PopUpWindow.js';
import { Button, Form, DropdownButton, Dropdown } from 'react-bootstrap';
import SureWindow from '../SureWindow';
import md5 from 'md5';

class DeleteBuilding extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data,
      name: '',
      address: '',
      found: false,
      emptyName: false,
      emptyAddress: false,
      emptyBuilding: false,
      errorMessage: '',
      addErrorMessage: '',
      showSure: false,
      sure: false,
      selectField: 'factory',
    }

    this.handleChange = this.handleChange.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.handleSureClick = this.handleSureClick.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }

  /**
   * Handles the change of the inputs in the form to set the corresponding state values
   * to those that have been changed.
   * @param {*} evt 
   * @param {*} str 
   */
  handleChange(evt, str) {
    switch (str) {
      case "name":
        this.setState({
          name: evt.target.value
        });
        break;
      case "address":
        this.setState({
          address: evt.target.value
        });
        break;
      default:
        throw new Error('What field was that?')
    }
  }

  onFilterChange(filter, key) {
    this.setState({
      [filter]: key,
    });
  }


  handleSureClick(str) {
    this.setState({
      showSure: false
    });

    switch (str) {
      case "yes":
        this.props.onSubmit(this.state.name, this.state.address, this.state.selectField.toLowerCase());
        this.props.onCancel();
        break;
      case "no":
        this.props.onCancel();
        break;
      default:
        throw new Error('Which button was that?');
    }
  }

  /**
   * Client side checks of the form to make sure user is not putting
   * in bad information before submission of form
   */
  async validationCheck() {
    let pass = true;

    let list = this.state.data;
    let found = false;
    for (let obj of list) {
      if ((this.state.name === obj.name && this.state.address === obj.address) && this.state.selectField === obj.type) {
        found = true;
      }
    }

    if (!found && this.state.name.length > 0) {
      this.setState({
        emptyBuilding: true,
        errorMessage: "Building does not exist, check the type, name and address again"
      });
    } else {
      this.setState({
        emptyBuilding: false
      });
    }

    if (this.state.name.length === 0) {
      pass = false;
      this.setState({
        emptyName: true,
        errorMessage: "Field Cannot Be Empty"
      });
    } else {
      this.setState({
        emptyName: false
      });
    }

    if (this.state.address.length === 0) {
      pass = false;
      this.setState({
        emptyAddress: true,
        addErrorMessage: "Field Cannot Be Empty"
      });
    } else {
      this.setState({
        emptyAddress: false
      });
    }

    if (pass && found) {
      this.setState({
        showSure: true
      });
    }

  }

  render() {
    return (
      <div style={{ width: 500 }}>
        <PopUpWindow shown={this.state.showSure}
          page={<SureWindow onSubmit={() => this.handleSureClick("yes")}
            onCancel={() => this.handleSureClick("no")} />} />
        <h1>Delete Location</h1>
        <Form>
          <Form.Group>
            <Form.Label>Location Type</Form.Label>
            <DropdownButton variant="outline-secondary" title={this.state.selectField === "NONE" ? "NONE" : this.state.selectField}
              onSelect={(k) => this.onFilterChange("selectField", k)} >
              <Dropdown.Item eventKey="factory" key="factory">Factory</Dropdown.Item>
              <Dropdown.Item eventKey="warehouse" key="warehouse">Warehouse</Dropdown.Item>
            </DropdownButton>
          </Form.Group>
          <Form.Group>
            <Form.Label>Name of Location</Form.Label>
            <Form.Control
              type="text"
              value={this.state.name}
              placeholder="Enter name of building to delete"
              onChange={evt => this.handleChange(evt, "name")} />
            {(this.state.emptyName || this.state.emptyBuilding) && <Form.Text><font className="text-danger">{this.state.errorMessage}</font></Form.Text>}
          </Form.Group>
          <Form.Group>
            <Form.Label>Address of Location</Form.Label>
            <Form.Control
              type="text"
              value={this.state.address}
              placeholder="Enter address of building to delete"
              onChange={evt => this.handleChange(evt, "address")} />
            {this.state.emptyAddress && <Form.Text><font className="text-danger">{this.state.addErrorMessage}</font></Form.Text>}
          </Form.Group>
        </Form>
        <Button type="submit" onClick={this.validationCheck}>Submit</Button>
        {'\t'}
        <Button onClick={this.props.onCancel}>Cancel</Button>
      </div>
    )
  }
}

export default DeleteBuilding;