import React from 'react';
import { Form, Button, Row, Col, Alert } from 'react-bootstrap';

class NamesForNewUsers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      emails: [],
      names: []
    }

    this.validateNames = this.validateNames.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

  }

  componentDidMount() {
    let emailList = [];
    let nameSpaces = [];
    for (let email in this.props.emails) {
      emailList.push(this.props.emails[email]);
      nameSpaces.push({
        name: "",
        valid: true
      });
    }

    this.setState({
      emails: emailList,
      names: nameSpaces
    });
  }

  handleNameChange = (event) => {
    const { name, value } = event.target;
    let newNames = this.state.names;

    newNames[name].name = value;

    this.setState({
      names: newNames
    }, () => console.log(this.state.names));

  }

  validateNames() {
    let names = this.state.names;

    let allGood = true;
    for (let name of names) {
      if (isEmptyOrSpaces(name.name)) {
        name.valid = false;
        allGood = false;
      } else {
        name.valid = true;
      }
    }

    this.setState({
      names: names
    }, () => {
      if (allGood) {
        this.onSubmit();
      }
    });
    
  }

  onSubmit() {
    const { emails, names } = this.state;
    let newContacts = [];

    for (let i = 0; i < emails.length; i++) {
      newContacts.push({
        email: emails[i],
        name: names[i].name
      });
    }

    this.props.onSubmit(newContacts);
  }

  render() {
    const { emails, names } = this.state;
    return (
      <> 
        <Row className="justify-content-center">
        {this.props.successMessage ? <Alert variant="success" dismissible>{this.props.successMessage}</Alert> : ''}
        {this.props.errorMessage ? <Alert variant="danger" dismissible>{this.props.errorMessage}</Alert> : ''} 
          <Col sm={{ span: 8 }}>
            <font size="3">We see that you've added some new contacts to the system. Please enter names for these new contacts</font>
          </Col>
        </Row>
        <Form>
          <br/>
          {emails.length > 0 && emails.map((email, idx) => {
            return (
              <>
                <Row>
                    <Col>
                      <Form.Control type="email"
                                    value={email}
                                    disabled />
                    </Col>
                    <Col>
                      <Form.Control name={idx}
                                    type="text"
                                    value={names[idx].name}
                                    placeholder="Enter their name"
                                    onChange={(e) => this.handleNameChange(e)}
                                    isInvalid={!names[idx].valid} />
                    </Col>
                </Row>
                <br/>
              </>
            )
          })}
        </Form>
        <br/>
        <Col sm={{ span: 6 }}>
          <Button block onClick={this.validateNames} disabled={this.props.submitting}>{this.props.submitting ? "Submitting..." : "Submit"}</Button>
        </Col>
      </>
    )
  }
}

function isEmptyOrSpaces(str){
  return str === null || str.match(/^\s*$/) !== null;
}

export default NamesForNewUsers;