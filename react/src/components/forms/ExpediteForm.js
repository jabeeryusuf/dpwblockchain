import React from 'react';
import {Button, Form, Row, Col} from 'react-bootstrap';

export default class ExpediteForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            field1: this.props.options[0],
            options: [],
        }
        this.renderOptions = this.renderOptions.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    handleSubmit() {
        if (this.state.field1) {
            this.props.onSubmit(this.state.field1, this.props.action);
        }

    }
    onChange = (event) => {
        const { value } = event.target;
        this.setState({
            field1: value
        });
    }
    renderOptions() {
        let update = []
        for (let i = 0; i < this.props.options.length; i++) {
            update.push(<option key={this.props.options[i]} value={this.props.options[i]}>{this.props.options[i]}</option>)
        }
        return update

    }
    render() {
        return (
            <div style={{ width: 400}}>
                <p style={{ color: 'red' }}> {this.props.errorMessage} </p>
                <p style={{ color: 'green' }}> {this.props.successMessage} </p>
                {this.props.head}
                <Row>
                    <Col className="justify-content-md-center">
                        <Form>
                        <Form.Control as="select" onSelect={(e) => this.onChange(e)} >
                            {this.renderOptions()}
                        </Form.Control>
                        </Form>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Button type="submit" disabled={this.props.submitting} block onClick={() => {this.handleSubmit()}}>{this.props.submitting ? "Submitting..." : "Submit"}</Button>
                </Row>
            </div>
        )
    }
}