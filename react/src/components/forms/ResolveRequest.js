import React from 'react';
import {Button, Row, Col,Form, FormGroup, FormControl, FormLabel} from 'react-bootstrap';

class ResolveRequest extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.req.action_id
    };

  }

  render() {
    return (
      <div style={{ width:  500}}>
        <p style={{ color: 'red'}}>{this.props.errorMessage}</p>
        <p style={{ color: 'green'}}>{this.props.successMessage}</p>
        <h1>Request Information</h1>
        <br></br>
        <Form>
          <FormGroup>
            <FormLabel>Request Type</FormLabel>
            <FormControl 
              disabled
              type="text"
              value={this.props.req.type} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Container</FormLabel>
            <FormControl
              disabled
              type="text"
              value={this.props.req.container_id} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Request Date</FormLabel>
            <FormControl
              disabled
              type="text"
              value={this.props.req.request_date} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Request Made By</FormLabel>
            <FormControl 
              disabled
              type="text"
              value={this.props.req.requested_by} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Location</FormLabel>
            <FormControl
              disabled
              type="text"
              value={this.props.req.pod} />
          </FormGroup>
          <FormGroup>
            <FormLabel>Status</FormLabel>
            <FormControl 
              disabled
              type="text"
              value={this.props.req.action_status} />
          </FormGroup>
        </Form>
        <Row>
          <Col md={6}>
            <Button text={<font className="text-success">Approve</font>} onClick={() => this.props.onClick(this.state.id, "approved")} />{'\t'}
            <Button text={<font className="text-danger">Deny</font>} onClick={() => this.props.onClick(this.state.id, "denied")} />{'\t'}
          </Col>
          <Col md={4}></Col>
          <Col mdPush={3}>
            <Button text="Cancel" onClick={this.props.onCancel()} />
          </Col>
        </Row>
      </div>
    )
  }
}

export default ResolveRequest;