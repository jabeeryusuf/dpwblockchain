import React from "react";
import { Dropdown, DropdownButton, Form, InputGroup} from "react-bootstrap";
import { connect } from "react-redux";
import Select from "react-select";
import { Asset, AssetType } from "tw-api-common";

interface IFormGroupSelectProps {
  submitted: boolean;
  groupsList: any[];
  vendorsList: any[];
  defaultAsset?: Asset;
  caseTypeAsset?: Asset;
  onChange(asset: Asset): void;
}

class FormGroupSelect extends React.Component<IFormGroupSelectProps, any> {
  private atMap: any;

  constructor(props: IFormGroupSelectProps) {

    super(props);
    const {defaultAsset, vendorsList} = props;

    this.atMap = {};
    this.atMap[AssetType.GROUP] = {name: "Group", render: this.renderGroups };
    this.atMap[AssetType.VENDOR] = {name: "Vendor", render: this.renderVendors};

    let defaultType = AssetType.GROUP;
    let defaultValue;

    if (defaultAsset) {
      defaultType = defaultAsset.type;
      if (defaultType === AssetType.VENDOR) {
        defaultValue = vendorsList.find((e: any) => e.value === defaultAsset.id);
      } else {
        defaultValue = defaultAsset.id;
      }
    }

    this.state = {
      type: defaultType,
      value: defaultValue,
    };

  }

  public componentDidUpdate(prevProps: any) {
    const { vendorsList, caseTypeAsset } = this.props;

    if (prevProps.caseTypeAsset !== caseTypeAsset) {
      if (caseTypeAsset) {
        this.setState({
          type: caseTypeAsset.type,
          value: caseTypeAsset.type === AssetType.GROUP ?
            caseTypeAsset.id : vendorsList.find((e: any) => e.value === caseTypeAsset.id),
        });
      }
    }
  }

  public render() {

    const {submitted, groupsList} = this.props;
    const { value } = this.state;
    console.log(this.props);
    const invalid = submitted && !value;

    return (
      <InputGroup >

        <DropdownButton as={InputGroup.Append} variant="outline-secondary" onSelect={this.handleSelect}
                        title={this.atMap[this.state.type].name} id="input-group-dropdown-1">
          {Object.keys(this.atMap).map((k, i) =>
            <Dropdown.Item key={i} eventKey={k}>{this.atMap[k].name}</Dropdown.Item>)}
        </DropdownButton>

        {this.atMap[this.state.type].render()}

      </InputGroup>
    );
  }

  private handleSelect = (eventKey: any, event: object) => {
    this.setState({
      type: eventKey,
      value: undefined,
    });
  }

  private onChangeGroups = (event: any) => {
    const {value}  = event.target;
    const {onChange} = this.props;

    onChange(new Asset(AssetType.GROUP, value));

    this.setState({value});
  }

  private renderGroups = () => {
    const { value } = this.state;
    const { groupsList, onChange } = this.props;

    return (
      <Form.Control as="select" placeholder="please select a group" name={name} value={value}
                    onChange={this.onChangeGroups}>

        {!value && <option key={0} value={undefined}></option> }
        {groupsList.map((group, i) => (
          <option key={i + 1} value={group.group_id}>{group.group_name}</option>
        ))}
      </Form.Control>
    );
  }

  private onChangeVendors = (option: any) => {
    const { onChange } = this.props;

    onChange(new Asset(AssetType.VENDOR, option.value));

    console.log("OPTION:", option);
    this.setState({ value: option });
  }

  private renderVendors = () => {
    const { vendorsList } = this.props;
    const { value } = this.state;

    // console.log("VENDOR LIST");
    // console.log(vendorsList);

    const customStyles = {
      container: (provided: any, state: any) => ({
        ...provided,
        // width: "100%",
      }),
    };

    return (
      <Select className="react-select" classNamePrefix="react-select"
        value={value} styles={customStyles} options={vendorsList} onChange={this.onChangeVendors}/>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    groupsList: state.groups.list,
    vendorsList: state.vendors.selectList,
  };
}

export default connect(mapStateToProps)(FormGroupSelect);
