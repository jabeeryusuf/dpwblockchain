import React from 'react';
import {Form,Button} from 'react-bootstrap';

export default class ContainerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: []
        }
        this.renderOptions = this.renderOptions.bind(this);
        this.onChange = this.onChange.bind(this);
        this.input = React.createRef();
    }
    handleSubmit() {
        if (this.state.field1) {
            this.props.onSubmit(this.state.field1, this.props.action, this.props.destination, this.props.destName);
            this.input.current._clearInput()
        }
    }
    handleEnter(){
        this.handleSubmit();
    }
    onChange(e) {
        this.setState({
            field1: e
        })
    }
    renderOptions() {
        let update = []
        for (let i in this.props.options) {
            update.push({ title: this.props.options[i], value: this.props.options[i] })
        }
        return update

    }
    render() {
        return (
            <div style={{ width: 400}}>
                <p style={{ color: 'red' }}> {this.props.errorMessage} </p>
                <p style={{ color: 'green' }}> {this.props.successMessage} </p>
                <Form.Control ref={this.input} onEnter ={()=>{this.handleEnter()}} onValueChange={this.onChange} />
                <Button text="Add Container(s)" onClick={() => {this.handleSubmit()}} /> 

            </div >
        )
    }
}