import React from 'react';
import { Form, Button, Col, Row, Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withLoader } from '../';

function validateEmail(email) {
    let re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function hasDuplicates(arr) {
  return (new Set(arr)).size !== arr.length;
}

class CreateGroup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      emails: [{ email: '', valid: true }],
      groups: [],
      options: [],
      // existingUsers: [],
      existingGroups: [],
      submitted: false
    }

    this.addClick = this.addClick.bind(this);
    this.addGroup = this.addGroup.bind(this);
  }

  async componentDidMount() {
    const { startLoading, stopLoading} = this.props;

    startLoading();
    //maybe compare users entered to ones that already exist?
    //let users = await this.props.client.getStuff(`SELECT * FROM users`);
    let userGroups = await this.props.client.getStuff(`SELECT group_id, group_name FROM groups WHERE type != 'case' AND type != 'factory' AND org_id='${this.props.org.org_id}'`);
    let providerGroups = await this.props.client.getStuff(`SELECT group_id, vendor_name AS group_name from vendors where org_id='${this.props.org.org_id}'`);
    let existingGroups = userGroups.concat(providerGroups);
    
    this.setState({
      // existingUsers: users,
      existingGroups: existingGroups
    });

    if (this.props.group) {
      let groupCopy = JSON.parse(JSON.stringify(this.props.group));
      
      let groups = [];
      if (groupCopy.childGroups) {
        for (let i = 0; i < groupCopy.childGroups.length; i++) {
          groups[i] = {
            value: groupCopy.childGroups[i].groupID 
          }
        }
      }

      for (let user of groupCopy.users) {
        user.valid = true;
      }

      this.setState({
        name: groupCopy.groupName,
        emails: groupCopy.users,
        groups: groups
      });
    }
    
    this.getGroupOptions();
    stopLoading();
  }


  addClick() {
    let newEmails = this.state.emails;
    newEmails.push({ email: '', valid: true });
    
    this.setState({
      emails: newEmails
    });
  }

  addGroup() {
    let newGroups = this.state.groups;
    newGroups.push({ value: this.state.existingGroups[0].group_id });

    this.setState({
      groups: newGroups
    });
  }

  handleNameChange = (event) => {
    const { value } = event.target;
    
    this.setState({
      name: value
    });
  }

  handleEmailChange = (event) => {
    const { name, value } = event.target;
    let emails = this.state.emails;
    emails[name].email = value;

    this.setState({
      emails: emails
    });
  }

  handleSelectChange = (event) => {
    const { name, value } = event.target;
    let groups = this.state.groups;
    groups[name].value = value;

    this.setState({
      groups: groups
    });
  }

  removeEmailSlot(index) {
    let emails = this.state.emails;
    emails.splice(index, 1);

    this.setState({
      emails: emails
    });
  }

  removeGroupSlot(index) {
    let groups = this.state.groups;
    groups.splice(index, 1);

    this.setState({
      groups: groups
    });
  }

  getGroupOptions() {
    const { existingGroups } = this.state;

    let groups = [];
    for (let group of existingGroups) {
      if (this.props.group) {
        if (group.group_id !== this.props.group.groupID) {
          groups.push(<option key={group.group_id} value={group.group_id}>{group.group_name}</option>);  
        }
      } else {
        groups.push(<option key={group.group_id} value={group.group_id}>{group.group_name}</option>);
      }
    }

    this.setState({
      options: groups
    });

  }

  onCreate() {
    const { name } = this.state;
    let emails = this.state.emails;
    let groups = this.state.groups;
    let duplicateEmails = false;
    let duplicateGroups = false;
    let allGood = true;
    let errorMessage = "";
    let emailList = [];
    let groupList = [];

    for (let emailObj of emails) {
      emailList.push(emailObj.email);
    }

    for (let groupObj of groups) {
      groupList.push(groupObj.value);
    }

    if (name.length === 0) {
      allGood = false;
    }

    if (hasDuplicates(emailList)) {
      allGood = false;
      duplicateEmails = true;
    } 

    if (hasDuplicates(groupList)) {
      allGood = false;
      duplicateGroups = true;
    }

    if (duplicateEmails) {
      errorMessage = "Cannot have duplicate entries";
    }

    if (duplicateGroups) {
      errorMessage = "Cannot have duplicate groups";
    }

    for (let email of emails) {
      if (!validateEmail(email.email)) {
        allGood = false;
        email.valid = false;
      } else {
        email.valid = true;
      }
    }

    this.setState({
      emails: emails,
      errorMessage: errorMessage,
      submitted: true
    });

    if (allGood) {
      this.props.onSubmit(name, emailList, groupList);
    }
  }

  render() {

    const { name, submitted, groups, options } = this.state;
    let nameInvalid = !name && submitted;
    let nameInvalidMsg = "Field cannot be empty"

    return (
      <>
        {this.props.successMessage ? <Alert variant="success" dismissible>{this.props.successMessage}</Alert> : ''}
        {this.props.errorMessage || this.state.errorMessage ? <Alert variant="danger" dismissible>{this.props.errorMessage ? this.props.errorMessage : this.state.errorMessage}</Alert> : ''}        
        <Form.Group>
          <Form.Label>Group Name</Form.Label>
          <Form.Control   style={{ width: "75%"}}
                          name="groupName"
                          type="text"
                          value={this.state.name}
                          disabled={this.props.type === 'org' || this.props.submitting}
                          placeholder="Enter a group name"
                          onChange={(e) => this.handleNameChange(e)}
                          isInvalid={nameInvalid} />
          {nameInvalid ? <Form.Control.Feedback type="invalid">{nameInvalidMsg}</Form.Control.Feedback> : ''}
        </Form.Group>
        <br/>
        <Form.Group>
          <Form.Label>Email Address</Form.Label>
          {this.state.emails.map((email, index) => {
            let margin = 10;
            if (index === 0) {
              margin = 0;
            }
            return (
              <Form inline key={index}>
                  <Form.Control name={index}
                                key={index}
                                type="email"
                                value={this.state.emails[index].email}
                                disabled={this.props.submitting}
                                placeholder="name@example.com"
                                style={{ marginTop: margin, width: 350 }}
                                onChange={(e) => this.handleEmailChange(e)}
                                isInvalid={!this.state.emails[index].valid} />
                  {this.state.emails.length > 1 ? <Button key={index+100} variant="link" size="sm" onClick={() => this.removeEmailSlot(index)}>Remove</Button> : ''}
                  {!this.state.emails[index].valid ? <Form.Control.Feedback key={index+200} type="invalid">Must be a valid email address</Form.Control.Feedback> : ''}
              </Form>
            );
          })}
        </Form.Group>
        <br/>
        {groups.length > 0 ? 
          <Form.Group>
            <Form.Label>Groups</Form.Label>
            {groups.map((group, index) => {
              let margin = 10;
              if (index === 0) {
                margin = 0;
              }
              return (
                <div>
                  <Form inline key={index}>
                    <Form.Control as="select"
                                  name={index}
                                  key={index}
                                  style={{ marginTop: margin, width: 350 }}
                                  value={group.value}
                                  disabled={this.props.submitting}
                                  onChange={(e) => this.handleSelectChange(e)} >
                    {options}
                    </Form.Control>
                    <Button key={index+1000} size="sm" variant="link" onClick={() => this.removeGroupSlot(index)}>Remove</Button>
                  </Form>
                </div>
              )
            })}
          </Form.Group> : ''
        }
        <Button size="sm" variant="link" onClick={this.addClick}>Add email</Button>
        <Button size="sm" variant="link" onClick={this.addGroup}>Add group</Button>
        <br/>
        <br/>
        <Row className="justify-content-center">
          <Col sm={{ span: 6 }}>
            <Button block disabled={this.props.submitting} onClick={() => this.onCreate()}>{this.props.submitting ? "Submitting..." : this.props.footerButton}</Button>
          </Col>
        </Row>
        <br/>
      </>
    )
  }
}

function mapStateToProps(state) {
  return {
    client : state.auth.client,
    org: state.auth.org
  }
}

export default withLoader(connect(mapStateToProps)(CreateGroup));