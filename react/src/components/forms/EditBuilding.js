import React from 'react';
import { Button, Form, DropdownButton, Dropdown } from 'react-bootstrap';
import md5 from 'md5';

class EditBuilding extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data,
      id: this.props.item.id,
      name: this.props.item.name,
      address: this.props.item.address,
      code: this.props.item.code,
      lat: this.props.item.lat,
      lng: this.props.item.lng,
      selectField: this.props.item.type,
      emptyCode: false,
      emptyName: false,
      emptyAddress: false,
      errorMessage: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.validationCheck = this.validationCheck.bind(this);
    this.onSubmitDoWork = this.onSubmitDoWork.bind(this);
    this.onFilterChange = this.onFilterChange.bind(this);
  }


  /**
   * Handles the change of the inputs in the form to set the corresponding state values
   * to those that have been changed.
   * @param {*} evt 
   * @param {*} str 
   */
  handleChange(evt, str) {
    switch (str) {
      case "name":
        this.setState({
          name: evt.target.value
        });
        break;
      case "address":
        this.setState({
          address: evt.target.value
        });
        break;
      case "lat":
        this.setState({
          lat: evt.target.value
        });
        break;
      case "lng":
        this.setState({
          lng: evt.target.value
        });
        break;
      case "code":
        this.setState({
          code: evt.target.value
        });
        break;
      default:
        throw new Error('Something horrbile happened');
    }
  }

  /**
   * Client side checks of the form to make sure user is not putting
   * in bad information before submission of form
   */
  validationCheck() {
    let pass = true;
    if (this.state.name.length === 0) {
      pass = false;
      this.setState({
        emptyName: true,
        errorMessage: "Field Cannot Be Empty"
      });
    } else {
      this.setState({
        emptyName: false
      });
    }

    if (this.state.address.length === 0) {
      pass = false;
      this.setState({
        emptyAddress: true,
        errorMessage: "Field Cannot Be Empty"
      });
    } else {
      this.setState({
        emptyAddress: false
      });
    }

    let list = this.state.data;
    for (let obj of list) {
      if (obj.name === this.state.name && obj.address === this.state.address && obj.id !== this.state.id) {
        pass = false;
        this.setState({
          emptyName: true,
          errorMessage: "Separate Building Already Exists"
        });
      }
    }

    if (pass) {
      if (this.state.code.length === 0) {
        const code = (md5(this.state.name)).slice(0, 10);
        this.setState({
          code: code
        }, () => this.onSubmitDoWork())
      } else {
        this.onSubmitDoWork();
      }
    }
  }

  onSubmitDoWork() {
    this.props.onSubmit(this.state.id, this.state.name, this.state.code, this.state.address, this.state.lat, this.state.lng, this.state.selectField.toLowerCase());
    this.props.onCancel();
  }

  onFilterChange(filter, key) {
    this.setState({
      [filter]: key,
    });
  }

  render() {
    return (
      <div style={{ width: 500 }}>
        <h1>{this.props.header}</h1>
        <Form>
          <Form.Group>
            <Form.Label>Location Type</Form.Label>
            <DropdownButton variant="outline-secondary" title={this.state.selectField === "NONE" ? "NONE" : this.state.selectField}
              onSelect={(k) => this.onFilterChange("selectField", k)} >
              <Dropdown.Item eventKey="factory" key="factory">Factory</Dropdown.Item>
              <Dropdown.Item eventKey="warehouse" key="warehouse">Warehouse</Dropdown.Item>
            </DropdownButton>
          </Form.Group>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              value={this.state.name}
              placeholder="Enter building name"
              onChange={evt => this.handleChange(evt, "name")}
            />
            {this.state.emptyName && <Form.Text><font className="text-danger">{this.state.errorMessage}</font></Form.Text>}
          </Form.Group>
          <Form.Group>
            <Form.Label>Address</Form.Label>
            <Form.Control
              type="text"
              value={this.state.address}
              placeholder="Enter building address"
              onChange={evt => this.handleChange(evt, "address")}
            />
            {this.state.emptyAddress && <Form.Text><font className="text-danger">{this.state.errorMessage}</font></Form.Text>}
          </Form.Group>
          <Form.Group>
            <Form.Label> Code </Form.Label>
            <Form.Control
              type="text"
              value={this.state.code}
              placeholder="Factory/ Warehouse Code, If available"
              onChange={evt => this.handleChange(evt, "code")}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Latitude</Form.Label>
            <Form.Control
              style={{ width: "30%" }}
              type="decimal"
              value={this.state.lat}
              placeholder="Latitude"
              onChange={evt => this.handleChange(evt, "lat")}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Longitutde</Form.Label>
            <Form.Control
              style={{ width: "30%" }}
              type="decimal"
              value={this.state.lng}
              placeholder="Longitude"
              onChange={evt => this.handleChange(evt, "lng")}
            />
          </Form.Group>
        </Form>
        <Button onClick={this.validationCheck}>Submit</Button>{'\t'}<Button onClick={this.props.onCancel}>Cancel</Button>
      </div>
    )
  }
}

export default EditBuilding;