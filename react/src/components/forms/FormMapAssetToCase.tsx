import React from "react";
import { Button, Col, Row  } from "react-bootstrap";
import { connect } from "react-redux";
import { Asset } from "tw-api-common";
import { FormGroupSelect } from ".";

interface IFormMapAssetProps {
  saving: boolean;
  removing: boolean;
  onSubmit: any;
}

class FormMapAssetToCase extends React.Component<IFormMapAssetProps, any> {
  constructor(props: IFormMapAssetProps) {
    super(props);

    this.state = {
      assetId: null,
      assetType: null,
      valid: false,
    };

  }

  public render() {
    const { saving, removing } = this.props;

    return (
      <div>
        <Row>
          <Col>
            <FormGroupSelect onChange={this.handleAssetChange} submitted={false} />
          </Col>
        </Row>
        <Row className="justify-content-center mt-3">
          <Button style={{ width: 175 }} onClick={this.onSave}
            disabled={saving || removing}>{saving ? "Saving..." : "Save"}
          </Button>

          <Button style={{ width: 175 }} className="ml-2"
              onClick={() =>
                this.props.onSubmit({
                  target: {
                    action: "remove",
                    idValue: null,
                    type: "asset",
                    typeValue: null,
                  },
                })}
              variant="outline-danger"
              disabled={saving || removing}>{removing ? "Removing..." : "Remove Asset"} </Button>
        </Row>
      </div>
    );
  }

  private handleAssetChange = (asset: Asset) => {
    this.setState({
      assetId: asset.id,
      assetType: asset.type,
    });
  }

  private onSave = () => {
    const { assetType, assetId } = this.state;
    const { onSubmit } = this.props;

    const type = assetType === null ? null : assetType;
    const id = assetId === null ? null : assetId;
    const ev: any = {
      target: {
        action: "save",
        idValue: id,
        type: "asset",
        typeValue: type,
      },
    };

    onSubmit(ev);
  }
}

function mapStateToProps(state: any) {
  return {
    groupsList : state.groups.list,
  };
}

export default connect(mapStateToProps)(FormMapAssetToCase);
