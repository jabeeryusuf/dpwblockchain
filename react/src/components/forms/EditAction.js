/**
 * Serves as the form for when user wants to make a change to a current request
 */

import React from 'react';
import {Button, Form, Row, Col, Alert} from 'react-bootstrap';
import VerticalModal from '../VerticalModal.js';
import SureWindow from '../SureWindow';

class EditAction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.row.action_id,
      container: this.props.row.container_id,
      status: this.props.row.action_status,
      location: this.props.row.name,
      locationContainer: this.props.row.location_container_id,
      requestDate: this.props.row.request_date,
      type: this.props.row.type,
      showSure: false
    }
    
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSureClick = this.handleSureClick.bind(this);
  }

  /**
   * Handles the change of the select component in the form
   * @param {*} e 
   */
  handleSelectChange = (event) => {
    const { value } = event.target;
    console.log(value);
    this.setState({
      type: value
    });
  }

  /**
   * Handles the click of the "Delete Request" Button on the form.
   * Will show the "Are You Sure" window
   */
  handleDelete() {
    this.setState({
      showSure: true
    });
  }

  /**
   * Handles the click of the "Are You Sure" window
   * @param {*} str 
   */
  handleSureClick(str) {

    this.setState({
      showSure: false
    });

    switch(str) {
      case "yes":
        this.props.onDelete(this.state.id);
        break;
      case "no":
        this.props.onCancel();
        break;
      default:
        throw new Error('Which button was that?');
    }
  }

  render() {
    let modalClose = () => this.setState({ showSure: false });

    return (
      <div style={{ width: 500 }}>
      <VerticalModal show={this.state.showSure}
                     size={"sm"}
                     title={<font size="3">Are You Sure?</font>}
                     onHide={modalClose} 
                     page={<SureWindow onSubmit={() => this.handleSureClick("yes")}
                                       onCancel={() => this.handleSureClick("no")} />} />
        {this.props.errorMessage && <Alert variant="danger">{this.props.errorMessage}</Alert>}
        {this.props.successMessage && <Alert variant="success">{this.props.successMessage}</Alert>}
        {/* <p style={{ color: 'red'}}>{this.props.errorMessage}</p>
        <p style={{ color: 'green'}}>{this.props.successMessage}</p> */}
        <Form>
          <Form.Group>
            <Form.Label>Container</Form.Label>
            <Form.Control
              disabled
              type="text"
              value={this.state.container} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Location</Form.Label>
            <Form.Control
              disabled
              type="text"
              value={this.state.location} />
          </Form.Group>
          <Form.Group>
            <Form.Label>Request Date</Form.Label>
            <Form.Control
              disabled
              type="text"
              value={this.state.requestDate} />
          </Form.Group>
            <Form.Label>Status</Form.Label>
            <Form.Control as="select" onChange={(e) => this.handleSelectChange(e)} defaultValue={this.props.row.type}>
              <option key={"expedite"} value={"expedite"}>{"expedite"}</option>
              <option key={"hold"} value={"hold"}>{"hold"}</option>
            </Form.Control>
        </Form>
        <br/>
        <Row>
          <Col sm={{ span: 4 }}>
            <Button onClick={() => {this.props.onSubmit(this.state.id, this.state.type)}}>Submit Request</Button>{'\t'}
          </Col>
          
          <Col sm={{ offset: 4 }}>
            <Button variant="outline-danger" onClick={() => this.handleDelete()}>Delete Request</Button>
          </Col>
        </Row>
      </div>
    )
  }
}

export default EditAction;