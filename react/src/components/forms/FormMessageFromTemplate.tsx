import * as React from "react";
import { Alert, Button, Col, FormControl, FormGroup, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { MessageTemplate } from "tw-api-common";
import { messageActions } from "../../redux/actions";
import VerticalModal from "../VerticalModal.js";
import NameMessageTemplate from "./NameMessageTemplate";

interface IFormMessageFromTemplateProps {
  messageName: string;
  subjectName: string;
  templateId: string;
  messageValue: string;
  subjectValue: string;
  templateList: any;
  templateMap: any;
  org: any;
  alert: any;
  saveTemplate: any;
  onChange: React.FormEventHandler;
  onTemplateChange: React.FormEventHandler;
}

class FormMessageFromTemplate
extends React.Component <IFormMessageFromTemplateProps, any> {

  constructor(props: IFormMessageFromTemplateProps) {
    super(props);

    this.state = {
      showSaveTemplate: false,
      submitting: false,
      templateChanged: false,
      templateName: "",
    };

    this.onSaveClick = this.onSaveClick.bind(this);

  }

  public render() {
    const { messageName, subjectName, messageValue, subjectValue, alert } = this.props;

    return (
      <FormGroup>
        { alert.message &&
          <Alert dismissible variant={alert.type}>{alert.message}</Alert>
        }

        {this.state.showSaveTemplate && this.renderSaveTemplate()}

        {this.renderTemplateSelect()}

        <FormControl className="mt-3"
                     as="input"
                     type="text"
                     placeholder="Enter message subject"
                     name={subjectName}
                     value={subjectValue}
                     onChange={(e: any) => this.handleMessageChange(e)} >
        </FormControl>

        <FormControl className="mt-3"
                     as="textarea"
                     rows="10"
                     name={messageName}
                     onChange={(e: any) => this.handleMessageChange(e)}
          value={messageValue} placeholder="enter message or select template" required>
        </FormControl>

        { this.state.templateChanged &&
          <Row>
            <Col className="mt-3">
              <Button onClick={() => this.setState({ showSaveTemplate: true })}>
                Save Template
              </Button>
            </Col>
          </Row>
        }

      </FormGroup>
    );
  }

  public componentDidUpdate(prevProps: any) {
    const { templateName, templateId } = this.state;

    console.log("SUBJECT VALUE:", this.props.subjectValue);

    if (prevProps.templateList.length > 0) {
      if (prevProps.templateList !== this.props.templateList) {
        let newTemplateId = templateId;
        for (const template of this.props.templateList) {
          if (template.name === templateName) {
            newTemplateId = template.messageTemplateId;
            break;
          }
        }
        const ev: any = { target: { value: newTemplateId } };
        this.handleTemplateSelect(ev);
      }
    }

  }

  private handleTemplateSelect = (e: any ) => {

    const { onTemplateChange, templateMap } = this.props;
    const { value: tValue } = e.target;

    this.setState({
      templateChanged: false,
      templateId: tValue,
      templateName: templateMap[tValue] ? templateMap[tValue].name : "",
    }, () => console.log(this.state.templateId));

    const templateObj: any = {
      message: templateMap[tValue].messageTemplate,
      subject: templateMap[tValue].subjectTemplate,
      templateId: tValue,
    };

    onTemplateChange(templateObj);

  }

  private async handleMessageChange(event: any) {
    const { onChange } = this.props;

    await onChange(event);
    this.checkChanged();

  }

  /**
   * Checks to see if user has changed an existing template
   * (or in the process of creating a new one) so that we can give
   * them the option to save it
   */
  private checkChanged() {
    const { subjectValue, messageValue, templateMap} = this.props;
    const { templateId } = this.state;

    let changed = false;
    const templateObj = templateMap[templateId];
    if (templateObj !== undefined) {
      if (messageValue !== templateObj.messageTemplate || subjectValue !== templateObj.subjectTemplate) {
        if (messageValue.length > 0 || subjectValue.length > 0) {
          changed = true;
        }
      }
    } else {
      changed = true;
    }

    this.setState({ templateChanged: changed });
  }

  private async onSaveClick(name: string) {

    this.setState({ submitting: true });

    const { org, subjectValue, messageValue, saveTemplate } = this.props;

    // template ID gets assigned (or stays the same) in rest layer
    const templateObj = new MessageTemplate();
    templateObj.orgId = org.org_id;
    templateObj.name = name;
    templateObj.messageTemplate = messageValue;
    templateObj.subjectTemplate = subjectValue;

    saveTemplate(templateObj);

    this.setState({ showSaveTemplate: false, templateChanged: false, templateName: name });

  }

  private renderTemplateSelect() {
    const { templateList } = this.props;

    return (
      <FormControl as="select" name="templateId" onChange={this.handleTemplateSelect} value={this.props.templateId} >
        {
          templateList.map((t: MessageTemplate, i: number) => (
            <option key={i} value={t.messageTemplateId}> {t.name} </option>
          ))
        }
      </FormControl>
    );
  }

  private renderSaveTemplate() {
    const modalClose = () => this.setState({ showSaveTemplate: false });

    return (
      <VerticalModal title="Give a name to your template"
      size="md"
      show={this.state.showSaveTemplate}
      onHide={modalClose}
      page={<NameMessageTemplate name={this.state.templateName}
                                 onSubmit={this.onSaveClick}
                                 submitting={this.state.submitting} />} />
    );
  }

}

function mapStateToProps(state: any) {
  return {
    alert: state.alert,
    org: state.auth.org,
    templateList : state.messages.templates,
    templateMap : state.messages.templatesMap,
  };
}

const mapDispatchToProps = (dispatch: any) => ({
  saveTemplate: (templateObj: MessageTemplate) => dispatch(messageActions.updateTemplate(templateObj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FormMessageFromTemplate);
