import React from 'react';
import { Form, Button, Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withLoader } from '../';
import { withRouter } from "react-router-dom"


const caseType = [
  <option key={"Feedback"} value={"Feedback"}>{"Feedback"}</option>,
  <option key={"Product Question"} value={"Product Question"}>{"Product Question"}</option>,
  <option key={"Part Missing"} value={"Part Missing"}>{"Part Missing"}</option>,
  <option key={"Minor Product Defect"} value={"Minor Product Defect"}>{"Minor Product Defect"}</option>,
  <option key={"Major Product Defect - Recall"} value={"Major Product Defect - Recall"}>{"Major Product Defect - Recall"}</option>,
  <option key={"RFID - Incorrect Application"} value={"RFID - Incorrect Application"}>{"RFID - Incorrect Application"}</option>,
  <option key={"RFID - Missing"} value={"RFID - Missing"}>{"RFID - Missing"}</option>,
  <option key={"Other"} value={"Other"}>{"Other"}</option>,
];

const casePriority = [
  <option key={"Low"} value={"Low"} >{"Low"}</option>,
  <option key={"Medium"} value={"Medium"}>{"Medium"}</option>,
  <option key={"High"} value={"High"}>{"High"}</option>,
];

class ItemCaseCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      case_priority: "Low",
      case_subType: "Feedback",
      OtherInputField: false,
      disable: false
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCaseTypeChange = (event) => {
    const { value } = event.target;
    if (value === "Other") {
      this.setState({
        OtherInputField: true,
        case_subType: value,
        case_subType_other: ''
      })
    } else {
      this.setState({
        OtherInputField: false,
        case_subType: value
      }, () => console.log("updated drop down", this.state.case_subType))
    }
  }

  handleCaseTypeTextChange = (event) => {
    const { value } = event.target;
    this.setState({
      case_subType_other: value
    })
  }

  handleCasePriorityChange = (event) => {
    this.setState({
      case_priority: event.target.value
    })
  }

  handleSubmit(event) {
    const { createData } = this.props;
    const { basePath } = this.props;
    event.preventDefault();
    this.setState({
      disable: true
    })
    let case_subject;
    let orgs = ["global_retailer2", "factory"];
    switch (this.state.case_subType) {
      case "Feedback":
        case_subject = "Feedback only (no action required)";
        break;
      case "Product Question":
        case_subject = "Provide requested product information";
        break;
      case "Part Missing":
        case_subject = "Send missing part";
        break;
      case "Minor Product Defect":
        case_subject = "Review/rework product";
        break;
      case "Major Product Defect - Recall":
        case_subject = "Suspend production";
        orgs.push("3PL");
        break;
      case "Other":
        case_subject = " Respond to request : " + this.state.case_subType_other;
        break;
      default:
        console.log(this.state.case_subType);
        break;
    }

    
    this.props.history.push({
      pathname: basePath + "/cases/create",
      state: {
        case_priority: this.state.case_priority,
        case_subject : case_subject,
        case_type : 'Product Quality',
        case_subType : this.state.case_subType,
        orgs : orgs,
        rows : createData.rows
      }
    });

  }

  render() {
    return (
      <>
        <Form onSubmit={this.handleSubmit} id="createCaseForm">
          <Form.Group controlId="formCaseType">
            <Form.Label>
              Case Type
            </Form.Label>
            <Form.Control as="select" value={this.state.case_subType} onChange={this.handleCaseTypeChange} required>
              {caseType}
            </Form.Control>
          </Form.Group>
          {
            this.state.OtherInputField &&
            <Form.Group controlId="formCaseSubject">
              <Form.Label>
                Please Specify Case Type :
            </Form.Label>
              <Form.Control type="text" onChange={this.handleCaseTypeTextChange} required />
            </Form.Group>
          }
          <Form.Group controlId="formCasePriority">
            <Form.Label>
              Case Priority :
            </Form.Label>
            <Form.Control as="select" value={this.state.case_priority} onChange={this.handleCasePriorityChange} required>
              {casePriority}
            </Form.Control>
          </Form.Group>

          <Row className="justify-content-center">
            <Col sm={{ span: 6 }}>
              <Button block disabled={this.state.disable} type="submit"> Next </Button>
            </Col>
          </Row>
        </Form>
      </>
    )
  }
}
function mapStateToProps(state) {
  return {
    client: state.auth.client,
    org: state.auth.org
  }
}

export default withRouter(withLoader(connect(mapStateToProps)(ItemCaseCreate)));