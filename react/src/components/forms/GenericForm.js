//First pass at a one size fits all form
//See Actions.js for example use or ask Ryan 
//TODO:
//Handle Validations within form
//Allow for styles to be specified through props
//Handle other form fields radio btns, large text area, etc ...
//Choose which buttons are used
import React from 'react';
import {Button, Form, Alert} from 'react-bootstrap'

export default class GenericForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: this.props.inputs,
      buttons: this.props.buttons,
      //this dictionary will house all fields that can be submitted
      //it is created during component mount from the inputs field
      //allows us to have an unknown number of values instead of 
      //having all field values in state
      formValues: {}
    }
  }

  componentDidMount() {
    let formValueDict = {};
    for (let i = 0; i < this.state.inputs.length; i++) {
      if (this.state.inputs[i].type === 'select') {
        formValueDict[this.state.inputs[i].fieldValue] = this.state.inputs[i].values[0]
      }
      if(this.state.inputs[i].type === 'text'){
        formValueDict[this.state.inputs[i].fieldValue] =  ''
      }
    }
    this.setState({
      formValues: formValueDict
    })
  }
  onChange(e,key){
    const { value } = e.target; 
    let formValueDict = this.state.formValues
    formValueDict[key] = value;
    this.setState({
      formValues: formValueDict
    })
  }
  generateForm() {
    return (
      <div>
        {
          this.state.inputs.map((input, index) => {
            if (input.type === 'text') {
              return (
                <div key ={index}>
                  {input.title} <Form.Control onValueChange = {(e)=>{this.onChange(e,input.fieldValue)}} />
                </div>
              )
            }
            if (input.type === 'select') {
              return (
                <div key={index}>
                  {input.title} 
                  <Form.Control as="select" onChange = {(e)=>{this.onChange(e,input.fieldValue)}}>
                    {input.values.map((val) => {
                      return <option key={val} value={val}>{val}</option>
                    })}
                  </Form.Control>
                </div>
              )
            } else { //this is here because there's a warning otherwise
              return null;
            }
          })
        }
        <div>
          {
            this.state.buttons.map((btn,index) => {
              console.log(this.state.formValues)
              return (
                <Button key = {index} onClick={()=>{btn.action(this.state.formValues)}}>{btn.text} </Button>
              )
            })
          }
        </div>
      </div>
    )
  }
  render() {
    return (
      <div style={{ width: 400 }}>
        {this.props.errorMessage && <Alert variant="danger">{this.props.errorMessage}</Alert>}
        {this.props.successMessage && <Alert variant="success">{this.props.successMessage}</Alert>}
        {this.generateForm()}
      </div >
    )
  }
}