import React from "react";
import { Button, Col, Form, Row  } from "react-bootstrap";
import { connect } from "react-redux";

interface IFormNameTemplateProps {
  name: string;
  onSubmit: any;
  submitting: boolean;
}

class NameMessageTemplate extends React.Component<IFormNameTemplateProps, any> {
  constructor(props: IFormNameTemplateProps) {
    super(props);

    this.state = {
      name: this.props.name,
      valid: true,
    };

  }

  public render() {
    const { name, valid } = this.state;
    const { submitting } = this.props;

    return (
      <div>
        <Row className="justify-content-center">
          <Form.Control style={{ width: "80%" }}
                        as="input"
                        value={name}
                        isInvalid={!valid}
                        placeholder="Enter a name"
                        onChange={(e: any) => this.handleChange(e)} />
        </Row>
        <Row className="justify-content-center mt-3">
          <Button style={{ width: "50%"}} onClick={this.onSave}
            disabled={submitting}>{submitting ? "Saving..." : "Save"}
          </Button>
        </Row>
      </div>
    );
  }

  private handleChange = (event: any) => {
    const { value } = event.target;

    this.setState({ name: value });
  }

  private onSave = () => {
    const { name } = this.state;
    const { onSubmit } = this.props;

    if (name.length < 1) {
      this.setState({
        valid: false,
      });
    } else {
      onSubmit(name);
    }
  }

}

function mapStateToProps(state: any) {
  return {
    groupsList : state.groups.list,
  };
}

export default connect(mapStateToProps)(NameMessageTemplate);
