import React from "react";
import { Button, ButtonToolbar, Form, Row   } from "react-bootstrap";
import { connect } from "react-redux";

interface IFormMapTemplateProps {
  saving: boolean;
  removing: boolean;
  onSubmit: any;
  templateList: any;
}

class FormMapTemplateToCase extends React.Component<IFormMapTemplateProps, any> {
  constructor(props: IFormMapTemplateProps) {
    super(props);

    this.state = {
      selectedTemplateId: null,
      valid: false,
    };

  }

  public render() {
    const { saving, removing, templateList } = this.props;

    return (
      <div>
        <Row className="justify-content-center">
          <Form.Control style={{ width: "80%" }} as="select" onChange={this.handleChange}>
            {templateList.map((t: any, i: number) => {
              return (
                <option key={i} value={t.messageTemplateId}>{t.name}</option>
              );
            })}
          </Form.Control>
        </Row>
        <Row className="justify-content-center mt-3">
          <ButtonToolbar>
            <Button style={{ width: 175 }}
              onClick={this.onSaveTemplate} disabled={saving || removing}>{saving ? "Saving..." : "Save"}</Button>

            <Button style={{ width: 175 }} className="ml-2"
              onClick={() =>
                this.props.onSubmit({
                  target: {
                    action: "remove",
                    name: "messageTemplateId",
                    type: "template",
                    value: null,
                  },
                })}
              variant="outline-danger"
              disabled={saving || removing}>{removing ? "Removing..." : "Remove Template"} </Button>
          </ButtonToolbar>
        </Row>
      </div>
    );
  }

  private handleChange = (event: any) => {
    this.setState({
      selectedTemplateId: event.target.value,
    });
  }

  private onSaveTemplate = () => {
    const { onSubmit } = this.props;
    const value = this.state.selectedTemplateId === null ? null : this.state.selectedTemplateId;
    const ev: any = { target: { action: "save", type: "template", name: "messageTemplateId", value } };
    onSubmit(ev);
  }
}

function mapStateToProps(state: any) {
  return {
    templateList : state.messages.templates,
  };
}

export default connect(mapStateToProps)(FormMapTemplateToCase);
