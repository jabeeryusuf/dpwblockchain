/*global google*/
import React from 'react';
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow, Polyline } from "react-google-maps"


//object used to specify the color of markers based on status
/*
const colorMap = {
  'Arrived': 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
  'In-route': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
  'Not shipped': 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
}*/
const GMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  lifecycle({
    componentDidMount() {
      this.setState({
        zoomToMarkers: map => {
          if (map) {
            const bounds = new window.google.maps.LatLngBounds();
            map.props.children.forEach((child) => {
              if (child.length >= 1) {
                child.forEach((grandchild) => {
                  if (grandchild.type === Marker) {
                    if (parseInt(grandchild.props.position.lng) && parseInt(grandchild.props.position.lat)) {
                      bounds.extend(new window.google.maps.LatLng(parseInt(grandchild.props.position.lat), parseInt(grandchild.props.position.lng)));
                    }
                  }
                });
                if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                  var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 3.00, bounds.getNorthEast().lng() + 3.00);
                  var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 3.00, bounds.getNorthEast().lng() - 3.00);
                  bounds.extend(extendPoint1);
                  bounds.extend(extendPoint2);
                }
                map.fitBounds(bounds);
              }
            });
          }
        }
      });
    }

  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap ref={props.zoomToMarkers} zoom={props.zoom} center={props.center} >
    {props.paths ? props.paths.map(path =>
      (
        <Polyline
          options={{ strokeColor: path.strokeColor, geodesic: path.geodesic }}
          key={path.id}
          path={path.path}
          visible={path.visible}
          strokeOpacity={path.strokeOpacity}
          onClick={(e) => { path.onClick(path.path, e) }}
          strokeWeight={path.strokeWeight}>

        </Polyline>

      )) : []}
    {props.markers && props.markers.map(marker =>
      (
        <Marker
          key={marker.id}
          id={marker.id}
          onClick={() => { marker.onClick(marker.id) }}
          onMouseOver={() => { marker.onMouseOver(marker) }}
          location={marker.location}
          qty={marker.qty}
          ponum={marker.ponum}
          position={{ lat: parseFloat(marker.lat), lng: parseFloat(marker.lng) }}
          icon={{ url: marker.color, anchor: marker.offset ? new google.maps.Point(4, 4) : null }}>
          {props.openWindow === marker.id &&
            <InfoWindow onCloseClick={marker.onClose}>
              <div>
               {marker.vessel_name &&
                  <p> {"Vessel Name: " + marker.vessel_name}</p>
                }
                {marker.status &&
                  <p> {"Status: " + marker.status}</p>
                }
                {marker.location &&
                  <p> {"Location: " + marker.location}</p>
                }
                {marker.quantity &&
                  <p> {"Qty: " + marker.quantity}</p>
                }
                {marker.volume &&
                  <p>{"Volume (cbm): " + marker.volume}</p>
                }
                {marker.sku &&
                  <p> {"SKU: " + marker.sku}</p>
                }
                {marker.arrivalTime &&
                  <p> {"Arrived On " + marker.arrivalTime}</p>
                }
                {marker.departureTime &&
                  <p> {"Departed on " + marker.departureTime}</p>
                }
                {marker.eta &&
                  <p> {"Estimated Arrival to Next Port: " + marker.eta}</p>
                }
                {marker.destination &&
                  <p> {"Destination Port:" + marker.destination}</p>
                }
                {marker.name &&
                  <p>{"Name: " + marker.name}</p>
                }
                {marker.address &&
                  <p>{"Address: " + marker.address}</p>
                }
                {marker.shippingContainers &&
                  <div>
                    <p> {"Shipping containers:"} </p>
                    {marker.shippingContainers.map((c, index) => {
                      return <p key={index}> {c.slice(19)}</p>
                    })}
                  </div>
                }
                {marker.button &&
                  marker.button
                }
                {
                  marker.moreInfo && 
                  marker.moreInfo
                }
              </div>
            </InfoWindow>
          }

        </Marker>
      ))}
  </GoogleMap>,

)


export { GMap, Marker, InfoWindow, Polyline };
