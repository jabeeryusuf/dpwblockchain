import * as React from 'react';
import { Button, ButtonToolbar, Row, Col } from 'react-bootstrap';

const SureWindow = (props) => {

  return (
    <Row className="justify-content-center">
      <ButtonToolbar>
        <Button style={{ width: 150 }} onClick={props.onSubmit}>Yes</Button>
        <Button className="ml-3" style={{ width: 150 }} onClick={props.onCancel}>No</Button>
      </ButtonToolbar> 
    </Row>
  )
}

export default SureWindow;