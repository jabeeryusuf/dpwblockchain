import * as React from 'react'

 const LocationDetailItem = (props) =>{
  return(
    <div>
      <h1>Location: {props.location.ref}  </h1>
        <ul style={{ listStyle: 'none', fontSize:22}}>
          {props.location.address && <li> Address: {props.location.address}</li>}
          {props.location.type === "ship" && <li> Type: Sailing Vessel </li>}
          <li> Lat: {props.location.lat}</li>
          <li> Lng: {props.location.lng}</li>
        </ul>
    </div>
  )
}
export default LocationDetailItem;