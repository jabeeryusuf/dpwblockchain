/*global google*/ 
import React from "react";
import ReactDOM from "react-dom";
import { compose, withProps, withState, lifecycle } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

export const MyMapComponent = compose(
  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyALvjmv4kQSoUXBwIw-mWUHiBIJQjSGGvk&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100vh` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  lifecycle({
    componentWillMount() {
      const refs = {};

      this.setState({
        mapProjection: null,
        zoom: 3,
        onMapMounted: ref => {
          refs.map = ref;
        },
        projectionChanged: () => {
          this.setState({
            mapProjection: refs.map.getProjection()
          });
        },
        onZoomChanged: () => {
          this.setState({
            zoom: refs.map.getZoom()
          });
        }
      });
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    defaultCenter={{ lat: 39.8283, lng: -98.5795 }}
    ref={props.onMapMounted}
    onProjectionChanged={props.projectionChanged}
    zoom={props.zoom}
    onZoomChanged={props.onZoomChanged}
  >
  {props.markers && props.paths.map(paths => {
    console.log("prpsss>>>>????", props.mapProjection);
    return (<Markers mapProjection={props.mapProjection} zoom={props.zoom} position1={paths[0]} position2={paths[1]} />)
  })}

  </GoogleMap>
));

class Markers extends React.Component {
  state = {
    position1: new google.maps.LatLng(this.props.position1.lat, this.props.position1.lng),
    position2: new google.maps.LatLng(this.props.position2.lat, this.props.position2.lng)
  };
  
  render() {
    const lineSymbol = {
      path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
      strokeColor: "#ff2527",
      strokeOpacity: 0.75,
      strokeWeight: 4,
      offset: '10%'
    };
    return (
      <div>
        <Marker position={this.state.position1} />
        <Marker position={this.state.position2} />
        <Marker position={this.state.position2} icon={lineSymbol} />
        <CurveMarker
          pos1={this.state.position1}
          pos2={this.state.position2}
          mapProjection={this.props.mapProjection}
          zoom={this.props.zoom}
        />
      </div>
    );
  }
}

const CurveMarker = ({ pos1, pos2, mapProjection, zoom }) => {
  if (!mapProjection) return <div />;
  var curvature = -0.4;

  const p1 = mapProjection.fromLatLngToPoint(pos1),
    p2 = mapProjection.fromLatLngToPoint(pos2);

  // Calculating the arc.
  const e = new google.maps.Point(p2.x - p1.x, p2.y - p1.y), // endpoint
    m = new google.maps.Point(e.x / 2, e.y / 2), // midpoint
    o = new google.maps.Point(e.y, -e.x), // orthogonal
    c = new google.maps.Point(m.x + curvature * o.x, m.y + curvature * o.y); //curve control point

  const pathDef = "M 0,0 " + "q " + c.x + "," + c.y + " " + e.x + "," + e.y;

  const scale = 1 / Math.pow(2, -zoom);
  console.log("zoom", zoom);
  console.log("pathDef", pathDef);
  const symbol = {
    path: pathDef,
    scale: scale,
    strokeWeight: 2,
  };
  
  //return <Marker position={pos1} clickable={false} icon={symbol} zIndex={0} />;
          
  return <Marker position={pos1} clickable={false} icon= {symbol} zIndex={0} />
};


