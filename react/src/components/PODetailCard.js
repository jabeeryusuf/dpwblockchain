
import React from 'react';
import { Card, Col, Row, Nav } from 'react-bootstrap';
import { GMap } from '../components/GMap'
const PODetailCard = (props) => {
  return (
    <>
      <h1> {props.page}: {props.heading} </h1>
      <Row>
        <Col md={6}>
          <Card style={{ minHeight: 453, marginRight: 0 }}>
            <h3> Order Summary: </h3>
            <ul style={{ listStyle: 'None', paddingLeft: 5 }}>
              {
                props.details.map((itm, i) => {
                  return (
                    <li key={i} >
                      <span style={{ fontSize: 18, fontWeight: 'bold' }}>{itm.label}</span>:<span style={{ fontSize: 18 }}>{itm.value}</span>
                    </li>
                  )
                })
              }
            </ul>
          </Card>
        </Col>
        <Col md={6}>
          <Card style={{ paddingLeft: 15, paddingBottom: 10 }}>
            <h3> Current PO Locationsss: </h3>
            {console.log("Map Markers")}
          {console.log(props.markers)}
            <GMap isMarkerShown
              markers={props.markers}
              openWindow={props.openWindow}
            />
{/* 
        <GMap isMarkerShown
          markers={markers}
          paths={this.state.paths}
          openWindow={this.state.openWindow}
          zoom={this.state.zoom}
          center={this.state.center} /> */}


              <Row>
              <Col>
                <Nav variant="tabs" defaultActiveKey="all_po" className="mb-3">
                  <Nav.Item  >
                    <Nav.Link style={{ minHeight: 52, paddingTop:20, fontSize: 10 }} eventKey="all_po" onSelect={() => { props.mapStatusFilter('*') }} > <span style ={{verticalAlign:'bottom'}} >Show All</span> </Nav.Link>
                  </Nav.Item>
                  <Nav.Item  >
                    <Nav.Link eventKey="factory" onSelect={() => { props.mapStatusFilter('Factory') }}><img src='http://maps.google.com/mapfiles/ms/icons/orange-dot.png' alt="Temporarily unavailable" /> <span style={{ fontSize: 10 }}>Show at Factory</span> </Nav.Link>
                  </Nav.Item>
                  <Nav.Item  >
                    <Nav.Link eventKey="warehouse_received" onSelect={() => { props.mapStatusFilter('Warehouse') }}><img src='http://maps.google.com/mapfiles/ms/icons/blue-dot.png' alt="Temporarily unavailable" /> <span style={{ fontSize: 10 }}>Show at Warehouse</span> </Nav.Link>
                  </Nav.Item>
                  <Nav.Item  >
                    <Nav.Link eventKey="shipped" onSelect={() => { props.mapStatusFilter('Shipped')}}><img src='http://maps.google.com/mapfiles/ms/icons/yellow-dot.png' alt="Temporarily unavailable" /> <span style={{ fontSize: 10 }}>Show on Vessel</span> </Nav.Link>
                  </Nav.Item>
                  <Nav.Item  >
                    <Nav.Link eventKey="delivered" onSelect={() => { props.mapStatusFilter('Delivered')}}><img src='http://maps.google.com/mapfiles/ms/icons/green-dot.png' alt="Temporarily unavailable" /> <span style={{ fontSize: 10 }}>Show Delivered</span> </Nav.Link>
                  </Nav.Item>
                </Nav>

              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </>
  )
}
export default PODetailCard; 