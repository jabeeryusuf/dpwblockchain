import React from 'react'
import Modal from 'react-modal';
const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    },
}
Modal.setAppElement('#root');
class PopUpWindow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: props.shown
        };
    }
    componentDidUpdate(newprops){
        if(this.props.shown !== this.state.modalIsOpen){
            this.setState({
                modalIsOpen:  newprops.shown
            })
        }
    }
    render(){
        return(
            <Modal
            isOpen = {this.state.modalIsOpen} 
            style={customStyles}
            contentLabel="Example Modal">
                <div>
                    {this.props.page}
                    </div>
                </Modal>
        )
    }
}
export default PopUpWindow;