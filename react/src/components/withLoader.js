import React from 'react';
import {connect} from 'react-redux';
import uuid from 'uuid';
import createClass from 'create-react-class';

import { loadingActions } from '../redux/actions';
import { PulseLoader } from 'react-spinners';
import { Card } from 'react-bootstrap';
import { css } from 'react-emotion';


const override = css`
    vertical-align: middle;
    display: block;
    margin: 0 auto;
    border-color: red;
`;

class DefaultLoader extends React.Component {

  render() {
    let {loadingText} = this.props;

    return (
      <div className="page-loader">
        <Card style={{border: "0px", textAlign: "center"}}>
          <Card.Title>{loadingText}</Card.Title>
          <Card.Body>
          <PulseLoader className={override}
                    sizeUnit={"px"}
                    size={20}
                    loading={true}/>
          </Card.Body>
        </Card>
      </div>
    );
  }
}


var withLoader = function(ComposedComponent, Loader = DefaultLoader, loadingIdArray) {
  var LoadingComponentClass = createClass({

    componentWillMount: function() {
      var loadingId = uuid.v4()
      this.loadingId = loadingId;
      this.loadingIdArray = loadingIdArray ? [loadingId, ...loadingIdArray] : [loadingId];
    },

    handleStartLoading(loadingText) {
      var {dispatch} = this.props;
      var object = loadingActions.start(this.loadingId, loadingText);
      dispatch(object);
    },

    handleStopLoading(loadId) {
      var {dispatch} = this.props;
      dispatch(loadingActions.stop(loadId ? loadId : this.loadingId));
    },

    render() {
      var {loading} = this.props;
      //console.log('loading');

      var {loadingId, loadingIdArray} = this;

      var passToChild = {
        startLoading: this.handleStartLoading,
        stopLoading: this.handleStopLoading,
        loadingIdArray,
        loadingId
      }
      
      var loadingObject;
      loadingIdArray.forEach((id) => {
        var loadObj = loading[id];

        if (loadObj && loadObj.isLoading) {
          loadingObject = loadObj;
          return true;
        } else {
          return false;
        }
      });

      //console.log(loadingObject);

      var isLoading = false;
      if (loadingObject && loadingObject.isLoading) {
        isLoading = true;
      }
      
      var styleLoader = !isLoading  || this.props.hide ? {display: 'none'} : {};
      var styleComponent = isLoading || this.props.hide  ? {display: 'none'} : {};

      return (
        <>
          <div style={{...styleLoader}}>
            <Loader loadingText={loadingObject ? loadingObject.text : "Loading..."}/>
          </div>
          <div style={{...styleComponent}}>
            <ComposedComponent {...this.props} {...passToChild}/>
          </div>
        </>
      )
    }
  });

  function mapStateToProps(state) {
    return { loading: state.loading };
  }

  return connect(mapStateToProps)(LoadingComponentClass);
}

export {withLoader,DefaultLoader};