import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';

const moment = require('moment');

const actionColumns = [
	{
		Header: 'Container ID',
		accessor: 'container_id'
	},
	{
		Header: 'Request Type',
		accessor: 'type'
	},
	{
		Header: 'Status',
		accessor: 'action_status'
	},
	{
		Header: 'Location',
		accessor: 'name'
	},
	{
		Header: 'Request Date',
		accessor: 'request_date'
	}
];

class DashboardRequests extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      showPopUp: false,
      currentRow: null
    };

  }

  async componentDidMount() {

    let sql = `SELECT a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id,  b.name 
               FROM actions a 
               LEFT JOIN containers c ON a.container_id = c.container_id 
               LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
               LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
               WHERE b.name is not null and a.action_status = 'pending' AND requestor_id='` +this.props.user.org_id  + `' group by a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id,  b.name;`;
    let actionData = await this.props.client.getStuff(sql);
    console.log(actionData)
    this.processData(actionData);

  }

  processData(actionData) {
    for (let obj of actionData) {
      obj.request_date = moment(obj.request_date).format('MMMM Do YYYY');
      obj.container_id = obj.container_id.slice(19);
      obj.name = obj.name ? obj.name.substring(7, obj.name.length) : "Unknown"; //Does not show "Port of"
    }

    this.setState({
      data: actionData
    });
  }

  handleLinkClick(row) {
    this.setState({
      showPopUp: true,
      currentRow: row
    });
  }

  render() {
    const { data } = this.state;

    return (
        <div>
          {data.length > 0 && <ReactTable data={data}
                      columns={actionColumns}
                      defaultPageSize={10}
                      className="-striped -highlight" />}
        </div>
    );
  }
}

function mapStateToProps(state) {
  return { client: state.auth.client, user: state.auth.user }
}

export default connect(mapStateToProps)(DashboardRequests);