import React, { Dispatch } from "react";
import { ListGroup } from "react-bootstrap";
import ReactDOM from "react-dom";
import { connect } from "react-redux";

import { conversationActions } from "../../redux/actions";
import { MessageHistoryEntry } from "./MessageHistoryEntry"

interface IMessageHistoryRedux {
  conversations: any;
  user: any;
  conversationApi: any;
  users: any;
}

interface IMessageHistoryDispatch {
  convHistory: any;
  convMembers: any;
  updatePrefs: any;
}

interface IMessageHistoryProps extends
  Partial<IMessageHistoryRedux>, Partial<IMessageHistoryDispatch> {

  conversationId: string;
  groupMembers: any;
  sent: boolean;
}

// The message history list
class MessageHistory extends React.Component<IMessageHistoryProps, any> {
  private messagesEnd: React.RefObject<any>;

  constructor(props: IMessageHistoryProps) {
    super(props);
    this.messagesEnd = React.createRef();
    this.state = {
      showDict: {},
      memberCache: {},
      msgsLength: 0,
      unreadCount: 0,
    };
  }

  public scrollToBottom = () => {
    const node:any = ReactDOM.findDOMNode(this.messagesEnd.current);
    if (node) {
      node.scrollIntoView({ behavior: "smooth" })
    }
  };

  public showEntireMessage(index: number) {
    const newDict = this.state.showDict;
    if (newDict[index] === true) {
      newDict[index] = false;
    } else {
      newDict[index] = true;
    }
    this.setState({
      showDict: newDict,
    });
  }

  public async componentDidMount() {
    await this.getMessageHistory();
    this.updateConversation();
  }

  public async componentDidUpdate(prevProps:IMessageHistoryProps, prevState:IMessageHistoryProps) {
    const {conversationId} = this.props;

    if (prevProps.conversationId !== conversationId) {
      await this.getMessageHistory();
    }

    const msgs = this.getMessages();
    if ((msgs &&  msgs.length) && (msgs.length !== this.state.msgsLength)) {
      this.scrollToBottom();
      this.setState({
        msgsLength: msgs.length,
      }, () => this.updateConversation());
    }

  }

  public updateConversation() {
    const { conversationId, conversations, user, sent } = this.props;
    const { memberCache } = this.state;
    console.log("UPDATE CONVERSATION");
    console.log(memberCache);
    console.log(conversations[conversationId]);

    if (sent) {
      conversations[conversationId].unread_count = 0;
      this.setState({
        unreadCount: conversations[conversationId].unread_count,
      });
    }

    if (conversations[conversationId].unread_count > 0) {
      this.setState({
        unreadCount: conversations[conversationId].unread_count,
      });
    }

    if (memberCache[user.user_id]) {
      console.log("UPDATING PREFS");
      const length = (conversations[conversationId] && conversations[conversationId].messages) 
        ? conversations[conversationId].messages.length
        : 0;
      this.props.updatePrefs(conversationId, user.user_id, {read_count : length});
    }

  }

  public async getMessageHistory() {
    const { conversationApi, conversationId } = this.props;

    console.log("dispatching conversation history");
    this.props.convHistory(conversationId);
    this.props.convMembers(conversationId);

    // let msgHistory = await conversationApi.history(conversationId);
    const members = await conversationApi.members(conversationId);

    // convert cache
    const memberCache = members.reduce((cache:any, m:any) => {
      cache[m.user_id] = m;
      return cache;
    }, {});

    this.setState({
      memberCache,
    });
  }

  public getMessages() {
    const {conversations, conversationId} = this.props;

    // console.log(conversationId)
    // console.log(conversations[conversationId])
    return (conversations[conversationId] && conversations[conversationId].messages) ?
      conversations[conversationId].messages :
      [];
  }

  public renderMessageList(messages:any) {

    return messages.map((message: any, index: number) =>
      <MessageHistoryEntry user={this.props.user}
                           unreadIndex={messages.length - this.state.unreadCount}
                           message={message}
                           key={index}
                           ref={(index === messages.length - 1) ? this.messagesEnd : null}
                           orgUsers={this.props.users}
                           convUsers={this.state.memberCache}
                           index = {index}
                           showDetail = {this.state.showDict[index]}
                           onClick ={(idx: number) => {this.showEntireMessage(idx);}}
                           />);
  }

  public render() {

    const msgs = this.getMessages();

    return (
      <div >
        <ListGroup >
          {this.renderMessageList(msgs)}
        </ListGroup>
      </div>);
  }
}

// Map required state into props
const mapStateToProps = (state: any): IMessageHistoryRedux => ({
  conversationApi: state.auth.conversationApi,
  conversations: state.conversation.conversations,
  user: state.auth.user,
  users: state.user.users,
});

// Map dispatch function into props
const mapDispatchToProps = (dispatch: Dispatch<any> ): IMessageHistoryDispatch => ({
  convHistory: (convId: string) => dispatch(conversationActions.history(convId)),
  convMembers: (convId: string) => dispatch(conversationActions.members(convId)),
  updatePrefs: (convId: string, userId: string, pref: any) =>
    dispatch(conversationActions.updatePreferences(convId, userId, pref)),
});

// Export props-mapped HOC
export default connect(mapStateToProps, mapDispatchToProps)(MessageHistory);
