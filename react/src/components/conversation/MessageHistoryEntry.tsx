import React from "react";
import { ListGroup } from "react-bootstrap";

// A formatted instant message
class MessageHistoryEntry extends React.Component<any, any> {
  // user, key, message
  public render() {

    const from = this.props.message.from;
    let name = from;
    if ( this.props.convUsers[from]) {
      name = this.props.convUsers[from].name;
    } else if (this.props.orgUsers[from]) {
      name = this.props.orgUsers[from].name;
    }
 
    const unreadIndex = this.props.unreadIndex;

    if (!this.props.showDetail) {
      return (
        <ListGroup.Item variant={ this.props.index >= unreadIndex ? "info" : undefined }>
          <strong>{name}: </strong><br />
          <p style = {{textAlign: "left"}}> {this.props.message.preview } </p>
         {this.props.message.lines.length > 1 &&
          <span onClick = {() => {this.props.onClick(this.props.index);}}> ... </span>
         }
        </ListGroup.Item>
      );
    } else {
      return (
      <ListGroup.Item variant={ this.props.index >= unreadIndex ? "info" : undefined }>
        <strong>{name}: </strong><br />
        {this.props.message.lines.map((line: any, index: number) => {
          return (
            <p key = {index} style = {{textAlign: "left"}}>
              {line}
            </p>
          );
        })
        }
        <br></br>
        <br></br>
        <span onClick = {() => {this.props.onClick(this.props.index); }}> show less </span>
      </ListGroup.Item>
      );
    }
  }
}

export { MessageHistoryEntry };