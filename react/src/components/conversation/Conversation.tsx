import React from "react";
import { connect } from "react-redux";
import MessageHistory from "./MessageHistory";
import MessageInput from "./MessageInput";

interface IProps {
  groupId: string;
  groupMembers: any;
}

class Conversation extends React.Component<IProps, any>  {

  constructor(props: IProps) {
    super(props);

    this.state = {
      sent: false,
    };

  }

  public render() {
    const { sent } = this.state;

    return (
      <>
        <MessageHistory conversationId={this.props.groupId} groupMembers={this.props.groupMembers} sent={sent} />
        <br/>
        <MessageInput groupId={this.props.groupId} onMessageSend={this.onMessageSent} />
      </>
    );
  }

  private onMessageSent = () => {
    this.setState({ sent: true });
  }
}

function mapStateToProps(state: any) {
  return {
    client: state.auth.client,
    org: state.auth.org,
    user: state.auth.user,
  };
}

export default connect(mapStateToProps)(Conversation);
