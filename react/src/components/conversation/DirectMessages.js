import React from 'react';
import {Link} from 'react-router-dom';
import {withLoader} from '..';
import {ListGroup} from 'react-bootstrap';
import { connect } from 'react-redux';


class DirectMessages extends React.Component {
  render() {
    
    let {convPath,users} = this.props;
    
    return (
      <>
        <ListGroup>
          {Object.keys(users).map((id,i) => (
            <ListGroup.Item key={i}>
              <Link to={convPath + id}>
                {users[id].name}
              </Link> 
            </ListGroup.Item>
          ))}
        </ListGroup>
      </>
    );
  }


}

function mapStateToProps(state) {
  return { 
    conversations: state.conversation.conversations,
    users: state.conversation.users,
  };
}

export default withLoader(connect(mapStateToProps)(DirectMessages));
