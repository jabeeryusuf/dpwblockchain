import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Form, Button,InputGroup, FormControl } from 'react-bootstrap';

// ACTIONS
import { conversationActions } from '../../redux/actions';

// Message input and send button
class MessageInput extends Component {

  // The outgoing message text has changed
  handleOutMessageChange = event => {
    this.props.changeOutgoingMessage(this.props.groupId, event.target.value);
  };

  // User wants to send the outgoing message
  handleClick(e) {
    e.preventDefault();
    this.props.sendMessage(this.props.groupId, this.getOutgoingMessage());
    this.props.onMessageSend();
  };
    
  // Is the button enabled?
  getOutgoingMessage = () => (this.props.conversations[this.props.groupId] && 
                              this.props.conversations[this.props.groupId].outgoingMessage ? 
    this.props.conversations[this.props.groupId].outgoingMessage : 
    '');
  isVisible = () => (this.props.connected);
  isEnabled = () => (this.getOutgoingMessage() ? true : false);
  

  renderInput() {
    return (
      <Form>
        
        <InputGroup> 
          <FormControl placeholder="Message..." value={this.getOutgoingMessage()} onChange={this.handleOutMessageChange} />
          <InputGroup.Append>
            <Button type="submit" variant="outline-secondary"
                    onClick={(e) => this.handleClick(e)}>
              Send
            </Button>
          </InputGroup.Append>
        </InputGroup>

      </Form>
    );
  }

  render() {
    //console.log("input connected = ", this.isEnabled());
    return this.isVisible() ? this.renderInput() : null;
  }
}


// Map required state into props
const mapStateToProps = (state) => ({
  connected: state.socket.connected,
  port: state.socket.port,
  user: state.auth.user,
  recipient: state.conversation.recipient,
  conversations: state.conversation.conversations
});

// Map dispatch function into props
const mapDispatchToProps = (dispatch) => ({
  sendMessage: (groupId, message) => dispatch(conversationActions.sendMessage(groupId,message)),
  changeOutgoingMessage: (groupId, message) => dispatch(conversationActions.changeOutgoingMessage(groupId,message))
});


// Export props-mapped HOC
export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
