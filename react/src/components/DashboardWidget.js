import React from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import { Link } from 'react-router-dom'
import Autosuggest from 'react-autosuggest';

const moment = require('moment');

function escapeRegexCharacters(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}



const actionColumns = [
    {
        Header: 'Container ID',
        accessor: 'container_id'
    },
    {
        Header: 'Request Type',
        accessor: 'type'
    },
    {
        Header: 'Status',
        accessor: 'action_status'
    },
    {
        Header: 'Location',
        accessor: 'name'
    },
    {
        Header: 'Request Date',
        accessor: 'request_date'
    }
];

class DashboardWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            suggestions: [],
            showPopUp: false,
            currentRow: null,
            searchid: null,
            searchlink: null,
            value: '',
        };

    }

    onChange = (event, { newValue, method }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {

        
        this.setState({
            suggestions: this.loadSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };


    onSuggestionSelected = ({ suggestion }) => {
    };

    getSuggestionValue = (suggestion) => {

        console.log(suggestion);
        var searchlinko = '/'+suggestion.searchtype+'/detail/' + suggestion.id;
        //console.log("Seach LINK is"+searchlinko);
        this.setState({
            searchlink:searchlinko
        });

        return suggestion.name;
    }


    renderSuggestion(suggestion) {
       
        return (
            <span>{suggestion.name}</span>
        );
    }

    renderSectionTitle(section) {
        return (
          <strong>{section.title}</strong>
        );
      }

      getSectionSuggestions(section) {
        return section.records;
      }


    async loadSuggestions(value) {

        if (value.length >= 2) {

            let allShipments = [];            
            allShipments = await this.props.client.shipmentsClient.searchSuggest(this.props.user.org_id, value);            
            
            //var result = Object.keys(resultgrouped).map(function (key) { return allShipments[key];    });

            let resultgrouped = allShipments.reduce((op,inp) => {
                let title = inp.searchtype 
                op[title] = op[title] || {title, records:[]}
                op[title].records.push(inp)
                return op
              },{})           

            var result = Object.keys(resultgrouped).map(function (key) { return resultgrouped[key];});


            console.log(" log of resultgrouped");
            console.log(resultgrouped);
            console.log("end of log of resultgrouped");


            console.log(" log of result");
            console.log(result);
            console.log("end of result");

            var uii =  result
            .map(section => {
              return {
                title: section.title,
                records: section.records
              };
            });

            this.setState({
                isLoading: false,
                suggestions: uii
            });
        }
    }


    async componentDidMount() {

        let sql = `SELECT a.container_id, a.type, a.action_statusselect * from shipments limit 7select * from shipments limit 7, a.action_id, a.request_date, c.parent_container_id,  b.name 
               FROM actions a 
               LEFT JOIN containers c ON a.container_id = c.container_id 
               LEFT JOIN shipments s ON c.parent_container_id = s.vessel_container_id 
               LEFT JOIN buildings b ON s.destination_container_id = b.container_id 
               WHERE requestor_id='` + this.props.user.org_id + `' group by a.container_id, a.type, a.action_status, a.action_id, a.request_date, c.parent_container_id,  b.name;`;

        let sqlin = `SELECT
               sum(case when DATE_PART('day', eta - original_eta)>=7 and (NOT (s.ata<localtimestamp) or s.ata isnull ) then 1 end) as Shipment_Delayed,
               sum(case when  s.shipment_status = 'in-transit' then 1 end) as Shipment_InTransit,
               sum(case when  s.ata<=localtimestamp and s.shipment_status='completed' then 1 end) as Shipment_Completed,

               (select count(sku) from shipment_items i, shipments s where  i.shipment_id=s.shipment_id and DATE_PART('day', s.eta - s.original_eta)>=7 and (NOT (s.ata<localtimestamp) or s.ata isnull) and i.org_id='` + this.props.user.org_id + `') as items_delayed,
               (select count(sku) from shipment_items i, shipments s where  i.shipment_id=s.shipment_id and s.shipment_status = 'in-transit' and i.org_id='` + this.props.user.org_id + `') as items_intransit,
               (select count(sku) from shipment_items i, shipments s where  i.shipment_id=s.shipment_id and s.ata<=localtimestamp and s.shipment_status='completed' and i.org_id='` + this.props.user.org_id + `') as items_instock,

               (select count(a.purchase_order_id) from (
                select i.purchase_order_id,count(i.purchase_order_id) as counts from shipments s left join shipment_items i on s.shipment_id = i.shipment_id
                where DATE_PART('day', s.eta - s.original_eta)>=7 and (NOT (s.ata<localtimestamp) or s.ata isnull) and s.org_id='` + this.props.user.org_id + `'
                group by i.purchase_order_id) a) as po_delayed,

                (select count(a.purchase_order_id) from (
                select i.purchase_order_id,count(i.purchase_order_id) as counts from shipments s left join shipment_items i on s.shipment_id = i.shipment_id
                where  s.shipment_status = 'in-transit' and s.org_id='` + this.props.user.org_id + `'
                group by i.purchase_order_id) a) as po_intransit,

               (select count(a.purchase_order_id) from (
                select i.purchase_order_id,count(i.purchase_order_id) as counts from shipments s left join shipment_items i on s.shipment_id = i.shipment_id
                where  s.ata<=localtimestamp and s.shipment_status='completed' and s.org_id='` + this.props.user.org_id + `'
                group by i.purchase_order_id) a) as po_completed,

                (select count(c.ref)  from shipments s
                Left Join containers c on c.ref=s.shipment_ref
                where org_id = '` + this.props.user.org_id + `') as container_count,
                (select count(vendor_code) from vendors where org_id = '` + this.props.user.org_id + `') as vendorcount,
                (select count(container_id)  from buildings where org_id = '` + this.props.user.org_id + `') as networkcount
               from shipments s
               where  s.org_id='` + this.props.user.org_id + `'
               ;`;

        let actionData = await this.props.client.getStuff(sqlin);

        //this.processData(actionData);
        this.processDshData(actionData);
    }

    processDshData(actionData) {
        this.setState({
            data: actionData[0]
        });

        //console.log("ACTIOn DATA iS");
        //console.log(this.state);
    }

    processData(actionData) {
        for (let obj of actionData) {
            obj.request_date = moment(obj.request_date).format('MMMM Do YYYY');
            obj.container_id = obj.container_id.slice(19);
            obj.name = obj.name ? obj.name.substring(7, obj.name.length) : "Unknown"; //Does not show "Port of"
        }

        this.setState({
            data: actionData
        });
    }

    handleLinkClick(row) {
        this.setState({
            showPopUp: true,
            currentRow: row
        });
    }

    render() {
        const { data } = this.state;
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: "Search Shipment Ref or Vessel Name [eg:Ho or po]",
            value,
            onChange: this.onChange
        };

        return (
            <div class="row py-5">
                <div class="col-md-12 py-5">
                    <div class="input-group">
                        {/*<input type="text" class="form-control" placeholder="Search..." />*/}
                        <Autosuggest
                            multiSection={true}
                            suggestions={suggestions}
                            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                            getSuggestionValue={this.getSuggestionValue}
                            renderSuggestion={this.renderSuggestion}
                            renderSectionTitle={this.renderSectionTitle}
                            getSectionSuggestions={this.getSectionSuggestions}
                            inputProps={inputProps} />
                        <span class="input-group-btn">
                            <Link to={this.props.user.org_id + this.state.searchlink}>
                                <button class="btn btn-info btn-flat" type="button">Go!</button>
                            </Link>
                        </span>
                    </div>
                </div>               

                <div class="col-md-4">
                    <div class="box box-widget widget-user  draw meet">
                        <div class="widget-user-header submitinbound bg-aqua-active">
                            <h3 class="widget-user-username">Shipments</h3>
                            <h5 class="widget-user-desc">Shipments Tracking</h5>
                        </div>
                        <div class="widget-user-image inboundimage">
                            <div class="img-circle iconnn text-aqua"> <i class="fa fa-ship"></i></div>
                        </div>
                        <div class="box-footer border-aqua-active">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/shipment-reports'}>
                                            <h5 class="description-header">{ data.shipment_delayed !=null ?<>{data.shipment_delayed }</>:<>0</>}</h5>
                                            <span class="description-text">Delayed</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/shipments/in-transit/all'}>
                                            <h5 class="description-header">{ data.shipment_intransit !=null ?<>{data.shipment_intransit }</>:<>0</>}</h5>
                                            <span class="description-text">In-Transit</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/shipments/completed/all'}>
                                            <h5 class="description-header">{ data.shipment_completed !=null ?<>{data.shipment_completed }</>:<>0</>}</h5>
                                            <span class="description-text">Arrived</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-widget widget-user  draw meet">
                        <div class="widget-user-header submitinbound bg-olive-active">
                            <h3 class="widget-user-username">Purchase Orders</h3>
                            <h5 class="widget-user-desc">PO Tracking</h5>
                        </div>
                        <div class="widget-user-image inboundimage">
                            <div class="img-circle iconnn text-olive"> <i class="fa fa-money"></i></div>
                        </div>
                        <div class="box-footer border-olive-active">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/purchaseorders/delayed/items'}>
                                            <h5 class="description-header">{ data.po_delayed !=null ?<>{data.po_delayed }</>:<>0</>}</h5>
                                            <span class="description-text">Delayed</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/purchaseorders/in-transit/items'}>
                                            <h5 class="description-header">{ data.po_intransit !=null ?<>{data.po_intransit }</>:<>0</>}</h5>
                                            <span class="description-text">In-Transit</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/purchaseorders/completed/items'}>
                                            <h5 class="description-header">{ data.po_completed !=null ?<>{data.po_completed }</>:<>0</>}</h5>
                                            <span class="description-text">Completed</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-widget widget-user  draw meet">
                        <div class="widget-user-header submitinbound bg-maroon-active">
                            <h3 class="widget-user-username">Products</h3>
                            <h5 class="widget-user-desc">Products Tracking</h5>
                        </div>
                        <div class="widget-user-image inboundimage">
                            <div class="img-circle iconnn text-maroon"> <i class="fa fa-table"></i></div>
                        </div>
                        <div class="box-footer border-maroon-active">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/items/explorer'}>
                                            <h5 class="description-header">{data.items_delayed}</h5>
                                            <span class="description-text">Delayed</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/items/explorer'}>
                                            <h5 class="description-header">{data.items_intransit}</h5>
                                            <span class="description-text">In-Transit</span>
                                        </Link>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <Link to={this.props.user.org_id + '/items/explorer'}>
                                            <h5 class="description-header">{data.items_instock}</h5>
                                            <span class="description-text">In Stock</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 py-3">

                        <div class="box">

                            <div class="box-body">

                                <Link to={this.props.user.org_id + '/containers/all'} class="btn btn-app">
                                    <span class="badge bg-red">{data.container_count}</span>
                                    <i class="fa fa-truck"></i> Containers
                                </Link>

                                <Link to={this.props.user.org_id + '/vendors'} class="btn btn-app">
                                    <span class="badge bg-yellow">{data.vendorcount}</span>
                                    <i class="fa fa-industry"></i> Vendors
                                </Link>

                                <Link to={this.props.user.org_id + '/my-buildings'} class="btn btn-app">
                                    <span class="badge bg-red-active">{data.networkcount}</span>
                                    <i class="fa fa-globe"></i> NetWorks
                                 </Link>

                                <Link to={this.props.user.org_id + '/milestones/tracker'} class="btn btn-app">
                                   
                                    <i class="fa fa-bars"></i> MileStones
                                </Link>

                                <Link to={this.props.user.org_id + '/feeds/list'} class="btn btn-app">
                                  
                                    <i class="fa fa-area-chart"></i> Feeds
                                </Link>

                                <Link to={this.props.user.org_id + '/generate-report'} class="btn btn-app">
                                    <i class="fa fa-table"></i> Analytics
                                </Link>

                                <Link to={this.props.user.org_id + '/team/members'} class="btn btn-app">
                                    <i class="fa fa-users"></i> Users
                                </Link>

                                <Link to={this.props.user.org_id + '/org-settings'} class="btn btn-app">
                                    <i class="fa fa-cogs"></i> Org Settings
                                </Link>



                            </div>
                        </div>



                    </div>
                </div>


            </div>

        );
    }
}

function mapStateToProps(state) {
    return {
        client: state.auth.client,
        org: state.auth.org,
        user: state.auth.user,
        orgApi: state.auth.orgApi,
    }
}

export default connect(mapStateToProps)(DashboardWidget);