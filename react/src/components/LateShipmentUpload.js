import React from 'react';
import { connect } from 'react-redux';
import { withLoader } from '.';
import { Table, Row, Col, Alert, Card, Container } from 'react-bootstrap';

class LateShipmentUpload extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      shipment: props.shipment,
      trackingInfo: [],
      toggle: false
    }

    this._isMounted = false;
  }

  async componentDidMount() {
    const { startLoading, stopLoading } = this.props;
    startLoading('Loading...');

    await this.getShipmentInfo();

    stopLoading();
    this._isMounted = true;
  }

  async getShipmentInfo() {
    let trackingInfo = await this.props.client.queryShipmentTrackingExist(this.state.shipment.tracking_id);

    this.setState({
      trackingInfo
    });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { shipment, trackingInfo } = this.state;

    return (
      <Container>
        <Card style={{ padding: "10px"}}>

          <Alert variant="warning">Unfortunately DPWChain is unable to track this shipment due to it being uploaded past its ETA. Here is the information we have about this shipment:</Alert>
          <Row className="justify-content-center">
            <Col sm={8} className="tablehead">
              <Table bordered responsive>
                <tbody>
                  <tr>
                    <th>Shipment ID: </th>
                    <td>{shipment.shipment_id}</td>
                  </tr>
                  <tr>
                    <th>Destination: </th>
                    <td>{shipment.dest_name}</td>
                  </tr>
                  <tr>
                    <th>Estimated Time of Arrival: </th>
                    <td>{shipment.format_eta}</td>
                  </tr>
                  <tr>
                    <th>Vessel: </th>
                    <td>{shipment.vessel_name}</td>
                  </tr>
                  <tr>
                    <th>Carrier: </th>
                    <td>{trackingInfo["carrier"] ? trackingInfo["carrier"] : "Unknown"}</td>
                  </tr>
                  <tr>
                    <th>Tracking Number: </th>
                    <td>{trackingInfo["tracking_ref"] ? trackingInfo["tracking_ref"] : "Unknown"}</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
        </Card>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    client: state.auth.client,
    org: state.auth.org
  }
}

export default withLoader(connect(mapStateToProps)(LateShipmentUpload));