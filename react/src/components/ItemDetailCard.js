
import React from 'react';
import { Container,Card } from 'react-bootstrap';
const ItemDetailCard = (props) => {
  return (
    <>
      <h1> {props.page}: {props.heading} </h1>
      {props.image &&
        <Container style={{ width: 300 }}>
          <Card>
            <img style={{ width: 250 }} src={props.image} alt={"Failed to Load"} />
          </Card>
        </Container>
      }
      {props.subComponent}

    </>
  )
}
export default ItemDetailCard; 