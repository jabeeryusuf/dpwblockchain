import * as React from 'react'

const PredictionDetailItem = (props) => {
    return (
        <div>
            <ul style={{ padding: 0, listStyle: 'none', fontSize: 18 }}>
                {props.current.location && 
                    <li> Location: {props.current.location}</li>
                }
                {props.current.arrivalTime &&
                    <li> Arrival Time: {props.current.arrivalTime}</li>
                }
                {props.current.eta &&
                    <li> Estimated time of arrival: {props.current.eta}</li>
                }
                {props.current.departureTime &&
                    <li> Depature Time: {props.current.departureTime}</li>
                }
                {props.current.etd &&
                    <li> Estimated time of departure: {props.current.etd}</li>
                }
            </ul>
        </div >
    )
}
export default PredictionDetailItem;