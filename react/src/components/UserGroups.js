import React from 'react';
import { Row, Col, Card, Button, Container, ListGroup, Tab } from 'react-bootstrap';
import VerticalModal from './VerticalModal';
import CreateGroup from './forms/CreateGroup';
import { connect } from 'react-redux';
import { isEmpty } from '../utils/aggregateByLocation';
import { withLoader } from '.'; 
//import { setPropTypes } from 'recompose';


class UserGroups extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      groups: {},
      activeGroup: null,
      type: 'user_created',
      successMessage: '',
      errorMessage: '',
      showCreateGroup: false,
      submitting: false
    }

    this.onCreateGroup = this.onCreateGroup.bind(this);
    this.onEditGroup = this.onEditGroup.bind(this);
  }

  componentDidMount() {
    let type = this.props.type;
    this.setState({
      type: type
    }, () => this.fetchGroups());
  }

  componentDidUpdate() {
    if (this.props.type !== this.state.type) {
      this.setState({
        type: this.props.type,
        activeGroup: null
      }, () => this.fetchGroups());
    }
  }

  async fetchGroups() {
    const { startLoading, stopLoading } = this.props;
    startLoading("Loading your data...");
    const { type } = this.state;

    let groups;
    if (type !== "org") {
      groups = await this.props.client.getUsersInGroups(type, this.props.org.org_id);
    } else {
      groups = await this.props.client.getUsersInOrgGroups(this.props.org.org_id);
    }

    this.setState({
      groups: groups,
      activeGroup: this.state.activeGroup ? this.findUpdatedActiveGroup(groups) : groups[0]
    }, () => this.forceUpdate());

    stopLoading();
  }

  findUpdatedActiveGroup(groups) {
    const { activeGroup } = this.state;
    
    for (let group of groups) {
      if (group.groupID === activeGroup.groupID) {
        return group;
      }
    }
  }

  async editGroupClick() {
    let group = this.state.activeGroup;
    for (let user of group.users) {
      user.valid = true;
    }

    this.setState({
      showEditGroup: true
    });

  }

  async onCreateGroup(groupName, emails, groups) {
    this.setState({
      submitting: true
    });

    let emailList = [];
    
    for (let i = 0; i < emails.length; i++) {
      emailList.push(emails[i].email)
    }

    let payload = {
      groupName: groupName,
      emails: emailList,
      groups: groups,
      type: this.state.type,
      org: this.props.org.org_id
    }

    let createGroup = await this.props.client.createGroup(payload);
    if (!createGroup.errorMessage) {
      this.setState({
        successMessage: "Group successfully created",
        errorMessage: ""
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showCreateGroup: false,
            submitting: false
          })
        }, 1500);
      });
      this.fetchGroups();
    } else {
      this.setState({
        errorMessage: createGroup.errorMessage,
        submitting: false
      });
    }
  }

  async onEditGroup(groupName, emails, groups) {
    this.setState({
      submitting: true
    });

    let emailDict = {};
    let childGroupDict = {};
    let venderCode = "";

    if (this.state.type === "org") {
      let strArr = this.state.activeGroup.groupID.split('&');
      venderCode = strArr[1];
    }

    for (let i = 0; i < emails.length; i++) {
      emailDict[emails[i].email] = emails[i].email;
    }

    for (let i = 0; i < groups.length; i++) {
      childGroupDict[groups[i].value] = groups[i].value;
    }

    let payload = {
      prevGroupID: this.state.activeGroup.groupID,
      prevGroupName : this.state.activeGroup.groupName,
      groupName: groupName,
      prevEmails: this.state.activeGroup.users,
      emails: emailDict,
      groups: childGroupDict,
      prevGroups: this.state.activeGroup.childGroups ? this.state.activeGroup.childGroups : [],
      type: this.state.type,
      org: this.props.org.org_id,
      venderCode: venderCode
    }

    let editGroup = await this.props.client.editGroup(payload);
    if (!editGroup.errorMessage) {
      this.setState({
        successMessage: "Group successfully edited",
        errorMessage: ""
      }, () => {
        setTimeout(() => {
          this.setState({
            successMessage: "",
            showEditGroup: false,
            submitting: false
          });
        }, 1500);
      });
      this.fetchGroups();
    } else {
      this.setState({
        errorMessage: editGroup.errorMessage,
        submitting: false
      });
    }
    
  }

  render() {
    const { groups, activeGroup, submitting, successMessage, errorMessage } = this.state;
    
    let modalClose = () => this.setState({ showCreateGroup: false, showEditGroup: false, errorMessage: "", successMessage: "" });

    return (
      <Container className="mt-2 ml-2 mr-2 mb-2">
        <VerticalModal title="Create a new group"
                       size="md"
                       show={this.state.showCreateGroup}
                       onHide={modalClose}
                       page={<CreateGroup onSubmit={this.onCreateGroup}
                                          submitting={submitting}
                                          successMessage={successMessage}
                                          errorMessage={errorMessage}
                                          footerButton={"Create"} />} />
        <VerticalModal title="Edit group"
                       size="md"
                       show={this.state.showEditGroup}
                       onHide={modalClose}
                       page={<CreateGroup onSubmit={this.onEditGroup}
                                          type={this.state.type}
                                          submitting={submitting}
                                          successMessage={successMessage}
                                          errorMessage={errorMessage}
                                          group={activeGroup}
                                          footerButton={"Edit"} />} />
        <Row>
          <Col>
            {this.state.type === 'user_created' ? <Button onClick={() => { this.setState({ showCreateGroup: true }) }}>Create a new group!</Button> : ''}
          </Col>
        </Row>
        <br/>
        <Row>
            { isEmpty(groups) ? this.buildNoGroups() : this.buildGroups() }
        </Row>
      </Container>
    )
  }

/* ------------------------------------------------------------------------- main componenets  ------------------------------------------------------------------------- */

  buildNoGroups() {
    return (
      <Container>
        <Row className="justify-content-md-center">
          <Card>
            <Card.Header>
              <Card.Title>
                No Groups Found
              </Card.Title>
            </Card.Header>
            <Card.Body>
              Click the button above to create a new group
            </Card.Body>
          </Card>
        </Row>
      </Container>
    )
  }

  buildGroups() {
    const { groups } = this.state;

    return (
      <Tab.Container id="test" defaultActiveKey={"#0"}>
        <Col>
        <ListGroup>
            {groups.map((obj, idx) => {
              return (
                <ListGroup.Item action href={"#" + idx} key={idx} onSelect={() => this.setState({ activeGroup : obj })} >{obj.groupName}</ListGroup.Item>
              )
            })}
          </ListGroup>
        </Col>
        <Col>
          <Card className="text-center">
            <Card.Header>
              <Card.Title>People in this group</Card.Title>
            </Card.Header>
            <Tab.Content>
              {groups.map((obj, idx) => {
                return (
                  <Tab.Pane eventKey={"#" + idx} key={idx}>
                    <Card.Body>
                      <ListGroup>
                        {obj.users.map((user, idx) => {
                          let name = user.name ? user.name : "Unknown";
                          return (
                            <ListGroup.Item key={idx}>
                              <div>
                                {"name: " + name}
                              </div>
                              <div>
                                {"email: " + user.email}
                              </div>
                            </ListGroup.Item>
                          )
                        })}
                      </ListGroup>
                      {obj.childGroups && obj.childGroups.length > 0 ? 
                        <div>
                          {obj.childGroups.map((group, idx) => {
                            return (
                              <div>
                                <font>{group.groupName}</font>
                                <ListGroup>
                                  {group.users.map((user, idx) => {
                                    let name = user.name ? user.name : "Unknown";
                                    return (
                                      <ListGroup.Item key={idx}>
                                        <div>
                                          {"name: " + name}
                                        </div>
                                        <div>
                                          {"email: " + user.email}
                                        </div>
                                      </ListGroup.Item>
                                    )
                                  })}
                                </ListGroup>
                              </div>
                            )                            
                          })}
                        </div>
                      : ''}
                    </Card.Body>
                  </Tab.Pane>
                )
              })}
            </Tab.Content>
            <Card.Footer>
              <Button style={{ width: 150 }} onClick={() => this.editGroupClick()}>Edit this group</Button>
            </Card.Footer>
          </Card>    
        </Col>
      </Tab.Container>
    )
  }

}

function mapStateToProps(state) {
  return {
    client: state.userAuth.client,
    org: state.userAuth.org
  }
}

export default withLoader(connect(mapStateToProps)(UserGroups));