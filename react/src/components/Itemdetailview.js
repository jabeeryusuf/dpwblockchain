import * as React from 'react';
import { Row, Col } from 'react-bootstrap';

 const Itemdetailview = (props) =>{
  return(
    <div>
      <h1>SKU#: {props.item.sku}  </h1>
      <Row>
        <Col md={4}></Col>
        <Col md={4}>
          <a href = {props.item.URL}>
            <img src = {props.item.image} alt = "Failed to load" style = {{width:250, height:250}}/>
          </a>
        </Col>
        <Col md={4}></Col>
      </Row>
      <Row>
        <Col md={2}></Col>
        <Col md={8}>
          <ul style={{ listStyle: 'none', fontSize:22}}>
            <li> Name: {props.item.name}</li>
            <li> Department: {props.item.department}</li>
          </ul>
        </Col>
        <Col md={2}></Col>
      </Row>
    </div>
  )
}
export default Itemdetailview;
