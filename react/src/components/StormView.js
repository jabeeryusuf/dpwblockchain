import * as React from 'react';
import {Button, Card} from 'react-bootstrap';
const loaderimg = require("../images/ajax-loader.gif");

const StormView = (props) => {
      
    return (
      <Card style={{ marginTop: 10 }}>
        <Card.Header >
          <font size="3"><b>{props.storm.name}</b></font>
          
          {!(props.buildingcount || props.vesselcount) &&
          <div className="float-right">
           
            <img src={loaderimg}></img>

          <p>Loading Data...</p>
          </div>
          }

          {(props.buildingcount || props.vesselcount) &&
             <Button className="float-right" onClick={props.onClick}>Investigate </Button>
          }
        
            </Card.Header>
              <Card.Body>
                <ul style={{ listStyle: 'none', fontSize: 14 }}>
                <li><b>Type:</b> {props.storm.stormType}</li>
                  <li><b>Category:</b> {props.storm.stormCat}</li>
                  <li><b>Position:</b></li>
                <ul style={{ listStylePosition: 'inside', fontSize: 14 }}>
              <li>Latitude: {props.storm.lat}</li>
              <li>Longitude: {props.storm.lng}</li>
            </ul>
            <li><b>Movement:</b> {props.storm.movement}</li>
            <ul style={{ listStylePosition: 'inside', fontSize: 14 }}>
              <li><b>KPH:</b> {props.storm.movementSpeedKPH}</li>
              <li><b>MPH:</b> {props.storm.movementSpeedMPH}</li>
            </ul>
            <li><b>Wind Speed KPH:</b> {props.storm.windSpeedKPH}</li>
            <li><b>Wind Speed MPH:</b> {props.storm.windSpeedMPH}</li>
          </ul>
        </Card.Body>
      </Card>
    );
}


const AffectedView = (props) => {
      
  return (
    <Card style={{ marginTop: 10 }}>
      <Card.Header >
        <font size="3"><b>{props.title}</b></font>     
        {}
        <Button className="float-right" onClick={props.closeButton}>Close </Button>   
          </Card.Header>
            <Card.Body>
             <ul>
            
            {props.items.map(i => {

              if(i.type=="Building")
              return <li>{i.name}</li>;

              else if (i.type=="Vessel")
              {
                return <li>{i.vessel_name}</li>;
              }
             
              else 
              return <li>N/A</li>
             })}

             </ul>
      </Card.Body>
    </Card>
  );
}



export  {StormView, AffectedView};