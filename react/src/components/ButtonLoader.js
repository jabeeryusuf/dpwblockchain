import React     from 'react';
import PropTypes from 'prop-types';
import {Button}  from 'react-bootstrap';
import SpinnerIcon from 'react-loader';

const sPropTypes = {
  spinColor:      PropTypes.string,
  spinConfig:     PropTypes.object,
  spinAlignment:  PropTypes.string
};

function Spinner({
  spinColor       = '#fff',
  spinConfig      = {
    length: 4,
    lines:  15,
    radius: 3,
    width:  2,
  },
  spinAlignment   = 'left',
  ...rest
}) {
  const style = {
    display:      'inline-block',
    height:       '11px',
    position:     'relative',
    width:        '16px',
  };

  const spinAlignmentStyle = {
    display: 'inline-block',
    float:   spinAlignment + ' !important',
    padding: '0 10px'
  };

  return (
    <div style={spinAlignmentStyle} {...rest}>
      <div style={style}>
        <SpinnerIcon {...spinConfig} color={spinColor} loaded={false} />
      </div>
    </div>
  );
}

Spinner.propTypes = sPropTypes;



const propTypes = {
  bsStyle:        PropTypes.string,
  children:       PropTypes.node,
  disabled:       PropTypes.bool,
  icon:           PropTypes.node,
  loading:        PropTypes.bool,
  spinColor:      PropTypes.string,
  spinAlignment:  PropTypes.string
};

const ButtonLoader = React.forwardRef((
  {
    //bsStyle   = 'default',
    children      = null,
    disabled      = false,
    icon          = null,
    loading       = false,
    spinColor     = '#fff',
    spinAlignment = 'left',
    ...rest
  },ref) =>  {

  const renderIcon = () => {
    if (loading) {
      return <Spinner spinColor={spinColor} spinAlignment={spinAlignment} />;
    }

    return icon;
  }

  const buttonDisabled = disabled;// || loading;

  return <Button ref={ref} disabled={buttonDisabled} {...rest}>{renderIcon()} {children}</Button>;
})

ButtonLoader.propTypes = propTypes;

export default ButtonLoader;
