import React from 'react';
import { Collapse, ListGroup } from 'react-bootstrap';
import ReactTable from 'react-table';
import { FaArrowDown, FaArrowUp } from 'react-icons/fa';

/**
 * This class is being used for the shipment detail page
 * 
 * Creates a list group with dropdowns containing a table of information
 */
class Accordion extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: {},
      list: props.list,
      columns: props.columns
    }
  }

  componentDidMount() {
    const { list } = this.state;
    let open = Object.assign({}, this.state.open);
    for (let obj of list) {
      open[obj.po] = false;
    }

    this.setState({ open });
  }
  componentDidUpdate(prevProps){
    if(prevProps != this.props){
      this.setState({ list:this.props.list})
    }
  }

  render() {
    const { list, columns } = this.state;
    return(
      <>
        {
          list.map((po, idx) => {
            return (
              <div key={ "div-1" + idx }>
                <ListGroup.Item key={"list-item" + idx}
                                action
                                active = {this.state.open[po.po]} 
                                onClick={() => {
                                  let openCopy = Object.assign({}, this.state.open);
                                  openCopy[po.po] = ! openCopy[po.po];
                                  this.setState({ open: openCopy });
                                }}>
                { po.po }
                &nbsp;
                { this.state.open[po.po] ? <FaArrowUp key={ "badge-up" + idx } /> : <FaArrowDown key={ "badge-down" + idx } /> }
                </ListGroup.Item>
                <Collapse key={"collapse" + idx} 
                          in={this.state.open[po.po]} >
                  <div key={ "div-2" + idx } >
                    <ReactTable key={"table" + idx}
                                data={po.items}
                                columns={columns}
                                defaultPageSize={5}
                                className="-striped -highlight" />
                  </div>
                </Collapse>
              </div>
            )
          
          })
        }
      </>
    )
  }
}

export default Accordion;