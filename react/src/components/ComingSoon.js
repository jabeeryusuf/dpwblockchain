import React from 'react';
import {Jumbotron} from 'react-bootstrap';

class ComingSoon extends React.Component {

  render() {
    return (
      <>
      <br/>
      <Jumbotron>
        <h1>Coming Soon!!!!</h1>
      </Jumbotron>
      </>
    );
  }
}

export default ComingSoon;