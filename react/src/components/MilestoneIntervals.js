import React from 'react';
import { connect } from 'react-redux';
import { Form, FormGroup, Button, Alert, Col, Row } from 'react-bootstrap';
import { withLoader } from '.';
// import { create } from 'domain';

const options = [ 
  <option key={'days'} value={'days'}>days</option>,
  <option key={'weeks'} value={'weeks'}>weeks</option>,
  <option key={'months'} value={'months'}>months</option>  
]

class MilestoneIntervals extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      intervals : {
        create_date: 0,
        create_dateInt: '',
        factory_booking_date: 0,
        factory_booking_dateInt: '',
        factory_appointment_date: 0,
        factory_appointment_dateInt: '',
        factory_delivery_date: 0,
        factory_delivery_dateInt: '',
        receive_date: 0,
        receive_dateInt: '',
        shipment_allocation_date: 0,
        shipment_allocation_dateInt: '',
      }
    };

  }

  async componentDidMount() {
    const { startLoading, stopLoading } = this.props;

    startLoading();
    
    let list = await this.props.client.getStuff(`SELECT * FROM config_settings WHERE purpose='milestones'`);

    let create_date = 0;
    let create_dateInt = '';
    let factory_booking_date = 0;
    let factory_booking_dateInt = '';
    let factory_appointment_date = 0;
    let factory_appointment_dateInt = '';
    let factory_delivery_date = 0;
    let factory_delivery_dateInt = '';
    let receive_date = 0;
    let receive_dateInt = '';
    let shipment_allocation_date = 0;
    let shipment_allocation_dateInt = '';

    for (let row of list) {
      console.log(row.interval);
      switch (row.setting) {
        case "create_date":
          create_date = row.number;
          create_dateInt = row.interval;
          break;
        case "factory_booking_date":
          factory_booking_date = row.number;
          factory_booking_dateInt = row.interval;
          break;
        case "factory_appointment_date":
          factory_appointment_date = row.number;
          factory_appointment_dateInt = row.interval;
          break;
        case "factory_delivery_date":
          factory_delivery_date = row.number;
          factory_delivery_dateInt = row.interval;
          break;
        case "receive_date":
          receive_date = row.number;
          receive_dateInt = row.interval;
          break;
        case "shipment_allocation_date":
          shipment_allocation_date = row.number;
          shipment_allocation_dateInt = row.interval;
          break;
        default:
          throw new Error("What was that?") // will change, no worries
      };
    }

    this.setState({
      intervals : {
        create_date: create_date,
        create_dateInt: create_dateInt,
        factory_booking_date: factory_booking_date,
        factory_booking_dateInt: factory_booking_dateInt,
        factory_appointment_date: factory_appointment_date,
        factory_appointment_dateInt: factory_appointment_dateInt,
        factory_delivery_date: factory_delivery_date,
        factory_delivery_dateInt: factory_delivery_dateInt,
        receive_date: receive_date,
        receive_dateInt: receive_dateInt,
        shipment_allocation_date: shipment_allocation_date,
        shipment_allocation_dateInt: shipment_allocation_dateInt
      }
    });

    stopLoading();

  }

  handleChange = (event) => {
    const { name, value } = event.target;
    const { intervals } = this.state;

    this.setState({
      intervals : {
        ...intervals,
        [name] : value
      }
    });
  }

  render() {
    const { intervals } = this.state;

    return (
      <Form>
        {this.props.successMessage ? <Alert variant="success" dismissible>{this.props.successMessage}</Alert> : ''}
        {this.props.errorMessage ? <Alert variant="danger" dismissible>{this.props.errorMessage}</Alert> : ''}
        <FormGroup>
          <Form.Label>PO Cons date approaching within:</Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.create_date}
                            name="create_date"
                            id="create_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.create_dateInt && <Form.Control as="select"
                            name="create_dateInt"
                            defaultValue={intervals.create_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Form.Label>Factory booking date not approved after:</Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.factory_booking_date}
                            name="factory_booking_date"
                            id="factory_booking_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.factory_booking_dateInt && <Form.Control as="select"
                            name="factory_booking_dateInt"
                            defaultValue={intervals.factory_booking_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
        <Form.Label>Factory appointment date approaching in: </Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.factory_appointment_date}
                            name="factory_appointment_date"
                            id="factory_appointment_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.factory_appointment_dateInt && <Form.Control as="select"
                            name="factory_appointment_dateInt"
                            defaultValue={intervals.factory_appointment_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
        <Form.Label>Receive date not recorded after:</Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.factory_delivery_date}
                            name="factory_delivery_date"
                            id="factory_delivery_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.factory_delivery_dateInt && <Form.Control as="select"
                            name="factory_delivery_dateInt"
                            defaultValue={intervals.factory_delivery_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
        <Form.Label>Shipment allocation date not recorded after:</Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.receive_date}
                            name="receive_date"
                            id="receive_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.receive_dateInt && <Form.Control as="select"
                            name="receive_dateInt"
                            defaultValue={intervals.receive_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
        <Form.Label>Time of departure not recorded after:</Form.Label>
          <Row>
            <Col>
              <Form.Control type="number"
                            value={intervals.shipment_allocation_date}
                            name="shipment_allocation_date"
                            id="shipment_allocation_date"
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} />
            </Col>
            <Col>
              {intervals.shipment_allocation_dateInt && <Form.Control as="select"
                            name="shipment_allocation_dateInt"
                            defaultValue={intervals.shipment_allocation_dateInt}
                            disabled={this.props.submitting}
                            onChange={(e) => {this.handleChange(e)}} >{options}</Form.Control>}
            </Col>
          </Row>
        </FormGroup>
        <Button block disabled={this.props.submitting} onClick={() => this.props.onSubmit(intervals)}>{this.props.submitting ? "Submitting..." : "Submit"}</Button>
      </Form>
    );
  }
}

function mapStateToProps(state) {
  return { client : state.auth.client }
};

export default withLoader(connect(mapStateToProps)(MilestoneIntervals));