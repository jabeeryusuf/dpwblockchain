import React from "react";
import { Col, Row, Table } from "react-bootstrap";

const TRow = (props: any) => {
  return (
    <tr>
      <th>{props.title}</th>
      <td>
        <h5>{props.children}</h5><br/>
        {props.buttons}
      </td>
    </tr>
  );
};

interface ICaseMapTableProps {
  groupName: string;
  messageTemplateName: string;
  groupButtons: any;
  templateButtons: any;
}

class CaseMapTable extends React.Component<ICaseMapTableProps, any> {
  constructor(props: ICaseMapTableProps) {
    super(props);
  }

  public render() {
    return (
      <Row>
        <Col className="tablehead">
          <Table bordered responsive>
            <tbody>
              <TRow title={"Assigned Asset:"}
                children={this.props.groupName ? this.props.groupName : "No default asset"}
                buttons={this.props.groupButtons} />
              <TRow title={"Default Message Template:"}
                children={this.props.messageTemplateName ? this.props.messageTemplateName : "No assigned template"}
                buttons={this.props.templateButtons} />
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

export default CaseMapTable;
