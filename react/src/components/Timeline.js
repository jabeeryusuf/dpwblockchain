import React from 'react'

class Timeline extends React.Component {
  render(){
    return(
          <div>
            <div className="m-portlet  m-portlet--full-height ">
            	<div className="m-portlet__head">
            		<div className="m-portlet__head-caption">
            			<div className="m-portlet__head-title">
            				<h3 className="m-portlet__head-text">
            					Shipment History
            				</h3>
            			</div>
            		</div>
            		<div className="m-portlet__head-tools">
            			<ul className="m-portlet__nav">
            				<li className="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
            					<a href="" className="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl m-dropdown__toggle">
            					<i className="la la-ellipsis-h m--font-brand"></i>
            					</a>
            					<div className="m-dropdown__wrapper">
            						<span className="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
            						<div className="m-dropdown__inner">
            							<div className="m-dropdown__body">
            								<div className="m-dropdown__content">
            									<ul className="m-nav">
            										<li className="m-nav__section m-nav__section--first">
            											<span className="m-nav__section-text">Quick Actions</span>
            										</li>
            										<li className="m-nav__item">
            											<a href="" className="m-nav__link">
            											<i className="m-nav__link-icon flaticon-share"></i>
            											<span className="m-nav__link-text">Activity</span>
            											</a>
            										</li>
            										<li className="m-nav__item">
            											<a href="" className="m-nav__link">
            											<i className="m-nav__link-icon flaticon-chat-1"></i>
            											<span className="m-nav__link-text">Messages</span>
            											</a>
            										</li>
            										<li className="m-nav__item">
            											<a href="" className="m-nav__link">
            											<i className="m-nav__link-icon flaticon-info"></i>
            											<span className="m-nav__link-text">FAQ</span>
            											</a>
            										</li>
            										<li className="m-nav__item">
            											<a href="" className="m-nav__link">
            											<i className="m-nav__link-icon flaticon-lifebuoy"></i>
            											<span className="m-nav__link-text">Support</span>
            											</a>
            										</li>
            										<li className="m-nav__separator m-nav__separator--fit">
            										</li>
            										<li className="m-nav__item">
            											<a href="" className="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Cancel</a>
            										</li>
            									</ul>
            								</div>
            							</div>
            						</div>
            					</div>
            				</li>
            			</ul>
            		</div>
            	</div>
            	<div className="m-portlet__body">
            		<div className="m-scrollable" data-scrollable="true" data-height="380" data-mobile-height="300">
            			<div className="m-timeline-2">
            				<div className="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
            	         {this.props.children}
            				</div>
            			</div>
            		</div>
            	</div>
            </div>

            </div>


    )
  }
}
export default Timeline;
