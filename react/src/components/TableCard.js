
import React from 'react';
import Table from '../components/Table';
import { Row, Col } from 'react-bootstrap';

const TableCard = (props) => {
  return (
    <div style = {{marginBottom:20}}>
      <Row>
        <Col sm={{ span: 3, offset: 4 }}>
          { props.heading && 
            <h1> {props.heading} </h1>
          }
        </Col>
        <Col sm={{ offset: 3 }}>
          {props.button ? props.button : ''}
        </Col>
      </Row>
      <Table style={{ cursor: 'pointer' }} className="-highlight -striped"
        data={props.data}
        columns={props.columns}
        getTdProps = {props.getTdProps}
        defaultPageSize = {props.tableSpecs.rowNumber}
        filterable = {props.tableSpecs.filterable} 
        pivotBy = {props.pivotBy}
        />
    </div>
  )
}
export default TableCard; 