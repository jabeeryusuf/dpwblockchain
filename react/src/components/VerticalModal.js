import React from 'react';
import { Modal, Row } from 'react-bootstrap';

class VerticalModal extends React.Component {
  render() {
    return (
      <Modal {...this.props} size={this.props.size} aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {this.props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="justify-content-center">
            {this.props.page}
        </Modal.Body>
      </Modal>
    )
  }
}

export default VerticalModal;