
import * as React from 'react';

const GreenPortlet = (props) => {

    return (
      <div className="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--rounded">
        <div className="m-portlet">
          <div className="m-portlet__head">
            <div className="m-portlet__head-caption">
              <div className="m-portlet__head-title">
                <span className="m-portlet__head-icon">
                </span>
                <h3 className="m-portlet__head-text">
                  {props.header}
                </h3>
              </div>
            </div>
            <div className="m-portlet__head-tools">
                <ul className="m-portlet__nav">
                  <li className="m-portlet__nav-item">
                    {props.onClick && <button onClick={props.onClick} className="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-brand m-btn--icon m-btn--pill">{props.buttonText}</button>}
                  </li>
                </ul>
              </div>
          </div>
          <div className="m-portlet__body">
              {props.content}
          </div>
          {props.footer &&
            <div className="m-portlet__foot">
              {props.footer}
            </div>
          }
        </div>
      </div>
    );
  }

  export default GreenPortlet;
