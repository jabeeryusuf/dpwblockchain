import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { authActions } from '../redux/actions';
import { Nav, NavDropdown, Navbar } from 'react-bootstrap';

//const s3URL = "https://s3-eu-west-1.amazonaws.com/dpwchainpublic/logo/honda-logo.png";
const s3URL = "https://s3-eu-west-1.amazonaws.com/dpwchainpublic/logo/";

class OrgDropdown extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }
  
  onClick(e,user) {
    e.preventDefault();

    // reset login status
    this.props.dispatch(authActions.change(user));
  }


  render() {
    let {user,orgs, allUsers} = this.props;

    return (
      <NavDropdown title={(orgs[user.org_id] ? orgs[user.org_id].name : user.org_id) + " - " + user.org_id }>
        {
          allUsers.map((user,index) => 
            <NavDropdown.Item key={index} onClick={(e) => this.onClick(e,user)}> 
              {(orgs[user.org_id] ? orgs[user.org_id].name : user.org_id) + " - " + user.org_id}
            </NavDropdown.Item>)
        }
      </NavDropdown>
    )
  }
}


class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
        emailLogo: null,
    };
}

  async componentDidMount() {
    console.log("Consoled");
    this.getEmailLogo();
  }


  async getEmailLogo() {

    var org_logo = "dpw-logow.png";

    if(this.props.orgApi)
    {
    const logo = await this.props.orgApi.getSetting(this.props.org.org_id, "logo");
    org_logo =  logo.value;
    console.log("OrgAPi exists");   
    }

    else
    console.log("NOT Exist Orgapi");

    this.setState({
    emailLogo: org_logo,
    });
  }


  logout(e) {
    e.preventDefault();

    // reset login status
    this.props.dispatch(authActions.signOut());
  }



  render () {

    //this.getEmailLogo();
    return (
      
      <Navbar variant="dark" fixed="top" bg="dark" className="p-0">
        <Navbar.Brand className="col-sm-3 col-md-2 mr-0" href="/">        
          {<img style={{ maxHeight: "45px", maxWidth: "193px", width: "auto", height: "auto" }}
                            src={s3URL+this.state.emailLogo} alt="Loading..."/>}
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end" >
          <Nav className="mr-auto" >            
          </Nav>
          <Nav className="ml-auto" >                  
                    {this.props.org && <OrgDropdown {...this.props} />}
                    {this.props.user && <Nav.Link onClick={(e) => this.logout(e)}>| Sign Out</Nav.Link>}
          </Nav>
        </Navbar.Collapse>
     </Navbar>
    )
  }
}

function mapStateToProps(state) {
  return { org: state.auth.org,
           allUsers: state.auth.allUsers,
           client: state.auth.client, 
           user: state.auth.user,
           isError: state.status.isError,
           status: state.status.status,
           orgs: state.orgs.orgs,
           orgApi: state.auth.orgApi
          };
}

export default withRouter(connect(mapStateToProps)(Header));