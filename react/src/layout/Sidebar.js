import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link, withRouter} from 'react-router-dom';
import { Nav,Badge} from 'react-bootstrap';
import {FiMessageSquare,  
        FiPlusCircle,
        FiChevronRight,FiChevronDown, FiChevronUp } from 'react-icons/fi';
import { connect } from 'react-redux';


class _SideMenuItem extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,
    route: PropTypes.string.isRequired,
  };

  render () {
    const {match, currentPath,route,tab, title,icon,badge,notificationChannels} = this.props;
    const active = tab ? currentPath.startsWith(match.url + route) : (currentPath === (match.url + route));
   
    const EntryIcon = icon ? icon : FiChevronRight;

    const fullRoute = tab ? route + "/" + tab : route;

    // console.log("tab" + tab + 'route' + route)

    let badgeText = "";
    if (badge) {
      let badgeEntry = notificationChannels[badge.channel] && 
        notificationChannels[badge.channel][badge.id];
        
      if (badgeEntry) {
        badgeText = badgeEntry;
      }
      else if (badgeEntry !== 0) {
        badgeText = '#';
      }
    }
    


    return (
      <Nav.Item >
        <span>
        <Nav.Link as={Link} to={match.url + fullRoute} active={active} >
            <EntryIcon className="icon" />
            {" " + title + "      "}
            {badge ? <Badge>{badgeText}</Badge> : ""}
        </Nav.Link>

        </span>
      </Nav.Item>
    )
  }
}


function mapStateToProps(state) {
  return { notificationChannels: state.notification.channels};
}

let SideMenuItem = connect(mapStateToProps)(_SideMenuItem);


class _GroupSideMenu extends Component {

  static defaultProps = {
    addConversations: false,
    expanded: true,
  }

  constructor(props) {
    super(props);
    
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      expanded: true,
      show: false,
    };

  }

  handleClose() {
    this.setState({ show: false });
  }

  onClickExpand(e) {
    e.preventDefault();
    this.setState(
      {expanded: !this.state.expanded}
    )
  }

  onClickeAddConversation(e) {
    e.preventDefault();
    
    this.setState({show: true});
  }

  render() {
    const {group } = this.props;

    if (group.dynamic) {
      return this.renderDynamicGroup();
    }
    else if (group.groupTitle) {
      return this.renderStaticGroup();
    }
    else {
      return this.renderStaticItems();
    }
  }

  renderTitle(Icon,to,onClick) {
    const {group} = this.props;
    const {groupTitle} = group;

    //console.log('TITLE')
    //console.log(group);
    
    return (
      <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>{groupTitle}</span>
        <Link className="d-flex align-items-center text-muted" to={to} onClick={onClick}>
          <Icon className="icon"/>
        </Link>
      </h6>
    );
  }

  renderDynamicGroup() {
    let {match, group, currentPath} = this.props;
    let {itemShow, itemTitle, itemPath, groupSelectPath} = group;
    let itemsProp = this.props[group.itemsProp];

    //console.log(itemsProp);
    return (
      <>
        {this.renderTitle(FiPlusCircle,match.url + groupSelectPath,null)}

        {Object.keys(itemsProp).reduce((items, k) => {
     
          if (!itemShow(itemsProp[k])) {
            return items;
          }
          
          let title = itemTitle(itemsProp[k]);
          let route = itemPath(itemsProp[k]);
       

          items.push(
            <SideMenuItem key={items.length} currentPath={currentPath} icon={FiMessageSquare} title={title} route={route} match={match}/>
          )

          return items;
        },[]  )}
      </>
    );
  }


  renderStaticItems() {
    const {match, currentPath, group} = this.props;
    const {items} = group;

    return (
      items.map((child,index) => {
        return (<SideMenuItem key={index} currentPath={currentPath} icon={child.icon} title={child.title} route={child.route} tab={child.tab} badge={child.badge} match={match} />)
      })
    )
  }


  renderStaticGroup() {
    const {match,currentPath,group} = this.props;
    let {childs} = group;
    
    const active = childs && childs.find(itm => (match.url + itm.route) === currentPath) != null;
    
    let expanded = this.state.expanded || active;
    let icon = expanded ? FiChevronUp  : FiChevronDown;

    //console.log("STATIC");
    //console.log(group)

    return (

      <>
        {this.renderTitle(icon,'#', e => this.onClickExpand(e))}
        {expanded && this.renderStaticItems()}
      </>
    )
  }
}

function mapGroupStateToProps(state) {
  return { conversations: state.conversation.conversations,
           users: state.user.users };
}

let GroupSideMenu = connect(mapGroupStateToProps)(_GroupSideMenu);


class Sidebar extends Component {

  static propTypes = {
    groups: PropTypes.array,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    currentPath: PropTypes.string.isRequired,
  }

  render () {
    const {match,currentPath} = this.props;    
    const groups = this.props.groups;

    //console.log('GROUP:')
    //console.log(groups);

    return (
      <Nav className="col-md-2 d-none d-md-block bg-dark sidebar">
        <div className="sidebar-sticky">
        
            {groups && groups.map((group,i) => {
              return <GroupSideMenu key={i}
                                    group={group}
                                    currentPath={currentPath}
                                    match={match}/>
            })}
          </div>
      </Nav>
    )
  }
}



export default withRouter(Sidebar)
