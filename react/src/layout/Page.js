import React, { Component } from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import {Row, Container} from 'react-bootstrap';

class Page extends Component {

  render() {
    let {match,currentPath} = this.props;

    return (

      <>
        <Header renderAsideHeader renderActiveUser/>
        <Container fluid>
          <Row className="h-100">
            <Sidebar groups={this.props.sideMenu} match={match} currentPath={currentPath} />
            <div className="main col-md-9 ml-sm-auto col-lg-10 px-4"> 
              {this.props.children}
            </div>
          </Row>
        </Container>
      </>
    )
  }
}


export default Page;