import React from "react";
import { Nav as TsNav } from "react-bootstrap";
import { Link, Route, Switch, withRouter, RouteComponentProps } from "react-router-dom";
import { Subheader } from ".";

const Nav: any = TsNav;

export interface ILandingTab {
  label: string;
  path?: string;
  component: any;
  props?: any;
}

interface IProps extends RouteComponentProps<IMatchParams> {
  tabs: ILandingTab[];
  basePath: string;
}

interface IMatchParams {
  tab: string;
}

class _Landing extends React.Component<IProps, any> {

  constructor(props: any) {
    super(props);
  }

  public render() {
    const { tabs, match , location } = this.props;
    console.log("Tab Paths");
    console.log(tabs);
    console.log(match);
    console.log(location);

    return (
      <>
        <Subheader/>

        <Nav variant="tabs" defaultActiveKey={location.pathname}>
          {
            tabs.map((e, i) =>
       
              <Nav.Item>
                 { console.log("Inside Nav Item-------------") }
                 { console.log(e) }
                 { console.log(i) }
                 { console.log(match.url) }
                  { console.log(match.path) }
                  { console.log(e.path) }                 
                  
                {/* <Nav.Link id={i} as={Link} href=ds{match.path + e.path} to={match.path + e.path}>{e.label}</Nav.Link>*/}
                <Nav.Link id={i} as={Link} href={match.url+e.path} to={match.url+e.path}>{e.label}</Nav.Link>
              </Nav.Item>
            )
          }
        </Nav>

        <Switch>
          {
            tabs.map((e) => {
              const Component = e.component;
              return <Route path={match.path + e.path}
                render={ (props) => <Component {...e.props} {...props} basePath={this.props.basePath} /> }
              />;
            })
          }
        </Switch>
      </>
    );
  }
}

export const Landing = withRouter(_Landing);
