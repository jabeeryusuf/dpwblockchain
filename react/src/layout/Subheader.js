/* eslint-disable */
import React from 'react'
import {Row,Col} from 'react-bootstrap'

class Subheader extends React.Component {

  render() {
    
    let {title} = this.props;

    return (
      <Row className={"pt-3 pb-2 mb-3 align-items-center " + this.props.className} >
          
          <Col>
            <h5 className="h5">{this.props.title}</h5>
          </Col>
      
          {this.props.children}

{/*          <Col>
            <ButtonToolbar className="mb-2 mb-md-0 justify-content-center">
              <ButtonGroup className="mr-2">
                <Button variant="outline-secondary" size="sm">Share</Button>
                <Button variant="outline-secondary" size="sm">Export</Button>
              </ButtonGroup>

            <DropdownButton variant="outline-secondary" size="sm" title="This Week"/>
          </ButtonToolbar>
          </Col> 
*/}


      </Row>
    )
  }
}

export default Subheader
