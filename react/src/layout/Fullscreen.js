import React from 'react';
import { withRouter } from 'react-router-dom'
import { Container, Row,Col,Button } from 'react-bootstrap'
import {FiX } from 'react-icons/fi';

class Fullscreen extends React.Component {
 
  constructor(props) {
    super(props);
    
    this.myRef = React.createRef();
  }

  back = (e) => {
    let {history} = this.props;

    e.stopPropagation();
    history.goBack();
  };

  render() {

    return (
      <>
        <br/>
        <br/>
        <br/>
      
        <Container>
          <Row>
            <Col >
              
              <Button className="fs-close close-xl" onClick={this.back} variant="outline-dark" >
                <FiX className="fs-close close-xl icon"/>
                <span>back</span>
              </Button>
            </Col>
          </Row>
          <br/>
          <br/>
          {this.props.children}
        </Container>
      </>
    )
  }
}

export default withRouter(Fullscreen);
