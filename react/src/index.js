import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter  } from 'react-router-dom';
import { Provider } from 'react-redux'

// Application Setup
import { store } from "./redux/store"
import AuthApp from './auth/AuthApp'

import * as serviceWorker from './utils/serviceWorker';

// Basic App setup
ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <AuthApp />
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
