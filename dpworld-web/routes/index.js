var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {title: 'Hointer'});
});

router.get('/index.html', function (req, res) {
    res.render('index', {title: 'Hointer'});
});

router.get('/:myfile', function (req, res) {
    res.render(req.params.myfile, {

    });
});


module.exports = router;
