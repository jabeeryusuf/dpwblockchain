
var express = require('express');
var router = express.Router();
/**
 * Setup RESTful paths for delivering data to jQuery in static pages.
 *     You will need as many of these as is required to handle your different RESTful paths
 *     This example allows different keywords to be passed and "SWITCH" to build the right
 *     QUERY based on the CONTENT_TYPE value passed into the path
 */
router.get('/db/:content_type', function (objRequest, objResponse) {
    // Set response type from text/html to application/json
    objResponse.setHeader('content-type', 'application/json');

    // Get a connection to the database
    db_pool.getConnection(function (objError, objConnection) {

        // check for any connection errors
        if (objError) {
            console.log("Can not connect to the database");
            // There was an error, so send JSON with an error message and an HTTP status of 503 (Service Unavailable)
            sendError(objResponse, 503, 'error', 'connection', objError);

        } else {

            // We have a connection to the database server and db:
            //      Let's figure out which CONTENT_TYPE they want and build
            //      the correct QUERY to request from MySQL
            var strQuery = "";
            switch (objRequest.params.content_type) {
                case 'frontpagenews':
                    strQuery = "SELECT * FROM hointerdb.hointer_in_the_news where frontpage=1 order by STR_TO_DATE(date, '%M %d,%Y') DESC;";
                    break;
                case 'news':
                    strQuery = "SELECT * FROM hointerdb.hointer_in_the_news order by STR_TO_DATE(date, '%M %d,%Y') DESC;";
                    break;
                case 'blog':
                    strQuery = "SELECT * FROM hointerdb.hointer_blog order by STR_TO_DATE(date, '%M %d,%Y') DESC;";
                    break;
                default :
                    // We don't know how to handle this kind of CONTENT_TYPE so
                    // we simply want to blow it up right now
                    sendError(objResponse, 503, 'error', 'content-type unkown', {code: 'CONTENT-TYPE MISMATCH'});
            }

            //Assuming we knew how to handle the CONTENT_TYPE above, we now send the query to MySQL
            objConnection.query(
                strQuery,
                function (objError, objRows, objFields) {
                    if (objError) {

                        console.log("Database query failed");
                        // Couldn't get the query to run, so send JSON with an error message and an HTTP status of 500 (Internal Server Error)
                        sendError(objResponse, 500, 'error', 'query', objError);

                    } else {

                        // We have query results back, so lets put the results in JSON and return them
                        objResponse.send({
                            result: 'success',
                            err: '',
                            err_type: '',
                            fields: objFields,
                            rows: objRows,
                            length: objRows.length
                        });

                    }
                }
            );
            objConnection.release();
        }
    });
});

/**
 * sendError is the JSON we use to send information about errors to the client-side.
 *     We need to check on the client-side for errors.
 */
function sendError(objResponse, iStatusCode, strResult, strType, objError) {
    // I could throw errors at the HTTP response level, but I want to trap handled errors in my code instead
    //objResponse.statusCode = iStatusCode;
    objResponse.send({
        result: strResult,
        err: objError.code,
        err_type: strType
    });
}

module.exports = router;